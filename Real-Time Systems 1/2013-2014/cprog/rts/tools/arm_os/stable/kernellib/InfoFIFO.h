#ifndef __INFOFIFO_H__
#define __INFOFIFO_H__


/*"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# total number of fifos that can be mapped in the Communication Assist
*/
typedef enum {NUM_FIFOS = 32, NOT_DEFINED = -1} enumForNumberOfFifos;



struct Info_FIFO
{

  uint32_t *  fifoAddress;
  uint32_t *  fifoAdminAddress;
  uint32_t writec : 32;
  uint32_t readc : 32;
  int32_t id : 32;
  uint32_t token_size : 32;
  uint32_t fifo_size : 32;
  uint32_t direction : 32;
  uint32_t con_ID    : 32;
  uint32_t active : 32;
  uint32_t tdma :32;
  uint32_t remote :32;

  void* data;
};

struct Info_FIFO_remote
{
  uint32_t fifoAddress : 32;
  uint32_t fifoAdminAddress : 32;
  int32_t id : 32;
  uint32_t token_size : 32;
  uint32_t fifo_size : 32;
  uint32_t direction : 32;
  uint32_t con_ID    : 32;
};


struct FIFO_ARRAY
{
  struct Info_FIFO *p_fifo;
};


struct TDMATimes
{
	uint32_t conn : 32;
};


struct TileArbiter
{
	uint32_t type	: 32;
	uint32_t totalT : 32;
	uint32_t procT : 32;
};

struct ConArbiter
{
	uint32_t type	: 32;
	uint32_t totalT : 32;
	uint32_t conT[NUM_FIFOS];
};


#endif 
