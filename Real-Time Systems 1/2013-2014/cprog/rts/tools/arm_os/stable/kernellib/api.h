#ifndef __API_H__
#define __API_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "InfoFIFO.h"
#include "memmap.h"

// define base address for FIFOs
// in memmap.h

/* define address for semaphores */
//#define LOCK_TURN_ADDRESS 0x295000
//#define LOCK_WP1_ADDRESS 0x295004
//#define LOCK_WP2_ADDRESS 0x295008

// To: memmap.h
//#define LOCK_CHOOSING_ADDRESS 0x280000
//#define LOCK_NUMBER_ADDRESS 0x270000

/* defines for locks */
#define NUMBER_OF_LOCKS 480
// max number of processes competing for one lock
// in total 16 threads on each core + main(). (2*(16+1)
#define NUMBER_OF_PROC 34

#define EXIT_ENTER_CS 0
#define EXIT_EXIT_CS 1
#define EXIT_LOCK 2
#define EXIT_LOCK_ERROR 3

#define TASK_START 0
#define TASK_STOP 1

#define CACHE_CTRL_NOOP 0x0
#define CACHE_CTRL_FLUSH_RANGE 0x1
#define CACHE_CTRL_INV_RANGE 0x2

#define CACHE_CTRL_FUNCTION ((volatile uint32_t*)0x01810000)
#define CACHE_CTRL_FROM ((volatile uint32_t*)    0x01810004)
#define CACHE_CTRL_UNTIL ((volatile uint32_t*)   0x01810008)

#define min(a, b)  (((a) < (b)) ? (a) : (b))
#define max(a, b)  (((a) > (b)) ? (a) : (b))

#define EXIT_ERROR	0
#define EXIT_OK		1
#define EXIT_PARAM_ER	2
#define EXIT_MALLOC	3
#define EXIT_FIFO_FULL	4
#define EXIT_FIFO_EMPTY 5

#define READ	0
#define WRITE	1

#define UNBLOCK	0
#define BLOCK	1

#define UPTD_MASK	0x00000001
#define WRAP_SET_MASK	0x80000000
#define WRAP_RST_MASK	0x00000000
#define WR_RD_MASK	0x7FFFFFFF


int createFifo(void*, uint32_t, uint32_t, int); /* .. , size in bytes, number of buffer places */

void deleteFifo(void*, int);

void initFifo(volatile struct Info_FIFO*, void*, uint32_t , uint32_t, int);

void initFifoInfo(void*);

volatile struct Info_FIFO* openFifo(void*, int, int);

/* Productor Functions */
int writeFifo(volatile struct Info_FIFO* , void*, int);

void* acquire_w(volatile struct Info_FIFO*);

void release_w(volatile struct Info_FIFO*);

/* Consumer Functions */
int readFifo(volatile struct Info_FIFO*, int*, int);

void *acquire_r(volatile struct Info_FIFO*);

void release_r(volatile struct Info_FIFO*);

void initSemaphores(int numberoflocks);

/* lockid is used to calculate the memory addresses */
void lock(int lockid, int id, int num);
void unlock(int lockid, int id, int num);

#endif

