#include "api.h"
#include "io.h"
#include "kernel.h"	// Fixes implicit declarations (good imho)
#include "kernel2.h"
#include "InfoFIFO.h"

extern uint32_t  numFifos;
volatile struct Info_FIFO *info;

#define OFFSET_NUM	sizeof(numFifos)
#define NUM_FIFOS 32
#define NOT_DEFINED -1
#define apiDebug 0

#define TRUE			1
#define FALSE			0

volatile uint32_t color = 0;
volatile int lockNumber[NUM_FIFOS] = {0};
volatile uint32_t lockBusy[NUM_FIFOS] = {0};
volatile uint32_t lockColors[NUM_FIFOS];

int createFifo(void* shmaddr, uint32_t tokensize, uint32_t fifosize, int id)
{
	// Bounds check and uninitialized check
	if( info && id < NUM_FIFOS && info[id].id == NOT_DEFINED )
	{
		info[id].id = id;
		info[id].fifoAddress = shmaddr;
		info[id].token_size = tokensize;
		info[id].fifo_size = fifosize;
		info[id].data = malloc( tokensize * fifosize );
		
		info[id].fifoAdminAddress = 0;
		info[id].writec = 0;
		info[id].readc = 0;
		info[id].direction = 0;
		info[id].con_ID = 0;
		info[id].active = 0;
		info[id].tdma = 0;
		info[id].remote = 0;

		return id;
	}
	return NOT_DEFINED;
}


void deleteFifo(void* shmaddr, int id)
{
	// Not really necessary
}

void initRemoteFifo(volatile struct Info_FIFO* ptrFifo,uint32_t *fifoAddress, uint32_t *fifoAdminAddress, uint32_t tokensize, uint32_t fifosize, int id)
{
}


void initFifo(volatile struct Info_FIFO* ptrFifo,void* data, uint32_t tokensize, uint32_t fifosize, int id)
{
}

void initFifoInfo(void* shmaddr)
{
	int i;

	if( !shmaddr ) return;
	
	info = malloc( NUM_FIFOS * sizeof( struct Info_FIFO ) );

	for( i = 0; i < NUM_FIFOS; i++ )
	{
		info[i].id = NOT_DEFINED;
	}
}

volatile struct Info_FIFO* openFifo(void* shmaddr, int id, int mode)
{
	if( id < NUM_FIFOS && shmaddr != NULL )
	{
		if( info[ id ].active == 0 )
		{
			info[ id ].active = 1;
		}
		return &info[ id ];
	}
	return NULL;
}

void* acquire_w(volatile struct Info_FIFO* ptrFifo)
{
	// N/A
	return NULL;
}


void release_w(volatile struct Info_FIFO* ptrFifo)
{
	// N/A
}

void *acquire_r(volatile struct Info_FIFO* ptrFifo)
{
	// N/A
	return NULL;
}

void release_r(volatile struct Info_FIFO* ptrFifo)
{
	// N/A
}


int writeFifo(volatile struct Info_FIFO* ptrFifo, void* data, int bytes)
{
	int ret;
	int k;

	if( ptrFifo && ptrFifo->active )
	{
		// Immutable FIFO variables, look them up once
		const uint32_t token_size = ptrFifo->token_size;
		const uint32_t fifo_size = ptrFifo->fifo_size;
		void * fifo_data = (void*)ptrFifo->data;

		// Sanity checks
		if( bytes > ( token_size * fifo_size ) )
		{
			// ERROR: Too much data!
			return EXIT_PARAM_ER;
		}
		
		// Compute #tokens to write, round up to nearest integer
		const int n = ( bytes + (token_size - 1) ) / token_size;

		// Process one token in chunk at a time
		for( k = 0; k < n; ++k )
		{
			ret = EXIT_ERROR;

			// Retrieve current token
			const void * token = ((void*)data) + k * token_size;

			while( ret == EXIT_ERROR )
			{
				uint32_t writec = ptrFifo->writec;
				const uint32_t readc  = ptrFifo->readc;	// must not be changed by producer

				// Ensure none of the loads and stores below are done before reading out the indices
				__sync_synchronize();
			
				// Check for mutual exclusion / overrun
				if( abs( (int)writec - (int)readc ) > fifo_size )
				{
					// ERROR: Mutual exclusion violation!
					//printf( "!!! VIOLATION !!!\n" );

					//assert(FALSE);

					return EXIT_FIFO_FULL;
				}
			
				if( ((int)fifo_size - ((int)writec - (int)readc)) > 0 || writec == readc )
				{
					// Write current token into FIFO
					memcpy( fifo_data + ( ( writec % fifo_size ) * token_size ), token, token_size );

					// Ensure data is written into the FIFO right now
					__sync_synchronize();

					// Increment write index
					ptrFifo->writec += 1;

					// Ensure the write index is updated right now
					__sync_synchronize();

					ret = EXIT_OK;
				}
			}
		}
	}
	else
	{
		// ERROR: Bad parameters!
		ret = EXIT_PARAM_ER;
	}
	return ret;
}

int readFifo(volatile struct Info_FIFO* ptrFifo, int* data, int bytes)
{
	int ret;
	int k;
	
	if( ptrFifo && ptrFifo->active )
	{
		// Immutable FIFO variables, look them up once
		const uint32_t token_size = ptrFifo->token_size;
		const uint32_t fifo_size = ptrFifo->fifo_size;
		const void * fifo_data = (const void*)ptrFifo->data;

		if( bytes > ( token_size * fifo_size ) )
		{
			// ERROR: Too much data!
			return EXIT_PARAM_ER;
		}
		
		// Compute #tokens to read, round up to nearest integer
		const int n = ( bytes + (token_size - 1) ) / token_size;

		// Process one token in chunk at a time
		for( k = 0; k < n; ++k )
		{
			ret = EXIT_ERROR;

			// Retrieve current token destination
			void * token = ((void*)data) + k * token_size;

			while( ret == EXIT_ERROR )
			{
				uint32_t readc  = ptrFifo->readc;
				const uint32_t writec = ptrFifo->writec;	// must not be changed by consumer
			
				// Ensure none of the loads and stores below are done before reading out the indices
				__sync_synchronize();
			
				// Check for mutual exclusion / overrun
				if( abs( (int)readc - (int)writec ) > fifo_size )
				{
					// ERROR: Mutual exclusion violation!
					//printf( "!!! VIOLATION !!!\n" );

					//assert(FALSE);

					return EXIT_FIFO_EMPTY;
				}					
			
				// Check if there are elements to read
				if( (((int)writec - (int)readc) > 0) )
				{
					// Read token from FIFO into current token destination
					memcpy( token, fifo_data + ( ( readc % fifo_size ) * token_size ), token_size );

					// Ensure data from the FIFO is loaded right now
					__sync_synchronize();

					// Increment read index
					ptrFifo->readc += 1;

					// Ensure the read index is updated right now
					__sync_synchronize();

					ret = EXIT_OK;
				}
			}
		}
	}
	else
	{
		// ERROR: Bad parameters!
		ret = EXIT_PARAM_ER;
	}
	return ret;
}

int maximum( int *ptr, int num )
{
  /* According to Gadi Taubenfeld -
     Synchronization Algorithms and Concurrent Programming
     This implementation of max is correct for Bakery algorithm */
  int max = 0, i, current;
  for ( i = 0; i < num; i++ )
  {
    current = ptr[i];
    if ( max < current )
      max = current;
  }

  return max;
}

int maximum_color( int *ptr, uint32_t col, int num )
{
  int max = 0, i, current;
  uint32_t col2;
  for ( i = 0; i < num; i++ )
  {
    current = ptr[i];
    col2 = lockNumber[i];
    if ( max < current && col == col2 )
      max = current;
  }
  
  return max;
}

/***************************************
* lock (based on Bakery)
* inputs:
*       * lockid: to identify between different locks
*       * id: to identify between competing tasks for a lock
*       * num: maximum number of competing tasks for this lock
*
* CS is entered after the function returns.
***************************************/
void lock(int lockid, int id, int num)
{
	int i;
	
	lockBusy[lockid] = TRUE;
	lockColors[lockid] = color;
	lockNumber[lockid] = 1 + maximum_color(lockNumber, lockColors[lockid], num);
	lockBusy[lockid] = FALSE;
	
	for( i = 0; i < NUM_FIFOS; i++ )
	{
		// Busy wait
		while( lockBusy[i] == TRUE )
		{}
		
		if( lockColors[i] == lockColors[lockid] )
		{
			// Busy wait
			while(	lockNumber[i] != 0 && ((lockNumber[i] < lockNumber[lockid]) ||
					((lockNumber[i] == lockNumber[lockid]) && i < lockid)) && lockColors[i] == lockColors[lockid])
			{}
		}
		else
		{
			// Busy wait
			while( lockNumber[i] != 0 && lockColors[lockid] == color && lockColors[i] != lockColors[lockid] )
			{}
		}
	}
}

/***************************************
* unlock
* inputs:
*       * lockid: to identify between different locks
*       * id: to identify between competing tasks for a lock
*       * num: maximum number of competing tasks for this lock
*
* Another CS(i) can be entered after the function returns.
***************************************/
void unlock(int lockid, int id, int num)
{
	// Switch colors and reset
	color = (lockColors[lockid] == 0) ? 1 : 0;
	lockNumber[lockid] = 0;
}


void initSemaphores(int numberoflocks)
{

}
