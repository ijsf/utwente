#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <kernel.h>
#include <kernel2.h>
#include <kernelConst.h>
#include <api.h>
#include <InfoFIFO.h>
#include <fcntl.h>
#include <unistd.h>

#define RESULT_OK  1
#define RESULT_ERR 0

uint32_t *shmaddr;
uint32_t *shmaddr_flag;

// CPU0: Simple producer
void task1()
{
	int j, n = 100;
	volatile struct Info_FIFO *ififo;
	int result = 0;
	
	ififo = openFifo( shmaddr, 1, 0 );
	
	fprintf( stdout, "==task1 writing %u elements to the CB\n", n );
	for( j = 0; j < n; j++ )
	{
		// write 32-bit integer of incrementing value
		result = writeFifo( ififo, &j, sizeof( int ) );
		
//		fprintf( stdout, "==task1 writeFifo(value: %u, size: %d)=%d\n", j, sizeof( int ), result );
	}

	// Idle wait
	fprintf( stdout, "==task1 idling\n" );
	for( j = 0; j < 10000000; ++j ) { Yield(); }
}

// CPU1: Simple consumer
void task2()
{
	int j;
	int data;
	int result = 0;
	volatile struct Info_FIFO *ififo;
	
	fprintf( stdout, "task 2 shmaddr %p data %x\n", shmaddr, *shmaddr );
	
	ififo = openFifo( shmaddr, 1, 0 );
	for( j = 0; j < 20; j++ )
	{
		// read from fifo
		result = readFifo( ififo, &data, sizeof( int ) );
		fprintf( stdout, "==task2 read()={value: %d, status: %d}\n", data, result );
		Yield();
	}

	// Idle wait
	fprintf( stdout, "==task2 idling\n" );
	for( j = 0; j < 10000000; ++j ) { Yield(); }
}

// CPU0: Idling task
void task3()
{
	int i, j;
	
	for( j = 0; j < 10000000; ++j )
	{
		for( i = 0; i < 100; ++i ) {}
		
//		fprintf( stdout, "task3 %d\n", j );
	}
}

// CPU1: Simple consumer
void task4()
{
	int j;
	int data;
	int result = 0;
	volatile struct Info_FIFO *ififo;
	
	fprintf( stdout, "task 4 shmaddr %p data %x\n", shmaddr, *shmaddr );
	
	ififo = openFifo( shmaddr, 1, 0 );
	for( j = 0; j < 20; j++ )
	{
		// read from fifo
		result = readFifo( ififo, &data, sizeof( int ) );
		fprintf( stdout, "==task4 read()={value: %d, status: %d}\n", data, result );
		Yield();
	}

	// Idle wait
	fprintf( stdout, "==task4 idling\n" );
	for( j = 0; j < 10000000; ++j ) { Yield(); }
}

int main()
{
	uint32_t taskId2, taskAddr2, sliceLength2, nArgs2;
	int		 result, i;
	
	*shmaddr_flag = 0;

	while( *shmaddr_flag == 0 );
	fprintf( stdout, "[main]: Flag has been set.\n" );
			 
	shmaddr = FIFO_ADMINISTRATION;
	initFifoInfo( shmaddr );
	
	fprintf( stdout, "[main]: shmaddr = %p\n", shmaddr );
	
	result = createFifo( shmaddr, sizeof( int ), 16, 1 );
	
	if( result == NOT_DEFINED )
	{
		fprintf( stdout, "[main]: Could not create FIFO.\n" );
		return EXIT_FAILURE;
	}
	
	InitTaskInfo( shmaddr );
	
	fprintf( stdout, "[main]: createNewTask task1\n" );
	result = CreateNewTask( 1, ( uint32_t )&task1, 100000, 0 );
	
	if( result == RESULT_ERR )
	{
		fprintf( stdout, "[main]: Could not create task1!\n" );
		return EXIT_FAILURE;
	}
	
	TaskStart( 1 );
	
	fprintf( stdout, "[main]: createNewTask task3\n" );
	result = CreateNewTask( 2, ( uint32_t )&task3, 100000, 0 );
	
	if( result == RESULT_ERR )
	{
		fprintf( stdout, "Could not create task2\n" );
		return EXIT_FAILURE;
	}
	
	TaskStart( 2 );

	InitKernel();
	
	for( i = 0; i < 1000000; ++i ) {}
	
	return EXIT_SUCCESS;
}

int main2()
{
	uint32_t taskId1, taskAddr1, sliceLength1, nArgs1;
	int		 result, i;
	
	shmaddr_flag  = FIFO_ADMINISTRATION + 0x10000;
	*shmaddr_flag = 0;
	
	// Wait loop
	for( i = 0; i < 10; ++i )
	{
//		fprintf( stdout, "[main2]: wait %d\n", i );
	}
	
	*shmaddr_flag = 1;
	
	fprintf( stdout, "[main2]: shmaddr = %p\n", shmaddr );
	
	for( i = 0; i < 10000; ++i ) {}		 
	
	InitTaskInfo2( shmaddr );
	
	fprintf( stdout, "[main]: createNewTask task2\n" );
	result = CreateNewTask2( 1, ( uint32_t )&task2, 10000000, 0 );
	
	if( result == RESULT_ERR )
	{
		fprintf( stdout, "Could not create task1!\n" );
		return EXIT_FAILURE;
	}
	
	fprintf( stdout, "[main]: createNewTask task4\n" );
	result = CreateNewTask2( 2, ( uint32_t )&task4, 10000000, 0 );
	
	if( result == RESULT_ERR )
	{
		fprintf( stdout, "[main2]: Could not create task2\n" );
		return EXIT_FAILURE;
	}
	
	TaskStart2( 1 );
	
	TaskStart2( 2 );

	InitKernel2();
	
	for( i = 0; i < 1000000; ++i ) {}
	
	return EXIT_SUCCESS;
}


