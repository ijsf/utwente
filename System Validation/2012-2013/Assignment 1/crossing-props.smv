--
-- System Validation 2012-1A - Homework Part 1
--
-- C.E. Etheredge (s0150207)
-- O. Meteer (s0140147)
--

  VAR
    check_S1 : CheckSignal(S1_red,S1_green,FALSE);
    check_S2 : CheckSignal(S2_red,S2_green,T2_occupied|T3_occupied|!B1_closed|!B2_closed);
    check_S3 : CheckSignal(S3_red,S3_green,T2_occupied|T1_occupied|!B1_closed|!B2_closed);
    check_S4 : CheckSignal(S4_red,S4_green,FALSE);
	
    -- check for identical behaviour of B1 and B2
	check_Brequest : CheckEqualityOfDevices(B1_request,B2_request);
	
	-- check for identical behaviour of W1 and W2
	check_Wactive : CheckEqualityOfDevices(W1_active,W2_active);
	
	-- check warning devices and barriers
	check_WB1 : CheckWarningBarrier(B1_open,B1_closed,B1_request,W1_active);
	check_WB2 : CheckWarningBarrier(B2_open,B2_closed,B2_request,W2_active);

	-- check cars
	check_cars : CheckCars(cars_crossing,B1_open|B2_open,B1_closed|B2_closed);
	
	-- check trains
	check_trains : CheckTrains(T1_occupied,T2_occupied,T3_occupied,cars_crossing);

-- Check properties of a signal.
-- red    : signal is red.
-- green  : signal is green.
-- unsafe : if TRUE then it is unsafe to pass this signal.
MODULE CheckSignal(red,green,unsafe)

-- safety: whenever the situation is unsafe, the signal must show red
LTLSPEC
  G (unsafe -> red)

-- safety: simultaneous red and green signals should never occur
LTLSPEC
  G !(red & green)

-- safety: there should always be a signal (red or green)
LTLSPEC
  G (red | green)

-- Check always-equal behaviour of identical devices
-- a : property of device A
-- b : property of device B
MODULE CheckEqualityOfDevices(a,b)

LTLSPEC
  G (a <-> b)

-- Check properties of warning device and associated barrier
-- Bopen : barrier is open
-- Bclosed : barrier is closed
-- Brequest : request to close barrier
-- Wactive : warning device is active
MODULE CheckWarningBarrier(Bopen,Bclosed,Brequest,Wactive)

-- safety: if barrier is closed, warning device must be active
LTLSPEC
  G (Bclosed -> Wactive)

-- liveness: if barrier close is requested, barrier must eventually close
LTLSPEC
  G (Brequest -> F Bclosed)

-- liveness: if barrier is closed, barrier should eventually open
--           e.g. cars can always make progress
LTLSPEC
  G (Bclosed -> F Bopen)

-- Check cars
-- cars: cars are crossing
-- Bopen: barriers are open
-- Bclosed: barriers are closed
MODULE CheckCars(cars,Bopen,Bclosed)

-- safety: cars can never cross if barriers are closed
LTLSPEC
  G( cars -> !Bclosed )

-- coverage: there is no point in time, after which cars are continuously visible
LTLSPEC
  !F( G cars )

-- coverage: there is no point in time, after which cars are continuously waiting
LTLSPEC
  !F( G !cars )

-- Check trains
-- assumption: trains cross both ways (T1 to T3 and T3 to T1)
-- T1_occupied: track 1 is occupied
-- T2_occupied: track 2 is occupied
-- T3_occupied: track 3 is occupied
-- cars: cars are crossing
MODULE CheckTrains(T1_occupied,T2_occupied,T3_occupied,cars)

-- liveness: if track 1 is occupied, track 3 must eventually be occupied - or vice versa
--           e.g. train should eventually move across tracks
LTLSPEC
  G ((T1_occupied -> F T3_occupied) | (T3_occupied -> F T1_occupied))

-- liveness: if track 2 is occupied, track 1 or 3 must eventually be occupied
--           e.g. train should not hang around on track 2
LTLSPEC
  G (T2_occupied -> F (T1_occupied | T3_occupied))

-- liveness/coverage: there is a situation where no trains are visible
LTLSPEC
  G F !(T1_occupied | T2_occupied | T3_occupied)

-- coverage: there is no point in time, after which a train is no longer crossing
LTLSPEC
  !F( G !T2_occupied )

-- safety: train can never cross if cars are crossing
LTLSPEC
  G !(T2_occupied & cars)
