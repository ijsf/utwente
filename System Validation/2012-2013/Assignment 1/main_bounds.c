#include "wrapped_fs.h"

typedef int dirHandle_t;
typedef int fileHandle_t;

// Function prototypes
int  nondet_int();
long nondet_long();
void *nondet_ptr();

int main( int argc, char *argv[] )
{
	// Used variables
	dirHandle_t  dir;
	fileHandle_t file;
	int			 parent;
	
	/*
	 *	Test case one: creating a directory
	 */
	// Initialize file system	
	init_fs();

	// Create a directory with a random parent
	parent = nondet_int();
	dir    = create_dir( parent, "Test" );	
	
	/*
	 *	Test case two: open a directory
	 */
	// Initialize file system
	init_fs();
	
	// Open a random directory in a random parent node
	parent = nondet_int();
	dir    = open_dir( parent, "RandomDirectoryName" );

	/*
	 *	Test case three: close an directory
	 */
	// Initialize file system
	init_fs();
	
	// Close a directory with a random parent node
	parent = nondet_int();
	dir    = close_dir( parent );
	
	/*
	 *	Test case four: creating a file
	 */
	// Initialize file system
	init_fs();
	
	// Create a file in a random parent directory node
	parent = nondet_int();
	file   = create_file( parent, "FileName" );
		
	/*
	 *	Test case five: opening file
	 */
	// Initialize file system
	init_fs();
	
	// Open a random file
	parent = nondet_int();
	file   = open_file( parent, "DoesNotExist" );
		
	/*
	 *	Test case six: reading a file
	 */
	// Initialize file system
	init_fs();
	
	// Read a random file
	long offset = nondet_long();
	int  length = nondet_int();
	void *buf   = nondet_ptr();
	parent = nondet_int();
	file   = read_file( parent, offset, length, buf );
	
	/*
	 *	Test case seven: writing a file
	 */
	// Initialize file system
	init_fs();
	
	// Write a random file
	offset = nondet_long();
	length = nondet_int();
	buf    = nondet_ptr();
	parent = nondet_int();
	file   = write_file( parent, offset, length, buf );
	
	/*
	 *	Test case eight: closing a file
	 */
	// Initialize file system
	init_fs();

	// Close a random file
	parent = nondet_int();
	file   = close_file( parent );

	return 0;
}
