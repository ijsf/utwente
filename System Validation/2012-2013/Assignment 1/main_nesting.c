#include "wrapped_fs.h"

typedef int dirHandle_t;
typedef int fileHandle_t;

// Function prototypes
int  nondet_int();
long nondet_long();
void *nondet_ptr();

int main( int argc, char *argv[] )
{
	// Used variables
	dirHandle_t  dir;
	fileHandle_t file;
	int			 parent;
	int			 result;
	long		 offset;
	int			 len;
	void		 *buffer;
	
	/*
	 *	Test case one: reading from a file that is not open
	 */
	// Initialize file system	
	init_fs();

	// Try to read the file without opening it.
	offset = 0;
	len	   = 1;
	buffer = 0;
	result = read_file( 0, offset, len, buffer );
	
	assert( result == ERR_NOT_OPEN );

	/*
	 *	Test case two: writing to a file that is not open
	 */
	// Initialize file system	
	init_fs();

	// Try to write to the file without opening it.
	offset = 0;
	len	   = 1;
	buffer = 0;
	result = write_file( 0, offset, len, buffer );
	
	assert( result == ERR_NOT_OPEN );
	
	/*
	 *	Test case three: reading from a file that is open
	 */
	// Initialize file system	
	init_fs();

	// First create a file
	file = create_file( 0, "test.txt" );
	__CPROVER_assume( file == 0 );
	
	// Open the file
	result = open_file( file, "test.txt" );
	__CPROVER_assume( result >= 0 );

	// Try to read the file.
	offset = 0;
	len	   = 1;
	buffer = 0;
	result = read_file( 0, offset, len, buffer );
	
	// File should be open.
	assert( result != ERR_NOT_OPEN );

	/*
	 *	Test case four: writing to a file that is open
	 */
	// Initialize file system	
	init_fs();
	
	// First create a file
	file = create_file( 0, "test.txt" );
	__CPROVER_assume( file == 0 );
	
	// Open the file
	result = open_file( file, "test.txt" );
	__CPROVER_assume( result >= 0 );

	// Try to write to the file.
	offset = 0;
	len	   = 1;
	buffer = 0;
	result = write_file( 0, offset, len, buffer );
	
	// File should be open.
	assert( result != ERR_NOT_OPEN );
	
	/*
	 *	Test case five: opening a file whose parent is not opened
	 */
	// Initialize file system	
	init_fs();
	
	// First create a directory in the root directory.
	dir = create_dir( 0, "test" );
	__CPROVER_assume( dir != ERR_EXISTS );
	
	// Should be 1 since this is the first directory
	// after the root directory.
	assert( dir == 1 );
	
	// Create a file in this new directory.
	file = create_file( dir, "test.txt" );
	__CPROVER_assume( file != ERR_EXISTS );
	
	// Should be 0 since this is the first file.
	assert( file == 0 );
	
	// Open a random file in a directory that is not open.
	result = open_file( dir, "test.txt" );
	__CPROVER_assume( result != ERR_NON_EXISTING );
	
	// File should not be opened, because parent directory
	// is not open.
	assert( result == ERR_NOT_OPEN );
	
	/*
	 *	Test case six: opening a file whose parent is open
	 */
	// Initialize file system	
	init_fs();
	
	// First create a directory in the root directory.
	dir = create_dir( 0, "test" );
	__CPROVER_assume( dir != ERR_EXISTS );
	
	// Should be 1 since this is the first directory
	// after the root directory.
	assert( dir == 1 );
	
	// Create a file in this new directory.
	file = create_file( dir, "test.txt" );
	__CPROVER_assume( file != ERR_EXISTS );
	
	// Should be 0 since this is the first file.
	assert( file == 0 );
	
	// Open the directory
	result = open_dir( 0, "test" );
	__CPROVER_assume( result != ERR_NON_EXISTING );
	
	// Should not return an error
	assert( result >= 0 );
	
	// Open a random file in a directory that is not open.
	result = open_file( dir, "test.txt" );
	__CPROVER_assume( result != ERR_NON_EXISTING );
	
	// File should be opened, because parent directory is open.
	assert( result != ERR_NOT_OPEN );
	
	/*
	 *	Test case seven: opening a directory whose ancestors are not open
	 */
	// Initialize file system	
	init_fs();
	
	// First create a directory in the root directory.
	dir = create_dir( 0, "A" );
	__CPROVER_assume( dir == 1 );
	
	// Create another directory inside director A.
	dirHandle_t dir2 = create_dir( dir, "B" );
	__CPROVER_assume( dir2 == 2 );
	
	// We used assumes when creating both directories,
	// because create_dir uses functions in fs.h which
	// are not implemented, meaning that create_dir
	// does not update directory status flags and
	// dir2 does not get the value 2 as it normally should.
	
	// Now open directory B without opening A.
	result = open_dir( dir2, "B" );
	__CPROVER_assume( result != ERR_NON_EXISTING );
	
	// Should return an error that the parent is not open.
	assert( result == ERR_NOT_OPEN );
	
	/*
	 *	Test case eight: opening a directory whose ancestors are open
	 */
	// Initialize file system	
	init_fs();
	
	// First create a directory in the root directory.
	dir = create_dir( 0, "A" );
	__CPROVER_assume( dir == 1 );
	
	// Create another directory inside director A.
	dirHandle_t dir2 = create_dir( dir, "B" );
	__CPROVER_assume( dir2 == 2 );
	
	// We used assumes when creating both directories,
	// because create_dir uses functions in fs.h which
	// are not implemented, meaning that create_dir
	// does not update directory status flags and
	// dir2 does not get the value 2 as it normally should.
	
	// Open directory A.
	result = open_dir( dir, "A" );
	__CPROVER_assume( result != ERR_NON_EXISTING );
	
	// Should not return an error since the root directory is open.
	assert( result != ERR_NOT_OPEN );
	
	// Open directory B.
	result = open_dir( dir2, "B" );
	__CPROVER_assume( result != ERR_NON_EXISTING );
	
	// Should not return an error since directory A is open.
	assert( result != ERR_NOT_OPEN );
			
	return 0;
}
