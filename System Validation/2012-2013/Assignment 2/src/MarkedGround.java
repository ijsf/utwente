/**
 * A piece of marked ground. 
 */
public class MarkedGround extends Ground
{
  /** @informal: this kind of crate is marked */
  //@ represents marked = true;

  MarkedGround (Position p) {
    super (p);
  }

  public boolean isMarked () {
    return true;
  }

  public String toString () {
    return "groundX(" + position.x + "," + position.y + ")";
  }
}
