/**
 * A wall piece on the board, untouchable obstacle. 
 */
final class Wall implements BoardItem
{
  //@ represents itemtype = Wall.class;

  /** @informal: the wall cannot be moved */
  //@ represents movable = false;

  /** @informal: we cannot stand on the wall */
  //@ represents steppedon = false;

  /** @informal: walls are not marked */
  //@ represents marked = false;

  /*@ spec_public @*/ final Position position;

  /** @informal: the constructed object has the given position. */
  //@ assignable position;
  //@ ensures position == p;
  Wall (Position p) {
    position = p;
  }

  public boolean isCanStepOn () {
    return false;
  }

  public boolean isMovable () {
    return false;
  }

  /** @informal: we return our position */
  //@ also
  //@ ensures \result == position;
  public /*@ pure @*/ Position position () {
    return position;
  }

  public void setPosition (Position newPosition)
      throws IllegalStateException {
    throw new IllegalStateException ();
  }

  public boolean isMarked () {
    return false;
  }

  public String toString () {
    return "wall(" + position.x + "," + position.y + ")";
  }

}
