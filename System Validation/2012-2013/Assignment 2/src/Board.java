/**
 * Represents the board of the game. The board is by default always square.
 */
final class Board {
  //@ public model instance int minxSize;
  //@ public model instance int minySize;

  //@ represents minxSize = 4;
  //@ represents minySize = 4;

  /*@ spec_public @*/ int xSize;
  /*@ spec_public @*/ int ySize;

  /** @informal: the game board actuall has some meaningful size */
  //@ public invariant (xSize >= minxSize) && (ySize >= minySize);

  /*@ spec_public @*/ BoardItem[][] items;

  /** @informal: The board contents has the right declared size
         and is completely filled with non null elements. Moreover,
         the items that are placed on the board have consistent position
         information -- their stored position is the one they occupy 
         on the board. */
  //@ public invariant items.length == xSize && (\forall int x; 0 <= x && x < xSize; items[x].length == ySize );
  //@ public invariant (\forall int x, y; 0 <= x && 0 <= y && x < xSize && y < ySize; items[x][y] != null && items[x][y].position().x == x && items[x][y].position().y == y);
  
  /** @informal: Based on the valid size input creates an empty board of
         that size. */
  //@ requires (_xSize >= minxSize) && (_ySize >= minySize); 
  //@ ensures (items.length == _xSize && (\forall int x; 0 <= x && x < xSize; items[x].length == _ySize ));
  Board (int _xSize, int _ySize) {
    this.xSize = _xSize;
    this.ySize = _ySize;
    items = new BoardItem[_xSize][_ySize];
    for (int x = 0; x < _xSize; x++) {
      for (int y = 0; y < _ySize; y++) {
	    //@ assert (x < xSize) && (y < ySize);
        items[x][y] = new Ground (new Position (x, y));
      }
    }
  }

  /** Used to build some meaningful game board. */
  /** @informal: only items with predeclared correct position
        information are allowed to be put on the board. */
  //@ requires (item.position().x < xSize) && (item.position().y < ySize);
  //@ ensures items[item.position().x][item.position().y] == item;
  void putItemOnBoard (BoardItem item) {
    items[item.position().x][item.position().y] = item;
  }

}
