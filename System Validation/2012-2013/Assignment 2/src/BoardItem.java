/**
 * Represents an item on the board that occupies one cell. Can
 * be either a piece of the wall (unmovable, cannot be stepped on),
 * a crate (movable, cannot be stepped on), ground (unmovable, can be stepped on).
 * Additionally each crate or ground can be marked. 
 */
public interface BoardItem
{
  //@ public model instance Class itemtype;
  //@ public model instance boolean movable, steppedon, marked;

  //@ ensures \result == movable;
  /*@ pure @*/ /*@ spec_public @*/ boolean isMovable ();

  //@ ensures \result == steppedon;
  /*@ pure @*/ /*@ spec_public @*/ boolean isCanStepOn ();

  //@ ensures \result == marked;
  /*@ pure @*/ /*@ spec_public @*/ boolean isMarked ();

  /** @informal: We cannot move ground  */
  //@ invariant (itemtype == Ground.class) ==> !movable;

  /** @informal: Something that can be moved cannot be stepped on */
  //@ invariant movable ==> !steppedon;
  
  /** @informal: Only ground and crates can be marked */
  //@ invariant marked ==> (itemtype == Ground.class || itemtype == Crate.class);
  
  public /*@ pure @*/ Position position ();

  /**
    * @informal: 
    *  - Only movable items can change their position. 
    *  - The position can be only changed by one increment in only
    *    one direction, class Position has a method to check that.
    *  - For all situations when the position cannot be changed
    *    an exception is thrown and the position indeed stays the 
    *    fixed.
    *  - For valid position changes the new position is in effect.
    */
  //@ normal_behaviour
  //@ requires movable == true;
  //@ requires position().isValidNextPosition( newPosition );
  //@ assignable \everything;
  //@ ensures position() == newPosition;
  //@ also
  //@ exceptional_behaviour
  //@ requires movable == false || !position().isValidNextPosition( newPosition );
  //@ assignable \nothing;
  //@ signals (IllegalStateException);
  void setPosition (Position newPosition)
      throws IllegalStateException;

}
