/**
 * A crate on the board that can be moved, that is not standing on a marked position. 
 */
public class Crate implements BoardItem
{
  //@ represents itemtype = Crate.class;

  /** @informal: crates can be moved */
  //@ represents movable = true;

  /** @informal: we cannot stand on crates */
  //@ represents steppedon = false;

  /** @informal: particular instances of a crate are marked, we are not */
  //@ represents marked = false;

  /*@ spec_public @*/ Position position;

  /** @informal: the constructed object has the given position. */
  //@ assignable position;
  //@ ensures position == p;
  Crate (Position p) {
    position = p;
  }

  public boolean isCanStepOn () {
    return false;
  }

  public boolean isMovable () {
    return true;
  }

  /** @informal: we return our position */
  //@ also
  //@ ensures \result == position;
   public /*@ pure @*/ Position position () {
    return position;
  }

  public void setPosition (Position newPosition)
      throws IllegalStateException {
    if (position.isValidNextPosition (newPosition)) {
      position = newPosition;
    } else {
      throw new IllegalStateException ();
    }
  }

  public boolean isMarked () {
    return false;
  }

  public String toString () {
    return "crate(" + position.x + "," + position.y + ")";
  }

}
