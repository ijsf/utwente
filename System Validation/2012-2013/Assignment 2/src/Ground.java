/**
 * A piece of unmarked ground. 
 */
class Ground implements BoardItem
{
  //@ represents itemtype = Ground.class;

  /** @informal: we cannot move ground */
  //@ represents movable = false;

  /** @informal: we can stand on the ground */
  //@ represents steppedon = true;

  /** @informal: particular instances of a ground are marked, we are not */
  //@ represents marked = false;

  /*@ spec_public @*/ final Position position;

  /** @informal: the constructed object has the given position. */
  //@ assignable position;
  //@ ensures position == p;
  Ground (Position p) {
    position = p;
  }

  public boolean isCanStepOn () {
    return true;
  }

  public boolean isMovable () {
    return false;
  }

  public boolean isMarked () {
    return false;
  }

  /** @informal: we return our position */
  //@ also
  //@ ensures \result == position;
  public /*@ pure @*/ Position position () {
    return position;
  }

  public void setPosition (Position newPosition)
      throws IllegalStateException {
    throw new IllegalStateException ();
  }

  public String toString () {
    return "ground(" + position.x + "," + position.y + ")";
  }

}
