function [ disparity ] = postprocess( im, D, liv, w, T, alfa )
%POSTPROCESS Summary of this function goes here
%   Detailed explanation goes here

DM = medfilt2( D, [5 5] );

[yDim, xDim] = size( DM );
xDimR = xDim - w - 1;
yDimR = yDim - w - 1;

disparity = zeros( yDim, xDim );
Tp = zeros( yDim, xDim );

T_2 = 0.5 * T;
T3_4 = ( 3 * T ) / 4;

for i = 1:xDim
    for j = 1:yDim
        value = liv( j, i );
        if( value >= 0 && value < T_2 )
            Tp( j, i ) = T_2;
        elseif( value >= T_2 && value < T3_4 )
            Tp( j, i ) = T3_4;
        elseif( value >= T3_4 )
            Tp( j, i ) = T;
        end            
    end
end

u = [];
ur = [];
r = [];
dr = [];
d = [];
dl = [];
l = [];
ul = [];

for i = w+1:xDimR
    t = cputime;
    
    for j = w+1:yDimR
        intensity = im( j, i );
        allValues = [];
        
        if( j > (w+1) )
            u = im( w+1:j-1, i );
            allValues = [allValues; u];
        end
        
        if( j < (yDimR-1) )
            d = im( j+1:yDimR, i );
            allValues = [allValues; d];
        end
        
        if( i > (w+1) )
            l = im( j, w+1:i-1 );
            allValues = [allValues; l'];
        end
        
        if( i < (xDimR-1) )
            r = im( j, i+1:xDimR );
            allValues = [allValues; r'];
        end
        
        dr = diag( im( ( j + 1 ):end,  ( i + 1 ):end ) );
        dl = diag( im( ( j + 1 ):end,  ( i - 1 ):-1:1 ) );
        ur = diag( im( ( j - 1 ):-1:1, ( i + 1 ):end ) );
        ul = diag( im( ( j - 1 ):-1:1, ( i - 1 ):-1:1 ) );
        
        allValues = [allValues; ur; dr; dl; ul];
        minValue = min( allValues );
        maxValue = uint16( max( allValues ) ) + 1;
        H = zeros( 1, maxValue );
        
        numElements = size( allValues, 1 );
        for k = 1:numElements
            if( abs( allValues( k ) - intensity ) < Tp( j, i ) )
                H( allValues( k ) + 1 ) = H( allValues( k ) + 1 ) + 1;
            end
        end
        
        h = zeros( 1, maxValue );
        sumH = sum( H );
        for k = 1:maxValue
            h( k ) = H( k ) / sumH;
        end
        
        [dhMax dhIdx] = max( h );
        
        if( abs( dhIdx - D( j, i ) ) > 1 && h( dhIdx ) > alfa )
            disparity( j, i ) = dhIdx;
        else
            disparity( j, i ) = D( j, i );
        end
    end
    
    disp( strcat( num2str( cputime - t ), 's' ) );
    disp( strcat( 'Processed:', num2str( i - w + 1 ), '/', num2str( xDimR - w+1 ) ) );
end



end

