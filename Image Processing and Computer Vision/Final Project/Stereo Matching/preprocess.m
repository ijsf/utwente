function [ processed_image ] = preprocess( im_name )
%PREPROCESS Summary of this function goes here
%   Detailed explanation goes here

if( ~ischar( im_name ) )
    error( 'Parameter "im_name" should be a string.' );
end

% Load image.
image = imread( im_name );
imageGray = rgb2gray( image );

% Get the size and number of color channels of the image.
[yDim, xDim] = size( imageGray );

% Prepare the output.
processed_image = zeros( yDim, xDim, 1 );

% Precalculate resize factor.
resizeFactor = 5 + ( 2 / 3 );

% Preprocess the images.
for x = 2:xDim-1
    for y = 2:yDim-1
        neighbors = imageGray( y-1:y+1, x-1:x+1 );
        nResized  = imresize( neighbors, resizeFactor, 'bicubic' );
        
        % Calculate the median and mean.
        vX = nResized( 9, 2:16 );
        vY = transpose( nResized( 2:16, 9 ) );
        v = [vX,vY];
        vMed  = median( single( v ) );
        vMean = mean( single( v ) );

        if( vMed > vMean )
            processed_image( y, x ) = max( v ); 
        else
            processed_image( y, x ) = min( v ); 
        end
    end
end

% Use the border pixels of the original grayscale image.
processed_image( 1, : ) = imageGray( 1, : );
processed_image( :, 1 ) = imageGray( :, 1 );
processed_image( yDim, : ) = imageGray( yDim, : );
processed_image( :, xDim ) = imageGray( :, xDim );

end    % function preprocess

