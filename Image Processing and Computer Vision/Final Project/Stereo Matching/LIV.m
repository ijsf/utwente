function [ liv ] = LIV( processed_image )
%LIV Summary of this function goes here
%   Detailed explanation goes here

% Get the size and number of color channels of the image.
[yDim, xDim] = size( processed_image );

% Prepare the output.
liv = zeros( yDim, xDim, 1 );

% Precalculate resize factor.
resizeFactor = 1 + ( 2 / 3 );

% Preprocess the images.
for x = 2:xDim-1
    for y = 2:yDim-1
        neighbors = processed_image( y-1:y+1, x-1:x+1 );
        nResized  = imresize( neighbors, resizeFactor, 'bicubic' );
        
        % Calculate the median and mean.
        H = abs( nResized( 3, 2 ) - nResized( 3, 4 ) );
        V = abs( nResized( 2, 3 ) - nResized( 4, 3 ) );
        Mt = max( [V, H] );
        
        liv( y, x ) = Mt; 
    end
end

end