#include "cuda.h"
#include "stdio.h"
#include "cudaFunctions.cuh"

int main( int argc, char **argv )
{
	int c;
	int *dev_c;
	
	AllocateArrayOnDevice( ( void ** )&dev_c, sizeof( int ) );
	
	Add( 2, 7, dev_c );
	
	CopyArrayToHost( &c, dev_c, sizeof( int ) );
	
	printf( "2 + 7 = %d\n", c );
	FreeArrayOnDevice( dev_c );
	
	return 0;
}
