extern "C"
{
	// Helper functions
	void AllocateArrayOnDevice( void **devicePtr, int size );
	void FreeArrayOnDevice( void *devicePtr );
	void CopyArrayToDevice( void *hostPtr, void *devicePtr, int size );
	void CopyArrayToHost( void *hostPtr, void *devicePtr, int size );
	
	// CUDA kernels
	void Add( int a, int b, int *c );
}
