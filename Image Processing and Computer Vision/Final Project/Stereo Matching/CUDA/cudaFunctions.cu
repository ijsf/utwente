#include "cudaKernels.cu"
#include "stdio.h"

extern "C"
{
#define err( ans ) { cudaAssert( ( ans ), __FILE__, __LINE__ ); }

	inline void cudaAssert( cudaError_t function, char *file, int line, bool abort = true )
	{
		if( function != cudaSuccess )
		{
			fprintf( stderr, "CUDA assert: %s %s %d\n", cudaGetErrorString( function ), file, line );
			if( abort )
				exit( function );
		}
	}

	void AllocateArrayOnDevice( void **devicePtr, int size )
	{
		if( size > 0 )
		{
			err( cudaMalloc( devicePtr, size ) );
		}
	}

	void FreeArrayOnDevice( void *devicePtr )
	{
		if( devicePtr )
		{
			err( cudaFree( devicePtr ) );
		}
	}
	
	void CopyArrayToDevice( void *hostPtr, void *devicePtr, int size )
	{
		err( cudaMemcpy( devicePtr, hostPtr, size, cudaMemcpyHostToDevice ) );
	}
	
	void CopyArrayToHost( void *hostPtr, void *devicePtr, int size )
	{
		err( cudaMemcpy( hostPtr, devicePtr, size, cudaMemcpyDeviceToHost ) );
	}
	
	void Add( int a, int b, int *c )
	{
		AddKernel<<< 1, 1 >>>( a, b, c );
		err( cudaPeekAtLastError() );
	}
}
