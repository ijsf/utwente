function [ Td ] = dynamicThreshold( liv, T )
%DYNAMICTHRESHOLD Summary of this function goes here
%   Detailed explanation goes here

% Get the size and number of color channels of the image.
[yDim, xDim] = size( liv );

% Prepare the output.
Td = zeros( yDim, xDim, 1 );

% Precalculate resize factor.
resizeFactor = 1 + ( 2 / 3 );

% Precalculate the ranges.
T_2 = T / 2;
T3_4 = ( 3 * T ) / 4;
T_4 = T / 4;
T2 = 2 * T;

% Preprocess the images.
for x = 2:xDim-1
    for y = 2:yDim-1
        input = liv( y, x );
        value = 0;
        
        if( input >= 0 && input < T_4 )
            value = T_2;
        elseif( input >= T_4 && input < T_2 )
            value = T3_4;
        elseif( input >= T_2 && input < T )
            value = T;
        elseif( input >= T )
            value = T2;
        end
        
        Td( y, x ) = value; 
    end
end

end