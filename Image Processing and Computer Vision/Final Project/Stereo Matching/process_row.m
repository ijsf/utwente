function disparities_row = process_row(imleft,imright,y,wx,wy)
% processing a row
% y:    row index
% wx:   2*wx+1 is window size along columns
% wy:   2*wy+1 is window size along rows

Wx = 2*wx+1;          % size of the window in x direction
Wy = 2*wy+1;          % size of the window in y direction

ncol = size(imleft,2);
ncolR = size(imright,2);


%% initialize the right image data buffer
% First stratify the rgb-data in the y-direction forming a 3*Wy dimensional vector for each column
% Then, collect each local neighbourhood as a 3*Wx*Wy vector in localright
databuffer = reshape(double(permute(imright(y-wy:y+wy,1:ncolR,:),[1 3 2])),3*Wy,ncolR);
localright = zeros(3*Wy*Wx,ncolR-Wx+1);
for j = 1:Wx
    localright((j-1)*3*Wy+1:j*3*Wy,:) = databuffer(:,j:ncolR-Wx+j);
end

%% initialize the left image data buffer
% First stratify the rgb-data in the y-direction forming a 3*Wy dimensional vector for each column
% Then, collect each local neighbourhood as a 3*Wx*Wy vector in localleft
databuffer = reshape(double(permute(imleft(y-wy:y+wy,1:ncol,:),[1 3 2])),3*Wy,ncol);
localleft = zeros(3*Wy*Wx,ncol-Wx+1);
for j = 1:Wx
    localleft((j-1)*3*Wy+1:j*3*Wy,:) = databuffer(:,j:ncol-Wx+j);
end

%% calculate an distance matrix between each neighborhood in the left and right image
mleft = size(localleft,2);
mright = size(localright,2);

% NORMALIZED CROSS CORRELATION:
% normalize w.r.t. brightness:
localleft  = localleft  - ones(size(localleft ,1),1)*mean(localleft );
localright = localright - ones(size(localright,1),1)*mean(localright);
% cross correlation:
D = localright'*localleft/size(localleft,1);
% normalize w.r.t. contrast
stdleft  = (mean(localleft .^2 )).^0.5;
stdright = (mean(localright.^2 )).^0.5;
D = D./(stdright'*stdleft + eps);

% SSD IMPLEMENTATION:
% D = ones(mright,1)*sum(localleft.*localleft); 
% D = D + sum(localright.*localright,1)'*ones(1,mleft);
% D = D - 2 .* localright'*localleft;
% D = D/max(D(:));   % to normalize the error metric between 0 and 1


%% show it
figure(100); imshow((D),[]);                   colormap(hot); 


%% find the disparities of the row
disparities_row = zeros(size(disp_cost));

% put your code for finding the disparities here ...




