function opt_path = dyn_prog_row(imleft,imright,y,sx,wx,wy)
% dynamic programming of a row
% y:    row index
% sx:   search interval in imright relative to a point in imleft
% wx:   2*wx+1 is window size along columns
% wy:   2*wy+1 is window size along rows

Wx = 2*wx+1;          % size of the window in x direction
Wy = 2*wy+1;          % size of the window in y direction

ncol = size(imleft,2);
ncolR = size(imright,2);


%% initialize the right image data buffer
% First stratify the rgb-data in the y-direction forming a 3*Wy dimensional vector for each column
% Then, collect each local neighbourhood as a 3*Wx*Wy vector in localright
if ndims(imleft)==3
    databuffer = reshape(double(permute(imright(y-wy:y+wy,1:ncolR,:),[1 3 2])),3*Wy,ncolR);
    localright = zeros(3*Wy*Wx,ncolR-Wx+1);
    for j = 1:Wx
        localright((j-1)*3*Wy+1:j*3*Wy,:) = databuffer(:,j:ncolR-Wx+j);
    end
else
    databuffer = reshape(double(        imright(y-wy:y+wy,1:Wx-sx(1)+sx(2)  )         ),  Wy,Wx-sx(1)+sx(2));
%     localn = ... nog niet af
end

%% initialize the left image data buffer
% First stratify the rgb-data in the y-direction forming a 3*Wy dimensional vector for each column
% Then, collect each local neighbourhood as a 3*Wx*Wy vector in localleft
if ndims(imleft)==3
    databuffer = reshape(double(permute(imleft(y-wy:y+wy,1:ncol,:),[1 3 2])),3*Wy,ncol);
    localleft = zeros(3*Wy*Wx,ncol-Wx+1);
    for j = 1:Wx
        localleft((j-1)*3*Wy+1:j*3*Wy,:) = databuffer(:,j:ncol-Wx+j);
    end
else
    databuffer = reshape(double(        imright(y-wy:y+wy,1:Wx-sx(1)+sx(2)  )         ),  Wy,Wx-sx(1)+sx(2));
%     localn = ... nog niet af
end

%% calculate an distance matrix between each neighborhood in the left and right image
mleft = size(localleft,2);
mright = size(localright,2);

% NORMALIZED CROSS CORRELATION:
% normalize w.r.t. brightness:
localleft  = localleft  - ones(size(localleft ,1),1)*mean(localleft );
localright = localright - ones(size(localright,1),1)*mean(localright);
% cross correlation:
D = localright'*localleft/size(localleft,1);
% normalize w.r.t. contrast
stdleft  = (mean(localleft .^2 )).^0.5;
stdright = (mean(localright.^2 )).^0.5;
D = D./(stdright'*stdleft + eps);
% D = (1-D)/2;    % to convert it to an error measure between 0 and 1 
D(D<0) = 0; D=1-D;    % to convert it to an error measure between 0 and 1 

% SSD IMPLEMENTATION:
% D = ones(mright,1)*sum(localleft.*localleft); 
% D = D + sum(localright.*localright,1)'*ones(1,mleft);
% D = D - 2 .* localright'*localleft;
% D = D/max(D(:));   % to normalize the error metric between 0 and 1

%% relate the distance matrix to an error metric for disparities
disp_cost = ones(sx(2)-sx(1)+1,ncol-2*wx);
for i=1:ncol-2*wx
    cleft = i+wx;
    cright_min = max(cleft+sx(1),wx+1);
    cright_max = min(cleft+sx(2),ncolR-wx);
    jmin = cright_min-wx;
    jmax = cright_max-wx;
    kmin = 1;
    if jmin==1, kmin = sx(2)-sx(1)+2-jmax; end
    kmax = sx(2)-sx(1)+1;
    if jmax==ncolR-2*wx, kmax = jmax-jmin+1; end
    disp_cost(kmin:kmax,i) = D(jmin:jmax,i);
end

%% show it
% figure(100); imshow((D),[]);                   colormap(hot);
figure(102); imshow((disp_cost),[]);   colormap(hot);
%% define the transition cost
disp_max = sx(2)-sx(1)+1;
transitioncost = ones(disp_max);
for i=-10:10
    transitioncost = transitioncost - diag(ones(disp_max-abs(i),1), i)*(10-abs(i))/10;
end
transitioncost = transitioncost.^2;

%% dynamic programming
minimalcost = zeros(size(disp_cost));
opt_pathes = zeros(size(disp_cost));
minimalcost(:,1) = disp_cost(:,1);

for i=2:size(minimalcost,2)
    for j=1:disp_max
        tot_cost = minimalcost(:,i-1) + disp_cost(j,i)*ones(disp_max,1) + 2.0*transitioncost(:,j); 
        [minimalcost(j,i), opt_pathes(j,i)] = min(tot_cost);
    end
end

% backtracing
opt_path = zeros(1,size(disp_cost,2));

[foo,opt_path(end)] = min(minimalcost(:,end));
for i=size(disp_cost,2)-1:-1:1
    opt_path(i) = opt_pathes(opt_path(i+1),i);
end
hold on; plot(opt_path,'g'); hold off;


