function [ D_lr, D_rl ] = disparityMap( im_left, im_right, Td_left, Td_right, w, sigma, T, Kp )
%DISPARITYMAP Summary of this function goes here
%   Detailed explanation goes here

sizeLX = size( im_left, 2 );
sizeLY = size( im_left, 1 );
sizeRX = size( im_right, 2 );
sizeRY = size( im_right, 1 );

sizeX = min( sizeLX, sizeRX );
sizeY = min( sizeLY, sizeRY );

D_lr = zeros( sizeLY, sizeLX );
D_rl = zeros( sizeRY, sizeRX );

rangeX = [w + 1, sizeX - w - 1];
rangeY = [w + 1, sizeY - w - 1];

% Left image as reference
for i = rangeX( 1 ):rangeX( 2 )
    t = cputime;
    for j = rangeY( 1 ):rangeY( 2 )
        D_lr( j, i ) = ALS( im_left, im_right, Td_left, Td_right, w, i, j, sigma, T, Kp );
    end
    disp( strcat( 'Left:', num2str( cputime - t ), 's' ) );
    disp( strcat( 'Processed:', num2str( i - rangeX( 1 ) + 1 ), '/', num2str( rangeX( 2 ) - rangeX( 1 ) ) ) );
end

% Right image as reference
for i = rangeX( 1 ):rangeX( 2 )
    t = cputime;
    for j = rangeY( 1 ):rangeY( 2 )
        D_rl( j, i ) = ALS( im_right, im_left, Td_right, Td_left, w, i, j, sigma, T, Kp );
    end
    disp( strcat( 'Right:', num2str( cputime - t ) ) );
    disp( strcat( 'Processed:', num2str( i - rangeX( 1 ) + 1 ), '/', num2str( rangeX( 2 ) - rangeX( 1 ) ) ) );
end

end

