%% dynamic programming

clear all
close all

imleft = imread('A_imleft.png');
imright = imread('B_imright.png');
figure; 
subplot('position',[ 0 0 0.5 1]); imshow(imleft);
subplot('position',[.5 0 0.5 1]); imshow(imright);

 max_disparity = 120;      % sixtijnse kapel
 min_disparity = -20;      % sixtijnse kapel
 offset_disparity = 392;   % sixtijnse kapel
%max_disparity = 60;
%min_disparity = -60;
%offset_disparity = 469;
D = 1044;
xc = -79;
yc = 633;
T = 4;

sx = [min_disparity max_disparity];
wx = 3; %15
wy = 3; %15
nrow = size(imleft,1);
ncol = size(imleft,2);
disparity = zeros(nrow-2*wy,ncol-2*wy);

tbegin=cputime;
for i=1+wy:nrow-wy
    t = cputime;
    opt_path = dyn_prog_row(imleft,imright,i,sx,wx,wy);
    trow = cputime-t;
    title(['row=' num2str(i) '  (max=' num2str(nrow-wy) ').    Processing time: ' ...
        num2str(round(trow*10)/10) ' s.   (still to go: ' num2str(round((t+trow-tbegin)*(nrow-wy-i)/(i-wy))) ' s)']); drawnow;
    disparity(i-wy,:) = -opt_path + min_disparity + offset_disparity;
    disp(i);
end

imleft = imleft(1+wy:nrow-wy,1+wx:ncol-wx,:);
x=2:size(disparity,2);
y=1:size(disparity,1);
[X,Y] = meshgrid(x,y);
Z = D*T./disparity;

figure;
nskip = 1;
[XI,YI] = meshgrid(min(x):nskip:max(x),min(y):nskip:max(y));
CI = double(imleft(YI(:,1),XI(1,:),:));
ZI=Z(min(y):nskip:max(y),min(x):nskip:max(x));
XI=(XI-xc).*ZI/D;
YI=(YI-yc).*ZI/D;

surf(XI,ZI,-YI,CI/255);
shading flat;
view(7.5,-20);
daspect([1 1 1]);
%axis([min(xc) max(xc) min(zc) max(zc) min(-yc) max(-yc)]);

nskip = 5;
[XI,YI] = meshgrid(min(x):nskip:max(x),min(y):nskip:max(y));
CI = double(imleft(YI(:,1),XI(1,:),:));
ZI=Z(min(y):nskip:max(y),min(x):nskip:max(x));
XI=(XI-xc).*ZI/D;
YI=(YI-yc).*ZI/D;
fig=figure;
set(fig,'position',[100 50 900 600]);
set(fig,'DoubleBuffer','on');
set(gca,'xlim',[-80 80],'ylim',[-80 80],...
       'NextPlot','replace','Visible','off');
mesh(XI,ZI,-YI); axis equal; colormap([0 0 0]);
view(16,8);
ax = axis;
[a,b]=view;
for i=1:1:360
    view(a+i,b);
    axis(ax);
    drawnow
%     f(i) = getframe(gcf);
end

