function [ d ] = ALS( im_left, im_right, Td_left, Td_right, w, x, y, sigma, T, Kp )
%ALS Summary of this function goes here
%   Detailed explanation goes here

%
% Step 1
%
W = ( 2 * w ) + 1;

Bl = zeros( W, W );
Br = zeros( W, W );

Td = Td_left( y, x );

wi_l = im_left( y - w:y + w, x - w:x + w );
wi_r = im_right( y - w:y + w, x - w:x + w );

cl = wi_l( w+1, w+1 );
cr = wi_r( w+1, w+1 );

for i = 1:W
    for j = 1:W
        if( abs( wi_l( j, i ) - cl ) < Td )
            Bl( j, i ) = 1;
        end
        
        if( abs( wi_r( j, i ) - cr ) < Td )
            Br( j, i ) = 1;
        end
    end
end

%
% Step 2
%
SE = strel( 'square', 3 );
Bl = imdilate( Bl, SE );
Br = imdilate( Br, SE );

%
% Step 3
%
%connL = bwlabel( Bl, 4 );
%connR = bwlabel( Br, 4 );

connL = bwlabel( Bl, 8 );
connR = bwlabel( Br, 8 );

cL = connL( w+1, w+1 );
cR = connR( w+1, w+1 );

for i = 1:W
    for j = 1:W
        if( ( Bl( j, i ) == 1 ) && ( connL( j, i ) ~= cL ) )
           Bl( j, i ) = 0; 
        end
        
        if( ( Br( j, i ) == 1 ) && ( connR( j, i ) ~= cR ) )
           Br( j, i ) = 0; 
        end
    end
end

%
% ALGORITHM 2 - Final binary map calculation
%
B = zeros( W, W );

for i = 1:W
    for j = 1:W
        if( ( Bl( j, i ) & Br( j, i ) ) == 1 )
            B( j, i ) = 1;
        end
    end
end

%
% ALGORITHM 3 - Offset neutralization
%
numberOfOnes = nnz( B );
zl = zeros( 1, numberOfOnes );
zr = zeros( 1, numberOfOnes );

index = 1;

for i = 1:W
    for j = 1:W
        if( B( j, i ) == 1 )
            zl( index ) = double( im_left( y - 1 + ( j - w ), x - 1 + ( i - w ) ) );
            zr( index ) = double( im_right( y - 1 + ( j - w ), x - 1 + ( i - w ) ) );
            
            index = index + 1;
        end
    end
end

cl = double( im_left( y, x ) );
cr = double( im_right( y, x ) );

for i = 1:numberOfOnes
    zl( i ) = zl( i ) - cl;
    zr( i ) = zr( i ) - cr;
end

% ALGORITHM 4 - Elimination of outliers
indices = [];

for i = 1:numberOfOnes
    if( abs( zl( i ) - zr( i ) ) >= T )
        indices = [indices i];
    end
end

zl( indices ) = [];
zr( indices ) = [];

% Determine [Dmin, Dmax]
Np = size( zl, 2 );
z = zeros( 1, Np );
Np_xy = [];

for i = 1:Np
   z( i ) = abs( zl( i ) - zr( i ) ); 
end

z = sort( z );

Dmin = z( 1 );
Dmax = z( Np );
range = Dmax - Dmin;

% Count how many times a certain disparity in the [Dmin, Dmax] range
% occurs.
for i = Dmin:Dmax
    Np_xy = [Np_xy sum( z == i )];
end

Np_xy = Np_xy( Np_xy ~= 0 );

z = unique( z );
Np = size( Np_xy, 2 );
Cn_xy = zeros( 1, Np );
RDC = zeros( 1, Np );

Ns = Kp * max( Np_xy );

for i = 1:Np
   Cn_xy( i ) = CnSSD( Np_xy( i ), z( i ), sigma );
   
   if( Np_xy( i ) > Ns )
       RDC( i ) = Cn_xy( i );
   end
end

RDC = RDC( RDC ~= 0 );

if( numel( RDC ) == 0 )
    d = 0;
else
    d = min( RDC );
end

function[ Cn_xy ] = CnSSD( Np, diff, variance )
    Cn_xy = ( 1 / Np ) * ( ( diff * diff ) / ( 4 * variance ) );
end

end