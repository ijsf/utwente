% Load the image.
image = imread( 'im_test.png' );

% Define the size of the gaussian filter.
fsize = [20 20];

% Fill array with sigma values.
sigmaArray = [1 3 5 9];

% Do four times.
for i = 1:4
    h = fspecial( 'gaussian', fsize, sigmaArray( i ) );  % Create the PSF.
    imfiltered = imfilter( image, h );                   % Apply the filter.
    subplot( 2, 2, i );                                  % Define a subplot.
    imshow( imfiltered, [] );                            % Show the image.
    title( ['sigma = ' num2str( sigmaArray( i ) ) ] );   % Include a plot title.
end

% Create a PSF matrix hx, which contains sampled derivate of 
% the gaussian function.
sigma = 5;
sigma2 = sigma^2;
sigma4 = sigma^4;
sigma6 = sigma^6;

x = linspace( -20, 20, 41 );
y = x';
[X,Y] = meshgrid( x, y );
hx = X.*exp( -( X.^2 + Y.^2 ) / ( 2 * sigma2 ) ) / ( 2 * pi * sigma4 );

% Display the PSF matrix.
figure; surf( x, y, hx ); title( ['hx'] );

% Create the OTF of the PSF hx.
OTF = psf2otf( hx, [size(image,1) size(image,2)] );
OTF = fftshift( OTF );

% Display the real and imaginary parts of the OTF.
figure;
%subplot( 1, 2, 1 ); 
mesh( real( OTF ) ); title( ['OTF: real'] );
figure;
%subplot( 1, 2, 2 ); 
mesh( imag( OTF ) ); title( ['OTF: imaginary'] );

% Apply the convolution with hx to the image.
im_hx = imfilter( double( image ), hx, 'replicate', 'conv' );
figure; imshow( im_hx );

% Apply the convolution with the other PSF's to the image.
im_hy  = ut_gauss( image, 5, 0, 1 );
im_hxx = ut_gauss( image, 5, 2, 0 );
im_hyy = ut_gauss( image, 5, 0, 2 );
im_hxy = ut_gauss( image, 5, 1, 1 );

% Display the filtered image after applying the convolution with
% the other derivatives.
figure;
subplot( 2, 2, 1 ); imshow( im_hy );  title( ['hy'] );
subplot( 2, 2, 2 ); imshow( im_hxx ); title( ['hxx'] );
subplot( 2, 2, 3 ); imshow( im_hyy ); title( ['hyy'] );
subplot( 2, 2, 4 ); imshow( im_hxy ); title( ['hxy'] );

% Calculate the gradient magnitude of the image.
im_grad = sqrt( im_hx.^2 + im_hy.^2 );

% Calculate the Laplacian of the image.
im_lap = im_hxx + im_hyy;

% Display the gradient magnitude and Laplacian of the image.
figure;
subplot( 1, 2, 1 ); imshow( im_grad ); title( ['Gradient magnitude'] );
subplot( 1, 2, 2 ); imshow( im_lap );  title( ['Laplacian'] );

% Create a binary image, where a value is 1 if
% the Laplacian of the image is positive.
im_bin = im_lap > 0.0;

% Determine boundary pixels.
im_bin = bwmorph( im_bin, 'remove' );

% Mask the gradient magnitude image by the Marr-Hildreth zero
% crossings image.
im_tmp = im_grad .* im_bin;

% Define a threshold T, and create an edge map where
% pixels are '1' if the gradient magnitude is larger than T.
T = 2.8; im_grad_edge = im_tmp > T;

% Display the result.
figure; imshow( im_grad_edge );

% Perform hysteresis thresholding.
T2 = 4; T3 = 2;
marker = im_tmp > T2;
mask = im_tmp > T3;
im_result = imreconstruct( marker, mask );

% Display the result.
figure; imshow( im_result );

% Use the ut_edge function and apply the Canny edge detector
% to the image.
im_canny = ut_edge( image, 'c', 's', 7, 'h', [0.015 0.005] );

% Display the result.
figure; imshow( im_canny );

% Determine the diameter of the vessel per column.
diam = zeros( 1, size( image, 2 ) );
middlerow = size( image, 1 ) / 2;

for col = 1:size( image, 2 )
    row = middlerow;
    edge = 0;
    
    % From the middle row upwards.
    while edge == 0
        edge = im_canny( row, col );
        if edge == 0
            diam( col ) = diam( col ) + 1;
            row = row - 1;
        end
    end
    
    % From the middle row + 1 downwards.
    row = middlerow + 1;
    edge = 0;
    
    while edge == 0
        edge = im_canny( row, col );
        if edge == 0
            diam( col ) = diam( col ) + 1;
            row = row + 1;
        end
    end
end

% Plot the diameter (in pixels) per column.
figure; plot( diam ); title( ['Diameter in pixels per column'] );

% Estimate the nominal diameter.
nom_diam_pix = mean( diam );
nom_diam = nom_diam_pix / ( 37.5 / 1.5 );