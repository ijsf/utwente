% Bloodvessel diameter in millimeters.
D = 1.5;

% Number of trials to be performed per sigma value.
numTrials = 100;

diameters   = zeros( 24, numTrials );
successRate = zeros( 24, 1 );
stdDev      = zeros( 24, 1 );

for sigma = 2:1:25
    for trial = 1:numTrials
        % Create a new image.
        image = im_bloodvessel( D );
        diam = 0;
        success = 0;
        % Perform diameter estimation.
        [diam, success] = getdiameters( image, sigma, 'm' );
        
        if success == 1
           diameters( sigma - 1, trial ) = diam;
        end
       
        successRate( sigma - 1 ) = successRate( sigma - 1 ) + success;
    end
    
    % Calculate success rate.
    successRate( sigma - 1 ) = successRate( sigma - 1 ) / numTrials;
end

% Calculate the standard deviation.
stdDev = std( diameters, 0, 2 );

% Calculate the unbiassed standard deviation.
% The value which the standard deviation is divided,
% Was calculated using the formula found in:
% http://en.wikipedia.org/wiki/Unbiased_estimation_of_standard_deviation
unbiassedStdDev = stdDev ./ 0.997477976071263510781;

% Calculate the bias. The unbiassed standard deviation is always
% larger than the unbiassed one, so subtract the biassed standard deviation
% from the unbiassed one.
bias = unbiassedStdDev - stdDev;