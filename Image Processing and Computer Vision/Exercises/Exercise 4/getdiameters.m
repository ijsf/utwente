function [diam, success] = getdiameters( im, sigma, op )
%getdiameters   Bloodvessel diameter estimation function
%
%   [diam, success] = getdiameters( im, sigma, op ) estimates the diameter
%   of a bloodvessel in millimeters for a given image. If the estimation
%   is successfull, 'success' will be 1 and 'diam' will contain the 
%   diameter. If the estimation is not successfull, then 'success' will be
%   0 and 'diam' will be empty.
%
%   [diam, success] = getdiameters( im, sigma, op ) uses the following
%   parameters:
%
%   im:     A gray-scale image of a bloodvessel.
%   sigma:  The standard deviation of the applied Gaussian filter.
%   op:     The type of edge filtering operation. There are two options:
%
%           'c':    Perform the Canny operation.
%           'm':    Perform the Marr-Hildreth operation.
%
try
    if ~ismatrix( im )
        error( 'Parameter "im" should be a matrix.' );
    end
    
    if ~isnumeric( sigma )
        error( 'Parameter "sigma" should be an integer.' );
    end

    if ~( strcmp( op, 'c' ) || strcmp( op, 'm' ) )
        error( 'Parameter "op" must be either "c" or "m"' );
    end

    % Use the ut_edge function and apply the Canny edge detector
    % to the image.
    im_canny = ut_edge( im, op, 's', sigma, 'h', [0.015 0.005] );

    % Display the result.
    %figure; imshow( im_canny );

    % Determine the diameter of the vessel per column.
    diameters = zeros( 1, size( im, 2 ) );
    middlerow = size( im, 1 ) / 2;

    for col = 1:size( im, 2 )
        row = middlerow;
        edge = 0;

        % From the middle row upwards.
        while edge == 0
            edge = im_canny( row, col );
            if edge == 0
                diameters( col ) = diameters( col ) + 1;
                row = row - 1;
            end
        end

        % If we didn't detect an edge, then the estimation has failed.
        if edge == 0
            success = 0;
            return;
        end

        % From the middle row + 1 downwards.
        row = middlerow + 1;
        edge = 0;

        while edge == 0
            edge = im_canny( row, col );
            if edge == 0
                diameters( col ) = diameters( col ) + 1;
                row = row + 1;
            end
        end

        % If we didn't detect an edge, then the estimation has failed.
        if edge == 0
            success = 0;
            return;
        end
    end

    % Plot the diameter (in pixels) per column.
    %figure; plot( diameters ); title( ['Diameter in pixels per column'] );

    % Estimate the nominal diameter.
    nom_diam_pix = mean( diameters );
    nom_diam = nom_diam_pix / ( 37.5 / 1.5 );
    diam = nom_diam;
    success = 1;
catch err
    diam = [];
    success = 0;
    %rethrow( err );
end