% Create a transformation structure based on the selected control points.
tform = cp2tform( input_points, base_points, 'projective' );

% Calculate the position of the transformed corner of the right image.
% This will be used to determine the final image size.
[X, Y] = tformfwd( tform, size( right, 2 ), size( right, 1 ) );

% Transform the right image and properly displace it,
% so that it is big enough to hold both images.
transformed = imtransform( right, tform, 'XData', [1 X], 'YData', [1 Y] );

% Copy left image over the tranformed right image.
transformed( 1:size( left, 1 ), 1:size( left, 2 ), 1:size( left, 3 ) ) = left;

% Display the result.
imshow( transformed, 'InitialMagnification', 100 );

% Write the result to a file.
imwrite( transformed, 'result.png', 'png' );