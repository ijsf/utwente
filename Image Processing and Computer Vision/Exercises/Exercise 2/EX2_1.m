% Load both images.
left = imread( 'schier_left.jpg' );
right = imread( 'schier_right.jpg' );

% Define control points in both images.
% Our base image is the left image.
[input_points, base_points] = cpselect( right, left, 'Wait', true );