% Read the png image into a matrix.
I = imread( 'car_dis.png' );

% Make sure that the values lie within [0..1].
I = double( I ) / 256;

% Display the image on screen.
imshow( I );

% Store the Fourier transform in a matrix.
F = fft2( I );

% Display the log-amplitude spectrum.
imshow( log( fftshift( abs( F ) ) + 1e+2 ), [] );

% Create a rectangular PSF of size 1x7.
h = fspecial( 'average', [7, 1] );
hf = fft2( h, size( F, 1 ), size( F, 2 ) );

% Apply the PSF to image.
I4 = ifft2( hf.*F );
I2 = imfilter( I, h );
imshow( I2 );

% Create the OTF of the PSF.
otf = psf2otf( h, [size( F, 1 ) size( F,2 )] );

% Display the magnitude of the OTF (MTF).
surf( abs( otf ) );

% Create a matrix H.
H = ones( size( F, 1 ), size( F, 2 ) );

% Mark the frequencies associated with the distortion.
% Based on the shifted FFT of the image.
H( 1:102, 122:134 ) = 0.0;

% Create a mirrored matrix.
H2 = flipdim( H, 1 );
H2 = flipdim( H2, 2 );

% Create the final matrix H.
H = H .* H2;

% Apply the filter to the image.
I3 = ifft2( H .* fftshift( F ) );

% Display the result.
imshow( real( I3 ) );

% Create a new matrix H.
H = ones( size( F, 1 ), size( F, 2 ) );

% Mark the frequencies associated with the distortion.
H( 37:39, 1:256 ) = 0.0;
H( 38:218, 1:3 ) = 0.0;

% Create a mirrored matrix.
H2 = flipdim( H, 1 );
H2 = flipdim( H2, 2 );

% Create the final matrix H.
H = H .* H2;

% Apply the filter to the image.
I4 = ifft2( H .* F );

% Display the result.
imshow( real( I4 ) );