/**
 * Source file for the OutputPWM model
 * Generated by the TERRA CSP2LUNA generator version 0.6.1
 *
 * protected region document description on begin
 * protected region document description end
 */

#include "include/OutputPWM.h"

// protected region additional headers on begin
// Each additional header should get a corresponding dependency in the Makefile
// protected region additional headers end

namespace MainModel { namespace OutputPWM { 



OutputPWM::OutputPWM(ChannelOut<int> *WritePWM_H, ChannelOut<int> *WritePWM_V) :
    Parallel(NULL)
{
  SETNAME(this, "OutputPWM");

  // Initialize channels
  mywriterTimestep2readerTimestepChannel = new UnbufferedChannel<int, One2In, Out2One>();

  // Initialize model objects
  myMyDrvPWM = new MyDrvPWM::MyDrvPWM(PWM_input_H, PWM_input_V);
  SETNAME(myMyDrvPWM, "MyDrvPWM");
  myPWM_read_H = new Reader<int>(&PWM_input_H, WritePWM_H);
  SETNAME(myPWM_read_H, "PWM_read_H");
  myPWM_read_V = new Reader<int>(&PWM_input_V, WritePWM_V);
  SETNAME(myPWM_read_V, "PWM_read_V");
  myreaderTimestep = new Reader<int>(&timestep, mywriterTimestep2readerTimestepChannel);
  SETNAME(myreaderTimestep, "readerTimestep");
  mywriterTimestep = new Writer<int>(&timestep, mywriterTimestep2readerTimestepChannel);
  SETNAME(mywriterTimestep, "writerTimestep");

  // Create SEQUENTIAL group
  mySEQUENTIAL = new Sequential(
    (CSPConstruct *) mywriterTimestep,
    (CSPConstruct *) myMyDrvPWM,
    (CSPConstruct *) myPWM_read_H,
    (CSPConstruct *) myPWM_read_V,
    NULL
  );
  SETNAME(mySEQUENTIAL, "SEQUENTIAL");




  // Register model objects
  this->append_child(myreaderTimestep);
  this->append_child(mySEQUENTIAL);

  // protected region constructor on begin
  // protected region constructor end
}

OutputPWM::~OutputPWM()
{
  // TODO Properly destroy all additional objects that got defined in the constructor

  // protected region destructor on begin
  // protected region destructor end

  // Destroy model groups
  delete mySEQUENTIAL;

  // Destroy model objects
  delete mywriterTimestep;
  delete myreaderTimestep;
  delete myPWM_read_V;
  delete myPWM_read_H;
  delete myMyDrvPWM;
}



// protected region additional functions on begin
// protected region additional functions end

// Close namespace(s)
} } 
