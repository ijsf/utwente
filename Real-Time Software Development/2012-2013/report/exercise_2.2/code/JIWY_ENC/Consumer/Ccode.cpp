/**
 * Source file for the Ccode model
 * Generated by the TERRA CSP2LUNA generator version 0.6.1
 *
 * protected region document description on begin
 * protected region document description end
 */

#include "include/Consumer/Ccode.h"

// protected region additional headers on begin
// Each additional header should get a corresponding dependency in the Makefile
// protected region additional headers end

namespace MainModel { namespace Consumer { namespace Ccode { 



Ccode::Ccode(int &var) :
    CodeBlock(), var(var)
{
  SETNAME(this, "Ccode");

  // protected region constructor on begin
  // protected region constructor end
}

Ccode::~Ccode()
{
  // TODO Properly destroy all additional objects that got defined in the constructor

  // protected region destructor on begin
  // protected region destructor end
}

void Ccode::execute()
{
  // protected region execute code on begin
	printf( "Consumer: %u\n", var );
  // protected region execute code end  
}



// protected region additional functions on begin
// protected region additional functions end

// Close namespace(s)
} } } 
