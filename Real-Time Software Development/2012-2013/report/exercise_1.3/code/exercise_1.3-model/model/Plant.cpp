/**
 * Source file for the Plant model
 * Generated by the TERRA CSP2LUNA generator version 0.6.1
 *
 * protected region document description on begin
 * protected region document description end
 */

#include "include/Plant.h"

// protected region additional headers on begin
// Each additional header should get a corresponding dependency in the Makefile
// protected region additional headers end

namespace MainModel { namespace Plant { 



Plant::Plant(ChannelOut<double> *in, ChannelIn<double> *out) :
    Sequential(NULL)
{
  SETNAME(this, "Plant");

  // Initialize model objects
  mycodePlant = new codePlant::codePlant(&inPlant,&outPlant);
  SETNAME(mycodePlant, "codePlant");
  myread_inPlant = new Reader<double>(&inPlant, in);
  SETNAME(myread_inPlant, "read_inPlant");
  mywrite_outPlant = new Writer<double>(&outPlant, out);
  SETNAME(mywrite_outPlant, "write_outPlant");


  // Register model objects
  this->append_child(mywrite_outPlant);
  this->append_child(myread_inPlant);
  this->append_child(mycodePlant);

  // protected region constructor on begin
  // protected region constructor end
}

Plant::~Plant()
{
  // TODO Properly destroy all additional objects that got defined in the constructor

  // protected region destructor on begin
  // protected region destructor end

  // Destroy model objects
  delete mywrite_outPlant;
  delete myread_inPlant;
  delete mycodePlant;
}



// protected region additional functions on begin
// protected region additional functions end

// Close namespace(s)
} } 
