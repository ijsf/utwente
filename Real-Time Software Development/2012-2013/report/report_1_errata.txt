In exercise 1.4 the following models were created:
 * JIWYDL (sequential I/O, with deadlock)
 * JIWYDF (parallel readers, without deadlock)
 * JIWYSEQ (parallel readers/writers, without deadlock)

The JIWYSEQ model was incorrect, in that the READH group was recursive and the SEQUENTIALH group was not. It has been corrected and now SEQUENTIALH is recursive and READH is not.
