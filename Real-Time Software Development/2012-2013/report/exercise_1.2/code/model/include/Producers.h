/**
 * Header file for the Producers model
 * Generated by the TERRA CSP2LUNA generator version 0.6.1
 *
 * protected region document description on begin

 * protected region document description end
 */

#pragma once

#include "csp/CSP.h"
#include "include/Producers/code1.h"

// protected region additional headers on begin
// Each additional header should get a corresponding dependency in the Makefile
// protected region additional headers end

using namespace LUNA::CSP;

namespace MainModel { namespace Producers { 

class Producers : public Sequential
{

public:
  // Define constructor and destructor
  Producers(ChannelIn<int> *ch1, ChannelIn<int> *ch2, ChannelIn<int> *ch3);
  virtual ~Producers();


  // Guard evaluation functions
  bool ch1sendGuardEvaluate();
  bool ch2sendGuardEvaluate();
  bool ch3sendGuardEvaluate();


private:

  // Class variables
  int var1;
  int var2;
  int var3;

  // Model objects
  Writer<int> *mych1send;
  Writer<int> *mych2send;
  Writer<int> *mych3send;
  code1::code1 *mycode1;

  // Model groups
  Alternative *myALTERNATIVE;

  // protected region additional class members or functions on begin

  // protected region additional class members or functions end
};

// Close namespace(s)
} } 
