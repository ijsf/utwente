# This is an automatically generated record.
# The area between QNX Internal Start and QNX Internal End is controlled by
# the QNX IDE properties.

ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

#===== USEFILE - the file containing the usage message for the application. 
USEFILE=

# Next lines are for C++ projects only

EXTRA_SUFFIXES+=cxx cpp

#===== LDFLAGS - add the flags to the linker command line.
LDFLAGS+=-lang-c++

VFLAG_g=-gstabs+

#===== EXTRA_INCVPATH - a space-separated list of directories to search for include files.
EXTRA_INCVPATH+=$(PROJECT_ROOT)/include  \
	D:\RTSD\LUNA\include $(PROJECT_ROOT)/20sim/common

#===== EXTRA_LIBVPATH - a space-separated list of directories to search for library files.
EXTRA_LIBVPATH+=D:\RTSD\LUNA\lib

#===== EXTRA_SRCVPATH - a space-separated list of directories to search for source files.
EXTRA_SRCVPATH+=$(PROJECT_ROOT)/20sim  \
	$(PROJECT_ROOT)/20sim/common $(PROJECT_ROOT)/GenV/20sim  \
	$(PROJECT_ROOT)/GenH/20sim $(PROJECT_ROOT)/GenH  \
	$(PROJECT_ROOT)/GenV $(PROJECT_ROOT)/OutputPWM  \
	$(PROJECT_ROOT)

#===== LIBS - a space-separated list of library items to be included in the link.
LIBS+=-Bstatic LUNA c m -Bdynamic hiddi

include $(MKFILES_ROOT)/qmacros.mk
ifndef QNX_INTERNAL
QNX_INTERNAL=$(PROJECT_ROOT)/.qnx_internal.mk
endif
include $(QNX_INTERNAL)

include $(MKFILES_ROOT)/qtargets.mk

OPTIMIZE_TYPE_g=none
OPTIMIZE_TYPE=$(OPTIMIZE_TYPE_$(filter g, $(VARIANTS)))

