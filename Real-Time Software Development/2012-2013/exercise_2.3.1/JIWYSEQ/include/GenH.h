/**
 * Header file for the GenH model
 * Generated by the TERRA CSP2LUNA generator version 0.6.1
 *
 * protected region document description on begin
 * protected region document description end
 */

#pragma once

#include "csp/CSP.h"
#include "include/GenH/MyDrvEnc.h"

// protected region additional headers on begin
// Each additional header should get a corresponding dependency in the Makefile
// protected region additional headers end

using namespace LUNA::CSP;

namespace MainModel { namespace GenH { 

class GenH : public Parallel
{

public:
  // Define constructor and destructor
  GenH(ChannelIn<int> *SensorH, ChannelIn<int> *SteerH);
  virtual ~GenH();



private:

  // Class variables
  int sensor;
  int steer;
  LUNA::Timer::period_t timestep;

  // Channel definitions
  ChannelIn<LUNA::Clock::period_t> *mywriterTimeStep2readerTimeStepChannel;

  // Model objects
  MyDrvEnc::MyDrvEnc *myMyDrvEnc;
  //Reader<int> *myreaderTimeStep;
  Writer<int> *mywriterSensor;
  Writer<int> *mywriterSteer;
  Writer<LUNA::Clock::period_t> *mywriterTimeStep;

  // Model groups
  Sequential *mySEQUENTIAL;

  // protected region additional class members or functions on begin
  // protected region additional class members or functions end
};

// Close namespace(s)
} } 
