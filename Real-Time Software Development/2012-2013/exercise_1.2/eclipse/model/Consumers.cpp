/**
 * Source file for the Consumers model
 * Generated by the TERRA CSP2LUNA generator version 0.6.1
 *
 * protected region document description on begin
 * protected region document description end
 */

#include "include/Consumers.h"

// protected region additional headers on begin
// Each additional header should get a corresponding dependency in the Makefile
// protected region additional headers end

namespace MainModel { namespace Consumers { 



Consumers::Consumers(GuardedChannelOut<int> *ch1, GuardedChannelOut<int> *ch2, GuardedChannelOut<int> *ch3) :
    Alternative()
{
  SETNAME(this, "Consumers");

  // Initialize model objects
  mych1read = new GuardedReader<int>(&var1, ch1);
  SETNAME(mych1read, "ch1read");
  mych2read = new GuardedReader<int>(&var2, ch2);
  SETNAME(mych2read, "ch2read");
  mych3read = new GuardedReader<int>(&var3, ch3);
  SETNAME(mych3read, "ch3read");
  mycode2 = new code2::code2();
  SETNAME(mycode2, "code2");


  // Register model objects
  this->append_child(mych1read);
  this->append_child(mych2read);
  this->append_child(mych3read);

  // protected region constructor on begin
  // protected region constructor end
}

Consumers::~Consumers()
{
  // TODO Properly destroy all additional objects that got defined in the constructor

  // protected region destructor on begin
  // protected region destructor end

  // Destroy model objects
  delete mycode2;
  delete mych3read;
  delete mych2read;
  delete mych1read;
}



// protected region additional functions on begin
// protected region additional functions end

// Close namespace(s)
} } 
