<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <title>SDF3 tutorial :: Hands-on exercises</title>
    <meta http-equiv='Content-type' content='text/html;charset=iso-8859-1' />
    <link rel='stylesheet' type='text/css' media='screen' href='../style/master.css' />
    <script src="../script/jquery.tools.min.js"></script>
    <script src="../script/toggle.js"></script>
</head>
<body>
<div id='mainone'>
    <!-- header -->
    <div id='header'>
        <div class='left'>
            <a href='http://www.es.ele.tue.nl/sdf3/'><img src='../img/sdf3.png' alt=''/></a>
        </div>
        <div class='right'>
            <a href='http://www.es.ele.tue.nl'><img src='../img/logo.png' alt='' height='50px'/></a>
       </div>
    </div>

    <!-- navigation -->
    <div id='nav-main'>
        <ul id='nav-main-list'>
         <li><a href='../../index.html'>Home</a></li>
         <li><strong><a href='index.html'>Hands-on exercises</a></strong></li>
         <li><a href='http://www.es.ele.tue.nl/sdf3/'>SDF3 online</a></li>
        </ul>
    </div>

    <div>
        <div id='content'>

<h1>Hands-on exercises</h1>

<p>You will be using the <a href='http://www.es.ele.tue.nl/sdf3'>SDF3 tool-kit</a> in this hands-on session. The archive that contains these exercises contains a pre-compiled version of SDF3 for both Linux and Windows. These executables are located in the folder '&lt;path where you unpacked the archive&gt;/sdf3/&lt;your platform&gt;'. Inside this folder you will find two executables called 'sdf3analyze-fsmsadf' and 'sdf3flow-fsmsadf' which you will use in this tutorial.

<div class='note'>
The Linux executables require libxml2 to be installed on your system. This library is installed by default on most distributions. Otherwise it should be available through your package manager. The Windows executables requires the Microsoft Visual C++ 2010 Redistributable Package. You can download this package at <a href='http://www.microsoft.com/download/en/details.aspx?id=5555'>this</a> location.</div>

<p>During this hands-on session, you will be working on the following four exercises:</p>

<ul>
    <li><a href='exercise1.html'>Exercise 1</a>: throughput analysis</li>
    <li><a href='exercise2.html'>Exercise 2</a>: modeling buffer sizes and analyzing buffer requirements</li>
    <li><a href='exercise3.html'>Exercise 3</a>: modeling interconnect delay</li>
    <li><a href='exercise3b.html'>Exercise 3</a>: latency analysis</li>
    <li><a href='exercise4.html'>Exercise 4</a>: putting it all together, using the SDF3 design-flow</li>
    <li><a href='assignment.html'>Exercise 4</a>: The Cyber-Physical-Systems assignment for QEES</li>   
</ul>

<div class='question'>There is a series of questions that you need to answer during these exercises. You can always get the answer to a question by clicking on the following icon <span id="triggers"><img class='right' src="../img/solution.png" rel="#answer1" height='20px'/></span></div>

<div class="simple_overlay" id="answer1">
Here you can find the answer to the question.
</div>

<div class='action'>There are also a number of actions that you need to perform. These actions have been marked with a blue box like the one containing this text.</div>

<p>Now it is time to get started with the <a href='exercise1.html'>first exercise</a>.</p>

<script>
$(document).ready(function() {
    $("img[rel]").overlay();
});
</script>

        </div>
    </div>
</div>
</body>
</html>


