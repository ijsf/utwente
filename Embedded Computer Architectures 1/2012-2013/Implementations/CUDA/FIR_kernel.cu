/*
 *
 *
 *
 */
#ifndef _FIR_KERNEL_H_
#define _FIR_KERNEL_H_

#include <fir.h>
#include <stdio.h>

template< typename T, bool useSinglePrecision, bool useFMA >
__global__ void FIR( T * results, T * signal, T * coeff, const size_t n_samples )
{
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;

	// discard out-of-bound threads
	if( i >= n_samples ) return;

	// clear output to initial zero
	T output = 0.0;

	for( int j = 0; j < NUM_COEFFICIENTS; ++j )
	{
		int k = 11 + i - j;

		// CUDA explicitly requires specification of the rounding mode
		// IEEE 754 describes four rounding modes, of which round-to-nearest is default

		if( useFMA )
		{
			if( useSinglePrecision )
			{
				// use explicit single-precision FMA operator
				output = __fmaf_rn( signal[ k ], coeff[ j ], output );
			}
			else
			{
				// use explicit double-precision FMA operator
				output = __fma_rn( signal[ k ], coeff[ j ], output );
			}
		}
		else
		{
			// combinations of * and + operations may be replaced with FMA equivalents by compiler,
			// so we have to use explicit add and mul operations as described by the manual
			if( useSinglePrecision )
			{
				output = __fadd_rn( output, __fmul_rn( signal[ k ], coeff[ j ] ) );
			}
			else
			{
				output = __dadd_rn( output, __dmul_rn( signal[ k ], coeff[ j ] ) );
			}
		}
	}
	results[ i ] = output;
}

#endif // #ifndef _FIR_KERNEL_H_
