#ifndef FIR_H
#define FIR_H

// Use single-precision, leave undefined to use double-precision
//#define USE_SINGLE_PRECISION
// Use FMA, leave undefined to use 2-step multiply-add
#define USE_FMA

//////////////////////////////////////////////////////////////////////////////
// CUDA

#ifndef MAX_PATH
#define MAX_PATH	255
#endif

#ifdef USE_SINGLE_PRECISION
typedef float	testtype_t;
#else
typedef double	testtype_t;
#endif

#ifndef size_t
typedef unsigned int	size_t;
#endif

//////////////////////////////////////////////////////////////////////////////

#endif
