/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

// Utilities and system includes
#include <cutil_inline.h>

#include <cuda_runtime.h>

#include "fir.h"
#include "data.h"

// includes, kernels
#include <FIR_kernel.cu>

////////////////////////////////////////////////////////////////////////////////
// These are CUDA Helper functions

    // This will output the proper CUDA error strings in the event that a CUDA host call returns an error
    #define checkCudaErrors(err)           __checkCudaErrors (err, __FILE__, __LINE__)

    inline void __checkCudaErrors( cudaError err, const char *file, const int line )
    {
        if( cudaSuccess != err) {
		    fprintf(stderr, "%s(%i) : CUDA Runtime API error %d: %s.\n",
                    file, line, (int)err, cudaGetErrorString( err ) );
            exit(-1);
        }
    }

    // This will output the proper error string when calling cudaGetLastError
    #define getLastCudaError(msg)      __getLastCudaError (msg, __FILE__, __LINE__)

    inline void __getLastCudaError( const char *errorMessage, const char *file, const int line )
    {
        cudaError_t err = cudaGetLastError();
        if( cudaSuccess != err) {
            fprintf(stderr, "%s(%i) : getLastCudaError() CUDA error : %s : (%d) %s.\n",
                    file, line, errorMessage, (int)err, cudaGetErrorString( err ) );
            exit(-1);
        }
    }

    // General GPU Device CUDA Initialization
    int gpuDeviceInit(int devID)
    {
        int deviceCount;
        checkCudaErrors(cudaGetDeviceCount(&deviceCount));
        if (deviceCount == 0) {
            fprintf(stderr, "gpuDeviceInit() CUDA error: no devices supporting CUDA.\n");
            exit(-1);
        }
        if (devID < 0) 
            devID = 0;
        if (devID > deviceCount-1) {
            fprintf(stderr, "\n");
            fprintf(stderr, ">> %d CUDA capable GPU device(s) detected. <<\n", deviceCount);
            fprintf(stderr, ">> gpuDeviceInit (-device=%d) is not a valid GPU device. <<\n", devID);
            fprintf(stderr, "\n");
            return -devID;
        }

        cudaDeviceProp deviceProp;
        checkCudaErrors( cudaGetDeviceProperties(&deviceProp, devID) );
        if (deviceProp.major < 1) {
            fprintf(stderr, "gpuDeviceInit(): GPU device does not support CUDA.\n");
            exit(-1);                                                  \
        }

        checkCudaErrors( cudaSetDevice(devID) );
        printf("> gpuDeviceInit() CUDA device [%d]: %s\n", devID, deviceProp.name);
        return devID;
    }
// end of CUDA Helper Functions

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
void runTest(int argc, char** argv);

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
    runTest(argc, argv);
}

////////////////////////////////////////////////////////////////////////////////
//! Run a simple test for CUDA
////////////////////////////////////////////////////////////////////////////////
void runTest(int argc, char** argv)
{
    if(checkCmdLineFlag(argc, (const char**)argv, "device"))
    {
        int devID = getCmdLineArgumentInt(argc, (const char **)argv, "device=");
        if (devID < 0) {
            printf("Invalid command line parameters\n");
            exit(-1);
        } else {
            devID = gpuDeviceInit(devID);
            if (devID < 0) {
               printf("exiting...\n");
               exit(-1);
            }
        }
    }
    else
    {
        cudaSetDevice( cutGetMaxGflopsDeviceId() );
    }

    int devID;
    cudaDeviceProp props;

    // get number of SMs on this GPU
    checkCudaErrors(cudaGetDevice(&devID));
    checkCudaErrors(cudaGetDeviceProperties(&props, devID));

    // execution settings
	char szFileResultsRaw[ MAX_PATH ];
	char szFileResultsDiff[ MAX_PATH ];
	char szFileResultsInfo[ MAX_PATH ];
    int block_size = 768;
    int nIter = 30;

	// predefined settings
#ifdef USE_FMA
	const bool useFMA = true;
#else
	const bool useFMA = false;
#endif
#ifdef USE_SINGLE_PRECISION
	const bool useSinglePrecision = true;
#else
	const bool useSinglePrecision = false;
#endif
	sprintf( szFileResultsRaw, "results_raw_%s_%s.txt", useSinglePrecision ? "sp" : "dp", useFMA ? "fma" : "no-fma" );
	sprintf( szFileResultsDiff, "results_diff_%s_%s.txt", useSinglePrecision ? "sp" : "dp", useFMA ? "fma" : "no-fma" );
	sprintf( szFileResultsInfo, "results_info_%s_%s.txt", useSinglePrecision ? "sp" : "dp", useFMA ? "fma" : "no-fma" );

	const size_t n_results = NUM_SAMPLES;
	const size_t n_awgn_signal = sizeof( dp_awgn_signal ) / sizeof( double );
	const size_t n_coeff = sizeof( dp_coeff ) / sizeof( double );

	testtype_t *	d_results;
	testtype_t *	h_results;

	testtype_t *	d_awgn_signal,	* d_coeff;
	testtype_t *	h_awgn_signal,	* h_coeff;

	// allocate CPU arrays
	h_results = (testtype_t *)		malloc( sizeof( testtype_t ) * n_results );
	h_awgn_signal = (testtype_t *)	malloc( sizeof( testtype_t ) * n_awgn_signal );
	h_coeff = (testtype_t *)		malloc( sizeof( testtype_t ) * n_coeff );
	
	// prepare predefined CPU arrays
	for( size_t i = 0; i < n_awgn_signal; ++i )
	{
		h_awgn_signal[i] = (testtype_t)dp_awgn_signal[i];
	}
	for( size_t i = 0; i < n_coeff; ++i )
	{
		h_coeff[i] = (testtype_t)dp_coeff[i];
	}

	// allocate GPU global memory
	cudaMalloc( (void**) &d_results,		sizeof( testtype_t ) * n_results );
	cudaMalloc( (void**) &d_awgn_signal,	sizeof( testtype_t ) * n_awgn_signal );
	cudaMalloc( (void**) &d_coeff,			sizeof( testtype_t ) * n_coeff );
	getLastCudaError( "cudaMalloc" );
	
	// copy predefined data to GPU
	cudaMemcpy( d_awgn_signal,	h_awgn_signal,	n_awgn_signal * sizeof( testtype_t ),	cudaMemcpyHostToDevice );
	cudaMemcpy( d_coeff,		h_coeff,		n_coeff * sizeof( testtype_t ),			cudaMemcpyHostToDevice );
	getLastCudaError( "cudaMemcpy" );

    // setup execution parameters
    dim3 threads(block_size, 1);
    dim3 grid(n_results / threads.x + 1, 1);

    // create and start timer
    unsigned int hTimer;

    // performs warmup operation
	FIR< testtype_t, useSinglePrecision, useFMA ><<< grid, threads >>>( d_results, d_awgn_signal, d_coeff, n_results );
    cudaDeviceSynchronize();

	// Start Timing	
	cutCreateTimer( &hTimer );
	cutResetTimer( hTimer );
	cutStartTimer( hTimer );
	{
		for( size_t j = 0; j < nIter; j++ )
		{
			FIR< testtype_t, useSinglePrecision, useFMA ><<< grid, threads >>>( d_results, d_awgn_signal, d_coeff, n_results );
		}
		getLastCudaError("CUDA kernel execution failed");
		cudaDeviceSynchronize();
	}
	cutStopTimer( hTimer );

	double dTotalTime = cutGetTimerValue( hTimer );
	double dIterTime = dTotalTime / double( nIter );

	// download results from device to host
	cudaMemcpy( h_results, d_results, sizeof( testtype_t ) * n_results, cudaMemcpyDeviceToHost );
	getLastCudaError( "cudaMemcpy" );

	FILE * file;

	// write info to file
	file = fopen( szFileResultsInfo, "w" );
	{
	    fprintf( file, "Device %d: \"%s\" with Compute %d.%d capability\n", devID, props.name, props.major, props.minor );
		fprintf( file, "Total execution time: %10.30f ms (%u iterations)\n", dTotalTime, nIter );
		fprintf( file, "Kernel execution time: %10.30f ms\n", dIterTime );
	}
	fclose( file );

	// write raw results to file
	file = fopen( szFileResultsRaw, "w" );
	for( size_t i = 0; i < n_results; ++i )
	{
		fprintf( file, "%10.30f\n", h_results[i] );
	}
	fclose( file );

	// perform comparison of results with reference and write to file
	file = fopen( szFileResultsDiff, "w" );
	for( size_t i = 0; i < n_results; ++i )
	{
		double diff = abs( double( h_results[i] ) - dp_reference_output[i] );
		fprintf( file, "%10.30f\n", diff );
	}
	fclose( file );

    // clean up
	cutDeleteTimer( hTimer );

	free( h_results );
	free( h_awgn_signal );
	free( h_coeff );

	cudaFree( d_results );
	cudaFree( d_awgn_signal );
	cudaFree( d_coeff );

    cudaDeviceReset();
}
