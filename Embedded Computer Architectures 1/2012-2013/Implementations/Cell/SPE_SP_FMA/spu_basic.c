#include <stdio.h>
#include <spu_mfcio.h>
#include <math.h>
#include <massv.h>
#include <spu_intrinsics.h>
#include <simdmath.h>
#include <simdmath/sqrtf4.h>
#include <simdmath/sqrtf4_fast.h>
#include <simdmath/expf4.h>
#include <simdmath/cosf4.h>
#include <simdmath/divf4.h>
#include <simdmath/divf4_fast.h>
#include <simdmath/fabsf4.h>
#include "data.h"

// A vector float contains four floats
vector float f_output[ BLOCK / 4 ]	__attribute__ ( ( aligned( 128 ) ) );
com 		 c 						__attribute__ ( ( aligned( 128 ) ) );

int main( unsigned long long spe_id, 
          unsigned long long argp, 
          unsigned long long envp )
{
	// Retrieve settings
   	spu_write_decrementer( 0 );

	mfc_get( &c, argp, sizeof( com ), TAG, 0, 0 );
	mfc_write_tag_mask( 1 << TAG );
	mfc_read_tag_status_all();

	int   x;
	vector float array_sine;
	vector float array_coeff[ NUM_COEFFICIENTS ];

	for( x = 0; x < NUM_COEFFICIENTS; ++x )
	{
		array_coeff[ x ] = spu_splats( ( float )dp_coeff[ x ] );
	}
	
	for( x = 0; x < 256; ++x )
	{
	    int i = NUM_COEFFICIENTS + x * 4;
      
	    f_output[ x ][ 0 ] = 0.0f;
	    f_output[ x ][ 1 ] = 0.0f;
	    f_output[ x ][ 2 ] = 0.0f;
	    f_output[ x ][ 3 ] = 0.0f;
		      
		array_sine[ 0 ] = ( float )dp_awgn_signal[ i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ i + 1 ]; 
		array_sine[ 2 ] = ( float )dp_awgn_signal[ i + 2 ]; 
		array_sine[ 3 ] = ( float )dp_awgn_signal[ i + 3 ]; 
			
		f_output[ x ] = spu_madd( array_sine, array_coeff[ 0 ], f_output[ x ] );

		array_sine[ 0 ] = ( float )dp_awgn_signal[ -1 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -1 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -1 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -1 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 1 ], f_output[ x ] );

		array_sine[ 0 ] = ( float )dp_awgn_signal[ -2 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -2 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -2 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -2 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 2 ], f_output[ x ] );
		
		array_sine[ 0 ] = ( float )dp_awgn_signal[ -3 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -3 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -3 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -3 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 3 ], f_output[ x ] );

		array_sine[ 0 ] = ( float )dp_awgn_signal[ -4 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -4 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -4 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -4 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 4 ], f_output[ x ] );

		array_sine[ 0 ] = ( float )dp_awgn_signal[ -5 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -5 + i + 1 ]; 
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -5 + i + 2 ]; 
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -5 + i + 3 ]; 
			
		f_output[ x ] = spu_madd( array_sine, array_coeff[ 5 ], f_output[ x ] );

		array_sine[ 0 ] = ( float )dp_awgn_signal[ -6 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -6 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -6 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -6 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 6 ], f_output[ x ] );

		array_sine[ 0 ] = ( float )dp_awgn_signal[ -7 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -7 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -7 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -7 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 7 ], f_output[ x ] );
		
		array_sine[ 0 ] = ( float )dp_awgn_signal[ -8 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -8 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -8 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -8 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 8 ], f_output[ x ] );

		array_sine[ 0 ] = ( float )dp_awgn_signal[ -9 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -9 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -9 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -9 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 9 ], f_output[ x ] );
		
		array_sine[ 0 ] = ( float )dp_awgn_signal[ -10 + i ];
		array_sine[ 1 ] = ( float )dp_awgn_signal[ -10 + i + 1 ];
		array_sine[ 2 ] = ( float )dp_awgn_signal[ -10 + i + 2 ];
		array_sine[ 3 ] = ( float )dp_awgn_signal[ -10 + i + 3 ];

		f_output[ x ] = spu_madd( array_sine, array_coeff[ 10 ], f_output[ x ] );
	} 

	// Write the output back
	mfc_put( f_output, c.output + c.begin * sizeof( float ), BLOCK * sizeof( float ), TAG, 0, 0 );
	mfc_write_tag_mask( 1 << TAG );
	mfc_read_tag_status_all();

	unsigned int time;
	time = -spu_read_decrementer();
	printf( "Spe %i took %.2fms\n", c.id, ( time / 79800.0f ) );

	return 0;
}
