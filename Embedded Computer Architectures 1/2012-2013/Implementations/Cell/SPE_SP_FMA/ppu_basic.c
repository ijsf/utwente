#include <stdio.h>
#include <stdlib.h>
#include <libspe2.h>
#include <pthread.h>
#include <ppu_intrinsics.h>
#include "data.h"

/* The data sent to the pthread */
typedef struct ppu_pthread_data
{
    spe_context_ptr_t	speid;
    pthread_t 			pthread;
    void 				*argp;
} ppu_pthread_data_t;

float output[ LENGTH ]    __attribute__ ( ( aligned( 128 ) ) );
com   c[ SPUS ]           __attribute__ ( ( aligned( 128 ) ) );

/* The function executed in the pthread */
void *ppu_pthread_function( void *arg )
{
	ppu_pthread_data_t *data = ( ppu_pthread_data_t * )arg;
	unsigned int 	   entry = SPE_DEFAULT_ENTRY;
	int				   retval;
   
	if( ( retval = spe_context_run( data->speid, &entry, 0, data->argp, NULL, NULL) ) < 0 )
	{
		perror( "spe_context_run" );
		exit( 1 );
	}
	
	pthread_exit( NULL );
}

/* SPU initialization data */
unsigned long long control_block;

/* SPU program handle */
extern spe_program_handle_t spu_basic;  
	   ppu_pthread_data_t 	data[ 16 ];

int main( int argc, char **argv )
{
	int i, retval, spus;
    spus = SPUS;   
 
    if( sizeof( com ) % 16 != 0 )
	{
		printf( "sizeof(com) =  %lu and this is not a multiple of 16\n", sizeof( com ) );
		printf( "fix padding in data.h\n" );
		
		return -1;
    }
  
	/* Create a context and thread for each SPU */
	for( i = 0; i < spus; ++i)
	{
		c[ i ].id	  = i;
		c[ i ].begin  = i * BLOCK;
		c[ i ].block  = BLOCK;
		c[ i ].output = ( unsigned long long )output;

		/* Create context */
		if( ( data[ i ].speid = spe_context_create( 0, NULL ) ) == NULL )
		{
         perror( "spe_context_create" );
         exit( 1 );
		}
      
		/* Load program into the context */
		if( ( retval = spe_program_load( data[ i ].speid, &spu_basic ) ) != 0 )
		{
			perror( "spe_program_load" );
			exit( 1 );
		}
      
		/* Initialize control block and thread data */
 	    data[ i ].argp = ( void * )&c[ i ];
     
		/* Create thread */
		if( ( retval = pthread_create( &data[ i ].pthread, NULL, &ppu_pthread_function, &data[ i ] ) ) != 0 ) 
		{
			perror( "pthread_create" );
			exit( 1 );
		}
	}

	/* Wait for the threads to finish processing */
	for( i = 0; i < spus; ++i ) 
	{
		if( ( retval = pthread_join( data[ i ].pthread, NULL ) ) != 0 ) 
		{
			perror( "pthread_join" );
			exit( 1 );
		}
		
		if( ( retval = spe_context_destroy( data[ i ].speid ) ) != 0 )
		{
			perror( "spe_context_destroy" );
			exit( 1 );
		}
	}

	for( i = 0; i < LENGTH; ++i )
	{
		printf( "%10.30f\n", output[ i ] );
	}
	
	printf( "\nDifference between calculated and reference values:\n" );
	
	double diff;
	double absDiff;
	double maxDiff = -1000.0;
	double minDiff = 1000.0;
	double averageDiff = 0.0;
	
	for( i = 0; i < LENGTH; ++i )
	{
		diff = dp_reference_output[ i ] - ( double )output[ i ];
		
		if( diff < 0.0 )
		{
			absDiff = -diff;
		}
		else
		{
			absDiff = diff;
		}
		
		// Calculate average difference
		averageDiff += absDiff;
		
		// Calculate minimum difference
		if( minDiff > absDiff )
		{
			minDiff = absDiff;
		}
		
		// Calculate maximum difference
		if( maxDiff < absDiff )
		{
			maxDiff = absDiff;
		}
		
		printf( "%10.30f\n", diff );
	}
	
	averageDiff /= LENGTH;
	printf( "Average difference between calculated and reference values: %10.30f\n", averageDiff );
	printf( "Minimum difference: %10.30f\tMaximum difference: %10.30f\n", minDiff, maxDiff );
	
	return 0;
}
