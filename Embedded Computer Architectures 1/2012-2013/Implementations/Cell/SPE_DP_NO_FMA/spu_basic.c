#include <stdio.h>
#include <spu_mfcio.h>
#include <math.h>
#include <massv.h>
#include <spu_intrinsics.h>
#include <simdmath.h>
#include <simdmath/sqrtf4.h>
#include <simdmath/sqrtf4_fast.h>
#include <simdmath/expf4.h>
#include <simdmath/cosf4.h>
#include <simdmath/divf4.h>
#include <simdmath/divf4_fast.h>
#include <simdmath/fabsf4.h>
#include "data.h"

// A vector double contains two doubles
vector double d_output[ BLOCK / 2 ]	__attribute__ ( ( aligned( 128 ) ) );
com 		  c 					__attribute__ ( ( aligned( 128 ) ) );

int main( unsigned long long spe_id, 
          unsigned long long argp, 
          unsigned long long envp )
{
	// Retrieve settings
   	spu_write_decrementer( 0 );

	mfc_get( &c, argp, sizeof( com ), TAG, 0, 0 );
	mfc_write_tag_mask( 1 << TAG );
	mfc_read_tag_status_all();

	int    x;
	vector double array_sine;
	vector double array_coeff[ NUM_COEFFICIENTS ];
	vector double temp;

	for( x = 0; x < NUM_COEFFICIENTS; ++x )
	{
		array_coeff[ x ] = spu_splats( dp_coeff[ x ] );
	}
	
	for( x = 0; x < 512; ++x )
	{
	    int i = NUM_COEFFICIENTS + x * 2;
	      
	    d_output[ x ][ 0 ] = 0.0;
	    d_output[ x ][ 1 ] = 0.0;
		      
		array_sine[ 0 ] = dp_awgn_signal[ i ];
		array_sine[ 1 ] = dp_awgn_signal[ i + 1 ]; 
					
		temp = spu_mul( array_sine, array_coeff[ 0 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );

		array_sine[ 0 ] = dp_awgn_signal[ -1 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -1 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 1 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );

		array_sine[ 0 ] = dp_awgn_signal[ -2 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -2 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 2 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );
		
		array_sine[ 0 ] = dp_awgn_signal[ -3 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -3 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 3 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );

		array_sine[ 0 ] = dp_awgn_signal[ -4 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -4 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 4 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );

		array_sine[ 0 ] = dp_awgn_signal[ -5 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -5 + i + 1 ]; 
			
		temp = spu_mul( array_sine, array_coeff[ 5 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );

		array_sine[ 0 ] = dp_awgn_signal[ -6 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -6 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 6 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );

		array_sine[ 0 ] = dp_awgn_signal[ -7 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -7 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 7 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );
		
		array_sine[ 0 ] = dp_awgn_signal[ -8 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -8 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 8 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );

		array_sine[ 0 ] = dp_awgn_signal[ -9 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -9 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 9 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );
		
		array_sine[ 0 ] = dp_awgn_signal[ -10 + i ];
		array_sine[ 1 ] = dp_awgn_signal[ -10 + i + 1 ];

		temp = spu_mul( array_sine, array_coeff[ 10 ] );
		d_output[ x ] = spu_add( temp, d_output[ x ] );
	} 

	// Write the output back
	mfc_put( d_output, c.output + c.begin * sizeof( double ), BLOCK * sizeof( double ), TAG, 0, 0 );
	mfc_write_tag_mask( 1 << TAG );
	mfc_read_tag_status_all();

	unsigned int time;
	time = -spu_read_decrementer();
	printf( "Spe %i took %.2fms\n", c.id, ( time / 79800.0f ) );

	return 0;
}
