#include <stdio.h>
#include <stdlib.h>
#include <ppu_intrinsics.h>
#include "data.h"

float output[ LENGTH ]	__attribute__( ( aligned( 128 ) ) );

int main( int argc, char **argv )
{
	int i, j, k;
 
	for( i = 0; i < LENGTH; ++i )
	{
		output[ i ] = 0.0f;
		
		for( j = 0; j < NUM_COEFFICIENTS; ++j )
		{
			k = 11 + i - j;
			
			output[ i ] = __fmadds( ( float )dp_awgn_signal[ k ], ( float )dp_coeff[ j ], output[ i ] );
		}
	}
 
	for( i = 0; i < LENGTH; ++i )
	{
		printf( "%10.30f\n", output[ i ] );
	}
	
	printf( "\nDifference between calculated and reference values:\n" );
	
	double diff;
	double absDiff;
	double maxDiff = -1000.0;
	double minDiff = 1000.0;
	double averageDiff = 0.0;
	
	for( i = 0; i < LENGTH; ++i )
	{
		diff = dp_reference_output[ i ] - ( double )output[ i ];
		
		if( diff < 0.0 )
		{
			absDiff = -diff;
		}
		else
		{
			absDiff = diff;
		}
		
		// Calculate average difference
		averageDiff += absDiff;
		
		// Calculate minimum difference
		if( minDiff > absDiff )
		{
			minDiff = absDiff;
		}
		
		// Calculate maximum difference
		if( maxDiff < absDiff )
		{
			maxDiff = absDiff;
		}
		
		printf( "%10.30f\n", diff );
	}
	
	averageDiff /= LENGTH;
	printf( "Average difference between calculated and reference values: %10.30f\n", averageDiff );
	printf( "Minimum difference: %10.30f\tMaximum difference: %10.30f\n", minDiff, maxDiff );
	
	return 0;
}
