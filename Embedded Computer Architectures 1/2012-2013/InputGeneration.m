format longG

Fs = 1024; % Sampling frequency
T = 1 / Fs; % Sample time
L = 1024; % Length of signal
t = (1:L) * T; % Time vector

% Setup default precision
mp.Digits( 40 );

% Generate single-precision input signal
dp_signal = double( 100 * sin( 2 * pi * 5 * t ) );
dp_awgn_signal = double( awgn( dp_signal, 30, 'measured' ) );

% Coefficients
coeff = double( [0.2326075560372, 0.0124340773324, 0.01264347488785, 0.01310273791468, 0.01305205931364, 0.01319799229558, 0.01305205931364, 0.01310273791468, 0.01264347488785, 0.0124340773324, 0.2326075560372] );

% Double-precision input, arbitrary-precision filter
dp_output_mp = mp( zeros( 1, length( dp_signal ) ) );
N=length(coeff)
for n=1:( length( dp_signal ) )
    for i=1:uint32(N)
        k = 1+n-i;
        if k > 0
            dp_output_mp(n) = dp_output_mp(n) + dp_awgn_signal(uint32(k)) * coeff(i);
        end
    end
end

% Convert to double-precision output
dp_output_dp = double( dp_output_mp(:) );

% Matlab reference filter
%filter_ref = mp(filter(coeff,1,dp_awgn_signal));

% Plot difference
%plot(t,abs(filter_ref-dp_output_mp));
