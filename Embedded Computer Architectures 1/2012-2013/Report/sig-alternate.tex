% This is "sig-alternate.tex" V2.0 May 2012
% This file should be compiled with V2.5 of "sig-alternate.cls" May 2012
%
% This example file demonstrates the use of the 'sig-alternate.cls'
% V2.5 LaTeX2e document class file. It is for those submitting
% articles to ACM Conference Proceedings WHO DO NOT WISH TO
% STRICTLY ADHERE TO THE SIGS (PUBS-BOARD-ENDORSED) STYLE.
% The 'sig-alternate.cls' file will produce a similar-looking,
% albeit, 'tighter' paper resulting in, invariably, fewer pages.
%
% ----------------------------------------------------------------------------------------------------------------
% This .tex file (and associated .cls V2.5) produces:
%       1) The Permission Statement
%       2) The Conference (location) Info information
%       3) The Copyright Line with ACM data
%       4) NO page numbers
%
% as against the acm_proc_article-sp.cls file which
% DOES NOT produce 1) thru' 3) above.
%
% Using 'sig-alternate.cls' you have control, however, from within
% the source .tex file, over both the CopyrightYear
% (defaulted to 200X) and the ACM Copyright Data
% (defaulted to X-XXXXX-XX-X/XX/XX).
% e.g.
% \CopyrightYear{2007} will cause 2007 to appear in the copyright line.
% \crdata{0-12345-67-8/90/12} will cause 0-12345-67-8/90/12 to appear in the copyright line.
%
% ---------------------------------------------------------------------------------------------------------------
% This .tex source is an example which *does* use
% the .bib file (from which the .bbl file % is produced).
% REMEMBER HOWEVER: After having produced the .bbl file,
% and prior to final submission, you *NEED* to 'insert'
% your .bbl file into your source .tex file so as to provide
% ONE 'self-contained' source file.
%
% ================= IF YOU HAVE QUESTIONS =======================
% Questions regarding the SIGS styles, SIGS policies and
% procedures, Conferences etc. should be sent to
% Adrienne Griscti (griscti@acm.org)
%
% Technical questions _only_ to
% Gerald Murray (murray@hq.acm.org)
% ===============================================================
%
% For tracking purposes - this is V2.0 - May 2012

\documentclass{sig-alternate}

\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{listings}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage[usenames,dvipsnames]{xcolor}

\begin{document}
%
% --- Author Metadata here ---
%\conferenceinfo{WOODSTOCK}{'97 El Paso, Texas USA}
%\CopyrightYear{2007} % Allows default copyright year (20XX) to be over-ridden - IF NEED BE.
%\crdata{0-12345-67-8/90/01}  % Allows default copyright data (0-89791-88-6/97/05) to be over-ridden - IF NEED BE.
% --- End of Author Metadata ---

\title{Fused Multiply-Add Implementations}
\subtitle{Embedded Computer Architectures 1}
%
% You need the command \numberofauthors to handle the 'placement
% and alignment' of the authors beneath the title.
%
% For aesthetic reasons, we recommend 'three authors at a time'
% i.e. three 'name/affiliation blocks' be placed beneath the title.
%
% NOTE: You are NOT restricted in how many 'rows' of
% "name/affiliations" may appear. We just ask that you restrict
% the number of 'columns' to three.
%
% Because of the available 'opening page real-estate'
% we ask you to refrain from putting more than six authors
% (two rows with three columns) beneath the article title.
% More than six makes the first-page appear very cluttered indeed.
%
% Use the \alignauthor commands to handle the names
% and affiliations for an 'aesthetic maximum' of six authors.
% Add names, affiliations, addresses for
% the seventh etc. author(s) as the argument for the
% \additionalauthors command.
% These 'additional authors' will be output/set for you
% without further effort on your part as the last section in
% the body of your article BEFORE References or any Appendices.

\numberofauthors{2} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Cecill Etheredge
% 2nd. author
\alignauthor
O\u{g}uz Meteer
}
\date{November 2012}
% Just remember to make sure that the TOTAL number of authors
% is the number that will appear on the first page PLUS the
% number that will appear in the \additionalauthors section.

\maketitle
\begin{abstract}
In this paper we aim to demonstrate the benefit of using the recent fused multiply-add (FMA) floating point operation on architectures that provide support for this operation.
Before getting into the gritty details of the aforementioned operation, we first explain its fundamentals and how it affects precision.
We then explain our choice of architectures, and the details of our implementations on each of the given architectures.
Finally, we compare the results in terms of precision and power of all our implementations, and make a conclusion regarding the use of the FMA operations versus regular multiply-add.
\end{abstract}

\section{Background}
The multiply-add (MA) or multiply-accumulate (MAC) operation is defined as follows:
\begin{equation}
	\label{eq:ma}
	a \leftarrow a + (b*c)
\end{equation}

Note that on most architectures (e.g. DSPs, and FPUs), the above operation is implemented using the IEEE 754 floating point representation. The equation clearly shows that the operation is in fact a combination of two operations: one multiplication followed by one addition. Due to the inherent precision limitations of the floating point representation, each arithmetic operation performed with floating point will implicitly round its output. Due to this rounding, it follows that the multiply-add operation is typically performed with two roundings, each for one its operations.

The issue of rounding poses a problem for situations where high precision is required. One might argue that it is possible to switch to IEEE 754's double-precision format (if using single-precision) to mitigate the issue, but although this might work for certain cases, it does not solve the underlying issue of performing two rounding steps and might incur a performance penalty. One such scenario where high precision is preferred is the case of a Finite Impulse Response (FIR) filter:
\begin{equation}
	\label{eq:fir}
	y[n] = \displaystyle\sum\limits_{i=0}^N c_i*x[n-i]
\end{equation}

In the above equation $x[n]$ is the input value, $y[n]$ is the output value, $c_i$ are the filter coefficients (impulse response) and $N$ is the filter order. Note that the only operations in this equation are indeed addition and multiplication. Due to the summation, it is easy to understand why this equation has the potential to suffer from precision limitations: the rounding errors are accumulated into the final output value.

The twofold rounding errors of the typical two step multiply-add operation can be resolved by performing the multiply-add as a single step operation: this is called \emph{fused multiply-add} (FMA), which is also included in the newer IEEE 754-2008 floating point standard. That is, the FMA operation computes equation \ref{eq:ma} in a single step with full precision after which the output value is rounded down. As a consequence, the rounding error is decreased and performance is increased both due to the single step execution.

\section{Implementation}
In order to test the practical difference between the regular multiply-add operation and the FMA operation, it is necessary to create implementations on architectures that provide built-in support for the FMA operation. Testing is done by using an implementation of the FIR filter as described in equation \ref{eq:fir}, with a simple distorted sine function as input that is sampled 1024 times and a set of predefined coefficients. Implementations are provided in both single- as well as double-precision floating point formats. A reference data set is provided by means of an implementation in Matlab using arbitrary precision. Experimental data sets are then gathered by running the implementations on the PowerPC, Cell SPE, and GPU architectures.

\begin{figure}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{firfilter.png}
	\caption{FIR filter functionality. Image courtesy of Agilent.}\label{fig:matlab}
\end{figure}

Since the IEEE 754 defines the exact behaviour of floating point types for both single- and double-precision, implementations on any architecture should yield the exact same results for either type of precision, assuming that the architecture implements the IEEE 754 standard properly. It is however possible that architectures do not fully comply to the IEEE 754 due to performance or chip area considerations. This is usually not the case for double-precision floating point where high precision is often a necessity, but more often true for single-precision, where (real-time) performance is often more important than exact precision, e.g. in case of graphics renderers or physics engines.

\subsection{Reference (Matlab)}
The reference data sets of input and output values are calculated using our implementation in Matlab. Due to Matlab's extensive library of mathematical operations, it is easy to model the FIR filter and distorted sine input. The actual reference results of the filter can be seen in figure \ref{fig:matlab_fir}, and the source code is provided in appendix \ref{ap:matlab}. Matlab itself also provides a built-in FIR filter (using the \emph{filter} function) as can be seen in the source code, but we choose to use a custom Matlab implementation as this can be easily ported to different architectures. The output of the custom implementation was tested against output of Matlab's built-in FIR filter, and verified to be working correctly without any significant differences in output.

\begin{figure}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{eca1-fir_input_output_matlab.pdf}
	\caption{Matlab reference implementation with input and output.}\label{fig:matlab_fir}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{eca1-fir_error_difference_double_arbitrary.pdf}
	\caption{Absolute output difference (rounding error) between filter with arbitrary-precision operations and double-precision operations.}\label{fig:matlab_error}
\end{figure}

\subsubsection{Arbitrary-precision}
In order to get the most accurate reference data, we have implemented the filter operations using the \emph{mp} arbitrary-precision library, resulting in a precision that is significantly higher than the highest possible precision on any of our architectures (e.g. double-precision floating point). In order to verify that the precision is indeed higher, the absolute output difference (or output rounding error) between a filter with arbitrary-precision operations and double-precision operations is calculated and visualized in figure \ref{fig:matlab_error}. As can be seen, the error is in the order of magnitude of $10^{-14}$, while a similar setup with single-precision operations yielded an error order of magnitude of approximately $10^{-6}$. The rounding errors between the double- and arbitrary-precision operations are clearly present and verify that the mp library is working as expected. 

Note that we are explicitly using double-precision for our input values (highest possible precision for our architectures), as we are solely interested in the rounding errors caused by the actual filter operations, and not those of the sine functions that are used as input to our filter. The double-precision input values are generated in Matlab and will be used as input data set for all subsequent filter implementations, both single- and double-precision versions, performing casts to single-precision on the architecture when needed.

\subsection{Cell PPE (PowerPC)}
The Cell PPE is a two-way multithreaded core based on IBM's Power Architecture (PowerPC) and is available in the Sony PlayStation 3 game console, where it is used as both a conventional CPU and a controller for the available Cell SPE units (see section \ref{sec:spe}). Due to its role as conventional CPU, it is normally used to run operating system and other general purpose tasks. The Cell PPE contains 64 KB of L1-cache and 512 KB of L2-cache as well as a FPU and an IBM AltiVec arithmetic unit. The FPU provides the ability to perform floating point operations of both single- and double-precision, including a scalar FMA operation. The AltiVec unit provides a similar set of operations for use with single-precision vectorized floating point types, including a vector FMA operation.

\subsubsection{Test setup}
The test setup for this architecture is as follows:
\begin{itemize}
\label{list:ps3}
	\item System: \emph{Sony PlayStation 3}
	\item Processing unit: \emph{IBM PowerPC at 3.2GHz}
	\item Memory: \emph{256MB XDR RAM}
	\item Operating system: \emph{Fedore Core 12 ppc64, kernel 2.6.32}
\end{itemize}

\subsubsection{Implementation}
For our implementations, we have chosen to use the FPU instead of the AltiVec unit because the latter merely implements single-precision FMA functionality in a vectorized version, which we do not require, while the double-precision FMA does not exist.

The source of the implementations can be found in appendix \ref{ap:ppe} and are compiled with the \emph{-O0} flag to make sure that the compiler does not perform optimizations by replacing the seperate multiplication and addition steps with FMA instructions.
Implementations that use FMA are implemented with the \emph{fmadd} and \emph{fmadds} intrinsics for double and single precision respectively, and for the non-FMA versions, we used the intrinsics \emph{fmul} and \emph{fmuls} for double and single precision respectively.

\subsection{Cell SPE}
\label{sec:spe}
The Cell SPE is a RISC core with an instruction set consisting of 128-bit SIMD operations for single- and double-precision floating point. Each SPE contains 256 KB of embedded SRAM (also referred to as \emph{local memory}) with no support for caching or memory access prediction. In common setups such as the Sony Playstation 3 and the Toshiba SpursEngine, an arbitrary number of SPEs are connected to the EIB (Element Interconnect Bus), a high-speed ring bus, and can be used for parallel computing. In the case of the Sony Playstation 3, there are seven SPEs of which six are available for user applications.

The Cell SPE does not have a compliant single-precision implementation of the IEEE 754 floating point standard: e.g. arithmetic operations do not support NaN and infinite values, can only use the round-to-zero (truncate) rounding mode, denormalized values are not supported and zero results are always positive (never negative). Double-precision floating points however are fully compliant with IEEE 754. Needless to say, the Cell SPE implements an FMA operation for both single- and double-precision floating point.

Unfortunately, the IEEE 754 violation in the case of single-precision floating point will likely affect our implementation: the default rounding mode of IEEE 754, which we aim to use in our implementations, is defined as round-to-nearest, while the only supported rounding mode on the SPE is round-to-zero. We therefore expect that the precision of this particular implementation will deviate from other architectures.

\subsubsection{Test setup}
The test setup is identical to the test setup given in \ref{list:ps3}.

\subsubsection{Implementation}
The source of the implementations can be found in appendix \ref{ap:spe}, where only one of the six available SPEs is used. We again compiled the with the \emph{-O0} flag, because otherwise the compiler performs optimizations by replacing the seperate multiplication and addition steps with FMA instructions.

Because the Cell SPE only operates on 128-bit vectors, we have parallellized our implementations, where instructions are performed on four or two elements for single and double precision respectively. Therefore we created a coefficient vector array \emph{array\_coeff}, in which a coefficient scalar is duplicated so that each element of a vector contains the coefficient. This is done using the \emph{spu\_splats} intrinsic. Implementations using FMA are implemented with the \emph{spu\_madd} intrinsic, and the non-FMA versions use the \emph{spu\_add} and \emph{spu\_mul} intrinsics.

\subsection{GPU}
The modern GPU is a highly parallel general-purpose processor with the ability of running numerous fine-grain threads or kernels. GPGPU programming can be seen as a form of stream processing, where identical operations are performed on each element of the input domain in parallel.
NVIDIA's CUDA programming platform exposes the parallel computing interface of the GPU in an extended ANSI C programming language.
Among a wide range of CUDA-capable hardware is the NVIDIA Tesla C2050, one of the high-end GPUs targeted at the high performance computing market, and also used in this paper, supporting 448 streaming multiprocessors (SMs) and GDDR5 memory. Each SM supports in the order of a thousand parallel threads and is equipped with a large register file, giving each thread its own dedicated set of registers.

In CUDA, different GPU devices are classified by Compute versions. All devices with Compute capability of 1.3 upward provide support for double-precision floating point, while 2.0 and up provide support for hardware FMA. Additionally, IEEE 754 compliance varies across different operations: not all operations provide the required IEEE 754 rounding modes; there is no support for NaN signaling or denormalized values. Fortunately, these violations do not affect our implementation as all devices with Compute capability 2.x implement support for an FMA operation that is fully compliant with IEEE 754-2008.

\subsubsection{Test setup}
\begin{itemize}
\label{list:tesla}
	\item System: \emph{Intel Core i7-860, 6GB RAM}
	\item Processing unit: \emph{NVIDIA Tesla C2050 (Compute 2.0)}
	\item CUDA version: \emph{CUDA 4.2, 32-bit}
	\item Operating system: \emph{Windows 7 x64}
\end{itemize}
\subsubsection{Implementation}
The source code of our implementation (or kernel) can be found in appendix \ref{ap:gpu}. Since the CUDA 4.2 compiler implicitly converts combinations of common operators $*$ and $+$ into FMA operations (when supported, e.g. Compute 2.x), our implementation uses explicit instruction intrinsics for either the multiply, add or fused multiply-add operations. We have adjusted the implementation to make use of the GPU's parallel capabilities by using one thread per element. Additionally, the kernel makes use of C++ templates for easy testing combinations of single-, double-precision and FMA support.

\section{Results and discussion}
In this section, we describe the results of our implementations on the given architectures and provide a comparison and discussion on both precision and power usage.

\subsection{Precision}
Table \ref{tab:precision} lists the mean differences of the single- and double-precision implementations with and without FMA operations on the PPE, SPE and CUDA architectures. Recall that the differences are based on the output data sets (of 1024 samples) of each implementation, compared to the output data set of the arbitrary-precision reference implementation in Matlab.
As explained in the previous sections, the IEEE 754 standard guarantees the behaviour of single- and double-precision floating point formats. This means that in theory, all mean differences of the same floating point format should be equal. Due to violations of the IEEE 754 standard in the case of single-precision SPE, deviated output for these implementations is to be expected.
The table clearly shows equal outputs for every type of implementation across every architecture, except in the case of single-precision SPE (marked in purple).

Needless to say, we are especially interested in the precision differences between the implementations with and without FMA. The table verifies our expectations of increased accuracy when using FMA: our test case shows a significant improvement of $~0.1417e^{-6}$ for single-precision FMA over non-FMA, whereas double-precision yielded a less but still significant improvement of $~0.0022e^{-14}$.
Figure \ref{fig:spprecision} provides a better view of how the single-precision difference varies between samples in the output data set, where $t$ corresponds to the range $[0,1024]$. The deviated SPE results were purposely left out to not skew the range of the results, but are available in a separate figure \ref{fig:spe_spprecision} that clearly show the destructive effect of the SPE's forced round-to-zero mode on the filter accuracy.
Figure \ref{fig:dpprecision} shows a similar behaviour for our double-precision implementations, although the difference is of a significantly smaller order of magnitude.

It is easy to see in both our single- and double-precision results that although FMA has an lower mean difference (or rounding error) and overall higher accuracy than non-FMA, a number of output samples seem to be exceptional in the sense that the non-FMA difference is actually lower than the FMA difference.

\begin{table}\scriptsize
\centering
\caption{Mean differences of PPE, SPE and CUDA architectures compared to arbitrary-precision Matlab output. Deviations from expected output are marked in purple.}
\begin{tabularx}{\linewidth}{lllll}
\toprule
& dp-fma & dp-no-fma & sp-fma & sp-no-fma \\
\midrule
PPE & $1.3986e^{-14}$ & $1.4008e^{-14}$ & $1.3574e^{-6}$ & $1.4991e^{-6}$ \\
SPE & $1.3986e^{-14}$ & $1.4008e^{-14}$ & \color{purple}{$8.6534e^{-6}$} & \color{purple}{$8.6622e^{-6}$} \\
CUDA & $1.3986e^{-14}$ & $1.4008e^{-14}$ & $1.3574e^{-6}$ & $1.4991e^{-6}$ \\
\bottomrule
\end{tabularx}
\label{tab:precision}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{eca1-precision_sp.pdf}
	\caption{Differences of PPE and CUDA architectures using single-precision floating point. Excludes SPE results due to IEEE 754 violation.}\label{fig:spprecision}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{eca1-precision_sp_with_spe.pdf}
	\caption{Differences of PPE, SPE and CUDA architectures using single-precision floating point. SPE results are shown to be more or less equal.}\label{fig:spe_spprecision}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{eca1-precision_dp.pdf}
	\caption{Differences of PPE, SPE and CUDA architectures using double-precision floating point.}\label{fig:dpprecision}
\end{figure}

\subsection{Power usage}
To measure the power usage of the architectures running our implementations, we used a generic socket energy meter. We have to note that while the precision of the energy meter was limited and affected our energy readings, we can still clearly see the differences in power consumption among architectures and chosen precision.

\subsubsection{Cell (PPE and SPE)}
The power usage of our PPE and SPE implementations can be seen in figure \ref{fig:cell_power}. The power usage of our Sony Playstation 3 when it is idle is around 166 W. Given the limited precision of our energy meter, we can see that the fluctuations in power usage per processing element are fairly minimal. The figure shows us that the PPE implementations consume more power compared to the SPE implementations, where we have to note that only one SPE was used. The difference in power consumption can be attributed to the fact that the SPE is a specialized processor with limited functionality, specifically designed for vector processing, whereas the PPE is a general purpose processor.

We would also like to note that there are no visible differences in power consumption between single- and double-precision implementations among the PPE and SPE. This is expected for both architectures.

For the SPE, this is because it processes 128-bit vectors and the difference in the single- and double-precision implementations is the number of elements that are processed simultaneously (four for single-precision and two for double-precision floating point numbers). This means that the choice of precision does not influence the utilization of the SPE per instruction that processes a vector.

In the case of the PPE, this is because it is PowerPC compliant. The PowerPC architecture has 32 64-bit floating point registers and every instruction interprets the contents of these floating point registers as double-precision floating point values. Loads convert single-precision floating point values to their double-precision counterparts and stores perform the opposite conversion. This means that for the PPE also, the choice of precision does not influence the utilization of the FPU.

\begin{figure}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{eca1-power_cell.pdf}
	\caption{Power usage for all our Cell (PPE and SPE) implementations.}\label{fig:cell_power}
	\centering
	\includegraphics[width=0.50\textwidth,keepaspectratio]{eca1-power_cuda.pdf}
	\caption{Power usage for all our CUDA implementations.}\label{fig:cuda_power}
\end{figure}

\subsubsection{CUDA}
Figure \ref{fig:cuda_power} shows the power usage for our CUDA kernels with single- and double-precision and with or without FMA. Due to many factors involved in total power usage when running CUDA kernels, such as CPU utilization by other processes or operating system services, it is difficult to get reliable measurements without the use of expensive tools. For this purpose, it was necessary to disable BIOS power management features such as Intel(R) Speedstep(TM), C-STATE and auto overclocking on our CUDA test system. Note that the idle power usage of our test system (with 0\% CPU utilization) is 200 W on average.

\begin{table}\scriptsize
\centering
\caption{Profiling results of our CUDA implementations.}
\begin{tabularx}{\linewidth}{lllll}
\toprule
& dp-fma & dp-no-fma & sp-fma & sp-no-fma \\
\midrule
CPU utilization & 100\% & 100\% & 100\% & 100\% \\
Achieved occupancy & 41.8\% & 41.3\% & 41.6\% & 41.6\% \\
\#registers & 22 & 21 & 19 & 20 \\
Avg. \#instructions & 2400 & 2756 & 2502 & 2855 \\
Avg. duration & 5280 ns & 6176 ns & 3552 ns & 3648 ns \\
\bottomrule
\end{tabularx}
\label{tab:timings}
\end{table}

The figure shows that both single-precision kernels required more power but also terminated earlier. The average duration of the kernels in table \ref{tab:timings} verifies the fact that single-precision performs faster than double-precision, although the number of instructions are not necessarily lower. This can be explained by looking at the GPU's design, where double-precision naturally imposes a bigger latency and lower throughput in the floating point units. This may explain the difference in power usage between the single- and double-precision kernels.

Results of achieved occupancy and instructions in the table also show a slight potential improvement of occupancy (computational efficiency) and a decrease of instructions when using FMA. The amount of registers seems to vary for no particular reason and is assumed to be unrelated to the use of FMA.

Overall, the power usage graph shows no significant difference between kernels with FMA operations and those without, taking the limited precision of our energy meter into account. This is despite the fact that the usage of FMA decreases the amount of required instructions and overall duration, though this may not necessarily result in a higher power usage as this depends entirely on the internal GPU scheduler being able to schedule more kernels across its multiprocessors.

\section{Conclusion}
In the course of this research, our goal was to research effects of the fused multiply-add instruction on precision and power across different architectures: Cell PPE, Cell SPE and GPU (CUDA). On all our architectures we have implemented a FIR filter (our test case) in both single- and double precision using either FMA operations or regular multiply and add floating point instructions, and compared the output with a reference FIR filter implemented with arbitrary precision operations using Matlab. Additionally, power measurements were performed for each combination of architecture and implementation using an off-the-shelf energy meter.

We have found out that the majority of our architectures, namely the Cell SPE and GPU (CUDA), are not fully compliant with the IEEE 754 single-precision floating point standard due to platform-specific performance (or chip area) versus precision tradeoffs. In the case of the Cell SPE, the output results of our implementation were skewed due to violations of the IEEE 754 rounding mode.

Nevertheless, our results have shown that for both single- and double-precision floating point types, the usage of FMA operations has an overall significant positive effect on the precision of our test case, where the most significant benefits are gained in single-precision implementations. This is explained by the fact that the FMA operation performs the multiply-add step in full precision before rounding the output result, as opposed to the two required rounding steps caused by two seperate multiply and add operations.

However, power measurement across different architectures have not shown a significant difference when using FMA operations. If anything, the use of FMA lowers the instruction count, as only one instruction instead of two is issued, which may lead to higher occupancy or computational efficiency on certain architectures.

We conclude that the use of FMA may prove to be beneficial in the case of floating point computation, as precision as well as computational efficiency has shown to increase without incurring any significant increase in power usage.

\appendix
\section{Matlab implementation}
\label{ap:matlab}
\begin{lstlisting}[breaklines=true,numberstyle=\scriptsize\ttfamily,basicstyle=\scriptsize\ttfamily,captionpos=b,caption={Matlab reference implementation source code with double-precision input and output values and arbitrary-precision filter operations.}]
Fs = 1024; % Sampling frequency
T = 1 / Fs; % Sample time
L = 1024; % Length of signal
t = (1:L) * T; % Time vector

% Setup default precision
mp.Digits( 40 );

% Generate single-precision input signal
dp_signal = double( 100 * sin( 2 * pi * 5 * t ) );
dp_awgn_signal = double( awgn( dp_signal, 30, 'measured' ) );

% Coefficients
coeff = double( [0.2326075560372, 0.0124340773324, 0.01264347488785, 0.01310273791468, 0.01305205931364, 0.01319799229558, 0.01305205931364, 0.01310273791468, 0.01264347488785, 0.0124340773324, 0.2326075560372] );

% Double-precision input, arbitrary-precision filter
dp_output_mp = mp( zeros( 1, length( dp_signal ) ) );
N=length(coeff)
for n=1:( length( dp_signal ) )
  for i=1:uint32(N)
    k = 1+n-i;
    if k > 0
      dp_output_mp(n) = dp_output_mp(n) + dp_awgn_signal(uint32(k)) * coeff(i);
    end
  end
end

% Convert to double-precision output
dp_output_dp = double( dp_output_mp(:) );

% Matlab reference filter
%filter_ref = mp(filter(coeff,1,dp_awgn_signal));
\end{lstlisting}

\section{PPE implementation}
\label{ap:ppe}
\begin{lstlisting}[breaklines=true,numberstyle=\scriptsize\ttfamily,basicstyle=\scriptsize\ttfamily,captionpos=b,caption={PPE implementation source code.}]
#ifdef USE_SINGLE_PRECISION
  float output[ LENGTH ]  __attribute__( ( aligned( 128 ) ) );
#else
  double output[ LENGTH ]  __attribute__( ( aligned( 128 ) ) );
#endif

int main( int argc, char **argv )
{
  int i, j, k;
  for( i = 0; i < LENGTH; ++i )
  {
    output[ i ] = 0.0;
    for( j = 0; j < NUM_COEFFICIENTS; ++j )
    {
      k = 11 + i - j;
      #ifdef USE_FMA
        #ifdef USE_SINGLE_PRECISION
        output[ i ] = __fmadds( ( float )dp_awgn_signal[ k ], ( float )dp_coeff[ j ], output[ i ] );
        #else
        output[ i ] = __fmadd( dp_awgn_signal[ k ], dp_coeff[ j ], output[ i ] );
        #endif
      #else
        #ifdef USE_SINGLE_PRECISION
        output[ i ] += __fmuls( ( float )dp_awgn_signal[ k ], ( float )dp_coeff[ j ] );
        #else
        output[ i ] += __fmul( ( float )dp_awgn_signal[ k ], ( float )dp_coeff[ j ] );
        #endif
      #endif
    }
  }
  /* ... */
  return 0;
}
\end{lstlisting}

\section{SPE implementation}
\label{ap:spe}
\begin{lstlisting}[breaklines=true,numberstyle=\scriptsize\ttfamily,basicstyle=\scriptsize\ttfamily,captionpos=b,caption={SPE implementation PPE control source code.}]
/* The data sent to the pthread */
typedef struct ppu_pthread_data {
  spe_context_ptr_t speid;
  pthread_t pthread;
  void *argp;
} ppu_pthread_data_t;

#ifdef USE_SINGLE_PRECISION
  float output[ LENGTH ] __attribute__ ( ( aligned( 128 ) ) );
#else
  double output[ LENGTH ] __attribute__ ( ( aligned( 128 ) ) );
#endif
com c[ SPUS ] __attribute__ ( ( aligned( 128 ) ) );

/* The function executed in the pthread */
void *ppu_pthread_function( void *arg ) {
  ppu_pthread_data_t *data = ( ppu_pthread_data_t * )arg;
  unsigned int entry = SPE_DEFAULT_ENTRY;
  int retval;
  spe_context_run( data->speid, &entry, 0, data->argp, NULL, NULL );
  pthread_exit( NULL );
}

/* SPU initialization data */
unsigned long long control_block;

/* SPU program handle */
extern spe_program_handle_t spu_basic;
ppu_pthread_data_t data[ 16 ];

int main( int argc, char **argv ) {
  int i, retval, spus;
  spus = SPUS;   
 
  /* Create a context and thread for each SPU */
  for( i = 0; i < spus; ++i) {
    c[ i ].id = i;
    c[ i ].begin = i * BLOCK;
    c[ i ].block = BLOCK;
    c[ i ].output = ( unsigned long long )output;

    /* Create context */
    data[ i ].speid = spe_context_create( 0, NULL );

    /* Load program into the context */
    spe_program_load( data[ i ].speid, &spu_basic );
  
    /* Initialize control block and thread data */
    data[ i ].argp = ( void * )&c[ i ];
   
    /* Create thread */
    pthread_create( &data[ i ].pthread, NULL, &ppu_pthread_function, &data[ i ] );
  }
  /* Wait for the threads to finish processing */
  for( i = 0; i < spus; ++i ) {
    pthread_join( data[ i ].pthread, NULL );
    spe_context_destroy( data[ i ].speid );
  }
  /* ... */
  return 0;
}
\end{lstlisting}

\begin{lstlisting}[breaklines=true,numberstyle=\scriptsize\ttfamily,basicstyle=\scriptsize\ttfamily,captionpos=b,caption={SPE implementation source code.}]
#ifdef USE_SINGLE_PRECISION
// A vector float contains four floats
vector float f_output[ BLOCK / 4 ] __attribute__ ( ( aligned( 128 ) ) );
#else
// A vector double contains two doubles
vector double d_output[ BLOCK / 2 ]  __attribute__ ( ( aligned( 128 ) ) );
#endif
com c __attribute__ ( ( aligned( 128 ) ) );

int main( unsigned long long spe_id, unsigned long long argp, unsigned long long envp ) {
  // Retrieve settings
  spu_write_decrementer( 0 );

  mfc_get( &c, argp, sizeof( com ), TAG, 0, 0 );
  mfc_write_tag_mask( 1 << TAG );
  mfc_read_tag_status_all();

  int x, k;
  #ifdef USE_SINGLE_PRECISION
  vector float array_sine;
  vector float array_coeff[ NUM_COEFFICIENTS ];
  #else
  vector double array_sine;
  vector double array_coeff[ NUM_COEFFICIENTS ];
  #endif

  for( x = 0; x < NUM_COEFFICIENTS; ++x ) {
    #ifdef USE_SINGLE_PRECISION
    array_coeff[ x ] = spu_splats( (float)dp_coeff[ x ] );
    #else
    array_coeff[ x ] = spu_splats( dp_coeff[ x ] );
    #endif
  }
  
  #ifdef USE_SINGLE_PRECISION
  for( x = 0; x < 256; ++x ) {
    int i = NUM_COEFFICIENTS + x * 4;
    f_output[ x ][ 0 ] = 0.0f; f_output[ x ][ 1 ] = 0.0f;
    f_output[ x ][ 2 ] = 0.0f; f_output[ x ][ 3 ] = 0.0f;
    #pragma unroll
    for( k = 0; k < 11; ++k ) {
      array_sine[ 0 ] = ( float )dp_awgn_signal[ -k + i ];
      array_sine[ 1 ] = ( float )dp_awgn_signal[ -k + i + 1 ];
      array_sine[ 2 ] = ( float )dp_awgn_signal[ -k + i + 2 ];
      array_sine[ 3 ] = ( float )dp_awgn_signal[ -k + i + 3 ];
      #ifdef USE_FMA
      f_output[ x ] = spu_madd( array_sine, array_coeff[ k ], f_output[ x ] );
      #else
      f_output[ x ] = spu_add( spu_mul( array_sine, array_coeff[ k ] ), f_output[ x ] );
      #endif
    }
  #else
  for( x = 0; x < 512; ++x ) {
    int i = NUM_COEFFICIENTS + x * 2;
    d_output[ x ][ 0 ] = d_output[ x ][ 1 ] = 0.0;
    #pragma unroll
	for( k = 0; k < 11; ++k ) {
      array_sine[ 0 ] = dp_awgn_signal[ -k + i ];
      array_sine[ 1 ] = dp_awgn_signal[ -k + i + 1 ];
      #ifdef USE_FMA
      d_output[ x ] = spu_madd( array_sine, array_coeff[ k ], d_output[ x ] );
      #else
      d_output[ x ] = spu_add( spu_mul( array_sine, array_coeff[ k ] ), d_output[ x ] );
      #endif
    }
  }
  #endif

  // Write the output back
  #ifdef USE_SINGLE_PRECISION
  mfc_put( f_output, c.output + c.begin * sizeof( float ), BLOCK * sizeof( float ), TAG, 0, 0 );
  #else
  mfc_put( d_output, c.output + c.begin * sizeof( double ), BLOCK * sizeof( double ), TAG, 0, 0 );
  #endif
  mfc_write_tag_mask( 1 << TAG );
  mfc_read_tag_status_all();

  /* ... */

  return 0;
}
\end{lstlisting}

\section{CUDA implementation}
\label{ap:gpu}
\begin{lstlisting}[breaklines=true,numberstyle=\scriptsize\ttfamily,basicstyle=\scriptsize\ttfamily,captionpos=b,caption={CUDA 4.2 implementation source code.}]
template< typename T, bool useSinglePrecision, bool useFMA >
__global__ void FIR( T * results, T * signal, T * coeff, const size_t n_samples )
{
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;

  // discard out-of-bound threads
  if( i >= n_samples ) return;

  // clear output to initial zero
  T output = 0.0;

  for( int j = 0; j < NUM_COEFFICIENTS; ++j ) {
    int k = 11 + i - j;

    // CUDA explicitly requires specification of the rounding mode
    // IEEE 754 describes four rounding modes, of which round-to-nearest is default

    if( useFMA ) {
      if( useSinglePrecision ) {
        // use explicit single-precision FMA operator
        output = __fmaf_rn( signal[ k ], coeff[ j ], output );
      } else {
        // use explicit double-precision FMA operator
        output = __fma_rn( signal[ k ], coeff[ j ], output );
        }
    } else {
      // combinations of * and + operations may be replaced with FMA equivalents by compiler,
      // so we have to use explicit add and mul operations as described by the manual
      if( useSinglePrecision ) {
        output = __fadd_rn( output, __fmul_rn( signal[ k ], coeff[ j ] ) );
      } else {
        output = __dadd_rn( output, __dmul_rn( signal[ k ], coeff[ j ] ) );
      }
    }
  }
  results[ i ] = output;
}
\end{lstlisting}
\end{document}
