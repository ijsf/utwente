function [out,diff] = ec1_parse(filename,in)
work = 'c:\work\dropbox\eca1\assignment\results\';
fc = fopen( strcat(work, filename), 'r' );
out = textscan( fc, '%f' );
out = out{1};
diff = abs(out-in);