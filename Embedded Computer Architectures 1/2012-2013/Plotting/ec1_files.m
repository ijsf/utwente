% Load variables
load 'c:\work\dropbox\eca1\assignment\Variables.mat'

% Load double-precision results
[ppe_dp_fma,ppe_dp_fma_diff] = ec1_parse('ppe_dp_fma.txt',dp_output_mp(:));
[ppe_dp_no_fma,ppe_dp_no_fma_diff] = ec1_parse('ppe_dp_no_fma.txt',dp_output_mp(:));
[spe_dp_fma,spe_dp_fma_diff] = ec1_parse('spe_dp_fma.txt',dp_output_mp(:));
[spe_dp_no_fma,spe_dp_no_fma_diff] = ec1_parse('spe_dp_no_fma.txt',dp_output_mp(:));
[cuda_dp_fma,cuda_dp_fma_diff] = ec1_parse('cuda_raw_dp_fma.txt',dp_output_mp(:));
[cuda_dp_no_fma,cuda_dp_no_fma_diff] = ec1_parse('cuda_raw_dp_no-fma.txt',dp_output_mp(:));

% Load single-precision results
[ppe_sp_fma,ppe_sp_fma_diff] = ec1_parse('ppe_sp_fma.txt',dp_output_mp(:));
[ppe_sp_no_fma,ppe_sp_no_fma_diff] = ec1_parse('ppe_sp_no_fma.txt',dp_output_mp(:));
[spe_sp_fma,spe_sp_fma_diff] = ec1_parse('spe_sp_fma.txt',dp_output_mp(:));
[spe_sp_no_fma,spe_sp_no_fma_diff] = ec1_parse('spe_sp_no_fma.txt',dp_output_mp(:));
[cuda_sp_fma,cuda_sp_fma_diff] = ec1_parse('cuda_raw_sp_fma.txt',dp_output_mp(:));
[cuda_sp_no_fma,cuda_sp_no_fma_diff] = ec1_parse('cuda_raw_sp_no-fma.txt',dp_output_mp(:));

% Differences
%plot(t,ppe_dp_fma_diff,t,ppe_dp_no_fma_diff)
%plot(t,cuda_dp_fma_diff,t,cuda_dp_no_fma_diff)
%plot(t,cuda_dp_fma_diff-ppe_dp_fma_diff)
plot(t,ppe_sp_fma_diff,t,cuda_sp_fma_diff,t,spe_sp_fma_diff)
