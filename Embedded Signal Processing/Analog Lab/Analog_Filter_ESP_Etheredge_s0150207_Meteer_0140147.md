# Embedded Signals Processing #
### Analog Lab (Filter) 2013-2014###
######C.E. Etheredge (s0150207), O. Meteer (s0140147)######
----------
### Problem 1 ###
To lower the requirements of the ADC. The carrier does not add any data to the signal, so we do not need to process it. If we lower the frequency, then the sampling frequency of the ADC can be lower, which consumes less power and adds less clock jitter noise.

### Problem 2 ###
The requirements of the ADC are minimal if the sampling frequency is the just high enough to process signals with a bandwidth of (in this case) 10 MHz. To do this, we need to multiply the center frequency with a oscillator frequency of:

	f_osc = f_cen - (f_band / 2) = 140 - 5 = 135 MHz

This places the lowered signal frequency at 140 - 135 = 5 MHz with a bandwidth of 10 MHz, so the signal occupies the 0 to 10 MHz range. This is within the capabilities of the ADC that has a sampling frequency of 24 MHz (and therefore can process signals up to 12 MHz without aliasing problems).

### Problem 3 ###
Multiplying the original signal with a local oscillator will create two copies with half the amplitude at frequencies f_cen - f_osc and f_cen + f_osc with the same bandwidth. Therefore the highest frequency component present at the mixer output is:

	f_cen + f_osc + (f_band / 2) = 140 + 135 + 5 = 280 MHz

### Problem 4 ###
If a 170 MHz signal is also received, then the following additional frequencies are present:

	f_un_low = f_un_cen - f_osc +- (f_band / 2) = 170 - 135 +- 5 = 30-40 MHz
	f_un_high = f_un_cen + f_osc +- (f_band / 2) = 170 + 135 +- 5 = 300-310 MHz

So the lowest frequency of the unwanted signal present at the mixer output is 30 MHz.

### Problem 5 ###
The filter order should be as high as possible within practical limits and given that the higher order filters are still stable, because a higher order filter will have a steeper slope which means that the filter performs better. In the ideal case and with an ideal (low-pass) filter, the corner frequency would be 10 MHz, and all frequencies between 0 and 10 MHz would have unity gain, and all frequencies higher than 10 MHz would have be completely attenuated.

### Problem 6 ###
We have chosen a corner frequency of `10 MHz` and the following values for the components:

- C1 = C2 = C = `1pF`
- R1 => `10 * 1e6 = 1 / ( 2 * pi * R1 * 1e-12 ) => R1 = 15915.5 Ω`
- R2 = `100 * R1 = 1591559 Ω`
- L = `2 * R1^2 * C = 2 * (15915.5)^2 * 1e-12 = 5.0660e-4 H`

We check the response of the filter in LTSpice in the next problem to see if the attenuation in the passband is smaller than 0.5 dB.

### Problem 7 ###
We have built the filter in LTSpice with the values found in the previous problem, and have a run an AC sweep analysis. The result is the following plot:

#### `plot`
![](assignment2/plots/ltspice_1.png)

We can also confirm that there is no attenuation at all in the passband.

### Problem 8 ###
We have determined the state equations by first finding the equations for the memory elements of the circuit (in this case C1, C2, and L):

![](assignment2/plots/state_equations_1.png)

Since we want to implement this circuit using the gm-c method, we need to express everything in voltages and therefore we need to determine the voltage of the inductor:

![](assignment2/plots/state_equations_2.png)

We want to express the state equations in terms of resistors and capacitors, and therefore we have to rewrite the equation for the voltage of the inductor:

![](assignment2/plots/state_equations_3.png)

In the next problem we have to build the filter using the gm-C structure, so we have to rewrite the state equations in terms of transconductance:

![](assignment2/plots/state_equations_4.png)

The values of C_int and the gms can be found in the schematic of the next problem.

### Problem 9 ###
We have built the filter using the gm-C structure and the schematic can be found below.

#### `circuit`
![](assignment2/plots/ltspice_2_schematic.png)

### Problem 10 ###
We have simulated the gm-c circuit and have verified that the response is the same as the first filter.

#### `frequency response plot`
![](assignment2/plots/ltspice_2_plot.png)

### Problem 11 ###
We changed the values of C1, C2, R1, and R2 to make sure that they meet the demands:

	C_1 = C_2 = 400 fF
	C_int = L / (R_1^2)
	R_1 = 39788,75 Ohms
	R_2 = 100 * R1

The frequency respons plot below shows that it has not changed compared to the previous plots.

#### `circuit`
![](assignment2/plots/ltspice_3_schematic.png)

#### `frequency respons plot`
![](assignment2/plots/ltspice_3_plot.png)

### Problem 12 ###
In our current gm design, we have three "positive gm" current sources, one of which is at the V_in input node. Since V_in is an AC signal, we can safely invert the gm of this particular current source without changing the frequency response (as only the phase is changed).

Since the current sources that we have used, as provided by LTspice, are four-port current sources with both positive and negative voltage and current ports, one of the easiest changes is simply to swap the positive and negative voltage inputs of all the "positive gm" current sources, and inverting the gm values. This example is given in below:

![](assignment2/plots/ltspice_4_schematic.png)

We also realize this may result in a circuit that is impossible to recreate in a chip, so an alternative modification would be adding extra circuitry to the "positive gm" current source voltage inputs.

First, we turn the gm in question (gm_x) into a "negative gm". At the input of gm_x, we add another "negative gm" current source (gm_z) in series with a resistor (R_z) to create the negative voltage that is needed to generate the positive voltage at the output of gm_x.

	R_z: V_z = R_z * i_xz
	gm_x: i_xz = -gm * V_in
	gm_z: i_out = -gm * V_z = -gm * (R_z * i_xz) = -gm * R_z * -gm * V_in

If we take R_z = 1/gm, then:

	i_out = -gm * 1/gm * -gm * V_in = gm * V_in

The "positive gm" is thus eliminated, as can be seen in circuit below:

#### `circuit`
![](assignment2/plots/ltspice_5_schematic.png)

#### `frequency response plot`
![](assignment2/plots/ltspice_5_plot.png)

### Problem 13 ###
We used the state equations to construct the filter using the R-C method. The amplification of the opamps was set to 1e5 and we set the GBW to 1e9.

#### `circuit`
![](assignment2/plots/ltspice_6_schematic.png)

#### `frequency responss plot`
![](assignment2/plots/ltspice_6_plot.png)

The frequency response is the same as those of the previous filter circuits.

### Problem 14 ###
We changed the values to meet the demands for the resistor and capacitor values:

	R_1 = 100 kOhm
	R_2 = 100 * R_1 = 10 MOhm
	C_1 = C_2 = 159.15 fF
	L = 3.183 mH
	C_int = 318.3 fF

The frequency response remains the same as was expected.

#### `circuit`
![](assignment2/plots/ltspice_7_schematic.png)

#### `frequency response plot`
![](assignment2/plots/ltspice_7_plot.png)