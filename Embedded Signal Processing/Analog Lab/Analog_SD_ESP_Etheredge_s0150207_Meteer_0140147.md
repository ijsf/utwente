# Embedded Signals Processing #
### Analog Lab (Sigma Delta) 2013-2014###
######C.E. Etheredge (s0150207), O. Meteer (s0140147)######
----------

### Problem 1.1 ###
Because the input signal is a wideband signal, every frequency withing the given bandwidth (in this case 15 MHz) is present in the signal. This means that the quantization error between -1 and 1 is uniformly distributed, which means that the **average** error at any given time is **zero**. The RMS error however is given as:

![](assignment1/plots/e_rms_1.png)

### Problem 1.2 ###
This time the quantizer has 10 bits, and the input signal is a sinusoidal signal. Given that the input signal is a sine wave, there are an equal amount of equal valued positive and negative errors. Because each error value is equally likely to occur (meaning that the average error is again zero), we can use the same formula to calculate the RMS error:

![](assignment1/plots/e_rms_2.png)

### Problem 1.3 ###
We have the same 10-bit quantizer, but now the input signal is a wideband signal. The same rules as in `Problem 1.1` apply, and therefore we can use the same formula again to calculate the RMS error. The result is the same as in `Problem 1.2`.

### Problem 2 ###
The requirements are that the signal-to-noise ratio (SNR) is 60 dB. The input signal has an amplitude of 1, so the RMS power of the input signal is:

![](assignment1/plots/signal_power.png)

With an SNR value of 60 dB, we can calculate the maximum allowed noise power:

![](assignment1/plots/SNR_formula.png)

The formula to calculate the noise power and used definitions are given as:

![](assignment1/plots/noise_formula.png)

With these values, we can calculate the sampling frequency needed to achieve the required SNR of 60 dB for a given loop filter order `N`. We first have to rewrite the noise power formula given the values and definitions above:

![](assignment1/plots/frequency_formula.png)

Using the rewritten noise power formula above, we can now calculate the required sampling frequencies for the first and second order filters:

![](assignment1/plots/order_frequencies.png)

Because the required sampling frequency is much lower for the second order loop filter, it is also the preferred one.

### Problem 3 ###
We have chosen to use a second order loop filter with a sampling frequency of `794.015.353 Hz`, and have used this value in the provided Simulink model. The chosen amplitude was 0.8 to be on the safe side, and with these values and the scope tool, we have verified that the Sigma Delta ADC is working correctly. Also, we did not change the simulation time of 2 microseconds, and used the same value for the next problem.

### Problem 4 ###
To find the `SINAD`, we wrote a script that calculates the power of the FFT of the second order Sigma Delta ADC output. We summed all power values between 0 and 15 MHz to obtain the total power, and removed the values belonging to the signal. The difference gives us the total noise and distortion. We can then use the noise and distortion, and the signal power to find the SINAD. Technically, this is not entirely correct as the signal power also contains some noise. To remedy this we would need to interpolate between the values on the edges of the signal peak, but we did not do this.

In the end, the obtained `SINAD` was 63.4 dB.

#### `code`
	
	sim( 'SigmaDelta_Simulink_Model' );

	num_samples = length( SD_2 );                           % Number of samples
	k_w = kaiser( num_samples, 20 );                        % Kaiser window with beta of 20
	power_values = abs( fft( SD_2 .* k_w ) ).^2;            % Calculate the power values of the FFT
	                                                        % after applying the Kaiser window.
	power_values = power_values / num_samples;              % Normalize the values.
	power_values = power_values( 1:(num_samples - 1)/ 2 );
	
	f_s = 794015353;                                        % Sampling frequency
	bandwidth = 15e6;                                       % Signal bandwidth
	FFT_step = f_s / num_samples;                           % Frequency step size per FFT sample
	range = 1:(bandwidth/FFT_step);                         % FFT sample range for 0..15 Mhz frequency range
	l_sample = range( length( range ) );                    % Index of the last sample in the frequency range
	f_range = 0:FFT_step:(num_samples / 2 - 1) * FFT_step;  % Frequency range of the FFT
	
	% Plot the FFT.
	semilogx( f_range, 10 * log10( power_values ) );
	axis tight; title( 'Spectral power' );
	xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );
	
	sig_sam_beg = 14;   % Begin sample of the signal.
	sig_sam_end = 28;   % End sample of the signal.
	
	% Find the total power.
	total_power = 0;
	
	for i = 1:l_sample/2
	   total_power = total_power + power_values( i ); 
	end
	
	% Find the signal power.
	signal_power = 0;
	for i = sig_sam_beg:sig_sam_end
	    signal_power = signal_power + power_values( i );
	end
	
	% Noise and distortion power is total power minus signal power.
	noise_dist_power = total_power - signal_power;
	
	% Calculate the SINAD.
	SINAD = 10 * log10( signal_power / noise_dist_power );

#### `plot`
![](assignment1/plots/spectral_plot.png)