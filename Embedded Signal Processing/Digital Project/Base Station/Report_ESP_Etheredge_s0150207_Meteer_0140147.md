# Embedded Signals Processing #
###Digital project 2 (2013-2014)###
######C.E. Etheredge (s0150207), O. Meteer (s0140147)######
----------

## Task 1
Using a modified version of the SigmaDelta simulink model we were given for the lab session, we generate an input signal consisting of three sine waves with frequencies that fall in each sub-band. In this assignment, the second order SD converter will be used.

#### `Modified simulink model`
![](plots/simulink_model.png)

The chosen frequencies and amplitudes are:

 * 3.3 MHz - 0.3
 * 6.6 MHz - 0.3
 * 11 MHz - 0.27

The frequencies are chosen in such a way that one of the signals (11 MHz) does not suffer from harmonic and intermodulation distortion products, whereas the others do. We calculated the sampling frequency in the same way as in the analog labs for an SNR of 60 dB with an amplitude of 0.8, which is around 868 MHz, but for this assignment we use **868 MHz**.

Since the SD converter does not function correctly when the amplitude of the input signal is close to or over 1, the amplitudes of the three sine waves were chosen such that the maximum amplitude of the input signal is around 0.8 (min=**-0.7855**, max=**0.7855** to be precise).

#### `code`
	f_1 = 3.3e6;        % Frequency signal 1
	f_2 = 6.6e6;        % Frequency signal 2
	f_3 = 11.0e6;       % Frequency signal 3
	
	a_1 = 0.30;         % Amplitude signal 1
	a_2 = 0.30;         % Amplitude signal 2
	a_3 = 0.27;         % Amplitude signal 3
	
	f_s = 868e6;        % Sampling frequency SD
	bw  = 15e6;         % Input signal bandwidth
	
	% Load the SigmaDelta model.
	load_system( 'SigmaDelta_Simulink_Model' );
	
	% Set model values.
	set_param( 'SigmaDelta_Simulink_Model/input 1', 'Frequency', num2str( f_1 ) );
	set_param( 'SigmaDelta_Simulink_Model/input 1', 'Amplitude', num2str( a_1 ) );
	set_param( 'SigmaDelta_Simulink_Model/input 2', 'Frequency', num2str( f_2 ) );
	set_param( 'SigmaDelta_Simulink_Model/input 2', 'Amplitude', num2str( a_2 ) );
	set_param( 'SigmaDelta_Simulink_Model/input 3', 'Frequency', num2str( f_3 ) );
	set_param( 'SigmaDelta_Simulink_Model/input 3', 'Amplitude', num2str( a_3 ) );
	set_param( 'SigmaDelta_Simulink_Model/ZOH', 'SampleTime', num2str( 1 / f_s ) );
	
	% Simulate model
	sim( 'SigmaDelta_Simulink_Model' );
	
	% Plot fft of the second order output SD converter.
	L = length( SD_2 );                                     % Number of samples
	FFT_step = f_s / L;                                     % Frequency step size per FFT sample
	range = 1 : ( bw / FFT_step ) + 1;                      % FFT sample range for 0..15 Mhz frequency range
	l_sample = range( length( range ) );                    % Index of the last sample in the frequency range
	f_range = 0 : FFT_step : ( ( L / 2 ) - 1 ) * FFT_step;  % Frequency range of the FFT
	kw = kaiser( L, 5 );                                    % Kaiser window
	
	a_val = abs( fft( SD_2 .* kw ) ) / ( 0.5 * L );
	a_val = a_val( 1 : round( ( ( L - 1 ) / 2 ) ) );
	
	% Plot the entire range.
	figure; semilogy( f_range, a_val ); axis tight; 
	title( 'Sigma Delta converter output' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Amplitude' );
	
	% Plot the 0..15 MHz range.
	figure; semilogy( f_range( range ), a_val( range ) ); axis tight; 
	title( 'Sigma Delta converter output' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Amplitude' );
	
	% Calculate the power values.
	p_val = ( abs( fft( SD_2 ) / ( L / f_s ) ) ) .^ 2;
	p_val = p_val( 1 : round( ( L - 1 ) / 2 ) );
	
	% Handpicked samples representing the three sine waves of the signal.
	sig1_beg = 60; sig1_end = 73;
	sig2_beg = 127; sig2_end = 140;
	sig3_beg = 214; sig3_end = 229;
	
	% Find the total power.
	total_power = sum( p_val( 1 : l_sample ) * FFT_step );
	
	% Find the signal power.
	signal_power = sum( p_val( sig1_beg : sig1_end ) * FFT_step );
	signal_power = signal_power + sum( p_val( sig2_beg : sig2_end ) * FFT_step );
	signal_power = signal_power + sum( p_val( sig3_beg : sig3_end ) * FFT_step );
	
	% Noise and distortion power is total power minus signal power.
	noise_dist_power = total_power - signal_power;
	
	% Calculate the SINAD.
	SINAD = abs( 10 * log10( signal_power / noise_dist_power ) );
	
	% Calculate the ENOB.
	ENOB = abs( ( SINAD - 1.76 ) / 6.02 );
	
	% Plot the power spectrum density.
	semilogx( f_range, 10 * log10( p_val ) ); axis tight; title( 'Power spectrum density' ); 
	xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );

#### `SD output plot (full range)`
![](plots/SD_output_full_range.png)

#### `SD output plot (0..15 MHz)`
![](plots/SD_output.png)

In the zoomed in plot, we can see three large spikes corresponding to the frequencies of the three input signals. There are also several peaks that occur due to harmonic and intermodulation distortion. Below, we list a few of the frequencies and the types of distortion:

 * 4.4 MHz (11 - 6.6 and 11 - 3.3 - 3.3, intermodulation)
 * 7.7 MHz (11 - 3.3, intermodulation)
 * 8.8 MHz (6.6 + 6.6 + 6.6 - 11, intermodulation)
 * 9.9 MHz (3.3 * 3 and 3.3 + 6.6, harmonic and intermodulation)
 * 13.2 MHz (3.3 * 4, 6.6 * 2, and 3.3 + 3.3 + 6.6, harmonic and intermodulation)
 * 14.3 MHz (11 + 3.3, intermodulation)

The simulink model is run for 20 microseconds which, with a sampling frequency of 868 MHz, generates 17360 samples. This gives a frequency resolution of `868*10^6 / 17360` = **50 kHz**. The smallest significant frequency "delta" that can be generated by the two distortion types is 100 kHz, and therefore it is also the lowest frequency resolution needed to be able to see these distortion products in an FFT plot. Therefore our 50 kHz frequency resolution is therefore high enough.

The SINAD is calculated by summing the total power and signal powers in the wanted frequency range, and then subtracting the signal powers from the total power. This method is not entirely correct as the signal power also contains noise, and a better way would be to interpolate between the edges of the signal power peaks, but we did not do this for simplicity's sake.

The calculated SINAD is 50.08, which gives an effective number of bits (ENOB) of 8 bits.

## Task 2
We have chosen to a Chebyshev Direct Form 2 lowpass IIR filter. FIR filters in general need to be of a higher order compared to IIR filters given the same requirements, so an IIR filter is "cheaper" to implement and has our preference.
Since there is a 0.5 MHz "buffer" on each side of each sub-band, we set the passband frequency to 14.5 MHz and the stopband frequency to 15 MHz. The passband ripple and stopband attenuation are 0.2 dB and 70 dB as given requested in this task.

#### `code`
	f_lp_pass = bw - 0.5e6;  % Passband frequency lowpass filter in Hz.
	f_lp_stop = bw;          % Stopband frequency lowpass filter in Hz.
	pb_ripple = 0.2;         % Passband ripple in dB.
	sb_atten  = 70;          % Stopband attenuation in dB.
	
	% Create a lowpass filter for the 0..15 MHz range.
	H_lp = design( fdesign.lowpass( f_lp_pass, ...
	                                f_lp_stop, ...
	                                pb_ripple, ...
	                                sb_atten, ...
	                                f_s ), ...
	               'cheby2', ...
	               'MatchExactly', ...
	               'stopband' );
	           
	% Filter the signal with a lowpass filter.
	SD_2_lp = filter( H_lp, SD_2 );
	kw_lp = kaiser( length( SD_2_lp ), 5 );
	a_val_lp_filt = abs( fft( SD_2_lp .* kw_lp ) ) / ( 0.5 * L );
	a_val_lp_filt = 20 * log10( a_val_lp_filt( 1 : round( ( L / 2 ) - 1 ) ) );
	
	% Calculate the frequency response of the lowpass filter.
	[f_resp_lp, f_range] = freqz( H_lp, round( ( L * 0.5 ) - 1 ), f_s );
	f_resp_lp = 20 * log10( abs( f_resp_lp ) );
	
	% Plot the frequency response of the lowpass filter (full range).
	figure; plot( f_range, f_resp_lp ); axis tight;
	title( 'Frequency response lowpass filter' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
	
	% Plot the frequency response of the lowpass filter (0..15 MHz)
	figure; plot( f_range( range ), f_resp_lp( range ) ); axis tight;
	title( 'Frequency response lowpass filter' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
	
	% Plot the lowpass filtered input signal (full range);
	figure; plot( f_range, a_val_lp_filt ); axis tight;
	title( 'Lowpass filtered input signal' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
	
	% Plot the lowpass filtered input signal (0..15 MHz);
	figure; plot( f_range( range ), a_val_lp_filt( range ) ); axis tight;
	title( 'Lowpass filtered input signal' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );


#### `frequency response plot (zoomed in)`
![](plots/f_resp_lp.png)

#### `plot lowpass filtered input signal (full range)`
![](plots/lowpass_filtered_signal_full_range.png)

#### `plot lowpass filtered input signal (zoomed in)`
![](plots/lowpass_filtered_signal.png)

In the plots, we only show the parts that are relevant. We can see from the plots that the lowpass filter mostly works as intended, as it filters away all noise above 15 MHz. However, there is some increasing noise from around 7.5 MHz to 15 MHz, most likely due to the characteristics of the filter.

## Task 3
In the same way as in task 2, we chose to use Chebyshev Direct Form 2 IIR bandpass filters for the same reason. The passbands and stopbands are chosen with the 0.5 MHz buffer in mind.

#### `code`
	match = 'stopband';     % Filters can match "passband" or "stopband".
	
	% Filter values for the 0..5 MHz bandpass filter called "low".
	f_bp_stop_1_low = 0.1;      % First stopband frequency in Hz.
	f_bp_pass_1_low = 0.5e6;    % First passband frequency in Hz.
	f_bp_pass_2_low = 4.5e6;    % Second passband frequency in Hz.
	f_bp_stop_2_low = 5.0e6;    % Second stopband frequency in Hz.
	
	% Create a bandpass filter for the 0..5 MHz range.
	H_bp_low = design( fdesign.bandpass( f_bp_stop_1_low, ...
	                                     f_bp_pass_1_low, ...
	                                     f_bp_pass_2_low, ...
	                                     f_bp_stop_2_low, ...
	                                     sb_atten, ...
	                                     pb_ripple, ...
	                                     sb_atten, ...
	                                     f_s ), ...
	                   'cheby2', ...
	                   'MatchExactly', ...
	                   match );
	                 
	% Filter the lowpass filtered signal with the "low" bandpass filter.
	SD_bp_low = filter( H_bp_low, SD_2_lp );
	kw_l = kaiser( length( SD_bp_low ), 5 );
	a_val_bp_low = abs( fft( SD_bp_low.* kw_l ) ) / ( 0.5 * L );
	a_val_bp_low = a_val_bp_low( 1 : round( ( L / 2 ) - 1 ) );
	a_val_bp_low = 20 * log10( a_val_bp_low );
	
	% Filter values for the 5..10 MHz bandpass filter called "med".
	f_bp_stop_1_med = 5.0e6;    % First stopband frequency in Hz.
	f_bp_pass_1_med = 5.5e6;    % First passband frequency in Hz.
	f_bp_pass_2_med = 9.5e6;    % Second passband frequency in Hz.
	f_bp_stop_2_med = 10e6;     % Second stopband frequency in Hz.
	
	% Create a bandpass filter for the 5..10 MHz range.
	H_bp_med = design( fdesign.bandpass( f_bp_stop_1_med, ...
	                                     f_bp_pass_1_med, ...
	                                     f_bp_pass_2_med, ...
	                                     f_bp_stop_2_med, ...
	                                     sb_atten, ...
	                                     pb_ripple, ...
	                                     sb_atten, ...
	                                     f_s ), ...
	                   'cheby2', ...
	                   'MatchExactly', ...
	                   match );
	                 
	% Filter the lowpass filtered signal with the "med" bandpass filter.
	SD_bp_med = filter( H_bp_med, SD_2_lp );
	kw_m = kaiser( length( SD_bp_med ), 5 );
	a_val_bp_med = abs( fft( SD_bp_med .* kw_m ) ) / ( 0.5 * L );
	a_val_bp_med = a_val_bp_med( 1 : round( ( L / 2 ) - 1 ) );
	a_val_bp_med = 20 * log10( a_val_bp_med );
	
	% Filter values for the 10..15 MHz bandpass filter called "high".
	f_bp_stop_1_high = 10.0e6;    % First stopband frequency in Hz.
	f_bp_pass_1_high = 10.5e6;    % First passband frequency in Hz.
	f_bp_pass_2_high = 14.5e6;    % Second passband frequency in Hz.
	f_bp_stop_2_high = 15.0e6;    % Second stopband frequency in Hz.
	
	% Create a bandpass filter for the 10..15 MHz range.
	H_bp_high = design( fdesign.bandpass( f_bp_stop_1_high, ...
	                                      f_bp_pass_1_high, ...
	                                      f_bp_pass_2_high, ...
	                                      f_bp_stop_2_high, ...
	                                      sb_atten, ...
	                                      pb_ripple, ...
	                                      sb_atten, ...
	                                      f_s ), ...
	                    'cheby2', ...
	                    'MatchExactly', ...
	                    match );
	
	% Filter the lowpass filtered signal with the "high" bandpass filter.
	SD_bp_high = filter( H_bp_high, SD_2_lp );
	kw_h = kaiser( length( SD_bp_high ), 5 );
	a_val_bp_high = abs( fft( SD_bp_high .* kw_h ) ) / ( 0.5 * L );
	a_val_bp_high = a_val_bp_high( 1 : round( ( L / 2 ) - 1 ) );
	a_val_bp_high = 20 * log10( a_val_bp_high );
	
	% Calculate the frequency response of the bandpass filters.
	[f_resp_low, f_range] = freqz( H_bp_low, round( ( L * 0.5 ) - 1 ), f_s );
	f_resp_low = 20 * log10( abs( f_resp_low ) );
	[f_resp_med, f_range] = freqz( H_bp_med, round( ( L * 0.5 ) - 1 ), f_s );
	f_resp_med = 20 * log10( abs( f_resp_med ) );
	[f_resp_high, f_range] = freqz( H_bp_high, round( ( L * 0.5 ) - 1 ), f_s );
	f_resp_high = 20 * log10( abs( f_resp_high ) );
	
	% Plot the frequency response of the three filters in one plot.
	figure;
	plot( f_range( range ), f_resp_low( range ), 'r' ); axis tight; hold on;
	plot( f_range( range ), f_resp_med( range ), 'b' ); axis tight; hold on;
	plot( f_range( range ), f_resp_high( range ), 'm' ); axis tight; hold on;
	title( 'Frequency response bandpass filters' ); xlabel( 'Frequency (Hz)' );
	ylabel( 'Magnitude (dB)' ); legend( 'low', 'med', 'high' );
	
	% Plot the three filtered signals in one plot (full range).
	figure;
	plot( f_range, a_val_bp_low, 'r' ); axis tight; hold on;
	plot( f_range, a_val_bp_med, 'k' ); axis tight; hold on;
	plot( f_range, a_val_bp_high, 'm' ); axis tight; hold on;
	title( 'Bandpass filtered signals' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
	legend( 'Signal 1 "low"', 'Signal 2 "med"', 'Signal 3 "high"' );
	
	% Plot the three filtered signals in one plot (0..15 MHz).
	figure;
	plot( f_range( range ), a_val_bp_low( range ), 'r' ); axis tight; hold on;
	plot( f_range( range ), a_val_bp_med( range ), 'k' ); axis tight; hold on;
	plot( f_range( range ), a_val_bp_high( range ), 'm' ); axis tight; hold on;
	title( 'Bandpass filtered signals' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
	legend( 'Signal 1 "low"', 'Signal 2 "med"', 'Signal 3 "high"' );

#### `frequency response plot (zoomed in)`
![](plots/f_resp_bp.png)

#### `plot bandpass filtered signals (full range)`
![](plots/bandpass_filtered_signals_full_range.png)

#### `plot bandpass filtered signals (zoomed in)`
![](plots/bandpass_filtered_signals.png)

From the frequency response plot, we can see that the three filters behave correctly and that they do not overlap with each other (other than the 70 dB stopband rejection). This is also visible when viewing the bandpass filtered signals where each bandpass filter correctly filters the signals it is supposed to filter.

## Task 4
Processing data at a lower frequency has several benefits:

 * Less processing power needed.
 * Less memory needed.
 * Lower power usage.

When a signal is downsampled, it is the effectively the same as having the original signal sampled with a lower sampling frequency. Therefore we have to make sure that the downsampling factor M is chosen in such a way that the effective sampling frequency after downsampling is at most the new Nyquist frequency. For example, if we downsample the "low" bandpass filtered signal that has a bandwidth of 5 MHz, the maximum factor M is `f_s / (2*5) = 86.8 => M = 86`. If we choose a higher factor, then aliasing will occur due to the violation of the Nyquist criterion. 

Using this method, the three bandpass filtered signals were downsampled by the maximum allowable factor per subband. Since the "med" and "high" signals have a bandwidth of 10 MHz and 15 MHz respectively (due to them not being frequency shifted to the 0..5 MHz range), their factors are different. For the "med" signal it is `f_s / (2*10) = 43.4 => M = 43` and for the "high" signal it is `f_s / (2*15) ~= 28.93 => M = 28`. 

#### `code`
	sim_time = 2e-5;                                            % Simulation time
	t_range = ( ( 0 : L - 1 ) / L ) * sim_time;                 % Time range
	
	dsr_l = 86;                                                 % Downsample ratio "low"
	dsr_m = 43;                                                 % Downsample ratio "med"
	dsr_h = 28;                                                 % Downsample ratio "high"
	
	dt_range_low = ( ( 0 : dsr_l : L - 1 ) / L ) * sim_time;    % Downsampled time range "low"
	dt_range_med = ( ( 0 : dsr_m : L - 1 ) / L ) * sim_time;    % Downsampled time range "med"
	dt_range_high = ( ( 0 : dsr_h : L - 1 ) / L ) * sim_time;   % Downsampled time range "high"
	
	% Downsample the output of the "low" filter.
	SD_bp_low_ds = downsample( SD_bp_low, dsr_l );
	Ll = length( SD_bp_low_ds ) - 1;
	kw_l = kaiser( Ll + 1, 5 );
	a_val_bp_low_ds = abs( fft( SD_bp_low_ds .* kw_l ) ) / ( 0.5 * Ll );
	a_val_bp_low_ds = a_val_bp_low_ds( 1 : round( ( Ll / 2 ) - 1 ) );
	a_val_bp_low_ds = 20 * log10( a_val_bp_low_ds );
	
	% Downsample the output of the "med" filter.
	SD_bp_med_ds = downsample( SD_bp_med, dsr_m );
	Lm = length( SD_bp_med_ds ) - 1;
	kw_m = kaiser( Lm + 1, 5 );
	a_val_bp_med_ds = abs( fft( SD_bp_med_ds .* kw_m ) ) / ( 0.5 * Lm );
	a_val_bp_med_ds = a_val_bp_med_ds( 1 : round( ( Lm / 2 ) - 1 ) );
	a_val_bp_med_ds = 20 * log10( a_val_bp_med_ds );
	
	% Downsample the output of the "high" filter.
	SD_bp_high_ds = downsample( SD_bp_high, dsr_h );
	Lh = length( SD_bp_high_ds ) - 1;
	kw_h = kaiser( Lh + 1, 5 );
	a_val_bp_high_ds = abs( fft( SD_bp_high_ds .* kw_h ) ) / ( 0.5 * Lh );
	a_val_bp_high_ds = a_val_bp_high_ds( 1 : round( ( Lh / 2 ) - 1 ) );
	a_val_bp_high_ds = 20 * log10( a_val_bp_high_ds );
	
	% Plot downsampled "low" sine wave.
	figure; plot( t_range, SD_bp_low, 'r' ); hold on; axis tight;
	plot( dt_range_low, SD_bp_low_ds, 'b' );
	title( '"low" sine wave: original vs downsampled' ); xlabel( 'Time (s)' ); ylabel( 'Amplitude' );
	legend( 'Original', strcat( 'Downsampled: ', int2str( dsr_l ), 'x' ) );
	
	% Plot downsampled "med" sine wave.
	figure; plot( t_range, SD_bp_med, 'r' ); hold on; axis tight;
	plot( dt_range_med, SD_bp_med_ds, 'b' );
	title( '"med" sine wave: original vs downsampled' ); xlabel( 'Time (s)' ); ylabel( 'Amplitude' );
	legend( 'Original', strcat( 'Downsampled: ', int2str( dsr_m ), 'x' ) );
	
	% Plot downsampled "high" sine wave.
	figure; plot( t_range, SD_bp_high, 'r' ); hold on; axis tight;
	plot( dt_range_high, SD_bp_high_ds, 'b' );
	title( '"high" sine wave: original vs downsampled' ); xlabel( 'Time (s)' ); ylabel( 'Amplitude' );
	legend( 'Original', strcat( 'Downsampled: ', int2str( dsr_h ), 'x' ) );
	
	f_range = 0 : FFT_step : ( ( ( L - 1 ) / 2 ) - 1 ) * FFT_step;  % Frequency range of the FFT
	
	% Calculate the correct ranges for the downsampled signals.
	FFT_step_l = ( f_s / dsr_l ) / Ll;
	FFT_step_m = ( f_s / dsr_m ) / Lm;
	FFT_step_h = ( f_s / dsr_h ) / Lh;
	df_range_low = ( 0 : FFT_step_l : round( ( Ll / 2 ) - 1 ) * FFT_step_l );
	df_range_med = ( 0 : FFT_step_m : round( ( Lm / 2 ) - 1 ) * FFT_step_m );
	df_range_high = ( 0 : FFT_step_h : round( ( Lh / 2 ) - 1 ) * FFT_step_h );
	range_l = 1 : length( df_range_low ) - 1;
	range_m = 1 : length( df_range_med ) - 1;
	range_h = 1 : length( df_range_high ) - 1;
	
	% Plot the original and downsampled filtered signals in one graph.
	figure;
	plot( f_range, a_val_bp_low, 'r' ); axis tight; hold on;
	plot( f_range, a_val_bp_med, 'g' ); axis tight; hold on;
	plot( f_range, a_val_bp_high, 'b' ); axis tight; hold on;
	plot( df_range_low( range_l ), a_val_bp_low_ds, 'y' ); axis tight; hold on;
	plot( df_range_med( range_m ), a_val_bp_med_ds, 'black' ); axis tight; hold on;
	plot( df_range_high( range_h ), a_val_bp_high_ds, 'm' ); axis tight; hold on;
	title( 'Filtered signals: original vs downsampled' ); xlabel( 'Frequency (Hz)' );
	ylabel( 'Amplitude' ); legend( 'low - original', 'med - original', 'high - original', ...
	                               'low - downsampled', 'med - downsampled', 'high - downsampled' );

#### `plot "low" signal: original vs downsampled (M=86)`
![](plots/ds_low_vs_orig.png)

#### `plot "med" signal: original vs downsampled (M=43)`
![](plots/ds_med_vs_orig.png)

#### `plot "high" signal: original vs downsampled (M=28)`
![](plots/ds_high_vs_orig.png)

#### `plot original vs downsampled filtered signals`
![](plots/bandpass_filtered_downsampled_signals.png)

From the three time plots we can see that although the downsampled signals are much less dense and detailed, they still follow the "shape" of the original signals. This can also be verified by looking at the frequency spectrum of the original vs downsampled, filtered signals as the downsampled signals more or less look the same as the original filtered signals. We have verified that with lower M factors, the downsampled signals increasing resemble the original signals, and that when increasing the M factors any more, aliasing occurs. This proves that we have chosen the maximum M factors per signal.

## Task 5

### 5.1
We have calculated the SINAD in the same way as in the first task, and so the same limitations apply. As a comparison, we have also calcuated the SINAD with the `sinad` function in Matlab.

#### `code`
	kw_l = kaiser( Ll + 1, 5 );
	kw_m = kaiser( Lm + 1, 5 );
	kw_h = kaiser( Lh + 1, 5 );
	
	% Handpicked samples representing the three sine waves of the signal.
	sig1_beg_ds = 65; sig1_end_ds = 69;
	sig2_beg_ds = 131; sig2_end_ds = 135;
	sig3_beg_ds = 216; sig3_end_ds = 225;
	
	% Calculate the powers.
	p_val_low = ( abs( fft( SD_bp_low_ds .* kw_l ) * ( Ll / ( Ll * FFT_step_l ) ) ) ) .^ 2;
	p_val_low = p_val_low( 1 : round( Ll / 2 ) );
	p_val_med = ( abs( fft( SD_bp_med_ds .* kw_m ) / ( Lm / ( Lm * FFT_step_m ) ) ) ) .^ 2;
	p_val_med = p_val_med( 1 : round( Lm / 2 ) );
	p_val_high = ( abs( fft( SD_bp_high_ds .* kw_h ) / ( Lh / ( Lh * FFT_step_h ) ) ) ) .^ 2;
	p_val_high = p_val_high( 1 : round( Lh / 2 ) );
	
	% Find the total power.
	total_power_low = sum( p_val_low * FFT_step_l );
	total_power_med = sum( p_val_med * FFT_step_m );
	total_power_high = sum( p_val_high * FFT_step_h );
	
	% Find the signal power.
	signal_power_low = sum( p_val_low( sig1_beg_ds : sig1_end_ds ) * FFT_step_l );
	signal_power_med = sum( p_val_med( sig2_beg_ds : sig2_end_ds ) * FFT_step_m );
	signal_power_high = sum( p_val_high( sig3_beg_ds : sig3_end_ds ) * FFT_step_h );
	
	% Calculate the noise and distortion power.
	noise_power_low = total_power_low - signal_power_low;
	noise_power_med = total_power_med - signal_power_med;
	noise_power_high = total_power_high - signal_power_high;
	
	% Calculate the SINAD
	SINAD_low = 10 * log10( signal_power_low / noise_power_low );
	SINAD_med = 10 * log10( signal_power_med / noise_power_med );
	SINAD_high = 10 * log10( signal_power_high / noise_power_high );
	
	% Calculate using the built-in Matlab functions as a comparison.
	[p_low, p_f_low] = pwelch( SD_bp_low_ds, kaiser( Ll, 5 ), [], Ll, Ll * FFT_step_l );
	SINAD_low_ds = sinad( p_low, p_f_low, 'psd' );
	
	[p_med, p_f_med] = pwelch( SD_bp_med_ds, kaiser( Lm, 5 ), [], Lm, Lm * FFT_step_m );
	SINAD_med_ds = sinad( p_med, p_f_med, 'psd' );
	
	[p_high, p_f_high] = pwelch( SD_bp_high_ds, kaiser( Lh, 5 ), [], Lh, Lh * FFT_step_h );
	SINAD_high_ds = sinad( p_high, p_f_high, 'psd' );

	% Calculate using the built-in Matlab functions as a comparison.
	[p_low, p_f_low] = pwelch( SD_bp_low, kaiser( L, 5 ), [], L, f_s );
	SINAD_low_nds = sinad( p_low, p_f_low, 'psd' );
	
	[p_med, p_f_med] = pwelch( SD_bp_med, kaiser( L, 5 ), [], L, f_s );
	SINAD_med_nds = sinad( p_med, p_f_med, 'psd' );
	
	[p_high, p_f_high] = pwelch( SD_bp_high, kaiser( L, 5 ), [], L, f_s );
	SINAD_high_nds = sinad( p_high, p_f_high, 'psd' );

The results are as follows (our method - built-in `sinad`):
 
 * "low": 32.06 dB - 34.08 dB
 * "med": 33.22 dB - 41.84 dB
 * "high": 34.13 dB - 35.37 dB

The differences between our method and the built-in functions most likely exist because of what we explained in task 1 namely the lack of noise in the signal band in our method, whereas we assume that the Matlab functions perform much more in-depth analysis in trying to calculate a more accurate SINAD.

We have also performed the SINAD analysis using the non-downsampled signals with the built-in Matlab functions with the following results:

 * "low": 36.86 dB
 * "med": 41.77 dB
 * "high": 37.10 dB

Again, there are differences between the SINAD of the original and downsampled signals. One of the reasons is that the sampling frequency cannot be divided by the signal bandwidths without a remainder (as we calculated in task 4). This means that there are samples in the downsampled signals that do not belong in the signal, and this can cause noise affecting the SINAD. The original signals do not suffer from this problem.

### 5.2a
We have plotted the power spectrum density of the downsampled signals in order to find the frequencies of the distortion products.

#### `code`
	% Calculate the correct frequency ranges.
	range_l = 1 : length( df_range_low );
	range_m = 1 : length( df_range_med );
	range_h = 1 : length( df_range_high );
	
	% Plot the power spectrum density of the signals.
	figure; plot( df_range_low( range_l ), 10*log10( p_val_low ) ); title( 'power spectrum density "low"' );
	xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );
	figure; plot( df_range_med( range_m ), 10*log10( p_val_med ) ); title( 'power spectrum density "med"' );
	xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );
	figure; plot( df_range_high( range_h ), 10*log10( p_val_high ) ); title( 'power spectrum density "high"' );
	xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );

#### `plot PSD "low"`
![](plots/psd_low.png)

#### `plot PSD "med"`
![](plots/psd_med.png)

#### `plot PSD "high"`
![](plots/psd_high.png)

In the PSD of "low", we see no peaks related to distortion products. In the PSD of "med", we can see peaks at around 3.3 MHz, and 8.8 MHz. In the PSD of "high", we can see peaks at around 3.3 MHz, 6.6 MHz, 13.2 MHz, and 14.3 MHz.

### 5.2b
We used the built-in `snr` function of Matlab to determine the SNR of the downsampled signals.

#### `code`
	% Calculate the SNR of the downsampled signals.
	SNR_low_ds = snr( p_low, p_f_low, 'psd' );
	SNR_med_ds = snr( p_med, p_f_med, 'psd' );
	SNR_high_ds = snr( p_high, p_f_high, 'psd' );

The results are as follows:

 * "low": 34.08 dB
 * "med": 41.84 dB
 * "high": 35.37 dB

From these results, we can conclude that the distortion products are very small compared to the total noise in the signals. Most likely, quantization noise and noise of the multiple filters are the main source of noise observed throughout this assignment.

## Task 6
Due to lack of time, this exercise has been skipped.

## Task 7
In this assignment, we learned how to theoretically build a base station. Most importantly, the usage of a signal consisting of multiple sine waves going through a Sigma Delta converter, and then separating the signal components to see the effects of distortion was a warm welcome since it goes a step further in complexity then what we have seen in the lab sessions. This increased the difficulty of the assignment, but not too much. However, this assignment did feel more difficult and time consuming compared to the wireless system assignment, therefore the weight of the grading of both assignments feels unbalanced.

There were some parts that were a bit unclear. For example, at first it was unclear that the simulink model provided in the lab sessions were to be used as the Sigma Delta converter (as this is the only SD converter model aside from the one created in the analog project). Another example is the expected SNR of the system. Since we had done taken similar step in the lab session before, we decided to stick to an SNR of 60 dB. Therefore a suggestion would be to explain these parts more clearly (whether we have requirements, or that we choose it ourselves).  