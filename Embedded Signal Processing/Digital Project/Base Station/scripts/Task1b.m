% Variables
%f_1 = 3.3 * 10^6;   % Frequency signal 1
%f_2 = 5.7 * 10^6;   % Frequency signal 2
%f_3 = 13.95 * 10^6; % Frequency signal 3

f_1 = 3.0 * 10^6;   % Frequency signal 1
f_2 = 7.0 * 10^6;   % Frequency signal 2
f_3 = 12.5 * 10^6; % Frequency signal 3

a_1 = 0.8;          % Amplitude signal 1
a_2 = 0.8;          % Amplitude signal 2
a_3 = 0.0;          % Amplitude signal 3

%f_s = 868146274;    % Sampling frequency SD
f_s = 868e6;    % Sampling frequency SD
%f_s = 30000000;    % Sampling frequency SD

% Load the SigmaDelta model
load_system( 'SigmaDelta_Simulink_Model' );

% Set model values
set_param( 'SigmaDelta_Simulink_Model/input 1', 'Frequency', num2str( f_1 ) );
set_param( 'SigmaDelta_Simulink_Model/input 1', 'Amplitude', num2str( a_1 ) );
set_param( 'SigmaDelta_Simulink_Model/input 2', 'Frequency', num2str( f_2 ) );
set_param( 'SigmaDelta_Simulink_Model/input 2', 'Amplitude', num2str( a_2 ) );
set_param( 'SigmaDelta_Simulink_Model/input 3', 'Frequency', num2str( f_3 ) );
set_param( 'SigmaDelta_Simulink_Model/input 3', 'Amplitude', num2str( a_3 ) );
set_param( 'SigmaDelta_Simulink_Model/ZOH', 'SampleTime', num2str( 1 / f_s ) );

% Simulate model
sim( 'SigmaDelta_Simulink_Model' );

% Plot fft of the second order output SD converter
L = length( SD_2 );
bandwidth = 15e6;                                       % Signal bandwidth
FFT_step = f_s / L;                           % Frequency step size per FFT sample
range = 1:(bandwidth/FFT_step);                         % FFT sample range for 0..15 Mhz frequency range
l_sample = range( length( range ) );                    % Index of the last sample in the frequency range
f_range = 0:FFT_step:(L / 2 - 1) * FFT_step;  % Frequency range of the FFT

k_w = 1;
%k_w = kaiser( L, 38 );                        % Kaiser window
a_values = abs( fft( SD_1 .* k_w ) ) / (0.5 * L);
a_values = a_values(1:(L - 1)/ 2 + 1);

semilogx( f_range, a_values );
axis tight; title( 'A graph' );
xlabel( 'Frequency (Hz)' ); ylabel( 'A' );
