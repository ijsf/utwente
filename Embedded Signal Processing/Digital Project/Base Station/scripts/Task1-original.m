% Variables
%f_1 = 3.3 * 10^6;   % Frequency signal 1
%f_2 = 5.7 * 10^6;   % Frequency signal 2
%f_3 = 13.95 * 10^6; % Frequency signal 3

f_1 = 3.0 * 10^6;   % Frequency signal 1
f_2 = 7.5 * 10^6;   % Frequency signal 2
f_3 = 12.5 * 10^6; % Frequency signal 3

a_1 = 0.8;          % Amplitude signal 1
a_2 = 0.8;          % Amplitude signal 2
a_3 = 0.8;          % Amplitude signal 3

%f_s = 868146274;    % Sampling frequency SD
f_s = 868e6;    % Sampling frequency SD
%f_s = 30000000;    % Sampling frequency SD

bw = 15e6;

% Load the SigmaDelta model
load_system( 'SigmaDelta_Simulink_Model' );

% Set model values
set_param( 'SigmaDelta_Simulink_Model/input 1', 'Frequency', num2str( f_1 ) );
set_param( 'SigmaDelta_Simulink_Model/input 1', 'Amplitude', num2str( a_1 ) );
set_param( 'SigmaDelta_Simulink_Model/input 2', 'Frequency', num2str( f_2 ) );
set_param( 'SigmaDelta_Simulink_Model/input 2', 'Amplitude', num2str( a_2 ) );
set_param( 'SigmaDelta_Simulink_Model/input 3', 'Frequency', num2str( f_3 ) );
set_param( 'SigmaDelta_Simulink_Model/input 3', 'Amplitude', num2str( a_3 ) );
set_param( 'SigmaDelta_Simulink_Model/ZOH', 'SampleTime', num2str( 1 / f_s ) );

% Simulate model
sim( 'SigmaDelta_Simulink_Model' );

% Plot fft of the summed input signals
Li = length( input_signals );
f_i = ( 0 : f_s / Li : f_s - ( f_s / Li ) );
%figure; plot( f_i, abs( fft( input_signals ) ) / ( 0.5 * Li ) ); axis tight;

% Plot fft of the second order output SD converter
num_samples = length( SD_2 );
FFT_step = f_s / num_samples;
wanted_range = bw / FFT_step;

X = abs( fft( SD_2 ) );
X = X( 1 : round( wanted_range ) );
num_samples = length( X );
X = abs( fft( SD_2( 1 : wanted_range ) .* k_w ) );
k_w = kaiser( num_samples, 38 );

f = ( 0 : FFT_step : bw - FFT_step );

figure; plot( f, X / ( 0.5 * L ) ); axis tight;