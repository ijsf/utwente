# Embedded Signals Processing #
###Digital project 1 (2013-2014)###
######C.E. Etheredge (s0150207), O. Meteer (s0140147)######
----------

## General Information

For tasks 1 to 3, we have used the same generated input symbols and parameters, which can be found in `scripts/input_123.m`. By setting the variable `generate` accordingly, one can choose to generate random symbols, or to use the input symbols that we generated. The same applies to task 4. The generated values are stored in `scripts/input_4.m`.

The answers given below are based on the generated input symbols and parameters stored in the mentioned files for the sake of reproducibility. Still, as we have to include random noise in some of the tasks, the answers do vary a bit each time the task scripts are run.

## Task 1

### 1.1

The Bit Error Rate (BER) is **0.5625**.

We consider this to be a relatively high error rate, as can be expected from this particular OFDM system without equalization.

### 1.2

The BER is **0**.

We expected the value of zero (ideal error-free communication), as the channel transfer *h(l)* is fully known. The OFDM system can thus perfectly compensate.

### 1.3

The cyclic prefix acts as a periodic extension (of length *M*) of the last part of an OFDM symbol, which is added to the beginning of the symbol in the transmitter and removed in the receiver before the demodulation pass.

The cyclic prefix serves two primary purposes:

1.	One result of this cyclic prefix is that the symbol becomes cyclic, and the convolution operation used for the channel also becomes cyclic (circular).

	We note that the input signal of the OFDM system is periodic, but due to the finite characteristics of the channel (e.g. only *N* symbols), this periodicity is no longer guaranteed. This property is restored by making the symbol cyclic or periodic, as described above.
	
2.	The cyclic prefix eliminates ISI.

	We
 can assume that the ISI is finite and only affects the beginning of the symbol. Specifically, the affected segment relates to the channel length or impulse response *L*. By removing the segment before demodulation, the ISI is thus eliminated.
 
	It thus follows that the **minimum cyclic prefix length has to equal *L* or *8***.

## Task 2

### 2.1

The BER when using a white Gaussian noise with a noise floor of **-10 dB** is around **0.035**.

### 2.2

The BER when using a white Gaussian noise with a noise floor of **-20 dB** is around **0.0035**.

## Task 3

The BER when a PBJ signal is present is around **0.043**.

#### `plot`
![](plots/PBJ.png)

## Task 4

### 4.1

We have found that the exact contents of the training symbols does not seem relevant to the BER, due to the fact that both the transmitter and receiver have full knowledge of the training symbols.

The length of the training symbols is important: more symbols reduce the BER, but also reduce the throughput efficiency of the system. We are therefore looking for a good trade-off.

We have found that **3** training symbols (per subcarrier) reduce the BER to an ideal *0*, while **2** symbols reduce the BER to a very reasonable *0.0047* on average.

We have therefore decided on **2** symbols per subcarrier (2 symbols * 64 subcarriers in total).

The contents of the training symbols have been experimentally chosen in such a way as to reduce the transmitter power output (e.g. after modulation), by using a combination of alternating bits, as can be seen below:

#### `code`
    t = [0, 0, 1, 0];
    t = [t, t, 1 - t, t];
	t = [t, t, 1 - t, t];
	t = repmat( t, 1, 2 );

Below is a plot of a part of a transmission. The first 160 symbols are relevant (2 * 64 training symbols + 2 * 16 cyclic prefix).

### `plot`
![](plots/training_symbols.png)

### 4.2

Please see the Matlab scripts.

### 4.3

The BER values vary quite a bit, but we found that the values are around **0.0047**. This shows that beamforming can help quite a bit.

## Task 5

### 5.1

Assuming the case in which the PBJ signal is our desired signal, the easiest method of extracting this signal that comes to mind is a *bandpass filter*.

This bandpass filter can be constructed (e.g. digitally) to pass the desired PBJ signal (between 5 and 6 MHz) while attenuating any signals outside this band.

### 5.2

When using the above mentioned bandpass filter, a single receive antenna would be sufficient enough to isolate the PBJ signal.

## Task 6

### 6.1

In this assignment, we first learned that discrete *wireless channels* can be modeled as *LTI systems* or filters. This generalization allowed us to apply the digital theory that was taught during this course, in order to construct and simulate wireless channels, and eventually the target OFDM system, in Matlab.

We have learned that these discrete wireless channels are affected by *noise* such as *ISI*, and the *BER* can be used as a heuristic to measure the integrity of the channel. The *OFDM* provides a solution to the reduction of noise thus increasing signal integrity, by means of *equalization* (if the channel transfer function is known) and the *cyclic prefix*.

Furthermore, we have been introduced to the concept of *beamforming* as a practical and effective method of interference suppression. By simulating a wireless channel corrupted by a PBJ signal, we learned how to effectively suppress interference by utilizing the beamforming algorithm together with a predefined set of training symbols on top of our OFDM system.

In short conclusion, we have learned to implement and simulate a digital wireless OFDM system with increasing levels of interference suppression.

### 6.2

The difficulty of this assignment was above average and in line with the rest of the digital assignments of the course.

We would have preferred to read somewhat more background information on the introduced concepts within this assignment. Because the concepts in this assignment are mostly (if not all) standardized, a list of references to relevant research would be possible. This would have allowed us to grasp the concepts more effectively.

Additionally, we would like to suggest the addition of more images and diagrams, specifically on the relation between subcarriers and the frequency spectrum, and concepts such as ISI and the cyclic prefix.

We believe the above points would ease the understanding of concepts that are otherwise very theoretical.
