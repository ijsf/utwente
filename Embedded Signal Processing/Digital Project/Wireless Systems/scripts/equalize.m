% Equalizes the input using the inverse of the given channel contributions.
%   Input:
%   Yk_in  - The received symbols.
%   h      - Contributions per channel.
%   N      - Number of sub-carriers.
%
%   Output:
%   Yk_out - The equalized input symbols.
function [ Yk_out ] = equalize( Yk_in, h, N )
    % Precalculated values.
    num_symbols = size( Yk_in, 2 );
    num_symbol_blocks = num_symbols / N;
    cont_size = size( h, 2 );
    mjTPdivN = ( -1j * 2 * pi ) / N;
    
    % Allocate space for the equalized symbols.
    Yk_out = zeros( 1, num_symbols );
    
    % Calculate the inverse of the channel contributions.
    Hk = zeros( 1, N );
    for k = 0 : N - 1
        sum = 0;
      
        for l = 0 : cont_size - 1
            sum = sum + h( l + 1 ) * exp( mjTPdivN * l * k );
        end
        
        Hk( k + 1 ) = sum;
    end
    
    % Perform equalization.
    %Yk_out = filter( ( 1 ./ Hk ), [ 1 ], Yk_in );
    for b = 0 : num_symbol_blocks - 1
        % Points to the first symbol of this symbol block.
        fsb = ( b * N ) + 1;
        
        % Points to the last symbol of this symbol block.
        lsb  = fsb + N - 1;
        
        Yk_out( fsb: lsb ) = Yk_in( fsb : lsb ) ./ Hk;
    end
end