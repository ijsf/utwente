% Task 4

% Start with a clean slate.
clear;

% Load the channel contributions.
load channel_task_4.mat;

% Generate a random input signal or load an input signal from file.
generate = 0;

if( generate == 1 )
    % Define the cyclic prefix size and number of sub-carriers.
    M = 16;
    N = 64;

    % Define the number of training symbol blocks (T * N symbols).
    T = 2;

    % Generate training symbols.
    t = [0, 0, 1, 0];
    t = [t, t, 1 - t, t];
    t = [t, t, 1 - t, t];
    t = repmat( t, 1, T );
    
    % Generate 300 random symbols blocks.
    num_symbols = 300 * 64;
    data = randi( [ 0, 1 ], 1, num_symbols );
    b = [t data];
else
    load input_4.mat;
    num_symbols = length( data );
end

% Send the symbols through the wireless channel,
% and collect the received symbols.
h_s = { h11; h12 };             % Channel contributions per antenna.
noise_floor = -20;              % 20 dB channel noise.
PBJ_power = 10;                 % 10 dB partial band jamming noise.
BW_signal = 10e6;               % 10 MHz input signal bandwidth.
BW_pbj = 1e6;                   % 1 MHz PBJ signal bandwidth.
f_ci = 5.5e6;                   % 5.5 MHz center frequency PBJ signal.
h_pbj = { hj1; hj2 };           % Channel contributions for the PBJ signal.
num_antennas = size( h_s, 1 );  % Number of antennas.

B = Beamforming_OFDM_system( b, ...            % Input signal.
                             h_s, ...          % Channel contributions.
                             N, ...            % Number of sub-carriers.
                             M, ...            % Cyclic prefix size.
                             T, ...            % 2 training symbol blocks.
                             noise_floor, ...  % Noise floor (negative dB).
                             PBJ_power, ...    % 10 dB PBJ noise power.
                             BW_signal, ...    % 10 MHz signal bandwidth.
                             BW_pbj, ...       % 1 MHz PJB noise bandwidth.
                             f_ci, ...         % 5.5 MHz PBJ signal center freq.
                             h_pbj, ...        % PBJ channel contributions.
                             num_antennas );   % 2 antennas.

% Calculate the Bit Error Rate.
num_error_symbols = sum( abs( B - data ) );
BER = num_error_symbols / num_symbols;