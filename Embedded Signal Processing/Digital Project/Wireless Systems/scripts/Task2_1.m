% Task 2.1

% Start with a clean slate.
clear;

% Load the channel contributions.
load channel_task_1.mat;

% Generate a random input signal or load an input signal from file.
generate = 0;

if( generate == 1 )
    % Define the cyclic prefix size and number of sub-carriers.
    M = 16;
    N = 64;

    % Generate 10240 random symbols.
    num_symbols = 10240;
    b = randi( [ 0, 1 ], 1, num_symbols );
else
    load input_123.mat;
    num_symbols = length( b );
end

% Send the symbols through the wireless channel,
% and collect the received symbols.
h_s = h;            % Channel contributions per antenna.
equalization = 1;   % Perform equalization.
noise_floor = -10;  % -10 dB channel noise.
PBJ_power = 0;      % No partial band jamming.
BW_signal = 0;      % Input signal bandwidth (if PBJ_power > 0).
BW_pbj = 0;         % PBJ signal bandwidth (if PBJ_power > 0).
f_ci = 0;           % Center frequency PBJ signal (if PBJ_power > 0).
h_pbj = 0;          % Use same contributions as for the input signal.

B = Baseband_OFDM_system( b, ...            % Input signal.
                          h_s, ...          % Channel contributions signal.
                          N, ...            % Number of sub-carriers.
                          M, ...            % Cyclic prefix size.
                          equalization, ... % Usage of equalization.
                          noise_floor, ...  % Noise floor (negative dB).
                          PBJ_power, ...    % PBJ noise power.
                          BW_signal, ...    % Input signal bandwidth.
                          BW_pbj, ...       % PJB noise bandwidth.
                          f_ci, ...         % PBJ signal center freq.
                          h_pbj );          % Channel contributions PBJ.

% Calculate the Bit Error Rate.
num_error_symbols = sum( abs( B - b ) );
BER = num_error_symbols / num_symbols;