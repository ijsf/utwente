% Simulates a beamforming OFDM system, that transmits binary symbols
% through a virtual wireless channel via multiple paths.
%
%   Input:
%   b     - Binary symbols to be transmitted.
%   h_s   - Array containing the contributions for the different paths.
%   N     - Number of sub-carriers.
%   M     - Cyclic prefix size.
%   T     - Number of traning symbols.
%   NF    - The noise floor in dB (0 for no noise floor).
%   Ppbj  - Partial band jamming power (0 for no PBJ).
%   bw_s  - Input signal bandwidth (if Ppbj > 0).
%   bw_i  - PBJ signal bandwidth (if Ppbj > 0).
%   f_ci  - Center frequency PBJ signal (if Ppbj > 0).
%   h_pbj - Channel contributions for the PBJ signal (if Ppbj > 0).
%           If this is 0, then do not use it.
%   nAnt  - Number of antennas. This means the size of input "b" needs
%           to be (nAnt * size( b, 2 )), and the output B will be the
%           same size (nAnt * size( b, 2 )).
%
%   Output:
%   B    - Binary symbols that are received.
function [ B ] = Beamforming_OFDM_system( b, h_s, N, M, T, NF, Ppbj, bw_s, bw_i, f_ci, h_pbj, nAnt )
    % Declare useful helper functions.
    Mapping = @( symbols )( ( symbols * -2 ) + 1 );
    Demapping = @( symbols )( real( symbols ) < 0 );
    
    % Map the symbols from 0,1 to 1,-1.
    Xk = Mapping( b );
    
    % Perform OFDM modulation.
    xm = OFDM_modulation( Xk, N, M );
    
    % Simulate the wireless channel by convolving the path contributions
    % with the symbols to be transmitted. The result is an array of 
    % received modulated symbols.
    ym = cell( nAnt, 1 );
    for h = 1 : nAnt
        ym{ h } = filter( h_s{ h }, 1, xm );
    end
    
    % Add partial band jamming if needed.
    if( Ppbj > 0 )
        PBJ = Generate_Partial_Band_Jamming( size( ym{ 1 }, 2 ), bw_s, bw_i, f_ci, Ppbj );
        
        f_s = 2 * bw_s;
        
        pbj = cell( nAnt, 1 );
        
        if( nAnt > 0 )
            for p = 1 : nAnt
                pbj{ p } = filter( h_pbj{ p }, [ 1 ], PBJ );
            end
        end
        
        for h = 1 : nAnt
            ym{ h } = ym{ h } + pbj{ h };
        end
    end
    
    % Add white Gaussian noise if needed.
    if( NF < 0 )
        for h = 1 : nAnt
            ym{ h } = awgn( ym{ h }, abs( NF ) );
        end
    end
    
    % Perform OFDM demodulation.
    Yk = cell( nAnt, 1 );
    for h = 1 : nAnt
        Yk{ h } = OFDM_demodulation( ym{ h }, N, M );
    end
    
    % Perform the actual beamforming using the Wiener beamformer.
    y_out = zeros( 1, size( b, 2 ) - ( T * N ) );

    for k = 0 : N - 1
        % Calculate the Ryk matrix of the Wiener beamformer.
        Ryk = zeros( nAnt, nAnt );
        
        for m = 0 : T - 1
			A = []; B = [];
			for h = 1 : nAnt
				A = [ A; Yk{ h }( N * m + 1 + k ) ];
				B = [ B ctranspose( Yk{ h }( N * m + 1 + k ) ) ];
			end
            Ryk = Ryk + A * B;
        end

        Ryk = Ryk ./ T;
        
        % Calculate the r_yk_Xk matrix of the Wiener beamformer.
        r_ykXk = zeros( nAnt, 1 );
    
        for m = 0 : T - 1
			A = [];
			for h = 1 : nAnt
				A = [ A; Yk{ h }( N * m + 1 + k ) ];
			end
            r_ykXk = r_ykXk + A * Xk( N * m + 1 + k )';
        end

        r_ykXk = r_ykXk ./ T;
        
        % Calculate the final solution of the Wiener beamformer.
        w_opt = pinv( Ryk ) * r_ykXk;
        
		A = [];
		for h = 1 : nAnt
			A = [ A; Yk{ h }( N * T + 1 + k : N : end ) ];
        end
        
        % Determine the final received symbols.
        y_out( k + 1 : N : end ) = w_opt' * A;
    end
    
    % Map the symbols 1,-1 back to 0,1.
    B = Demapping( y_out );
end