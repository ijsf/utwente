% Generates a partial band jamming signal.
%
%   Input:
%   input_length - Number of symbols in the signal.
%   bw_s         - Bandwidth of the signal.
%   bw_i         - Bandwidth of the interference signal.
%   f_ci         - Center frequency of the interference signal.
%   power        - Interference power in dB.
%
%   Output:
%   pbj          - Partial band jamming interference signal.
function [ pbj ] = Generate_Partial_Band_Jamming( input_length, bw_s, bw_i, f_ci, power )
    % The interference signal is assumed to be white Gaussian noise.
    noise = awgn( zeros( 1, input_length ), -power );
    
    f_s = 2 * bw_s;
    % Calculate the normalized stopband edge frequency.
    nf_s = bw_i / f_s;
    
    % Use a high order Chebyshev lowpass filter to only have
    % noise in the bandwidth of the wanted interference signal.
    [ B, A ] = cheby2( 11, 80, nf_s );
    
    % Filter away
    filt_noise = filter( B, A, noise );
    
    % Create a carrier signal.
    jTPfc = 1j * 2 * pi * ( f_ci / f_s );
    carrier = sqrt( 2 ) * exp( jTPfc * ( 0 : input_length - 1 ) );
    
    pbj = filt_noise .* carrier;
end