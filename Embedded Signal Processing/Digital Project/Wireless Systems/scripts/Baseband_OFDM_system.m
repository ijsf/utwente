% Simulates a baseband OFDM system, that transmits binary symbols through 
% a virtual wireless channel via multiple paths.
%
%   Input:
%   b     - Binary symbols to be transmitted.
%   h_s   - Array containing the contributions for the different paths.
%   N     - Number of sub-carriers.
%   M     - Cyclic prefix size.
%   E     - If 1, use an equalizer, else do not use an equalizer.
%   NF    - The noise floor in dB (0 for no noise floor).
%   Ppbj  - Partial band jamming power (0 for no PBJ).
%   bw_s  - Input signal bandwidth (if Ppbj > 0).
%   bw_i  - PBJ signal bandwidth (if Ppbj > 0).
%   f_ci  - Center frequency PBJ signal (if Ppbj > 0).
%   h_pbj - Channel contributions for the PBJ signal (if Ppbj > 0).
%           If this is 0, then use the value "h" given above.
%
%   Output:
%   B    - Binary symbols that are received.
function [ B ] = Baseband_OFDM_system( b, h_s, N, M, E, NF, Ppbj, bw_s, bw_i, f_ci, h_pbj )
    % Declare useful helper functions.
    Mapping = @( symbols )( ( symbols * -2 ) + 1 );
    Demapping = @( symbols )( real( symbols ) < 0 );
    
    % Map the symbols from 0,1 to 1,-1.
    Xk = Mapping( b );
    
    % Perform OFDM modulation.
    xm = OFDM_modulation( Xk, N, M );
    
    % Simulate the wireless channel by convolving the path contributions
    % with the symbols to be transmitted. The result is an array of 
    % received modulated symbols.
    ym = filter( h_s, 1, xm );
    
    % Add partial band jamming if needed.
    if( Ppbj > 0 )
        PBJ = Generate_Partial_Band_Jamming( size( ym, 2 ), bw_s, bw_i, f_ci, Ppbj );
        
        f_s = 2 * bw_s;

        % Plot the power spectral density of both the PBJ signal.
        pwelch( PBJ, hamming(512), [], [], f_s, 'centered' ); hold;
        
        ym = ym + PBJ;
        
        % Plot the power spectral density of the PBJ interfered signal.
        pwelch( ym, hamming(512), [], [], f_s, 'centered' );
        hLines = get( gca, 'Children' );
        set( hLines( 1 ), 'Color', [1 0 0] );
        legend( 'PBJ signal', 'ym + PBJ signal', 'Location', 'southwest' );
    end
    
    % Add white Gaussian noise if needed.
    if( NF < 0 )
        ym = awgn( ym, abs( NF ) );
    end
    
    % Perform OFDM demodulation.
    Yk = OFDM_demodulation( ym, N, M );
    
    % Perform equalization if requested.
    if( E == 1 )
        Yk_out = equalize( Yk, h_s, N );
        Yk = Yk_out;
    end
    
    % Map the symbols 1,-1 back to 0,1.
    B = Demapping( Yk );
end