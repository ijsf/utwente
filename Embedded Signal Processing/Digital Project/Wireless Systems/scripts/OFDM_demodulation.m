% Performs OFDM demodulation on an input signal.
%
%   Input:
%   ym - Input signal that has been transformed with 
%        the Discrete Fourier Transform.
%   N  - Number of sub-carriers.
%   M  - Size of the cyclic prefix.
%
%   Output:
%   xm - Symbols to be transmitted.
function [ Yk ] = OFDM_demodulation( ym, N, M )
    % Precalculated values.
    num_symbols = size( ym, 2 );
    num_symbol_blocks = num_symbols / ( N + M );
    mjTPdivN = ( -1j * 2 * pi ) / N;
    InvsqrtN = 1 / sqrt( N );
    
    % Allocate space for the symbols.
    Yk = zeros( 1, num_symbol_blocks * N );
    
    % Remove the cyclic prefix of of symbol block 
    % and then perform OFDM demodulation.
    for b = 0 : num_symbol_blocks - 1
        % Points to the first symbol after the cyclic prefix.
        index = ( b * ( M + N ) ) + M;
        
        for k = 0 : N - 1
            sum = 0;

            for m = 0 : N - 1
                sum = sum + ( ym( index + m + 1 ) * exp( mjTPdivN * k * m ) );
            end

            Yk( ( b * N ) + k + 1 ) = sum * InvsqrtN;
        end
    end
end