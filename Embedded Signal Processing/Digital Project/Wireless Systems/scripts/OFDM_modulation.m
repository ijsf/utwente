% Performs OFDM modulation on an input signal.
%
%   Input:
%   Xk - Input signal that has been transformed with 
%        the Discrete Fourier Transform.
%   N  - Number of sub-carriers.
%   M  - Size of the cyclic prefix.
%
%   Output:
%   xm - Symbols to be transmitted.
function [ xm ] = OFDM_modulation( Xk, N, M )
    % Precalculated values.
    num_symbols = size( Xk, 2 );
    num_symbol_blocks = num_symbols / N;
    jTPdivN = ( 1j * 2 * pi ) / N;
    InvsqrtN = 1 / sqrt( N );
    
    % Allocate space for the symbols and the cyclic prefix.
    xm = zeros( 1, num_symbols + num_symbol_blocks * M );
    
    % Perform OFDM modulation and prepend the cyclic prefix
    % to each symbol block.
    for b = 0 : num_symbol_blocks - 1
        % Points to the first index of the current symbol block.
        index = b * ( M + N );
        
        for m = 0 : N - 1
            sum = 0;

            for k = 0 : N - 1
                sum = sum + ( Xk( ( b * N ) + k + 1 ) * exp( jTPdivN * k * m ) );
            end

            % The first M symbols per symbol block are the cyclic
            % prefix, and the next N symbols are the modulated
            % symbols.
            xm( index + M + m + 1 ) = sum * InvsqrtN;
        end
        
        % Copy the last M symbols of this symbol block to the first
        % M positions of this symbol block.
        xm( index + 1 : index + M ) = xm( index + N + 1 : index + N + M );
    end
end