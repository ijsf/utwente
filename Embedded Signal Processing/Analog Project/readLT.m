function [m, names] = readLT(filename)
% [m, names] = readLT(filename);
%
% Read an LTspice transient file into MATLAB
% arguments: 
%   filename [input]  - the filename of the file to import
%   m        [output] - a matrix that contains the imported data
%   names    [output] - a string containing the data names of each column
%
% Note: LTspice produces transient data with an irregular time grid, i.e. non-equidistant time. To obtain transient data resampled to a
% regular time grid, use readLTandResample.

fid = fopen(filename, 'rt');
[filename, mode, machineformat] = fopen(fid);

if (fid == -1)
    fprintf(1, 'Cannot open file %s, aborting.', filename);
    return;
end

%Read the header and extract necessary information
fprintf(1, 'readLT - version 1.0 by N.A. Moseley - IC Design - EEMCS - UTwente\n');
names = fgets(fid);
fprintf(1, '> Importing the following variables: %s', names); %Display the header info

datapos = ftell(fid);   % read the position of the data

% make a shadow copy without the header
fid2 = fopen('shadow_copy.txt', 'wt');
while(~feof(fid))
    fprintf(fid2, '%s', fgets(fid));    % copy lines to shadow file
end
fclose(fid2);
fclose(fid);

% load the shadow copy
m = load('shadow_copy.txt'); % fast load?
s = size(m);
fprintf(1, '> Reading %d points of %d variables complete\n', s(1), s(2));
%delete('shadow_copy.txt'); % erase shadow copy
