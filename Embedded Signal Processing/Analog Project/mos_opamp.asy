Version 4
SymbolType BLOCK
LINE Normal -48 49 -47 -48
LINE Normal 48 1 -48 49
LINE Normal -47 -49 48 1
PIN -48 0 LEFT 8
PINATTR PinName in
PINATTR SpiceOrder 1
PIN 48 0 RIGHT 8
PINATTR PinName out
PINATTR SpiceOrder 2
