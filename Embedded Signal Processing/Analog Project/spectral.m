% Sample frequency used in LTspice
f_s_org = 868e6;

% Safety factor for resampling to prevent aliasing while parsing (see "help readLTandResample")
factor = 10;

% Parse data from LTspice
%
% THIS WILL TAKE A LONG TIME.
%
%data=readLTandResample('11-rescaling.txt',1/f_s);

% Or instead, just load predefined data
%
% Choose your file:
% 12-ideal-ideal.mat: ideal integrators, ideal comparator
% 12-real-ideal.mat: real CMOS integrators, ideal comparator
% 12-real-real.mat: real CMOS integrators, real comparator
%
load('12-ideal-ideal.mat');

f_s = f_s_org * factor;

% Scale to [1,-1]
SD_2 = data( 1:end,2 );
SD_2 = SD_2 - mean( SD_2 );

num_samples = length( SD_2 );                           % Number of samples
k_w = kaiser( num_samples, 38 );                        % Kaiser window with beta of 38

% Calculate the power values of the FFT after applying the Kaiser window.
%power_values = (abs( fft( SD_2 .* k_w ) )/(num_samples/f_s)).^2;
%power_values = power_values( 1:(num_samples - 1)/ 2 + 1 );

% Alternative, we can use the Matlab function pwelch to determine the PSD
power_values = pwelch( SD_2, k_w, [], num_samples, f_s );

bandwidth = 15e6;                                       % Signal bandwidth
FFT_step = f_s / num_samples;                           % Frequency step size per FFT sample
range = 1:(bandwidth/FFT_step);                         % FFT sample range for 0..15 Mhz frequency range
l_sample = range( length( range ) );                    % Index of the last sample in the frequency range
f_range = 0:FFT_step:(num_samples / 2 - 0) * FFT_step;  % Frequency range of the FFT

% Plot the FFT.
semilogx( f_range, 10 * log10( power_values ) );
axis tight; title( 'Spectral power' );
xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );

sig_sam_beg = 43;   % Begin sample of the signal.
sig_sam_end = 59;   % End sample of the signal.

% Find the total power.
total_power = sum( power_values(1:l_sample) ) * FFT_step;

% Find the signal power.
signal_power = sum( power_values(sig_sam_beg:sig_sam_end) ) * FFT_step;

% Noise and distortion power is total power minus signal power.
noise_dist_power = total_power - signal_power;

% Calculate the SINAD
SINAD = 10 * log10( signal_power / noise_dist_power )

% Calculate the THD for 14 harmonics (2 to 15 MHz)
THD = thd( SD_2, f_s, 15 );
THD = 10^(THD / 10)

% Calculate the SNR
SNR = 10 * log10( (signal_power) / (noise_dist_power - dist_power) )
