function [data,names] = readLTandResample(filename,Tsample);

% [data, names] = readLTandResample(filename,Tsample);
%
% Note: to avoid aliasing problems, it is recommended to use a sample time
% 'Tsample' that is much smaller (e.g. factor 10) than the clock period that
% is used in the Tanner simulation.
%
% Code by N.A. Moseley, IC Design, EEMCS, University of Twente.
% This code is based on work by Daniel Schinkel.

% Import data:
[data2,names] = readLT(filename);

% Generate equidistant time data:
tsim = data2(:,1);    % get time

w_ptr = 1;  % current writepointer
fprintf(1,'> Filtering out duplicate points (deltaT==0)\n');
t_cur = -1; % current time value
for x=1:length(tsim),
  if (t_cur == tsim(x))
      % do nothing, this point is a duplicate
  else
      t_cur = tsim(x);
      tsim(w_ptr) = t_cur;                  % copy time
      data2(w_ptr,2:end) = data2(x,2:end);  % copy data
      w_ptr = w_ptr + 1;
  end
end

t = (0:Tsample:tsim(w_ptr-1));
fprintf(1,'> Removed %d duplicate points.\n', length(tsim)-w_ptr); 
fprintf(1,'> Resampling to %.3f [MHz]...\n', (1e-6)/Tsample);

% Interpolate with standard Matlab interpolation function:
newdata = interp1(tsim(1:w_ptr-1),data2(1:w_ptr-1,2:end),t);
[x1,x2] = size(newdata);
if (x1 > x2)
    data = [t' newdata];
else
    data = [t' newdata'];
end    
fprintf(1,'Done!\n');
