# Embedded Signals Processing #
###Analog project 1 (2013-2014)###
######C.E. Etheredge (s0150207), O. Meteer (s0140147)######
----------

## 1

We can use the fractional expansion formula `H(z) = A / (z-1) + B / ((z-1)^2)` to rewrite the given transfer function:

	H(z) = 2 / (z-1) + 1 / ((z-1)^2)

From this formula we can derive that the gain **A** for the first integrator is **2**, and the gain **B** for the second integrator is **1**.

## 2

### Topology

We start by making a general side-by-side comparison of the two available topology options, based on the information given in this course:

*	**RC integrator**: suitable for large signals (Vpp ~ Vdd), suitable for small to moderate bandwidth.
*	**gm-C integrator**: suitable for small signals (e.g. 1 Vpp), suitable for large bandwidth.

Since we will be dealing with a signal that does not have a very large bandwidth but does have a peak-to-peak voltage that falls outside of the *small signal* category (Vpp > 1), our preference goes out to the **RC integrator** topology.

### Component values

Based on previous experience in the Analog Lab part of this course, we know that a sampling frequency of approximately **868 MHz** (period of 1.15 nanoseconds) is suitable in the case of a wideband 15 MHz signal. For this particular frequency, we have already determined before that the expected SINAD for a Sigma-Delta simulation is **52.87** using ideal components.

We first transform our transfer function to the frequency domain using substitution of `z`:

	H(s) = 2 / (e^(s*t)-1) + 1 / ((e^(s*t)-1)^2)

Where `t` can be derived using our sampling frequency: `t = 868 MHz = 1/(868 * 10^6`). We can then substitute `e^(s*t)` in this equation using the known linear approximation `e^(x) = 1 + x`. This linear approximation works because `s*t` approaches zero, since our value for `t` is very small due to our high sampling rate. We can then solve for `s`:

	|s| = 1 / (2 * t) = 0.5 * 868 * 10^6 = 434 * 10^6

Using the RC time constant formula `1 / (R*C)`, we can derive possible values for R and C:

	R*C = 1 / s = 1 / (434 * 10^6)

Note that this allows for multiple combinations of R and C values. Since we prefer realistic capacitance values, our desired range of capacitance is somewhere below 1 pF.

Using this constraint, the following values can then be plugged into the above equation:

	R = 10 KOhm
	C = 0.23 pF

For each of the two integrators, the ratio between the two resistors in the circuit will determine the gain. For the integrator with gain 1, the resistors and thus capacitors will be equal (without changing `R*C`). However, for the integrator with gain 2 we will assume the following values for now:

	Ra_1 = 10 KOhm
	Ra_2 = 2 * R1 = 20 KOhm
	Ca = C / 2 = 0.115 pF

### 3

The RC topology for the sigma-delta has been implemented in LTspice, as can be seen in the following schematics:

![](3-schematics.png)

Some remarks on our implementation with regard to our earlier sigma-delta analog laboratory assignment:

*	For the two opamps U1 and U2, we use a gain-bandwidth product (GBW) of 1000, as determined in the analog laboratory assignment.
*	`Vin` (input signal) is `1MHz` with amplitude `0.8V`, bias `0.0V`, identical to the analog laboratory assignment.
*	`Vdd` is set to `1.8V` as specified in the assignment.
*	`Vss` is set to `-1.0V` to allow the comparator to work in the full range of the input signal.

The results of the transient analysis for the input signal versus the integrator outputs are as follows:

![](3-transient-analysis-integrator-outputs.png)

Finally, the results of the transient analysis for the input signal versus the comparator output are as follows:

![](3-transient-analysis-comparator-output.png)

### 4

As can be seen in the previous section, the internal integrator output signals are currently bounded somewhere between `1 * Vdd` and `-2 * Vdd`. However, our target is bound to any signal between `GND` and `Vss` instead.

In order to accomplish this, we first start by writing down the transfer equation for the two integrators when only considering the signals between the integrators and the comparator:

![](4-transfer-equations.png)

In this transfer equation, it is evident that the zero (in the numerator) is not affected by the value of `C2` but determined by the ratio `R1*C1*R3`.

In the previous section, `R1*C1*R3 = 10KOhm * 0.23pF * 20KOhm = 0.000046`, thus the values R1, C1 and R3 can be scaled proportionally but must retaining this ratio, while `C2` can be scaled independently.

In our to get *all* signals in our circuit between `GND` and `Vss`, it is necessary to adjust the circuit, notably:

*	The input signal must now be biased to fall between the bounds. We therefore now assume the final input signal range as defined in the assignment: `0.1V < Vin < 1.7V`, by biasing `Vin` with `0.9V` and using an amplitude of `0.8V`.
*	The RC-integrators must now also be biased with `0.9V`.
*	The comparator must be equally biased with `0.9V`.

Finally, we have experimentally determined the following variables to be suitable to reach the target bounds:

	R1 = 10KOhm
	R2 = 10KOhm
	R3 = 4KOhm
	R4 = 10KOhm
	C1 = 1.15pF
	C2 = 0.35pF

	R1*C1*R3 = 0.000046 (identical)

The adjusted circuit is now as follows:

![](4-schematics.png)

Transient analysis of this circuit results in the following signals:

![](4-transient-analysis.png)

And we can indeed verify that all signals within the circuit fall within the bounds of `GND = 0V` and `Vdd = 1.8V`.

### 5

We first construct the given schematic consisting of the PMOS and NMOS using the given values:

![](5-schematic-original.png)

We then perform DC bias analysis on this circuit:

	--- Operating Point ---
	V(vdd):	 1.8	 voltage
	V(vin):	 0.9	 voltage
	V(vout):	 1.54349	 voltage
	I(Vin):	 0	 device_current
	I(Vdd):	 -7.39593e-006	 device_current
	Ix(mn1:D):	 7.39593e-006	 subckt_current
	Ix(mn1:G):	 0	 subckt_current
	Ix(mn1:S):	 -7.39593e-006	 subckt_current
	Ix(mn1:B):	 -1.54368e-012	 subckt_current
	Ix(mp1:D):	 -7.39592e-006	 subckt_current
	Ix(mp1:G):	 0	 subckt_current
	Ix(mp1:S):	 7.39592e-006	 subckt_current
	Ix(mp1:B):	 2.56633e-013	 subckt_current

With the aforementioned circuit, `Vout ~= 1.54V` which clearly falls outside our target range of `800mV` and `1000mV`.

By changing the length of the PMOS (and this the `W / L` ratio of this PMOS), we can manipulate the current and output voltage. We have experimentally determined a suitable PMOS length of `L = 0.361`, which yields the following DC bias analysis:

	--- Operating Point ---
	V(vdd):	 1.8	 voltage
	V(vin):	 0.9	 voltage
	V(vout):	 0.902926	 voltage
	I(Vin):	 0	 device_current
	I(Vdd):	 -7.16077e-006	 device_current
	Ix(mn1:D):	 7.16077e-006	 subckt_current
	Ix(mn1:G):	 0	 subckt_current
	Ix(mn1:S):	 -7.16077e-006	 subckt_current
	Ix(mn1:B):	 -9.03125e-013	 subckt_current
	Ix(mp1:D):	 -7.16077e-006	 subckt_current
	Ix(mp1:G):	 0	 subckt_current
	Ix(mp1:S):	 7.16077e-006	 subckt_current
	Ix(mp1:B):	 8.97192e-013	 subckt_current

Here, it shows that `Vout ~= 900mV` which falls within our target range.

The difference in the `W / L` ratios between the PMOS and NMOS can be explained by the transistor beta calculation. If we assume that the ratio of beta values between the PMOS and NMOS is equal to 1, the difference in `W / L` can only be attributed to a difference in `k_p` and `k_n` transconductance:

	β = k * W / L

	β_p = β_n
	β_p = k_p * 0.5 / 0.361
	β_n = k_n * 0.5 / 1.5

	k_n ~= 4.16 * k_p

### 6

To construct the given gm-c integrator, we modified the earlier circuit by adding a 1pF capacitor, adding our earlier AC input signal, and enabling AC analysis:

![](6-schematics.png)

AC analysis of this circuit resulted in the following Bode plot:

![](6-bode.png)

From this plot, we conclude that the circuit is suitable as an integrator for our specific use case due to the following:

*	Phase is nearly constant at 90 degrees between approximately `1MHz` and `1GHz`. This corresponds with the behavior of an integrator (constant at 90 degrees).
*	Transfer is linear between approximately `500KHz` and `10GHz`. This corresponds with the behavior of an integrator (where the transfer function is `H(s) = 1 / (s * R * C)`).

We note that these characteristics hold in our frequency region of interest, e.g. 868 MHz (sampling frequency).

The limited gain at low frequencies can be explained by the fact that the supply voltage `Vdd = 1.8V` is effectively clamping the output voltage at low frequencies.

### 7

As seen in the previous section, the Bode frequency plot shows a linear sloped region with a `1 / f` slope. We can therefore pick two arbitrary points on this slope with a known gain and frequency, and use this to derive the impedance at the output.

We start with the gain equation of the inverter in terms of impedance, resistance and transconductance, to derive the equation for the transconductance:

![](7-equations.png)

We then determine the gain and frequency at two arbitrary points on the slope in our Bode plot, and use the capacitance value `C` from our circuit. We thus have the following variables:

	f_1 = 1.012 MHz = 1.012 * 10^6 Hz
	A_1 = 18.5814 dB = 10^(18.5814/20) = 8.49317

	f_2 = 1.005 GHz = 1.005 * 10^9 Hz
	A_2 = -41.2125 dB = 10^(-41.2125/20) = 0.00869711

	C = 1 pF = 1 * 10^(-12) F

Since we know the transconductance `gm` for both these values must be equal, we can solve the remaining unknown resistance `R` by using the above equations:

	gm_1 = gm_2
	8.49317 * (2*pi * 1.012*10^6 * 1*10^(-12) + 1/(R)) = 0.00869711 * (2*pi * 1.005*10^9 * 1*10^(-12) + 1/(R))

Solving the above equation yields `R = 9.28025 MOhm` and `gm = 0.0000549197`. The intrinsic gain is thus:

	R*gm ~= 510

### 8

To construct the RC integrator, we modify the circuit from the previous section as described:

![](8-schematics.png)

Performing AC analysis on this circuit results in the following Bode plot:

![](8-bode.png)

Comparing the characteristics of this circuit to the earlier circuit, we notice the following:

*	The linearity of the transfer slope is violated by a constant value between approximately `50 MHz` and `1 GHz`.
*	The slope of the phase alternates and is neither constant not linear.

We observe that in our frequency region of interest (e.g. `868 MHz` or the sampling frequency), this circuit does not behave as an integrator, unlike the integrator in the previous sections. We thus consider this circuit unsuitable as an integrator.

### 9

Using the circuit from the previous section, we now multiple the width of both the NMOS and PMOS transistors by a factor 100 as specified:

![](9-schematics.png)

Performing AC analysis on this circuit results in the following Bode plot:

![](9-bode.png)

Comparing the characteristics of this circuit to the circuit from the previous section, we notice the following:

*	The transfer slope is now more or less constant between approximately `5 MHz` and `1 GHz`. This corresponds with the behavior of an integrator.
*	The slope of the phase still alternates, but is more or less constant at 90 degrees between approximately `10 MHz` and `50 MHz`.

We observe that the circuit can operate as an integrator within a fairly narrow frequency range. Unfortunately, in our particular frequency region of interest (e.g. `868 MHz` or the sampling frequency), this circuit does not behave as an integrator, due to the phase not being a constant 90 degrees. We can explain this by the excessively big width of the transistor, increasing the parasitic capacitance up to a point where it becomes a significant interference on the characteristics of this circuit.

In order to make this circuit suitable as an integrator, we have to add an additional resistor in series with the feedback capacitor. We have experimentally determined that a resistor of 300 Ohm stabilizes the phase to a constant 90 degrees. This extra resistor modifies the transfer function of the circuit.

The circuit is now as follows:

![](9-schematics-correct-integration.png)

The resulting Bode plot is now as follows:

![](9-bode-correct-integration.png)

As can be seen, the circuit now behaves like an integrator in our frequency region of interest.

### 10

We believe that the increase of the PMOS and NMOS widths in the previous section may have solved inherent instability in the system caused by circuit delay.

Since we are working with high frequencies, the rise and fall times (or the delay) of the inverter will eventually interfere with the stability of the system. The widths of the PMOS and NMOS transistors have a direct effect on respectively the rise and fall times (and thus signal delay) of the inverter. By increasing the widths, the rise and fall times (and thus delay) are decreased.

Additionally, it is generally preferred to maintain symmetry among the rise (PMOS) and fall (NMOS) time, and keep these more or less equal. Since the PMOS differs in characteristics of the NMOS, it is therefore usual to keep the PMOS width at a small multiple of the NMOS width.

### 11

We have tested our integrator with the resistance and capacitance values as found in problem 4, and determined that the current capacitance value `C2 = 0.35pF` results in an unwanted non-constant phase around 90 degrees.

To solve this problem, we can either increase the series resistance, or the capacitance `C2`. An increase of the capacitance `C2` will result in a decreased signal swing at the input of the capacitor. As has been stated, the comparator works better if input voltage swings are higher, so this may degrade performance. We therefore increase the series resistance from 300 to 600 Ohm for which we have verified that the phase is more or less constant at 90 degrees.

The circuit now seems to function properly as an integrator with our known resistance and capacitance values.

### 12-13

Our ideal inverters are first substituted for the CMOS inverters that we have constructed in the previous sections. The ideal comparator is then substituted for the non-ideal (real) comparator that has been supplied for the assignment. As a result, the CLK signal has to be scaled properly between `0` and `Vdd`.

We then change the transient analysis time from 1 microsecond to 50 microseconds in LTspice. This allows us to extend the lower bounds of the FFT and spectral analysis that we will be doing later on in Matlab.

The resulting "real" circuit which is used for our transient analysis is as follows:

![](12-schematics-real-real.png)

We have verified that all signals are still properly within the `0` and `Vdd` bounds. We then perform a spectral analysis in Matlab by using the method we learned during the analog lab. For the spectral analysis, the steps are as follows:

1.	The sample rate is increased by tenfold as suggested by the `readLTandResample` function.
2.	The LTspice exported transient analysis data is loaded using `readLTandResample`.
3.	The parsed transient data is rebiased and rescaled so it falls within the [-1,1] range.
4.	The power spectral density is determined using the FFT of the resampled data.
5.	The power density values are trimmed to the bandwidth interest region of `0 MHz` to `15 MHz`.
6.	The bin of the peak power density value is determined (corresponding to the fundamental of our signal at `1 MHz`). This gives us the exact bin in which our fundamental signal appears.
7.	Due to the windowing function of the FFT, we sum all values within a certain range of the signal bin, to determine the signal power.
8.	All bins of the FFT are summed to determine the total power.
9.	The signal power is subtracted from the total power to derive the noise (+ distortion) power.
10.	The SINAD is calculated by taking the ratio of the signal power to noise (+ distortion) power.
11.	The `thd` function in Matlab is used to determine the total harmonic distortion (power values for all 14 harmonics of the `1 MHz` signal between `0 MHz` and `15 MHz`).
12.	The SNR is calculated by taking the ratio of the signal power to noise (- distortion) power.

For the above steps, we use the following Matlab code:

	% Sample frequency used in LTspice
	f_s_org = 868e6;

	% Safety factor for resampling to prevent aliasing while parsing (see "help readLTandResample")
	factor = 10;

	% Parse data from LTspice
	%
	% THIS WILL TAKE A LONG TIME.
	%
	%data=readLTandResample('12-real-real.txt',1/f_s);

	% Or instead, just load predefined data into 'data' variable
	%
	% Choose your file:
	% 12-ideal-ideal.mat: ideal integrators, ideal comparator
	% 12-real-ideal.mat: real CMOS integrators, ideal comparator
	% 12-real-real.mat: real CMOS integrators, real comparator
	%
	load('12-real-real.mat');

	f_s = f_s_org * factor;

	% Scale to [1,-1]
	SD_2 = data( 1:end,2 );
	SD_2 = SD_2 - mean( SD_2 );

	num_samples = length( SD_2 );                           % Number of samples
	k_w = kaiser( num_samples, 38 );                        % Kaiser window

	% Calculate the power values of the FFT after applying the Kaiser window.
	%power_values = (abs( fft( SD_2 .* k_w ) )/(num_samples/f_s)).^2;
	%power_values = power_values( 1:(num_samples - 1)/ 2 + 1 );

	% Alternatively , we can use the Matlab function pwelch to determine the PSD. This yields the same result!
	[ power_values, power_f ] = pwelch( SD_2, k_w, [], num_samples, f_s );

	bandwidth = 15e6;                                       % Signal bandwidth
	FFT_step = f_s / num_samples;                           % Frequency step size per FFT sample
	range = 1:(bandwidth/FFT_step);                         % FFT sample range for 0..15 Mhz frequency range
	l_sample = range( length( range ) );                    % Index of the last sample in the frequency range
	f_range = 0:FFT_step:(num_samples / 2 - 0) * FFT_step;  % Frequency range of the FFT

	% Plot the FFT.
	semilogx( f_range, 10 * log10( power_values ) );
	axis tight; title( 'Spectral power' );
	xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );

	% Find peak of fundamental signal in our bandwidth
	[sig_center_max, sig_center] = max( power_values(1:l_sample) );
	sig_sam_beg = sig_center - 8;
	sig_sam_end = sig_center + 8;

	% Find the total power.
	total_power = sum( power_values(1:l_sample) * FFT_step );

	% Find the signal power.
	signal_power = sum( power_values(sig_sam_beg:sig_sam_end) * FFT_step );

	% Noise and distortion power is total power minus signal power.
	noise_dist_power = total_power - signal_power;

	% Calculate the SINAD
	%SINAD = 10 * log10( signal_power / noise_dist_power )

	% Alternatively, we can use Matlab's built-in SINAD function. This yields the same result!
	% Additionally, we can also use Matlab's SNR function to exclude the THD and get a final result.
	SINAD_mat = sinad( power_values(1:l_sample), power_f(1:l_sample), 'psd' )
	SNR = snr( power_values(1:l_sample), power_f(1:l_sample), 'psd' )

To verify that our own calculations and understanding of the SINAD and SNR is correct, we have checked the results of our calculations with the results from Matlab's own `sinad` and `snr` functions, and found that the results were nearly identical and thus correct.

#### Spectral analysis: ideal integrators, ideal comparator
For the circuit with ideal integrators and an ideal comparator, the above spectral analysis results in the following power spectral density graph:

![](13-spectral-ideal-ideal.png)

	SINAD: 49.06 dB
	SNR: 49.67 dB

We note that the SNR excludes the total harmonic distortion (THD) from the noise calculation.

#### Spectral analysis: CMOS integrators, ideal comparator
For the circuit with our CMOS integrators and an ideal comparator, the above spectral analysis results in the following power spectral density graph:

![](13-spectral-real-ideal.png)

	SINAD: 47.09 dB
	SNR: 47.42 dB

#### Spectral analysis: CMOS integrators, non-ideal comparator
For the circuit with our CMOS integrators and the given non-ideal comparator, the above spectral analysis results in the following power spectral density graph:

![](13-spectral-real-real.png)

	SINAD: 39.44 dB
	SNR: 39.90 dB

#### Power dissipation
The power dissipation of the integrator circuitry is calculated as follows:

	P = 8 * k * T * f * SNR * Vdd / Vpp

Where `Vdd = 1.8 V`, `Vpp = 2 * 0.8 = 1.6 V`, `SNR = 39.90` (as determined above), `k = 1.38e-23` (Boltzmann constant), `T = 295` (room temperature) and `f = 868 MHz`. Since we have two integrators, the formula is as follows:

	P = 2 * (8 * 1.38e-23 * 295 * 868e6 * 39.90 * 1.8 / 1.6) = 2.52e-9 W

The minimal total power of the two CMOS integrators is thus `2.52 nW`.

We note that the above calculation is only a generalized theoretical minimal power formula (as explained in the course), and excludes the power dissipation of the non-ideal comparator and worst-case power dissipation of the integrators themselves (e.g. when switching). A full power analysis would likely require more complex tools and/or estimation formulas, however the above figure should give a reasonable indication of the minimal power in our integrators. Also as explained in the lectures, the practical analog limit is usually 100 to 1000 times larger than the theoretical minimal power we calculated above, which gives us a general power dissipation figure that is more realistic. 

#### Performance overview
From the results of the spectral analysis, we can see that our designed integrators do not impose a great loss on performance. This leads us to conclude that the choice of component values is, at this point, already reasonably optimized.

The most significant performance loss occurs when the non-ideal comparator that was supplied with the assignment is added to the circuit. Naturally, the comparator is far more complex and contains a vast amount of components, each incurring a performance penalty.

#### Comparison with Simulink results
The Simulink results obtained in the SD analog lab were incorrect, so we have to correct it before comparing it with the performance results in this project. The errors were fixed by calculating the SNR and SINAD in the same way as in this project, and setting the sampling frequency to **868e6** MHz. The following performance figures were obtained from the corrected Simulink model:

	SINAD: 54.09
	SNR: 54.06

One reason for the performance difference is because of the simulation methods. Even though the LTspice model with ideal components and the Simulink model are 'ideal', there is still a 4.5 - 5 dB difference. This can be attributed to how the models are simulated, rounding errors, window size, etc. The output of the Simulink model generates perfect square waves, whereas the output of LTspice clearly displays a non-infinite rising and falling edges. Also, the data outputted by LTspice is resampled which can lead to loss of precision.

### 14

In this section, we will discuss and/or implement a series of changes that will optimize the performance of our sigma-delta circuit. Our optimizations are first verified using real inverters and an ideal comparator. If the SINAD is increased, we proceed to simulate the circuit with real inverters and a real comparator.

Unfortunately, due to the time required to simulate the "full" circuit with real components (CMOS integrators + non-ideal comparator), we find it extremely difficult and time consuming to perform full optimization. The sections below are thus primarily focused on optimization of the CMOS integrators we have designed in the previous sections.

#### Inverter linearity

We start off by noting that we have already implicitly optimized the linearity of the inverters before by adding resistors in series with the respective capacitors. We thus state that the linearity of the inverters is thus properly optimized at this point.

#### Decreasing CMOS width

Our first attempt at optimization is trying to decrease the width of the CMOS inverters (thus changing the ratio `W / L`) from `W = 50` to `W = 25`. We do not change any other values apart from the series resistance `R5` and `R6` to ensure that the phase is still an approximate constant 90 degrees.

We find that changing the width of the CMOS inverters decreases the SINAD, thus degrading performance.

#### Decreasing CMOS area

We then try to decrease the CMOS length, while retaining the same `W / L` ratio, with the assumption that we can optimize the integrator characteristics this way. The PMOS is changed from `W = 50, L = 0.361` to `W = 25, L = 0.1805`, while the NMOS is changed from `W = 50, L = 1.5` to `W = 25, L = 0.75`. The series resistance `R5` and `R6` is changed to ensure that the phase is still an approximate constant 90 degrees.

We find that changing the CMOS area in this way decreases the SINAD, thus degrading performance.

#### Comparator input signal (VintB)

We then attempt to manipulate the signal swing at the input of the comparator (`VintB`). As stated in the assignment, the inverters perform better with a lower swing, whereas the comparator performs better with a higher swing. However, there is an optimal point somewhere in between since the input of the comparator is also fed back into the integrator.

To find out if we can optimize this signal even further, we take the capacitance `C2 = 0.35 pF` as a starting point to see what happens if we both increase, as well as decrease the value (and corresponding series resistance). We find that for the case of our CMOS integrators with an ideal comparator:

1.	Decreasing `C2` to `0.3 pF` (with `R5 = 900`) decreases the SINAD.
2.	Increasing `C2` to `0.5 pF` (with `R5 = 480`) increases the SINAD.
3.	Increasing `C2` to `0.7 pF` (with `R5 = 375`) decreases the SINAD.

This is curious, as a higher capacitance only decreases the signal swing at the comparator input.

We therefore test both the highest and lowest values for `C2` with the non-ideal comparator to see what happens.

##### Higher capacitance

The resulting circuit with CMOS integrators and the non-ideal comparator for high capacitance `C2 = 0.5 pF` is as follows:

![](14-c2-schematics.png)

However, after performing spectral analysis, the power density graph is as follows:

![](14-c2-bode.png)

As can be seen, the circuit is now misbehaving and is no longer usable. This is curious, as the same circuit with an ideal comparator does exhibit better performance. We believe we may have exceeded the tolerable capacitance on the comparator input in this circuit.

##### Lower capacitance

The resulting circuit with CMOS integrators and the non-ideal comparator for high capacitance `C2 = 0.3 pF` is as follows:

![](14-c2-low-schematics.png)

However, after performing spectral analysis, the power density graph is as follows:

![](14-c2-low-bode.png)

Spectral analysis yields the following results:

	SINAD: 39.61 dB (before: 39.44 dB)
	SNR: 39.85 dB (before: 39.90 dB)

We therefore conclude that lowering the capacitance or input swing does have a significant effect on performance, which leads us to believe that our initial capacitance `C2 = 0.35 pF` is already reasonably optimized.

