sim( 'SigmaDelta_Simulink_Model' );

num_samples = length( SD_2 );                           % Number of samples
k_w = kaiser( num_samples, 20 );                        % Kaiser window with beta of 20
power_values = abs( fft( SD_2 .* k_w ) ).^2;            % Calculate the power values of the FFT
                                                        % after applying the Kaiser window.
power_values = power_values / num_samples;              % Normalize the values.
power_values = power_values( 1:(num_samples - 1)/ 2 );

%f_s = 794015353;                                        % Sampling frequency
f_s = 868e6;                                        % Sampling frequency
bandwidth = 15e6;                                       % Signal bandwidth
FFT_step = f_s / num_samples;                           % Frequency step size per FFT sample
range = 1:(bandwidth/FFT_step);                         % FFT sample range for 0..15 Mhz frequency range
l_sample = range( length( range ) );                    % Index of the last sample in the frequency range
f_range = 0:FFT_step:(num_samples / 2 - 1) * FFT_step;  % Frequency range of the FFT

% Plot the FFT.
semilogx( f_range, 10 * log10( power_values ) );
axis tight; title( 'Spectral power' );
xlabel( 'Frequency (Hz)' ); ylabel( 'Power (dBm/Hz)' );

sig_sam_beg = 14;   % Begin sample of the signal.
sig_sam_end = 28;   % End sample of the signal.

% Find the total power.
total_power = 0;

for i = 1:l_sample
   total_power = total_power + power_values( i ); 
end

% Find the signal power.
signal_power = 0;
for i = sig_sam_beg:sig_sam_end
    signal_power = signal_power + power_values( i );
end

% Noise and distortion power is total power minus signal power.
noise_dist_power = total_power - signal_power;

% Calculate the SINAD.
SINAD = 10 * log10( signal_power / noise_dist_power );

% Alternatively , we can use the Matlab function pwelch to determine the PSD. This yields the same result!
[ power_values, power_f ] = pwelch( SD_2, k_w, [], num_samples, f_s );

% Alternatively, we can use Matlab's built-in SINAD function. This yields the same result!
% Additionally, we can also use Matlab's SNR function to exclude the THD and get a final result.
SINAD_mat = sinad( power_values(1:l_sample), power_f(1:l_sample), 'psd' )
SNR = snr( power_values(1:l_sample), power_f(1:l_sample), 'psd' )