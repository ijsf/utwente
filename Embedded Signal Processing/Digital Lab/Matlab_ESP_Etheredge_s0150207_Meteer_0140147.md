# Embedded Signals Processing #
###Lab assignments (2013-2014)###
######C.E. Etheredge (s0150207), O. Meteer (s0140147)######
----------

## Assignment 1 ##

### Questions 1.1 to 1.5 ###

#### `code`
    % Question 1.1
    x = 1:50;
    y = x(1:2:end);
    
    % Question 1.2
    o = zeros( 1, ( size( y, 2 ) * 2 - 1 ) );
    o(1:2:end) = y;
    
    % Question 1.3
    o(2:2:end) = ( o(1:2:end-1) + o(3:2:end) ) / 2;
    
    % Question 1.4
    randn( n, m ) % where n and m determine the size of the matrix.
    
    % Question 1.5
    toeplitz( [5 6 7 8 9], [5 4 3 2 1] );

### Problem 1.1.a ###

#### `code`
    f_0 = 300;          % base frequency
    f_s = 8000;         % sampling rate
    phi = 3;            % random phase
    end_time = 0.01;    % end time in seconds
    
    t = 0 : 1 / f_s : end_time; % sampling moments.
    
    % signal
    y = sin( 2 * pi * f_0 * t + phi );
    
    % display the discrete sequence plot
    figure;
    stem( t, y );
    xlabel( 'time in seconds' );
    ylabel( 'amplitude' );
    
    % plot the signal
    figure;
    plot( t, y );
    xlabel( 'time in seconds' );
    ylabel( 'amplitude' );

#### `stem` plot
![](assignment1/plots/problem11a.png)

#### `plot` plot
![](assignment1/plots/problem11a_plot.png)

The reconstructed plot seems to be interpolated by Matlab between the sampling points.

### Problem 1.1.b ###

#### `code`
    f_0 = 100;          % base frequency
    f_s = 8000;         % sampling rate
    phi = 3;            % random phase
    end_time = 0.01;    % end time in seconds
    
    t = 0 : 1 / f_s : end_time; % sampling moments.
    
    % display the discrete sequence plot
    figure;
    stem( t, [ sin( 2 * pi * f_0 * t' + phi ), sin( 2 * pi * ( f_0 + 125 ) * t' + phi ), sin( 2 * pi * ( f_0 + 250 ) * t' + phi ),
      sin( 2 * pi * ( f_0 + 375 ) * t' + phi ) ] );
    xlabel( 'time in seconds' );
    ylabel( 'amplitude' );

### Problem 1.1.c ###

#### `code`
    f_0 = 7525;          % base frequency
    f_s = 8000;         % sampling rate
    phi = 3;            % random phase
    end_time = 0.01;    % end time in seconds
    
    t = 0 : 1 / f_s : end_time; % sampling moments.
    
    % display the discrete sequence plot
    figure;
    stem( t, [ sin( 2 * pi * f_0 * t' + phi ), sin( 2 * pi * ( f_0 + 125 ) * t' + phi ),
	  sin( 2 * pi * ( f_0 + 250 ) * t' + phi ), sin( 2 * pi * ( f_0 + 375 ) * t' + phi ) ] );
    xlabel( 'time in seconds' );
    ylabel( 'amplitude' );

#### Plot
![](assignment1/plots/problem11c.png)

The frequency of the sine waves as plotted in 1.1b appear to be higher than those in 1.1c, while the latter has sine waves with higher frequencies. This is due to the effect of aliasing, as a resulting of the sampling frequency of 8 KHz.

### Problem 1.1.d ###

#### `code`
    f_1 = 4000;         % 
    f_s = 8000;         % sampling rate
    mu = 600000;        % in Hz
    phi = 3;            % random phase
    end_time = 0.05;    % end time in seconds
    
    t = 0 : 1 / f_s : end_time; % sampling moments
    
    % chirp signal
    y = cos( ( pi * mu * power( t, 2 ) ) + ( 2 * pi * f_1 * t ) + phi );
    
    % momentaneous rate
    f_m = mu * t + f_1;
    
    % determine minimum and maximum rate
    f_m_min = min( f_m );
    f_m_max = max( f_m );
    
    % display the discrete sequence plot
    figure;
    stem( t, y );
    xlabel( 'time in seconds' );
    ylabel( 'amplitude' );
    
    % plot the chirp signal
    figure;
    plot( t, y );
    xlabel( 'time in seconds' );
    ylabel( 'amplitude' );

#### `stem` plot
![](assignment1/plots/problem11d.png)

#### `plot` plot
![](assignment1/plots/problem11d_plot.png)

The frequency range of the signal can be derived from the momentaneous rate `f_m`, namely the minimum and maximum values for the range `0 <= t <= 0.05s`. Since the rate function is linear, these values are `4000` and `34000`.

The aliasing phenomenon causes a loss of information in the sampled plot. In particular, the continuous chirp signal contains high frequency waves that can no longer be found in the sampled plot.

The momentaneous rate is quite small at certain moments in the sampled signal. We have verified that these moments occur when the momentaneous rate is a multiple of the sampling frequency of 8KHz, thus when `f_m` is 8, 16, 24, 32 KHz at respectively 0.00666, 0.0200, 0.0333 and 0.0466 seconds, as can be seen in our plot.

To show the aliasing, we have included two additional zoomed plots below (with the sampling frequency increased to a tenfold 80 KHz). In the latter plot, the high frequency waves are clearly visible.

##### Zoomed plot at 8 KHz
![](assignment1/plots/problem11d-aliasing.png)

##### Zoomed plot at 80 KHz
![](assignment1/plots/problem11d-aliasing-80k.png)

### Problem 1.1.e ###

#### `code`
    f_1 = 4000;         % 
    f_s = 8000;         % sampling rate
    mu = 20000;         % in Hz
    phi = 3;            % random phase
    end_time = 2.0;     % end time in seconds
    
    t = 0 : 1 / f_s : end_time; % sampling moments
    
    % chirp signal
    y = cos( ( pi * mu * power( t, 2 ) ) + ( 2 * pi * f_1 * t ) + phi );
    
    % play the signal
    wavplay( y, f_s );

The variable `mu` was set to 20 KHz or 5 times the base frequency `f_1`. The result is a sound that repeats itself 5 times within 2 seconds.

### Problem 1.2 ###

#### `code`
    function problem12_qe( t, y )
        % quantize the signal
        q16 = bitround( y, 16, 1 );
        q12 = bitround( y, 12, 1 );
        q8  = bitround( y, 8, 1 );
        q4  = bitround( y, 4, 1 );
    
        % display the discrete sequence plot
        figure;
        subplot( 2, 2, 1 );
        stem( t, y - q16 );
        xlabel( 'time in seconds' );
        ylabel( 'quantization error' );
    
        subplot( 2, 2, 2 );
        stem( t, y - q12 );
        xlabel( 'time in seconds' );
        ylabel( 'quantization error' );
    
        subplot( 2, 2, 3 );
        stem( t, y - q8 );
        xlabel( 'time in seconds' );
        ylabel( 'quantization error' );
    
        subplot( 2, 2, 4 );
        stem( t, y - q4 );
        xlabel( 'time in seconds' );
        ylabel( 'quantization error' );
    end

    function problem12_hist( t, y )
        % quantize the signal
        q16 = bitround( y, 16, 1 );
        q12 = bitround( y, 12, 1 );
        q8  = bitround( y, 8, 1 );
        q4  = bitround( y, 4, 1 );
    
        % display the discrete sequence plot
        figure;
        subplot( 2, 2, 1 );
        hist( y - q16, 64 );
        xlabel( 'quantization error' );
        ylabel( 'occurences' );
    
        subplot( 2, 2, 2 );
        hist( y - q12, 64 );
        xlabel( 'quantization error' );
        ylabel( 'occurences' );
    
        subplot( 2, 2, 3 );
        hist( y - q8, 64 );
        xlabel( 'quantization error' );
        ylabel( 'occurences' );
    
        subplot( 2, 2, 4 );
        hist( y - q4, 64 );
        xlabel( 'quantization error' );
        ylabel( 'occurences' );
    end

    f_0 = 300;          % base frequency
    f_s = 8000;         % sampling rate
    phi = 3;            % random phase
    end_time = 0.01;    % end time in seconds
    
    t = 0 : 1 / f_s : end_time; % sampling moments.
    
    % chirp signal
    y = sin( 2 * pi * f_0 * t + phi );
    
    % plot the quantization errors
    problem12_qe( t, y );
    
    % plot the histograms
    problem12_hist( t, y );
    
    % normally distributed signal (standard normal distribution with mean zero
    % and variance 1).
    y = randn( 1, length( t ) );
    
    % plot the quantization errors
    problem12_qe( t, y );
    
    % plot the histograms
    problem12_hist( t, y );
    
    % uniform signal
    y = ( rand( 1, length( t ) ) * 2 - 1 );
    
    % plot the quantization errors
    problem12_qe( t, y );
    
    % plot the histograms
    problem12_hist( t, y );

##### Chirp signal
![](assignment1/plots/problem12-chirp-qe.png)
![](assignment1/plots/problem12-chirp-hist.png)

##### Normal signal
![](assignment1/plots/problem12-normal-qe.png)
![](assignment1/plots/problem12-normal-hist.png)

##### Uniform signal
![](assignment1/plots/problem12-uniform-qe.png)
![](assignment1/plots/problem12-uniform-hist.png)

The `problem12_qe` and `problem12_hist` functions are created to respectively plot four graphs and four histograms of the quantization error. These functions are then used on the given chirp signal, normal distribution signal and uniform signal.

When looking at the above graphs, we notice that with each reduction in the amount of bits (from 16 to 4), the quantization error increases an order in magnitude (from 0.00001 to 0.01).

Also, for the chirp signal, the histograms are relatively symmetric for each quantization.

## Assignment 2 ##

### Problem 2.1 ###

#### `simulink`
![](assignment2/plots/simulink21.png)

#### `code`
    Amp = 1;            % amplitude
    Freq = 0;           % frequency in Hz
    Bias = 0;           % amplitude bias
    Phase = 0;          % phase in radians
    SampT = 1 / 1000;   % sample time in seconds
    beginTime = 0;      % time to begin sampling in seconds
    endTime = 1;        % time to end sampling in seconds
    t = beginTime : SampT : endTime;    % sampling moments
    
    % create the sinusoidal signals
    Freq = 10; sin10 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 20; sin20 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 30; sin30 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 50; sin50 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 80; sin80 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 120; sin120 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 200; sin200 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 300; sin300 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    Freq = 400; sin400 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
    
    % feed the sinusoidal signals to the FIR filter
    simin = timeseries( sin10, t );  sim( 'FIR_2' );  f10 = simout( 1, : );
    simin = timeseries( sin20, t );  sim( 'FIR_2' );  f20 = simout( 1, : );
    simin = timeseries( sin30, t );  sim( 'FIR_2' );  f30 = simout( 1, : );
    simin = timeseries( sin50, t );  sim( 'FIR_2' );  f50 = simout( 1, : );
    simin = timeseries( sin80, t );  sim( 'FIR_2' );  f80 = simout( 1, : );
    simin = timeseries( sin120, t ); sim( 'FIR_2' ); f120 = simout( 1, : );
    simin = timeseries( sin200, t ); sim( 'FIR_2' ); f200 = simout( 1, : );
    simin = timeseries( sin300, t ); sim( 'FIR_2' ); f300 = simout( 1, : );
    simin = timeseries( sin400, t ); sim( 'FIR_2' ); f400 = simout( 1, : );
    
    % finally plot amplitude vs frequency
    plot( [10 20 30 50 80 120 200 300 400], [abs(min(f10)), abs(min(f20)), abs(min(f30)), abs(min(f50)),
          abs(min(f80)), abs(min(f120)), abs(min(f200)), abs(min(f300)), abs(min(f400))] );
    xlabel( 'Frequency - Hz' );
    ylabel( 'Amplitude' );

#### `plot`
![](assignment2/plots/problem21.png)

The plot roughly portrays the characteristic frequency response of the FIR filter.

### Problem 2.2 ###

#### `simulink`
![](assignment2/plots/simulink22_before.png)

#### `code`
    figure;
    sim( 'FIR22_before' );
    plotfreq( simout );

#### `plot`
![](assignment2/plots/problem22.png)

We plotted the frequency response of the FIR filter using the supplied `plotfreq` script. The unit frequency in the plot corresponds with the Nyquist frequency which is half the sampling frequency (1000/2=500Hz in our case). We see that signals with a frequency of 0.1 * 500 = 50Hz are filtered away, which is what was expected of the FIR filter. We also see that higher frequencies are amplified significantly, and shows a similar characteristic as the plot of `Problem 2.1`.

Below is the improved version of the FIR filter, where another gain and unit delay were added.
![](assignment2/plots/simulink22_after.png)

We have:

    y(k) = b0*x(k) + b1*x(k-1) + b2*x(k-2) + b3*x(k-3)

with

    x(k) = e^(j*k*theta)

This gives us the transfer function:

    H(e^(j*theta)) = b0 + b1*e^(-j*theta) + b2*e^(-2*j*theta) + b3*e^(-3*j*theta)

The constraints are as follows:

    H(e^(j*theta))|(theta=0) = 1 = b0 + b1 + b2 + b3
    H(e^(j*theta))|(theta=theta50Hz) = 0 = b0 + b1*e^(-j*theta50Hz) + b2*e^(-2*j*theta50Hz) + b3*e^(-3*j*theta50Hz)

The other variables are the same as before the optimization. To find the gain coefficients, we have to solve the following set of equations:

    b0 + b1 + b2 + b3 = 1
    b0 + b1*cos(0.1*pi) + b2*cos(0.2*pi) + b3*cos(0.3*pi) = 0
    b1*sin(0.1*pi) + b2*sin(0.2*pi) + b3*sin(0.3*pi) = 0

Solving this set gives us the following results:

-   `b1`: 10.2159 - (2.90211 * b0)
-   `b2`: (2.90211 * b0) - 19.4317
-   `b3`: 10.2159 - b0

So this the performance of the improved FIR filter depends on the gain value `b0`. To find an optimum value for `b0` we plotted the frequency response of the FIR filter with `b0` values ranging from 1 to 8 (from left to right, top to bottom on the plot below).

#### `code`
    figure;
    for i = 1:8
        subplot(2,4,i);
        K = i;
        set_param( 'FIR22_after/b0', 'Gain', 'K' );
        sim( 'FIR22_after' );
        plotfreq( simout );
        title( strcat( 'b0 = ', int2str( i ) ) );
    end

#### `plot`
![](assignment2/plots/problem22_after.png)

We see that for `b0` = 6, the frequency response after 50 Hz tends closest to 0dB. For `b0` = 5, the response at 500Hz is less than 0dB and seems to decline even more at higher frequencies. Therefore we choose `b0` = 6, which results in the following frequency response.

![](assignment2/plots/problem22_optimized.png)

### Problem 2.3

We need to find the impulse response of the given difference equation:

    y(n) + 0.9*y(n-2) = 0.3*x(n) + 0.6*x(n-1) + 0.3*x(n-2)

The general solution consists of the homogeneous and particular solutions. The homogeneous solution can be found by solving the following equation:

    y(n) + 0.9*y(n-2) = 0

which yields the characteristic equation:

    r^2 + 0.9 = 0 => r^2 = -0.9 => r = sqrt(-0.9)

The roots are:

    r = (3/sqrt(10))*i
	r = -(3/sqrt(10))*i

Since the roots are complex so the homogeneous solution is:

    y_h(n) = C1*cos(3/sqrt(10)) + C2*sin(3/sqrt(10))

Next, we need to find the particular solution, and since we need to find the impulse response of the system, we replace `u(n)` with 



#### `code`

    % Generate unit impuls signal.
    input = zeros( 64, 1 );
    input( 1 ) = 1;
    
    % Coefficients of the system.
    a = [1 0 0.9];
    b = [0.3 0.6 0.3];
    
    % Calculate the impulse response.
    impResp = filter( b, a, input );
    
    stem( impResp );
    xlabel( 'Sample n' );
    ylabel( 'Amplitude' );

#### `stem` plot
![](assignment2/plots/problem23.png)

We can see that the output corresponds with the values we calculated ourselves, and it oscillates with decreasing amplitude exponentially (of order `0.9^n`).

### Problem 2.4

### Problem 2.5

#### `simulink`
![](assignment2/plots/simulink25.png)

#### `code`
    sim( 'FIR25' );
    subplot( 1, 2, 1 );
    stem( simout ); xlabel( 'Sample n' ); ylabel( 'Amplitude' );
    
    subplot( 1, 2, 2 );
    plot( simout ); xlabel( 'Sample n' ); ylabel( 'Amplitude' );

#### `stem` plot
![](assignment2/plots/problem25.png)

As expected, the stem plot is the same as the stem plot in `Problem 2.3`.

## Assignment 3

### Problem 3.1

#### `code`
    Fs = 20000;         % Sampling frequency
    f1 = 2500;          % Signal frequency
    L = 256;            % Number of samples
    phi = 2*pi*f1 / Fs;
    n = 0:1:L-1;
    
    % Calculate the signal.
    x = sin(n*phi);
    
    % Plot the signal.
    stem(n,x); xlabel( 'Sample n' ); ylabel( 'Amplitude' );

#### `stem` plot

![](assignment3/plots/problem31.png)

### Problem 3.2

#### `code`
	Fs = 20000;         % Sampling frequency
	f1 = 2500;          % Signal frequency
	L = 256;            % Number of samples
	theta = 2*pi*f1/Fs;
	n = 0:1:L-1;
	
	% Calculate the signal.
	x = sin(n*theta);
	
	% Frequency from 0 to 10 kHz in 5 Hz steps converted to radians.
	w = 2*pi*(0:5:10000)/Fs;
	
	% Calculate the DTFT of x(n)
	X = 0;
	
	for k = 1:1:L
	    X = X + x(k) * exp(-j*w*k);
	end
	
	delta = 5;
	stepsize = Fs / 2 / delta;
	f = (0:Fs/stepsize:Fs)/2;
	
	% Plot the DTFT.
	figure; semilogy(f, abs(X)); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' ); axis tight;

#### `plot`
![](assignment3/plots/problem32.png)

### Problem 3.3

#### `plot`
![](assignment3/plots/problem33.png)

The `fftflops` script plots the above graph, which shows the number of floating point operations are needed for a given input size. We can clearly see that the theory that the number of flops are minimal when the input size is of `N=2^m` holds, as there are minima at `N=32`, `64`, `128`, and `256`. The local maxima are at input sizes that are the size of a prime number.

### Problem 3.4

#### `code`
    Fs = 20000;         % Sampling frequency
    f1 = 2500;          % Signal frequency
    L = 256;            % Number of samples
    theta = 2*pi*f1/Fs;
    n = 0:1:L-1;
    f = (0:Fs/L:Fs-(Fs/L));
    
    % Calculate the signal.
    x = sin(n*theta);
    
    X = fft(x); semilogy(f, abs(X)); axis tight; xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );

#### `plot`
![](assignment3/plots/problem34.png)

The values for i != 32 (= 128*20000/256) are not zero due to Matlab' use of floating point precision and round off errors when calculating the FFT. This is also called broadband noise.

### Problem 3.5

### `code`
    Fs = 20000;         % Sampling frequency
	f1 = 2501;          % Signal frequency
	L = 256;            % Number of samples
	theta = 2*pi*f1/Fs;
	n = 0:1:L-1;
	f = (0:Fs/L:Fs-(Fs/L))/2;
	
	% Calculate the signal.
	x = sin(n*theta);
	
	X = fft(x); semilogy(f, abs(X)); axis tight; xlabel('Frequency (Hz)'); ylabel('Magnitude (dB)');

#### `plot`
![](assignment3/plots/problem35.png)

The difference between the results of `Problem 3.2` and `Problem 3.4`/`Problem 3.5` occurs because in the former we calculated the DTFT and in the latter two we calculated the DFT. The DTFT takes the signal from minus infinity to plus infinity, whereas the DFT treats the input signal as a periodic signal (even if it is not periodic). The sine wave that we generated is not of infinite length, therefore the DTFT takes as input a signal that has a sine wave between time t = (0..255) and 0 elsewhere. This is equal to multiplying an infinite signal with a rectangle function between 0 and 255, and since the Fourier transform of a rectangle function is the sinc function, we see lobes in the frequency response plot. Because of these zeros, it makes the frequency response plot look as if the signal also contains lots of other frequencies corresponding to those zeros in the input signal of the DTFT, and these frequencies are present in the plot in the form of side lobes.

The difference between the results of `Problem 3.4` and `Problem 3.5` stem from the fact that the number of samples per period of the sinusoidal signal is an integer (20000/2500 = 8) in `Problem 3.4`, whereas in `Problem 3.5`, this number is a fractional number (20000/2501 ~= 7.997). This causes discontinuities in the sampled periodic signal and results in signal energy 'leaking' into adjacent bins of the FFT. This is called spectral leakage, and it makes the frequency spectrum look like many more frequencies are contained in the signal than there are in reality.

### Problem 3.6

### `code`
    Fs = 20000;         % Sampling frequency
	f1 = 2500;          % Signal frequency
	L = 200;            % Number of samples
	theta = 2*pi*f1/Fs;
	n = 0:1:L-1;
	f = (0:Fs/L:Fs-(Fs/L))/2;
	
	% Calculate the signal.
	x = sin(n*theta);
	
	X1 = fft(x,200);
	X2 = fft(x,256);
	
	subplot(2,1,1); semilogy(f, abs(X1)); title( 'Number of FFT samples: 200' );
	xlabel( 'Frequency (Hz) (\Delta f=50 Hz)' ); ylabel( 'Magnitude (dB)' ); axis tight;
	
	L = 256;
	f = (0:Fs/L:Fs-(Fs/L))/2;
	
	subplot(2,1,2); semilogy(f, abs(X2)); title( 'Number of FFT samples: 256' );
	xlabel( 'Frequency (Hz) (\Delta f ~= 39 Hz)' ); ylabel( 'Magnitude (dB)' ); axis tight;

### `plot`
![](assignment3/plots/problem36.png)

The results differ because the transform length of the second `fft` is not the same length as the window size. The `fft` function first chops or pads the input with zeroes before it perform an FFT, and because the transform length in the second `fft` is larger than the window size, an FFT is performed on a signal padded with an additional 56 zeros. Obviously, the FFT of a signal with padded zeros will be different since it now contains extra frequencies corresponding to those padded zeros.

### Problem 3.7

#### Problem 3.7.1

#### `code`
	N = 21;         % Window length
    Fs = 20000;     % Sampling frequency
    L = 512;        % Step size
    f = (0:Fs/L:Fs-(Fs/L))/2;
    
    % Different windows
    r = rectwin( N );
    t = triang( N );
    h = hamming( N );
    k = kaiser( N, 5.65 );
    
    % Plot the window functions
    figure;
    subplot(2,2,1); plot( r ); axis tight; title( 'Rectangular window' ); 
    xlabel( 'Sample n' ); ylabel( 'Amplitude' );
    
    subplot(2,2,2); plot( t ); axis tight; title( 'Triangular window' );
    xlabel( 'Sample n' ); ylabel( 'Amplitude' );
    
    subplot(2,2,3); plot( h ); axis tight; title( 'Hamming window' );
    xlabel( 'Sample n' ); ylabel( 'Amplitude' );
    
    subplot(2,2,4); plot( k ); axis tight; title( 'Kaiser window' );
    xlabel( 'Sample n' ); ylabel( 'Amplitude' );
    
    % Plot the normalized spectra of the window functions
    figure;
    R = abs( fft( r, L ) ); R = R / R( 1 ); semilogy( f, R, 'r' ); axis tight; hold on; 
    T = abs( fft( t, L ) ); T = T / T( 1 ); semilogy( f, T, 'm' ); axis tight; hold on;
    H = abs( fft( h, L ) ); H = H / H( 1 ); semilogy( f, H, 'b' ); axis tight; hold on;
    K = abs( fft( k, 512 ) ); K = K / K( 1 ); semilogy( f, K, 'k' ); axis tight; 

	title( 'Window spectra' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
    legend( 'Rectangle', 'Triangle', 'Hamming', 'Kaiser' );

#### Window `plot`
![](assignment3/plots/problem371_plots.png)

#### Spectra `plot`
![](assignment3/plots/problem371_spectra.png)

The main differences between the four window functions can be categorized into four categories: main lobe width (`MLW`), peak side lobe (`PSL`), side lobe roll-off (`SLR`), the number of side lobes (`NSL`), and width of the side lobes (`WSL`).

- `MLW (from most narrow to widest):` **rectangle, triangle, Hamming, Kaiser**.
- `PSL (from lowest to highest):` **Kaiser, Hamming, triangle, rectangle**.
- `SLR (from highest to lowest):` **Kaiser, triangle, rectangle, Hamming**.
- `NSL (from highest to lowest):` **Kaiser and rectangle, Hamming, triangle**.
- `WSL (from most narrow to widest):` **Kaiser, rectangle, Hamming, triangle**.

The `MLW` determines the spectral resolution; if it is wide then it is difficult to separate two frequencies that are close to each other. So it should be as narrow as possible.
The `PSL` determines the sensitivity; if it is high then it frequencies with a small amplitude that fall into the bucket where the side lobe is do not show up because they are 'drowned' in the side lobes.
The `SLR` also determines the sensitivity as is defines the ratio with which the magnitude of the side lobes decay the farther they are away from the frequencies contained in a signal.

#### Problem 3.7.2

#### `code`
	N = 21;     % Window size
	L = 512;    % Transform length
	f = (0:Fs/L:Fs-(Fs/L))/2;
	
	for i = 1:4
	    K = abs( fft( kaiser( N, 2*i + 1 ), L ) );
	    subplot( 2, 2, i ); semilogy( f, K ); axis tight;
	    title( strcat( 'Kaiser \beta = ', int2str( 2*i + 1 ) ) );
	    xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
	end

#### `plot`
![](assignment3/plots/problem372.png)

#### Problem 3.8

#### `code`
	load problem3_8;
	
	plot( y ); axis tight; xlabel( 'Sample n' ); ylabel( 'Amplitude' );

#### `plot`
![](assignment3/plots/problem38.png)

First we plot the signal and observe it to determine which window function will give the best results in determining the frequencies and amplitudes of the two sine waves. We know that the signal consists of two sine waves and by looking at the plot of the signal, we can see a high-frequency and a low-frequency wave. One of the waves has an amplitude of 1 whereas the other wave seems to have a small amplitude. From this we can conclude that the two frequencies will be relatively far apart in the frequency spectrum, and that the low-frequency wave will have a smaller peak.

Based on our assumptions we can evaluate which window function will be the most effective to find the two peaks in the frequency spectrum. Since the frequencies are far apart, we are not worried about spectral resolution so the main lobe width is not an important factor. Because the amplitudes of both sine waves are probably far apart, we need a window function that has a low peak side lobe and a steep side lobe roll-off so that spectral leakage has a lower magnitude than the peak of the low-frequency sine wave. Based on our results in `Problem 3.7.1`, the window function that fits our needs the best among the tested functions is the Kaiser window function. Also, in `Problem 3.7.2`, we have seen that the peak side lobe becomes lower as the β value increases.

Next we apply four Kaiser window functions with increasing β values to the signal and plot the FFT of the resulting signal.

#### `code`
	ws = 2*pi/L;
	wnorm = -pi:ws:pi;
	wnorm = wnorm(1:L);
	w = wnorm*Fs;

	k1 = kaiser( L, 7 );
	k2 = kaiser( L, 9 );
	k3 = kaiser( L, 11 );
	k4 = kaiser( L, 13 );
	
	yk1 = k1 .*y;
	yk2 = k2 .*y;
	yk3 = k3 .*y;
	yk4 = k4 .*y;
	
	figure;
	subplot( 2, 2, 1 ); semilogy( w, abs( fftshift( fft( yk1, L ) ) ) ); axis tight;
	title( '|Y_N(\omega)|, \beta = 7' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
	subplot( 2, 2, 2 ); semilogy( w, abs( fftshift( fft( yk2, L ) ) ) ); axis tight;
	title( '|Y_N(\omega)|, \beta = 9' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
	subplot( 2, 2, 3 ); semilogy( w, abs( fftshift( fft( yk3, L ) ) ) ); axis tight;
	title( '|Y_N(\omega)|, \beta = 11' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
	subplot( 2, 2, 4 ); semilogy( w, abs( fftshift( fft( yk4, L ) ) ) ); axis tight;
	title( '|Y_N(\omega)|, \beta = 13' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );

#### `plot`
![](assignment3/plots/problem38_fft_kaiser.png)

We can clearly see that there is a low-frequency peak in the frequency spectrum, and indeed, a larger β contributes to the visibility of that peak. To confirm that the chosen window function for this problem is indeed the most suited one, we also plotted the frequency spectra of the input signal with the rectangle, triangle, Hamming, and Kaiser (β=13) window functions.

#### `code`
	r = rectwin( L );
	t = triang( L );
	h = hamming( L );
	
	yr = r .* y;
	yt = t .* y;
	yh = h .* y;
	
	figure;
	semilogy( w, abs( fftshift( fft( yr, L ) ) ), 'r' ); axis tight; hold on;
	semilogy( w, abs( fftshift( fft( yt, L ) ) ), 'm' ); axis tight; hold on;
	semilogy( w, abs( fftshift( fft( yh, L ) ) ), 'b' ); axis tight; hold on;
	semilogy( w, abs( fftshift( fft( yk4, L ) ) ), 'k' ); axis tight;
	title( '|Y_N(\omega)|' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
	legend( 'Rectangle', 'Triangle', 'Hamming', 'Kaiser (\beta=13)' );

#### `plot`
![](assignment3/plots/problem38_comparison.png)

From the comparison plot we can conclude that our choice of using the Kaiser window function was indeed the correct one. Of the other window functions, only the triangle displayed a very small, visible peak for the low-frequency sine wave. The peak values are `25400/(2π)` and `5277/(2π)` rad/s which correspond to **4042.54 Hz** and **839.86 Hz** respectively, and the magnitudes are **165.9** and **0.001741** respectively. But since the precision is not infinite, these values are not exact but should be close to the real values.

By using the given formula for the two sine wave signal and plugging in the values we found, we calculate a new signal **z** and we apply the same Kaiser window function to it and calculate the FFT **Z**. By subtracting **Z** (the FFT of our new signal) from the FFT of the original signal **Y**, we calculate the error. For the amplitude `a2` we simply choose 1 for now and try to find the correct frequency of the high-frequency sine wave first. 

#### `code`
	f1 = 4042.54;
	f2 = 839.86; 
	
	theta1 = 2*pi*f1/Fs;
	theta2 = 2*pi*f2/Fs;
	n = 0:1:L-1;
	f = (0:Fs/L:Fs-(Fs/L))/2;
	
	z = sin( n*theta1 ) + sin( n*theta2 );
	
	zk4 = k4 .* z'; Zk4 = fft( zk4, L );
	figure; plot( w, abs( fftshift( Yk4 ) ) - abs( fftshift( Zk4 ) ) );% axis tight;
	title('|Y_N(\omega)| - |Z_N(\omega)|, \beta = 13'); xlabel('Frequency (rad/s)'); ylabel('Magnitude');

#### `plot`
![](assignment3/plots/problem38_first_guess.png)

We can see that the error for the low-frequency sine wave is very large compared to the other sine wave. By trying other frequencies close to the initial value several time, we find that high-frequency sine wave has a frequency of **4050 Hz** as the error is the smallest among all values from 4020 to 4060 Hz as can be seen below.

#### `plot`
![](assignment3/plots/problem38_first_correct.png)

Next we try to change amplitude `a2` of the low-frequency sine wave. If we divide 0.001741 by 165.9 (the amplitudes we found initially), then we get a value around **1*10^-5**. Using this value and plotting the error again we get the following:

#### `plot`
![](assignment3/plots/problem38_second_correct.png)

We can see that the error is much smaller now, and therefore the final values we have chosen are **theta1=4050 Hz**, **theta2=839.86 Hz**, and **a2=0.00001**. With these values, the plot given below looks quite similar to the plot of the original function.

#### `plot`
![](assignment3/plots/problem38_final.png)

## Assignment 4

#### Problem 4.1
The digital bandstop filter using the `fir1` function and Kaiser window is implemented as follows:

	% Normalized frequencies between 0 and 1 pi rad/sec.
    
    Fpass1 = 0.1;             % First Passband Frequency
    Fstop1 = 0.18;            % First Stopband Frequency
    Fstop2 = 0.62;            % Second Stopband Frequency
    Fpass2 = 0.7;             % Second Passband Frequency
    Dpass1 = 0.011512416822;  % First Passband Ripple
    Dstop  = 0.001;           % Stopband Attenuation
    Dpass2 = 0.011512416822;  % Second Passband Ripple
    flag   = 'scale';         % Sampling Flag
    
    [N,Wn,BETA,TYPE] = kaiserord([Fpass1 Fstop1 Fstop2 Fpass2], [1 0 1], ...
                                 [Dpass1 Dstop Dpass2]);
    
    b  = fir1(N, Wn, TYPE, kaiser(N+1, BETA), flag);

#### Impulse response plot
![](assignment4/plots/problem41.png)

#### Problem 4.2
The digital bandstop filter using the `firpm` function (equiripple) is implemented as follows:

	% Normalized frequencies between 0 and 1 pi rad/sec.
    
    Fpass1 = 0.1;             % First Passband Frequency
    Fstop1 = 0.18;            % First Stopband Frequency
    Fstop2 = 0.62;            % Second Stopband Frequency
    Fpass2 = 0.7;             % Second Passband Frequency
    Dpass1 = 0.011512416822;  % First Passband Ripple
    Dstop  = 0.001;           % Stopband Attenuation
    Dpass2 = 0.011512416822;  % Second Passband Ripple
    
    [N, Fo, Ao, W] = firpmord([Fpass1 Fstop1 Fstop2 Fpass2], [1 0 1], ...
                              [Dpass1 Dstop Dpass2]);
    
    b  = firpm(N, Fo, Ao, W);

#### Impulse response plot
![](assignment4/plots/problem42.png)

#### Problem 4.3
As can be seen, the most significant difference between the FIR bandstop filters of problem 4.1 and 4.2 is the minimum length or amount of required samples for the respective filters. The optimal equiripple FIR filter requires significantly less samples than the earlier FIR filter that uses the Kaiser windowing function.

#### Problem 4.4
For this problem, the coefficients were reduced to 16-bit, 12-bit and 10-bit and the respective attenuation plots are shown below, together with the original attenuation plot from problem 4.2

#### Original attenuation plot
![](assignment4/plots/problem44-original.png)

#### 16-bit attenuation plot
![](assignment4/plots/problem44-16bit.png)

#### 12-bit attenuation plot
![](assignment4/plots/problem44-12bit.png)

#### 10-bit attenuation plot
![](assignment4/plots/problem44-10bit.png)

The bit rounding that was applied to the coefficients does not seem to impact the sensitivity of the filter in the pass band, but does have an effect on the sensitivity in the stop band.

The supplied `bitround` function first normalizes the input (defaults to unity 1), then performs a multiplication so that the input falls within the range of the integers bounded between 0 and the amount of bits (`2^(b-1)`). A rounding is then performed so that the subsequent division (by the same `2^(b-1)`) scales the input back to the original range, ensuring that the resulting input is now quantized to the given amount of bits.

#### Problem 4.5
The problem describes the deformation of the signal spectrum as a `sinc` function. To compensate the signal, the inverse function `1./sinc` must be applied to the signal by means of a FIR filter before it is processed by the DAC.

We construct a FIR filter with order 20 with a transfer function that corresponds to the inverse sinc (with rectangular window):

    M = 20; L = M+1; k = -M/2:M/2;
    Ad_comp = ifftshift(1./sinc(k/L));
    h_comp=fir2(M,[0:(M/2)]/(M/2),Ad_comp(1:(M/2+1)),rectwin(L))
	freqz(h_comp);

The original DAC signal deformation is modelled as follows:

    Ad_org = ifftshift(sinc(k/L));
    h_org=fir2(M,[0:(M/2)]/(M/2),Ad_org(1:(M/2+1)),rectwin(L))
    freqz(h_org);

#### FIR compensation impulse plot
![](assignment4/plots/problem45-impulse.png)

#### Frequency response plot
![](assignment4/plots/problem45-all.png)

The original signal response, FIR compensation response and (combined) computed response are respectively visible as blue, green and red.

The desired frequency response that we should be left with would ideally be a constant 0 dB. The difference between the computed response and this ideal response is thus visible in the above plot as the red response line.

#### Problem 4.6
The minimum order for the IIR filters defined using the given approximations are as follows:

* Butterworth: 48
* Chebyshev (non-inverse): 22
* Cauer: 14

And corresponding code to implement the above IIR filters:

    Fpass1 = 0.1;         % First Passband Frequency
    Fstop1 = 0.18;        % First Stopband Frequency
    Fstop2 = 0.62;        % Second Stopband Frequency
    Fpass2 = 0.7;         % Second Passband Frequency
    Apass1 = 0.2;         % First Passband Ripple (dB)
    Astop  = 60;          % Stopband Attenuation (dB)
    Apass2 = 0.2;         % Second Passband Ripple (dB)
    
    h  = fdesign.bandstop(Fpass1, Fstop1, Fstop2, Fpass2, Apass1, Astop, ...
                          Apass2);
    
    % Butterworth
    H_b = design(h, 'butter', 'MatchExactly', 'stopband');
    
    % Chevyshev (non-inverting)
    H_c = design(h, 'cheby1', 'MatchExactly', 'stopband');
    
    % Cauer
    H_c = design(h, 'ellip', 'MatchExactly', 'both');


#### Problem 4.7
The plots for the Cauer function of problem 4.6 are below.

#### Attenuation plot
![](assignment4/plots/problem47-att.png)

#### Group delay plot
![](assignment4/plots/problem47-groupdelay.png)

#### Pole - zero diagram
![](assignment4/plots/problem47-pole.png)

#### Impulse response plot
![](assignment4/plots/problem47-impulse.png)

#### Step response plot
![](assignment4/plots/problem47-step.png)

#### Problem 4.8
The plots for the Cauer function of problem 4.6 with 12-bit coefficients are below.

The coefficient nominators and denominators (contained in the Matlab filter coefficient matrix) are easily rounded as follows:

    Hd.SOSMatrix = bitround(Hd.SOSMatrix, 12);

#### Attenuation plot
![](assignment4/plots/problem48-att.png)

#### Pole - zero diagram
![](assignment4/plots/problem48-pole.png)

#### Problem 4.9
The observed differences between the bit quantized versions of the FIR filter (of 4.2 to 4.4) versus IIR filter (of 4.6 to 4.8) are respectively more deviations in the stop-band versus more deviations in the pass-band. The quantized IIR filter seems to violate the characteristics of the pass-band (not easily visible in the graph due to resolution) while the quantized FIR filter seems to violate the characteristics of the stop-band.