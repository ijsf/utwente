figure;
sim( 'FIR22_before' );
plotfreq( simout );

figure;
for i = 1:8
    subplot(2,4,i);
    K = i;
    set_param( 'FIR22_after/b0', 'Gain', 'K' );
    sim( 'FIR22_after' );
    plotfreq( simout );
end