sim( 'FIR25' );
subplot( 1, 2, 1 );
stem( simout );
xlabel( 'Sample n' );
ylabel( 'Amplitude' );

subplot( 1, 2, 2 );
plot( simout );
xlabel( 'Sample n' );
ylabel( 'Amplitude' );