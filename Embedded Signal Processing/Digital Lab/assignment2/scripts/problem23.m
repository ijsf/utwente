% Generate unit impuls signal.
input = zeros( 64, 1 );
input( 1 ) = 1;

% Coefficients of the system.
a = [1 0 0.9];
b = [0.3 0.6 0.3];

% Calculate the impulse response.
impResp = filter( b, a, input );

stem( impResp );
xlabel( 'Sample n' );
ylabel( 'Amplitude' );

% Calculate the impulse response analytically.
n = 0;
