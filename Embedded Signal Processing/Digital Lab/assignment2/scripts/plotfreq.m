function plotfreq(s, logscale)
%PLOTFREQ(S, LOGSCALE) plots the magnitude of the spectrum of S.
%If the signal S is the impulse response of a linear time-invariant system, 
%the plot shown is also the frequency response of the system.
%
%When the second argument 'LOGSCALE' is given, the frequency axis is
%plotted using a logarithmic scale.
%
% EXAMPLE:
%
%   plotfreq(s);        plots the spectrum of s using a linear frequency
%   axis.
%
%   plotfreq(s, 'LOG'); plots the spectrum of s using a logarithmic
%   frequency axis.

% Code by N.A.Moseley - University of Twente, Enschede, The Netherlands

error(nargchk(1,2,nargin));

[H] = fft(s, 8192);

% plot frequency response

%subplot(211);
if (nargin == 2)
    semilogx([0:4095]/4095, 20*log10(abs(H(1:4096))));
else
    plot([0:4095]/4095, 20*log10(abs(H(1:4096))));
end

% check the y-axis and limit the minimum size to 3 dB either side of 0 dB!
v = axis;
v(3) = min(v(3), -3);
v(4) = max(v(4), 3);
axis(v);

% plot phase response

%subplot(212);
%phase = 180*unwrap(atan2(imag(H), real(H)))/pi;
%plot([0:4095]/4095, phase(1:4096));


% add axis descriptions
%subplot(211);
xlabel('Normalized frequency (2*F/Fs)');
ylabel('Magnitude [dB]');
grid on;

%subplot(212);
%xlabel('Normalized frequency (2*F/Fs)');
%ylabel('Phase [Degrees]');
%grid on;
