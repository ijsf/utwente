% Generate unit impuls signal.
t = 0:1/1000:1;
%input = sin( 2*pi*t );
input = ones( 1000, 1 );

% Coefficients of the system.
a = [1 0 0.9];
b = [0.3 0.6 0.3];

% Calculate the impulse response.
impResp = filter( b, a, input );

plotfreq( impResp );