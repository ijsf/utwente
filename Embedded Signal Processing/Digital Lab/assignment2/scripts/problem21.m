Amp = 1;            % amplitude
Freq = 0;           % frequency in Hz
Bias = 0;           % amplitude bias
Phase = 0;          % phase in radians
SampT = 1 / 1000;   % sample time in seconds
beginTime = 0;      % time to begin sampling in seconds
endTime = 1;        % time to end sampling in seconds
t = beginTime : SampT : endTime;    % sampling moments

% create the sinusoidal signals
Freq = 10; sin10 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 20; sin20 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 30; sin30 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 50; sin50 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 80; sin80 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 120; sin120 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 200; sin200 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 300; sin300 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;
Freq = 400; sin400 = Amp * sin( ( 2 * pi * Freq ) * t + Phase ) + Bias;

% feed the sinusoidal signals to the FIR filter
simin = timeseries( sin10, t );  sim( 'FIR_2' );  f10 = simout( 1, : );
simin = timeseries( sin20, t );  sim( 'FIR_2' );  f20 = simout( 1, : );
simin = timeseries( sin30, t );  sim( 'FIR_2' );  f30 = simout( 1, : );
simin = timeseries( sin50, t );  sim( 'FIR_2' );  f50 = simout( 1, : );
simin = timeseries( sin80, t );  sim( 'FIR_2' );  f80 = simout( 1, : );
simin = timeseries( sin120, t ); sim( 'FIR_2' ); f120 = simout( 1, : );
simin = timeseries( sin200, t ); sim( 'FIR_2' ); f200 = simout( 1, : );
simin = timeseries( sin300, t ); sim( 'FIR_2' ); f300 = simout( 1, : );
simin = timeseries( sin400, t ); sim( 'FIR_2' ); f400 = simout( 1, : );

% finally plot amplitude vs frequency
plot( [10 20 30 50 80 120 200 300 400 ], [abs( min( f10 ) ), abs( min( f20 ) ), abs( min( f30 ) ), abs( min( f50 ) ), abs( min( f80 ) ), abs( min( f120 ) ), abs( min( f200 ) ), abs( min( f300 ) ), abs( min( f400 ) )] );
xlabel( 'Frequency - Hz' );
ylabel( 'Amplitude' );