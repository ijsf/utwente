Fs = 20000;         % Sampling frequency
f1 = 2500;          % Signal frequency
L = 256;            % Number of samples
theta = 2*pi*f1/Fs;
n = 0:1:L-1;

% Calculate the signal.
x = sin(n*theta);

% Frequency from 0 to 10 kHz in 5 Hz steps converted to radians.
w = 2*pi*(0:5:10000)/Fs;

% Calculate the DTFT of x(n)
X = 0;

for k = 1:1:L
    X = X + x(k) * exp(-j*w*k);
end

delta = 5;
stepsize = Fs / 2 / delta;
f = (0:Fs/stepsize:Fs)/2;

% Plot the DTFT.
figure; semilogy(f, abs(X)); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' ); axis tight;