N = 21;         % Window length
Fs = 20000;     % Sampling frequency
L = 512;        % Step size
f = (0:Fs/L:Fs-(Fs/L))/2;

% Different windows
r = rectwin( N );
t = triang( N );
h = hamming( N );
k = kaiser( N, 5.65 );

% Plot the window functions
figure;
subplot(2,2,1); plot( r ); axis tight; title( 'Rectangular window' );
xlabel( 'Sample n' ); ylabel( 'Amplitude' );

subplot(2,2,2); plot( t ); axis tight; title( 'Triangular window' );
xlabel( 'Sample n' ); ylabel( 'Amplitude' );

subplot(2,2,3); plot( h ); axis tight; title( 'Hamming window' );
xlabel( 'Sample n' ); ylabel( 'Amplitude' );

subplot(2,2,4); plot( k ); axis tight; title( 'Kaiser window' );
xlabel( 'Sample n' ); ylabel( 'Amplitude' );

% Plot the normalized spectra of the window functions
figure;
R = abs( fft( r, L ) ); R = R / R( 1 ); semilogy( f, R, 'r' ); axis tight; hold on;
T = abs( fft( t, L ) ); T = T / T( 1 ); semilogy( f, T, 'm' ); axis tight; hold on;
H = abs( fft( h, L ) ); H = H / H( 1 ); semilogy( f, H, 'b' ); axis tight; hold on;
K = abs( fft( k, 512 ) ); K = K / K( 1 ); semilogy( f, K, 'k' ); axis tight;

title( 'Window spectra' ); xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
legend( 'Rectangle', 'Triangle', 'Hamming', 'Kaiser' );