load problem3_8;

%figure; plot( y ); axis tight; xlabel( 'Sample n' ); ylabel( 'Amplitude' );

ws = 2*pi/L;
wnorm = -pi:ws:pi;
wnorm = wnorm(1:L);
w = wnorm*Fs

k1 = kaiser( L, 7 );
k2 = kaiser( L, 9 );
k3 = kaiser( L, 11 );
k4 = kaiser( L, 13 );

yk1 = k1 .*y;
yk2 = k2 .*y;
yk3 = k3 .*y;
yk4 = k4 .*y;

figure;
subplot( 2, 2, 1 ); semilogy( w, abs( fftshift( fft( yk1, L ) ) ) ); axis tight;
title( '|Y_N(\omega)|, \beta = 7' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
subplot( 2, 2, 2 ); semilogy( w, abs( fftshift( fft( yk2, L ) ) ) ); axis tight;
title( '|Y_N(\omega)|, \beta = 9' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
subplot( 2, 2, 3 ); semilogy( w, abs( fftshift( fft( yk3, L ) ) ) ); axis tight;
title( '|Y_N(\omega)|, \beta = 11' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
subplot( 2, 2, 4 ); semilogy( w, abs( fftshift( fft( yk4, L ) ) ) ); axis tight;
title( '|Y_N(\omega)|, \beta = 13' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );

r = rectwin( L );
t = triang( L );
h = hamming( L );

yr = r .* y;
yt = t .* y;
yh = h .* y;

figure;
semilogy( w, abs( fftshift( fft( yr, L ) ) ), 'r' ); axis tight; hold on;
semilogy( w, abs( fftshift( fft( yt, L ) ) ), 'm' ); axis tight; hold on;
semilogy( w, abs( fftshift( fft( yh, L ) ) ), 'b' ); axis tight; hold on;
semilogy( w, abs( fftshift( fft( yk4, L ) ) ), 'k' ); axis tight;
title( '|Y_N(\omega)|' ); xlabel( 'Frequency (rad/s)' ); ylabel( 'Magnitude (dB)' );
legend( 'Rectangle', 'Triangle', 'Hamming', 'Kaiser (\beta=13)' );

% 25400 = 25400 / (2*pi) = 4043Hz
% 5277 = 5277 / (2*pi) = 840Hz

f1 = 4042;
f2 = 840;

theta1 = 2*pi*f1/Fs;
theta2 = 2*pi*f2/Fs;
n = 0:1:L-1;
f = (0:Fs/L:Fs-(Fs/L))/2;

z = sin( n*theta1 ) + sin( n*theta2 );

%figure; plot( z ); axis tight;