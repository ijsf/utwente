Fs = 20000;         % Sampling frequency
f1 = 2500;          % Signal frequency
L = 256;            % Number of samples
theta = 2*pi*f1/Fs;
n = 0:1:L-1;
f = (0:Fs/L:Fs-(Fs/L))/2;

% Calculate the signal.
x = sin(n*theta);

X = fft(x); semilogy(f, abs(X)); axis tight; xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );