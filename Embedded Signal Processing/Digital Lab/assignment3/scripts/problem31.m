Fs = 20000;         % Sampling frequency
f1 = 2500;          % Signal frequency
L = 256;            % Number of samples
phi = 2*pi*f1 / Fs;
n = 0:1:L;

% Calculate the signal.
x = sin(n*phi);

% Plot the signal.
stem(n,x); xlabel( 'Sample n' ); ylabel( 'Amplitude' );