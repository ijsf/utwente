Fs = 20000;         % Sampling frequency
f1 = 2500;          % Signal frequency
L = 200;            % Number of samples
theta = 2*pi*f1/Fs;
n = 0:1:L-1;
f = (0:Fs/L:Fs-(Fs/L))/2;

% Calculate the signal.
x = sin(n*theta);

X1 = fft(x,200);
X2 = fft(x,256);

subplot(2,1,1); semilogy(f, abs(X1)); title( 'Number of FFT samples: 200' );
xlabel( 'Frequency (Hz) (\Delta f=50 Hz)' ); ylabel( 'Magnitude (dB)' ); axis tight;

L = 256;
f = (0:Fs/L:Fs-(Fs/L))/2;

subplot(2,1,2); semilogy(f, abs(X2)); title( 'Number of FFT samples: 256' );
xlabel( 'Frequency (Hz) (\Delta f ~= 39 Hz)' ); ylabel( 'Magnitude (dB)' ); axis tight;