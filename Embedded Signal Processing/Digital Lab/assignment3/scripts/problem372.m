N = 21;     % Window size
L = 512;    % Transform length
f = (0:Fs/L:Fs-(Fs/L))/2;

for i = 1:4
    K = abs( fft( kaiser( N, 2*i + 1 ), L ) );
    subplot( 2, 2, i ); semilogy( f, K ); axis tight;
    title( strcat( 'Kaiser \beta = ', int2str( 2*i + 1 ) ) );
    xlabel( 'Frequency (Hz)' ); ylabel( 'Magnitude (dB)' );
end