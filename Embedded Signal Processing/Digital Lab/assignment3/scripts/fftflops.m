% fftflops plots the number of floating-point operations needed for an N-point FFT.
% this script needs 'fft_flops.mat'

% Code N.A. Moseley - Copyright University of Twente 2005

clf;
load fft_flops.mat;

semilogy(N,flops);
xlabel('length of fft')
ylabel('number of flops')
grid on;
