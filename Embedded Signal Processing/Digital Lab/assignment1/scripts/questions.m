% Question 1.1 - Give the command that returns the output vector y consisting of all oddly indexed elements of an input vector x.
x = 1:50;
y = x(1:2:end);

% Question 1.2
o = zeros( 1, ( size( y, 2 ) * 2 - 1 ) );
o(1:2:end) = y;

% Question 1.3
o(2:2:end) = ( o(1:2:end-1) + o(3:2:end) ) / 2;

% Question 1.4
randn( n, m ) % where n and m determine the size of the matrix.

% Question 1.5
toeplitz( [5 6 7 8 9], [5 4 3 2 1] );