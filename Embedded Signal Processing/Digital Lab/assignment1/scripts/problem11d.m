f_1 = 4000;         % 
f_s = 8000;         % sampling rate
mu = 600000;        % in Hz
phi = 3;            % random phase
end_time = 0.05;    % end time in seconds

t = 0 : 1 / f_s : end_time; % sampling moments

% chirp signal
y = cos( ( pi * mu * power( t, 2 ) ) + ( 2 * pi * f_1 * t ) + phi );

% momentaneous rate
f_m = mu * t + f_1;

% determine minimum and maximum rate
f_m_min = min( f_m );
f_m_max = max( f_m );

% display the discrete sequence plot
figure;
stem( t, y );
xlabel( 'time in seconds' );
ylabel( 'amplitude' );

% plot the chirp signal
figure;
plot( t, y );
xlabel( 'time in seconds' );
ylabel( 'amplitude' );