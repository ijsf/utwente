f_0 = 300;          % base frequency
f_s = 8000;         % sampling rate
phi = 3;            % random phase
end_time = 0.01;    % end time in seconds

t = 0 : 1 / f_s : end_time; % sampling moments.

% signal
y = sin( 2 * pi * f_0 * t + phi );

% display the discrete sequence plot
figure;
stem( t, y );
xlabel( 'time in seconds' );
ylabel( 'amplitude' );

% plot the signal
figure;
plot( t, y );
xlabel( 'time in seconds' );
ylabel( 'amplitude' );