function problem12_qe( t, y )
    % quantize the signal
    q16 = bitround( y, 16, 1 );
    q12 = bitround( y, 12, 1 );
    q8  = bitround( y, 8, 1 );
    q4  = bitround( y, 4, 1 );

    % display the discrete sequence plot
    figure;
    subplot( 2, 2, 1 );
    stem( t, y - q16 );
    xlabel( 'time in seconds' );
    ylabel( 'quantization error' );

    subplot( 2, 2, 2 );
    stem( t, y - q12 );
    xlabel( 'time in seconds' );
    ylabel( 'quantization error' );

    subplot( 2, 2, 3 );
    stem( t, y - q8 );
    xlabel( 'time in seconds' );
    ylabel( 'quantization error' );

    subplot( 2, 2, 4 );
    stem( t, y - q4 );
    xlabel( 'time in seconds' );
    ylabel( 'quantization error' );
end