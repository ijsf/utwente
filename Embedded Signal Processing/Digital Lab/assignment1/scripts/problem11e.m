f_1 = 4000;         % 
f_s = 8000;         % sampling rate
mu = 20000;         % in Hz
phi = 3;            % random phase
end_time = 2.0;     % end time in seconds

t = 0 : 1 / f_s : end_time; % sampling moments

% chirp signal
y = cos( ( pi * mu * power( t, 2 ) ) + ( 2 * pi * f_1 * t ) + phi );

% play the signal
wavplay( y, f_s );