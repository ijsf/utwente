function problem12_hist( t, y )
    % quantize the signal
    q16 = bitround( y, 16, 1 );
    q12 = bitround( y, 12, 1 );
    q8  = bitround( y, 8, 1 );
    q4  = bitround( y, 4, 1 );

    % display the discrete sequence plot
    figure;
    subplot( 2, 2, 1 );
    hist( y - q16, 64 );
    xlabel( 'quantization error' );
    ylabel( 'occurences' );

    subplot( 2, 2, 2 );
    hist( y - q12, 64 );
    xlabel( 'quantization error' );
    ylabel( 'occurences' );

    subplot( 2, 2, 3 );
    hist( y - q8, 64 );
    xlabel( 'quantization error' );
    ylabel( 'occurences' );

    subplot( 2, 2, 4 );
    hist( y - q4, 64 );
    xlabel( 'quantization error' );
    ylabel( 'occurences' );
end