f_0 = 300;          % base frequency
f_s = 8000;         % sampling rate
phi = 3;            % random phase
end_time = 0.01;    % end time in seconds

t = 0 : 1 / f_s : end_time; % sampling moments.

% chirp signal
y = sin( 2 * pi * f_0 * t + phi );

% plot the quantization errors
problem12_qe( t, y );

% plot the histograms
problem12_hist( t, y );

% normally distributed signal (standard normal distribution with mean zero
% and variance 1).
y = randn( 1, length( t ) );

% plot the quantization errors
problem12_qe( t, y );

% plot the histograms
problem12_hist( t, y );

% uniform signal
y = ( rand( 1, length( t ) ) * 2 - 1 );

% plot the quantization errors
problem12_qe( t, y );

% plot the histograms
problem12_hist( t, y );