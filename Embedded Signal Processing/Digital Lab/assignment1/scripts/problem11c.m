f_0 = 7525;          % base frequency
f_s = 8000;         % sampling rate
phi = 3;            % random phase
end_time = 0.01;    % end time in seconds

t = 0 : 1 / f_s : end_time; % sampling moments.

% display the discrete sequence plot
figure;
stem( t, [ sin( 2 * pi * f_0 * t' + phi ), sin( 2 * pi * ( f_0 + 125 ) * t' + phi ), sin( 2 * pi * ( f_0 + 250 ) * t' + phi ), sin( 2 * pi * ( f_0 + 375 ) * t' + phi ) ] );
xlabel( 'time in seconds' );
ylabel( 'amplitude' );