function br=bitround(b,bit,norm)
%
% br=bitround(b,bit,norm)
% rounds the coefficients of vector "b" to "bit" bits including
% sign!!!.
% Normalizes to "norm". "norm" defaults to 1.
%
if nargin == 2
   norm=1;
end
br=round(b./norm*2^(bit-1))./2^(bit-1);

