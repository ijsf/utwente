function [Hd, Hd16, Hd12, Hd10] = problem43

Fpass1 = 0.1;             % First Passband Frequency
Fstop1 = 0.18;            % First Stopband Frequency
Fstop2 = 0.62;            % Second Stopband Frequency
Fpass2 = 0.7;             % Second Passband Frequency
Dpass1 = 0.011512416822;  % First Passband Ripple
Dstop  = 0.001;           % Stopband Attenuation
Dpass2 = 0.011512416822;  % Second Passband Ripple
dens   = 16;              % Density Factor

[N, Fo, Ao, W] = firpmord([Fpass1 Fstop1 Fstop2 Fpass2], [1 0 1], ...
                          [Dpass1 Dstop Dpass2]);

b  = firpm(N, Fo, Ao, W, {dens});

Hd = dfilt.dffir(b);
Hd16 = dfilt.dffir(bitround(b,16));
Hd12 = dfilt.dffir(bitround(b,12));
Hd10 = dfilt.dffir(bitround(b,10));

% [EOF]
