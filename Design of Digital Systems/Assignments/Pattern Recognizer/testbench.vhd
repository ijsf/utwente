library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY testbench IS

END testbench;



ARCHITECTURE structure OF testbench IS
COMPONENT pattern_recogniser IS
	PORT( data : IN std_logic;
			reset: IN std_logic;
			clk:	 IN std_logic;
			seg1:	 OUT std_logic_vector( 6 DOWNTO 0 );
			seg2:  OUT std_logic_vector( 6 DOWNTO 0 )
			);
END COMPONENT;

COMPONENT testset IS
	PORT( data : OUT std_logic;
			reset: OUT std_logic;
			clk:	 OUT std_logic
			);
END COMPONENT;

signal data_i,clk_i,reset_i : std_logic;
	FOR pr : pattern_recogniser  USE ENTITY work.pattern_recogniser(behavioral);
	
BEGIN
	pr: pattern_recogniser PORT MAP (data => data_i, reset => reset_i, clk => clk_i, seg1=>open , seg2=>open);
	ts : testset PORT MAP (data => data_i, reset => reset_i, clk => clk_i);
END structure;