-- Copyright (C) 1991-2012 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 12.1 Build 243 01/31/2013 Service Pack 1 SJ Web Edition"

-- DATE "02/21/2014 12:20:07"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	pattern_recogniser IS
    PORT (
	data : IN std_logic;
	reset : IN std_logic;
	clk : IN std_logic;
	seg1 : OUT std_logic_vector(6 DOWNTO 0);
	seg2 : OUT std_logic_vector(6 DOWNTO 0)
	);
END pattern_recogniser;

-- Design Ports Information
-- seg1[0]	=>  Location: PIN_J13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg1[1]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg1[2]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg1[3]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg1[4]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg1[5]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg1[6]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg2[0]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg2[1]	=>  Location: PIN_N15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg2[2]	=>  Location: PIN_L13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg2[3]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg2[4]	=>  Location: PIN_L16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg2[5]	=>  Location: PIN_N16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg2[6]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_L15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data	=>  Location: PIN_L14,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF pattern_recogniser IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_data : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_seg1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_seg2 : std_logic_vector(6 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \seg1[0]~output_o\ : std_logic;
SIGNAL \seg1[1]~output_o\ : std_logic;
SIGNAL \seg1[2]~output_o\ : std_logic;
SIGNAL \seg1[3]~output_o\ : std_logic;
SIGNAL \seg1[4]~output_o\ : std_logic;
SIGNAL \seg1[5]~output_o\ : std_logic;
SIGNAL \seg1[6]~output_o\ : std_logic;
SIGNAL \seg2[0]~output_o\ : std_logic;
SIGNAL \seg2[1]~output_o\ : std_logic;
SIGNAL \seg2[2]~output_o\ : std_logic;
SIGNAL \seg2[3]~output_o\ : std_logic;
SIGNAL \seg2[4]~output_o\ : std_logic;
SIGNAL \seg2[5]~output_o\ : std_logic;
SIGNAL \seg2[6]~output_o\ : std_logic;
SIGNAL \count|cnt1~5_combout\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \data~input_o\ : std_logic;
SIGNAL \list|pattern_position[0]~2_combout\ : std_logic;
SIGNAL \list|pattern_position~3_combout\ : std_logic;
SIGNAL \list|process_0~0_combout\ : std_logic;
SIGNAL \list|pattern_position~5_combout\ : std_logic;
SIGNAL \list|pattern_position~4_combout\ : std_logic;
SIGNAL \list|match~0_combout\ : std_logic;
SIGNAL \list|match~q\ : std_logic;
SIGNAL \count|cnt1[3]~3_combout\ : std_logic;
SIGNAL \count|cnt1~0_combout\ : std_logic;
SIGNAL \count|cnt10~3_combout\ : std_logic;
SIGNAL \count|cnt10[3]~1_combout\ : std_logic;
SIGNAL \count|cnt10~0_combout\ : std_logic;
SIGNAL \count|Add0~1_combout\ : std_logic;
SIGNAL \count|cnt10~4_combout\ : std_logic;
SIGNAL \count|Add0~0_combout\ : std_logic;
SIGNAL \count|cnt10~2_combout\ : std_logic;
SIGNAL \count|cnt1~1_combout\ : std_logic;
SIGNAL \count|cnt1~2_combout\ : std_logic;
SIGNAL \count|Equal0~0_combout\ : std_logic;
SIGNAL \count|Add1~1_combout\ : std_logic;
SIGNAL \count|cnt1~6_combout\ : std_logic;
SIGNAL \count|Add1~0_combout\ : std_logic;
SIGNAL \count|cnt1~4_combout\ : std_logic;
SIGNAL \disp1|Mux6~0_combout\ : std_logic;
SIGNAL \disp1|Mux5~0_combout\ : std_logic;
SIGNAL \disp1|Mux4~0_combout\ : std_logic;
SIGNAL \disp1|Mux3~0_combout\ : std_logic;
SIGNAL \disp1|Mux2~0_combout\ : std_logic;
SIGNAL \disp1|Mux1~0_combout\ : std_logic;
SIGNAL \disp1|Mux0~0_combout\ : std_logic;
SIGNAL \disp2|Mux6~0_combout\ : std_logic;
SIGNAL \disp2|Mux5~0_combout\ : std_logic;
SIGNAL \disp2|Mux4~0_combout\ : std_logic;
SIGNAL \disp2|Mux3~0_combout\ : std_logic;
SIGNAL \disp2|Mux2~0_combout\ : std_logic;
SIGNAL \disp2|Mux1~0_combout\ : std_logic;
SIGNAL \disp2|Mux0~0_combout\ : std_logic;
SIGNAL \list|pattern_position\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \count|cnt10\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \count|cnt1\ : std_logic_vector(3 DOWNTO 0);

BEGIN

ww_data <= data;
ww_reset <= reset;
ww_clk <= clk;
seg1 <= ww_seg1;
seg2 <= ww_seg2;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);

-- Location: IOIBUF_X0_Y16_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOOBUF_X53_Y16_N9
\seg1[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp1|Mux6~0_combout\,
	devoe => ww_devoe,
	o => \seg1[0]~output_o\);

-- Location: IOOBUF_X53_Y14_N9
\seg1[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp1|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \seg1[1]~output_o\);

-- Location: IOOBUF_X53_Y20_N23
\seg1[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp1|Mux4~0_combout\,
	devoe => ww_devoe,
	o => \seg1[2]~output_o\);

-- Location: IOOBUF_X53_Y21_N23
\seg1[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp1|Mux3~0_combout\,
	devoe => ww_devoe,
	o => \seg1[3]~output_o\);

-- Location: IOOBUF_X53_Y14_N2
\seg1[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp1|Mux2~0_combout\,
	devoe => ww_devoe,
	o => \seg1[4]~output_o\);

-- Location: IOOBUF_X53_Y20_N16
\seg1[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp1|Mux1~0_combout\,
	devoe => ww_devoe,
	o => \seg1[5]~output_o\);

-- Location: IOOBUF_X53_Y15_N9
\seg1[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp1|Mux0~0_combout\,
	devoe => ww_devoe,
	o => \seg1[6]~output_o\);

-- Location: IOOBUF_X53_Y12_N2
\seg2[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp2|Mux6~0_combout\,
	devoe => ww_devoe,
	o => \seg2[0]~output_o\);

-- Location: IOOBUF_X53_Y9_N16
\seg2[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp2|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \seg2[1]~output_o\);

-- Location: IOOBUF_X53_Y10_N16
\seg2[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp2|Mux4~0_combout\,
	devoe => ww_devoe,
	o => \seg2[2]~output_o\);

-- Location: IOOBUF_X53_Y8_N23
\seg2[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp2|Mux3~0_combout\,
	devoe => ww_devoe,
	o => \seg2[3]~output_o\);

-- Location: IOOBUF_X53_Y11_N9
\seg2[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp2|Mux2~0_combout\,
	devoe => ww_devoe,
	o => \seg2[4]~output_o\);

-- Location: IOOBUF_X53_Y9_N23
\seg2[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp2|Mux1~0_combout\,
	devoe => ww_devoe,
	o => \seg2[5]~output_o\);

-- Location: IOOBUF_X53_Y13_N9
\seg2[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \disp2|Mux0~0_combout\,
	devoe => ww_devoe,
	o => \seg2[6]~output_o\);

-- Location: LCCOMB_X51_Y11_N8
\count|cnt1~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt1~5_combout\ = (!\reset~input_o\ & !\count|cnt1\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reset~input_o\,
	datac => \count|cnt1\(0),
	combout => \count|cnt1~5_combout\);

-- Location: IOIBUF_X53_Y11_N1
\reset~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: IOIBUF_X53_Y9_N8
\data~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data,
	o => \data~input_o\);

-- Location: LCCOMB_X51_Y11_N14
\list|pattern_position[0]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \list|pattern_position[0]~2_combout\ = (!\reset~input_o\ & \list|process_0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \reset~input_o\,
	datad => \list|process_0~0_combout\,
	combout => \list|pattern_position[0]~2_combout\);

-- Location: LCCOMB_X51_Y11_N22
\list|pattern_position~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \list|pattern_position~3_combout\ = (!\list|pattern_position\(0) & (\list|pattern_position[0]~2_combout\ & ((\list|pattern_position\(1)) # (!\list|pattern_position\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \list|pattern_position\(2),
	datab => \list|pattern_position\(1),
	datac => \list|pattern_position\(0),
	datad => \list|pattern_position[0]~2_combout\,
	combout => \list|pattern_position~3_combout\);

-- Location: FF_X51_Y11_N23
\list|pattern_position[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \list|pattern_position~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \list|pattern_position\(0));

-- Location: LCCOMB_X51_Y11_N12
\list|process_0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \list|process_0~0_combout\ = \data~input_o\ $ (((\list|pattern_position\(2)) # ((\list|pattern_position\(0) & \list|pattern_position\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \list|pattern_position\(2),
	datab => \data~input_o\,
	datac => \list|pattern_position\(0),
	datad => \list|pattern_position\(1),
	combout => \list|process_0~0_combout\);

-- Location: LCCOMB_X51_Y11_N24
\list|pattern_position~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \list|pattern_position~5_combout\ = (!\reset~input_o\ & (\list|process_0~0_combout\ & (\list|pattern_position\(0) $ (\list|pattern_position\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \list|pattern_position\(0),
	datab => \reset~input_o\,
	datac => \list|pattern_position\(1),
	datad => \list|process_0~0_combout\,
	combout => \list|pattern_position~5_combout\);

-- Location: FF_X51_Y11_N25
\list|pattern_position[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \list|pattern_position~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \list|pattern_position\(1));

-- Location: LCCOMB_X51_Y11_N30
\list|pattern_position~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \list|pattern_position~4_combout\ = (\list|pattern_position[0]~2_combout\ & ((\list|pattern_position\(0) & (\list|pattern_position\(1) $ (\list|pattern_position\(2)))) # (!\list|pattern_position\(0) & (\list|pattern_position\(1) & 
-- \list|pattern_position\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \list|pattern_position\(0),
	datab => \list|pattern_position\(1),
	datac => \list|pattern_position\(2),
	datad => \list|pattern_position[0]~2_combout\,
	combout => \list|pattern_position~4_combout\);

-- Location: FF_X51_Y11_N31
\list|pattern_position[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \list|pattern_position~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \list|pattern_position\(2));

-- Location: LCCOMB_X51_Y11_N0
\list|match~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \list|match~0_combout\ = (!\list|pattern_position\(0) & (!\list|pattern_position\(1) & (\list|pattern_position\(2) & \list|pattern_position[0]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \list|pattern_position\(0),
	datab => \list|pattern_position\(1),
	datac => \list|pattern_position\(2),
	datad => \list|pattern_position[0]~2_combout\,
	combout => \list|match~0_combout\);

-- Location: FF_X51_Y11_N1
\list|match\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \list|match~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \list|match~q\);

-- Location: LCCOMB_X51_Y11_N26
\count|cnt1[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt1[3]~3_combout\ = (\reset~input_o\) # (\list|match~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reset~input_o\,
	datad => \list|match~q\,
	combout => \count|cnt1[3]~3_combout\);

-- Location: FF_X51_Y11_N9
\count|cnt1[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt1~5_combout\,
	ena => \count|cnt1[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt1\(0));

-- Location: LCCOMB_X51_Y11_N16
\count|cnt1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt1~0_combout\ = (\count|cnt1\(0) & (!\count|cnt1\(1) & ((\count|cnt1\(2)) # (!\count|cnt1\(3))))) # (!\count|cnt1\(0) & (((\count|cnt1\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(0),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(2),
	datad => \count|cnt1\(1),
	combout => \count|cnt1~0_combout\);

-- Location: LCCOMB_X52_Y11_N8
\count|cnt10~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt10~3_combout\ = (!\count|cnt10\(0) & !\reset~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \count|cnt10\(0),
	datad => \reset~input_o\,
	combout => \count|cnt10~3_combout\);

-- Location: LCCOMB_X51_Y11_N20
\count|cnt10[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt10[3]~1_combout\ = (\reset~input_o\) # ((\list|match~q\ & !\count|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reset~input_o\,
	datab => \list|match~q\,
	datad => \count|Equal0~0_combout\,
	combout => \count|cnt10[3]~1_combout\);

-- Location: FF_X52_Y11_N9
\count|cnt10[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt10~3_combout\,
	ena => \count|cnt10[3]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt10\(0));

-- Location: LCCOMB_X52_Y11_N12
\count|cnt10~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt10~0_combout\ = (!\reset~input_o\ & (\count|cnt10\(0) $ (\count|cnt10\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \count|cnt10\(0),
	datac => \count|cnt10\(1),
	datad => \reset~input_o\,
	combout => \count|cnt10~0_combout\);

-- Location: FF_X52_Y11_N13
\count|cnt10[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt10~0_combout\,
	ena => \count|cnt10[3]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt10\(1));

-- Location: LCCOMB_X52_Y11_N14
\count|Add0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|Add0~1_combout\ = \count|cnt10\(2) $ (((\count|cnt10\(0) & \count|cnt10\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(1),
	combout => \count|Add0~1_combout\);

-- Location: LCCOMB_X52_Y11_N16
\count|cnt10~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt10~4_combout\ = (!\reset~input_o\ & (\count|Add0~1_combout\ & !\count|cnt1~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \reset~input_o\,
	datac => \count|Add0~1_combout\,
	datad => \count|cnt1~1_combout\,
	combout => \count|cnt10~4_combout\);

-- Location: FF_X52_Y11_N17
\count|cnt10[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt10~4_combout\,
	ena => \count|cnt10[3]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt10\(2));

-- Location: LCCOMB_X52_Y11_N0
\count|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|Add0~0_combout\ = \count|cnt10\(3) $ (((\count|cnt10\(1) & (\count|cnt10\(2) & \count|cnt10\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \count|Add0~0_combout\);

-- Location: LCCOMB_X52_Y11_N18
\count|cnt10~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt10~2_combout\ = (!\reset~input_o\ & ((\count|Add0~0_combout\) # (\count|cnt1~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \count|Add0~0_combout\,
	datac => \count|cnt1~1_combout\,
	datad => \reset~input_o\,
	combout => \count|cnt10~2_combout\);

-- Location: FF_X52_Y11_N19
\count|cnt10[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt10~2_combout\,
	ena => \count|cnt10[3]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt10\(3));

-- Location: LCCOMB_X52_Y11_N2
\count|cnt1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt1~1_combout\ = (!\count|cnt10\(1) & (!\count|cnt10\(2) & (\count|cnt10\(0) & \count|cnt10\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \count|cnt1~1_combout\);

-- Location: LCCOMB_X51_Y11_N4
\count|cnt1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt1~2_combout\ = (!\reset~input_o\ & ((\count|cnt1~0_combout\) # ((!\count|Equal0~0_combout\ & \count|cnt1~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reset~input_o\,
	datab => \count|cnt1~0_combout\,
	datac => \count|Equal0~0_combout\,
	datad => \count|cnt1~1_combout\,
	combout => \count|cnt1~2_combout\);

-- Location: FF_X51_Y11_N5
\count|cnt1[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt1~2_combout\,
	ena => \count|cnt1[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt1\(1));

-- Location: LCCOMB_X51_Y11_N6
\count|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|Equal0~0_combout\ = (\count|cnt1\(2)) # (((\count|cnt1\(1)) # (!\count|cnt1\(3))) # (!\count|cnt1\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(2),
	datab => \count|cnt1\(0),
	datac => \count|cnt1\(1),
	datad => \count|cnt1\(3),
	combout => \count|Equal0~0_combout\);

-- Location: LCCOMB_X51_Y11_N2
\count|Add1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|Add1~1_combout\ = \count|cnt1\(2) $ (((\count|cnt1\(0) & \count|cnt1\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(0),
	datac => \count|cnt1\(2),
	datad => \count|cnt1\(1),
	combout => \count|Add1~1_combout\);

-- Location: LCCOMB_X51_Y11_N10
\count|cnt1~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt1~6_combout\ = (!\reset~input_o\ & (\count|Equal0~0_combout\ & \count|Add1~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reset~input_o\,
	datac => \count|Equal0~0_combout\,
	datad => \count|Add1~1_combout\,
	combout => \count|cnt1~6_combout\);

-- Location: FF_X51_Y11_N11
\count|cnt1[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt1~6_combout\,
	ena => \count|cnt1[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt1\(2));

-- Location: LCCOMB_X51_Y11_N28
\count|Add1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|Add1~0_combout\ = \count|cnt1\(3) $ (((\count|cnt1\(0) & (\count|cnt1\(2) & \count|cnt1\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(0),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(2),
	datad => \count|cnt1\(1),
	combout => \count|Add1~0_combout\);

-- Location: LCCOMB_X51_Y11_N18
\count|cnt1~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \count|cnt1~4_combout\ = (!\reset~input_o\ & ((\count|Equal0~0_combout\ & (\count|Add1~0_combout\)) # (!\count|Equal0~0_combout\ & ((\count|cnt1~1_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reset~input_o\,
	datab => \count|Add1~0_combout\,
	datac => \count|Equal0~0_combout\,
	datad => \count|cnt1~1_combout\,
	combout => \count|cnt1~4_combout\);

-- Location: FF_X51_Y11_N19
\count|cnt1[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \count|cnt1~4_combout\,
	ena => \count|cnt1[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \count|cnt1\(3));

-- Location: LCCOMB_X52_Y14_N12
\disp1|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp1|Mux6~0_combout\ = (!\count|cnt1\(0) & (\count|cnt1\(3) $ (\count|cnt1\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(0),
	datab => \count|cnt1\(3),
	datad => \count|cnt1\(1),
	combout => \disp1|Mux6~0_combout\);

-- Location: LCCOMB_X52_Y14_N6
\disp1|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp1|Mux5~0_combout\ = (\count|cnt1\(1) & (!\count|cnt1\(3) & (!\count|cnt1\(0) & \count|cnt1\(2)))) # (!\count|cnt1\(1) & (\count|cnt1\(3) $ (((\count|cnt1\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(1),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(0),
	datad => \count|cnt1\(2),
	combout => \disp1|Mux5~0_combout\);

-- Location: LCCOMB_X52_Y14_N4
\disp1|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp1|Mux4~0_combout\ = (\count|cnt1\(1) & (!\count|cnt1\(3))) # (!\count|cnt1\(1) & ((\count|cnt1\(3) & ((!\count|cnt1\(2)))) # (!\count|cnt1\(3) & (\count|cnt1\(0) & \count|cnt1\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(1),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(0),
	datad => \count|cnt1\(2),
	combout => \disp1|Mux4~0_combout\);

-- Location: LCCOMB_X52_Y14_N10
\disp1|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp1|Mux3~0_combout\ = (\count|cnt1\(1) & (!\count|cnt1\(3) & ((\count|cnt1\(0)) # (!\count|cnt1\(2))))) # (!\count|cnt1\(1) & (\count|cnt1\(2) $ (((\count|cnt1\(3)) # (\count|cnt1\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000101110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(1),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(0),
	datad => \count|cnt1\(2),
	combout => \disp1|Mux3~0_combout\);

-- Location: LCCOMB_X52_Y14_N20
\disp1|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp1|Mux2~0_combout\ = (\count|cnt1\(0)) # ((\count|cnt1\(2)) # ((!\count|cnt1\(1) & \count|cnt1\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(1),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(0),
	datad => \count|cnt1\(2),
	combout => \disp1|Mux2~0_combout\);

-- Location: LCCOMB_X52_Y14_N22
\disp1|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp1|Mux1~0_combout\ = (\count|cnt1\(3) & (!\count|cnt1\(1) & ((!\count|cnt1\(2))))) # (!\count|cnt1\(3) & (\count|cnt1\(1) $ (((\count|cnt1\(0) & \count|cnt1\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(1),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(0),
	datad => \count|cnt1\(2),
	combout => \disp1|Mux1~0_combout\);

-- Location: LCCOMB_X52_Y14_N0
\disp1|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp1|Mux0~0_combout\ = (\count|cnt1\(3)) # ((\count|cnt1\(1) & ((!\count|cnt1\(2)) # (!\count|cnt1\(0)))) # (!\count|cnt1\(1) & ((\count|cnt1\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt1\(1),
	datab => \count|cnt1\(3),
	datac => \count|cnt1\(0),
	datad => \count|cnt1\(2),
	combout => \disp1|Mux0~0_combout\);

-- Location: LCCOMB_X52_Y11_N30
\disp2|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp2|Mux6~0_combout\ = (!\count|cnt10\(0) & (\count|cnt10\(3) $ (\count|cnt10\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \count|cnt10\(3),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(1),
	combout => \disp2|Mux6~0_combout\);

-- Location: LCCOMB_X52_Y11_N6
\disp2|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp2|Mux5~0_combout\ = (\count|cnt10\(1) & (\count|cnt10\(2) & (!\count|cnt10\(0) & !\count|cnt10\(3)))) # (!\count|cnt10\(1) & (\count|cnt10\(2) $ (((\count|cnt10\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000101001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \disp2|Mux5~0_combout\);

-- Location: LCCOMB_X52_Y11_N28
\disp2|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp2|Mux4~0_combout\ = (\count|cnt10\(1) & (((!\count|cnt10\(3))))) # (!\count|cnt10\(1) & ((\count|cnt10\(2) & (\count|cnt10\(0) & !\count|cnt10\(3))) # (!\count|cnt10\(2) & ((\count|cnt10\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \disp2|Mux4~0_combout\);

-- Location: LCCOMB_X52_Y11_N10
\disp2|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp2|Mux3~0_combout\ = (\count|cnt10\(1) & (!\count|cnt10\(3) & ((\count|cnt10\(0)) # (!\count|cnt10\(2))))) # (!\count|cnt10\(1) & (\count|cnt10\(2) $ (((\count|cnt10\(0)) # (\count|cnt10\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \disp2|Mux3~0_combout\);

-- Location: LCCOMB_X52_Y11_N20
\disp2|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp2|Mux2~0_combout\ = (\count|cnt10\(2)) # ((\count|cnt10\(0)) # ((!\count|cnt10\(1) & \count|cnt10\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \disp2|Mux2~0_combout\);

-- Location: LCCOMB_X52_Y11_N26
\disp2|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp2|Mux1~0_combout\ = (\count|cnt10\(2) & (!\count|cnt10\(3) & (\count|cnt10\(1) $ (\count|cnt10\(0))))) # (!\count|cnt10\(2) & (\count|cnt10\(1) $ (((\count|cnt10\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \disp2|Mux1~0_combout\);

-- Location: LCCOMB_X52_Y11_N24
\disp2|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \disp2|Mux0~0_combout\ = (\count|cnt10\(3)) # ((\count|cnt10\(1) & ((!\count|cnt10\(0)) # (!\count|cnt10\(2)))) # (!\count|cnt10\(1) & (\count|cnt10\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count|cnt10\(1),
	datab => \count|cnt10\(2),
	datac => \count|cnt10\(0),
	datad => \count|cnt10\(3),
	combout => \disp2|Mux0~0_combout\);

ww_seg1(0) <= \seg1[0]~output_o\;

ww_seg1(1) <= \seg1[1]~output_o\;

ww_seg1(2) <= \seg1[2]~output_o\;

ww_seg1(3) <= \seg1[3]~output_o\;

ww_seg1(4) <= \seg1[4]~output_o\;

ww_seg1(5) <= \seg1[5]~output_o\;

ww_seg1(6) <= \seg1[6]~output_o\;

ww_seg2(0) <= \seg2[0]~output_o\;

ww_seg2(1) <= \seg2[1]~output_o\;

ww_seg2(2) <= \seg2[2]~output_o\;

ww_seg2(3) <= \seg2[3]~output_o\;

ww_seg2(4) <= \seg2[4]~output_o\;

ww_seg2(5) <= \seg2[5]~output_o\;

ww_seg2(6) <= \seg2[6]~output_o\;
END structure;


