library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY compare IS
	PORT( reset		  : IN std_logic;
		  clk		  :	IN std_logic;
		  behav_seg1  : IN std_logic_vector( 6 DOWNTO 0 );
		  behav_seg2  : IN std_logic_vector( 6 DOWNTO 0 );
		  struct_seg1 : IN std_logic_vector( 6 DOWNTO 0 );
		  struct_seg2 : IN std_logic_vector( 6 DOWNTO 0 )			
		);
END compare;

ARCHITECTURE behavioral OF compare IS
BEGIN

	compare: PROCESS( behav_seg1, behav_seg2, struct_seg1, struct_seg2, reset, clk )
		VARIABLE b_seg1_i    : std_logic_vector( 6 DOWNTO 0 );
		VARIABLE b_seg2_i    : std_logic_vector( 6 DOWNTO 0 );
		VARIABLE s_seg1_i    : std_logic_vector( 6 DOWNTO 0 );
		VARIABLE s_seg2_i    : std_logic_vector( 6 DOWNTO 0 );
		VARIABLE f_reset : std_logic := '0';
	BEGIN
		IF rising_edge( clk ) THEN
			IF reset = '1' THEN
				IF f_reset = '0' THEN
					f_reset := '1';
				END IF;
			END IF;
			
			IF f_reset = '1' THEN
				IF reset = '0' THEN
					ASSERT ( b_seg1_i = s_seg1_i ) REPORT "seg1 differs!" SEVERITY failure;
					ASSERT ( b_seg2_i = s_seg2_i ) REPORT "seg2 differs!" SEVERITY failure;
				END IF;
			END IF;
		END IF;
		
		b_seg1_i := behav_seg1;
		b_seg2_i := behav_seg2;
		s_seg1_i := struct_seg1;
		s_seg2_i := struct_seg2;
		
	END PROCESS;
END ARCHITECTURE;
