LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY list_det IS
	PORT(	
			data:  IN std_logic;
			clk:   IN std_logic;
			reset: IN std_logic;
			match: OUT std_logic
		);
END ENTITY;

ARCHITECTURE behavioral OF list_det IS
	CONSTANT pattern : std_logic_vector( 0 TO 4 ) := "11100";
BEGIN
	PROCESS( clk )
		--VARIABLE pattern		  :	std_logic_vector( 0 TO 4 );
		VARIABLE pattern_position :	integer RANGE 0 TO 4;
	BEGIN
		IF rising_edge( clk ) THEN
			IF reset = '1' THEN
				--pattern 		  := "11100";
				pattern_position  := 0;
				match 			  <= '0';
			ELSE
				IF data = pattern( pattern_position ) THEN
					IF pattern_position = 4 THEN
						match <= '1';
						pattern_position := 0;
					ELSE
						match <= '0';
						pattern_position := pattern_position + 1;
					END IF; -- position update
				ELSE
					match <= '0';
					pattern_position := 0;
				END IF; -- data regonition
			END IF;
		END IF;
	END PROCESS;
END ARCHITECTURE;