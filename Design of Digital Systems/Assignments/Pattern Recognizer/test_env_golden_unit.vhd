library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY test_env_golden_unit IS

END ENTITY;

ARCHITECTURE structure OF test_env_golden_unit IS

	-- Compare Component
	COMPONENT compare IS
		PORT( reset: IN std_logic;
					clk:	 IN std_logic;
					behav_seg1:	 IN std_logic_vector( 6 DOWNTO 0 );
					behav_seg2:  IN std_logic_vector( 6 DOWNTO 0 );
					struct_seg1:	IN std_logic_vector( 6 DOWNTO 0 );
					struct_seg2: IN std_logic_vector( 6 DOWNTO 0 )			
					);
	END COMPONENT;
	
	-- Pattern recogniser component
	COMPONENT pattern_recogniser IS 
		PORT( data : IN std_logic;
				reset: IN std_logic;
				clk:	 IN std_logic;
				seg1:	 OUT std_logic_vector( 6 DOWNTO 0 );
				seg2:  OUT std_logic_vector( 6 DOWNTO 0 )
				);
	END COMPONENT;
	
	-- Test set component
	COMPONENT testset IS
		PORT( data : OUT std_logic;
				reset: OUT std_logic;
				clk:	 OUT std_logic
				);
	END COMPONENT;
	signal behav_seg1_i, behav_seg2_i, struct_seg1_i, struct_seg2_i: std_logic_vector( 6 DOWNTO 0 );
	signal clk_i, reset_i, data_i: std_logic;
	
	FOR pr_behav : pattern_recogniser  USE ENTITY work.pattern_recogniser(behavioral);
	FOR pr_struct : pattern_recogniser  USE ENTITY work.pattern_recogniser(structural);
	
BEGIN
	comp : compare 
		PORT MAP (
			reset => reset_i,
			clk => clk_i,
			behav_seg1 => behav_seg1_i,
			behav_seg2 => behav_seg2_i,
			struct_seg1 => struct_seg1_i,
			struct_seg2 => struct_seg2_i
		);
		
	ts : testset
		PORT MAP (
			data => data_i,
			reset => reset_i,
			clk => clk_i
		);
		
	pr_behav : pattern_recogniser 
		PORT MAP (
			data => data_i,
			reset => reset_i,
			clk   => clk_i,
			seg1 => behav_seg1_i,
			seg2 => behav_seg2_i
		);
	
	pr_struct : pattern_recogniser 
		PORT MAP (
			data => data_i,
			reset => reset_i,
			clk   => clk_i,
			seg1 => struct_seg1_i,
			seg2 => struct_seg2_i
		);
		
		
	
END ARCHITECTURE;