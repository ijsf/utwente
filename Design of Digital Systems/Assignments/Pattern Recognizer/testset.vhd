library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY testset IS
	PORT( data : OUT std_logic;
			reset: OUT std_logic;
			clk:	 OUT std_logic
			);
END testset;


ARCHITECTURE structure OF testset IS
	signal clk_i: std_logic;
	
	type state is (init, start, pattern110, pattern_reset, pattern_reset2, pattern50,done);
	
	signal current_state,next_state: state;
BEGIN
	
	clk <= clk_i;
	
	clock: PROCESS
		constant half_clock_period : time := 5 ns;
	BEGIN
			clk_i <= '1';
			wait for half_clock_period;
			clk_i <= '0';
			wait for half_clock_period;
	END PROCESS;
	
	
	stimuli: PROCESS (clk_i)
		constant pattern:				std_logic_vector( 0 to 4 ) := "11100";
		variable pattern_position:	integer range 0 to 4;
		variable pattern_count : integer range 0 to 160;
		
	BEGIN
		IF falling_edge( clk_i ) THEN
		
			CASE current_state IS
				WHEN init => 
					pattern_position := 0;
					pattern_count := 0;
					next_state	<= start;
					data <= '0';
					reset <= '1';
				WHEN start =>
					reset <= '0';
					next_state <= pattern110;
				WHEN pattern110 => 
					data <= pattern(pattern_position);
					
					-- Update position in pattern
					IF pattern_position < 4 THEN
						pattern_position := pattern_position + 1;
					ELSE 
						pattern_position := 0;
						pattern_count := pattern_count + 1;
					END IF;
					
					-- Set next state if count == 110
					IF pattern_count = 110 THEN
						next_state <= pattern_reset;
					END IF;
					
				WHEN pattern_reset =>
					reset <= '1';
					next_state <= pattern_reset2;
					
					-- Reset initial values
					pattern_position := 0;
					pattern_count := 0;
					
				WHEN pattern_reset2 =>
					reset <= '0';
					next_state <= pattern50;
					
				WHEN pattern50 =>
				  data <= pattern(pattern_position);
				  
					IF pattern_position < 4 THEN
						pattern_position := pattern_position + 1;
					ELSE 
						pattern_position := 0;
						pattern_count := pattern_count + 1;
					END IF;
					
					-- Set next state if count == 50
					IF pattern_count = 50 THEN
						next_state <= done;
					END IF;
					
				WHEN done => 
          ASSERT false REPORT "the input pattern is generated 50 times" SEVERITY failure;
			END CASE;
		END IF; -- clock wait
	END PROCESS;
	
	PROCESS (next_state)
	BEGIN
		current_state <= next_state;
	END PROCESS;
	
END structure;
