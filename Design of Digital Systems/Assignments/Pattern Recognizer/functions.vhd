library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


PACKAGE demo IS
	Function int2segm(inp :integer) return std_logic_vector;
END demo;	

PACKAGE BODY demo IS
	Function int2segm(inp :integer) return std_logic_vector is
		TYPE seg_type IS ARRAY( 0 to 10 ) OF std_logic_vector( 6 downto 0 );
			CONSTANT seg_value : seg_type :=
				( 0 => "0000000",
				  1 => "0011000",
				  2 => "1101101",
				  3 => "1111100",
				  4 => "1011010",
				  5 => "1110110",
				  6 => "1110111",
				  7 => "0011100",
				  8 => "1111111",
				  9 => "1111110",
				  10 => "1000000"
				);
		Begin

		return seg_value(inp);
	End int2segm;

END demo;