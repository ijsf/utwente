library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.demo.ALL;

ENTITY pattern_recogniser IS
	PORT( data : IN std_logic;
			reset: IN std_logic;
			clk:	 IN std_logic;
			seg1:	 OUT std_logic_vector( 6 DOWNTO 0 );
			seg2:  OUT std_logic_vector( 6 DOWNTO 0 )
			);
END pattern_recogniser;

ARCHITECTURE behavioral OF pattern_recogniser IS
	signal cnt1_i  : integer range 0 to 9;
	signal cnt10_i : integer range 0 to 9;
	signal overflow: boolean;
BEGIN
	PROCESS( clk )
		VARIABLE cnt1 : 			integer RANGE 0 TO 9;
		VARIABLE cnt10: 			integer RANGE 0 TO 9;
		VARIABLE pattern:			std_logic_vector( 0 TO 4 );
		VARIABLE pattern_position : integer range 0 TO 4;
	BEGIN
		IF rising_edge( clk ) THEN
			IF reset = '1' THEN
				cnt1  			 := 0;
				cnt10 			 := 0;
				pattern 		 := "11100";
				pattern_position := 0;
				overflow		 <= false;
			ELSE
				IF overflow = false THEN
					IF data = pattern( pattern_position ) THEN
						IF pattern_position = 4 THEN
							pattern_position := 0;
						
							IF cnt1 = 9 THEN
								IF cnt10 /= 9 THEN
									cnt1 := 0;
									cnt10 := cnt10 + 1;
								ELSE
									overflow <= true;
								END IF;
							ELSE
								cnt1 := cnt1 + 1;
							END IF; -- count checking
						ELSE
							pattern_position := pattern_position + 1;
						END IF; -- position update
					ELSE
						pattern_position := 0;
					END IF; -- data regonition
				END IF; -- overflow
			END IF;
		END IF;
		
		cnt1_i  <= cnt1;
		cnt10_i <= cnt10;
	END PROCESS;
	
	seg1 <= int2segm(cnt1_i);--std_logic_vector( to_unsigned( cnt1_i, seg1'length ) );
	seg2 <= int2segm(cnt10_i);--std_logic_vector( to_unsigned( cnt10_i, seg2'length ) );
END ARCHITECTURE;

ARCHITECTURE structural OF pattern_recogniser IS
	COMPONENT list_det IS
	PORT(	
			data:  IN std_logic;
			clk:   IN std_logic;
			reset: IN std_logic;
			match: OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT counter IS
	PORT(
			reset: IN std_logic;
			clk:	 IN std_logic;
			match:   IN std_logic;
			bcd1:	 OUT std_logic_vector( 3 DOWNTO 0 );
			bcd2:    OUT std_logic_vector( 3 DOWNTO 0 )			
		);
	END COMPONENT;
	
	COMPONENT disp_drv IS
	PORT(
			bcd:  IN std_logic_vector( 3 downto 0 );
			seg:  OUT std_logic_vector( 6 downto 0 )
		);
	END COMPONENT;

	SIGNAL match_i : std_logic;
	SIGNAL bcd1_i : std_logic_vector( 3 downto 0 );
	SIGNAL bcd2_i : std_logic_vector( 3 downto 0 );
BEGIN
	list : list_det 
	PORT MAP( data => data, 
			  clk => clk,
			  reset => reset,
			  match => match_i
			);
	
	count : counter
	PORT MAP( reset => reset,
			  clk => clk,
			  match => match_i,
			  bcd1 => bcd1_i,
			  bcd2 => bcd2_i
			);
	
	disp1 : disp_drv
	PORT MAP( bcd => bcd1_i,
			  seg => seg1
			);
			
	disp2 : disp_drv
	PORT MAP( bcd => bcd2_i,
			  seg => seg2
			);
END ARCHITECTURE;