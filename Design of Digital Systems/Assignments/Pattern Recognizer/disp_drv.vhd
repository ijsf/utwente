library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
USE work.demo.ALL;

ENTITY disp_drv IS
	PORT(	
			bcd:  IN std_logic_vector( 3 downto 0 );
			seg:  OUT std_logic_vector( 6 downto 0 )
		);
END ENTITY;

ARCHITECTURE behavioral OF disp_drv IS

BEGIN	
	seg <= int2segm( to_integer( unsigned(bcd )) );
END ARCHITECTURE;