LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
ENTITY counter IS 
	PORT(   reset: IN std_logic;
			clk:	 IN std_logic;
			match:   IN std_logic;
			bcd1:	 OUT std_logic_vector( 3 DOWNTO 0 );
			bcd2:    OUT std_logic_vector( 3 DOWNTO 0 )
			);
END counter;

ARCHITECTURE behaviour OF counter IS
	SIGNAL overflow: boolean;
	SIGNAL cnt1_i  : integer RANGE 0 TO 10;
	SIGNAL cnt10_i : integer RANGE 0 TO 10;
BEGIN
	PROCESS( clk )
		VARIABLE cnt1  : integer RANGE 0 TO 10;
		VARIABLE cnt10 : integer RANGE 0 TO 10;
	BEGIN
		IF rising_edge( clk ) THEN
			IF reset = '1' THEN
				cnt1  	 := 0;
				cnt10 	 := 0;
				overflow <= false;
			ELSE
				IF match = '1' THEN
					IF cnt1 = 9 THEN
						IF cnt10 /= 9 THEN
							cnt1 := 0;
							cnt10 := cnt10 + 1;
						ELSE
							overflow <= true;
							cnt1 := 10;
							cnt10 := 10;
						END IF;
					ELSE
						cnt1 := cnt1 + 1;
					END IF; -- count checking
				END IF;
			END IF;
			
			cnt1_i  <= cnt1;
			cnt10_i <= cnt10;
		END IF;
	END PROCESS;
	
	bcd1 <= std_logic_vector( to_unsigned( cnt1_i, bcd1'length ) );
	bcd2 <= std_logic_vector( to_unsigned( cnt10_i, bcd2'length ) );
	
END ARCHITECTURE;