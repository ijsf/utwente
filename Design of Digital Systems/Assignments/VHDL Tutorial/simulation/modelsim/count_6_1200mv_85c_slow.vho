-- Copyright (C) 1991-2012 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 12.1 Build 243 01/31/2013 Service Pack 1 SJ Web Edition"

-- DATE "02/06/2014 11:25:01"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	count IS
    PORT (
	a : IN std_logic_vector(7 DOWNTO 0);
	q : OUT std_logic_vector(3 DOWNTO 0)
	);
END count;

-- Design Ports Information
-- q[0]	=>  Location: PIN_P1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[1]	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[2]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[3]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[0]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[1]	=>  Location: PIN_A2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[2]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[3]	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[4]	=>  Location: PIN_T3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[5]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[6]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a[7]	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF count IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_a : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_q : std_logic_vector(3 DOWNTO 0);
SIGNAL \a[3]~input_o\ : std_logic;
SIGNAL \a[5]~input_o\ : std_logic;
SIGNAL \q[0]~output_o\ : std_logic;
SIGNAL \q[1]~output_o\ : std_logic;
SIGNAL \q[2]~output_o\ : std_logic;
SIGNAL \q[3]~output_o\ : std_logic;
SIGNAL \a[0]~input_o\ : std_logic;
SIGNAL \a[2]~input_o\ : std_logic;
SIGNAL \a[1]~input_o\ : std_logic;
SIGNAL \a[7]~input_o\ : std_logic;
SIGNAL \a[6]~input_o\ : std_logic;
SIGNAL \a[4]~input_o\ : std_logic;
SIGNAL \nmb~0_combout\ : std_logic;
SIGNAL \nmb~1_combout\ : std_logic;
SIGNAL \nmb~2_combout\ : std_logic;
SIGNAL \nmb~3_combout\ : std_logic;
SIGNAL \nmb~4_combout\ : std_logic;
SIGNAL \nmb~5_combout\ : std_logic;
SIGNAL \nmb~6_combout\ : std_logic;
SIGNAL \nmb~7_combout\ : std_logic;
SIGNAL \nmb~8_combout\ : std_logic;
SIGNAL \nmb~9_combout\ : std_logic;
SIGNAL \nmb~10_combout\ : std_logic;
SIGNAL \nmb~11_combout\ : std_logic;
SIGNAL \nmb~12_combout\ : std_logic;

BEGIN

ww_a <= a;
q <= ww_q;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOIBUF_X0_Y15_N1
\a[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(3),
	o => \a[3]~input_o\);

-- Location: IOIBUF_X0_Y11_N8
\a[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(5),
	o => \a[5]~input_o\);

-- Location: IOOBUF_X0_Y4_N23
\q[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \nmb~2_combout\,
	devoe => ww_devoe,
	o => \q[0]~output_o\);

-- Location: IOOBUF_X0_Y23_N23
\q[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \nmb~6_combout\,
	devoe => ww_devoe,
	o => \q[1]~output_o\);

-- Location: IOOBUF_X0_Y12_N9
\q[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \nmb~11_combout\,
	devoe => ww_devoe,
	o => \q[2]~output_o\);

-- Location: IOOBUF_X0_Y15_N9
\q[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \nmb~12_combout\,
	devoe => ww_devoe,
	o => \q[3]~output_o\);

-- Location: IOIBUF_X0_Y12_N1
\a[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(0),
	o => \a[0]~input_o\);

-- Location: IOIBUF_X0_Y10_N22
\a[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(2),
	o => \a[2]~input_o\);

-- Location: IOIBUF_X7_Y34_N8
\a[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(1),
	o => \a[1]~input_o\);

-- Location: IOIBUF_X0_Y23_N15
\a[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(7),
	o => \a[7]~input_o\);

-- Location: IOIBUF_X0_Y11_N1
\a[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(6),
	o => \a[6]~input_o\);

-- Location: IOIBUF_X1_Y0_N1
\a[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_a(4),
	o => \a[4]~input_o\);

-- Location: LCCOMB_X1_Y12_N0
\nmb~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~0_combout\ = \a[5]~input_o\ $ (\a[7]~input_o\ $ (\a[6]~input_o\ $ (\a[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[5]~input_o\,
	datab => \a[7]~input_o\,
	datac => \a[6]~input_o\,
	datad => \a[4]~input_o\,
	combout => \nmb~0_combout\);

-- Location: LCCOMB_X1_Y12_N2
\nmb~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~1_combout\ = \a[3]~input_o\ $ (\a[2]~input_o\ $ (\a[1]~input_o\ $ (\nmb~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[3]~input_o\,
	datab => \a[2]~input_o\,
	datac => \a[1]~input_o\,
	datad => \nmb~0_combout\,
	combout => \nmb~1_combout\);

-- Location: LCCOMB_X1_Y12_N28
\nmb~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~2_combout\ = \a[0]~input_o\ $ (\nmb~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[0]~input_o\,
	datad => \nmb~1_combout\,
	combout => \nmb~2_combout\);

-- Location: LCCOMB_X1_Y12_N22
\nmb~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~3_combout\ = (\a[5]~input_o\ & ((\a[7]~input_o\ & ((!\a[4]~input_o\) # (!\a[6]~input_o\))) # (!\a[7]~input_o\ & ((\a[6]~input_o\) # (\a[4]~input_o\))))) # (!\a[5]~input_o\ & ((\a[7]~input_o\ & ((\a[6]~input_o\) # (\a[4]~input_o\))) # (!\a[7]~input_o\ 
-- & (\a[6]~input_o\ & \a[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[5]~input_o\,
	datab => \a[7]~input_o\,
	datac => \a[6]~input_o\,
	datad => \a[4]~input_o\,
	combout => \nmb~3_combout\);

-- Location: LCCOMB_X1_Y12_N24
\nmb~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~4_combout\ = \nmb~3_combout\ $ (((\a[3]~input_o\ & ((\a[2]~input_o\) # (\nmb~0_combout\))) # (!\a[3]~input_o\ & (\a[2]~input_o\ & \nmb~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[3]~input_o\,
	datab => \a[2]~input_o\,
	datac => \nmb~3_combout\,
	datad => \nmb~0_combout\,
	combout => \nmb~4_combout\);

-- Location: LCCOMB_X1_Y12_N26
\nmb~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~5_combout\ = (\a[1]~input_o\ & (\a[3]~input_o\ $ (\a[2]~input_o\ $ (\nmb~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[3]~input_o\,
	datab => \a[2]~input_o\,
	datac => \a[1]~input_o\,
	datad => \nmb~0_combout\,
	combout => \nmb~5_combout\);

-- Location: LCCOMB_X1_Y12_N20
\nmb~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~6_combout\ = \nmb~4_combout\ $ (\nmb~5_combout\ $ (((\a[0]~input_o\ & \nmb~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[0]~input_o\,
	datab => \nmb~4_combout\,
	datac => \nmb~5_combout\,
	datad => \nmb~1_combout\,
	combout => \nmb~6_combout\);

-- Location: LCCOMB_X1_Y12_N6
\nmb~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~7_combout\ = (\a[0]~input_o\ & \nmb~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[0]~input_o\,
	datad => \nmb~1_combout\,
	combout => \nmb~7_combout\);

-- Location: LCCOMB_X1_Y12_N16
\nmb~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~8_combout\ = (((!\a[4]~input_o\) # (!\a[6]~input_o\)) # (!\a[7]~input_o\)) # (!\a[5]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[5]~input_o\,
	datab => \a[7]~input_o\,
	datac => \a[6]~input_o\,
	datad => \a[4]~input_o\,
	combout => \nmb~8_combout\);

-- Location: LCCOMB_X1_Y12_N10
\nmb~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~9_combout\ = (\a[3]~input_o\ & ((\nmb~0_combout\ & (\nmb~8_combout\)) # (!\nmb~0_combout\ & ((\a[2]~input_o\))))) # (!\a[3]~input_o\ & (((\a[2]~input_o\ & \nmb~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \a[3]~input_o\,
	datab => \nmb~8_combout\,
	datac => \a[2]~input_o\,
	datad => \nmb~0_combout\,
	combout => \nmb~9_combout\);

-- Location: LCCOMB_X1_Y12_N12
\nmb~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~10_combout\ = \nmb~8_combout\ $ (((!\nmb~9_combout\) # (!\nmb~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \nmb~8_combout\,
	datac => \nmb~3_combout\,
	datad => \nmb~9_combout\,
	combout => \nmb~10_combout\);

-- Location: LCCOMB_X1_Y12_N14
\nmb~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~11_combout\ = \nmb~10_combout\ $ (((\nmb~7_combout\ & ((\nmb~4_combout\) # (\nmb~5_combout\))) # (!\nmb~7_combout\ & (\nmb~4_combout\ & \nmb~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011111101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nmb~7_combout\,
	datab => \nmb~4_combout\,
	datac => \nmb~5_combout\,
	datad => \nmb~10_combout\,
	combout => \nmb~11_combout\);

-- Location: LCCOMB_X1_Y12_N8
\nmb~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \nmb~12_combout\ = (\nmb~10_combout\ & ((\nmb~7_combout\ & ((\nmb~4_combout\) # (\nmb~5_combout\))) # (!\nmb~7_combout\ & (\nmb~4_combout\ & \nmb~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nmb~7_combout\,
	datab => \nmb~4_combout\,
	datac => \nmb~5_combout\,
	datad => \nmb~10_combout\,
	combout => \nmb~12_combout\);

ww_q(0) <= \q[0]~output_o\;

ww_q(1) <= \q[1]~output_o\;

ww_q(2) <= \q[2]~output_o\;

ww_q(3) <= \q[3]~output_o\;
END structure;


