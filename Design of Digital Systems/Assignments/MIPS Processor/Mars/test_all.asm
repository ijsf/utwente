.data 0x00002000
pi57: .word 57 
pi76: .word 76
pi5:  .word 5

mi57: .word -57
mi76: .word -76
mi5:  .word -5

mi1:  .word -1
zi:   .word 0
pi1:  .word 1

mi4:	.word -4

res1: .word 0
res2: .word 0
res3: .word 0
res4: .word 0
res5: .word 0

x80000000: .word 0x80000000
x00003000: .word 0x00003000
x00002000: .word 0x00002000

mx80000000: .word -2147483648

jmp_over: .word jump_over

N_list:	.word 0
	.word 1
	.word 23
	.word 0xFFFFFFFE
	.word 0xFFFFFFFF
N_list_size:  .word 20 # 5 * 4
	
D_list: .word 0
	.word 1
	.word 2
	.word 23
	.word 0xFFFFFFFF
D_list_size:  .word 20 # 5 * 4

result: .space 200 # 5 * 5 * 2 * 4
result_end:

.data 0x00002800
.word 0xDEADC0DE
.word 0xBEEFBEEF

.text 

# Test LW with positive and negative offsets
LW $t7, x00002000 	# positive offset
LW $t6, x00003000
LW $t0, 0x800($t7) 	# 0x00002800
LW $t1, -0x7FC($t6) 	# negative offset 0x00002804

## Test SW with positive and negative offsets
SW $t0  0x804($t7)  	# positive offset  0x00002804
SW $t1 -0x800($t6)	# negative offset  0x00002800

LW $t0, pi57
LW $t1, pi76
LW $t2, pi5
LW $t3, mi57
LW $t4, mi76
LW $t5, mi5
LW $t6, x80000000

# Test ADD with positive and negative numbers
ADDU $t7, $t0, $t1	# result = 133 = 0x85
SW $t7, res1
ADDU $t7, $t0, $t2	# result = -19 = 0xFFFFFFED
SW $t7, res2
ADDU $t7, $t3, $t4	# result = -133 = 0xFFFFFF7B
SW $t7, res3

# Test ADD for overflow and underflow
ADDU $t6, $t6, $t6	# overflow
SW $t6, res4

LW $t6, mx80000000
LW $t7, mx80000000
ADDU $t6, $t6, $t7 	# underflow
SW $t6, res5

# Test DIVU, MFHI, and MFLO
DIVU $t0, $t2
MFHI $t0	# contains $t0 / $t2
MFLO $t0	# contains $t0 mod $t2

# test BGTZ
bgtz_1:
LW $t7, pi1	# load '1' into $t7
BGTZ $t7, bgtz_4

bgtz_2:
LW $t7, zi	# load '0' into $t7
BGTZ $t7, bgtz_1

bgtz_3:
LW $t7, pi5
BGTZ $t7, bgtz_end

bgtz_4:
LW $t7, mi1	# load '-1' into $t7
BGTZ $t7, bgtz_1

bgtz_5:
LW $t7, pi57
BGTZ $t7, bgtz_2

bgtz_end:
## test SRLV
LW $t0, x80000000
LW $t1, pi5

SRLV $t2, $t0, $t1

LW $t1, pi57

SRLV $t3, $t0, $t1

### SLTIU ### 
LW $t1, pi57
SLTIU $t0, $t1, 50
SLTIU $t0, $t1, 57
SLTIU $t0, $t1, 60

### JR
LW $t0, jmp_over
JR $t0

syscall	# Will be jumped over

### JAL
jump_to:
JR $ra

jump_over:
JAL jump_to

# End program
end_program:

LW $t1, pi1
LW $t2, mi4
LW $t3, zi

LW $t4, N_list_size
loop_i:
ADDU $t4, $t4, $t2
LW $t6, N_list($t4)

LW $t5, D_list_size
loop_j:
ADDU $t5, $t5, $t2
LW $t7, D_list($t5)

DIVU $t6, $t7

MFHI $t7
SW $t7, result_end($t3)	
ADDU $t3, $t3, $t2

MFLO $t7
SW $t7, result_end($t3)
ADDU $t3, $t3, $t2

BGTZ $t5, loop_j
		
BGTZ $t4, loop_i

syscall
