# Compute first twelve Fibonacci numbers and put in array, then print
      .data 0x2000
size: .word  40             # size of "array" 
data_adress: .word 0x2000   # address of fibs 
one: .word 1
m_one: .word -1
p_four: .word 4

#.data 0x2100
#fibs: .word   0 : 100      # "array" of 12 words to contain fib values

      .text 0x0000
      lw   $t0, data_adress # load address of array
      lw   $t5, size        # load address of size variable
      
      lw   $t2, one         # 1 is first and second Fib. number
      
      lw $s1, m_one
      lw $s4, p_four
      
      sw   $t2, 0($t0)      # F[0] = 1
      sw   $t2, 4($t0)      # F[1] = F[0] = 1
      
      addu $t1, $t5, $s1    # Counter for loop, will execute (size-2) times
      addu  $t1, $t1, $s1     
      
loop: lw   $t3, 0($t0)		# Get value from array F[n] 
      lw   $t4, 4($t0)      # Get value from array F[n+1]
      addu  $t2, $t3, $t4	# $t2 = F[n] + F[n+1]
      sw   $t2, 8($t0)      # Store F[n+2] = F[n] + F[n+1] in array
      addu $t0, $t0, $s4    # increment address of Fib. number source
      addu  $t1, $t1, $s1   # decrement loop counter
      bgtz $t1, loop        # repeat if not finished yet.

syscall