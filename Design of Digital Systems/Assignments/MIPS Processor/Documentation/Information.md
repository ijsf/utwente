# Design of Digital Systems #
###MIPS Assignment (2013-2014)###
######Oscar Starink S1378694, Qiang Fu s1409840, Ruud Harmsen s0165999, O. Meteer (s0140147)######
----------

## Teams ##
- Oscar & Qiang: Behavioral description of the processor
- Ruud & Oguz: Testbench & memory

## Instructions ##

### ADD ###
Add (with overflow)

### SRLV ###
Shift right logical variable

### SLTIU ### 
Set on less than immediate unsigned

### BGTZ
Branch on greater than zero

### LW
Load word

### SW
Store word

### JAL
Jump and link

### JR
Jump register

### DIVU
Divide unsigned

### MFHI
Move from HI

### MFLO
Move from LO


## Design decisions ##

- Registers reside in main memory.
- One access point to the main memory.
- Separate instructions based on whether the result is written to memory or not. When instructions are done can easily be determined for instructions that write results to memory. For the other instructions we have yet to determine how to check if they are done.

## Entities ##

### Processor ###

- GENERIC WORD_SIZE (32 bits in our case)

- data_in 32-bit
- data_out 32-bit
- address 32-bit
- nRD_WR 1-bit ( 0 = read, 1 = write )
- enable 1-bit
- clk 1-bit
- reset 1-bit

### Memory ###

- GENERIC WORD_SIZE (32 bits in our case)
- GENERIC FILENAME (contains the machine code to be loaded into memory)
- GENERIC REAL_SIZE (defines the real size of the memory (so not 4GiB))

- data_in 32-bit
- data_out 32-bit
- address 32-bit (last two bits are ignored since we read/write 4 bytes (1 word) at a time)
- nRD_WR 1-bit
- clk 1-bit (reading and writing takes 1 clock cycle)
- enable 1-bit