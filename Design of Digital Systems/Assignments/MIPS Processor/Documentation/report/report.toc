\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Testing}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Approach}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Testbench}{3}{section.2.2}
\contentsline {chapter}{\numberline {3}Architectures}{4}{chapter.3}
\contentsline {section}{\numberline {3.1}General Description}{4}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Memory}{4}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Behavioral / Golden Unit}{5}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Correctness / Testing}{5}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Algorithmic}{5}{section.3.3}
\contentsline {section}{\numberline {3.4}Structural}{5}{section.3.4}
\contentsline {chapter}{\numberline {4}Instructions}{6}{chapter.4}
\contentsline {section}{\numberline {4.1}SRLV}{6}{section.4.1}
\contentsline {section}{\numberline {4.2}SLTIU}{6}{section.4.2}
\contentsline {section}{\numberline {4.3}BGTZ}{7}{section.4.3}
\contentsline {section}{\numberline {4.4}LW}{7}{section.4.4}
\contentsline {section}{\numberline {4.5}SW}{7}{section.4.5}
\contentsline {section}{\numberline {4.6}JR}{7}{section.4.6}
\contentsline {section}{\numberline {4.7}JAL}{7}{section.4.7}
\contentsline {section}{\numberline {4.8}DIVU}{7}{section.4.8}
\contentsline {subsubsection}{1. Euclidian Division, Division by repeated subtraction}{7}{section*.2}
\contentsline {subsubsection}{2. Restoring Unsigned Division Algorithm}{8}{section*.3}
\contentsline {subsubsection}{3. Nonrestoring unsigned division}{8}{section*.4}
\contentsline {section}{\numberline {4.9}MFHI}{9}{section.4.9}
\contentsline {section}{\numberline {4.10}MFLO}{9}{section.4.10}
\contentsline {section}{\numberline {4.11}Comparison}{9}{section.4.11}
\contentsline {subsubsection}{Choice}{10}{section*.5}
