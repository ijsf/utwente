LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY processor IS 
	PORT( clk      : IN  std_logic;
		  reset    : IN  std_logic;
		  data_in  : IN  std_logic_vector( 31 DOWNTO 0 ); 
		  data_out : OUT std_logic_vector( 31 DOWNTO 0 ); 
		  address  : OUT std_logic_vector( 31 DOWNTO 0 ); 
		  rd_wr    : OUT std_logic; --( 0 = read, 1 = write )
		  enable   : OUT std_logic;
		  syscall  : OUT std_logic
		); 
END;
 
