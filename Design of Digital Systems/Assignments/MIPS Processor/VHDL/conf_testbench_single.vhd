CONFIGURATION conf_testbench_single OF testbench IS
  for single
    for pr: processor use entity work.processor(behavior);
    end for;
    for mem : memory use entity work.memory(behavior)
    GENERIC MAP( WORD_SIZE 	  => 32,
				 CODE_FILE 	  => "../Mars/test1_text_0x0000.bin",
				 CODE_ADDRESS => 0,
				 CODE_SIZE	  => 36,
				 DATA_FILE	  => "../Mars/test1_data_0x2000.bin",
				 DATA_ADDRESS => 8192, -- 0x2000
				 DATA_SIZE	  => 4096, -- 4kB
				 REG_ADDRESS  => 12288, -- 0x3000
				 REG_SIZE	  => 128 -- 32 registers
				);
    end for;
  end for;
END;
