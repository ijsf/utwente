LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY WORK;
USE WORK.control_pack.ALL;
USE WORK.functional_units_pack.ALL;

ARCHITECTURE behavior OF datapath IS 
	SIGNAL HI 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL LO 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL PC 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL A 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL B 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL O 	: std_logic_vector( 31 DOWNTO 0 ); -- OUT reg
	
	SIGNAL HI_n 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL LO_n 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL PC_n 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL A_n 		: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL B_n 		: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL O_n 		: std_logic_vector( 31 DOWNTO 0 ); -- OUT reg

	SIGNAL alu_n 	: std_logic_vector( 31 DOWNTO 0 ); -- Interal signal for alu (divu instruction)
	
	SIGNAL left 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL right 	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL alu 		: std_logic_vector( 31 DOWNTO 0 ); 
	
	 
	SIGNAL shift_out : std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL shift_lo : std_logic_vector( 31 DOWNTO 0 ); 

	SIGNAL alu_c_i 	: std_logic;
BEGIN
	regs : PROCESS( clk ) 	
	BEGIN
		IF rising_edge( clk ) THEN
			IF( reset = '1' ) THEN
				HI <= ( OTHERS => '0' );
				LO <= ( OTHERS => '0' );
				PC <= ( OTHERS => '0' );
				A  <= ( OTHERS => '0' );
				B  <= ( OTHERS => '0' );
				O  <= ( OTHERS => '0' );
			ELSE
				HI <= HI_n;
				LO <= LO_n;
				PC <= PC_n;
				A  <= A_n;
				B  <= B_n;
				O  <= O_n;
			END IF;
		END IF;
	END PROCESS;
	
	HI_n <= HI 					  WHEN mux_hi = mux_hi_hi ELSE
			  alu_n 				  WHEN mux_hi = mux_hi_alu ELSE
			  ( OTHERS => '0' ) WHEN mux_hi = mux_hi_zero ELSE
			  HI;
			
	LO_n <= LO 		  WHEN mux_lo = mux_lo_lo ELSE
			  shift_lo WHEN mux_lo = mux_lo_shift ELSE
			  data_in  WHEN mux_lo = mux_lo_di ELSE
			  LO;
	
	PC_n <= PC 										     WHEN mux_pc = mux_pc_pc ELSE
			  alu 										  WHEN mux_pc = mux_pc_alu ELSE
			  data_in  									  WHEN mux_pc = mux_pc_di ELSE
			  PC( 31 DOWNTO 28 ) & intr_pc & "00" WHEN mux_pc = mux_pc_intr ELSE
			  PC;				
	
	O_n <= O 										WHEN mux_out = mux_out_out ELSE
			 alu 										WHEN mux_out = mux_out_alu ELSE
			 shift_out  							WHEN mux_out = mux_out_shift ELSE
			 data_in  								WHEN mux_out = mux_out_di ELSE
			 ( 0 => alu_c_i, OTHERS => '0' )	WHEN mux_out = mux_out_sltiu ELSE
			 O;			
			
	A_n <= A 		WHEN mux_a = mux_a_a ELSE
			 data_in WHEN mux_a = mux_a_di ELSE
			 A;			
	
	B_n <= B 		WHEN mux_b = mux_b_b ELSE
			 data_in WHEN mux_b = mux_b_di ELSE
			 B;
			
	left <= A 									 WHEN mux_alu_left = mux_alu_left_a ELSE
			  PC 									 WHEN mux_alu_left = mux_alu_left_pc ELSE
			  HI( 30 DOWNTO 0 ) & LO( 31 ) WHEN mux_alu_left = mux_alu_left_hilo ELSE
			  HI									 WHEN mux_alu_left =  mux_alu_left_hi ELSE
			  ( OTHERS => '0' );	
	
	right <= B 													  WHEN mux_alu_right = mux_alu_right_b ELSE
			   intr_right 										  WHEN mux_alu_right = mux_alu_right_intr ELSE
			   std_logic_vector( to_unsigned( 4, 32 ) ) WHEN mux_alu_right = mux_alu_right_four ELSE
			   ( OTHERS => '0' );		

  	
  	data_out <= O	WHEN mux_data_out = mux_data_out_out ELSE
  					A	WHEN mux_data_out = mux_data_out_a ELSE
  					HI	WHEN mux_data_out = mux_data_out_hi ELSE
  					LO	WHEN mux_data_out = mux_data_out_lo ELSE
  					PC	WHEN mux_data_out = mux_data_out_pc ELSE
  					( OTHERS => '0' );
       
       
	address <= PC			  WHEN mux_address = mux_address_pc ELSE
				  O			  WHEN mux_address = mux_address_out ELSE
				  address_reg WHEN mux_address = mux_address_reg ELSE
				  ( OTHERS => '0' );
     
     
	alu_n <=	alu WHEN alu_c_i = '0' ELSE
				HI( 30 downto 0 ) & LO( 31 );

	
	alu_c <= alu_c_i;
	
	a_msb   <= A( 31 );
	alu_msb <= alu( 31 );
                
   PROCESS( left, right, alu_add_sub )
		VARIABLE carry  : std_logic;
     	VARIABLE result : std_logic_vector( 31 DOWNTO 0 ); 
   BEGIN
     	add_sub( left, right, alu_add_sub, result, carry );
     	alu <= result;
     	alu_c_i <= carry;
	END PROCESS;                     

   PROCESS( A )
     	VARIABLE zero : std_logic;
   BEGIN
     	is_zero( A, zero );
     	a_zero <= zero;
   END PROCESS;
     
   PROCESS( O )
     	VARIABLE result : std_logic_vector( 31 DOWNTO 0 ); 
   BEGIN
     	rshift( O, '0', result );
     	shift_out <= result;
   END PROCESS;
     
   PROCESS( alu_c_i, LO )
     	VARIABLE result : std_logic_vector( 31 DOWNTO 0 ); 
   BEGIN
     	lshift( LO, not alu_c_i , result );
		shift_lo <= result;
	END PROCESS;
END;