CONFIGURATION conf_testbench_single OF testbench IS
	FOR single
		FOR pr: processor USE ENTITY work.processor(behavior);
		END FOR;
		FOR mem : memory USE ENTITY work.memory(behavior)
			GENERIC MAP( WORD_SIZE 	  => 32,
						 CODE_FILE 	  => "../Mars/test_all_text_0x0000.bin",
						 CODE_ADDRESS => 0,
						 CODE_SIZE    => 1024,
						 DATA_FILE    => "../Mars/test_all_data_0x2000.bin",
						 DATA_RESULT  => "../Mars/test_all_data_0x2000_result.bin",
						 DATA_ADDRESS => 8192, -- 0x2000
						 DATA_SIZE    => 4096, -- 4kB
						 REG_ADDRESS  => 12288, -- 0x3000
						 REG_SIZE     => 128 -- 32 registers
						);
		END FOR;
	END FOR;
END;