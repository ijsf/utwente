LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;


LIBRARY work;
USE work.instructions_pack.ALL;
USE work.control_pack.ALL;

ENTITY controller IS 
	PORT( clk      : IN  std_logic;
		  reset    : IN  std_logic;
		  data_in  : IN  std_logic_vector( 31 DOWNTO 0 ); 
		 
		  	  
		  mux_hi	: OUT mux_hi_t;
		  mux_lo	: OUT mux_lo_t;
		  mux_pc	: OUT mux_pc_t;
		  
		  mux_out	: OUT mux_out_t;
		  mux_a		: OUT mux_a_t;
		  mux_b		: OUT mux_b_t;
		  
		  mux_data_out	: OUT mux_data_out_t;
		  mux_address	: OUT mux_address_t;
		  
		  mux_alu_left	: OUT mux_alu_left_t;
		  mux_alu_right	: OUT mux_alu_right_t;
		  
		  alu_add_sub 	: OUT std_logic;
		  
		  address_reg  	: OUT std_logic_vector( 31 DOWNTO 0 ); 
		  intr_right 	: OUT std_logic_vector( 31 DOWNTO 0 ); 
		  
		  intr_pc		: OUT std_logic_vector( 25 DOWNTO 0 ); 	  
		  	
		  a_zero	: IN std_logic;
		  a_msb		: IN std_logic;
		  
		  alu_c		: IN std_logic;
		  alu_msb	: IN std_logic;
		  
		  -- Memory access
		  rd_wr    : OUT std_logic; --( 0 = read, 1 = write )
		  enable   : OUT std_logic;
		  
		  syscall  : OUT std_logic

		); 
END;
 
 LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use STD.textio.all;


ARCHITECTURE behavhior OF controller IS 
BEGIN
	PROCESS 
		CONSTANT reg_offset : std_logic_vector( 31 DOWNTO 0 ) := x"00003000";
		CONSTANT thirty_one : std_logic_vector( 4 DOWNTO 0 )  := "11111";  
		

		PROCEDURE controller_reg_read(reg : IN std_logic_vector) IS
		BEGIN
			mux_address <= mux_address_reg;
			address_reg <= reg_offset( 31 DOWNTO 7 ) & reg & "00";

			rd_wr    <= '0';
			enable   <= '1';
			WAIT UNTIL rising_edge( clk );
			enable   <= '0';
			WAIT UNTIL rising_edge( clk );

			--WAIT UNTIL rising_edge( clk );

		END;


		PROCEDURE controller_reg_write(reg : IN std_logic_vector) IS
		BEGIN
			mux_address <= mux_address_reg;
			address_reg <= reg_offset( 31 DOWNTO 7 ) & reg & "00";
			rd_wr    <= '1';
			enable   <= '1';
			WAIT UNTIL rising_edge( clk );
			enable   <= '0';
		END;

		PROCEDURE datapath_reset IS
		BEGIN
			mux_hi	<= mux_hi_hi;
			mux_lo	<= mux_lo_lo;
			mux_pc	<= mux_pc_pc;

			mux_out	<= mux_out_out;
			mux_a	<= mux_a_a;
			mux_b	<= mux_b_b;

			mux_data_out	<= mux_data_out_out;
			mux_address     <= mux_address_reg;

			mux_alu_left	<= mux_alu_left_a;
			mux_alu_right	<= mux_alu_right_b;

			alu_add_sub 	<= '0';
			address_reg  	<= ( OTHERS => '0' );
		END PROCEDURE;

		PROCEDURE wait_c ( cycles: IN integer) IS
		BEGIN
			for i in 1 to cycles loop
				WAIT UNTIL rising_edge( clk );
			end loop ; -- identifier
			
		END PROCEDURE;
  
		VARIABLE instruction    : std_logic_vector( 31 DOWNTO 0 );
		VARIABLE op_code        : std_logic_vector( 5 DOWNTO 0 );
		VARIABLE second_op_code : std_logic_vector( 10 DOWNTO 0 );
		
		VARIABLE i_26 : std_logic_vector( 25 DOWNTO 0 );
		VARIABLE i_16 : std_logic_vector( 15 DOWNTO 0 );
		VARIABLE s    : std_logic_vector( 31 DOWNTO 0 );
		VARIABLE t    : std_logic_vector( 31 DOWNTO 0 );
		VARIABLE d    : std_logic_vector( 31 DOWNTO 0 );
		
		
		VARIABLE offset : signed( 31 DOWNTO 0 );  
		VARIABLE var_shifts : std_logic_vector(4 DOWNTO 0);
		
		---- used for fuctional function calls		
		VARIABLE carry_out : std_logic;  
		
		VARIABLE zero_bit : std_logic;  
		
		VARIABLE dummy_bit : std_logic;  
		VARIABLE dummy_32  : std_logic_vector( 31 DOWNTO 0 );
		CONSTANT FOUR 	   : std_logic_vector( 31 DOWNTO 0 ) := std_logic_vector( to_unsigned( 4, 32 ) );
		
		-- Division variables
		CONSTANT SUBTRACT : std_logic := '1';
  		CONSTANT ADD : std_logic := '0';
		CONSTANT ONE 	   : std_logic_vector( 31 DOWNTO 0 ) := 	std_logic_vector( to_unsigned(1, 32));
		CONSTANT ZERO      : std_logic_vector( 31 downto 0 ) := (OTHERS =>'0');
		variable sub_add : std_logic;
	    --variable cntr : std_logic_vector(31 downto 0);
	    --variable cntr_gt_z : std_logic;
	    --variable result_gt_zero : std_logic;
	    variable msb_divider : std_logic;
	    variable div_by_zero : std_logic;
	    -- end division variables


		VARIABLE temp_reg1 : std_logic_vector( 31 DOWNTO 0 );		
	BEGIN
		wait_c(1); 
		
		loop_r: WHILE true LOOP
			IF reset = '1' THEN
				rd_wr    <= '0';
				enable   <= '0';
				syscall  <= '0';
				wait_c(1); 
			ELSE

				-- Reset Datapath
				datapath_reset;
				mux_address <= mux_address_pc;
				rd_wr    <= '0';
				enable   <= '1';
				wait_c(1); 
				enable   <= '0';
				wait_c(1); 

				instruction := data_in;
				op_code := instruction( 31 DOWNTO 26 );
				datapath_reset;
				
				-- Increase program counter
				mux_alu_left <= mux_alu_left_pc;
				mux_alu_right <= mux_alu_right_four;
				alu_add_sub <= ADD;
				mux_pc <= mux_pc_alu;
				wait_c(1);
				datapath_reset;

				CASE op_code IS
					WHEN instr6_generic => -- generic
					second_op_code := instruction( 10 DOWNTO 0 );
					
					CASE second_op_code IS
						WHEN instr11_ADDU => --ADD
							-- Read reg(s) into A
							datapath_reset;
							mux_a <= mux_a_di;
							controller_reg_read(instruction(25 DOWNTO 21)); --s
							
							-- Read reg(t) into B
							datapath_reset;
							mux_b <= mux_b_di;
							controller_reg_read(instruction(20 DOWNTO 16)); --t
							

							-- Perform addition
							datapath_reset;
							mux_alu_left <= mux_alu_left_a;
							mux_alu_right <= mux_alu_right_b;
							alu_add_sub <= ADD;
							mux_out <= mux_out_alu;

							wait_c(1);

							-- Save to register
							datapath_reset;
							mux_data_out <= mux_data_out_out;
							controller_reg_write(instruction( 15 DOWNTO 11 ));
						
						WHEN instr11_SRLV => --SRLV
						  	
							--read reg(s) to var_shifts
							datapath_reset;
						    controller_reg_read(instruction( 25 DOWNTO 21 ));
						    var_shifts := data_in(4 DOWNTO 0);

							--reg_read( , s );
							
							-- Read reg(t) into OUT
							datapath_reset;
							mux_out <= mux_out_di;
							controller_reg_read(instruction(20 DOWNTO 16));
							
							datapath_reset;
							mux_out <= mux_out_shift;
							--reg_read( instruction( 20 DOWNTO 16 ), t );
							
							--s(31 DOWNTO 5) := (OTHERS => '0'); -- only use 5 LSB, clear OTHERS
							
							--wait for shift to be done
							wait_c(to_integer(unsigned(var_shifts)));
							--rshift(t,'0',t);
							
							
							-- Save to register
							datapath_reset;
							mux_data_out <= mux_data_out_out;
							controller_reg_write(instruction( 15 DOWNTO 11 ));
							--d := t;  
							
							--add_sub(s, one,'1',s,dummy_bit);
							--WHILE dummy_bit = '0' loop
								--rshift(t,'0',t);
								--add_sub(s, one,'1',s,dummy_bit);
							--END loop; 
							--d := t;         	                    

							--reg_write( instruction( 15 DOWNTO 11 ), d );
						
						WHEN instr11_JR => --JR
						  
						  -- Read reg(s) into PC
							datapath_reset;
							mux_pc <= mux_pc_di;
							controller_reg_read(instruction(25 DOWNTO 21)); --s
							
							--reg_read( instruction( 25 DOWNTO 21 ), s );
							--nPC := s;
						
						WHEN instr11_DIVU => --DIVU

							
							-- Set HI zero
							mux_hi <= mux_hi_zero;

							-- Read reg(s) into LO
							mux_lo <= mux_lo_di;
							controller_reg_read(instruction(25 DOWNTO 21)); --s
							s := data_in;
							-- Read reg(t) into B
							datapath_reset;
							mux_b <= mux_b_di;
							--mux_a <= mux_a_di;
							controller_reg_read(instruction(20 DOWNTO 16)); --t (divider)
							--msb_divider := data_in(31);
							t:= data_in;

							datapath_reset;

							-- Check div by zero:
							if (t = ZERO) THEN
								
							else
								-- Set up adder
								mux_alu_left <= mux_alu_left_hilo;
								mux_alu_right <= mux_alu_right_b;
								alu_add_sub <= '1';
								mux_hi <= mux_hi_alu;
								mux_lo <= mux_lo_shift;

								wait_c(32);
								datapath_reset;							    
							      
					          END IF;
					          REPORT "END Division " & integer'image( to_integer( signed( s ) ) ) &" / " & integer'image( to_integer( signed( t ) ) ) SEVERITY note; 


						WHEN instr11_MFHI => --MFHI
							datapath_reset;
							mux_data_out <= mux_data_out_hi;
							controller_reg_write(instruction( 15 DOWNTO 11 ));						
						WHEN instr11_MFLO => --MFLO
							datapath_reset;
							mux_data_out <= mux_data_out_lo;
							controller_reg_write(instruction( 15 DOWNTO 11 ));	
						WHEN instr11_SYSCALL => --SYSCALL kill simulation
						syscall <= '1';
						--REPORT "SYSCALL kill simulation" SEVERITY FAILURE;

						WHEN OTHERS => --error 
							REPORT "Unsupported Instruction" SEVERITY warning;
					END CASE;
						
					WHEN instr6_JAL => --JAL
						--i_26 := instruction( 25 DOWNTO 0 );
						
						-- Save pc to register
						datapath_reset;
						mux_data_out <= mux_data_out_pc;
						controller_reg_write(thirty_one);
						--reg_write( thirty_one, std_logic_vector( nPC ) ); -- no delay slot, so +4 instead of +8
						
						--update PC
						datapath_reset;
						intr_pc <= instruction( 25 DOWNTO 0 );
						mux_pc <= mux_pc_intr;
						wait_c(1);
						--nPC := PC( 31 DOWNTO 28 ) & i_26 & "00";
					
					WHEN instr6_SLTIU => --SLTIU
					  
					  -- Read reg(s) into A
						datapath_reset;
						mux_a <= mux_a_di;
						controller_reg_read(instruction(25 DOWNTO 21));
						
						-- Perform subtraction
						datapath_reset;
						mux_alu_left <= mux_alu_left_a;

						intr_right(31 DOWNTO 16) <= (OTHERS => '0');
						intr_right(15 DOWNTO 0) <=  instruction( 15 DOWNTO 0 );		

						mux_alu_right <= mux_alu_right_intr;
						alu_add_sub <= SUBTRACT;
						mux_out <= mux_out_sltiu;
						wait_c(1);
						--add_sub(s, temp_reg1, '1', dummy_32, carry_out);
						
						--t:=( 0 => carry_out, OTHERS => '0' ); -- output 1 or 0
						
						-- Save to register
						datapath_reset;
						mux_data_out <= mux_data_out_out;
						controller_reg_write(instruction( 20 DOWNTO 16 ));
						--reg_write( instruction( 20 DOWNTO 16 ), t );
					
					WHEN instr6_BGTZ => --BGTZ
						
						-- Read reg(s) into A
						datapath_reset;
						mux_a <= mux_a_di;
						controller_reg_read(instruction(25 DOWNTO 21));
						--reg_read( instruction( 25 DOWNTO 21 ), s );
						wait_c(1);
						intr_right <= std_logic_vector(resize( signed( instruction( 15 DOWNTO 0 ) & "00" ), 32 ));
						
						IF not(a_zero	or a_msb) = '1' THEN -- s > 0 <=> !(s==0 or s<0)
						  --may be wrong, the nPC is added twice, first with 4 and next with offset
						  -- Perform addition

						  datapath_reset;
						  mux_alu_left <= mux_alu_left_pc;
					      mux_alu_right <= mux_alu_right_intr;
					      alu_add_sub <= ADD;
					      mux_pc <= mux_pc_alu;
					      wait_c(1);
						END IF;
						
						--is_zero( s , zero_bit);
						--IF not(zero_bit or s(31)) = '1' THEN -- s > 0 <=> !(s==0 or s<0)
							--offset := resize( signed( i_16 & "00" ), 32 );
							--add_sub(nPC, std_logic_vector(offset), '0', nPC, dummy_bit); --add offset to nPC
						--END IF;
					
					when instr6_LW => --LW
					  
					  --read from reg(s) to A
					  datapath_reset;
						mux_a <= mux_a_di;
						controller_reg_read(instruction(25 DOWNTO 21));

						--reg_read(instruction(25 DOWNTO 21),s);
						
						--i_16 := instruction(15 DOWNTO 0);
						--offset := resize(signed(i_16), 32);
						
						intr_right <= std_logic_vector( resize( signed( instruction( 15 DOWNTO 0 )), 32 ));
						
						-- Perform addition
						datapath_reset;
						mux_alu_left <= mux_alu_left_a;
						mux_alu_right <= mux_alu_right_intr;
						alu_add_sub <= ADD;
						mux_out <= mux_out_alu;

						wait_c(1);
						--add_sub(s, std_logic_vector(offset), '0', dummy_32, dummy_bit); --add offset to register
						
						--read from memory, save to OUT
						datapath_reset;
						--mux_out <= mux_out_di;
						mux_address <= mux_address_out;
						rd_wr    <= '0';
					    enable   <= '1';
					    wait_c(1);
					    mux_out <= mux_out_di;
					    enable   <= '0';
					    wait_c(1);
						--memory_read(dummy_32,t); --offset is signed!
						
						--save out to data_out
						datapath_reset;
						mux_data_out <= mux_data_out_out;
						controller_reg_write(instruction( 20 DOWNTO 16 ));
						--reg_write(instruction(20 DOWNTO 16),t);
					
					WHEN instr6_SW => --SW
					  
					  --read from reg(s) to A
					  datapath_reset;
						mux_a <= mux_a_di;
						controller_reg_read(instruction(25 DOWNTO 21));
						
						--reg_read( instruction( 25 DOWNTO 21 ), s );
						
						--i_16 := instruction( 15 DOWNTO 0 );
						--offset := resize( signed( i_16 ), 32 );
						
						intr_right <= std_logic_vector(resize( signed( instruction( 15 DOWNTO 0 )), 32 ));
						
						-- Perform addition
						datapath_reset;
						mux_alu_left <= mux_alu_left_a;
						mux_alu_right <= mux_alu_right_intr;
						alu_add_sub <= ADD;
						mux_out <= mux_out_alu;
						wait_c(1);
						--add_sub(s, std_logic_vector(offset), '0', dummy_32, dummy_bit); --add offset to register
						
						--read from reg(t) to A
					  datapath_reset;
						mux_a <= mux_a_di;
						controller_reg_read(instruction(20 DOWNTO 16));
						--reg_read( instruction( 20 DOWNTO 16 ), t );
						
						--write A to memory
						datapath_reset;
						mux_address <= mux_address_out;
						mux_data_out <= mux_data_out_a;
			      rd_wr    <= '1';
			      enable   <= '1';
			      wait_c(1);
			      enable   <= '0';
						--memory_write( dummy_32, t ); --offset is signed!
					
					WHEN OTHERS => --error
						REPORT "Unsupported Instruction" SEVERITY warning;
				END CASE; 
				
			END IF;
		END LOOP; 
	END PROCESS;
END ARCHITECTURE;