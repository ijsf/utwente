# funny trick to prevent an error
# create WORK (if it exists a warning is raised; delete WORK and create an empty WORK
vlib work
vdel -all
vlib work

vcom instructions_pack.vhd
vcom functional_units_pack.vhd 
vcom control_pack.vhd
vcom memory.vhd
vcom processor_ent.vhd
vcom testbench_ent.vhd

vcom processor_behav_arch.vhd
vcom compare.vhd
vcom testbench_dual_arch.vhd
vcom controller.vhd
vcom datapath.vhd
vcom processor_controller_datapath_arch.vhd

vcom conf_tb_all_dual_datapath.vhd

vsim -msgmode both -displaymsgmode both work.conf_testbench_dual_datapath
# Compare unit
add wave -label clk sim:/testbench/clk_i
add wave -label reset sim:/testbench/reset_i
add wave -label C_data_CIMO sim:/testbench/data_ci_mo
add wave -label C_data_COMI sim:/testbench/data_co_mi

# Golden unit
add wave -label G_clk sim:/testbench/clk_gold
add wave -label G_nRD_WR sim:/testbench/nRD_WR_gold
add wave -label G_enable sim:/testbench/enable_gold
add wave -label G_instruction sim:/testbench/proc_gold/proc_proc/instruction
add wave -label G_PC sim:/testbench/proc_gold/proc_proc/PC
add wave -label G_nPC sim:/testbench/proc_gold/proc_proc/nPC
add wave -label G_HI sim:/testbench/proc_gold/proc_proc/HI
add wave -label G_LO sim:/testbench/proc_gold/proc_proc/LO
add wave -label G_offset sim:/testbench/proc_gold/proc_proc/offset
add wave -label G_d_address sim:/testbench/proc_gold/address

# Datapath/Controller
add wave -label DC_contr_reset sim:/testbench/proc_silver/controller_comp/reset
add wave -label DC_d_address sim:/testbench/proc_silver/address
add wave -label DC_contr_clk sim:/testbench/proc_silver/controller_comp/clk
#add wave -label DC_nRD_WR sim:/testbench/nRD_WR_silver
#add wave -label DC_enable sim:/testbench/enable_silver
add wave -label DC_enable sim:/testbench/proc_silver/controller_comp/enable
add wave -label DC_rdwr sim:/testbench/proc_silver/controller_comp/rd_wr
#add wave -label DC_rdwr sim:/testbench/proc_silver/controller_comp/rd_wr
add wave -label DC_mux_alu_left sim:/testbench/proc_silver/controller_comp/mux_alu_left
add wave -label DC_mux_alu_right sim:/testbench/proc_silver/controller_comp/mux_alu_right
add wave -label DC_intr_right sim:/testbench/proc_silver/controller_comp/intr_right
add wave -label DC_syscall sim:/testbench/syscall_silver
add wave -label DC_instruction sim:/testbench/proc_silver/controller_comp/line__61/instruction

add wave -label D_reg_a sim:/testbench/proc_silver/datapath_comp/a
add wave -label D_reg_b sim:/testbench/proc_silver/datapath_comp/b
add wave -label D_reg_out sim:/testbench/proc_silver/datapath_comp/o
add wave -label D_reg_pc sim:/testbench/proc_silver/datapath_comp/pc
add wave -label D_alu sim:/testbench/proc_silver/datapath_comp/alu

add wave -label DC_PC sim:/testbench/proc_silver/datapath_comp/pc
add wave -label DC_HI sim:/testbench/proc_silver/datapath_comp/hi
add wave -label DC_LO sim:/testbench/proc_silver/datapath_comp/lo
add wave -label DC_offset sim:/testbench/proc_silver/controller_comp/line__61/offset

# Memory unit
add wave -label M_nRD_WR sim:/testbench/nRD_WR_mem
add wave -label M_enable sim:/testbench/enable_mem
add wave -label M_address sim:/testbench/address_mem
add wave -label M_d_mem_2800 sim:/testbench/mem/mem_proc/data_mem(512)
add wave -label M_d_mem_2804 sim:/testbench/mem/mem_proc/data_mem(513)

radix -hexadecimal

run -all

wave zoom full