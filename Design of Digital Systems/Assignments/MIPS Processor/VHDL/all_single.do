# funny trick to prevent an error
# create WORK (if it exists a warning is raised; delete WORK and create an empty WORK
vlib work
vdel -all
vlib work

vcom memory.vhd
vcom processor_ent.vhd
vcom testbench_ent.vhd
vcom instructions_pack.vhd
vcom processor_behav_arch.vhd
vcom testbench_single_arch.vhd
vcom conf_tb_all_single.vhd

vsim -msgmode both -displaymsgmode both work.conf_testbench_single

add wave -label clk sim:/testbench/clk_i
add wave -label reset sim:/testbench/reset_i
add wave -label nRD_WR sim:/testbench/nRD_WR_i
add wave -label enable sim:/testbench/enable_i
add wave -label syscall sim:/testbench/syscall_i
add wave -label data_MIPO sim:/testbench/data_mi_po
add wave -label data_MOPI sim:/testbench/data_mo_pi
add wave -label address sim:/testbench/address_i
add wave -label instruction sim:/testbench/pr/proc_proc/instruction
add wave -label PC sim:/testbench/pr/proc_proc/PC
add wave -label nPC sim:/testbench/pr/proc_proc/nPC
add wave -label HI sim:/testbench/pr/proc_proc/HI
add wave -label LO sim:/testbench/pr/proc_proc/LO
add wave -label offset sim:/testbench/pr/proc_proc/offset
add wave -label d_mem_2800 sim:/testbench/mem/mem_proc/data_mem(512)
add wave -label d_mem_2804 sim:/testbench/mem/mem_proc/data_mem(513)

radix -hexadecimal

run -all

wave zoom full