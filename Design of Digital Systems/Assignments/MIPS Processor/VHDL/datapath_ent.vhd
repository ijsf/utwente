LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY WORK;
USE WORK.control_pack.ALL;
USE WORK.functional_units_pack.ALL;

ENTITY datapath IS 
	PORT( clk           : IN  std_logic;
		   reset         : IN  std_logic;
		   data_in       : IN  std_logic_vector( 31 DOWNTO 0 ); 
		   data_out      : OUT std_logic_vector( 31 DOWNTO 0 ); 
		   address       : OUT std_logic_vector( 31 DOWNTO 0 ); 
		  	       
		   mux_hi	     : IN mux_hi_t;
		   mux_lo	     : IN mux_lo_t;
		   mux_pc	     : IN mux_pc_t;
		        
		   mux_out	     : IN mux_out_t;
		   mux_a		     : IN mux_a_t;
		   mux_b		     : IN mux_b_t;
		   
		   mux_data_out  : IN mux_data_out_t;
		   mux_address	  : IN mux_address_t;
		   
		   mux_alu_left  : IN mux_alu_left_t;
		   mux_alu_right : IN mux_alu_right_t;
		   
		   alu_add_sub   : IN std_logic;
		   
		   address_reg   : IN std_logic_vector( 31 DOWNTO 0 ); 	  
		   intr_right 	  : IN std_logic_vector( 31 DOWNTO 0 ); 
		   
		   intr_pc		  : IN std_logic_vector( 25 DOWNTO 0 ); 
		   
		   a_zero	     : OUT std_logic;
		   a_msb		     : OUT std_logic;
		        
		   alu_c		     : OUT std_logic;
		   alu_msb	     : OUT std_logic
		); 
END;