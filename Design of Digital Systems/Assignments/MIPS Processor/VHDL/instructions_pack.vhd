LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

PACKAGE instructions_pack IS
	CONSTANT instr6_generic	    : std_logic_vector( 5 DOWNTO 0 ) := "000000"; --generic 
	CONSTANT instr6_JAL 	    : std_logic_vector( 5 DOWNTO 0 ) := "000011"; --JAL
	CONSTANT instr6_SLTIU 	    : std_logic_vector( 5 DOWNTO 0 ) := "001011"; --SLTIU
	CONSTANT instr6_BGTZ 	    : std_logic_vector( 5 DOWNTO 0 ) := "000111"; --BGTZ
	CONSTANT instr6_LW 		    : std_logic_vector( 5 DOWNTO 0 ) := "100011"; --LW
	CONSTANT instr6_SW 		    : std_logic_vector( 5 DOWNTO 0 ) := "101011"; --SW
        
	CONSTANT instr11_ADDU	    : std_logic_vector( 10 DOWNTO 0 ) := "00000100001"; --ADDU
	CONSTANT instr11_SRLV 	    : std_logic_vector( 10 DOWNTO 0 ) := "00000000110"; --SRLV
	CONSTANT instr11_JR 	    : std_logic_vector( 10 DOWNTO 0 ) := "00000001000"; --JR
	CONSTANT instr11_DIVU 	    : std_logic_vector( 10 DOWNTO 0 ) := "00000011011"; --DIVU
	CONSTANT instr11_MFHI 	    : std_logic_vector( 10 DOWNTO 0 ) := "00000010000"; --MFHI
	CONSTANT instr11_MFLO 	    : std_logic_vector( 10 DOWNTO 0 ) := "00000010010"; --MFLO
	
	CONSTANT instr11_SYSCALL    : std_logic_vector( 10 DOWNTO 0 ) := "00000001100"; --SYSCALL
END PACKAGE;