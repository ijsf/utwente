LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

PACKAGE functional_units_pack IS
    CONSTANT zero : std_logic_vector( 31 DOWNTO 0 ) := ( OTHERS => '0' );
    CONSTANT one  : std_logic_vector( 32 DOWNTO 0 ) := ( 0 => '1', OTHERS => '0' );
    
    PROCEDURE add_sub( op1        : IN  std_logic_vector( 31 DOWNTO 0 ); -- op1 must be a signed extended unsigned
                       op2        : IN  std_logic_vector( 31 DOWNTO 0 );
                       add_or_sub : IN  std_logic; -- 1 is subtract and 0 is add
                       result     : OUT std_logic_vector( 31 downto 0 );
                       carry      : OUT std_logic -- 1 is negative, when subtracting
                     );

    PROCEDURE lshift( input  : IN  std_logic_vector( 31 DOWNTO 0 );
                      lsb    : IN  std_logic;
                      result : OUT std_logic_vector( 31 DOWNTO 0 )
                    );
    
    PROCEDURE rshift( input  : IN  std_logic_vector( 31 DOWNTO 0 );                    
                      msb    : IN  std_logic;
                      result : OUT std_logic_vector( 31 DOWNTO 0 )
                    );
                        
    PROCEDURE is_zero( input :  IN  std_logic_vector( 31 DOWNTO 0 );                    
                       result : OUT std_logic
                     );                      
END functional_units_pack;
    
PACKAGE BODY functional_units_pack IS
    PROCEDURE add_sub( op1        : IN  std_logic_vector( 31 DOWNTO 0 ); -- op1 must be a signed extended unsigned
                       op2        : IN  std_logic_vector( 31 DOWNTO 0 );
                       add_or_sub : IN  std_logic;                       --1 is subtract and 0 is add.
                       result     : OUT std_logic_vector( 31 DOWNTO 0 );
                       carry      : OUT std_logic                        --1 is negative,when subtraction
                     ) IS 
        VARIABLE result_inter, op2_real : signed( 32 DOWNTO 0 );    
    BEGIN
        IF add_or_sub = '0' THEN
            op2_real := signed( '0' & op2 );
        ELSE
            op2_real := signed( NOT( '0' & op2 ) );
        END IF;
        
        result_inter := signed( '0' & op1 ) + op2_real + signed( zero & add_or_sub );
        carry := result_inter( 32 );
        result := std_logic_vector( result_inter( 31 DOWNTO 0 ) );        
    END PROCEDURE;
    
    PROCEDURE lshift( input  : IN  std_logic_vector( 31 DOWNTO 0 );
                    lsb    : IN  std_logic;
                    result : OUT std_logic_vector( 31 DOWNTO 0 )
                    ) IS 
    BEGIN
        result := input( 30 DOWNTO 0 ) & lsb;
    END PROCEDURE;
    
    PROCEDURE rshift( input  : IN  std_logic_vector( 31 DOWNTO 0 );
                    msb    : IN  std_logic;
                    result : OUT std_logic_vector( 31 DOWNTO 0 )
                    ) IS
    BEGIN
        result := msb & input( 31 DOWNTO 1 );
    END PROCEDURE;
    
    PROCEDURE is_zero( input  : IN  std_logic_vector( 31 DOWNTO 0 );                    
                    result : OUT std_logic
                    ) IS
    BEGIN
        IF( input = zero ) THEN
            result := '1';
        ELSE 
            result := '0';
        END IF;
    END PROCEDURE;                   
END functional_units_pack;