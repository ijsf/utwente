LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;

ENTITY memory IS
	GENERIC( WORD_SIZE    : natural := 32;
			 CODE_FILE    : string  := "";
			 CODE_ADDRESS : natural := 0;
			 CODE_SIZE    : natural := 0;
			 DATA_FILE    : string  := "";
			 DATA_RESULT  : string  := "";
			 DATA_ADDRESS : natural := 0;
			 DATA_SIZE    : natural := 0;
			 REG_ADDRESS  : natural := 0;
			 REG_SIZE     : natural := 128
			);
	PORT(	 data_in	: IN std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
			 data_out	: OUT std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
			 address	: IN std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
			 nRD_WR		: IN std_logic;
			 clk		: IN std_logic;
			 enable		: IN std_logic;
			 comp_mem	: IN std_logic
		);
	
	-- Custom types
	TYPE t_int_file IS FILE OF integer;
	TYPE memory_array IS
			ARRAY( natural RANGE <> ) OF std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
	
	-- Helper functions
	PROCEDURE int2bitv( int : IN integer; bitv : OUT std_logic_vector ) IS
	BEGIN
		bitv := std_logic_vector( to_signed( int, bitv'LENGTH ) );
	END int2bitv;
	
	FUNCTION bitv2int( bitv : IN std_logic_vector ) RETURN integer IS
	BEGIN
		RETURN to_integer( signed( bitv ) );
	END bitv2int;
	
	FUNCTION bitv2nat( bitv : IN std_logic_vector ) RETURN natural IS
	BEGIN
		RETURN to_integer( unsigned( bitv ) );
	END bitv2nat;
	
	PROCEDURE compareData( data_mem : IN memory_array; comp_file_name : IN string ) IS
		FILE comp_file : t_int_file OPEN read_mode IS comp_file_name;
		VARIABLE read_buffer : integer;
        VARIABLE data_buffer : integer;
		VARIABLE mem_address : integer;
	BEGIN		
		FOR i IN 0 TO ( DATA_SIZE / 4 ) - 1 LOOP
			read( comp_file, read_buffer );
			
            data_buffer := bitv2int( data_mem( i ) );
            
			mem_address := ( i * 4 ) + DATA_ADDRESS;
			
			ASSERT read_buffer = bitv2int( data_mem( i ) ) 
				REPORT "Data comparison failed at address: " & integer'image( mem_address ) &
                       " data: " & integer'image( data_buffer ) & " should be " & 
                       integer'image( read_buffer )
				SEVERITY warning;
		END LOOP;
	END compareData;
END memory;

ARCHITECTURE behavior OF memory IS
	FILE in_code_file : t_int_file OPEN read_mode IS CODE_FILE;
	FILE in_data_file : t_int_file OPEN read_mode IS DATA_FILE;
BEGIN
	mem_proc: PROCESS
		VARIABLE code_mem : memory_array( 0 TO ( CODE_SIZE / 4 ) - 1 ) := ( OTHERS => ( OTHERS => '0' ) );
		VARIABLE data_mem : memory_array( 0 TO ( DATA_SIZE / 4 ) - 1 ) := ( OTHERS => ( OTHERS => '0' ) );
		VARIABLE reg_mem  : memory_array( 0 TO ( REG_SIZE / 4 ) - 1 )  := ( OTHERS => ( OTHERS => '0' ) );
		VARIABLE addr 	  : natural;
		VARIABLE d_out 	  : std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
		VARIABLE unknown  : std_logic_vector( WORD_SIZE - 1 DOWNTO 0 ) := ( OTHERS => '-' );
		
		VARIABLE counter  	 : natural := 0;
		VARIABLE read_buffer : integer;
	BEGIN
		-- INIT
	
		-- Fill code memory
		FOR i IN 0 TO ( CODE_SIZE / 4 ) - 1 LOOP
			code_mem( i ) := ( OTHERS => '0' );
		END LOOP;
		
		WHILE not ( endfile( in_code_file ) ) LOOP
			read( in_code_file, read_buffer );
			int2bitv( read_buffer, code_mem( counter ) );
			counter := counter + 1;
		END LOOP;
		
		-- Fill data memory
		FOR i IN 0 TO ( DATA_SIZE / 4 ) - 1 LOOP
			data_mem( i ) := ( OTHERS => '0' );
		END LOOP;
		
		counter := 0;
		
		WHILE not( endfile( in_data_file ) ) LOOP
			read( in_data_file, read_buffer );
			int2bitv( read_buffer, data_mem( counter ) );
			counter := counter + 1;
		END LOOP;
		
		-- Set registers to zero
		FOR i IN 0 TO ( REG_SIZE / 4 ) - 1 LOOP
			reg_mem( i ) := ( OTHERS => '0' );
		END LOOP;
	
		LOOP
			WAIT UNTIL rising_edge( clk );
			IF enable = '1' THEN
				addr := bitv2nat( address );
		
				ASSERT( ( addr >= CODE_ADDRESS ) and ( addr < ( CODE_ADDRESS + CODE_SIZE ) ) ) or
					  ( ( addr >= DATA_ADDRESS ) and ( addr < ( DATA_ADDRESS + DATA_SIZE ) ) ) or
					  ( ( addr >= REG_ADDRESS ) and ( addr < ( REG_ADDRESS + REG_SIZE ) ) )
					REPORT "Out of memory range" SEVERITY warning;
		
				IF( addr >= CODE_ADDRESS and addr < ( CODE_ADDRESS + CODE_SIZE ) ) THEN
					IF nRD_WR = '1' THEN -- Write to code memory
						code_mem( ( addr - CODE_ADDRESS ) / 4 ) := data_in;
					ELSE -- Read from code memory
						data_out <= code_mem( ( addr - CODE_ADDRESS ) / 4 );
					END IF;
				ELSIF( addr >= DATA_ADDRESS and addr < ( DATA_ADDRESS + DATA_SIZE ) ) THEN
					IF nRD_WR = '1' THEN -- Write to data memory
						data_mem( ( addr - DATA_ADDRESS ) / 4 ) := data_in;
					ELSE -- Read from data memory
						data_out <= data_mem( ( addr - DATA_ADDRESS ) / 4 );
					END IF;
				ELSIF( addr >= REG_ADDRESS and addr < ( REG_ADDRESS + REG_SIZE ) ) THEN
					IF nRD_WR = '1' THEN -- Write to register
						reg_mem( ( addr - REG_ADDRESS ) / 4 ) := data_in;
					ELSE -- Read from register
						data_out <= reg_mem( ( addr - REG_ADDRESS ) / 4 );
					END IF;
				END IF;
			ELSE -- enable == '0'
				data_out <= unknown;
			END IF;
			
			-- Compare the current memory contents with
			-- the contents of the given data result file.
			IF comp_mem = '1' THEN
				compareData( data_mem, DATA_RESULT );
			END IF;
		END LOOP;
	END PROCESS;
END ARCHITECTURE;