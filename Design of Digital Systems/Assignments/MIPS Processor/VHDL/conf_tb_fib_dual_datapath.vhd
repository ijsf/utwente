CONFIGURATION conf_testbench_dual_datapath OF testbench IS
    FOR dual
        FOR proc_gold: processor USE ENTITY work.processor(behavior);
        END FOR;
        FOR proc_silver: processor USE ENTITY work.processor(controller_datapath);
        END FOR;
        FOR mem : memory USE ENTITY work.memory(behavior)
            GENERIC MAP( WORD_SIZE 	  => 32,
                         CODE_FILE 	  => "../Mars/fib_text_0x0000.bin",
                         CODE_ADDRESS => 0,
                         CODE_SIZE	  => 1024,
                         DATA_FILE	  => "../Mars/fib_data_0x2000.bin",
                         DATA_RESULT  => "../Mars/fib_data_0x2000_result.bin",
                         DATA_ADDRESS => 8192, -- 0x2000
                         DATA_SIZE	  => 4096, -- 4kB
                         REG_ADDRESS  => 12288, -- 0x3000
                         REG_SIZE	  => 128 -- 32 registers
                        );
        END FOR;
    END FOR;
END;
