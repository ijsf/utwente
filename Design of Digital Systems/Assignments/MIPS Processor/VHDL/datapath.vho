-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/12/2014 16:53:07"

-- 
-- Device: Altera EP4CE30F23A7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
ARCHITECTURE datapath OF datapath IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_data_in : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_data_out : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_address : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_mux_hi : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_mux_lo : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_mux_pc : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_mux_out : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_mux_a : std_logic_vector(0 DOWNTO 0);
SIGNAL ww_mux_b : std_logic_vector(0 DOWNTO 0);
SIGNAL ww_mux_data_out : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_mux_address : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_mux_alu_left : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_mux_alu_right : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_alu_add_sub : std_logic;
SIGNAL ww_address_reg : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_intr_right : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_intr_pc : std_logic_vector(25 DOWNTO 0);
SIGNAL ww_a_zero : std_logic;
SIGNAL ww_a_msb : std_logic;
SIGNAL ww_alu_c : std_logic;
SIGNAL ww_alu_msb : std_logic;
SIGNAL clk_ainputclkctrl_INCLK_bus : std_logic_vector(3 DOWNTO 0);
SIGNAL data_out_a1_combout : std_logic;
SIGNAL data_out_a4_combout : std_logic;
SIGNAL data_out_a10_combout : std_logic;
SIGNAL data_out_a13_combout : std_logic;
SIGNAL data_out_a25_combout : std_logic;
SIGNAL data_out_a31_combout : std_logic;
SIGNAL data_out_a40_combout : std_logic;
SIGNAL data_out_a46_combout : std_logic;
SIGNAL data_out_a55_combout : std_logic;
SIGNAL data_out_a70_combout : std_logic;
SIGNAL data_out_a73_combout : std_logic;
SIGNAL data_out_a76_combout : std_logic;
SIGNAL data_out_a85_combout : std_logic;
SIGNAL Equal30_a8_combout : std_logic;
SIGNAL left_a30_a_a1_combout : std_logic;
SIGNAL left_a29_a_a2_combout : std_logic;
SIGNAL left_a28_a_a3_combout : std_logic;
SIGNAL left_a23_a_a8_combout : std_logic;
SIGNAL left_a22_a_a9_combout : std_logic;
SIGNAL left_a21_a_a10_combout : std_logic;
SIGNAL left_a20_a_a11_combout : std_logic;
SIGNAL left_a19_a_a12_combout : std_logic;
SIGNAL left_a18_a_a13_combout : std_logic;
SIGNAL left_a15_a_a16_combout : std_logic;
SIGNAL left_a14_a_a17_combout : std_logic;
SIGNAL left_a11_a_a20_combout : std_logic;
SIGNAL left_a8_a_a23_combout : std_logic;
SIGNAL left_a7_a_a24_combout : std_logic;
SIGNAL left_a6_a_a25_combout : std_logic;
SIGNAL left_a5_a_a26_combout : std_logic;
SIGNAL left_a4_a_a27_combout : std_logic;
SIGNAL left_a3_a_a28_combout : std_logic;
SIGNAL left_a2_a_a29_combout : std_logic;
SIGNAL HI_a5_combout : std_logic;
SIGNAL LO_a10_combout : std_logic;
SIGNAL LO_a20_combout : std_logic;
SIGNAL LO_a21_combout : std_logic;
SIGNAL A_a29_combout : std_logic;
SIGNAL O_n_a31_a_a5_combout : std_logic;
SIGNAL op2_real_a126_combout : std_logic;
SIGNAL op2_real_a127_combout : std_logic;
SIGNAL op2_real_a128_combout : std_logic;
SIGNAL op2_real_a129_combout : std_logic;
SIGNAL op2_real_a142_combout : std_logic;
SIGNAL op2_real_a143_combout : std_logic;
SIGNAL op2_real_a144_combout : std_logic;
SIGNAL op2_real_a145_combout : std_logic;
SIGNAL op2_real_a148_combout : std_logic;
SIGNAL op2_real_a149_combout : std_logic;
SIGNAL op2_real_a150_combout : std_logic;
SIGNAL op2_real_a151_combout : std_logic;
SIGNAL op2_real_a152_combout : std_logic;
SIGNAL op2_real_a153_combout : std_logic;
SIGNAL op2_real_a156_combout : std_logic;
SIGNAL op2_real_a157_combout : std_logic;
SIGNAL op2_real_a158_combout : std_logic;
SIGNAL op2_real_a159_combout : std_logic;
SIGNAL op2_real_a172_combout : std_logic;
SIGNAL op2_real_a173_combout : std_logic;
SIGNAL op2_real_a174_combout : std_logic;
SIGNAL op2_real_a175_combout : std_logic;
SIGNAL op2_real_a176_combout : std_logic;
SIGNAL op2_real_a177_combout : std_logic;
SIGNAL op2_real_a178_combout : std_logic;
SIGNAL op2_real_a179_combout : std_logic;
SIGNAL op2_real_a180_combout : std_logic;
SIGNAL op2_real_a181_combout : std_logic;
SIGNAL op2_real_a184_combout : std_logic;
SIGNAL op2_real_a185_combout : std_logic;
SIGNAL op2_real_a186_combout : std_logic;
SIGNAL op2_real_a187_combout : std_logic;
SIGNAL address_reg_a1_a_ainput_o : std_logic;
SIGNAL address_reg_a4_a_ainput_o : std_logic;
SIGNAL address_reg_a5_a_ainput_o : std_logic;
SIGNAL address_reg_a9_a_ainput_o : std_logic;
SIGNAL address_reg_a15_a_ainput_o : std_logic;
SIGNAL address_reg_a21_a_ainput_o : std_logic;
SIGNAL address_reg_a22_a_ainput_o : std_logic;
SIGNAL address_reg_a23_a_ainput_o : std_logic;
SIGNAL intr_right_a31_a_ainput_o : std_logic;
SIGNAL intr_right_a30_a_ainput_o : std_logic;
SIGNAL intr_right_a29_a_ainput_o : std_logic;
SIGNAL intr_right_a28_a_ainput_o : std_logic;
SIGNAL intr_right_a27_a_ainput_o : std_logic;
SIGNAL intr_right_a26_a_ainput_o : std_logic;
SIGNAL intr_right_a25_a_ainput_o : std_logic;
SIGNAL intr_right_a24_a_ainput_o : std_logic;
SIGNAL intr_right_a17_a_ainput_o : std_logic;
SIGNAL intr_right_a16_a_ainput_o : std_logic;
SIGNAL intr_right_a14_a_ainput_o : std_logic;
SIGNAL intr_right_a13_a_ainput_o : std_logic;
SIGNAL intr_right_a12_a_ainput_o : std_logic;
SIGNAL intr_right_a11_a_ainput_o : std_logic;
SIGNAL intr_right_a10_a_ainput_o : std_logic;
SIGNAL intr_right_a9_a_ainput_o : std_logic;
SIGNAL intr_right_a1_a_ainput_o : std_logic;
SIGNAL intr_right_a0_a_ainput_o : std_logic;
SIGNAL clk_ainput_o : std_logic;
SIGNAL mux_hi_a0_a_ainput_o : std_logic;
SIGNAL intr_pc_a1_a_ainput_o : std_logic;
SIGNAL intr_pc_a2_a_ainput_o : std_logic;
SIGNAL intr_pc_a3_a_ainput_o : std_logic;
SIGNAL intr_pc_a4_a_ainput_o : std_logic;
SIGNAL intr_pc_a5_a_ainput_o : std_logic;
SIGNAL intr_pc_a6_a_ainput_o : std_logic;
SIGNAL intr_pc_a7_a_ainput_o : std_logic;
SIGNAL intr_pc_a8_a_ainput_o : std_logic;
SIGNAL intr_pc_a9_a_ainput_o : std_logic;
SIGNAL intr_pc_a10_a_ainput_o : std_logic;
SIGNAL intr_pc_a11_a_ainput_o : std_logic;
SIGNAL intr_pc_a16_a_ainput_o : std_logic;
SIGNAL intr_pc_a24_a_ainput_o : std_logic;
SIGNAL clk_ainputclkctrl_outclk : std_logic;
SIGNAL data_out_a0_a_aoutput_o : std_logic;
SIGNAL data_out_a1_a_aoutput_o : std_logic;
SIGNAL data_out_a2_a_aoutput_o : std_logic;
SIGNAL data_out_a3_a_aoutput_o : std_logic;
SIGNAL data_out_a4_a_aoutput_o : std_logic;
SIGNAL data_out_a5_a_aoutput_o : std_logic;
SIGNAL data_out_a6_a_aoutput_o : std_logic;
SIGNAL data_out_a7_a_aoutput_o : std_logic;
SIGNAL data_out_a8_a_aoutput_o : std_logic;
SIGNAL data_out_a9_a_aoutput_o : std_logic;
SIGNAL data_out_a10_a_aoutput_o : std_logic;
SIGNAL data_out_a11_a_aoutput_o : std_logic;
SIGNAL data_out_a12_a_aoutput_o : std_logic;
SIGNAL data_out_a13_a_aoutput_o : std_logic;
SIGNAL data_out_a14_a_aoutput_o : std_logic;
SIGNAL data_out_a15_a_aoutput_o : std_logic;
SIGNAL data_out_a16_a_aoutput_o : std_logic;
SIGNAL data_out_a17_a_aoutput_o : std_logic;
SIGNAL data_out_a18_a_aoutput_o : std_logic;
SIGNAL data_out_a19_a_aoutput_o : std_logic;
SIGNAL data_out_a20_a_aoutput_o : std_logic;
SIGNAL data_out_a21_a_aoutput_o : std_logic;
SIGNAL data_out_a22_a_aoutput_o : std_logic;
SIGNAL data_out_a23_a_aoutput_o : std_logic;
SIGNAL data_out_a24_a_aoutput_o : std_logic;
SIGNAL data_out_a25_a_aoutput_o : std_logic;
SIGNAL data_out_a26_a_aoutput_o : std_logic;
SIGNAL data_out_a27_a_aoutput_o : std_logic;
SIGNAL data_out_a28_a_aoutput_o : std_logic;
SIGNAL data_out_a29_a_aoutput_o : std_logic;
SIGNAL data_out_a30_a_aoutput_o : std_logic;
SIGNAL data_out_a31_a_aoutput_o : std_logic;
SIGNAL address_a0_a_aoutput_o : std_logic;
SIGNAL address_a1_a_aoutput_o : std_logic;
SIGNAL address_a2_a_aoutput_o : std_logic;
SIGNAL address_a3_a_aoutput_o : std_logic;
SIGNAL address_a4_a_aoutput_o : std_logic;
SIGNAL address_a5_a_aoutput_o : std_logic;
SIGNAL address_a6_a_aoutput_o : std_logic;
SIGNAL address_a7_a_aoutput_o : std_logic;
SIGNAL address_a8_a_aoutput_o : std_logic;
SIGNAL address_a9_a_aoutput_o : std_logic;
SIGNAL address_a10_a_aoutput_o : std_logic;
SIGNAL address_a11_a_aoutput_o : std_logic;
SIGNAL address_a12_a_aoutput_o : std_logic;
SIGNAL address_a13_a_aoutput_o : std_logic;
SIGNAL address_a14_a_aoutput_o : std_logic;
SIGNAL address_a15_a_aoutput_o : std_logic;
SIGNAL address_a16_a_aoutput_o : std_logic;
SIGNAL address_a17_a_aoutput_o : std_logic;
SIGNAL address_a18_a_aoutput_o : std_logic;
SIGNAL address_a19_a_aoutput_o : std_logic;
SIGNAL address_a20_a_aoutput_o : std_logic;
SIGNAL address_a21_a_aoutput_o : std_logic;
SIGNAL address_a22_a_aoutput_o : std_logic;
SIGNAL address_a23_a_aoutput_o : std_logic;
SIGNAL address_a24_a_aoutput_o : std_logic;
SIGNAL address_a25_a_aoutput_o : std_logic;
SIGNAL address_a26_a_aoutput_o : std_logic;
SIGNAL address_a27_a_aoutput_o : std_logic;
SIGNAL address_a28_a_aoutput_o : std_logic;
SIGNAL address_a29_a_aoutput_o : std_logic;
SIGNAL address_a30_a_aoutput_o : std_logic;
SIGNAL address_a31_a_aoutput_o : std_logic;
SIGNAL a_zero_aoutput_o : std_logic;
SIGNAL a_msb_aoutput_o : std_logic;
SIGNAL alu_c_aoutput_o : std_logic;
SIGNAL alu_msb_aoutput_o : std_logic;
SIGNAL mux_data_out_a2_a_ainput_o : std_logic;
SIGNAL mux_pc_a1_a_ainput_o : std_logic;
SIGNAL mux_alu_left_a0_a_ainput_o : std_logic;
SIGNAL reset_ainput_o : std_logic;
SIGNAL data_in_a0_a_ainput_o : std_logic;
SIGNAL A_a0_combout : std_logic;
SIGNAL mux_a_a0_a_ainput_o : std_logic;
SIGNAL A_a3_a_a1_combout : std_logic;
SIGNAL mux_lo_a1_a_ainput_o : std_logic;
SIGNAL data_in_a31_a_ainput_o : std_logic;
SIGNAL data_in_a29_a_ainput_o : std_logic;
SIGNAL data_in_a28_a_ainput_o : std_logic;
SIGNAL LO_a29_combout : std_logic;
SIGNAL mux_lo_a0_a_ainput_o : std_logic;
SIGNAL LO_a0_a_a1_combout : std_logic;
SIGNAL LO_a30_combout : std_logic;
SIGNAL LO_a31_combout : std_logic;
SIGNAL LO_a32_combout : std_logic;
SIGNAL left_a0_a_a31_combout : std_logic;
SIGNAL Add0_a1_cout : std_logic;
SIGNAL Add0_a2_combout : std_logic;
SIGNAL PC_a0_combout : std_logic;
SIGNAL mux_pc_a0_a_ainput_o : std_logic;
SIGNAL PC_a1_a_a1_combout : std_logic;
SIGNAL mux_data_out_a1_a_ainput_o : std_logic;
SIGNAL mux_data_out_a0_a_ainput_o : std_logic;
SIGNAL data_out_a0_combout : std_logic;
SIGNAL alu_add_sub_ainput_o : std_logic;
SIGNAL mux_hi_a1_a_ainput_o : std_logic;
SIGNAL HI_a14_a_a0_combout : std_logic;
SIGNAL mux_alu_right_a0_a_ainput_o : std_logic;
SIGNAL A_a30_combout : std_logic;
SIGNAL mux_b_a0_a_ainput_o : std_logic;
SIGNAL B_a22_a_a0_combout : std_logic;
SIGNAL mux_alu_right_a1_a_ainput_o : std_logic;
SIGNAL op2_real_a182_combout : std_logic;
SIGNAL op2_real_a183_combout : std_logic;
SIGNAL data_in_a27_a_ainput_o : std_logic;
SIGNAL A_a28_combout : std_logic;
SIGNAL mux_alu_left_a1_a_ainput_o : std_logic;
SIGNAL data_in_a26_a_ainput_o : std_logic;
SIGNAL A_a27_combout : std_logic;
SIGNAL left_a26_a_a5_combout : std_logic;
SIGNAL PC_a27_combout : std_logic;
SIGNAL intr_pc_a23_a_ainput_o : std_logic;
SIGNAL PC_a26_combout : std_logic;
SIGNAL data_in_a25_a_ainput_o : std_logic;
SIGNAL A_a26_combout : std_logic;
SIGNAL intr_right_a22_a_ainput_o : std_logic;
SIGNAL data_in_a22_a_ainput_o : std_logic;
SIGNAL A_a23_combout : std_logic;
SIGNAL op2_real_a168_combout : std_logic;
SIGNAL op2_real_a169_combout : std_logic;
SIGNAL intr_right_a21_a_ainput_o : std_logic;
SIGNAL data_in_a21_a_ainput_o : std_logic;
SIGNAL A_a22_combout : std_logic;
SIGNAL op2_real_a166_combout : std_logic;
SIGNAL op2_real_a167_combout : std_logic;
SIGNAL intr_right_a20_a_ainput_o : std_logic;
SIGNAL A_a21_combout : std_logic;
SIGNAL op2_real_a164_combout : std_logic;
SIGNAL op2_real_a165_combout : std_logic;
SIGNAL intr_right_a19_a_ainput_o : std_logic;
SIGNAL data_in_a19_a_ainput_o : std_logic;
SIGNAL A_a20_combout : std_logic;
SIGNAL op2_real_a162_combout : std_logic;
SIGNAL op2_real_a163_combout : std_logic;
SIGNAL intr_right_a18_a_ainput_o : std_logic;
SIGNAL data_in_a18_a_ainput_o : std_logic;
SIGNAL A_a19_combout : std_logic;
SIGNAL op2_real_a160_combout : std_logic;
SIGNAL op2_real_a161_combout : std_logic;
SIGNAL intr_pc_a15_a_ainput_o : std_logic;
SIGNAL A_a17_combout : std_logic;
SIGNAL intr_right_a15_a_ainput_o : std_logic;
SIGNAL A_a16_combout : std_logic;
SIGNAL op2_real_a154_combout : std_logic;
SIGNAL op2_real_a155_combout : std_logic;
SIGNAL data_in_a12_a_ainput_o : std_logic;
SIGNAL A_a13_combout : std_logic;
SIGNAL intr_right_a7_a_ainput_o : std_logic;
SIGNAL data_in_a7_a_ainput_o : std_logic;
SIGNAL A_a8_combout : std_logic;
SIGNAL op2_real_a138_combout : std_logic;
SIGNAL op2_real_a139_combout : std_logic;
SIGNAL intr_right_a6_a_ainput_o : std_logic;
SIGNAL A_a7_combout : std_logic;
SIGNAL op2_real_a136_combout : std_logic;
SIGNAL op2_real_a137_combout : std_logic;
SIGNAL intr_right_a5_a_ainput_o : std_logic;
SIGNAL A_a6_combout : std_logic;
SIGNAL op2_real_a134_combout : std_logic;
SIGNAL op2_real_a135_combout : std_logic;
SIGNAL intr_right_a4_a_ainput_o : std_logic;
SIGNAL A_a5_combout : std_logic;
SIGNAL op2_real_a132_combout : std_logic;
SIGNAL op2_real_a133_combout : std_logic;
SIGNAL intr_right_a3_a_ainput_o : std_logic;
SIGNAL A_a4_combout : std_logic;
SIGNAL op2_real_a130_combout : std_logic;
SIGNAL op2_real_a131_combout : std_logic;
SIGNAL intr_right_a2_a_ainput_o : std_logic;
SIGNAL op2_real_a124_combout : std_logic;
SIGNAL data_in_a2_a_ainput_o : std_logic;
SIGNAL A_a3_combout : std_logic;
SIGNAL op2_real_a125_combout : std_logic;
SIGNAL Add0_a3 : std_logic;
SIGNAL Add0_a4_combout : std_logic;
SIGNAL HI_a1_combout : std_logic;
SIGNAL HI_a14_a_a2_combout : std_logic;
SIGNAL HI_a3_combout : std_logic;
SIGNAL data_in_a1_a_ainput_o : std_logic;
SIGNAL A_a2_combout : std_logic;
SIGNAL left_a1_a_a30_combout : std_logic;
SIGNAL Add0_a5 : std_logic;
SIGNAL Add0_a7 : std_logic;
SIGNAL Add0_a9 : std_logic;
SIGNAL Add0_a11 : std_logic;
SIGNAL Add0_a13 : std_logic;
SIGNAL Add0_a15 : std_logic;
SIGNAL Add0_a16_combout : std_logic;
SIGNAL HI_a9_combout : std_logic;
SIGNAL intr_right_a8_a_ainput_o : std_logic;
SIGNAL data_in_a8_a_ainput_o : std_logic;
SIGNAL A_a9_combout : std_logic;
SIGNAL op2_real_a140_combout : std_logic;
SIGNAL op2_real_a141_combout : std_logic;
SIGNAL Add0_a17 : std_logic;
SIGNAL Add0_a18_combout : std_logic;
SIGNAL HI_a10_combout : std_logic;
SIGNAL PC_a10_combout : std_logic;
SIGNAL data_in_a9_a_ainput_o : std_logic;
SIGNAL A_a10_combout : std_logic;
SIGNAL left_a9_a_a22_combout : std_logic;
SIGNAL Add0_a19 : std_logic;
SIGNAL Add0_a20_combout : std_logic;
SIGNAL HI_a11_combout : std_logic;
SIGNAL data_in_a10_a_ainput_o : std_logic;
SIGNAL A_a11_combout : std_logic;
SIGNAL left_a10_a_a21_combout : std_logic;
SIGNAL PC_a11_combout : std_logic;
SIGNAL Add0_a21 : std_logic;
SIGNAL Add0_a22_combout : std_logic;
SIGNAL HI_a12_combout : std_logic;
SIGNAL data_in_a11_a_ainput_o : std_logic;
SIGNAL A_a12_combout : std_logic;
SIGNAL op2_real_a146_combout : std_logic;
SIGNAL op2_real_a147_combout : std_logic;
SIGNAL Add0_a23 : std_logic;
SIGNAL Add0_a24_combout : std_logic;
SIGNAL HI_a13_combout : std_logic;
SIGNAL left_a12_a_a19_combout : std_logic;
SIGNAL PC_a13_combout : std_logic;
SIGNAL Add0_a25 : std_logic;
SIGNAL Add0_a26_combout : std_logic;
SIGNAL HI_a14_combout : std_logic;
SIGNAL data_in_a13_a_ainput_o : std_logic;
SIGNAL A_a14_combout : std_logic;
SIGNAL left_a13_a_a18_combout : std_logic;
SIGNAL PC_a14_combout : std_logic;
SIGNAL Add0_a27 : std_logic;
SIGNAL Add0_a28_combout : std_logic;
SIGNAL HI_a15_combout : std_logic;
SIGNAL Add0_a29 : std_logic;
SIGNAL Add0_a30_combout : std_logic;
SIGNAL HI_a16_combout : std_logic;
SIGNAL intr_pc_a12_a_ainput_o : std_logic;
SIGNAL PC_a15_combout : std_logic;
SIGNAL data_in_a14_a_ainput_o : std_logic;
SIGNAL Add0_a31 : std_logic;
SIGNAL Add0_a32_combout : std_logic;
SIGNAL HI_a17_combout : std_logic;
SIGNAL left_a16_a_a15_combout : std_logic;
SIGNAL HI_a18_combout : std_logic;
SIGNAL intr_pc_a14_a_ainput_o : std_logic;
SIGNAL Add0_a33 : std_logic;
SIGNAL Add0_a34_combout : std_logic;
SIGNAL PC_a17_combout : std_logic;
SIGNAL data_in_a16_a_ainput_o : std_logic;
SIGNAL Add0_a35 : std_logic;
SIGNAL Add0_a36_combout : std_logic;
SIGNAL PC_a18_combout : std_logic;
SIGNAL data_in_a17_a_ainput_o : std_logic;
SIGNAL A_a18_combout : std_logic;
SIGNAL left_a17_a_a14_combout : std_logic;
SIGNAL HI_a19_combout : std_logic;
SIGNAL Add0_a37 : std_logic;
SIGNAL Add0_a39 : std_logic;
SIGNAL Add0_a41 : std_logic;
SIGNAL Add0_a43 : std_logic;
SIGNAL Add0_a45 : std_logic;
SIGNAL Add0_a46_combout : std_logic;
SIGNAL Add0_a44_combout : std_logic;
SIGNAL HI_a23_combout : std_logic;
SIGNAL HI_a24_combout : std_logic;
SIGNAL HI_a25_combout : std_logic;
SIGNAL HI_a26_combout : std_logic;
SIGNAL left_a25_a_a6_combout : std_logic;
SIGNAL HI_a27_combout : std_logic;
SIGNAL data_in_a24_a_ainput_o : std_logic;
SIGNAL A_a25_combout : std_logic;
SIGNAL left_a24_a_a7_combout : std_logic;
SIGNAL intr_right_a23_a_ainput_o : std_logic;
SIGNAL data_in_a23_a_ainput_o : std_logic;
SIGNAL A_a24_combout : std_logic;
SIGNAL op2_real_a170_combout : std_logic;
SIGNAL op2_real_a171_combout : std_logic;
SIGNAL Add0_a47 : std_logic;
SIGNAL Add0_a49 : std_logic;
SIGNAL Add0_a51 : std_logic;
SIGNAL Add0_a53 : std_logic;
SIGNAL Add0_a54_combout : std_logic;
SIGNAL HI_a28_combout : std_logic;
SIGNAL left_a27_a_a4_combout : std_logic;
SIGNAL Add0_a55 : std_logic;
SIGNAL Add0_a56_combout : std_logic;
SIGNAL HI_a29_combout : std_logic;
SIGNAL Add0_a57 : std_logic;
SIGNAL Add0_a58_combout : std_logic;
SIGNAL PC_a29_combout : std_logic;
SIGNAL PC_a29_a_a30_combout : std_logic;
SIGNAL HI_a30_combout : std_logic;
SIGNAL Add0_a59 : std_logic;
SIGNAL Add0_a61 : std_logic;
SIGNAL Add0_a62_combout : std_logic;
SIGNAL HI_a32_combout : std_logic;
SIGNAL PC_a32_combout : std_logic;
SIGNAL Add0_a63 : std_logic;
SIGNAL Add0_a64_combout : std_logic;
SIGNAL HI_a33_combout : std_logic;
SIGNAL A_a32_combout : std_logic;
SIGNAL left_a31_a_a0_combout : std_logic;
SIGNAL Add0_a65 : std_logic;
SIGNAL Add0_a66_combout : std_logic;
SIGNAL LO_a0_combout : std_logic;
SIGNAL data_out_a2_combout : std_logic;
SIGNAL data_out_a3_combout : std_logic;
SIGNAL LO_a2_combout : std_logic;
SIGNAL data_out_a5_combout : std_logic;
SIGNAL PC_a2_combout : std_logic;
SIGNAL data_out_a6_combout : std_logic;
SIGNAL intr_pc_a0_a_ainput_o : std_logic;
SIGNAL PC_a3_combout : std_logic;
SIGNAL Add0_a6_combout : std_logic;
SIGNAL HI_a4_combout : std_logic;
SIGNAL LO_a3_combout : std_logic;
SIGNAL mux_out_a0_a_ainput_o : std_logic;
SIGNAL O_a2_a_a1_combout : std_logic;
SIGNAL data_in_a3_a_ainput_o : std_logic;
SIGNAL O_a3_a_a2_combout : std_logic;
SIGNAL data_in_a4_a_ainput_o : std_logic;
SIGNAL Add0_a10_combout : std_logic;
SIGNAL O_a4_a_a3_combout : std_logic;
SIGNAL data_in_a5_a_ainput_o : std_logic;
SIGNAL Add0_a12_combout : std_logic;
SIGNAL O_a5_a_a4_combout : std_logic;
SIGNAL data_in_a6_a_ainput_o : std_logic;
SIGNAL Add0_a14_combout : std_logic;
SIGNAL O_a6_a_a5_combout : std_logic;
SIGNAL O_a7_a_a6_combout : std_logic;
SIGNAL O_a8_a_a7_combout : std_logic;
SIGNAL O_a9_a_a8_combout : std_logic;
SIGNAL O_a10_a_a9_combout : std_logic;
SIGNAL O_a11_a_a10_combout : std_logic;
SIGNAL O_a12_a_a11_combout : std_logic;
SIGNAL O_a13_a_a12_combout : std_logic;
SIGNAL O_a14_a_a13_combout : std_logic;
SIGNAL O_a15_a_a14_combout : std_logic;
SIGNAL O_a16_a_a15_combout : std_logic;
SIGNAL O_a17_a_a16_combout : std_logic;
SIGNAL Add0_a38_combout : std_logic;
SIGNAL O_a18_a_a17_combout : std_logic;
SIGNAL Add0_a40_combout : std_logic;
SIGNAL O_a19_a_a18_combout : std_logic;
SIGNAL Add0_a42_combout : std_logic;
SIGNAL O_a20_a_a19_combout : std_logic;
SIGNAL O_a21_a_a20_combout : std_logic;
SIGNAL O_a22_a_a21_combout : std_logic;
SIGNAL Add0_a48_combout : std_logic;
SIGNAL O_a23_a_a22_combout : std_logic;
SIGNAL Add0_a50_combout : std_logic;
SIGNAL O_a24_a_a23_combout : std_logic;
SIGNAL Add0_a52_combout : std_logic;
SIGNAL O_a25_a_a24_combout : std_logic;
SIGNAL O_a26_a_a25_combout : std_logic;
SIGNAL O_a27_a_a26_combout : std_logic;
SIGNAL O_a28_a_a27_combout : std_logic;
SIGNAL Add0_a60_combout : std_logic;
SIGNAL O_a29_a_a28_combout : std_logic;
SIGNAL data_in_a30_a_ainput_o : std_logic;
SIGNAL O_a30_a_a29_combout : std_logic;
SIGNAL mux_out_a1_a_ainput_o : std_logic;
SIGNAL O_n_a31_a_a4_combout : std_logic;
SIGNAL O_n_a31_a_a6_combout : std_logic;
SIGNAL O_a27_a_a30_combout : std_logic;
SIGNAL mux_out_a2_a_ainput_o : std_logic;
SIGNAL O_a27_a_a31_combout : std_logic;
SIGNAL data_out_a7_combout : std_logic;
SIGNAL data_out_a8_combout : std_logic;
SIGNAL data_out_a9_combout : std_logic;
SIGNAL Add0_a8_combout : std_logic;
SIGNAL PC_a4_combout : std_logic;
SIGNAL LO_a4_combout : std_logic;
SIGNAL data_out_a11_combout : std_logic;
SIGNAL data_out_a12_combout : std_logic;
SIGNAL HI_a6_combout : std_logic;
SIGNAL LO_a5_combout : std_logic;
SIGNAL data_out_a14_combout : std_logic;
SIGNAL PC_a5_combout : std_logic;
SIGNAL data_out_a15_combout : std_logic;
SIGNAL HI_a7_combout : std_logic;
SIGNAL data_out_a16_combout : std_logic;
SIGNAL LO_a6_combout : std_logic;
SIGNAL data_out_a17_combout : std_logic;
SIGNAL PC_a6_combout : std_logic;
SIGNAL data_out_a18_combout : std_logic;
SIGNAL PC_a7_combout : std_logic;
SIGNAL HI_a8_combout : std_logic;
SIGNAL data_out_a19_combout : std_logic;
SIGNAL data_out_a20_combout : std_logic;
SIGNAL data_out_a21_combout : std_logic;
SIGNAL data_out_a22_combout : std_logic;
SIGNAL LO_a7_combout : std_logic;
SIGNAL LO_a8_combout : std_logic;
SIGNAL data_out_a23_combout : std_logic;
SIGNAL PC_a8_combout : std_logic;
SIGNAL data_out_a24_combout : std_logic;
SIGNAL PC_a9_combout : std_logic;
SIGNAL LO_a9_combout : std_logic;
SIGNAL data_out_a26_combout : std_logic;
SIGNAL data_out_a27_combout : std_logic;
SIGNAL data_out_a28_combout : std_logic;
SIGNAL data_out_a29_combout : std_logic;
SIGNAL data_out_a30_combout : std_logic;
SIGNAL LO_a11_combout : std_logic;
SIGNAL data_out_a32_combout : std_logic;
SIGNAL data_out_a33_combout : std_logic;
SIGNAL PC_a12_combout : std_logic;
SIGNAL LO_a12_combout : std_logic;
SIGNAL data_out_a34_combout : std_logic;
SIGNAL data_out_a35_combout : std_logic;
SIGNAL data_out_a36_combout : std_logic;
SIGNAL data_out_a37_combout : std_logic;
SIGNAL LO_a13_combout : std_logic;
SIGNAL data_out_a38_combout : std_logic;
SIGNAL data_out_a39_combout : std_logic;
SIGNAL LO_a14_combout : std_logic;
SIGNAL data_out_a41_combout : std_logic;
SIGNAL data_out_a42_combout : std_logic;
SIGNAL A_a15_combout : std_logic;
SIGNAL data_out_a43_combout : std_logic;
SIGNAL LO_a15_combout : std_logic;
SIGNAL data_out_a44_combout : std_logic;
SIGNAL data_out_a45_combout : std_logic;
SIGNAL data_in_a15_a_ainput_o : std_logic;
SIGNAL LO_a16_combout : std_logic;
SIGNAL data_out_a47_combout : std_logic;
SIGNAL intr_pc_a13_a_ainput_o : std_logic;
SIGNAL PC_a16_combout : std_logic;
SIGNAL data_out_a48_combout : std_logic;
SIGNAL data_out_a49_combout : std_logic;
SIGNAL LO_a17_combout : std_logic;
SIGNAL data_out_a50_combout : std_logic;
SIGNAL data_out_a51_combout : std_logic;
SIGNAL data_out_a52_combout : std_logic;
SIGNAL data_out_a53_combout : std_logic;
SIGNAL data_out_a54_combout : std_logic;
SIGNAL LO_a18_combout : std_logic;
SIGNAL LO_a19_combout : std_logic;
SIGNAL HI_a20_combout : std_logic;
SIGNAL data_out_a56_combout : std_logic;
SIGNAL PC_a19_combout : std_logic;
SIGNAL data_out_a57_combout : std_logic;
SIGNAL HI_a21_combout : std_logic;
SIGNAL data_out_a58_combout : std_logic;
SIGNAL data_out_a59_combout : std_logic;
SIGNAL intr_pc_a17_a_ainput_o : std_logic;
SIGNAL PC_a20_combout : std_logic;
SIGNAL data_out_a60_combout : std_logic;
SIGNAL intr_pc_a18_a_ainput_o : std_logic;
SIGNAL PC_a21_combout : std_logic;
SIGNAL data_in_a20_a_ainput_o : std_logic;
SIGNAL HI_a22_combout : std_logic;
SIGNAL data_out_a61_combout : std_logic;
SIGNAL data_out_a62_combout : std_logic;
SIGNAL data_out_a63_combout : std_logic;
SIGNAL intr_pc_a19_a_ainput_o : std_logic;
SIGNAL PC_a22_combout : std_logic;
SIGNAL data_out_a64_combout : std_logic;
SIGNAL LO_a22_combout : std_logic;
SIGNAL data_out_a65_combout : std_logic;
SIGNAL data_out_a66_combout : std_logic;
SIGNAL intr_pc_a20_a_ainput_o : std_logic;
SIGNAL PC_a23_combout : std_logic;
SIGNAL data_out_a67_combout : std_logic;
SIGNAL LO_a23_combout : std_logic;
SIGNAL data_out_a68_combout : std_logic;
SIGNAL data_out_a69_combout : std_logic;
SIGNAL LO_a24_combout : std_logic;
SIGNAL data_out_a71_combout : std_logic;
SIGNAL intr_pc_a21_a_ainput_o : std_logic;
SIGNAL PC_a24_combout : std_logic;
SIGNAL data_out_a72_combout : std_logic;
SIGNAL LO_a25_combout : std_logic;
SIGNAL data_out_a74_combout : std_logic;
SIGNAL intr_pc_a22_a_ainput_o : std_logic;
SIGNAL PC_a25_combout : std_logic;
SIGNAL data_out_a75_combout : std_logic;
SIGNAL LO_a26_combout : std_logic;
SIGNAL data_out_a77_combout : std_logic;
SIGNAL data_out_a78_combout : std_logic;
SIGNAL LO_a27_combout : std_logic;
SIGNAL data_out_a79_combout : std_logic;
SIGNAL data_out_a80_combout : std_logic;
SIGNAL data_out_a81_combout : std_logic;
SIGNAL intr_pc_a25_a_ainput_o : std_logic;
SIGNAL PC_a28_combout : std_logic;
SIGNAL LO_a28_combout : std_logic;
SIGNAL data_out_a82_combout : std_logic;
SIGNAL data_out_a83_combout : std_logic;
SIGNAL data_out_a84_combout : std_logic;
SIGNAL data_out_a86_combout : std_logic;
SIGNAL data_out_a87_combout : std_logic;
SIGNAL HI_a31_combout : std_logic;
SIGNAL data_out_a88_combout : std_logic;
SIGNAL data_out_a89_combout : std_logic;
SIGNAL PC_a31_combout : std_logic;
SIGNAL data_out_a90_combout : std_logic;
SIGNAL A_a31_combout : std_logic;
SIGNAL data_out_a91_combout : std_logic;
SIGNAL data_out_a92_combout : std_logic;
SIGNAL data_out_a93_combout : std_logic;
SIGNAL data_out_a94_combout : std_logic;
SIGNAL data_out_a95_combout : std_logic;
SIGNAL PC_a33_combout : std_logic;
SIGNAL data_out_a96_combout : std_logic;
SIGNAL mux_address_a1_a_ainput_o : std_logic;
SIGNAL mux_address_a0_a_ainput_o : std_logic;
SIGNAL O_n_a0_a_a0_combout : std_logic;
SIGNAL O_n_a0_a_a1_combout : std_logic;
SIGNAL O_n_a0_a_a2_combout : std_logic;
SIGNAL O_n_a0_a_a3_combout : std_logic;
SIGNAL address_reg_a0_a_ainput_o : std_logic;
SIGNAL address_a0_combout : std_logic;
SIGNAL address_a1_combout : std_logic;
SIGNAL O_a1_a_a0_combout : std_logic;
SIGNAL address_a2_combout : std_logic;
SIGNAL address_a3_combout : std_logic;
SIGNAL address_reg_a2_a_ainput_o : std_logic;
SIGNAL address_a4_combout : std_logic;
SIGNAL address_a5_combout : std_logic;
SIGNAL address_reg_a3_a_ainput_o : std_logic;
SIGNAL address_a6_combout : std_logic;
SIGNAL address_a7_combout : std_logic;
SIGNAL address_a8_combout : std_logic;
SIGNAL address_a9_combout : std_logic;
SIGNAL address_a10_combout : std_logic;
SIGNAL address_a11_combout : std_logic;
SIGNAL address_reg_a6_a_ainput_o : std_logic;
SIGNAL address_a12_combout : std_logic;
SIGNAL address_a13_combout : std_logic;
SIGNAL address_reg_a7_a_ainput_o : std_logic;
SIGNAL address_a14_combout : std_logic;
SIGNAL address_a15_combout : std_logic;
SIGNAL address_reg_a8_a_ainput_o : std_logic;
SIGNAL address_a16_combout : std_logic;
SIGNAL address_a17_combout : std_logic;
SIGNAL address_a18_combout : std_logic;
SIGNAL address_a19_combout : std_logic;
SIGNAL address_reg_a10_a_ainput_o : std_logic;
SIGNAL address_a20_combout : std_logic;
SIGNAL address_a21_combout : std_logic;
SIGNAL address_reg_a11_a_ainput_o : std_logic;
SIGNAL address_a22_combout : std_logic;
SIGNAL address_a23_combout : std_logic;
SIGNAL address_reg_a12_a_ainput_o : std_logic;
SIGNAL address_a24_combout : std_logic;
SIGNAL address_a25_combout : std_logic;
SIGNAL address_reg_a13_a_ainput_o : std_logic;
SIGNAL address_a26_combout : std_logic;
SIGNAL address_a27_combout : std_logic;
SIGNAL address_reg_a14_a_ainput_o : std_logic;
SIGNAL address_a28_combout : std_logic;
SIGNAL address_a29_combout : std_logic;
SIGNAL address_a30_combout : std_logic;
SIGNAL address_a31_combout : std_logic;
SIGNAL address_reg_a16_a_ainput_o : std_logic;
SIGNAL address_a32_combout : std_logic;
SIGNAL address_a33_combout : std_logic;
SIGNAL address_reg_a17_a_ainput_o : std_logic;
SIGNAL address_a34_combout : std_logic;
SIGNAL address_a35_combout : std_logic;
SIGNAL address_reg_a18_a_ainput_o : std_logic;
SIGNAL address_a36_combout : std_logic;
SIGNAL address_a37_combout : std_logic;
SIGNAL address_reg_a19_a_ainput_o : std_logic;
SIGNAL address_a38_combout : std_logic;
SIGNAL address_a39_combout : std_logic;
SIGNAL address_reg_a20_a_ainput_o : std_logic;
SIGNAL address_a40_combout : std_logic;
SIGNAL address_a41_combout : std_logic;
SIGNAL address_a42_combout : std_logic;
SIGNAL address_a43_combout : std_logic;
SIGNAL address_a44_combout : std_logic;
SIGNAL address_a45_combout : std_logic;
SIGNAL address_a46_combout : std_logic;
SIGNAL address_a47_combout : std_logic;
SIGNAL address_reg_a24_a_ainput_o : std_logic;
SIGNAL address_a48_combout : std_logic;
SIGNAL address_a49_combout : std_logic;
SIGNAL address_reg_a25_a_ainput_o : std_logic;
SIGNAL address_a50_combout : std_logic;
SIGNAL address_a51_combout : std_logic;
SIGNAL address_reg_a26_a_ainput_o : std_logic;
SIGNAL address_a52_combout : std_logic;
SIGNAL address_a53_combout : std_logic;
SIGNAL address_reg_a27_a_ainput_o : std_logic;
SIGNAL address_a54_combout : std_logic;
SIGNAL address_a55_combout : std_logic;
SIGNAL address_reg_a28_a_ainput_o : std_logic;
SIGNAL address_a56_combout : std_logic;
SIGNAL address_a57_combout : std_logic;
SIGNAL address_reg_a29_a_ainput_o : std_logic;
SIGNAL address_a58_combout : std_logic;
SIGNAL address_a59_combout : std_logic;
SIGNAL address_reg_a30_a_ainput_o : std_logic;
SIGNAL address_a60_combout : std_logic;
SIGNAL address_a61_combout : std_logic;
SIGNAL address_reg_a31_a_ainput_o : std_logic;
SIGNAL address_a62_combout : std_logic;
SIGNAL address_a63_combout : std_logic;
SIGNAL A_a4_a_afeeder_combout : std_logic;
SIGNAL Equal30_a6_combout : std_logic;
SIGNAL Equal30_a5_combout : std_logic;
SIGNAL Equal30_a7_combout : std_logic;
SIGNAL Equal30_a9_combout : std_logic;
SIGNAL Equal30_a3_combout : std_logic;
SIGNAL Equal30_a2_combout : std_logic;
SIGNAL Equal30_a4_combout : std_logic;
SIGNAL Equal30_a0_combout : std_logic;
SIGNAL Equal30_a1_combout : std_logic;
SIGNAL Equal30_a10_combout : std_logic;
SIGNAL left : std_logic_vector(31 DOWNTO 0);
SIGNAL PC : std_logic_vector(31 DOWNTO 0);
SIGNAL O : std_logic_vector(31 DOWNTO 0);
SIGNAL LO : std_logic_vector(31 DOWNTO 0);
SIGNAL HI : std_logic_vector(31 DOWNTO 0);
SIGNAL B : std_logic_vector(31 DOWNTO 0);
SIGNAL A : std_logic_vector(31 DOWNTO 0);
SIGNAL ALT_INV_mux_pc_a0_a_ainput_o : std_logic;
SIGNAL ALT_INV_Add0_a66_combout : std_logic;
SIGNAL LO_a9_a_CLK_driver : std_logic;
SIGNAL LO_a9_a_D_driver : std_logic;
SIGNAL LO_a9_a_SCLR_driver : std_logic;
SIGNAL LO_a9_a_ENA_driver : std_logic;
SIGNAL LO_a19_a_CLK_driver : std_logic;
SIGNAL LO_a19_a_D_driver : std_logic;
SIGNAL LO_a19_a_SCLR_driver : std_logic;
SIGNAL LO_a19_a_ENA_driver : std_logic;
SIGNAL LO_a20_a_CLK_driver : std_logic;
SIGNAL LO_a20_a_D_driver : std_logic;
SIGNAL LO_a20_a_SCLR_driver : std_logic;
SIGNAL LO_a20_a_ENA_driver : std_logic;
SIGNAL data_out_a1_DATAA_driver : std_logic;
SIGNAL data_out_a1_DATAB_driver : std_logic;
SIGNAL data_out_a1_DATAC_driver : std_logic;
SIGNAL data_out_a1_DATAD_driver : std_logic;
SIGNAL data_out_a4_DATAA_driver : std_logic;
SIGNAL data_out_a4_DATAB_driver : std_logic;
SIGNAL data_out_a4_DATAC_driver : std_logic;
SIGNAL data_out_a4_DATAD_driver : std_logic;
SIGNAL HI_a3_a_CLK_driver : std_logic;
SIGNAL HI_a3_a_D_driver : std_logic;
SIGNAL HI_a3_a_ENA_driver : std_logic;
SIGNAL data_out_a10_DATAA_driver : std_logic;
SIGNAL data_out_a10_DATAB_driver : std_logic;
SIGNAL data_out_a10_DATAC_driver : std_logic;
SIGNAL data_out_a10_DATAD_driver : std_logic;
SIGNAL data_out_a13_DATAA_driver : std_logic;
SIGNAL data_out_a13_DATAB_driver : std_logic;
SIGNAL data_out_a13_DATAC_driver : std_logic;
SIGNAL data_out_a13_DATAD_driver : std_logic;
SIGNAL data_out_a25_DATAA_driver : std_logic;
SIGNAL data_out_a25_DATAB_driver : std_logic;
SIGNAL data_out_a25_DATAC_driver : std_logic;
SIGNAL data_out_a25_DATAD_driver : std_logic;
SIGNAL data_out_a31_DATAA_driver : std_logic;
SIGNAL data_out_a31_DATAB_driver : std_logic;
SIGNAL data_out_a31_DATAC_driver : std_logic;
SIGNAL data_out_a31_DATAD_driver : std_logic;
SIGNAL data_out_a40_DATAA_driver : std_logic;
SIGNAL data_out_a40_DATAB_driver : std_logic;
SIGNAL data_out_a40_DATAC_driver : std_logic;
SIGNAL data_out_a40_DATAD_driver : std_logic;
SIGNAL data_out_a46_DATAA_driver : std_logic;
SIGNAL data_out_a46_DATAB_driver : std_logic;
SIGNAL data_out_a46_DATAC_driver : std_logic;
SIGNAL data_out_a46_DATAD_driver : std_logic;
SIGNAL data_out_a55_DATAA_driver : std_logic;
SIGNAL data_out_a55_DATAB_driver : std_logic;
SIGNAL data_out_a55_DATAC_driver : std_logic;
SIGNAL data_out_a55_DATAD_driver : std_logic;
SIGNAL data_out_a70_DATAA_driver : std_logic;
SIGNAL data_out_a70_DATAB_driver : std_logic;
SIGNAL data_out_a70_DATAC_driver : std_logic;
SIGNAL data_out_a70_DATAD_driver : std_logic;
SIGNAL data_out_a73_DATAA_driver : std_logic;
SIGNAL data_out_a73_DATAB_driver : std_logic;
SIGNAL data_out_a73_DATAC_driver : std_logic;
SIGNAL data_out_a73_DATAD_driver : std_logic;
SIGNAL data_out_a76_DATAA_driver : std_logic;
SIGNAL data_out_a76_DATAB_driver : std_logic;
SIGNAL data_out_a76_DATAC_driver : std_logic;
SIGNAL data_out_a76_DATAD_driver : std_logic;
SIGNAL A_a28_a_CLK_driver : std_logic;
SIGNAL A_a28_a_ASDATA_driver : std_logic;
SIGNAL A_a28_a_ENA_driver : std_logic;
SIGNAL data_out_a85_DATAA_driver : std_logic;
SIGNAL data_out_a85_DATAB_driver : std_logic;
SIGNAL data_out_a85_DATAC_driver : std_logic;
SIGNAL data_out_a85_DATAD_driver : std_logic;
SIGNAL Equal30_a8_DATAA_driver : std_logic;
SIGNAL Equal30_a8_DATAB_driver : std_logic;
SIGNAL Equal30_a8_DATAC_driver : std_logic;
SIGNAL Equal30_a8_DATAD_driver : std_logic;
SIGNAL B_a31_a_CLK_driver : std_logic;
SIGNAL B_a31_a_ASDATA_driver : std_logic;
SIGNAL B_a31_a_ENA_driver : std_logic;
SIGNAL B_a30_a_CLK_driver : std_logic;
SIGNAL B_a30_a_ASDATA_driver : std_logic;
SIGNAL B_a30_a_ENA_driver : std_logic;
SIGNAL left_a30_a_a1_DATAA_driver : std_logic;
SIGNAL left_a30_a_a1_DATAB_driver : std_logic;
SIGNAL left_a30_a_a1_DATAC_driver : std_logic;
SIGNAL left_a30_a_a1_DATAD_driver : std_logic;
SIGNAL left_a29_a_a2_DATAA_driver : std_logic;
SIGNAL left_a29_a_a2_DATAB_driver : std_logic;
SIGNAL left_a29_a_a2_DATAC_driver : std_logic;
SIGNAL left_a29_a_a2_DATAD_driver : std_logic;
SIGNAL left_a29_a_DATAA_driver : std_logic;
SIGNAL left_a29_a_DATAB_driver : std_logic;
SIGNAL left_a29_a_DATAC_driver : std_logic;
SIGNAL left_a29_a_DATAD_driver : std_logic;
SIGNAL B_a28_a_CLK_driver : std_logic;
SIGNAL B_a28_a_ASDATA_driver : std_logic;
SIGNAL B_a28_a_ENA_driver : std_logic;
SIGNAL left_a28_a_a3_DATAA_driver : std_logic;
SIGNAL left_a28_a_a3_DATAB_driver : std_logic;
SIGNAL left_a28_a_a3_DATAC_driver : std_logic;
SIGNAL left_a28_a_a3_DATAD_driver : std_logic;
SIGNAL B_a27_a_CLK_driver : std_logic;
SIGNAL B_a27_a_ASDATA_driver : std_logic;
SIGNAL B_a27_a_ENA_driver : std_logic;
SIGNAL B_a26_a_CLK_driver : std_logic;
SIGNAL B_a26_a_ASDATA_driver : std_logic;
SIGNAL B_a26_a_ENA_driver : std_logic;
SIGNAL B_a25_a_CLK_driver : std_logic;
SIGNAL B_a25_a_ASDATA_driver : std_logic;
SIGNAL B_a25_a_ENA_driver : std_logic;
SIGNAL B_a24_a_CLK_driver : std_logic;
SIGNAL B_a24_a_ASDATA_driver : std_logic;
SIGNAL B_a24_a_ENA_driver : std_logic;
SIGNAL left_a23_a_a8_DATAA_driver : std_logic;
SIGNAL left_a23_a_a8_DATAB_driver : std_logic;
SIGNAL left_a23_a_a8_DATAC_driver : std_logic;
SIGNAL left_a23_a_a8_DATAD_driver : std_logic;
SIGNAL left_a23_a_DATAA_driver : std_logic;
SIGNAL left_a23_a_DATAB_driver : std_logic;
SIGNAL left_a23_a_DATAC_driver : std_logic;
SIGNAL left_a23_a_DATAD_driver : std_logic;
SIGNAL left_a22_a_a9_DATAA_driver : std_logic;
SIGNAL left_a22_a_a9_DATAB_driver : std_logic;
SIGNAL left_a22_a_a9_DATAC_driver : std_logic;
SIGNAL left_a22_a_a9_DATAD_driver : std_logic;
SIGNAL left_a22_a_DATAA_driver : std_logic;
SIGNAL left_a22_a_DATAB_driver : std_logic;
SIGNAL left_a22_a_DATAC_driver : std_logic;
SIGNAL left_a22_a_DATAD_driver : std_logic;
SIGNAL left_a21_a_a10_DATAA_driver : std_logic;
SIGNAL left_a21_a_a10_DATAB_driver : std_logic;
SIGNAL left_a21_a_a10_DATAC_driver : std_logic;
SIGNAL left_a21_a_a10_DATAD_driver : std_logic;
SIGNAL left_a21_a_DATAA_driver : std_logic;
SIGNAL left_a21_a_DATAB_driver : std_logic;
SIGNAL left_a21_a_DATAC_driver : std_logic;
SIGNAL left_a21_a_DATAD_driver : std_logic;
SIGNAL left_a20_a_a11_DATAA_driver : std_logic;
SIGNAL left_a20_a_a11_DATAB_driver : std_logic;
SIGNAL left_a20_a_a11_DATAC_driver : std_logic;
SIGNAL left_a20_a_a11_DATAD_driver : std_logic;
SIGNAL left_a20_a_DATAA_driver : std_logic;
SIGNAL left_a20_a_DATAB_driver : std_logic;
SIGNAL left_a20_a_DATAC_driver : std_logic;
SIGNAL left_a20_a_DATAD_driver : std_logic;
SIGNAL left_a19_a_a12_DATAA_driver : std_logic;
SIGNAL left_a19_a_a12_DATAB_driver : std_logic;
SIGNAL left_a19_a_a12_DATAC_driver : std_logic;
SIGNAL left_a19_a_a12_DATAD_driver : std_logic;
SIGNAL left_a19_a_DATAA_driver : std_logic;
SIGNAL left_a19_a_DATAB_driver : std_logic;
SIGNAL left_a19_a_DATAC_driver : std_logic;
SIGNAL left_a19_a_DATAD_driver : std_logic;
SIGNAL left_a18_a_a13_DATAA_driver : std_logic;
SIGNAL left_a18_a_a13_DATAB_driver : std_logic;
SIGNAL left_a18_a_a13_DATAC_driver : std_logic;
SIGNAL left_a18_a_a13_DATAD_driver : std_logic;
SIGNAL left_a18_a_DATAA_driver : std_logic;
SIGNAL left_a18_a_DATAB_driver : std_logic;
SIGNAL left_a18_a_DATAC_driver : std_logic;
SIGNAL left_a18_a_DATAD_driver : std_logic;
SIGNAL B_a17_a_CLK_driver : std_logic;
SIGNAL B_a17_a_ASDATA_driver : std_logic;
SIGNAL B_a17_a_ENA_driver : std_logic;
SIGNAL B_a16_a_CLK_driver : std_logic;
SIGNAL B_a16_a_ASDATA_driver : std_logic;
SIGNAL B_a16_a_ENA_driver : std_logic;
SIGNAL left_a15_a_a16_DATAA_driver : std_logic;
SIGNAL left_a15_a_a16_DATAB_driver : std_logic;
SIGNAL left_a15_a_a16_DATAC_driver : std_logic;
SIGNAL left_a15_a_a16_DATAD_driver : std_logic;
SIGNAL left_a15_a_DATAA_driver : std_logic;
SIGNAL left_a15_a_DATAB_driver : std_logic;
SIGNAL left_a15_a_DATAC_driver : std_logic;
SIGNAL left_a15_a_DATAD_driver : std_logic;
SIGNAL B_a14_a_CLK_driver : std_logic;
SIGNAL B_a14_a_ASDATA_driver : std_logic;
SIGNAL B_a14_a_ENA_driver : std_logic;
SIGNAL left_a14_a_a17_DATAA_driver : std_logic;
SIGNAL left_a14_a_a17_DATAB_driver : std_logic;
SIGNAL left_a14_a_a17_DATAC_driver : std_logic;
SIGNAL left_a14_a_a17_DATAD_driver : std_logic;
SIGNAL B_a13_a_CLK_driver : std_logic;
SIGNAL B_a13_a_ASDATA_driver : std_logic;
SIGNAL B_a13_a_ENA_driver : std_logic;
SIGNAL B_a12_a_CLK_driver : std_logic;
SIGNAL B_a12_a_ASDATA_driver : std_logic;
SIGNAL B_a12_a_ENA_driver : std_logic;
SIGNAL left_a11_a_a20_DATAA_driver : std_logic;
SIGNAL left_a11_a_a20_DATAB_driver : std_logic;
SIGNAL left_a11_a_a20_DATAC_driver : std_logic;
SIGNAL left_a11_a_a20_DATAD_driver : std_logic;
SIGNAL left_a11_a_DATAA_driver : std_logic;
SIGNAL left_a11_a_DATAB_driver : std_logic;
SIGNAL left_a11_a_DATAC_driver : std_logic;
SIGNAL left_a11_a_DATAD_driver : std_logic;
SIGNAL B_a10_a_CLK_driver : std_logic;
SIGNAL B_a10_a_ASDATA_driver : std_logic;
SIGNAL B_a10_a_ENA_driver : std_logic;
SIGNAL B_a9_a_CLK_driver : std_logic;
SIGNAL B_a9_a_ASDATA_driver : std_logic;
SIGNAL B_a9_a_ENA_driver : std_logic;
SIGNAL left_a8_a_a23_DATAA_driver : std_logic;
SIGNAL left_a8_a_a23_DATAB_driver : std_logic;
SIGNAL left_a8_a_a23_DATAC_driver : std_logic;
SIGNAL left_a8_a_a23_DATAD_driver : std_logic;
SIGNAL left_a8_a_DATAA_driver : std_logic;
SIGNAL left_a8_a_DATAB_driver : std_logic;
SIGNAL left_a8_a_DATAC_driver : std_logic;
SIGNAL left_a8_a_DATAD_driver : std_logic;
SIGNAL left_a7_a_a24_DATAA_driver : std_logic;
SIGNAL left_a7_a_a24_DATAB_driver : std_logic;
SIGNAL left_a7_a_a24_DATAC_driver : std_logic;
SIGNAL left_a7_a_a24_DATAD_driver : std_logic;
SIGNAL left_a7_a_DATAA_driver : std_logic;
SIGNAL left_a7_a_DATAB_driver : std_logic;
SIGNAL left_a7_a_DATAC_driver : std_logic;
SIGNAL left_a7_a_DATAD_driver : std_logic;
SIGNAL left_a6_a_a25_DATAA_driver : std_logic;
SIGNAL left_a6_a_a25_DATAB_driver : std_logic;
SIGNAL left_a6_a_a25_DATAC_driver : std_logic;
SIGNAL left_a6_a_a25_DATAD_driver : std_logic;
SIGNAL left_a6_a_DATAA_driver : std_logic;
SIGNAL left_a6_a_DATAB_driver : std_logic;
SIGNAL left_a6_a_DATAC_driver : std_logic;
SIGNAL left_a6_a_DATAD_driver : std_logic;
SIGNAL left_a5_a_a26_DATAA_driver : std_logic;
SIGNAL left_a5_a_a26_DATAB_driver : std_logic;
SIGNAL left_a5_a_a26_DATAC_driver : std_logic;
SIGNAL left_a5_a_a26_DATAD_driver : std_logic;
SIGNAL left_a5_a_DATAA_driver : std_logic;
SIGNAL left_a5_a_DATAB_driver : std_logic;
SIGNAL left_a5_a_DATAC_driver : std_logic;
SIGNAL left_a5_a_DATAD_driver : std_logic;
SIGNAL left_a4_a_a27_DATAA_driver : std_logic;
SIGNAL left_a4_a_a27_DATAB_driver : std_logic;
SIGNAL left_a4_a_a27_DATAC_driver : std_logic;
SIGNAL left_a4_a_a27_DATAD_driver : std_logic;
SIGNAL left_a4_a_DATAA_driver : std_logic;
SIGNAL left_a4_a_DATAB_driver : std_logic;
SIGNAL left_a4_a_DATAC_driver : std_logic;
SIGNAL left_a4_a_DATAD_driver : std_logic;
SIGNAL left_a3_a_a28_DATAA_driver : std_logic;
SIGNAL left_a3_a_a28_DATAB_driver : std_logic;
SIGNAL left_a3_a_a28_DATAC_driver : std_logic;
SIGNAL left_a3_a_a28_DATAD_driver : std_logic;
SIGNAL left_a3_a_DATAA_driver : std_logic;
SIGNAL left_a3_a_DATAB_driver : std_logic;
SIGNAL left_a3_a_DATAC_driver : std_logic;
SIGNAL left_a3_a_DATAD_driver : std_logic;
SIGNAL left_a2_a_a29_DATAA_driver : std_logic;
SIGNAL left_a2_a_a29_DATAB_driver : std_logic;
SIGNAL left_a2_a_a29_DATAC_driver : std_logic;
SIGNAL left_a2_a_a29_DATAD_driver : std_logic;
SIGNAL left_a2_a_DATAA_driver : std_logic;
SIGNAL left_a2_a_DATAB_driver : std_logic;
SIGNAL left_a2_a_DATAC_driver : std_logic;
SIGNAL left_a2_a_DATAD_driver : std_logic;
SIGNAL B_a1_a_CLK_driver : std_logic;
SIGNAL B_a1_a_ASDATA_driver : std_logic;
SIGNAL B_a1_a_ENA_driver : std_logic;
SIGNAL B_a0_a_CLK_driver : std_logic;
SIGNAL B_a0_a_ASDATA_driver : std_logic;
SIGNAL B_a0_a_ENA_driver : std_logic;
SIGNAL HI_a5_DATAA_driver : std_logic;
SIGNAL HI_a5_DATAB_driver : std_logic;
SIGNAL HI_a5_DATAC_driver : std_logic;
SIGNAL HI_a5_DATAD_driver : std_logic;
SIGNAL LO_a10_DATAB_driver : std_logic;
SIGNAL LO_a10_DATAC_driver : std_logic;
SIGNAL LO_a10_DATAD_driver : std_logic;
SIGNAL LO_a20_DATAA_driver : std_logic;
SIGNAL LO_a20_DATAB_driver : std_logic;
SIGNAL LO_a20_DATAC_driver : std_logic;
SIGNAL LO_a21_DATAB_driver : std_logic;
SIGNAL LO_a21_DATAC_driver : std_logic;
SIGNAL LO_a21_DATAD_driver : std_logic;
SIGNAL A_a29_DATAC_driver : std_logic;
SIGNAL A_a29_DATAD_driver : std_logic;
SIGNAL O_n_a31_a_a5_DATAA_driver : std_logic;
SIGNAL O_n_a31_a_a5_DATAC_driver : std_logic;
SIGNAL O_n_a31_a_a5_DATAD_driver : std_logic;
SIGNAL op2_real_a126_DATAA_driver : std_logic;
SIGNAL op2_real_a126_DATAB_driver : std_logic;
SIGNAL op2_real_a126_DATAC_driver : std_logic;
SIGNAL op2_real_a126_DATAD_driver : std_logic;
SIGNAL op2_real_a127_DATAA_driver : std_logic;
SIGNAL op2_real_a127_DATAC_driver : std_logic;
SIGNAL op2_real_a127_DATAD_driver : std_logic;
SIGNAL op2_real_a128_DATAA_driver : std_logic;
SIGNAL op2_real_a128_DATAB_driver : std_logic;
SIGNAL op2_real_a128_DATAC_driver : std_logic;
SIGNAL op2_real_a128_DATAD_driver : std_logic;
SIGNAL op2_real_a129_DATAB_driver : std_logic;
SIGNAL op2_real_a129_DATAC_driver : std_logic;
SIGNAL op2_real_a129_DATAD_driver : std_logic;
SIGNAL op2_real_a142_DATAA_driver : std_logic;
SIGNAL op2_real_a142_DATAB_driver : std_logic;
SIGNAL op2_real_a142_DATAC_driver : std_logic;
SIGNAL op2_real_a142_DATAD_driver : std_logic;
SIGNAL op2_real_a143_DATAB_driver : std_logic;
SIGNAL op2_real_a143_DATAC_driver : std_logic;
SIGNAL op2_real_a143_DATAD_driver : std_logic;
SIGNAL op2_real_a144_DATAA_driver : std_logic;
SIGNAL op2_real_a144_DATAB_driver : std_logic;
SIGNAL op2_real_a144_DATAC_driver : std_logic;
SIGNAL op2_real_a144_DATAD_driver : std_logic;
SIGNAL op2_real_a145_DATAA_driver : std_logic;
SIGNAL op2_real_a145_DATAB_driver : std_logic;
SIGNAL op2_real_a145_DATAD_driver : std_logic;
SIGNAL op2_real_a148_DATAA_driver : std_logic;
SIGNAL op2_real_a148_DATAB_driver : std_logic;
SIGNAL op2_real_a148_DATAC_driver : std_logic;
SIGNAL op2_real_a148_DATAD_driver : std_logic;
SIGNAL op2_real_a149_DATAA_driver : std_logic;
SIGNAL op2_real_a149_DATAB_driver : std_logic;
SIGNAL op2_real_a149_DATAD_driver : std_logic;
SIGNAL op2_real_a150_DATAA_driver : std_logic;
SIGNAL op2_real_a150_DATAB_driver : std_logic;
SIGNAL op2_real_a150_DATAC_driver : std_logic;
SIGNAL op2_real_a150_DATAD_driver : std_logic;
SIGNAL op2_real_a151_DATAA_driver : std_logic;
SIGNAL op2_real_a151_DATAB_driver : std_logic;
SIGNAL op2_real_a151_DATAD_driver : std_logic;
SIGNAL op2_real_a152_DATAA_driver : std_logic;
SIGNAL op2_real_a152_DATAB_driver : std_logic;
SIGNAL op2_real_a152_DATAC_driver : std_logic;
SIGNAL op2_real_a152_DATAD_driver : std_logic;
SIGNAL op2_real_a153_DATAA_driver : std_logic;
SIGNAL op2_real_a153_DATAC_driver : std_logic;
SIGNAL op2_real_a153_DATAD_driver : std_logic;
SIGNAL op2_real_a156_DATAA_driver : std_logic;
SIGNAL op2_real_a156_DATAB_driver : std_logic;
SIGNAL op2_real_a156_DATAC_driver : std_logic;
SIGNAL op2_real_a156_DATAD_driver : std_logic;
SIGNAL op2_real_a157_DATAB_driver : std_logic;
SIGNAL op2_real_a157_DATAC_driver : std_logic;
SIGNAL op2_real_a157_DATAD_driver : std_logic;
SIGNAL op2_real_a158_DATAA_driver : std_logic;
SIGNAL op2_real_a158_DATAB_driver : std_logic;
SIGNAL op2_real_a158_DATAC_driver : std_logic;
SIGNAL op2_real_a158_DATAD_driver : std_logic;
SIGNAL op2_real_a159_DATAA_driver : std_logic;
SIGNAL op2_real_a159_DATAB_driver : std_logic;
SIGNAL op2_real_a159_DATAC_driver : std_logic;
SIGNAL op2_real_a172_DATAA_driver : std_logic;
SIGNAL op2_real_a172_DATAB_driver : std_logic;
SIGNAL op2_real_a172_DATAC_driver : std_logic;
SIGNAL op2_real_a172_DATAD_driver : std_logic;
SIGNAL op2_real_a173_DATAB_driver : std_logic;
SIGNAL op2_real_a173_DATAC_driver : std_logic;
SIGNAL op2_real_a173_DATAD_driver : std_logic;
SIGNAL op2_real_a174_DATAA_driver : std_logic;
SIGNAL op2_real_a174_DATAB_driver : std_logic;
SIGNAL op2_real_a174_DATAC_driver : std_logic;
SIGNAL op2_real_a174_DATAD_driver : std_logic;
SIGNAL op2_real_a175_DATAB_driver : std_logic;
SIGNAL op2_real_a175_DATAC_driver : std_logic;
SIGNAL op2_real_a175_DATAD_driver : std_logic;
SIGNAL op2_real_a176_DATAA_driver : std_logic;
SIGNAL op2_real_a176_DATAB_driver : std_logic;
SIGNAL op2_real_a176_DATAC_driver : std_logic;
SIGNAL op2_real_a176_DATAD_driver : std_logic;
SIGNAL op2_real_a177_DATAA_driver : std_logic;
SIGNAL op2_real_a177_DATAC_driver : std_logic;
SIGNAL op2_real_a177_DATAD_driver : std_logic;
SIGNAL op2_real_a178_DATAA_driver : std_logic;
SIGNAL op2_real_a178_DATAB_driver : std_logic;
SIGNAL op2_real_a178_DATAC_driver : std_logic;
SIGNAL op2_real_a178_DATAD_driver : std_logic;
SIGNAL op2_real_a179_DATAA_driver : std_logic;
SIGNAL op2_real_a179_DATAC_driver : std_logic;
SIGNAL op2_real_a179_DATAD_driver : std_logic;
SIGNAL op2_real_a180_DATAA_driver : std_logic;
SIGNAL op2_real_a180_DATAB_driver : std_logic;
SIGNAL op2_real_a180_DATAC_driver : std_logic;
SIGNAL op2_real_a180_DATAD_driver : std_logic;
SIGNAL op2_real_a181_DATAA_driver : std_logic;
SIGNAL op2_real_a181_DATAB_driver : std_logic;
SIGNAL op2_real_a181_DATAC_driver : std_logic;
SIGNAL op2_real_a184_DATAA_driver : std_logic;
SIGNAL op2_real_a184_DATAB_driver : std_logic;
SIGNAL op2_real_a184_DATAC_driver : std_logic;
SIGNAL op2_real_a184_DATAD_driver : std_logic;
SIGNAL op2_real_a185_DATAA_driver : std_logic;
SIGNAL op2_real_a185_DATAC_driver : std_logic;
SIGNAL op2_real_a185_DATAD_driver : std_logic;
SIGNAL op2_real_a186_DATAA_driver : std_logic;
SIGNAL op2_real_a186_DATAB_driver : std_logic;
SIGNAL op2_real_a186_DATAC_driver : std_logic;
SIGNAL op2_real_a186_DATAD_driver : std_logic;
SIGNAL op2_real_a187_DATAA_driver : std_logic;
SIGNAL op2_real_a187_DATAC_driver : std_logic;
SIGNAL op2_real_a187_DATAD_driver : std_logic;
SIGNAL address_reg_a1_a_ainput_I_driver : std_logic;
SIGNAL address_reg_a4_a_ainput_I_driver : std_logic;
SIGNAL address_reg_a5_a_ainput_I_driver : std_logic;
SIGNAL address_reg_a9_a_ainput_I_driver : std_logic;
SIGNAL address_reg_a15_a_ainput_I_driver : std_logic;
SIGNAL address_reg_a21_a_ainput_I_driver : std_logic;
SIGNAL address_reg_a22_a_ainput_I_driver : std_logic;
SIGNAL address_reg_a23_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a31_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a30_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a29_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a28_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a27_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a26_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a25_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a24_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a17_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a16_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a14_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a13_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a12_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a11_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a10_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a9_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a1_a_ainput_I_driver : std_logic;
SIGNAL intr_right_a0_a_ainput_I_driver : std_logic;
SIGNAL clk_ainput_I_driver : std_logic;
SIGNAL mux_hi_a0_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a1_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a2_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a3_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a4_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a5_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a6_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a7_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a8_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a9_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a10_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a11_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a16_a_ainput_I_driver : std_logic;
SIGNAL intr_pc_a24_a_ainput_I_driver : std_logic;
SIGNAL data_out_a0_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a1_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a2_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a3_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a4_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a5_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a6_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a7_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a8_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a9_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a10_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a11_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a12_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a13_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a14_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a15_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a16_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a17_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a18_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a19_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a20_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a21_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a22_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a23_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a24_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a25_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a26_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a27_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a28_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a29_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a30_a_aoutput_I_driver : std_logic;
SIGNAL data_out_a31_a_aoutput_I_driver : std_logic;
SIGNAL address_a0_a_aoutput_I_driver : std_logic;
SIGNAL address_a1_a_aoutput_I_driver : std_logic;
SIGNAL address_a2_a_aoutput_I_driver : std_logic;
SIGNAL address_a3_a_aoutput_I_driver : std_logic;
SIGNAL address_a4_a_aoutput_I_driver : std_logic;
SIGNAL address_a5_a_aoutput_I_driver : std_logic;
SIGNAL address_a6_a_aoutput_I_driver : std_logic;
SIGNAL address_a7_a_aoutput_I_driver : std_logic;
SIGNAL address_a8_a_aoutput_I_driver : std_logic;
SIGNAL address_a9_a_aoutput_I_driver : std_logic;
SIGNAL address_a10_a_aoutput_I_driver : std_logic;
SIGNAL address_a11_a_aoutput_I_driver : std_logic;
SIGNAL address_a12_a_aoutput_I_driver : std_logic;
SIGNAL address_a13_a_aoutput_I_driver : std_logic;
SIGNAL address_a14_a_aoutput_I_driver : std_logic;
SIGNAL address_a15_a_aoutput_I_driver : std_logic;
SIGNAL address_a16_a_aoutput_I_driver : std_logic;
SIGNAL address_a17_a_aoutput_I_driver : std_logic;
SIGNAL address_a18_a_aoutput_I_driver : std_logic;
SIGNAL address_a19_a_aoutput_I_driver : std_logic;
SIGNAL address_a20_a_aoutput_I_driver : std_logic;
SIGNAL address_a21_a_aoutput_I_driver : std_logic;
SIGNAL address_a22_a_aoutput_I_driver : std_logic;
SIGNAL address_a23_a_aoutput_I_driver : std_logic;
SIGNAL address_a24_a_aoutput_I_driver : std_logic;
SIGNAL address_a25_a_aoutput_I_driver : std_logic;
SIGNAL address_a26_a_aoutput_I_driver : std_logic;
SIGNAL address_a27_a_aoutput_I_driver : std_logic;
SIGNAL address_a28_a_aoutput_I_driver : std_logic;
SIGNAL address_a29_a_aoutput_I_driver : std_logic;
SIGNAL address_a30_a_aoutput_I_driver : std_logic;
SIGNAL address_a31_a_aoutput_I_driver : std_logic;
SIGNAL a_zero_aoutput_I_driver : std_logic;
SIGNAL a_msb_aoutput_I_driver : std_logic;
SIGNAL alu_c_aoutput_I_driver : std_logic;
SIGNAL alu_msb_aoutput_I_driver : std_logic;
SIGNAL mux_data_out_a2_a_ainput_I_driver : std_logic;
SIGNAL mux_pc_a1_a_ainput_I_driver : std_logic;
SIGNAL mux_alu_left_a0_a_ainput_I_driver : std_logic;
SIGNAL reset_ainput_I_driver : std_logic;
SIGNAL data_in_a0_a_ainput_I_driver : std_logic;
SIGNAL A_a0_DATAB_driver : std_logic;
SIGNAL A_a0_DATAC_driver : std_logic;
SIGNAL mux_a_a0_a_ainput_I_driver : std_logic;
SIGNAL A_a3_a_a1_DATAB_driver : std_logic;
SIGNAL A_a3_a_a1_DATAC_driver : std_logic;
SIGNAL A_a0_a_CLK_driver : std_logic;
SIGNAL A_a0_a_ASDATA_driver : std_logic;
SIGNAL A_a0_a_ENA_driver : std_logic;
SIGNAL mux_lo_a1_a_ainput_I_driver : std_logic;
SIGNAL data_in_a31_a_ainput_I_driver : std_logic;
SIGNAL data_in_a29_a_ainput_I_driver : std_logic;
SIGNAL data_in_a28_a_ainput_I_driver : std_logic;
SIGNAL LO_a29_DATAA_driver : std_logic;
SIGNAL LO_a29_DATAB_driver : std_logic;
SIGNAL LO_a29_DATAD_driver : std_logic;
SIGNAL mux_lo_a0_a_ainput_I_driver : std_logic;
SIGNAL LO_a0_a_a1_DATAB_driver : std_logic;
SIGNAL LO_a0_a_a1_DATAC_driver : std_logic;
SIGNAL LO_a0_a_a1_DATAD_driver : std_logic;
SIGNAL LO_a28_a_CLK_driver : std_logic;
SIGNAL LO_a28_a_D_driver : std_logic;
SIGNAL LO_a28_a_SCLR_driver : std_logic;
SIGNAL LO_a28_a_ENA_driver : std_logic;
SIGNAL LO_a30_DATAB_driver : std_logic;
SIGNAL LO_a30_DATAC_driver : std_logic;
SIGNAL LO_a30_DATAD_driver : std_logic;
SIGNAL LO_a29_a_CLK_driver : std_logic;
SIGNAL LO_a29_a_D_driver : std_logic;
SIGNAL LO_a29_a_SCLR_driver : std_logic;
SIGNAL LO_a29_a_ENA_driver : std_logic;
SIGNAL LO_a31_DATAA_driver : std_logic;
SIGNAL LO_a31_DATAB_driver : std_logic;
SIGNAL LO_a31_DATAD_driver : std_logic;
SIGNAL LO_a30_a_CLK_driver : std_logic;
SIGNAL LO_a30_a_D_driver : std_logic;
SIGNAL LO_a30_a_SCLR_driver : std_logic;
SIGNAL LO_a30_a_ENA_driver : std_logic;
SIGNAL LO_a32_DATAB_driver : std_logic;
SIGNAL LO_a32_DATAC_driver : std_logic;
SIGNAL LO_a32_DATAD_driver : std_logic;
SIGNAL LO_a31_a_CLK_driver : std_logic;
SIGNAL LO_a31_a_ASDATA_driver : std_logic;
SIGNAL LO_a31_a_SCLR_driver : std_logic;
SIGNAL LO_a31_a_ENA_driver : std_logic;
SIGNAL left_a0_a_a31_DATAA_driver : std_logic;
SIGNAL left_a0_a_a31_DATAB_driver : std_logic;
SIGNAL left_a0_a_a31_DATAC_driver : std_logic;
SIGNAL left_a0_a_a31_DATAD_driver : std_logic;
SIGNAL left_a0_a_DATAA_driver : std_logic;
SIGNAL left_a0_a_DATAB_driver : std_logic;
SIGNAL left_a0_a_DATAC_driver : std_logic;
SIGNAL left_a0_a_DATAD_driver : std_logic;
SIGNAL Add0_a1_DATAA_driver : std_logic;
SIGNAL Add0_a2_DATAA_driver : std_logic;
SIGNAL Add0_a2_DATAB_driver : std_logic;
SIGNAL Add0_a2_CIN_driver : std_logic;
SIGNAL PC_a0_DATAB_driver : std_logic;
SIGNAL PC_a0_DATAD_driver : std_logic;
SIGNAL mux_pc_a0_a_ainput_I_driver : std_logic;
SIGNAL PC_a1_a_a1_DATAB_driver : std_logic;
SIGNAL PC_a1_a_a1_DATAC_driver : std_logic;
SIGNAL PC_a1_a_a1_DATAD_driver : std_logic;
SIGNAL PC_a0_a_CLK_driver : std_logic;
SIGNAL PC_a0_a_D_driver : std_logic;
SIGNAL PC_a0_a_ASDATA_driver : std_logic;
SIGNAL PC_a0_a_SCLR_driver : std_logic;
SIGNAL PC_a0_a_SLOAD_driver : std_logic;
SIGNAL PC_a0_a_ENA_driver : std_logic;
SIGNAL mux_data_out_a1_a_ainput_I_driver : std_logic;
SIGNAL mux_data_out_a0_a_ainput_I_driver : std_logic;
SIGNAL data_out_a0_DATAB_driver : std_logic;
SIGNAL data_out_a0_DATAC_driver : std_logic;
SIGNAL data_out_a0_DATAD_driver : std_logic;
SIGNAL alu_add_sub_ainput_I_driver : std_logic;
SIGNAL mux_hi_a1_a_ainput_I_driver : std_logic;
SIGNAL HI_a14_a_a0_DATAC_driver : std_logic;
SIGNAL HI_a14_a_a0_DATAD_driver : std_logic;
SIGNAL mux_alu_right_a0_a_ainput_I_driver : std_logic;
SIGNAL A_a30_DATAC_driver : std_logic;
SIGNAL A_a30_DATAD_driver : std_logic;
SIGNAL mux_b_a0_a_ainput_I_driver : std_logic;
SIGNAL B_a22_a_a0_DATAA_driver : std_logic;
SIGNAL B_a22_a_a0_DATAD_driver : std_logic;
SIGNAL B_a29_a_CLK_driver : std_logic;
SIGNAL B_a29_a_ASDATA_driver : std_logic;
SIGNAL B_a29_a_ENA_driver : std_logic;
SIGNAL mux_alu_right_a1_a_ainput_I_driver : std_logic;
SIGNAL op2_real_a182_DATAA_driver : std_logic;
SIGNAL op2_real_a182_DATAB_driver : std_logic;
SIGNAL op2_real_a182_DATAC_driver : std_logic;
SIGNAL op2_real_a182_DATAD_driver : std_logic;
SIGNAL op2_real_a183_DATAB_driver : std_logic;
SIGNAL op2_real_a183_DATAC_driver : std_logic;
SIGNAL op2_real_a183_DATAD_driver : std_logic;
SIGNAL data_in_a27_a_ainput_I_driver : std_logic;
SIGNAL A_a28_DATAC_driver : std_logic;
SIGNAL A_a28_DATAD_driver : std_logic;
SIGNAL A_a27_a_CLK_driver : std_logic;
SIGNAL A_a27_a_ASDATA_driver : std_logic;
SIGNAL A_a27_a_ENA_driver : std_logic;
SIGNAL mux_alu_left_a1_a_ainput_I_driver : std_logic;
SIGNAL data_in_a26_a_ainput_I_driver : std_logic;
SIGNAL A_a27_DATAC_driver : std_logic;
SIGNAL A_a27_DATAD_driver : std_logic;
SIGNAL A_a26_a_CLK_driver : std_logic;
SIGNAL A_a26_a_ASDATA_driver : std_logic;
SIGNAL A_a26_a_ENA_driver : std_logic;
SIGNAL left_a26_a_a5_DATAA_driver : std_logic;
SIGNAL left_a26_a_a5_DATAB_driver : std_logic;
SIGNAL left_a26_a_a5_DATAC_driver : std_logic;
SIGNAL left_a26_a_a5_DATAD_driver : std_logic;
SIGNAL PC_a27_DATAA_driver : std_logic;
SIGNAL PC_a27_DATAB_driver : std_logic;
SIGNAL PC_a27_DATAD_driver : std_logic;
SIGNAL PC_a26_a_CLK_driver : std_logic;
SIGNAL PC_a26_a_D_driver : std_logic;
SIGNAL PC_a26_a_ASDATA_driver : std_logic;
SIGNAL PC_a26_a_SCLR_driver : std_logic;
SIGNAL PC_a26_a_SLOAD_driver : std_logic;
SIGNAL PC_a26_a_ENA_driver : std_logic;
SIGNAL left_a26_a_DATAA_driver : std_logic;
SIGNAL left_a26_a_DATAB_driver : std_logic;
SIGNAL left_a26_a_DATAC_driver : std_logic;
SIGNAL left_a26_a_DATAD_driver : std_logic;
SIGNAL intr_pc_a23_a_ainput_I_driver : std_logic;
SIGNAL PC_a26_DATAA_driver : std_logic;
SIGNAL PC_a26_DATAB_driver : std_logic;
SIGNAL PC_a26_DATAD_driver : std_logic;
SIGNAL data_in_a25_a_ainput_I_driver : std_logic;
SIGNAL PC_a25_a_CLK_driver : std_logic;
SIGNAL PC_a25_a_D_driver : std_logic;
SIGNAL PC_a25_a_ASDATA_driver : std_logic;
SIGNAL PC_a25_a_SCLR_driver : std_logic;
SIGNAL PC_a25_a_SLOAD_driver : std_logic;
SIGNAL PC_a25_a_ENA_driver : std_logic;
SIGNAL A_a26_DATAB_driver : std_logic;
SIGNAL A_a26_DATAC_driver : std_logic;
SIGNAL A_a25_a_CLK_driver : std_logic;
SIGNAL A_a25_a_ASDATA_driver : std_logic;
SIGNAL A_a25_a_ENA_driver : std_logic;
SIGNAL intr_right_a22_a_ainput_I_driver : std_logic;
SIGNAL data_in_a22_a_ainput_I_driver : std_logic;
SIGNAL A_a23_DATAC_driver : std_logic;
SIGNAL A_a23_DATAD_driver : std_logic;
SIGNAL B_a22_a_CLK_driver : std_logic;
SIGNAL B_a22_a_ASDATA_driver : std_logic;
SIGNAL B_a22_a_ENA_driver : std_logic;
SIGNAL op2_real_a168_DATAA_driver : std_logic;
SIGNAL op2_real_a168_DATAB_driver : std_logic;
SIGNAL op2_real_a168_DATAC_driver : std_logic;
SIGNAL op2_real_a168_DATAD_driver : std_logic;
SIGNAL op2_real_a169_DATAA_driver : std_logic;
SIGNAL op2_real_a169_DATAC_driver : std_logic;
SIGNAL op2_real_a169_DATAD_driver : std_logic;
SIGNAL intr_right_a21_a_ainput_I_driver : std_logic;
SIGNAL data_in_a21_a_ainput_I_driver : std_logic;
SIGNAL A_a22_DATAB_driver : std_logic;
SIGNAL A_a22_DATAC_driver : std_logic;
SIGNAL B_a21_a_CLK_driver : std_logic;
SIGNAL B_a21_a_ASDATA_driver : std_logic;
SIGNAL B_a21_a_ENA_driver : std_logic;
SIGNAL op2_real_a166_DATAA_driver : std_logic;
SIGNAL op2_real_a166_DATAB_driver : std_logic;
SIGNAL op2_real_a166_DATAC_driver : std_logic;
SIGNAL op2_real_a166_DATAD_driver : std_logic;
SIGNAL op2_real_a167_DATAA_driver : std_logic;
SIGNAL op2_real_a167_DATAC_driver : std_logic;
SIGNAL op2_real_a167_DATAD_driver : std_logic;
SIGNAL intr_right_a20_a_ainput_I_driver : std_logic;
SIGNAL A_a21_DATAA_driver : std_logic;
SIGNAL A_a21_DATAD_driver : std_logic;
SIGNAL B_a20_a_CLK_driver : std_logic;
SIGNAL B_a20_a_ASDATA_driver : std_logic;
SIGNAL B_a20_a_ENA_driver : std_logic;
SIGNAL op2_real_a164_DATAA_driver : std_logic;
SIGNAL op2_real_a164_DATAB_driver : std_logic;
SIGNAL op2_real_a164_DATAC_driver : std_logic;
SIGNAL op2_real_a164_DATAD_driver : std_logic;
SIGNAL op2_real_a165_DATAB_driver : std_logic;
SIGNAL op2_real_a165_DATAC_driver : std_logic;
SIGNAL op2_real_a165_DATAD_driver : std_logic;
SIGNAL intr_right_a19_a_ainput_I_driver : std_logic;
SIGNAL data_in_a19_a_ainput_I_driver : std_logic;
SIGNAL A_a20_DATAB_driver : std_logic;
SIGNAL A_a20_DATAD_driver : std_logic;
SIGNAL B_a19_a_CLK_driver : std_logic;
SIGNAL B_a19_a_ASDATA_driver : std_logic;
SIGNAL B_a19_a_ENA_driver : std_logic;
SIGNAL op2_real_a162_DATAA_driver : std_logic;
SIGNAL op2_real_a162_DATAB_driver : std_logic;
SIGNAL op2_real_a162_DATAC_driver : std_logic;
SIGNAL op2_real_a162_DATAD_driver : std_logic;
SIGNAL op2_real_a163_DATAA_driver : std_logic;
SIGNAL op2_real_a163_DATAB_driver : std_logic;
SIGNAL op2_real_a163_DATAC_driver : std_logic;
SIGNAL intr_right_a18_a_ainput_I_driver : std_logic;
SIGNAL data_in_a18_a_ainput_I_driver : std_logic;
SIGNAL A_a19_DATAB_driver : std_logic;
SIGNAL A_a19_DATAC_driver : std_logic;
SIGNAL B_a18_a_CLK_driver : std_logic;
SIGNAL B_a18_a_ASDATA_driver : std_logic;
SIGNAL B_a18_a_ENA_driver : std_logic;
SIGNAL op2_real_a160_DATAA_driver : std_logic;
SIGNAL op2_real_a160_DATAB_driver : std_logic;
SIGNAL op2_real_a160_DATAC_driver : std_logic;
SIGNAL op2_real_a160_DATAD_driver : std_logic;
SIGNAL op2_real_a161_DATAB_driver : std_logic;
SIGNAL op2_real_a161_DATAC_driver : std_logic;
SIGNAL op2_real_a161_DATAD_driver : std_logic;
SIGNAL intr_pc_a15_a_ainput_I_driver : std_logic;
SIGNAL A_a17_DATAA_driver : std_logic;
SIGNAL A_a17_DATAC_driver : std_logic;
SIGNAL A_a16_a_CLK_driver : std_logic;
SIGNAL A_a16_a_ASDATA_driver : std_logic;
SIGNAL A_a16_a_ENA_driver : std_logic;
SIGNAL intr_right_a15_a_ainput_I_driver : std_logic;
SIGNAL A_a16_DATAA_driver : std_logic;
SIGNAL A_a16_DATAC_driver : std_logic;
SIGNAL B_a15_a_CLK_driver : std_logic;
SIGNAL B_a15_a_ASDATA_driver : std_logic;
SIGNAL B_a15_a_ENA_driver : std_logic;
SIGNAL op2_real_a154_DATAA_driver : std_logic;
SIGNAL op2_real_a154_DATAB_driver : std_logic;
SIGNAL op2_real_a154_DATAC_driver : std_logic;
SIGNAL op2_real_a154_DATAD_driver : std_logic;
SIGNAL op2_real_a155_DATAB_driver : std_logic;
SIGNAL op2_real_a155_DATAC_driver : std_logic;
SIGNAL op2_real_a155_DATAD_driver : std_logic;
SIGNAL data_in_a12_a_ainput_I_driver : std_logic;
SIGNAL A_a13_DATAA_driver : std_logic;
SIGNAL A_a13_DATAC_driver : std_logic;
SIGNAL A_a12_a_CLK_driver : std_logic;
SIGNAL A_a12_a_ASDATA_driver : std_logic;
SIGNAL A_a12_a_ENA_driver : std_logic;
SIGNAL intr_right_a7_a_ainput_I_driver : std_logic;
SIGNAL data_in_a7_a_ainput_I_driver : std_logic;
SIGNAL A_a8_DATAA_driver : std_logic;
SIGNAL A_a8_DATAD_driver : std_logic;
SIGNAL B_a7_a_CLK_driver : std_logic;
SIGNAL B_a7_a_ASDATA_driver : std_logic;
SIGNAL B_a7_a_ENA_driver : std_logic;
SIGNAL op2_real_a138_DATAA_driver : std_logic;
SIGNAL op2_real_a138_DATAB_driver : std_logic;
SIGNAL op2_real_a138_DATAC_driver : std_logic;
SIGNAL op2_real_a138_DATAD_driver : std_logic;
SIGNAL op2_real_a139_DATAA_driver : std_logic;
SIGNAL op2_real_a139_DATAC_driver : std_logic;
SIGNAL op2_real_a139_DATAD_driver : std_logic;
SIGNAL intr_right_a6_a_ainput_I_driver : std_logic;
SIGNAL A_a7_DATAA_driver : std_logic;
SIGNAL A_a7_DATAC_driver : std_logic;
SIGNAL B_a6_a_CLK_driver : std_logic;
SIGNAL B_a6_a_ASDATA_driver : std_logic;
SIGNAL B_a6_a_ENA_driver : std_logic;
SIGNAL op2_real_a136_DATAA_driver : std_logic;
SIGNAL op2_real_a136_DATAB_driver : std_logic;
SIGNAL op2_real_a136_DATAC_driver : std_logic;
SIGNAL op2_real_a136_DATAD_driver : std_logic;
SIGNAL op2_real_a137_DATAB_driver : std_logic;
SIGNAL op2_real_a137_DATAC_driver : std_logic;
SIGNAL op2_real_a137_DATAD_driver : std_logic;
SIGNAL intr_right_a5_a_ainput_I_driver : std_logic;
SIGNAL A_a6_DATAA_driver : std_logic;
SIGNAL A_a6_DATAC_driver : std_logic;
SIGNAL B_a5_a_CLK_driver : std_logic;
SIGNAL B_a5_a_ASDATA_driver : std_logic;
SIGNAL B_a5_a_ENA_driver : std_logic;
SIGNAL op2_real_a134_DATAA_driver : std_logic;
SIGNAL op2_real_a134_DATAB_driver : std_logic;
SIGNAL op2_real_a134_DATAC_driver : std_logic;
SIGNAL op2_real_a134_DATAD_driver : std_logic;
SIGNAL op2_real_a135_DATAA_driver : std_logic;
SIGNAL op2_real_a135_DATAC_driver : std_logic;
SIGNAL op2_real_a135_DATAD_driver : std_logic;
SIGNAL intr_right_a4_a_ainput_I_driver : std_logic;
SIGNAL A_a5_DATAA_driver : std_logic;
SIGNAL A_a5_DATAC_driver : std_logic;
SIGNAL B_a4_a_CLK_driver : std_logic;
SIGNAL B_a4_a_ASDATA_driver : std_logic;
SIGNAL B_a4_a_ENA_driver : std_logic;
SIGNAL op2_real_a132_DATAA_driver : std_logic;
SIGNAL op2_real_a132_DATAB_driver : std_logic;
SIGNAL op2_real_a132_DATAC_driver : std_logic;
SIGNAL op2_real_a132_DATAD_driver : std_logic;
SIGNAL op2_real_a133_DATAA_driver : std_logic;
SIGNAL op2_real_a133_DATAC_driver : std_logic;
SIGNAL op2_real_a133_DATAD_driver : std_logic;
SIGNAL intr_right_a3_a_ainput_I_driver : std_logic;
SIGNAL A_a4_DATAA_driver : std_logic;
SIGNAL A_a4_DATAC_driver : std_logic;
SIGNAL B_a3_a_CLK_driver : std_logic;
SIGNAL B_a3_a_ASDATA_driver : std_logic;
SIGNAL B_a3_a_ENA_driver : std_logic;
SIGNAL op2_real_a130_DATAA_driver : std_logic;
SIGNAL op2_real_a130_DATAB_driver : std_logic;
SIGNAL op2_real_a130_DATAC_driver : std_logic;
SIGNAL op2_real_a130_DATAD_driver : std_logic;
SIGNAL op2_real_a131_DATAA_driver : std_logic;
SIGNAL op2_real_a131_DATAC_driver : std_logic;
SIGNAL op2_real_a131_DATAD_driver : std_logic;
SIGNAL intr_right_a2_a_ainput_I_driver : std_logic;
SIGNAL op2_real_a124_DATAA_driver : std_logic;
SIGNAL op2_real_a124_DATAB_driver : std_logic;
SIGNAL op2_real_a124_DATAC_driver : std_logic;
SIGNAL op2_real_a124_DATAD_driver : std_logic;
SIGNAL data_in_a2_a_ainput_I_driver : std_logic;
SIGNAL A_a3_DATAB_driver : std_logic;
SIGNAL A_a3_DATAC_driver : std_logic;
SIGNAL B_a2_a_CLK_driver : std_logic;
SIGNAL B_a2_a_ASDATA_driver : std_logic;
SIGNAL B_a2_a_ENA_driver : std_logic;
SIGNAL op2_real_a125_DATAA_driver : std_logic;
SIGNAL op2_real_a125_DATAB_driver : std_logic;
SIGNAL op2_real_a125_DATAC_driver : std_logic;
SIGNAL op2_real_a125_DATAD_driver : std_logic;
SIGNAL Add0_a4_DATAA_driver : std_logic;
SIGNAL Add0_a4_DATAB_driver : std_logic;
SIGNAL Add0_a4_CIN_driver : std_logic;
SIGNAL HI_a1_DATAA_driver : std_logic;
SIGNAL HI_a1_DATAB_driver : std_logic;
SIGNAL HI_a1_DATAC_driver : std_logic;
SIGNAL HI_a1_DATAD_driver : std_logic;
SIGNAL HI_a14_a_a2_DATAA_driver : std_logic;
SIGNAL HI_a14_a_a2_DATAC_driver : std_logic;
SIGNAL HI_a14_a_a2_DATAD_driver : std_logic;
SIGNAL HI_a0_a_CLK_driver : std_logic;
SIGNAL HI_a0_a_D_driver : std_logic;
SIGNAL HI_a0_a_ENA_driver : std_logic;
SIGNAL HI_a3_DATAA_driver : std_logic;
SIGNAL HI_a3_DATAB_driver : std_logic;
SIGNAL HI_a3_DATAC_driver : std_logic;
SIGNAL HI_a3_DATAD_driver : std_logic;
SIGNAL HI_a1_a_CLK_driver : std_logic;
SIGNAL HI_a1_a_D_driver : std_logic;
SIGNAL HI_a1_a_ENA_driver : std_logic;
SIGNAL data_in_a1_a_ainput_I_driver : std_logic;
SIGNAL A_a2_DATAB_driver : std_logic;
SIGNAL A_a2_DATAC_driver : std_logic;
SIGNAL A_a1_a_CLK_driver : std_logic;
SIGNAL A_a1_a_ASDATA_driver : std_logic;
SIGNAL A_a1_a_ENA_driver : std_logic;
SIGNAL left_a1_a_a30_DATAA_driver : std_logic;
SIGNAL left_a1_a_a30_DATAB_driver : std_logic;
SIGNAL left_a1_a_a30_DATAC_driver : std_logic;
SIGNAL left_a1_a_a30_DATAD_driver : std_logic;
SIGNAL left_a1_a_DATAA_driver : std_logic;
SIGNAL left_a1_a_DATAB_driver : std_logic;
SIGNAL left_a1_a_DATAC_driver : std_logic;
SIGNAL left_a1_a_DATAD_driver : std_logic;
SIGNAL Add0_a6_DATAA_driver : std_logic;
SIGNAL Add0_a6_DATAB_driver : std_logic;
SIGNAL Add0_a6_CIN_driver : std_logic;
SIGNAL Add0_a8_DATAA_driver : std_logic;
SIGNAL Add0_a8_DATAB_driver : std_logic;
SIGNAL Add0_a8_CIN_driver : std_logic;
SIGNAL Add0_a10_DATAA_driver : std_logic;
SIGNAL Add0_a10_DATAB_driver : std_logic;
SIGNAL Add0_a10_CIN_driver : std_logic;
SIGNAL Add0_a12_DATAA_driver : std_logic;
SIGNAL Add0_a12_DATAB_driver : std_logic;
SIGNAL Add0_a12_CIN_driver : std_logic;
SIGNAL Add0_a14_DATAA_driver : std_logic;
SIGNAL Add0_a14_DATAB_driver : std_logic;
SIGNAL Add0_a14_CIN_driver : std_logic;
SIGNAL Add0_a16_DATAA_driver : std_logic;
SIGNAL Add0_a16_DATAB_driver : std_logic;
SIGNAL Add0_a16_CIN_driver : std_logic;
SIGNAL HI_a9_DATAA_driver : std_logic;
SIGNAL HI_a9_DATAB_driver : std_logic;
SIGNAL HI_a9_DATAC_driver : std_logic;
SIGNAL HI_a9_DATAD_driver : std_logic;
SIGNAL HI_a7_a_CLK_driver : std_logic;
SIGNAL HI_a7_a_D_driver : std_logic;
SIGNAL HI_a7_a_ENA_driver : std_logic;
SIGNAL intr_right_a8_a_ainput_I_driver : std_logic;
SIGNAL data_in_a8_a_ainput_I_driver : std_logic;
SIGNAL A_a9_DATAA_driver : std_logic;
SIGNAL A_a9_DATAD_driver : std_logic;
SIGNAL B_a8_a_CLK_driver : std_logic;
SIGNAL B_a8_a_ASDATA_driver : std_logic;
SIGNAL B_a8_a_ENA_driver : std_logic;
SIGNAL op2_real_a140_DATAA_driver : std_logic;
SIGNAL op2_real_a140_DATAB_driver : std_logic;
SIGNAL op2_real_a140_DATAC_driver : std_logic;
SIGNAL op2_real_a140_DATAD_driver : std_logic;
SIGNAL op2_real_a141_DATAA_driver : std_logic;
SIGNAL op2_real_a141_DATAC_driver : std_logic;
SIGNAL op2_real_a141_DATAD_driver : std_logic;
SIGNAL Add0_a18_DATAA_driver : std_logic;
SIGNAL Add0_a18_DATAB_driver : std_logic;
SIGNAL Add0_a18_CIN_driver : std_logic;
SIGNAL HI_a10_DATAA_driver : std_logic;
SIGNAL HI_a10_DATAB_driver : std_logic;
SIGNAL HI_a10_DATAC_driver : std_logic;
SIGNAL HI_a10_DATAD_driver : std_logic;
SIGNAL HI_a8_a_CLK_driver : std_logic;
SIGNAL HI_a8_a_D_driver : std_logic;
SIGNAL HI_a8_a_ENA_driver : std_logic;
SIGNAL PC_a10_DATAA_driver : std_logic;
SIGNAL PC_a10_DATAB_driver : std_logic;
SIGNAL PC_a10_DATAD_driver : std_logic;
SIGNAL data_in_a9_a_ainput_I_driver : std_logic;
SIGNAL PC_a9_a_CLK_driver : std_logic;
SIGNAL PC_a9_a_D_driver : std_logic;
SIGNAL PC_a9_a_ASDATA_driver : std_logic;
SIGNAL PC_a9_a_SCLR_driver : std_logic;
SIGNAL PC_a9_a_SLOAD_driver : std_logic;
SIGNAL PC_a9_a_ENA_driver : std_logic;
SIGNAL A_a10_DATAA_driver : std_logic;
SIGNAL A_a10_DATAC_driver : std_logic;
SIGNAL A_a9_a_CLK_driver : std_logic;
SIGNAL A_a9_a_ASDATA_driver : std_logic;
SIGNAL A_a9_a_ENA_driver : std_logic;
SIGNAL left_a9_a_a22_DATAA_driver : std_logic;
SIGNAL left_a9_a_a22_DATAB_driver : std_logic;
SIGNAL left_a9_a_a22_DATAC_driver : std_logic;
SIGNAL left_a9_a_a22_DATAD_driver : std_logic;
SIGNAL left_a9_a_DATAA_driver : std_logic;
SIGNAL left_a9_a_DATAB_driver : std_logic;
SIGNAL left_a9_a_DATAC_driver : std_logic;
SIGNAL left_a9_a_DATAD_driver : std_logic;
SIGNAL Add0_a20_DATAA_driver : std_logic;
SIGNAL Add0_a20_DATAB_driver : std_logic;
SIGNAL Add0_a20_CIN_driver : std_logic;
SIGNAL HI_a11_DATAA_driver : std_logic;
SIGNAL HI_a11_DATAB_driver : std_logic;
SIGNAL HI_a11_DATAC_driver : std_logic;
SIGNAL HI_a11_DATAD_driver : std_logic;
SIGNAL HI_a9_a_CLK_driver : std_logic;
SIGNAL HI_a9_a_D_driver : std_logic;
SIGNAL HI_a9_a_ENA_driver : std_logic;
SIGNAL data_in_a10_a_ainput_I_driver : std_logic;
SIGNAL A_a11_DATAA_driver : std_logic;
SIGNAL A_a11_DATAC_driver : std_logic;
SIGNAL A_a10_a_CLK_driver : std_logic;
SIGNAL A_a10_a_ASDATA_driver : std_logic;
SIGNAL A_a10_a_ENA_driver : std_logic;
SIGNAL left_a10_a_a21_DATAA_driver : std_logic;
SIGNAL left_a10_a_a21_DATAB_driver : std_logic;
SIGNAL left_a10_a_a21_DATAC_driver : std_logic;
SIGNAL left_a10_a_a21_DATAD_driver : std_logic;
SIGNAL PC_a11_DATAA_driver : std_logic;
SIGNAL PC_a11_DATAB_driver : std_logic;
SIGNAL PC_a11_DATAD_driver : std_logic;
SIGNAL PC_a10_a_CLK_driver : std_logic;
SIGNAL PC_a10_a_D_driver : std_logic;
SIGNAL PC_a10_a_ASDATA_driver : std_logic;
SIGNAL PC_a10_a_SCLR_driver : std_logic;
SIGNAL PC_a10_a_SLOAD_driver : std_logic;
SIGNAL PC_a10_a_ENA_driver : std_logic;
SIGNAL left_a10_a_DATAA_driver : std_logic;
SIGNAL left_a10_a_DATAB_driver : std_logic;
SIGNAL left_a10_a_DATAC_driver : std_logic;
SIGNAL left_a10_a_DATAD_driver : std_logic;
SIGNAL Add0_a22_DATAA_driver : std_logic;
SIGNAL Add0_a22_DATAB_driver : std_logic;
SIGNAL Add0_a22_CIN_driver : std_logic;
SIGNAL HI_a12_DATAA_driver : std_logic;
SIGNAL HI_a12_DATAB_driver : std_logic;
SIGNAL HI_a12_DATAC_driver : std_logic;
SIGNAL HI_a12_DATAD_driver : std_logic;
SIGNAL HI_a10_a_CLK_driver : std_logic;
SIGNAL HI_a10_a_D_driver : std_logic;
SIGNAL HI_a10_a_ENA_driver : std_logic;
SIGNAL data_in_a11_a_ainput_I_driver : std_logic;
SIGNAL A_a12_DATAC_driver : std_logic;
SIGNAL A_a12_DATAD_driver : std_logic;
SIGNAL B_a11_a_CLK_driver : std_logic;
SIGNAL B_a11_a_ASDATA_driver : std_logic;
SIGNAL B_a11_a_ENA_driver : std_logic;
SIGNAL op2_real_a146_DATAA_driver : std_logic;
SIGNAL op2_real_a146_DATAB_driver : std_logic;
SIGNAL op2_real_a146_DATAC_driver : std_logic;
SIGNAL op2_real_a146_DATAD_driver : std_logic;
SIGNAL op2_real_a147_DATAA_driver : std_logic;
SIGNAL op2_real_a147_DATAB_driver : std_logic;
SIGNAL op2_real_a147_DATAD_driver : std_logic;
SIGNAL Add0_a24_DATAA_driver : std_logic;
SIGNAL Add0_a24_DATAB_driver : std_logic;
SIGNAL Add0_a24_CIN_driver : std_logic;
SIGNAL HI_a13_DATAA_driver : std_logic;
SIGNAL HI_a13_DATAB_driver : std_logic;
SIGNAL HI_a13_DATAC_driver : std_logic;
SIGNAL HI_a13_DATAD_driver : std_logic;
SIGNAL HI_a11_a_CLK_driver : std_logic;
SIGNAL HI_a11_a_D_driver : std_logic;
SIGNAL HI_a11_a_ENA_driver : std_logic;
SIGNAL left_a12_a_a19_DATAA_driver : std_logic;
SIGNAL left_a12_a_a19_DATAB_driver : std_logic;
SIGNAL left_a12_a_a19_DATAC_driver : std_logic;
SIGNAL left_a12_a_a19_DATAD_driver : std_logic;
SIGNAL PC_a13_DATAA_driver : std_logic;
SIGNAL PC_a13_DATAB_driver : std_logic;
SIGNAL PC_a13_DATAD_driver : std_logic;
SIGNAL PC_a12_a_CLK_driver : std_logic;
SIGNAL PC_a12_a_D_driver : std_logic;
SIGNAL PC_a12_a_ASDATA_driver : std_logic;
SIGNAL PC_a12_a_SCLR_driver : std_logic;
SIGNAL PC_a12_a_SLOAD_driver : std_logic;
SIGNAL PC_a12_a_ENA_driver : std_logic;
SIGNAL left_a12_a_DATAA_driver : std_logic;
SIGNAL left_a12_a_DATAB_driver : std_logic;
SIGNAL left_a12_a_DATAC_driver : std_logic;
SIGNAL left_a12_a_DATAD_driver : std_logic;
SIGNAL Add0_a26_DATAA_driver : std_logic;
SIGNAL Add0_a26_DATAB_driver : std_logic;
SIGNAL Add0_a26_CIN_driver : std_logic;
SIGNAL HI_a14_DATAA_driver : std_logic;
SIGNAL HI_a14_DATAB_driver : std_logic;
SIGNAL HI_a14_DATAC_driver : std_logic;
SIGNAL HI_a14_DATAD_driver : std_logic;
SIGNAL HI_a12_a_CLK_driver : std_logic;
SIGNAL HI_a12_a_D_driver : std_logic;
SIGNAL HI_a12_a_ENA_driver : std_logic;
SIGNAL data_in_a13_a_ainput_I_driver : std_logic;
SIGNAL A_a14_DATAA_driver : std_logic;
SIGNAL A_a14_DATAC_driver : std_logic;
SIGNAL A_a13_a_CLK_driver : std_logic;
SIGNAL A_a13_a_ASDATA_driver : std_logic;
SIGNAL A_a13_a_ENA_driver : std_logic;
SIGNAL left_a13_a_a18_DATAA_driver : std_logic;
SIGNAL left_a13_a_a18_DATAB_driver : std_logic;
SIGNAL left_a13_a_a18_DATAC_driver : std_logic;
SIGNAL left_a13_a_a18_DATAD_driver : std_logic;
SIGNAL PC_a14_DATAA_driver : std_logic;
SIGNAL PC_a14_DATAB_driver : std_logic;
SIGNAL PC_a14_DATAD_driver : std_logic;
SIGNAL PC_a13_a_CLK_driver : std_logic;
SIGNAL PC_a13_a_D_driver : std_logic;
SIGNAL PC_a13_a_ASDATA_driver : std_logic;
SIGNAL PC_a13_a_SCLR_driver : std_logic;
SIGNAL PC_a13_a_SLOAD_driver : std_logic;
SIGNAL PC_a13_a_ENA_driver : std_logic;
SIGNAL left_a13_a_DATAA_driver : std_logic;
SIGNAL left_a13_a_DATAB_driver : std_logic;
SIGNAL left_a13_a_DATAC_driver : std_logic;
SIGNAL left_a13_a_DATAD_driver : std_logic;
SIGNAL Add0_a28_DATAA_driver : std_logic;
SIGNAL Add0_a28_DATAB_driver : std_logic;
SIGNAL Add0_a28_CIN_driver : std_logic;
SIGNAL HI_a15_DATAA_driver : std_logic;
SIGNAL HI_a15_DATAB_driver : std_logic;
SIGNAL HI_a15_DATAC_driver : std_logic;
SIGNAL HI_a15_DATAD_driver : std_logic;
SIGNAL HI_a13_a_CLK_driver : std_logic;
SIGNAL HI_a13_a_D_driver : std_logic;
SIGNAL HI_a13_a_ENA_driver : std_logic;
SIGNAL Add0_a30_DATAA_driver : std_logic;
SIGNAL Add0_a30_DATAB_driver : std_logic;
SIGNAL Add0_a30_CIN_driver : std_logic;
SIGNAL HI_a16_DATAA_driver : std_logic;
SIGNAL HI_a16_DATAB_driver : std_logic;
SIGNAL HI_a16_DATAC_driver : std_logic;
SIGNAL HI_a16_DATAD_driver : std_logic;
SIGNAL HI_a14_a_CLK_driver : std_logic;
SIGNAL HI_a14_a_D_driver : std_logic;
SIGNAL HI_a14_a_ENA_driver : std_logic;
SIGNAL intr_pc_a12_a_ainput_I_driver : std_logic;
SIGNAL PC_a15_DATAA_driver : std_logic;
SIGNAL PC_a15_DATAB_driver : std_logic;
SIGNAL PC_a15_DATAD_driver : std_logic;
SIGNAL data_in_a14_a_ainput_I_driver : std_logic;
SIGNAL PC_a14_a_CLK_driver : std_logic;
SIGNAL PC_a14_a_D_driver : std_logic;
SIGNAL PC_a14_a_ASDATA_driver : std_logic;
SIGNAL PC_a14_a_SCLR_driver : std_logic;
SIGNAL PC_a14_a_SLOAD_driver : std_logic;
SIGNAL PC_a14_a_ENA_driver : std_logic;
SIGNAL left_a14_a_DATAA_driver : std_logic;
SIGNAL left_a14_a_DATAB_driver : std_logic;
SIGNAL left_a14_a_DATAC_driver : std_logic;
SIGNAL left_a14_a_DATAD_driver : std_logic;
SIGNAL Add0_a32_DATAA_driver : std_logic;
SIGNAL Add0_a32_DATAB_driver : std_logic;
SIGNAL Add0_a32_CIN_driver : std_logic;
SIGNAL HI_a17_DATAA_driver : std_logic;
SIGNAL HI_a17_DATAB_driver : std_logic;
SIGNAL HI_a17_DATAC_driver : std_logic;
SIGNAL HI_a17_DATAD_driver : std_logic;
SIGNAL HI_a15_a_CLK_driver : std_logic;
SIGNAL HI_a15_a_D_driver : std_logic;
SIGNAL HI_a15_a_ENA_driver : std_logic;
SIGNAL left_a16_a_a15_DATAA_driver : std_logic;
SIGNAL left_a16_a_a15_DATAB_driver : std_logic;
SIGNAL left_a16_a_a15_DATAC_driver : std_logic;
SIGNAL left_a16_a_a15_DATAD_driver : std_logic;
SIGNAL HI_a18_DATAA_driver : std_logic;
SIGNAL HI_a18_DATAB_driver : std_logic;
SIGNAL HI_a18_DATAC_driver : std_logic;
SIGNAL HI_a18_DATAD_driver : std_logic;
SIGNAL HI_a16_a_CLK_driver : std_logic;
SIGNAL HI_a16_a_D_driver : std_logic;
SIGNAL HI_a16_a_ENA_driver : std_logic;
SIGNAL intr_pc_a14_a_ainput_I_driver : std_logic;
SIGNAL Add0_a34_DATAA_driver : std_logic;
SIGNAL Add0_a34_DATAB_driver : std_logic;
SIGNAL Add0_a34_CIN_driver : std_logic;
SIGNAL PC_a17_DATAA_driver : std_logic;
SIGNAL PC_a17_DATAB_driver : std_logic;
SIGNAL PC_a17_DATAD_driver : std_logic;
SIGNAL data_in_a16_a_ainput_I_driver : std_logic;
SIGNAL PC_a16_a_CLK_driver : std_logic;
SIGNAL PC_a16_a_D_driver : std_logic;
SIGNAL PC_a16_a_ASDATA_driver : std_logic;
SIGNAL PC_a16_a_SCLR_driver : std_logic;
SIGNAL PC_a16_a_SLOAD_driver : std_logic;
SIGNAL PC_a16_a_ENA_driver : std_logic;
SIGNAL left_a16_a_DATAA_driver : std_logic;
SIGNAL left_a16_a_DATAB_driver : std_logic;
SIGNAL left_a16_a_DATAC_driver : std_logic;
SIGNAL left_a16_a_DATAD_driver : std_logic;
SIGNAL Add0_a36_DATAA_driver : std_logic;
SIGNAL Add0_a36_DATAB_driver : std_logic;
SIGNAL Add0_a36_CIN_driver : std_logic;
SIGNAL PC_a18_DATAA_driver : std_logic;
SIGNAL PC_a18_DATAB_driver : std_logic;
SIGNAL PC_a18_DATAD_driver : std_logic;
SIGNAL data_in_a17_a_ainput_I_driver : std_logic;
SIGNAL PC_a17_a_CLK_driver : std_logic;
SIGNAL PC_a17_a_D_driver : std_logic;
SIGNAL PC_a17_a_ASDATA_driver : std_logic;
SIGNAL PC_a17_a_SCLR_driver : std_logic;
SIGNAL PC_a17_a_SLOAD_driver : std_logic;
SIGNAL PC_a17_a_ENA_driver : std_logic;
SIGNAL A_a18_DATAB_driver : std_logic;
SIGNAL A_a18_DATAC_driver : std_logic;
SIGNAL A_a17_a_CLK_driver : std_logic;
SIGNAL A_a17_a_ASDATA_driver : std_logic;
SIGNAL A_a17_a_ENA_driver : std_logic;
SIGNAL left_a17_a_a14_DATAA_driver : std_logic;
SIGNAL left_a17_a_a14_DATAB_driver : std_logic;
SIGNAL left_a17_a_a14_DATAC_driver : std_logic;
SIGNAL left_a17_a_a14_DATAD_driver : std_logic;
SIGNAL HI_a19_DATAA_driver : std_logic;
SIGNAL HI_a19_DATAB_driver : std_logic;
SIGNAL HI_a19_DATAC_driver : std_logic;
SIGNAL HI_a19_DATAD_driver : std_logic;
SIGNAL HI_a17_a_CLK_driver : std_logic;
SIGNAL HI_a17_a_D_driver : std_logic;
SIGNAL HI_a17_a_ENA_driver : std_logic;
SIGNAL left_a17_a_DATAA_driver : std_logic;
SIGNAL left_a17_a_DATAB_driver : std_logic;
SIGNAL left_a17_a_DATAC_driver : std_logic;
SIGNAL left_a17_a_DATAD_driver : std_logic;
SIGNAL Add0_a38_DATAA_driver : std_logic;
SIGNAL Add0_a38_DATAB_driver : std_logic;
SIGNAL Add0_a38_CIN_driver : std_logic;
SIGNAL Add0_a40_DATAA_driver : std_logic;
SIGNAL Add0_a40_DATAB_driver : std_logic;
SIGNAL Add0_a40_CIN_driver : std_logic;
SIGNAL Add0_a42_DATAA_driver : std_logic;
SIGNAL Add0_a42_DATAB_driver : std_logic;
SIGNAL Add0_a42_CIN_driver : std_logic;
SIGNAL Add0_a44_DATAA_driver : std_logic;
SIGNAL Add0_a44_DATAB_driver : std_logic;
SIGNAL Add0_a44_CIN_driver : std_logic;
SIGNAL Add0_a46_DATAA_driver : std_logic;
SIGNAL Add0_a46_DATAB_driver : std_logic;
SIGNAL Add0_a46_CIN_driver : std_logic;
SIGNAL HI_a23_DATAA_driver : std_logic;
SIGNAL HI_a23_DATAB_driver : std_logic;
SIGNAL HI_a23_DATAC_driver : std_logic;
SIGNAL HI_a23_DATAD_driver : std_logic;
SIGNAL HI_a21_a_CLK_driver : std_logic;
SIGNAL HI_a21_a_D_driver : std_logic;
SIGNAL HI_a21_a_ENA_driver : std_logic;
SIGNAL HI_a24_DATAA_driver : std_logic;
SIGNAL HI_a24_DATAB_driver : std_logic;
SIGNAL HI_a24_DATAC_driver : std_logic;
SIGNAL HI_a24_DATAD_driver : std_logic;
SIGNAL HI_a22_a_CLK_driver : std_logic;
SIGNAL HI_a22_a_D_driver : std_logic;
SIGNAL HI_a22_a_ENA_driver : std_logic;
SIGNAL HI_a25_DATAA_driver : std_logic;
SIGNAL HI_a25_DATAB_driver : std_logic;
SIGNAL HI_a25_DATAC_driver : std_logic;
SIGNAL HI_a25_DATAD_driver : std_logic;
SIGNAL HI_a23_a_CLK_driver : std_logic;
SIGNAL HI_a23_a_D_driver : std_logic;
SIGNAL HI_a23_a_ENA_driver : std_logic;
SIGNAL HI_a26_DATAA_driver : std_logic;
SIGNAL HI_a26_DATAB_driver : std_logic;
SIGNAL HI_a26_DATAC_driver : std_logic;
SIGNAL HI_a26_DATAD_driver : std_logic;
SIGNAL HI_a24_a_CLK_driver : std_logic;
SIGNAL HI_a24_a_D_driver : std_logic;
SIGNAL HI_a24_a_ENA_driver : std_logic;
SIGNAL left_a25_a_a6_DATAA_driver : std_logic;
SIGNAL left_a25_a_a6_DATAB_driver : std_logic;
SIGNAL left_a25_a_a6_DATAC_driver : std_logic;
SIGNAL left_a25_a_a6_DATAD_driver : std_logic;
SIGNAL HI_a27_DATAA_driver : std_logic;
SIGNAL HI_a27_DATAB_driver : std_logic;
SIGNAL HI_a27_DATAC_driver : std_logic;
SIGNAL HI_a27_DATAD_driver : std_logic;
SIGNAL HI_a25_a_CLK_driver : std_logic;
SIGNAL HI_a25_a_D_driver : std_logic;
SIGNAL HI_a25_a_ENA_driver : std_logic;
SIGNAL left_a25_a_DATAA_driver : std_logic;
SIGNAL left_a25_a_DATAB_driver : std_logic;
SIGNAL left_a25_a_DATAC_driver : std_logic;
SIGNAL left_a25_a_DATAD_driver : std_logic;
SIGNAL data_in_a24_a_ainput_I_driver : std_logic;
SIGNAL A_a25_DATAC_driver : std_logic;
SIGNAL A_a25_DATAD_driver : std_logic;
SIGNAL A_a24_a_CLK_driver : std_logic;
SIGNAL A_a24_a_ASDATA_driver : std_logic;
SIGNAL A_a24_a_ENA_driver : std_logic;
SIGNAL left_a24_a_a7_DATAA_driver : std_logic;
SIGNAL left_a24_a_a7_DATAB_driver : std_logic;
SIGNAL left_a24_a_a7_DATAC_driver : std_logic;
SIGNAL left_a24_a_a7_DATAD_driver : std_logic;
SIGNAL left_a24_a_DATAA_driver : std_logic;
SIGNAL left_a24_a_DATAB_driver : std_logic;
SIGNAL left_a24_a_DATAC_driver : std_logic;
SIGNAL left_a24_a_DATAD_driver : std_logic;
SIGNAL intr_right_a23_a_ainput_I_driver : std_logic;
SIGNAL data_in_a23_a_ainput_I_driver : std_logic;
SIGNAL A_a24_DATAB_driver : std_logic;
SIGNAL A_a24_DATAC_driver : std_logic;
SIGNAL B_a23_a_CLK_driver : std_logic;
SIGNAL B_a23_a_ASDATA_driver : std_logic;
SIGNAL B_a23_a_ENA_driver : std_logic;
SIGNAL op2_real_a170_DATAA_driver : std_logic;
SIGNAL op2_real_a170_DATAB_driver : std_logic;
SIGNAL op2_real_a170_DATAC_driver : std_logic;
SIGNAL op2_real_a170_DATAD_driver : std_logic;
SIGNAL op2_real_a171_DATAB_driver : std_logic;
SIGNAL op2_real_a171_DATAC_driver : std_logic;
SIGNAL op2_real_a171_DATAD_driver : std_logic;
SIGNAL Add0_a48_DATAA_driver : std_logic;
SIGNAL Add0_a48_DATAB_driver : std_logic;
SIGNAL Add0_a48_CIN_driver : std_logic;
SIGNAL Add0_a50_DATAA_driver : std_logic;
SIGNAL Add0_a50_DATAB_driver : std_logic;
SIGNAL Add0_a50_CIN_driver : std_logic;
SIGNAL Add0_a52_DATAA_driver : std_logic;
SIGNAL Add0_a52_DATAB_driver : std_logic;
SIGNAL Add0_a52_CIN_driver : std_logic;
SIGNAL Add0_a54_DATAA_driver : std_logic;
SIGNAL Add0_a54_DATAB_driver : std_logic;
SIGNAL Add0_a54_CIN_driver : std_logic;
SIGNAL HI_a28_DATAA_driver : std_logic;
SIGNAL HI_a28_DATAB_driver : std_logic;
SIGNAL HI_a28_DATAC_driver : std_logic;
SIGNAL HI_a28_DATAD_driver : std_logic;
SIGNAL HI_a26_a_CLK_driver : std_logic;
SIGNAL HI_a26_a_D_driver : std_logic;
SIGNAL HI_a26_a_ENA_driver : std_logic;
SIGNAL left_a27_a_a4_DATAA_driver : std_logic;
SIGNAL left_a27_a_a4_DATAB_driver : std_logic;
SIGNAL left_a27_a_a4_DATAC_driver : std_logic;
SIGNAL left_a27_a_a4_DATAD_driver : std_logic;
SIGNAL Add0_a56_DATAA_driver : std_logic;
SIGNAL Add0_a56_DATAB_driver : std_logic;
SIGNAL Add0_a56_CIN_driver : std_logic;
SIGNAL HI_a29_DATAA_driver : std_logic;
SIGNAL HI_a29_DATAB_driver : std_logic;
SIGNAL HI_a29_DATAC_driver : std_logic;
SIGNAL HI_a29_DATAD_driver : std_logic;
SIGNAL HI_a27_a_CLK_driver : std_logic;
SIGNAL HI_a27_a_D_driver : std_logic;
SIGNAL HI_a27_a_ENA_driver : std_logic;
SIGNAL left_a27_a_DATAA_driver : std_logic;
SIGNAL left_a27_a_DATAB_driver : std_logic;
SIGNAL left_a27_a_DATAC_driver : std_logic;
SIGNAL left_a27_a_DATAD_driver : std_logic;
SIGNAL Add0_a58_DATAA_driver : std_logic;
SIGNAL Add0_a58_DATAB_driver : std_logic;
SIGNAL Add0_a58_CIN_driver : std_logic;
SIGNAL PC_a29_DATAB_driver : std_logic;
SIGNAL PC_a29_DATAC_driver : std_logic;
SIGNAL PC_a29_DATAD_driver : std_logic;
SIGNAL PC_a29_a_a30_DATAA_driver : std_logic;
SIGNAL PC_a29_a_a30_DATAB_driver : std_logic;
SIGNAL PC_a29_a_a30_DATAC_driver : std_logic;
SIGNAL PC_a28_a_CLK_driver : std_logic;
SIGNAL PC_a28_a_D_driver : std_logic;
SIGNAL PC_a28_a_SCLR_driver : std_logic;
SIGNAL PC_a28_a_ENA_driver : std_logic;
SIGNAL HI_a30_DATAA_driver : std_logic;
SIGNAL HI_a30_DATAB_driver : std_logic;
SIGNAL HI_a30_DATAC_driver : std_logic;
SIGNAL HI_a30_DATAD_driver : std_logic;
SIGNAL HI_a28_a_CLK_driver : std_logic;
SIGNAL HI_a28_a_D_driver : std_logic;
SIGNAL HI_a28_a_ENA_driver : std_logic;
SIGNAL left_a28_a_DATAA_driver : std_logic;
SIGNAL left_a28_a_DATAB_driver : std_logic;
SIGNAL left_a28_a_DATAC_driver : std_logic;
SIGNAL left_a28_a_DATAD_driver : std_logic;
SIGNAL Add0_a60_DATAA_driver : std_logic;
SIGNAL Add0_a60_DATAB_driver : std_logic;
SIGNAL Add0_a60_CIN_driver : std_logic;
SIGNAL Add0_a62_DATAA_driver : std_logic;
SIGNAL Add0_a62_DATAB_driver : std_logic;
SIGNAL Add0_a62_CIN_driver : std_logic;
SIGNAL HI_a32_DATAA_driver : std_logic;
SIGNAL HI_a32_DATAB_driver : std_logic;
SIGNAL HI_a32_DATAC_driver : std_logic;
SIGNAL HI_a32_DATAD_driver : std_logic;
SIGNAL HI_a30_a_CLK_driver : std_logic;
SIGNAL HI_a30_a_D_driver : std_logic;
SIGNAL HI_a30_a_ENA_driver : std_logic;
SIGNAL PC_a32_DATAA_driver : std_logic;
SIGNAL PC_a32_DATAC_driver : std_logic;
SIGNAL PC_a32_DATAD_driver : std_logic;
SIGNAL PC_a30_a_CLK_driver : std_logic;
SIGNAL PC_a30_a_D_driver : std_logic;
SIGNAL PC_a30_a_SCLR_driver : std_logic;
SIGNAL PC_a30_a_ENA_driver : std_logic;
SIGNAL left_a30_a_DATAA_driver : std_logic;
SIGNAL left_a30_a_DATAB_driver : std_logic;
SIGNAL left_a30_a_DATAC_driver : std_logic;
SIGNAL left_a30_a_DATAD_driver : std_logic;
SIGNAL Add0_a64_DATAA_driver : std_logic;
SIGNAL Add0_a64_DATAB_driver : std_logic;
SIGNAL Add0_a64_CIN_driver : std_logic;
SIGNAL HI_a33_DATAA_driver : std_logic;
SIGNAL HI_a33_DATAB_driver : std_logic;
SIGNAL HI_a33_DATAC_driver : std_logic;
SIGNAL HI_a33_DATAD_driver : std_logic;
SIGNAL HI_a31_a_CLK_driver : std_logic;
SIGNAL HI_a31_a_D_driver : std_logic;
SIGNAL HI_a31_a_ENA_driver : std_logic;
SIGNAL A_a32_DATAB_driver : std_logic;
SIGNAL A_a32_DATAC_driver : std_logic;
SIGNAL A_a31_a_CLK_driver : std_logic;
SIGNAL A_a31_a_ASDATA_driver : std_logic;
SIGNAL A_a31_a_ENA_driver : std_logic;
SIGNAL left_a31_a_a0_DATAA_driver : std_logic;
SIGNAL left_a31_a_a0_DATAB_driver : std_logic;
SIGNAL left_a31_a_a0_DATAC_driver : std_logic;
SIGNAL left_a31_a_a0_DATAD_driver : std_logic;
SIGNAL left_a31_a_DATAA_driver : std_logic;
SIGNAL left_a31_a_DATAB_driver : std_logic;
SIGNAL left_a31_a_DATAC_driver : std_logic;
SIGNAL left_a31_a_DATAD_driver : std_logic;
SIGNAL Add0_a66_DATAD_driver : std_logic;
SIGNAL Add0_a66_CIN_driver : std_logic;
SIGNAL LO_a0_DATAB_driver : std_logic;
SIGNAL LO_a0_DATAC_driver : std_logic;
SIGNAL LO_a0_DATAD_driver : std_logic;
SIGNAL LO_a0_a_CLK_driver : std_logic;
SIGNAL LO_a0_a_D_driver : std_logic;
SIGNAL LO_a0_a_SCLR_driver : std_logic;
SIGNAL LO_a0_a_ENA_driver : std_logic;
SIGNAL data_out_a2_DATAA_driver : std_logic;
SIGNAL data_out_a2_DATAB_driver : std_logic;
SIGNAL data_out_a2_DATAC_driver : std_logic;
SIGNAL data_out_a2_DATAD_driver : std_logic;
SIGNAL data_out_a3_DATAA_driver : std_logic;
SIGNAL data_out_a3_DATAB_driver : std_logic;
SIGNAL data_out_a3_DATAC_driver : std_logic;
SIGNAL data_out_a3_DATAD_driver : std_logic;
SIGNAL LO_a2_DATAB_driver : std_logic;
SIGNAL LO_a2_DATAC_driver : std_logic;
SIGNAL LO_a2_DATAD_driver : std_logic;
SIGNAL LO_a1_a_CLK_driver : std_logic;
SIGNAL LO_a1_a_D_driver : std_logic;
SIGNAL LO_a1_a_SCLR_driver : std_logic;
SIGNAL LO_a1_a_ENA_driver : std_logic;
SIGNAL data_out_a5_DATAA_driver : std_logic;
SIGNAL data_out_a5_DATAB_driver : std_logic;
SIGNAL data_out_a5_DATAC_driver : std_logic;
SIGNAL data_out_a5_DATAD_driver : std_logic;
SIGNAL PC_a2_DATAB_driver : std_logic;
SIGNAL PC_a2_DATAD_driver : std_logic;
SIGNAL PC_a1_a_CLK_driver : std_logic;
SIGNAL PC_a1_a_D_driver : std_logic;
SIGNAL PC_a1_a_ASDATA_driver : std_logic;
SIGNAL PC_a1_a_SCLR_driver : std_logic;
SIGNAL PC_a1_a_SLOAD_driver : std_logic;
SIGNAL PC_a1_a_ENA_driver : std_logic;
SIGNAL data_out_a6_DATAA_driver : std_logic;
SIGNAL data_out_a6_DATAB_driver : std_logic;
SIGNAL data_out_a6_DATAC_driver : std_logic;
SIGNAL data_out_a6_DATAD_driver : std_logic;
SIGNAL intr_pc_a0_a_ainput_I_driver : std_logic;
SIGNAL PC_a3_DATAA_driver : std_logic;
SIGNAL PC_a3_DATAB_driver : std_logic;
SIGNAL PC_a3_DATAD_driver : std_logic;
SIGNAL PC_a2_a_CLK_driver : std_logic;
SIGNAL PC_a2_a_D_driver : std_logic;
SIGNAL PC_a2_a_ASDATA_driver : std_logic;
SIGNAL PC_a2_a_SCLR_driver : std_logic;
SIGNAL PC_a2_a_SLOAD_driver : std_logic;
SIGNAL PC_a2_a_ENA_driver : std_logic;
SIGNAL HI_a4_DATAA_driver : std_logic;
SIGNAL HI_a4_DATAB_driver : std_logic;
SIGNAL HI_a4_DATAC_driver : std_logic;
SIGNAL HI_a4_DATAD_driver : std_logic;
SIGNAL HI_a2_a_CLK_driver : std_logic;
SIGNAL HI_a2_a_D_driver : std_logic;
SIGNAL HI_a2_a_ENA_driver : std_logic;
SIGNAL LO_a3_DATAA_driver : std_logic;
SIGNAL LO_a3_DATAC_driver : std_logic;
SIGNAL LO_a3_DATAD_driver : std_logic;
SIGNAL LO_a2_a_CLK_driver : std_logic;
SIGNAL LO_a2_a_D_driver : std_logic;
SIGNAL LO_a2_a_SCLR_driver : std_logic;
SIGNAL LO_a2_a_ENA_driver : std_logic;
SIGNAL A_a2_a_CLK_driver : std_logic;
SIGNAL A_a2_a_ASDATA_driver : std_logic;
SIGNAL A_a2_a_ENA_driver : std_logic;
SIGNAL mux_out_a0_a_ainput_I_driver : std_logic;
SIGNAL O_a2_a_a1_DATAA_driver : std_logic;
SIGNAL O_a2_a_a1_DATAB_driver : std_logic;
SIGNAL O_a2_a_a1_DATAD_driver : std_logic;
SIGNAL data_in_a3_a_ainput_I_driver : std_logic;
SIGNAL O_a3_a_a2_DATAA_driver : std_logic;
SIGNAL O_a3_a_a2_DATAB_driver : std_logic;
SIGNAL O_a3_a_a2_DATAD_driver : std_logic;
SIGNAL data_in_a4_a_ainput_I_driver : std_logic;
SIGNAL O_a4_a_a3_DATAA_driver : std_logic;
SIGNAL O_a4_a_a3_DATAB_driver : std_logic;
SIGNAL O_a4_a_a3_DATAD_driver : std_logic;
SIGNAL data_in_a5_a_ainput_I_driver : std_logic;
SIGNAL O_a5_a_a4_DATAA_driver : std_logic;
SIGNAL O_a5_a_a4_DATAB_driver : std_logic;
SIGNAL O_a5_a_a4_DATAD_driver : std_logic;
SIGNAL data_in_a6_a_ainput_I_driver : std_logic;
SIGNAL O_a6_a_a5_DATAA_driver : std_logic;
SIGNAL O_a6_a_a5_DATAB_driver : std_logic;
SIGNAL O_a6_a_a5_DATAD_driver : std_logic;
SIGNAL O_a7_a_a6_DATAA_driver : std_logic;
SIGNAL O_a7_a_a6_DATAB_driver : std_logic;
SIGNAL O_a7_a_a6_DATAD_driver : std_logic;
SIGNAL O_a8_a_a7_DATAA_driver : std_logic;
SIGNAL O_a8_a_a7_DATAB_driver : std_logic;
SIGNAL O_a8_a_a7_DATAD_driver : std_logic;
SIGNAL O_a9_a_a8_DATAA_driver : std_logic;
SIGNAL O_a9_a_a8_DATAB_driver : std_logic;
SIGNAL O_a9_a_a8_DATAD_driver : std_logic;
SIGNAL O_a10_a_a9_DATAA_driver : std_logic;
SIGNAL O_a10_a_a9_DATAB_driver : std_logic;
SIGNAL O_a10_a_a9_DATAD_driver : std_logic;
SIGNAL O_a11_a_a10_DATAA_driver : std_logic;
SIGNAL O_a11_a_a10_DATAB_driver : std_logic;
SIGNAL O_a11_a_a10_DATAD_driver : std_logic;
SIGNAL O_a12_a_a11_DATAA_driver : std_logic;
SIGNAL O_a12_a_a11_DATAB_driver : std_logic;
SIGNAL O_a12_a_a11_DATAD_driver : std_logic;
SIGNAL O_a13_a_a12_DATAA_driver : std_logic;
SIGNAL O_a13_a_a12_DATAB_driver : std_logic;
SIGNAL O_a13_a_a12_DATAD_driver : std_logic;
SIGNAL O_a14_a_a13_DATAA_driver : std_logic;
SIGNAL O_a14_a_a13_DATAB_driver : std_logic;
SIGNAL O_a14_a_a13_DATAD_driver : std_logic;
SIGNAL O_a15_a_a14_DATAA_driver : std_logic;
SIGNAL O_a15_a_a14_DATAB_driver : std_logic;
SIGNAL O_a15_a_a14_DATAD_driver : std_logic;
SIGNAL O_a16_a_a15_DATAA_driver : std_logic;
SIGNAL O_a16_a_a15_DATAB_driver : std_logic;
SIGNAL O_a16_a_a15_DATAD_driver : std_logic;
SIGNAL O_a17_a_a16_DATAA_driver : std_logic;
SIGNAL O_a17_a_a16_DATAB_driver : std_logic;
SIGNAL O_a17_a_a16_DATAD_driver : std_logic;
SIGNAL O_a18_a_a17_DATAA_driver : std_logic;
SIGNAL O_a18_a_a17_DATAB_driver : std_logic;
SIGNAL O_a18_a_a17_DATAD_driver : std_logic;
SIGNAL O_a19_a_a18_DATAA_driver : std_logic;
SIGNAL O_a19_a_a18_DATAB_driver : std_logic;
SIGNAL O_a19_a_a18_DATAD_driver : std_logic;
SIGNAL O_a20_a_a19_DATAA_driver : std_logic;
SIGNAL O_a20_a_a19_DATAB_driver : std_logic;
SIGNAL O_a20_a_a19_DATAD_driver : std_logic;
SIGNAL O_a21_a_a20_DATAA_driver : std_logic;
SIGNAL O_a21_a_a20_DATAB_driver : std_logic;
SIGNAL O_a21_a_a20_DATAD_driver : std_logic;
SIGNAL O_a22_a_a21_DATAA_driver : std_logic;
SIGNAL O_a22_a_a21_DATAB_driver : std_logic;
SIGNAL O_a22_a_a21_DATAD_driver : std_logic;
SIGNAL O_a23_a_a22_DATAA_driver : std_logic;
SIGNAL O_a23_a_a22_DATAB_driver : std_logic;
SIGNAL O_a23_a_a22_DATAD_driver : std_logic;
SIGNAL O_a24_a_a23_DATAA_driver : std_logic;
SIGNAL O_a24_a_a23_DATAB_driver : std_logic;
SIGNAL O_a24_a_a23_DATAD_driver : std_logic;
SIGNAL O_a25_a_a24_DATAA_driver : std_logic;
SIGNAL O_a25_a_a24_DATAB_driver : std_logic;
SIGNAL O_a25_a_a24_DATAD_driver : std_logic;
SIGNAL O_a26_a_a25_DATAA_driver : std_logic;
SIGNAL O_a26_a_a25_DATAB_driver : std_logic;
SIGNAL O_a26_a_a25_DATAD_driver : std_logic;
SIGNAL O_a27_a_a26_DATAA_driver : std_logic;
SIGNAL O_a27_a_a26_DATAB_driver : std_logic;
SIGNAL O_a27_a_a26_DATAD_driver : std_logic;
SIGNAL O_a28_a_a27_DATAA_driver : std_logic;
SIGNAL O_a28_a_a27_DATAB_driver : std_logic;
SIGNAL O_a28_a_a27_DATAD_driver : std_logic;
SIGNAL O_a29_a_a28_DATAA_driver : std_logic;
SIGNAL O_a29_a_a28_DATAB_driver : std_logic;
SIGNAL O_a29_a_a28_DATAD_driver : std_logic;
SIGNAL data_in_a30_a_ainput_I_driver : std_logic;
SIGNAL O_a30_a_a29_DATAA_driver : std_logic;
SIGNAL O_a30_a_a29_DATAB_driver : std_logic;
SIGNAL O_a30_a_a29_DATAD_driver : std_logic;
SIGNAL mux_out_a1_a_ainput_I_driver : std_logic;
SIGNAL O_n_a31_a_a4_DATAA_driver : std_logic;
SIGNAL O_n_a31_a_a4_DATAC_driver : std_logic;
SIGNAL O_n_a31_a_a4_DATAD_driver : std_logic;
SIGNAL O_n_a31_a_a6_DATAA_driver : std_logic;
SIGNAL O_n_a31_a_a6_DATAB_driver : std_logic;
SIGNAL O_n_a31_a_a6_DATAC_driver : std_logic;
SIGNAL O_n_a31_a_a6_DATAD_driver : std_logic;
SIGNAL O_a31_a_CLK_driver : std_logic;
SIGNAL O_a31_a_D_driver : std_logic;
SIGNAL O_a31_a_SCLR_driver : std_logic;
SIGNAL O_a27_a_a30_DATAA_driver : std_logic;
SIGNAL O_a27_a_a30_DATAC_driver : std_logic;
SIGNAL O_a27_a_a30_DATAD_driver : std_logic;
SIGNAL mux_out_a2_a_ainput_I_driver : std_logic;
SIGNAL O_a27_a_a31_DATAA_driver : std_logic;
SIGNAL O_a27_a_a31_DATAB_driver : std_logic;
SIGNAL O_a27_a_a31_DATAC_driver : std_logic;
SIGNAL O_a27_a_a31_DATAD_driver : std_logic;
SIGNAL O_a30_a_CLK_driver : std_logic;
SIGNAL O_a30_a_D_driver : std_logic;
SIGNAL O_a30_a_ASDATA_driver : std_logic;
SIGNAL O_a30_a_SCLR_driver : std_logic;
SIGNAL O_a30_a_SLOAD_driver : std_logic;
SIGNAL O_a30_a_ENA_driver : std_logic;
SIGNAL O_a29_a_CLK_driver : std_logic;
SIGNAL O_a29_a_D_driver : std_logic;
SIGNAL O_a29_a_ASDATA_driver : std_logic;
SIGNAL O_a29_a_SCLR_driver : std_logic;
SIGNAL O_a29_a_SLOAD_driver : std_logic;
SIGNAL O_a29_a_ENA_driver : std_logic;
SIGNAL O_a28_a_CLK_driver : std_logic;
SIGNAL O_a28_a_D_driver : std_logic;
SIGNAL O_a28_a_ASDATA_driver : std_logic;
SIGNAL O_a28_a_SCLR_driver : std_logic;
SIGNAL O_a28_a_SLOAD_driver : std_logic;
SIGNAL O_a28_a_ENA_driver : std_logic;
SIGNAL O_a27_a_CLK_driver : std_logic;
SIGNAL O_a27_a_D_driver : std_logic;
SIGNAL O_a27_a_ASDATA_driver : std_logic;
SIGNAL O_a27_a_SCLR_driver : std_logic;
SIGNAL O_a27_a_SLOAD_driver : std_logic;
SIGNAL O_a27_a_ENA_driver : std_logic;
SIGNAL O_a26_a_CLK_driver : std_logic;
SIGNAL O_a26_a_D_driver : std_logic;
SIGNAL O_a26_a_ASDATA_driver : std_logic;
SIGNAL O_a26_a_SCLR_driver : std_logic;
SIGNAL O_a26_a_SLOAD_driver : std_logic;
SIGNAL O_a26_a_ENA_driver : std_logic;
SIGNAL O_a25_a_CLK_driver : std_logic;
SIGNAL O_a25_a_D_driver : std_logic;
SIGNAL O_a25_a_ASDATA_driver : std_logic;
SIGNAL O_a25_a_SCLR_driver : std_logic;
SIGNAL O_a25_a_SLOAD_driver : std_logic;
SIGNAL O_a25_a_ENA_driver : std_logic;
SIGNAL O_a24_a_CLK_driver : std_logic;
SIGNAL O_a24_a_D_driver : std_logic;
SIGNAL O_a24_a_ASDATA_driver : std_logic;
SIGNAL O_a24_a_SCLR_driver : std_logic;
SIGNAL O_a24_a_SLOAD_driver : std_logic;
SIGNAL O_a24_a_ENA_driver : std_logic;
SIGNAL O_a23_a_CLK_driver : std_logic;
SIGNAL O_a23_a_D_driver : std_logic;
SIGNAL O_a23_a_ASDATA_driver : std_logic;
SIGNAL O_a23_a_SCLR_driver : std_logic;
SIGNAL O_a23_a_SLOAD_driver : std_logic;
SIGNAL O_a23_a_ENA_driver : std_logic;
SIGNAL O_a22_a_CLK_driver : std_logic;
SIGNAL O_a22_a_D_driver : std_logic;
SIGNAL O_a22_a_ASDATA_driver : std_logic;
SIGNAL O_a22_a_SCLR_driver : std_logic;
SIGNAL O_a22_a_SLOAD_driver : std_logic;
SIGNAL O_a22_a_ENA_driver : std_logic;
SIGNAL O_a21_a_CLK_driver : std_logic;
SIGNAL O_a21_a_D_driver : std_logic;
SIGNAL O_a21_a_ASDATA_driver : std_logic;
SIGNAL O_a21_a_SCLR_driver : std_logic;
SIGNAL O_a21_a_SLOAD_driver : std_logic;
SIGNAL O_a21_a_ENA_driver : std_logic;
SIGNAL O_a20_a_CLK_driver : std_logic;
SIGNAL O_a20_a_D_driver : std_logic;
SIGNAL O_a20_a_ASDATA_driver : std_logic;
SIGNAL O_a20_a_SCLR_driver : std_logic;
SIGNAL O_a20_a_SLOAD_driver : std_logic;
SIGNAL O_a20_a_ENA_driver : std_logic;
SIGNAL O_a19_a_CLK_driver : std_logic;
SIGNAL O_a19_a_D_driver : std_logic;
SIGNAL O_a19_a_ASDATA_driver : std_logic;
SIGNAL O_a19_a_SCLR_driver : std_logic;
SIGNAL O_a19_a_SLOAD_driver : std_logic;
SIGNAL O_a19_a_ENA_driver : std_logic;
SIGNAL O_a18_a_CLK_driver : std_logic;
SIGNAL O_a18_a_D_driver : std_logic;
SIGNAL O_a18_a_ASDATA_driver : std_logic;
SIGNAL O_a18_a_SCLR_driver : std_logic;
SIGNAL O_a18_a_SLOAD_driver : std_logic;
SIGNAL O_a18_a_ENA_driver : std_logic;
SIGNAL O_a17_a_CLK_driver : std_logic;
SIGNAL O_a17_a_D_driver : std_logic;
SIGNAL O_a17_a_ASDATA_driver : std_logic;
SIGNAL O_a17_a_SCLR_driver : std_logic;
SIGNAL O_a17_a_SLOAD_driver : std_logic;
SIGNAL O_a17_a_ENA_driver : std_logic;
SIGNAL O_a16_a_CLK_driver : std_logic;
SIGNAL O_a16_a_D_driver : std_logic;
SIGNAL O_a16_a_ASDATA_driver : std_logic;
SIGNAL O_a16_a_SCLR_driver : std_logic;
SIGNAL O_a16_a_SLOAD_driver : std_logic;
SIGNAL O_a16_a_ENA_driver : std_logic;
SIGNAL O_a15_a_CLK_driver : std_logic;
SIGNAL O_a15_a_D_driver : std_logic;
SIGNAL O_a15_a_ASDATA_driver : std_logic;
SIGNAL O_a15_a_SCLR_driver : std_logic;
SIGNAL O_a15_a_SLOAD_driver : std_logic;
SIGNAL O_a15_a_ENA_driver : std_logic;
SIGNAL O_a14_a_CLK_driver : std_logic;
SIGNAL O_a14_a_D_driver : std_logic;
SIGNAL O_a14_a_ASDATA_driver : std_logic;
SIGNAL O_a14_a_SCLR_driver : std_logic;
SIGNAL O_a14_a_SLOAD_driver : std_logic;
SIGNAL O_a14_a_ENA_driver : std_logic;
SIGNAL O_a13_a_CLK_driver : std_logic;
SIGNAL O_a13_a_D_driver : std_logic;
SIGNAL O_a13_a_ASDATA_driver : std_logic;
SIGNAL O_a13_a_SCLR_driver : std_logic;
SIGNAL O_a13_a_SLOAD_driver : std_logic;
SIGNAL O_a13_a_ENA_driver : std_logic;
SIGNAL O_a12_a_CLK_driver : std_logic;
SIGNAL O_a12_a_D_driver : std_logic;
SIGNAL O_a12_a_ASDATA_driver : std_logic;
SIGNAL O_a12_a_SCLR_driver : std_logic;
SIGNAL O_a12_a_SLOAD_driver : std_logic;
SIGNAL O_a12_a_ENA_driver : std_logic;
SIGNAL O_a11_a_CLK_driver : std_logic;
SIGNAL O_a11_a_D_driver : std_logic;
SIGNAL O_a11_a_ASDATA_driver : std_logic;
SIGNAL O_a11_a_SCLR_driver : std_logic;
SIGNAL O_a11_a_SLOAD_driver : std_logic;
SIGNAL O_a11_a_ENA_driver : std_logic;
SIGNAL O_a10_a_CLK_driver : std_logic;
SIGNAL O_a10_a_D_driver : std_logic;
SIGNAL O_a10_a_ASDATA_driver : std_logic;
SIGNAL O_a10_a_SCLR_driver : std_logic;
SIGNAL O_a10_a_SLOAD_driver : std_logic;
SIGNAL O_a10_a_ENA_driver : std_logic;
SIGNAL O_a9_a_CLK_driver : std_logic;
SIGNAL O_a9_a_D_driver : std_logic;
SIGNAL O_a9_a_ASDATA_driver : std_logic;
SIGNAL O_a9_a_SCLR_driver : std_logic;
SIGNAL O_a9_a_SLOAD_driver : std_logic;
SIGNAL O_a9_a_ENA_driver : std_logic;
SIGNAL O_a8_a_CLK_driver : std_logic;
SIGNAL O_a8_a_D_driver : std_logic;
SIGNAL O_a8_a_ASDATA_driver : std_logic;
SIGNAL O_a8_a_SCLR_driver : std_logic;
SIGNAL O_a8_a_SLOAD_driver : std_logic;
SIGNAL O_a8_a_ENA_driver : std_logic;
SIGNAL O_a7_a_CLK_driver : std_logic;
SIGNAL O_a7_a_D_driver : std_logic;
SIGNAL O_a7_a_ASDATA_driver : std_logic;
SIGNAL O_a7_a_SCLR_driver : std_logic;
SIGNAL O_a7_a_SLOAD_driver : std_logic;
SIGNAL O_a7_a_ENA_driver : std_logic;
SIGNAL O_a6_a_CLK_driver : std_logic;
SIGNAL O_a6_a_D_driver : std_logic;
SIGNAL O_a6_a_ASDATA_driver : std_logic;
SIGNAL O_a6_a_SCLR_driver : std_logic;
SIGNAL O_a6_a_SLOAD_driver : std_logic;
SIGNAL O_a6_a_ENA_driver : std_logic;
SIGNAL O_a5_a_CLK_driver : std_logic;
SIGNAL O_a5_a_D_driver : std_logic;
SIGNAL O_a5_a_ASDATA_driver : std_logic;
SIGNAL O_a5_a_SCLR_driver : std_logic;
SIGNAL O_a5_a_SLOAD_driver : std_logic;
SIGNAL O_a5_a_ENA_driver : std_logic;
SIGNAL O_a4_a_CLK_driver : std_logic;
SIGNAL O_a4_a_D_driver : std_logic;
SIGNAL O_a4_a_ASDATA_driver : std_logic;
SIGNAL O_a4_a_SCLR_driver : std_logic;
SIGNAL O_a4_a_SLOAD_driver : std_logic;
SIGNAL O_a4_a_ENA_driver : std_logic;
SIGNAL O_a3_a_CLK_driver : std_logic;
SIGNAL O_a3_a_D_driver : std_logic;
SIGNAL O_a3_a_ASDATA_driver : std_logic;
SIGNAL O_a3_a_SCLR_driver : std_logic;
SIGNAL O_a3_a_SLOAD_driver : std_logic;
SIGNAL O_a3_a_ENA_driver : std_logic;
SIGNAL O_a2_a_CLK_driver : std_logic;
SIGNAL O_a2_a_D_driver : std_logic;
SIGNAL O_a2_a_ASDATA_driver : std_logic;
SIGNAL O_a2_a_SCLR_driver : std_logic;
SIGNAL O_a2_a_SLOAD_driver : std_logic;
SIGNAL O_a2_a_ENA_driver : std_logic;
SIGNAL data_out_a7_DATAA_driver : std_logic;
SIGNAL data_out_a7_DATAB_driver : std_logic;
SIGNAL data_out_a7_DATAC_driver : std_logic;
SIGNAL data_out_a7_DATAD_driver : std_logic;
SIGNAL data_out_a8_DATAA_driver : std_logic;
SIGNAL data_out_a8_DATAB_driver : std_logic;
SIGNAL data_out_a8_DATAC_driver : std_logic;
SIGNAL data_out_a8_DATAD_driver : std_logic;
SIGNAL data_out_a9_DATAA_driver : std_logic;
SIGNAL data_out_a9_DATAB_driver : std_logic;
SIGNAL data_out_a9_DATAC_driver : std_logic;
SIGNAL data_out_a9_DATAD_driver : std_logic;
SIGNAL PC_a4_DATAA_driver : std_logic;
SIGNAL PC_a4_DATAB_driver : std_logic;
SIGNAL PC_a4_DATAD_driver : std_logic;
SIGNAL PC_a3_a_CLK_driver : std_logic;
SIGNAL PC_a3_a_D_driver : std_logic;
SIGNAL PC_a3_a_ASDATA_driver : std_logic;
SIGNAL PC_a3_a_SCLR_driver : std_logic;
SIGNAL PC_a3_a_SLOAD_driver : std_logic;
SIGNAL PC_a3_a_ENA_driver : std_logic;
SIGNAL A_a3_a_CLK_driver : std_logic;
SIGNAL A_a3_a_ASDATA_driver : std_logic;
SIGNAL A_a3_a_ENA_driver : std_logic;
SIGNAL LO_a4_DATAB_driver : std_logic;
SIGNAL LO_a4_DATAC_driver : std_logic;
SIGNAL LO_a4_DATAD_driver : std_logic;
SIGNAL LO_a3_a_CLK_driver : std_logic;
SIGNAL LO_a3_a_D_driver : std_logic;
SIGNAL LO_a3_a_SCLR_driver : std_logic;
SIGNAL LO_a3_a_ENA_driver : std_logic;
SIGNAL data_out_a11_DATAA_driver : std_logic;
SIGNAL data_out_a11_DATAB_driver : std_logic;
SIGNAL data_out_a11_DATAC_driver : std_logic;
SIGNAL data_out_a11_DATAD_driver : std_logic;
SIGNAL data_out_a12_DATAA_driver : std_logic;
SIGNAL data_out_a12_DATAB_driver : std_logic;
SIGNAL data_out_a12_DATAC_driver : std_logic;
SIGNAL data_out_a12_DATAD_driver : std_logic;
SIGNAL HI_a6_DATAA_driver : std_logic;
SIGNAL HI_a6_DATAB_driver : std_logic;
SIGNAL HI_a6_DATAC_driver : std_logic;
SIGNAL HI_a6_DATAD_driver : std_logic;
SIGNAL HI_a4_a_CLK_driver : std_logic;
SIGNAL HI_a4_a_D_driver : std_logic;
SIGNAL HI_a4_a_ENA_driver : std_logic;
SIGNAL LO_a5_DATAA_driver : std_logic;
SIGNAL LO_a5_DATAC_driver : std_logic;
SIGNAL LO_a5_DATAD_driver : std_logic;
SIGNAL LO_a4_a_CLK_driver : std_logic;
SIGNAL LO_a4_a_D_driver : std_logic;
SIGNAL LO_a4_a_SCLR_driver : std_logic;
SIGNAL LO_a4_a_ENA_driver : std_logic;
SIGNAL data_out_a14_DATAA_driver : std_logic;
SIGNAL data_out_a14_DATAB_driver : std_logic;
SIGNAL data_out_a14_DATAC_driver : std_logic;
SIGNAL data_out_a14_DATAD_driver : std_logic;
SIGNAL PC_a5_DATAA_driver : std_logic;
SIGNAL PC_a5_DATAB_driver : std_logic;
SIGNAL PC_a5_DATAD_driver : std_logic;
SIGNAL PC_a4_a_CLK_driver : std_logic;
SIGNAL PC_a4_a_D_driver : std_logic;
SIGNAL PC_a4_a_ASDATA_driver : std_logic;
SIGNAL PC_a4_a_SCLR_driver : std_logic;
SIGNAL PC_a4_a_SLOAD_driver : std_logic;
SIGNAL PC_a4_a_ENA_driver : std_logic;
SIGNAL data_out_a15_DATAA_driver : std_logic;
SIGNAL data_out_a15_DATAB_driver : std_logic;
SIGNAL data_out_a15_DATAC_driver : std_logic;
SIGNAL data_out_a15_DATAD_driver : std_logic;
SIGNAL HI_a7_DATAA_driver : std_logic;
SIGNAL HI_a7_DATAB_driver : std_logic;
SIGNAL HI_a7_DATAC_driver : std_logic;
SIGNAL HI_a7_DATAD_driver : std_logic;
SIGNAL HI_a5_a_CLK_driver : std_logic;
SIGNAL HI_a5_a_D_driver : std_logic;
SIGNAL HI_a5_a_ENA_driver : std_logic;
SIGNAL data_out_a16_DATAA_driver : std_logic;
SIGNAL data_out_a16_DATAB_driver : std_logic;
SIGNAL data_out_a16_DATAC_driver : std_logic;
SIGNAL data_out_a16_DATAD_driver : std_logic;
SIGNAL A_a5_a_CLK_driver : std_logic;
SIGNAL A_a5_a_ASDATA_driver : std_logic;
SIGNAL A_a5_a_ENA_driver : std_logic;
SIGNAL LO_a6_DATAA_driver : std_logic;
SIGNAL LO_a6_DATAB_driver : std_logic;
SIGNAL LO_a6_DATAC_driver : std_logic;
SIGNAL LO_a5_a_CLK_driver : std_logic;
SIGNAL LO_a5_a_D_driver : std_logic;
SIGNAL LO_a5_a_SCLR_driver : std_logic;
SIGNAL LO_a5_a_ENA_driver : std_logic;
SIGNAL data_out_a17_DATAA_driver : std_logic;
SIGNAL data_out_a17_DATAB_driver : std_logic;
SIGNAL data_out_a17_DATAC_driver : std_logic;
SIGNAL data_out_a17_DATAD_driver : std_logic;
SIGNAL PC_a6_DATAA_driver : std_logic;
SIGNAL PC_a6_DATAB_driver : std_logic;
SIGNAL PC_a6_DATAD_driver : std_logic;
SIGNAL PC_a5_a_CLK_driver : std_logic;
SIGNAL PC_a5_a_D_driver : std_logic;
SIGNAL PC_a5_a_ASDATA_driver : std_logic;
SIGNAL PC_a5_a_SCLR_driver : std_logic;
SIGNAL PC_a5_a_SLOAD_driver : std_logic;
SIGNAL PC_a5_a_ENA_driver : std_logic;
SIGNAL data_out_a18_DATAA_driver : std_logic;
SIGNAL data_out_a18_DATAB_driver : std_logic;
SIGNAL data_out_a18_DATAC_driver : std_logic;
SIGNAL data_out_a18_DATAD_driver : std_logic;
SIGNAL PC_a7_DATAA_driver : std_logic;
SIGNAL PC_a7_DATAB_driver : std_logic;
SIGNAL PC_a7_DATAD_driver : std_logic;
SIGNAL PC_a6_a_CLK_driver : std_logic;
SIGNAL PC_a6_a_D_driver : std_logic;
SIGNAL PC_a6_a_ASDATA_driver : std_logic;
SIGNAL PC_a6_a_SCLR_driver : std_logic;
SIGNAL PC_a6_a_SLOAD_driver : std_logic;
SIGNAL PC_a6_a_ENA_driver : std_logic;
SIGNAL HI_a8_DATAA_driver : std_logic;
SIGNAL HI_a8_DATAB_driver : std_logic;
SIGNAL HI_a8_DATAC_driver : std_logic;
SIGNAL HI_a8_DATAD_driver : std_logic;
SIGNAL HI_a6_a_CLK_driver : std_logic;
SIGNAL HI_a6_a_D_driver : std_logic;
SIGNAL HI_a6_a_ENA_driver : std_logic;
SIGNAL A_a6_a_CLK_driver : std_logic;
SIGNAL A_a6_a_ASDATA_driver : std_logic;
SIGNAL A_a6_a_ENA_driver : std_logic;
SIGNAL data_out_a19_DATAA_driver : std_logic;
SIGNAL data_out_a19_DATAB_driver : std_logic;
SIGNAL data_out_a19_DATAC_driver : std_logic;
SIGNAL data_out_a19_DATAD_driver : std_logic;
SIGNAL data_out_a20_DATAA_driver : std_logic;
SIGNAL data_out_a20_DATAB_driver : std_logic;
SIGNAL data_out_a20_DATAC_driver : std_logic;
SIGNAL data_out_a20_DATAD_driver : std_logic;
SIGNAL data_out_a21_DATAA_driver : std_logic;
SIGNAL data_out_a21_DATAB_driver : std_logic;
SIGNAL data_out_a21_DATAC_driver : std_logic;
SIGNAL data_out_a21_DATAD_driver : std_logic;
SIGNAL data_out_a22_DATAA_driver : std_logic;
SIGNAL data_out_a22_DATAB_driver : std_logic;
SIGNAL data_out_a22_DATAC_driver : std_logic;
SIGNAL data_out_a22_DATAD_driver : std_logic;
SIGNAL LO_a7_DATAA_driver : std_logic;
SIGNAL LO_a7_DATAB_driver : std_logic;
SIGNAL LO_a7_DATAC_driver : std_logic;
SIGNAL LO_a6_a_CLK_driver : std_logic;
SIGNAL LO_a6_a_D_driver : std_logic;
SIGNAL LO_a6_a_SCLR_driver : std_logic;
SIGNAL LO_a6_a_ENA_driver : std_logic;
SIGNAL LO_a8_DATAB_driver : std_logic;
SIGNAL LO_a8_DATAC_driver : std_logic;
SIGNAL LO_a8_DATAD_driver : std_logic;
SIGNAL LO_a7_a_CLK_driver : std_logic;
SIGNAL LO_a7_a_D_driver : std_logic;
SIGNAL LO_a7_a_SCLR_driver : std_logic;
SIGNAL LO_a7_a_ENA_driver : std_logic;
SIGNAL data_out_a23_DATAA_driver : std_logic;
SIGNAL data_out_a23_DATAB_driver : std_logic;
SIGNAL data_out_a23_DATAC_driver : std_logic;
SIGNAL data_out_a23_DATAD_driver : std_logic;
SIGNAL PC_a8_DATAA_driver : std_logic;
SIGNAL PC_a8_DATAB_driver : std_logic;
SIGNAL PC_a8_DATAD_driver : std_logic;
SIGNAL PC_a7_a_CLK_driver : std_logic;
SIGNAL PC_a7_a_D_driver : std_logic;
SIGNAL PC_a7_a_ASDATA_driver : std_logic;
SIGNAL PC_a7_a_SCLR_driver : std_logic;
SIGNAL PC_a7_a_SLOAD_driver : std_logic;
SIGNAL PC_a7_a_ENA_driver : std_logic;
SIGNAL data_out_a24_DATAA_driver : std_logic;
SIGNAL data_out_a24_DATAB_driver : std_logic;
SIGNAL data_out_a24_DATAC_driver : std_logic;
SIGNAL data_out_a24_DATAD_driver : std_logic;
SIGNAL PC_a9_DATAA_driver : std_logic;
SIGNAL PC_a9_DATAB_driver : std_logic;
SIGNAL PC_a9_DATAD_driver : std_logic;
SIGNAL PC_a8_a_CLK_driver : std_logic;
SIGNAL PC_a8_a_D_driver : std_logic;
SIGNAL PC_a8_a_ASDATA_driver : std_logic;
SIGNAL PC_a8_a_SCLR_driver : std_logic;
SIGNAL PC_a8_a_SLOAD_driver : std_logic;
SIGNAL PC_a8_a_ENA_driver : std_logic;
SIGNAL LO_a9_DATAA_driver : std_logic;
SIGNAL LO_a9_DATAC_driver : std_logic;
SIGNAL LO_a9_DATAD_driver : std_logic;
SIGNAL LO_a8_a_CLK_driver : std_logic;
SIGNAL LO_a8_a_D_driver : std_logic;
SIGNAL LO_a8_a_SCLR_driver : std_logic;
SIGNAL LO_a8_a_ENA_driver : std_logic;
SIGNAL data_out_a26_DATAA_driver : std_logic;
SIGNAL data_out_a26_DATAB_driver : std_logic;
SIGNAL data_out_a26_DATAC_driver : std_logic;
SIGNAL data_out_a26_DATAD_driver : std_logic;
SIGNAL data_out_a27_DATAA_driver : std_logic;
SIGNAL data_out_a27_DATAB_driver : std_logic;
SIGNAL data_out_a27_DATAC_driver : std_logic;
SIGNAL data_out_a27_DATAD_driver : std_logic;
SIGNAL data_out_a28_DATAA_driver : std_logic;
SIGNAL data_out_a28_DATAB_driver : std_logic;
SIGNAL data_out_a28_DATAC_driver : std_logic;
SIGNAL data_out_a28_DATAD_driver : std_logic;
SIGNAL data_out_a29_DATAA_driver : std_logic;
SIGNAL data_out_a29_DATAB_driver : std_logic;
SIGNAL data_out_a29_DATAC_driver : std_logic;
SIGNAL data_out_a29_DATAD_driver : std_logic;
SIGNAL data_out_a30_DATAA_driver : std_logic;
SIGNAL data_out_a30_DATAB_driver : std_logic;
SIGNAL data_out_a30_DATAC_driver : std_logic;
SIGNAL data_out_a30_DATAD_driver : std_logic;
SIGNAL LO_a11_DATAA_driver : std_logic;
SIGNAL LO_a11_DATAC_driver : std_logic;
SIGNAL LO_a11_DATAD_driver : std_logic;
SIGNAL LO_a10_a_CLK_driver : std_logic;
SIGNAL LO_a10_a_D_driver : std_logic;
SIGNAL LO_a10_a_SCLR_driver : std_logic;
SIGNAL LO_a10_a_ENA_driver : std_logic;
SIGNAL data_out_a32_DATAA_driver : std_logic;
SIGNAL data_out_a32_DATAB_driver : std_logic;
SIGNAL data_out_a32_DATAC_driver : std_logic;
SIGNAL data_out_a32_DATAD_driver : std_logic;
SIGNAL data_out_a33_DATAA_driver : std_logic;
SIGNAL data_out_a33_DATAB_driver : std_logic;
SIGNAL data_out_a33_DATAC_driver : std_logic;
SIGNAL data_out_a33_DATAD_driver : std_logic;
SIGNAL PC_a12_DATAA_driver : std_logic;
SIGNAL PC_a12_DATAB_driver : std_logic;
SIGNAL PC_a12_DATAD_driver : std_logic;
SIGNAL PC_a11_a_CLK_driver : std_logic;
SIGNAL PC_a11_a_D_driver : std_logic;
SIGNAL PC_a11_a_ASDATA_driver : std_logic;
SIGNAL PC_a11_a_SCLR_driver : std_logic;
SIGNAL PC_a11_a_SLOAD_driver : std_logic;
SIGNAL PC_a11_a_ENA_driver : std_logic;
SIGNAL LO_a12_DATAA_driver : std_logic;
SIGNAL LO_a12_DATAC_driver : std_logic;
SIGNAL LO_a12_DATAD_driver : std_logic;
SIGNAL LO_a11_a_CLK_driver : std_logic;
SIGNAL LO_a11_a_D_driver : std_logic;
SIGNAL LO_a11_a_SCLR_driver : std_logic;
SIGNAL LO_a11_a_ENA_driver : std_logic;
SIGNAL data_out_a34_DATAA_driver : std_logic;
SIGNAL data_out_a34_DATAB_driver : std_logic;
SIGNAL data_out_a34_DATAC_driver : std_logic;
SIGNAL data_out_a34_DATAD_driver : std_logic;
SIGNAL A_a11_a_CLK_driver : std_logic;
SIGNAL A_a11_a_ASDATA_driver : std_logic;
SIGNAL A_a11_a_ENA_driver : std_logic;
SIGNAL data_out_a35_DATAA_driver : std_logic;
SIGNAL data_out_a35_DATAB_driver : std_logic;
SIGNAL data_out_a35_DATAC_driver : std_logic;
SIGNAL data_out_a35_DATAD_driver : std_logic;
SIGNAL data_out_a36_DATAA_driver : std_logic;
SIGNAL data_out_a36_DATAB_driver : std_logic;
SIGNAL data_out_a36_DATAC_driver : std_logic;
SIGNAL data_out_a36_DATAD_driver : std_logic;
SIGNAL data_out_a37_DATAA_driver : std_logic;
SIGNAL data_out_a37_DATAB_driver : std_logic;
SIGNAL data_out_a37_DATAC_driver : std_logic;
SIGNAL data_out_a37_DATAD_driver : std_logic;
SIGNAL LO_a13_DATAB_driver : std_logic;
SIGNAL LO_a13_DATAC_driver : std_logic;
SIGNAL LO_a13_DATAD_driver : std_logic;
SIGNAL LO_a12_a_CLK_driver : std_logic;
SIGNAL LO_a12_a_D_driver : std_logic;
SIGNAL LO_a12_a_SCLR_driver : std_logic;
SIGNAL LO_a12_a_ENA_driver : std_logic;
SIGNAL data_out_a38_DATAA_driver : std_logic;
SIGNAL data_out_a38_DATAB_driver : std_logic;
SIGNAL data_out_a38_DATAC_driver : std_logic;
SIGNAL data_out_a38_DATAD_driver : std_logic;
SIGNAL data_out_a39_DATAA_driver : std_logic;
SIGNAL data_out_a39_DATAB_driver : std_logic;
SIGNAL data_out_a39_DATAC_driver : std_logic;
SIGNAL data_out_a39_DATAD_driver : std_logic;
SIGNAL LO_a14_DATAA_driver : std_logic;
SIGNAL LO_a14_DATAC_driver : std_logic;
SIGNAL LO_a14_DATAD_driver : std_logic;
SIGNAL LO_a13_a_CLK_driver : std_logic;
SIGNAL LO_a13_a_D_driver : std_logic;
SIGNAL LO_a13_a_SCLR_driver : std_logic;
SIGNAL LO_a13_a_ENA_driver : std_logic;
SIGNAL data_out_a41_DATAA_driver : std_logic;
SIGNAL data_out_a41_DATAB_driver : std_logic;
SIGNAL data_out_a41_DATAC_driver : std_logic;
SIGNAL data_out_a41_DATAD_driver : std_logic;
SIGNAL data_out_a42_DATAA_driver : std_logic;
SIGNAL data_out_a42_DATAB_driver : std_logic;
SIGNAL data_out_a42_DATAC_driver : std_logic;
SIGNAL data_out_a42_DATAD_driver : std_logic;
SIGNAL A_a15_DATAC_driver : std_logic;
SIGNAL A_a15_DATAD_driver : std_logic;
SIGNAL A_a14_a_CLK_driver : std_logic;
SIGNAL A_a14_a_ASDATA_driver : std_logic;
SIGNAL A_a14_a_ENA_driver : std_logic;
SIGNAL data_out_a43_DATAA_driver : std_logic;
SIGNAL data_out_a43_DATAB_driver : std_logic;
SIGNAL data_out_a43_DATAC_driver : std_logic;
SIGNAL data_out_a43_DATAD_driver : std_logic;
SIGNAL LO_a15_DATAA_driver : std_logic;
SIGNAL LO_a15_DATAB_driver : std_logic;
SIGNAL LO_a15_DATAC_driver : std_logic;
SIGNAL LO_a14_a_CLK_driver : std_logic;
SIGNAL LO_a14_a_D_driver : std_logic;
SIGNAL LO_a14_a_SCLR_driver : std_logic;
SIGNAL LO_a14_a_ENA_driver : std_logic;
SIGNAL data_out_a44_DATAA_driver : std_logic;
SIGNAL data_out_a44_DATAB_driver : std_logic;
SIGNAL data_out_a44_DATAC_driver : std_logic;
SIGNAL data_out_a44_DATAD_driver : std_logic;
SIGNAL data_out_a45_DATAA_driver : std_logic;
SIGNAL data_out_a45_DATAB_driver : std_logic;
SIGNAL data_out_a45_DATAC_driver : std_logic;
SIGNAL data_out_a45_DATAD_driver : std_logic;
SIGNAL data_in_a15_a_ainput_I_driver : std_logic;
SIGNAL LO_a16_DATAB_driver : std_logic;
SIGNAL LO_a16_DATAC_driver : std_logic;
SIGNAL LO_a16_DATAD_driver : std_logic;
SIGNAL LO_a15_a_CLK_driver : std_logic;
SIGNAL LO_a15_a_D_driver : std_logic;
SIGNAL LO_a15_a_SCLR_driver : std_logic;
SIGNAL LO_a15_a_ENA_driver : std_logic;
SIGNAL A_a15_a_CLK_driver : std_logic;
SIGNAL A_a15_a_ASDATA_driver : std_logic;
SIGNAL A_a15_a_ENA_driver : std_logic;
SIGNAL data_out_a47_DATAA_driver : std_logic;
SIGNAL data_out_a47_DATAB_driver : std_logic;
SIGNAL data_out_a47_DATAC_driver : std_logic;
SIGNAL data_out_a47_DATAD_driver : std_logic;
SIGNAL intr_pc_a13_a_ainput_I_driver : std_logic;
SIGNAL PC_a16_DATAA_driver : std_logic;
SIGNAL PC_a16_DATAB_driver : std_logic;
SIGNAL PC_a16_DATAD_driver : std_logic;
SIGNAL PC_a15_a_CLK_driver : std_logic;
SIGNAL PC_a15_a_D_driver : std_logic;
SIGNAL PC_a15_a_ASDATA_driver : std_logic;
SIGNAL PC_a15_a_SCLR_driver : std_logic;
SIGNAL PC_a15_a_SLOAD_driver : std_logic;
SIGNAL PC_a15_a_ENA_driver : std_logic;
SIGNAL data_out_a48_DATAA_driver : std_logic;
SIGNAL data_out_a48_DATAB_driver : std_logic;
SIGNAL data_out_a48_DATAC_driver : std_logic;
SIGNAL data_out_a48_DATAD_driver : std_logic;
SIGNAL data_out_a49_DATAA_driver : std_logic;
SIGNAL data_out_a49_DATAB_driver : std_logic;
SIGNAL data_out_a49_DATAC_driver : std_logic;
SIGNAL data_out_a49_DATAD_driver : std_logic;
SIGNAL LO_a17_DATAA_driver : std_logic;
SIGNAL LO_a17_DATAC_driver : std_logic;
SIGNAL LO_a17_DATAD_driver : std_logic;
SIGNAL LO_a16_a_CLK_driver : std_logic;
SIGNAL LO_a16_a_D_driver : std_logic;
SIGNAL LO_a16_a_SCLR_driver : std_logic;
SIGNAL LO_a16_a_ENA_driver : std_logic;
SIGNAL data_out_a50_DATAA_driver : std_logic;
SIGNAL data_out_a50_DATAB_driver : std_logic;
SIGNAL data_out_a50_DATAC_driver : std_logic;
SIGNAL data_out_a50_DATAD_driver : std_logic;
SIGNAL data_out_a51_DATAA_driver : std_logic;
SIGNAL data_out_a51_DATAB_driver : std_logic;
SIGNAL data_out_a51_DATAC_driver : std_logic;
SIGNAL data_out_a51_DATAD_driver : std_logic;
SIGNAL data_out_a52_DATAA_driver : std_logic;
SIGNAL data_out_a52_DATAB_driver : std_logic;
SIGNAL data_out_a52_DATAC_driver : std_logic;
SIGNAL data_out_a52_DATAD_driver : std_logic;
SIGNAL data_out_a53_DATAA_driver : std_logic;
SIGNAL data_out_a53_DATAB_driver : std_logic;
SIGNAL data_out_a53_DATAC_driver : std_logic;
SIGNAL data_out_a53_DATAD_driver : std_logic;
SIGNAL data_out_a54_DATAA_driver : std_logic;
SIGNAL data_out_a54_DATAB_driver : std_logic;
SIGNAL data_out_a54_DATAC_driver : std_logic;
SIGNAL data_out_a54_DATAD_driver : std_logic;
SIGNAL LO_a18_DATAB_driver : std_logic;
SIGNAL LO_a18_DATAC_driver : std_logic;
SIGNAL LO_a18_DATAD_driver : std_logic;
SIGNAL LO_a17_a_CLK_driver : std_logic;
SIGNAL LO_a17_a_D_driver : std_logic;
SIGNAL LO_a17_a_SCLR_driver : std_logic;
SIGNAL LO_a17_a_ENA_driver : std_logic;
SIGNAL LO_a19_DATAA_driver : std_logic;
SIGNAL LO_a19_DATAC_driver : std_logic;
SIGNAL LO_a19_DATAD_driver : std_logic;
SIGNAL LO_a18_a_CLK_driver : std_logic;
SIGNAL LO_a18_a_D_driver : std_logic;
SIGNAL LO_a18_a_SCLR_driver : std_logic;
SIGNAL LO_a18_a_ENA_driver : std_logic;
SIGNAL HI_a20_DATAA_driver : std_logic;
SIGNAL HI_a20_DATAB_driver : std_logic;
SIGNAL HI_a20_DATAC_driver : std_logic;
SIGNAL HI_a20_DATAD_driver : std_logic;
SIGNAL HI_a18_a_CLK_driver : std_logic;
SIGNAL HI_a18_a_D_driver : std_logic;
SIGNAL HI_a18_a_ENA_driver : std_logic;
SIGNAL data_out_a56_DATAA_driver : std_logic;
SIGNAL data_out_a56_DATAB_driver : std_logic;
SIGNAL data_out_a56_DATAC_driver : std_logic;
SIGNAL data_out_a56_DATAD_driver : std_logic;
SIGNAL PC_a19_DATAA_driver : std_logic;
SIGNAL PC_a19_DATAB_driver : std_logic;
SIGNAL PC_a19_DATAD_driver : std_logic;
SIGNAL PC_a18_a_CLK_driver : std_logic;
SIGNAL PC_a18_a_D_driver : std_logic;
SIGNAL PC_a18_a_ASDATA_driver : std_logic;
SIGNAL PC_a18_a_SCLR_driver : std_logic;
SIGNAL PC_a18_a_SLOAD_driver : std_logic;
SIGNAL PC_a18_a_ENA_driver : std_logic;
SIGNAL data_out_a57_DATAA_driver : std_logic;
SIGNAL data_out_a57_DATAB_driver : std_logic;
SIGNAL data_out_a57_DATAC_driver : std_logic;
SIGNAL data_out_a57_DATAD_driver : std_logic;
SIGNAL A_a19_a_CLK_driver : std_logic;
SIGNAL A_a19_a_ASDATA_driver : std_logic;
SIGNAL A_a19_a_ENA_driver : std_logic;
SIGNAL HI_a21_DATAA_driver : std_logic;
SIGNAL HI_a21_DATAB_driver : std_logic;
SIGNAL HI_a21_DATAC_driver : std_logic;
SIGNAL HI_a21_DATAD_driver : std_logic;
SIGNAL HI_a19_a_CLK_driver : std_logic;
SIGNAL HI_a19_a_D_driver : std_logic;
SIGNAL HI_a19_a_ENA_driver : std_logic;
SIGNAL data_out_a58_DATAA_driver : std_logic;
SIGNAL data_out_a58_DATAB_driver : std_logic;
SIGNAL data_out_a58_DATAC_driver : std_logic;
SIGNAL data_out_a58_DATAD_driver : std_logic;
SIGNAL data_out_a59_DATAA_driver : std_logic;
SIGNAL data_out_a59_DATAB_driver : std_logic;
SIGNAL data_out_a59_DATAC_driver : std_logic;
SIGNAL data_out_a59_DATAD_driver : std_logic;
SIGNAL intr_pc_a17_a_ainput_I_driver : std_logic;
SIGNAL PC_a20_DATAA_driver : std_logic;
SIGNAL PC_a20_DATAB_driver : std_logic;
SIGNAL PC_a20_DATAD_driver : std_logic;
SIGNAL PC_a19_a_CLK_driver : std_logic;
SIGNAL PC_a19_a_D_driver : std_logic;
SIGNAL PC_a19_a_ASDATA_driver : std_logic;
SIGNAL PC_a19_a_SCLR_driver : std_logic;
SIGNAL PC_a19_a_SLOAD_driver : std_logic;
SIGNAL PC_a19_a_ENA_driver : std_logic;
SIGNAL data_out_a60_DATAA_driver : std_logic;
SIGNAL data_out_a60_DATAB_driver : std_logic;
SIGNAL data_out_a60_DATAC_driver : std_logic;
SIGNAL data_out_a60_DATAD_driver : std_logic;
SIGNAL intr_pc_a18_a_ainput_I_driver : std_logic;
SIGNAL PC_a21_DATAA_driver : std_logic;
SIGNAL PC_a21_DATAB_driver : std_logic;
SIGNAL PC_a21_DATAD_driver : std_logic;
SIGNAL data_in_a20_a_ainput_I_driver : std_logic;
SIGNAL PC_a20_a_CLK_driver : std_logic;
SIGNAL PC_a20_a_D_driver : std_logic;
SIGNAL PC_a20_a_ASDATA_driver : std_logic;
SIGNAL PC_a20_a_SCLR_driver : std_logic;
SIGNAL PC_a20_a_SLOAD_driver : std_logic;
SIGNAL PC_a20_a_ENA_driver : std_logic;
SIGNAL HI_a22_DATAA_driver : std_logic;
SIGNAL HI_a22_DATAB_driver : std_logic;
SIGNAL HI_a22_DATAC_driver : std_logic;
SIGNAL HI_a22_DATAD_driver : std_logic;
SIGNAL HI_a20_a_CLK_driver : std_logic;
SIGNAL HI_a20_a_D_driver : std_logic;
SIGNAL HI_a20_a_ENA_driver : std_logic;
SIGNAL A_a20_a_CLK_driver : std_logic;
SIGNAL A_a20_a_ASDATA_driver : std_logic;
SIGNAL A_a20_a_ENA_driver : std_logic;
SIGNAL data_out_a61_DATAA_driver : std_logic;
SIGNAL data_out_a61_DATAB_driver : std_logic;
SIGNAL data_out_a61_DATAC_driver : std_logic;
SIGNAL data_out_a61_DATAD_driver : std_logic;
SIGNAL data_out_a62_DATAA_driver : std_logic;
SIGNAL data_out_a62_DATAB_driver : std_logic;
SIGNAL data_out_a62_DATAC_driver : std_logic;
SIGNAL data_out_a62_DATAD_driver : std_logic;
SIGNAL data_out_a63_DATAA_driver : std_logic;
SIGNAL data_out_a63_DATAB_driver : std_logic;
SIGNAL data_out_a63_DATAC_driver : std_logic;
SIGNAL data_out_a63_DATAD_driver : std_logic;
SIGNAL intr_pc_a19_a_ainput_I_driver : std_logic;
SIGNAL PC_a22_DATAA_driver : std_logic;
SIGNAL PC_a22_DATAB_driver : std_logic;
SIGNAL PC_a22_DATAD_driver : std_logic;
SIGNAL PC_a21_a_CLK_driver : std_logic;
SIGNAL PC_a21_a_D_driver : std_logic;
SIGNAL PC_a21_a_ASDATA_driver : std_logic;
SIGNAL PC_a21_a_SCLR_driver : std_logic;
SIGNAL PC_a21_a_SLOAD_driver : std_logic;
SIGNAL PC_a21_a_ENA_driver : std_logic;
SIGNAL A_a21_a_CLK_driver : std_logic;
SIGNAL A_a21_a_ASDATA_driver : std_logic;
SIGNAL A_a21_a_ENA_driver : std_logic;
SIGNAL data_out_a64_DATAA_driver : std_logic;
SIGNAL data_out_a64_DATAB_driver : std_logic;
SIGNAL data_out_a64_DATAC_driver : std_logic;
SIGNAL data_out_a64_DATAD_driver : std_logic;
SIGNAL LO_a22_DATAA_driver : std_logic;
SIGNAL LO_a22_DATAC_driver : std_logic;
SIGNAL LO_a22_DATAD_driver : std_logic;
SIGNAL LO_a21_a_CLK_driver : std_logic;
SIGNAL LO_a21_a_D_driver : std_logic;
SIGNAL LO_a21_a_SCLR_driver : std_logic;
SIGNAL LO_a21_a_ENA_driver : std_logic;
SIGNAL data_out_a65_DATAA_driver : std_logic;
SIGNAL data_out_a65_DATAB_driver : std_logic;
SIGNAL data_out_a65_DATAC_driver : std_logic;
SIGNAL data_out_a65_DATAD_driver : std_logic;
SIGNAL data_out_a66_DATAA_driver : std_logic;
SIGNAL data_out_a66_DATAB_driver : std_logic;
SIGNAL data_out_a66_DATAC_driver : std_logic;
SIGNAL data_out_a66_DATAD_driver : std_logic;
SIGNAL intr_pc_a20_a_ainput_I_driver : std_logic;
SIGNAL PC_a23_DATAA_driver : std_logic;
SIGNAL PC_a23_DATAB_driver : std_logic;
SIGNAL PC_a23_DATAD_driver : std_logic;
SIGNAL PC_a22_a_CLK_driver : std_logic;
SIGNAL PC_a22_a_D_driver : std_logic;
SIGNAL PC_a22_a_ASDATA_driver : std_logic;
SIGNAL PC_a22_a_SCLR_driver : std_logic;
SIGNAL PC_a22_a_SLOAD_driver : std_logic;
SIGNAL PC_a22_a_ENA_driver : std_logic;
SIGNAL A_a22_a_CLK_driver : std_logic;
SIGNAL A_a22_a_ASDATA_driver : std_logic;
SIGNAL A_a22_a_ENA_driver : std_logic;
SIGNAL data_out_a67_DATAA_driver : std_logic;
SIGNAL data_out_a67_DATAB_driver : std_logic;
SIGNAL data_out_a67_DATAC_driver : std_logic;
SIGNAL data_out_a67_DATAD_driver : std_logic;
SIGNAL LO_a23_DATAA_driver : std_logic;
SIGNAL LO_a23_DATAC_driver : std_logic;
SIGNAL LO_a23_DATAD_driver : std_logic;
SIGNAL LO_a22_a_CLK_driver : std_logic;
SIGNAL LO_a22_a_D_driver : std_logic;
SIGNAL LO_a22_a_SCLR_driver : std_logic;
SIGNAL LO_a22_a_ENA_driver : std_logic;
SIGNAL data_out_a68_DATAA_driver : std_logic;
SIGNAL data_out_a68_DATAB_driver : std_logic;
SIGNAL data_out_a68_DATAC_driver : std_logic;
SIGNAL data_out_a68_DATAD_driver : std_logic;
SIGNAL data_out_a69_DATAA_driver : std_logic;
SIGNAL data_out_a69_DATAB_driver : std_logic;
SIGNAL data_out_a69_DATAC_driver : std_logic;
SIGNAL data_out_a69_DATAD_driver : std_logic;
SIGNAL LO_a24_DATAA_driver : std_logic;
SIGNAL LO_a24_DATAC_driver : std_logic;
SIGNAL LO_a24_DATAD_driver : std_logic;
SIGNAL LO_a23_a_CLK_driver : std_logic;
SIGNAL LO_a23_a_D_driver : std_logic;
SIGNAL LO_a23_a_SCLR_driver : std_logic;
SIGNAL LO_a23_a_ENA_driver : std_logic;
SIGNAL A_a23_a_CLK_driver : std_logic;
SIGNAL A_a23_a_ASDATA_driver : std_logic;
SIGNAL A_a23_a_ENA_driver : std_logic;
SIGNAL data_out_a71_DATAA_driver : std_logic;
SIGNAL data_out_a71_DATAB_driver : std_logic;
SIGNAL data_out_a71_DATAC_driver : std_logic;
SIGNAL data_out_a71_DATAD_driver : std_logic;
SIGNAL intr_pc_a21_a_ainput_I_driver : std_logic;
SIGNAL PC_a24_DATAA_driver : std_logic;
SIGNAL PC_a24_DATAB_driver : std_logic;
SIGNAL PC_a24_DATAD_driver : std_logic;
SIGNAL PC_a23_a_CLK_driver : std_logic;
SIGNAL PC_a23_a_D_driver : std_logic;
SIGNAL PC_a23_a_ASDATA_driver : std_logic;
SIGNAL PC_a23_a_SCLR_driver : std_logic;
SIGNAL PC_a23_a_SLOAD_driver : std_logic;
SIGNAL PC_a23_a_ENA_driver : std_logic;
SIGNAL data_out_a72_DATAA_driver : std_logic;
SIGNAL data_out_a72_DATAB_driver : std_logic;
SIGNAL data_out_a72_DATAC_driver : std_logic;
SIGNAL data_out_a72_DATAD_driver : std_logic;
SIGNAL LO_a25_DATAA_driver : std_logic;
SIGNAL LO_a25_DATAC_driver : std_logic;
SIGNAL LO_a25_DATAD_driver : std_logic;
SIGNAL LO_a24_a_CLK_driver : std_logic;
SIGNAL LO_a24_a_D_driver : std_logic;
SIGNAL LO_a24_a_SCLR_driver : std_logic;
SIGNAL LO_a24_a_ENA_driver : std_logic;
SIGNAL data_out_a74_DATAA_driver : std_logic;
SIGNAL data_out_a74_DATAB_driver : std_logic;
SIGNAL data_out_a74_DATAC_driver : std_logic;
SIGNAL data_out_a74_DATAD_driver : std_logic;
SIGNAL intr_pc_a22_a_ainput_I_driver : std_logic;
SIGNAL PC_a25_DATAA_driver : std_logic;
SIGNAL PC_a25_DATAB_driver : std_logic;
SIGNAL PC_a25_DATAD_driver : std_logic;
SIGNAL PC_a24_a_CLK_driver : std_logic;
SIGNAL PC_a24_a_D_driver : std_logic;
SIGNAL PC_a24_a_ASDATA_driver : std_logic;
SIGNAL PC_a24_a_SCLR_driver : std_logic;
SIGNAL PC_a24_a_SLOAD_driver : std_logic;
SIGNAL PC_a24_a_ENA_driver : std_logic;
SIGNAL data_out_a75_DATAA_driver : std_logic;
SIGNAL data_out_a75_DATAB_driver : std_logic;
SIGNAL data_out_a75_DATAC_driver : std_logic;
SIGNAL data_out_a75_DATAD_driver : std_logic;
SIGNAL LO_a26_DATAB_driver : std_logic;
SIGNAL LO_a26_DATAC_driver : std_logic;
SIGNAL LO_a26_DATAD_driver : std_logic;
SIGNAL LO_a25_a_CLK_driver : std_logic;
SIGNAL LO_a25_a_D_driver : std_logic;
SIGNAL LO_a25_a_SCLR_driver : std_logic;
SIGNAL LO_a25_a_ENA_driver : std_logic;
SIGNAL data_out_a77_DATAA_driver : std_logic;
SIGNAL data_out_a77_DATAB_driver : std_logic;
SIGNAL data_out_a77_DATAC_driver : std_logic;
SIGNAL data_out_a77_DATAD_driver : std_logic;
SIGNAL data_out_a78_DATAA_driver : std_logic;
SIGNAL data_out_a78_DATAB_driver : std_logic;
SIGNAL data_out_a78_DATAC_driver : std_logic;
SIGNAL data_out_a78_DATAD_driver : std_logic;
SIGNAL LO_a27_DATAA_driver : std_logic;
SIGNAL LO_a27_DATAB_driver : std_logic;
SIGNAL LO_a27_DATAC_driver : std_logic;
SIGNAL LO_a26_a_CLK_driver : std_logic;
SIGNAL LO_a26_a_D_driver : std_logic;
SIGNAL LO_a26_a_SCLR_driver : std_logic;
SIGNAL LO_a26_a_ENA_driver : std_logic;
SIGNAL data_out_a79_DATAA_driver : std_logic;
SIGNAL data_out_a79_DATAB_driver : std_logic;
SIGNAL data_out_a79_DATAC_driver : std_logic;
SIGNAL data_out_a79_DATAD_driver : std_logic;
SIGNAL data_out_a80_DATAA_driver : std_logic;
SIGNAL data_out_a80_DATAB_driver : std_logic;
SIGNAL data_out_a80_DATAC_driver : std_logic;
SIGNAL data_out_a80_DATAD_driver : std_logic;
SIGNAL data_out_a81_DATAA_driver : std_logic;
SIGNAL data_out_a81_DATAB_driver : std_logic;
SIGNAL data_out_a81_DATAC_driver : std_logic;
SIGNAL data_out_a81_DATAD_driver : std_logic;
SIGNAL intr_pc_a25_a_ainput_I_driver : std_logic;
SIGNAL PC_a28_DATAA_driver : std_logic;
SIGNAL PC_a28_DATAB_driver : std_logic;
SIGNAL PC_a28_DATAD_driver : std_logic;
SIGNAL PC_a27_a_CLK_driver : std_logic;
SIGNAL PC_a27_a_D_driver : std_logic;
SIGNAL PC_a27_a_ASDATA_driver : std_logic;
SIGNAL PC_a27_a_SCLR_driver : std_logic;
SIGNAL PC_a27_a_SLOAD_driver : std_logic;
SIGNAL PC_a27_a_ENA_driver : std_logic;
SIGNAL LO_a28_DATAB_driver : std_logic;
SIGNAL LO_a28_DATAC_driver : std_logic;
SIGNAL LO_a28_DATAD_driver : std_logic;
SIGNAL LO_a27_a_CLK_driver : std_logic;
SIGNAL LO_a27_a_D_driver : std_logic;
SIGNAL LO_a27_a_SCLR_driver : std_logic;
SIGNAL LO_a27_a_ENA_driver : std_logic;
SIGNAL data_out_a82_DATAA_driver : std_logic;
SIGNAL data_out_a82_DATAB_driver : std_logic;
SIGNAL data_out_a82_DATAC_driver : std_logic;
SIGNAL data_out_a82_DATAD_driver : std_logic;
SIGNAL data_out_a83_DATAA_driver : std_logic;
SIGNAL data_out_a83_DATAB_driver : std_logic;
SIGNAL data_out_a83_DATAC_driver : std_logic;
SIGNAL data_out_a83_DATAD_driver : std_logic;
SIGNAL data_out_a84_DATAA_driver : std_logic;
SIGNAL data_out_a84_DATAB_driver : std_logic;
SIGNAL data_out_a84_DATAC_driver : std_logic;
SIGNAL data_out_a84_DATAD_driver : std_logic;
SIGNAL data_out_a86_DATAA_driver : std_logic;
SIGNAL data_out_a86_DATAB_driver : std_logic;
SIGNAL data_out_a86_DATAC_driver : std_logic;
SIGNAL data_out_a86_DATAD_driver : std_logic;
SIGNAL data_out_a87_DATAA_driver : std_logic;
SIGNAL data_out_a87_DATAB_driver : std_logic;
SIGNAL data_out_a87_DATAC_driver : std_logic;
SIGNAL data_out_a87_DATAD_driver : std_logic;
SIGNAL A_a29_a_CLK_driver : std_logic;
SIGNAL A_a29_a_ASDATA_driver : std_logic;
SIGNAL A_a29_a_ENA_driver : std_logic;
SIGNAL HI_a31_DATAA_driver : std_logic;
SIGNAL HI_a31_DATAB_driver : std_logic;
SIGNAL HI_a31_DATAC_driver : std_logic;
SIGNAL HI_a31_DATAD_driver : std_logic;
SIGNAL HI_a29_a_CLK_driver : std_logic;
SIGNAL HI_a29_a_D_driver : std_logic;
SIGNAL HI_a29_a_ENA_driver : std_logic;
SIGNAL data_out_a88_DATAA_driver : std_logic;
SIGNAL data_out_a88_DATAB_driver : std_logic;
SIGNAL data_out_a88_DATAC_driver : std_logic;
SIGNAL data_out_a88_DATAD_driver : std_logic;
SIGNAL data_out_a89_DATAA_driver : std_logic;
SIGNAL data_out_a89_DATAB_driver : std_logic;
SIGNAL data_out_a89_DATAC_driver : std_logic;
SIGNAL data_out_a89_DATAD_driver : std_logic;
SIGNAL PC_a31_DATAB_driver : std_logic;
SIGNAL PC_a31_DATAC_driver : std_logic;
SIGNAL PC_a31_DATAD_driver : std_logic;
SIGNAL PC_a29_a_CLK_driver : std_logic;
SIGNAL PC_a29_a_D_driver : std_logic;
SIGNAL PC_a29_a_SCLR_driver : std_logic;
SIGNAL PC_a29_a_ENA_driver : std_logic;
SIGNAL data_out_a90_DATAA_driver : std_logic;
SIGNAL data_out_a90_DATAB_driver : std_logic;
SIGNAL data_out_a90_DATAC_driver : std_logic;
SIGNAL data_out_a90_DATAD_driver : std_logic;
SIGNAL A_a31_DATAC_driver : std_logic;
SIGNAL A_a31_DATAD_driver : std_logic;
SIGNAL A_a30_a_CLK_driver : std_logic;
SIGNAL A_a30_a_ASDATA_driver : std_logic;
SIGNAL A_a30_a_ENA_driver : std_logic;
SIGNAL data_out_a91_DATAA_driver : std_logic;
SIGNAL data_out_a91_DATAB_driver : std_logic;
SIGNAL data_out_a91_DATAC_driver : std_logic;
SIGNAL data_out_a91_DATAD_driver : std_logic;
SIGNAL data_out_a92_DATAA_driver : std_logic;
SIGNAL data_out_a92_DATAB_driver : std_logic;
SIGNAL data_out_a92_DATAC_driver : std_logic;
SIGNAL data_out_a92_DATAD_driver : std_logic;
SIGNAL data_out_a93_DATAA_driver : std_logic;
SIGNAL data_out_a93_DATAB_driver : std_logic;
SIGNAL data_out_a93_DATAC_driver : std_logic;
SIGNAL data_out_a93_DATAD_driver : std_logic;
SIGNAL data_out_a94_DATAA_driver : std_logic;
SIGNAL data_out_a94_DATAB_driver : std_logic;
SIGNAL data_out_a94_DATAC_driver : std_logic;
SIGNAL data_out_a94_DATAD_driver : std_logic;
SIGNAL data_out_a95_DATAA_driver : std_logic;
SIGNAL data_out_a95_DATAB_driver : std_logic;
SIGNAL data_out_a95_DATAC_driver : std_logic;
SIGNAL data_out_a95_DATAD_driver : std_logic;
SIGNAL PC_a33_DATAA_driver : std_logic;
SIGNAL PC_a33_DATAC_driver : std_logic;
SIGNAL PC_a33_DATAD_driver : std_logic;
SIGNAL PC_a31_a_CLK_driver : std_logic;
SIGNAL PC_a31_a_D_driver : std_logic;
SIGNAL PC_a31_a_SCLR_driver : std_logic;
SIGNAL PC_a31_a_ENA_driver : std_logic;
SIGNAL data_out_a96_DATAA_driver : std_logic;
SIGNAL data_out_a96_DATAB_driver : std_logic;
SIGNAL data_out_a96_DATAC_driver : std_logic;
SIGNAL data_out_a96_DATAD_driver : std_logic;
SIGNAL mux_address_a1_a_ainput_I_driver : std_logic;
SIGNAL mux_address_a0_a_ainput_I_driver : std_logic;
SIGNAL O_n_a0_a_a0_DATAA_driver : std_logic;
SIGNAL O_n_a0_a_a0_DATAB_driver : std_logic;
SIGNAL O_n_a0_a_a0_DATAC_driver : std_logic;
SIGNAL O_n_a0_a_a0_DATAD_driver : std_logic;
SIGNAL O_n_a0_a_a1_DATAA_driver : std_logic;
SIGNAL O_n_a0_a_a1_DATAB_driver : std_logic;
SIGNAL O_n_a0_a_a1_DATAC_driver : std_logic;
SIGNAL O_n_a0_a_a1_DATAD_driver : std_logic;
SIGNAL O_n_a0_a_a2_DATAB_driver : std_logic;
SIGNAL O_n_a0_a_a2_DATAC_driver : std_logic;
SIGNAL O_n_a0_a_a2_DATAD_driver : std_logic;
SIGNAL O_n_a0_a_a3_DATAA_driver : std_logic;
SIGNAL O_n_a0_a_a3_DATAB_driver : std_logic;
SIGNAL O_n_a0_a_a3_DATAC_driver : std_logic;
SIGNAL O_n_a0_a_a3_DATAD_driver : std_logic;
SIGNAL O_a0_a_CLK_driver : std_logic;
SIGNAL O_a0_a_D_driver : std_logic;
SIGNAL O_a0_a_SCLR_driver : std_logic;
SIGNAL address_reg_a0_a_ainput_I_driver : std_logic;
SIGNAL address_a0_DATAA_driver : std_logic;
SIGNAL address_a0_DATAB_driver : std_logic;
SIGNAL address_a0_DATAC_driver : std_logic;
SIGNAL address_a0_DATAD_driver : std_logic;
SIGNAL address_a1_DATAA_driver : std_logic;
SIGNAL address_a1_DATAB_driver : std_logic;
SIGNAL address_a1_DATAC_driver : std_logic;
SIGNAL address_a1_DATAD_driver : std_logic;
SIGNAL O_a1_a_a0_DATAA_driver : std_logic;
SIGNAL O_a1_a_a0_DATAB_driver : std_logic;
SIGNAL O_a1_a_a0_DATAD_driver : std_logic;
SIGNAL O_a1_a_CLK_driver : std_logic;
SIGNAL O_a1_a_D_driver : std_logic;
SIGNAL O_a1_a_ASDATA_driver : std_logic;
SIGNAL O_a1_a_SCLR_driver : std_logic;
SIGNAL O_a1_a_SLOAD_driver : std_logic;
SIGNAL O_a1_a_ENA_driver : std_logic;
SIGNAL address_a2_DATAA_driver : std_logic;
SIGNAL address_a2_DATAB_driver : std_logic;
SIGNAL address_a2_DATAC_driver : std_logic;
SIGNAL address_a2_DATAD_driver : std_logic;
SIGNAL address_a3_DATAA_driver : std_logic;
SIGNAL address_a3_DATAB_driver : std_logic;
SIGNAL address_a3_DATAC_driver : std_logic;
SIGNAL address_a3_DATAD_driver : std_logic;
SIGNAL address_reg_a2_a_ainput_I_driver : std_logic;
SIGNAL address_a4_DATAA_driver : std_logic;
SIGNAL address_a4_DATAB_driver : std_logic;
SIGNAL address_a4_DATAC_driver : std_logic;
SIGNAL address_a4_DATAD_driver : std_logic;
SIGNAL address_a5_DATAA_driver : std_logic;
SIGNAL address_a5_DATAB_driver : std_logic;
SIGNAL address_a5_DATAC_driver : std_logic;
SIGNAL address_a5_DATAD_driver : std_logic;
SIGNAL address_reg_a3_a_ainput_I_driver : std_logic;
SIGNAL address_a6_DATAA_driver : std_logic;
SIGNAL address_a6_DATAB_driver : std_logic;
SIGNAL address_a6_DATAC_driver : std_logic;
SIGNAL address_a6_DATAD_driver : std_logic;
SIGNAL address_a7_DATAA_driver : std_logic;
SIGNAL address_a7_DATAB_driver : std_logic;
SIGNAL address_a7_DATAC_driver : std_logic;
SIGNAL address_a7_DATAD_driver : std_logic;
SIGNAL address_a8_DATAA_driver : std_logic;
SIGNAL address_a8_DATAB_driver : std_logic;
SIGNAL address_a8_DATAC_driver : std_logic;
SIGNAL address_a8_DATAD_driver : std_logic;
SIGNAL address_a9_DATAA_driver : std_logic;
SIGNAL address_a9_DATAB_driver : std_logic;
SIGNAL address_a9_DATAC_driver : std_logic;
SIGNAL address_a9_DATAD_driver : std_logic;
SIGNAL address_a10_DATAA_driver : std_logic;
SIGNAL address_a10_DATAB_driver : std_logic;
SIGNAL address_a10_DATAC_driver : std_logic;
SIGNAL address_a10_DATAD_driver : std_logic;
SIGNAL address_a11_DATAA_driver : std_logic;
SIGNAL address_a11_DATAB_driver : std_logic;
SIGNAL address_a11_DATAC_driver : std_logic;
SIGNAL address_a11_DATAD_driver : std_logic;
SIGNAL address_reg_a6_a_ainput_I_driver : std_logic;
SIGNAL address_a12_DATAA_driver : std_logic;
SIGNAL address_a12_DATAB_driver : std_logic;
SIGNAL address_a12_DATAC_driver : std_logic;
SIGNAL address_a12_DATAD_driver : std_logic;
SIGNAL address_a13_DATAA_driver : std_logic;
SIGNAL address_a13_DATAB_driver : std_logic;
SIGNAL address_a13_DATAC_driver : std_logic;
SIGNAL address_a13_DATAD_driver : std_logic;
SIGNAL address_reg_a7_a_ainput_I_driver : std_logic;
SIGNAL address_a14_DATAA_driver : std_logic;
SIGNAL address_a14_DATAB_driver : std_logic;
SIGNAL address_a14_DATAC_driver : std_logic;
SIGNAL address_a14_DATAD_driver : std_logic;
SIGNAL address_a15_DATAA_driver : std_logic;
SIGNAL address_a15_DATAB_driver : std_logic;
SIGNAL address_a15_DATAC_driver : std_logic;
SIGNAL address_a15_DATAD_driver : std_logic;
SIGNAL address_reg_a8_a_ainput_I_driver : std_logic;
SIGNAL address_a16_DATAA_driver : std_logic;
SIGNAL address_a16_DATAB_driver : std_logic;
SIGNAL address_a16_DATAC_driver : std_logic;
SIGNAL address_a16_DATAD_driver : std_logic;
SIGNAL address_a17_DATAA_driver : std_logic;
SIGNAL address_a17_DATAB_driver : std_logic;
SIGNAL address_a17_DATAC_driver : std_logic;
SIGNAL address_a17_DATAD_driver : std_logic;
SIGNAL address_a18_DATAA_driver : std_logic;
SIGNAL address_a18_DATAB_driver : std_logic;
SIGNAL address_a18_DATAC_driver : std_logic;
SIGNAL address_a18_DATAD_driver : std_logic;
SIGNAL address_a19_DATAA_driver : std_logic;
SIGNAL address_a19_DATAB_driver : std_logic;
SIGNAL address_a19_DATAC_driver : std_logic;
SIGNAL address_a19_DATAD_driver : std_logic;
SIGNAL address_reg_a10_a_ainput_I_driver : std_logic;
SIGNAL address_a20_DATAA_driver : std_logic;
SIGNAL address_a20_DATAB_driver : std_logic;
SIGNAL address_a20_DATAC_driver : std_logic;
SIGNAL address_a20_DATAD_driver : std_logic;
SIGNAL address_a21_DATAA_driver : std_logic;
SIGNAL address_a21_DATAB_driver : std_logic;
SIGNAL address_a21_DATAC_driver : std_logic;
SIGNAL address_a21_DATAD_driver : std_logic;
SIGNAL address_reg_a11_a_ainput_I_driver : std_logic;
SIGNAL address_a22_DATAA_driver : std_logic;
SIGNAL address_a22_DATAB_driver : std_logic;
SIGNAL address_a22_DATAC_driver : std_logic;
SIGNAL address_a22_DATAD_driver : std_logic;
SIGNAL address_a23_DATAA_driver : std_logic;
SIGNAL address_a23_DATAB_driver : std_logic;
SIGNAL address_a23_DATAC_driver : std_logic;
SIGNAL address_a23_DATAD_driver : std_logic;
SIGNAL address_reg_a12_a_ainput_I_driver : std_logic;
SIGNAL address_a24_DATAA_driver : std_logic;
SIGNAL address_a24_DATAB_driver : std_logic;
SIGNAL address_a24_DATAC_driver : std_logic;
SIGNAL address_a24_DATAD_driver : std_logic;
SIGNAL address_a25_DATAA_driver : std_logic;
SIGNAL address_a25_DATAB_driver : std_logic;
SIGNAL address_a25_DATAC_driver : std_logic;
SIGNAL address_a25_DATAD_driver : std_logic;
SIGNAL address_reg_a13_a_ainput_I_driver : std_logic;
SIGNAL address_a26_DATAA_driver : std_logic;
SIGNAL address_a26_DATAB_driver : std_logic;
SIGNAL address_a26_DATAC_driver : std_logic;
SIGNAL address_a26_DATAD_driver : std_logic;
SIGNAL address_a27_DATAA_driver : std_logic;
SIGNAL address_a27_DATAB_driver : std_logic;
SIGNAL address_a27_DATAC_driver : std_logic;
SIGNAL address_a27_DATAD_driver : std_logic;
SIGNAL address_reg_a14_a_ainput_I_driver : std_logic;
SIGNAL address_a28_DATAA_driver : std_logic;
SIGNAL address_a28_DATAB_driver : std_logic;
SIGNAL address_a28_DATAC_driver : std_logic;
SIGNAL address_a28_DATAD_driver : std_logic;
SIGNAL address_a29_DATAA_driver : std_logic;
SIGNAL address_a29_DATAB_driver : std_logic;
SIGNAL address_a29_DATAC_driver : std_logic;
SIGNAL address_a29_DATAD_driver : std_logic;
SIGNAL address_a30_DATAA_driver : std_logic;
SIGNAL address_a30_DATAB_driver : std_logic;
SIGNAL address_a30_DATAC_driver : std_logic;
SIGNAL address_a30_DATAD_driver : std_logic;
SIGNAL address_a31_DATAA_driver : std_logic;
SIGNAL address_a31_DATAB_driver : std_logic;
SIGNAL address_a31_DATAC_driver : std_logic;
SIGNAL address_a31_DATAD_driver : std_logic;
SIGNAL address_reg_a16_a_ainput_I_driver : std_logic;
SIGNAL address_a32_DATAA_driver : std_logic;
SIGNAL address_a32_DATAB_driver : std_logic;
SIGNAL address_a32_DATAC_driver : std_logic;
SIGNAL address_a32_DATAD_driver : std_logic;
SIGNAL address_a33_DATAA_driver : std_logic;
SIGNAL address_a33_DATAB_driver : std_logic;
SIGNAL address_a33_DATAC_driver : std_logic;
SIGNAL address_a33_DATAD_driver : std_logic;
SIGNAL address_reg_a17_a_ainput_I_driver : std_logic;
SIGNAL address_a34_DATAA_driver : std_logic;
SIGNAL address_a34_DATAB_driver : std_logic;
SIGNAL address_a34_DATAC_driver : std_logic;
SIGNAL address_a34_DATAD_driver : std_logic;
SIGNAL address_a35_DATAA_driver : std_logic;
SIGNAL address_a35_DATAB_driver : std_logic;
SIGNAL address_a35_DATAC_driver : std_logic;
SIGNAL address_a35_DATAD_driver : std_logic;
SIGNAL address_reg_a18_a_ainput_I_driver : std_logic;
SIGNAL address_a36_DATAA_driver : std_logic;
SIGNAL address_a36_DATAB_driver : std_logic;
SIGNAL address_a36_DATAC_driver : std_logic;
SIGNAL address_a36_DATAD_driver : std_logic;
SIGNAL address_a37_DATAA_driver : std_logic;
SIGNAL address_a37_DATAB_driver : std_logic;
SIGNAL address_a37_DATAC_driver : std_logic;
SIGNAL address_a37_DATAD_driver : std_logic;
SIGNAL address_reg_a19_a_ainput_I_driver : std_logic;
SIGNAL address_a38_DATAA_driver : std_logic;
SIGNAL address_a38_DATAB_driver : std_logic;
SIGNAL address_a38_DATAC_driver : std_logic;
SIGNAL address_a38_DATAD_driver : std_logic;
SIGNAL address_a39_DATAA_driver : std_logic;
SIGNAL address_a39_DATAB_driver : std_logic;
SIGNAL address_a39_DATAC_driver : std_logic;
SIGNAL address_a39_DATAD_driver : std_logic;
SIGNAL address_reg_a20_a_ainput_I_driver : std_logic;
SIGNAL address_a40_DATAA_driver : std_logic;
SIGNAL address_a40_DATAB_driver : std_logic;
SIGNAL address_a40_DATAC_driver : std_logic;
SIGNAL address_a40_DATAD_driver : std_logic;
SIGNAL address_a41_DATAA_driver : std_logic;
SIGNAL address_a41_DATAB_driver : std_logic;
SIGNAL address_a41_DATAC_driver : std_logic;
SIGNAL address_a41_DATAD_driver : std_logic;
SIGNAL address_a42_DATAA_driver : std_logic;
SIGNAL address_a42_DATAB_driver : std_logic;
SIGNAL address_a42_DATAC_driver : std_logic;
SIGNAL address_a42_DATAD_driver : std_logic;
SIGNAL address_a43_DATAA_driver : std_logic;
SIGNAL address_a43_DATAB_driver : std_logic;
SIGNAL address_a43_DATAC_driver : std_logic;
SIGNAL address_a43_DATAD_driver : std_logic;
SIGNAL address_a44_DATAA_driver : std_logic;
SIGNAL address_a44_DATAB_driver : std_logic;
SIGNAL address_a44_DATAC_driver : std_logic;
SIGNAL address_a44_DATAD_driver : std_logic;
SIGNAL address_a45_DATAA_driver : std_logic;
SIGNAL address_a45_DATAB_driver : std_logic;
SIGNAL address_a45_DATAC_driver : std_logic;
SIGNAL address_a45_DATAD_driver : std_logic;
SIGNAL address_a46_DATAA_driver : std_logic;
SIGNAL address_a46_DATAB_driver : std_logic;
SIGNAL address_a46_DATAC_driver : std_logic;
SIGNAL address_a46_DATAD_driver : std_logic;
SIGNAL address_a47_DATAA_driver : std_logic;
SIGNAL address_a47_DATAB_driver : std_logic;
SIGNAL address_a47_DATAC_driver : std_logic;
SIGNAL address_a47_DATAD_driver : std_logic;
SIGNAL address_reg_a24_a_ainput_I_driver : std_logic;
SIGNAL address_a48_DATAA_driver : std_logic;
SIGNAL address_a48_DATAB_driver : std_logic;
SIGNAL address_a48_DATAC_driver : std_logic;
SIGNAL address_a48_DATAD_driver : std_logic;
SIGNAL address_a49_DATAA_driver : std_logic;
SIGNAL address_a49_DATAB_driver : std_logic;
SIGNAL address_a49_DATAC_driver : std_logic;
SIGNAL address_a49_DATAD_driver : std_logic;
SIGNAL address_reg_a25_a_ainput_I_driver : std_logic;
SIGNAL address_a50_DATAA_driver : std_logic;
SIGNAL address_a50_DATAB_driver : std_logic;
SIGNAL address_a50_DATAC_driver : std_logic;
SIGNAL address_a50_DATAD_driver : std_logic;
SIGNAL address_a51_DATAA_driver : std_logic;
SIGNAL address_a51_DATAB_driver : std_logic;
SIGNAL address_a51_DATAC_driver : std_logic;
SIGNAL address_a51_DATAD_driver : std_logic;
SIGNAL address_reg_a26_a_ainput_I_driver : std_logic;
SIGNAL address_a52_DATAA_driver : std_logic;
SIGNAL address_a52_DATAB_driver : std_logic;
SIGNAL address_a52_DATAC_driver : std_logic;
SIGNAL address_a52_DATAD_driver : std_logic;
SIGNAL address_a53_DATAA_driver : std_logic;
SIGNAL address_a53_DATAB_driver : std_logic;
SIGNAL address_a53_DATAC_driver : std_logic;
SIGNAL address_a53_DATAD_driver : std_logic;
SIGNAL address_reg_a27_a_ainput_I_driver : std_logic;
SIGNAL address_a54_DATAA_driver : std_logic;
SIGNAL address_a54_DATAB_driver : std_logic;
SIGNAL address_a54_DATAC_driver : std_logic;
SIGNAL address_a54_DATAD_driver : std_logic;
SIGNAL address_a55_DATAA_driver : std_logic;
SIGNAL address_a55_DATAB_driver : std_logic;
SIGNAL address_a55_DATAC_driver : std_logic;
SIGNAL address_a55_DATAD_driver : std_logic;
SIGNAL address_reg_a28_a_ainput_I_driver : std_logic;
SIGNAL address_a56_DATAA_driver : std_logic;
SIGNAL address_a56_DATAB_driver : std_logic;
SIGNAL address_a56_DATAC_driver : std_logic;
SIGNAL address_a56_DATAD_driver : std_logic;
SIGNAL address_a57_DATAA_driver : std_logic;
SIGNAL address_a57_DATAB_driver : std_logic;
SIGNAL address_a57_DATAC_driver : std_logic;
SIGNAL address_a57_DATAD_driver : std_logic;
SIGNAL address_reg_a29_a_ainput_I_driver : std_logic;
SIGNAL address_a58_DATAA_driver : std_logic;
SIGNAL address_a58_DATAB_driver : std_logic;
SIGNAL address_a58_DATAC_driver : std_logic;
SIGNAL address_a58_DATAD_driver : std_logic;
SIGNAL address_a59_DATAA_driver : std_logic;
SIGNAL address_a59_DATAB_driver : std_logic;
SIGNAL address_a59_DATAC_driver : std_logic;
SIGNAL address_a59_DATAD_driver : std_logic;
SIGNAL address_reg_a30_a_ainput_I_driver : std_logic;
SIGNAL address_a60_DATAA_driver : std_logic;
SIGNAL address_a60_DATAB_driver : std_logic;
SIGNAL address_a60_DATAC_driver : std_logic;
SIGNAL address_a60_DATAD_driver : std_logic;
SIGNAL address_a61_DATAA_driver : std_logic;
SIGNAL address_a61_DATAB_driver : std_logic;
SIGNAL address_a61_DATAC_driver : std_logic;
SIGNAL address_a61_DATAD_driver : std_logic;
SIGNAL address_reg_a31_a_ainput_I_driver : std_logic;
SIGNAL address_a62_DATAA_driver : std_logic;
SIGNAL address_a62_DATAB_driver : std_logic;
SIGNAL address_a62_DATAC_driver : std_logic;
SIGNAL address_a62_DATAD_driver : std_logic;
SIGNAL address_a63_DATAA_driver : std_logic;
SIGNAL address_a63_DATAB_driver : std_logic;
SIGNAL address_a63_DATAC_driver : std_logic;
SIGNAL address_a63_DATAD_driver : std_logic;
SIGNAL A_a4_a_afeeder_DATAD_driver : std_logic;
SIGNAL A_a4_a_CLK_driver : std_logic;
SIGNAL A_a4_a_D_driver : std_logic;
SIGNAL A_a4_a_ENA_driver : std_logic;
SIGNAL Equal30_a6_DATAA_driver : std_logic;
SIGNAL Equal30_a6_DATAB_driver : std_logic;
SIGNAL Equal30_a6_DATAC_driver : std_logic;
SIGNAL Equal30_a6_DATAD_driver : std_logic;
SIGNAL A_a8_a_CLK_driver : std_logic;
SIGNAL A_a8_a_ASDATA_driver : std_logic;
SIGNAL A_a8_a_ENA_driver : std_logic;
SIGNAL A_a7_a_CLK_driver : std_logic;
SIGNAL A_a7_a_ASDATA_driver : std_logic;
SIGNAL A_a7_a_ENA_driver : std_logic;
SIGNAL Equal30_a5_DATAA_driver : std_logic;
SIGNAL Equal30_a5_DATAB_driver : std_logic;
SIGNAL Equal30_a5_DATAC_driver : std_logic;
SIGNAL Equal30_a5_DATAD_driver : std_logic;
SIGNAL Equal30_a7_DATAA_driver : std_logic;
SIGNAL Equal30_a7_DATAB_driver : std_logic;
SIGNAL Equal30_a7_DATAC_driver : std_logic;
SIGNAL Equal30_a7_DATAD_driver : std_logic;
SIGNAL Equal30_a9_DATAA_driver : std_logic;
SIGNAL Equal30_a9_DATAB_driver : std_logic;
SIGNAL Equal30_a9_DATAC_driver : std_logic;
SIGNAL Equal30_a9_DATAD_driver : std_logic;
SIGNAL Equal30_a3_DATAA_driver : std_logic;
SIGNAL Equal30_a3_DATAB_driver : std_logic;
SIGNAL Equal30_a3_DATAC_driver : std_logic;
SIGNAL Equal30_a3_DATAD_driver : std_logic;
SIGNAL A_a18_a_CLK_driver : std_logic;
SIGNAL A_a18_a_ASDATA_driver : std_logic;
SIGNAL A_a18_a_ENA_driver : std_logic;
SIGNAL Equal30_a2_DATAB_driver : std_logic;
SIGNAL Equal30_a2_DATAC_driver : std_logic;
SIGNAL Equal30_a4_DATAA_driver : std_logic;
SIGNAL Equal30_a4_DATAB_driver : std_logic;
SIGNAL Equal30_a4_DATAC_driver : std_logic;
SIGNAL Equal30_a4_DATAD_driver : std_logic;
SIGNAL Equal30_a0_DATAA_driver : std_logic;
SIGNAL Equal30_a0_DATAB_driver : std_logic;
SIGNAL Equal30_a0_DATAC_driver : std_logic;
SIGNAL Equal30_a0_DATAD_driver : std_logic;
SIGNAL Equal30_a1_DATAA_driver : std_logic;
SIGNAL Equal30_a1_DATAB_driver : std_logic;
SIGNAL Equal30_a1_DATAC_driver : std_logic;
SIGNAL Equal30_a1_DATAD_driver : std_logic;
SIGNAL Equal30_a10_DATAA_driver : std_logic;
SIGNAL Equal30_a10_DATAB_driver : std_logic;
SIGNAL Equal30_a10_DATAC_driver : std_logic;
SIGNAL Equal30_a10_DATAD_driver : std_logic;

BEGIN

ww_clk <= clk;
ww_reset <= reset;
ww_data_in <= data_in;
data_out <= ww_data_out;
address <= ww_address;
ww_mux_hi <= mux_hi;
ww_mux_lo <= mux_lo;
ww_mux_pc <= mux_pc;
ww_mux_out <= mux_out;
ww_mux_a <= mux_a;
ww_mux_b <= mux_b;
ww_mux_data_out <= mux_data_out;
ww_mux_address <= mux_address;
ww_mux_alu_left <= mux_alu_left;
ww_mux_alu_right <= mux_alu_right;
ww_alu_add_sub <= alu_add_sub;
ww_address_reg <= address_reg;
ww_intr_right <= intr_right;
ww_intr_pc <= intr_pc;
a_zero <= ww_a_zero;
a_msb <= ww_a_msb;
alu_c <= ww_alu_c;
alu_msb <= ww_alu_msb;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_mux_pc_a0_a_ainput_o <= NOT mux_pc_a0_a_ainput_o;
ALT_INV_Add0_a66_combout <= NOT Add0_a66_combout;

LO_a9_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a9_a_CLK_driver);

LO_a9_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a10_combout,
	dataout => LO_a9_a_D_driver);

LO_a9_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a9_a_SCLR_driver);

LO_a9_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a9_a_ENA_driver);

-- Location: FF_X41_Y27_N7
LO_a9_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a9_a_CLK_driver,
	d => LO_a9_a_D_driver,
	sclr => LO_a9_a_SCLR_driver,
	ena => LO_a9_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(9));

LO_a19_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a19_a_CLK_driver);

LO_a19_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a20_combout,
	dataout => LO_a19_a_D_driver);

LO_a19_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a19_a_SCLR_driver);

LO_a19_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a19_a_ENA_driver);

-- Location: FF_X35_Y28_N9
LO_a19_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a19_a_CLK_driver,
	d => LO_a19_a_D_driver,
	sclr => LO_a19_a_SCLR_driver,
	ena => LO_a19_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(19));

LO_a20_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a20_a_CLK_driver);

LO_a20_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a21_combout,
	dataout => LO_a20_a_D_driver);

LO_a20_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a20_a_SCLR_driver);

LO_a20_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a20_a_ENA_driver);

-- Location: FF_X35_Y28_N23
LO_a20_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a20_a_CLK_driver,
	d => LO_a20_a_D_driver,
	sclr => LO_a20_a_SCLR_driver,
	ena => LO_a20_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(20));

data_out_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(0),
	dataout => data_out_a1_DATAA_driver);

data_out_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a1_DATAB_driver);

data_out_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a1_DATAC_driver);

data_out_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(0),
	dataout => data_out_a1_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N12
data_out_a1 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a1_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o & ((A(0)))) # (!mux_data_out_a0_a_ainput_o & (O(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a1_DATAA_driver,
	datab => data_out_a1_DATAB_driver,
	datac => data_out_a1_DATAC_driver,
	datad => data_out_a1_DATAD_driver,
	combout => data_out_a1_combout);

data_out_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a4_DATAA_driver);

data_out_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a4_DATAB_driver);

data_out_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(1),
	dataout => data_out_a4_DATAC_driver);

data_out_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(1),
	dataout => data_out_a4_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N6
data_out_a4 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a4_combout = (mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o) # ((HI(1))))) # (!mux_data_out_a1_a_ainput_o & (!mux_data_out_a0_a_ainput_o & ((O(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a4_DATAA_driver,
	datab => data_out_a4_DATAB_driver,
	datac => data_out_a4_DATAC_driver,
	datad => data_out_a4_DATAD_driver,
	combout => data_out_a4_combout);

HI_a3_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a3_a_CLK_driver);

HI_a3_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a5_combout,
	dataout => HI_a3_a_D_driver);

HI_a3_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a3_a_ENA_driver);

-- Location: FF_X36_Y29_N11
HI_a3_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a3_a_CLK_driver,
	d => HI_a3_a_D_driver,
	ena => HI_a3_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(3));

data_out_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a10_DATAA_driver);

data_out_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(3),
	dataout => data_out_a10_DATAB_driver);

data_out_a10_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a10_DATAC_driver);

data_out_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(3),
	dataout => data_out_a10_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N26
data_out_a10 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a10_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o) # (HI(3))))) # (!mux_data_out_a1_a_ainput_o & (O(3) & (!mux_data_out_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a10_DATAA_driver,
	datab => data_out_a10_DATAB_driver,
	datac => data_out_a10_DATAC_driver,
	datad => data_out_a10_DATAD_driver,
	combout => data_out_a10_combout);

data_out_a13_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(4),
	dataout => data_out_a13_DATAA_driver);

data_out_a13_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a13_DATAB_driver);

data_out_a13_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a13_DATAC_driver);

data_out_a13_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(4),
	dataout => data_out_a13_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N6
data_out_a13 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a13_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o) # (A(4))))) # (!mux_data_out_a0_a_ainput_o & (O(4) & (!mux_data_out_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a13_DATAA_driver,
	datab => data_out_a13_DATAB_driver,
	datac => data_out_a13_DATAC_driver,
	datad => data_out_a13_DATAD_driver,
	combout => data_out_a13_combout);

data_out_a25_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(8),
	dataout => data_out_a25_DATAA_driver);

data_out_a25_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a25_DATAB_driver);

data_out_a25_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a25_DATAC_driver);

data_out_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(8),
	dataout => data_out_a25_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N0
data_out_a25 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a25_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o & ((A(8)))) # (!mux_data_out_a0_a_ainput_o & (O(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a25_DATAA_driver,
	datab => data_out_a25_DATAB_driver,
	datac => data_out_a25_DATAC_driver,
	datad => data_out_a25_DATAD_driver,
	combout => data_out_a25_combout);

data_out_a31_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a31_DATAA_driver);

data_out_a31_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a31_DATAB_driver);

data_out_a31_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(10),
	dataout => data_out_a31_DATAC_driver);

data_out_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(10),
	dataout => data_out_a31_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N6
data_out_a31 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a31_combout = (mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o) # ((A(10))))) # (!mux_data_out_a0_a_ainput_o & (!mux_data_out_a1_a_ainput_o & (O(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a31_DATAA_driver,
	datab => data_out_a31_DATAB_driver,
	datac => data_out_a31_DATAC_driver,
	datad => data_out_a31_DATAD_driver,
	combout => data_out_a31_combout);

data_out_a40_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(13),
	dataout => data_out_a40_DATAA_driver);

data_out_a40_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a40_DATAB_driver);

data_out_a40_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a40_DATAC_driver);

data_out_a40_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(13),
	dataout => data_out_a40_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N22
data_out_a40 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a40_combout = (mux_data_out_a1_a_ainput_o & ((HI(13)) # ((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & (((!mux_data_out_a0_a_ainput_o & O(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a40_DATAA_driver,
	datab => data_out_a40_DATAB_driver,
	datac => data_out_a40_DATAC_driver,
	datad => data_out_a40_DATAD_driver,
	combout => data_out_a40_combout);

data_out_a46_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(15),
	dataout => data_out_a46_DATAA_driver);

data_out_a46_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a46_DATAB_driver);

data_out_a46_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a46_DATAC_driver);

data_out_a46_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(15),
	dataout => data_out_a46_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N16
data_out_a46 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a46_combout = (mux_data_out_a1_a_ainput_o & ((HI(15)) # ((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & (((!mux_data_out_a0_a_ainput_o & O(15)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a46_DATAA_driver,
	datab => data_out_a46_DATAB_driver,
	datac => data_out_a46_DATAC_driver,
	datad => data_out_a46_DATAD_driver,
	combout => data_out_a46_combout);

data_out_a55_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(18),
	dataout => data_out_a55_DATAA_driver);

data_out_a55_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(18),
	dataout => data_out_a55_DATAB_driver);

data_out_a55_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a55_DATAC_driver);

data_out_a55_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a55_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N10
data_out_a55 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a55_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o & (A(18))) # (!mux_data_out_a0_a_ainput_o & ((O(18))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a55_DATAA_driver,
	datab => data_out_a55_DATAB_driver,
	datac => data_out_a55_DATAC_driver,
	datad => data_out_a55_DATAD_driver,
	combout => data_out_a55_combout);

data_out_a70_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(23),
	dataout => data_out_a70_DATAA_driver);

data_out_a70_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(23),
	dataout => data_out_a70_DATAB_driver);

data_out_a70_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a70_DATAC_driver);

data_out_a70_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a70_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N28
data_out_a70 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a70_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o & ((HI(23)))) # (!mux_data_out_a1_a_ainput_o & (O(23)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a70_DATAA_driver,
	datab => data_out_a70_DATAB_driver,
	datac => data_out_a70_DATAC_driver,
	datad => data_out_a70_DATAD_driver,
	combout => data_out_a70_combout);

data_out_a73_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(24),
	dataout => data_out_a73_DATAA_driver);

data_out_a73_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a73_DATAB_driver);

data_out_a73_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(24),
	dataout => data_out_a73_DATAC_driver);

data_out_a73_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a73_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N6
data_out_a73 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a73_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o & (A(24))) # (!mux_data_out_a0_a_ainput_o & ((O(24))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a73_DATAA_driver,
	datab => data_out_a73_DATAB_driver,
	datac => data_out_a73_DATAC_driver,
	datad => data_out_a73_DATAD_driver,
	combout => data_out_a73_combout);

data_out_a76_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a76_DATAA_driver);

data_out_a76_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a76_DATAB_driver);

data_out_a76_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(25),
	dataout => data_out_a76_DATAC_driver);

data_out_a76_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(25),
	dataout => data_out_a76_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N12
data_out_a76 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a76_combout = (mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o) # ((HI(25))))) # (!mux_data_out_a1_a_ainput_o & (!mux_data_out_a0_a_ainput_o & (O(25))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a76_DATAA_driver,
	datab => data_out_a76_DATAB_driver,
	datac => data_out_a76_DATAC_driver,
	datad => data_out_a76_DATAD_driver,
	combout => data_out_a76_combout);

A_a28_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a28_a_CLK_driver);

A_a28_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a29_combout,
	dataout => A_a28_a_ASDATA_driver);

A_a28_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a28_a_ENA_driver);

-- Location: FF_X38_Y27_N31
A_a28_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a28_a_CLK_driver,
	asdata => A_a28_a_ASDATA_driver,
	sload => VCC,
	ena => A_a28_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(28));

data_out_a85_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(28),
	dataout => data_out_a85_DATAA_driver);

data_out_a85_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(28),
	dataout => data_out_a85_DATAB_driver);

data_out_a85_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a85_DATAC_driver);

data_out_a85_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a85_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N14
data_out_a85 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a85_combout = (mux_data_out_a0_a_ainput_o & (((A(28)) # (mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & (O(28) & ((!mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a85_DATAA_driver,
	datab => data_out_a85_DATAB_driver,
	datac => data_out_a85_DATAC_driver,
	datad => data_out_a85_DATAD_driver,
	combout => data_out_a85_combout);

Equal30_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(30),
	dataout => Equal30_a8_DATAA_driver);

Equal30_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(29),
	dataout => Equal30_a8_DATAB_driver);

Equal30_a8_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(31),
	dataout => Equal30_a8_DATAC_driver);

Equal30_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(28),
	dataout => Equal30_a8_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N18
Equal30_a8 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a8_combout = (!A(30) & (!A(29) & (!A(31) & !A(28))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a8_DATAA_driver,
	datab => Equal30_a8_DATAB_driver,
	datac => Equal30_a8_DATAC_driver,
	datad => Equal30_a8_DATAD_driver,
	combout => Equal30_a8_combout);

B_a31_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a31_a_CLK_driver);

B_a31_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a32_combout,
	dataout => B_a31_a_ASDATA_driver);

B_a31_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a31_a_ENA_driver);

-- Location: FF_X37_Y24_N5
B_a31_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a31_a_CLK_driver,
	asdata => B_a31_a_ASDATA_driver,
	sload => VCC,
	ena => B_a31_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(31));

B_a30_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a30_a_CLK_driver);

B_a30_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a31_combout,
	dataout => B_a30_a_ASDATA_driver);

B_a30_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a30_a_ENA_driver);

-- Location: FF_X36_Y25_N17
B_a30_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a30_a_CLK_driver,
	asdata => B_a30_a_ASDATA_driver,
	sload => VCC,
	ena => B_a30_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(30));

left_a30_a_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a30_a_a1_DATAA_driver);

left_a30_a_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a30_a_a1_DATAB_driver);

left_a30_a_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(30),
	dataout => left_a30_a_a1_DATAC_driver);

left_a30_a_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(29),
	dataout => left_a30_a_a1_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N26
left_a30_a_a1 : cycloneive_lcell_comb
-- Equation(s):
-- left_a30_a_a1_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(29))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a30_a_a1_DATAA_driver,
	datab => left_a30_a_a1_DATAB_driver,
	datac => left_a30_a_a1_DATAC_driver,
	datad => left_a30_a_a1_DATAD_driver,
	combout => left_a30_a_a1_combout);

left_a29_a_a2_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a29_a_a2_DATAA_driver);

left_a29_a_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a29_a_a2_DATAB_driver);

left_a29_a_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(29),
	dataout => left_a29_a_a2_DATAC_driver);

left_a29_a_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(28),
	dataout => left_a29_a_a2_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N22
left_a29_a_a2 : cycloneive_lcell_comb
-- Equation(s):
-- left_a29_a_a2_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(28))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(29))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a29_a_a2_DATAA_driver,
	datab => left_a29_a_a2_DATAB_driver,
	datac => left_a29_a_a2_DATAC_driver,
	datad => left_a29_a_a2_DATAD_driver,
	combout => left_a29_a_a2_combout);

left_a29_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(29),
	dataout => left_a29_a_DATAA_driver);

left_a29_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a29_a_DATAB_driver);

left_a29_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(29),
	dataout => left_a29_a_DATAC_driver);

left_a29_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a29_a_a2_combout,
	dataout => left_a29_a_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N2
left_a29_a : cycloneive_lcell_comb
-- Equation(s):
-- left(29) = (mux_alu_left_a0_a_ainput_o & ((left_a29_a_a2_combout & ((HI(29)))) # (!left_a29_a_a2_combout & (PC(29))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a29_a_a2_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a29_a_DATAA_driver,
	datab => left_a29_a_DATAB_driver,
	datac => left_a29_a_DATAC_driver,
	datad => left_a29_a_DATAD_driver,
	combout => left(29));

B_a28_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a28_a_CLK_driver);

B_a28_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a29_combout,
	dataout => B_a28_a_ASDATA_driver);

B_a28_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a28_a_ENA_driver);

-- Location: FF_X37_Y24_N9
B_a28_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a28_a_CLK_driver,
	asdata => B_a28_a_ASDATA_driver,
	sload => VCC,
	ena => B_a28_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(28));

left_a28_a_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a28_a_a3_DATAA_driver);

left_a28_a_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a28_a_a3_DATAB_driver);

left_a28_a_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(28),
	dataout => left_a28_a_a3_DATAC_driver);

left_a28_a_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(27),
	dataout => left_a28_a_a3_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N30
left_a28_a_a3 : cycloneive_lcell_comb
-- Equation(s):
-- left_a28_a_a3_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(27))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(28))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a28_a_a3_DATAA_driver,
	datab => left_a28_a_a3_DATAB_driver,
	datac => left_a28_a_a3_DATAC_driver,
	datad => left_a28_a_a3_DATAD_driver,
	combout => left_a28_a_a3_combout);

B_a27_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a27_a_CLK_driver);

B_a27_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a28_combout,
	dataout => B_a27_a_ASDATA_driver);

B_a27_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a27_a_ENA_driver);

-- Location: FF_X37_Y24_N31
B_a27_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a27_a_CLK_driver,
	asdata => B_a27_a_ASDATA_driver,
	sload => VCC,
	ena => B_a27_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(27));

B_a26_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a26_a_CLK_driver);

B_a26_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a27_combout,
	dataout => B_a26_a_ASDATA_driver);

B_a26_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a26_a_ENA_driver);

-- Location: FF_X36_Y25_N3
B_a26_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a26_a_CLK_driver,
	asdata => B_a26_a_ASDATA_driver,
	sload => VCC,
	ena => B_a26_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(26));

B_a25_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a25_a_CLK_driver);

B_a25_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a26_combout,
	dataout => B_a25_a_ASDATA_driver);

B_a25_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a25_a_ENA_driver);

-- Location: FF_X42_Y27_N1
B_a25_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a25_a_CLK_driver,
	asdata => B_a25_a_ASDATA_driver,
	sload => VCC,
	ena => B_a25_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(25));

B_a24_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a24_a_CLK_driver);

B_a24_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a25_combout,
	dataout => B_a24_a_ASDATA_driver);

B_a24_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a24_a_ENA_driver);

-- Location: FF_X38_Y25_N13
B_a24_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a24_a_CLK_driver,
	asdata => B_a24_a_ASDATA_driver,
	sload => VCC,
	ena => B_a24_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(24));

left_a23_a_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a23_a_a8_DATAA_driver);

left_a23_a_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a23_a_a8_DATAB_driver);

left_a23_a_a8_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(23),
	dataout => left_a23_a_a8_DATAC_driver);

left_a23_a_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(22),
	dataout => left_a23_a_a8_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N22
left_a23_a_a8 : cycloneive_lcell_comb
-- Equation(s):
-- left_a23_a_a8_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(22))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a23_a_a8_DATAA_driver,
	datab => left_a23_a_a8_DATAB_driver,
	datac => left_a23_a_a8_DATAC_driver,
	datad => left_a23_a_a8_DATAD_driver,
	combout => left_a23_a_a8_combout);

left_a23_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a23_a_a8_combout,
	dataout => left_a23_a_DATAA_driver);

left_a23_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a23_a_DATAB_driver);

left_a23_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(23),
	dataout => left_a23_a_DATAC_driver);

left_a23_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(23),
	dataout => left_a23_a_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N16
left_a23_a : cycloneive_lcell_comb
-- Equation(s):
-- left(23) = (left_a23_a_a8_combout & (((HI(23))) # (!mux_alu_left_a0_a_ainput_o))) # (!left_a23_a_a8_combout & (mux_alu_left_a0_a_ainput_o & (PC(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a23_a_DATAA_driver,
	datab => left_a23_a_DATAB_driver,
	datac => left_a23_a_DATAC_driver,
	datad => left_a23_a_DATAD_driver,
	combout => left(23));

left_a22_a_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a22_a_a9_DATAA_driver);

left_a22_a_a9_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a22_a_a9_DATAB_driver);

left_a22_a_a9_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(21),
	dataout => left_a22_a_a9_DATAC_driver);

left_a22_a_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(22),
	dataout => left_a22_a_a9_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N26
left_a22_a_a9 : cycloneive_lcell_comb
-- Equation(s):
-- left_a22_a_a9_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(21))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & ((A(22)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a22_a_a9_DATAA_driver,
	datab => left_a22_a_a9_DATAB_driver,
	datac => left_a22_a_a9_DATAC_driver,
	datad => left_a22_a_a9_DATAD_driver,
	combout => left_a22_a_a9_combout);

left_a22_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a22_a_a9_combout,
	dataout => left_a22_a_DATAA_driver);

left_a22_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a22_a_DATAB_driver);

left_a22_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(22),
	dataout => left_a22_a_DATAC_driver);

left_a22_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(22),
	dataout => left_a22_a_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N12
left_a22_a : cycloneive_lcell_comb
-- Equation(s):
-- left(22) = (left_a22_a_a9_combout & (((HI(22))) # (!mux_alu_left_a0_a_ainput_o))) # (!left_a22_a_a9_combout & (mux_alu_left_a0_a_ainput_o & (PC(22))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a22_a_DATAA_driver,
	datab => left_a22_a_DATAB_driver,
	datac => left_a22_a_DATAC_driver,
	datad => left_a22_a_DATAD_driver,
	combout => left(22));

left_a21_a_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a21_a_a10_DATAA_driver);

left_a21_a_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a21_a_a10_DATAB_driver);

left_a21_a_a10_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(21),
	dataout => left_a21_a_a10_DATAC_driver);

left_a21_a_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(20),
	dataout => left_a21_a_a10_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N20
left_a21_a_a10 : cycloneive_lcell_comb
-- Equation(s):
-- left_a21_a_a10_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(20))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a21_a_a10_DATAA_driver,
	datab => left_a21_a_a10_DATAB_driver,
	datac => left_a21_a_a10_DATAC_driver,
	datad => left_a21_a_a10_DATAD_driver,
	combout => left_a21_a_a10_combout);

left_a21_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(21),
	dataout => left_a21_a_DATAA_driver);

left_a21_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a21_a_DATAB_driver);

left_a21_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(21),
	dataout => left_a21_a_DATAC_driver);

left_a21_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a21_a_a10_combout,
	dataout => left_a21_a_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N10
left_a21_a : cycloneive_lcell_comb
-- Equation(s):
-- left(21) = (mux_alu_left_a0_a_ainput_o & ((left_a21_a_a10_combout & ((HI(21)))) # (!left_a21_a_a10_combout & (PC(21))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a21_a_a10_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a21_a_DATAA_driver,
	datab => left_a21_a_DATAB_driver,
	datac => left_a21_a_DATAC_driver,
	datad => left_a21_a_DATAD_driver,
	combout => left(21));

left_a20_a_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a20_a_a11_DATAA_driver);

left_a20_a_a11_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a20_a_a11_DATAB_driver);

left_a20_a_a11_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(20),
	dataout => left_a20_a_a11_DATAC_driver);

left_a20_a_a11_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(19),
	dataout => left_a20_a_a11_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N30
left_a20_a_a11 : cycloneive_lcell_comb
-- Equation(s):
-- left_a20_a_a11_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(19)))) # (!mux_alu_left_a1_a_ainput_o & (A(20)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a20_a_a11_DATAA_driver,
	datab => left_a20_a_a11_DATAB_driver,
	datac => left_a20_a_a11_DATAC_driver,
	datad => left_a20_a_a11_DATAD_driver,
	combout => left_a20_a_a11_combout);

left_a20_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a20_a_DATAA_driver);

left_a20_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(20),
	dataout => left_a20_a_DATAB_driver);

left_a20_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a20_a_a11_combout,
	dataout => left_a20_a_DATAC_driver);

left_a20_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(20),
	dataout => left_a20_a_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N6
left_a20_a : cycloneive_lcell_comb
-- Equation(s):
-- left(20) = (mux_alu_left_a0_a_ainput_o & ((left_a20_a_a11_combout & ((HI(20)))) # (!left_a20_a_a11_combout & (PC(20))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a20_a_a11_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a20_a_DATAA_driver,
	datab => left_a20_a_DATAB_driver,
	datac => left_a20_a_DATAC_driver,
	datad => left_a20_a_DATAD_driver,
	combout => left(20));

left_a19_a_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a19_a_a12_DATAA_driver);

left_a19_a_a12_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a19_a_a12_DATAB_driver);

left_a19_a_a12_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(19),
	dataout => left_a19_a_a12_DATAC_driver);

left_a19_a_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(18),
	dataout => left_a19_a_a12_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N2
left_a19_a_a12 : cycloneive_lcell_comb
-- Equation(s):
-- left_a19_a_a12_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(18))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a19_a_a12_DATAA_driver,
	datab => left_a19_a_a12_DATAB_driver,
	datac => left_a19_a_a12_DATAC_driver,
	datad => left_a19_a_a12_DATAD_driver,
	combout => left_a19_a_a12_combout);

left_a19_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(19),
	dataout => left_a19_a_DATAA_driver);

left_a19_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a19_a_DATAB_driver);

left_a19_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(19),
	dataout => left_a19_a_DATAC_driver);

left_a19_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a19_a_a12_combout,
	dataout => left_a19_a_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N28
left_a19_a : cycloneive_lcell_comb
-- Equation(s):
-- left(19) = (mux_alu_left_a0_a_ainput_o & ((left_a19_a_a12_combout & (HI(19))) # (!left_a19_a_a12_combout & ((PC(19)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a19_a_a12_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a19_a_DATAA_driver,
	datab => left_a19_a_DATAB_driver,
	datac => left_a19_a_DATAC_driver,
	datad => left_a19_a_DATAD_driver,
	combout => left(19));

left_a18_a_a13_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a18_a_a13_DATAA_driver);

left_a18_a_a13_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a18_a_a13_DATAB_driver);

left_a18_a_a13_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(18),
	dataout => left_a18_a_a13_DATAC_driver);

left_a18_a_a13_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(17),
	dataout => left_a18_a_a13_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N0
left_a18_a_a13 : cycloneive_lcell_comb
-- Equation(s):
-- left_a18_a_a13_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(17)))) # (!mux_alu_left_a1_a_ainput_o & (A(18)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a18_a_a13_DATAA_driver,
	datab => left_a18_a_a13_DATAB_driver,
	datac => left_a18_a_a13_DATAC_driver,
	datad => left_a18_a_a13_DATAD_driver,
	combout => left_a18_a_a13_combout);

left_a18_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a18_a_DATAA_driver);

left_a18_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(18),
	dataout => left_a18_a_DATAB_driver);

left_a18_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(18),
	dataout => left_a18_a_DATAC_driver);

left_a18_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a18_a_a13_combout,
	dataout => left_a18_a_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N20
left_a18_a : cycloneive_lcell_comb
-- Equation(s):
-- left(18) = (mux_alu_left_a0_a_ainput_o & ((left_a18_a_a13_combout & ((HI(18)))) # (!left_a18_a_a13_combout & (PC(18))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a18_a_a13_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a18_a_DATAA_driver,
	datab => left_a18_a_DATAB_driver,
	datac => left_a18_a_DATAC_driver,
	datad => left_a18_a_DATAD_driver,
	combout => left(18));

B_a17_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a17_a_CLK_driver);

B_a17_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a18_combout,
	dataout => B_a17_a_ASDATA_driver);

B_a17_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a17_a_ENA_driver);

-- Location: FF_X37_Y31_N9
B_a17_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a17_a_CLK_driver,
	asdata => B_a17_a_ASDATA_driver,
	sload => VCC,
	ena => B_a17_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(17));

B_a16_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a16_a_CLK_driver);

B_a16_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a17_combout,
	dataout => B_a16_a_ASDATA_driver);

B_a16_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a16_a_ENA_driver);

-- Location: FF_X37_Y31_N19
B_a16_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a16_a_CLK_driver,
	asdata => B_a16_a_ASDATA_driver,
	sload => VCC,
	ena => B_a16_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(16));

left_a15_a_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a15_a_a16_DATAA_driver);

left_a15_a_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a15_a_a16_DATAB_driver);

left_a15_a_a16_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(15),
	dataout => left_a15_a_a16_DATAC_driver);

left_a15_a_a16_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(14),
	dataout => left_a15_a_a16_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N28
left_a15_a_a16 : cycloneive_lcell_comb
-- Equation(s):
-- left_a15_a_a16_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(14))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a15_a_a16_DATAA_driver,
	datab => left_a15_a_a16_DATAB_driver,
	datac => left_a15_a_a16_DATAC_driver,
	datad => left_a15_a_a16_DATAD_driver,
	combout => left_a15_a_a16_combout);

left_a15_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a15_a_DATAA_driver);

left_a15_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(15),
	dataout => left_a15_a_DATAB_driver);

left_a15_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a15_a_a16_combout,
	dataout => left_a15_a_DATAC_driver);

left_a15_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(15),
	dataout => left_a15_a_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N2
left_a15_a : cycloneive_lcell_comb
-- Equation(s):
-- left(15) = (mux_alu_left_a0_a_ainput_o & ((left_a15_a_a16_combout & (HI(15))) # (!left_a15_a_a16_combout & ((PC(15)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a15_a_a16_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a15_a_DATAA_driver,
	datab => left_a15_a_DATAB_driver,
	datac => left_a15_a_DATAC_driver,
	datad => left_a15_a_DATAD_driver,
	combout => left(15));

B_a14_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a14_a_CLK_driver);

B_a14_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a15_combout,
	dataout => B_a14_a_ASDATA_driver);

B_a14_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a14_a_ENA_driver);

-- Location: FF_X41_Y28_N11
B_a14_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a14_a_CLK_driver,
	asdata => B_a14_a_ASDATA_driver,
	sload => VCC,
	ena => B_a14_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(14));

left_a14_a_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a14_a_a17_DATAA_driver);

left_a14_a_a17_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a14_a_a17_DATAB_driver);

left_a14_a_a17_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(14),
	dataout => left_a14_a_a17_DATAC_driver);

left_a14_a_a17_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(13),
	dataout => left_a14_a_a17_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N26
left_a14_a_a17 : cycloneive_lcell_comb
-- Equation(s):
-- left_a14_a_a17_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(13))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a14_a_a17_DATAA_driver,
	datab => left_a14_a_a17_DATAB_driver,
	datac => left_a14_a_a17_DATAC_driver,
	datad => left_a14_a_a17_DATAD_driver,
	combout => left_a14_a_a17_combout);

B_a13_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a13_a_CLK_driver);

B_a13_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a14_combout,
	dataout => B_a13_a_ASDATA_driver);

B_a13_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a13_a_ENA_driver);

-- Location: FF_X37_Y24_N21
B_a13_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a13_a_CLK_driver,
	asdata => B_a13_a_ASDATA_driver,
	sload => VCC,
	ena => B_a13_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(13));

B_a12_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a12_a_CLK_driver);

B_a12_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a13_combout,
	dataout => B_a12_a_ASDATA_driver);

B_a12_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a12_a_ENA_driver);

-- Location: FF_X37_Y24_N11
B_a12_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a12_a_CLK_driver,
	asdata => B_a12_a_ASDATA_driver,
	sload => VCC,
	ena => B_a12_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(12));

left_a11_a_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a11_a_a20_DATAA_driver);

left_a11_a_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a11_a_a20_DATAB_driver);

left_a11_a_a20_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(10),
	dataout => left_a11_a_a20_DATAC_driver);

left_a11_a_a20_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(11),
	dataout => left_a11_a_a20_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N22
left_a11_a_a20 : cycloneive_lcell_comb
-- Equation(s):
-- left_a11_a_a20_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & (HI(10))) # (!mux_alu_left_a1_a_ainput_o & ((A(11))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a11_a_a20_DATAA_driver,
	datab => left_a11_a_a20_DATAB_driver,
	datac => left_a11_a_a20_DATAC_driver,
	datad => left_a11_a_a20_DATAD_driver,
	combout => left_a11_a_a20_combout);

left_a11_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a11_a_DATAA_driver);

left_a11_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(11),
	dataout => left_a11_a_DATAB_driver);

left_a11_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a11_a_a20_combout,
	dataout => left_a11_a_DATAC_driver);

left_a11_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(11),
	dataout => left_a11_a_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N4
left_a11_a : cycloneive_lcell_comb
-- Equation(s):
-- left(11) = (mux_alu_left_a0_a_ainput_o & ((left_a11_a_a20_combout & (HI(11))) # (!left_a11_a_a20_combout & ((PC(11)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a11_a_a20_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a11_a_DATAA_driver,
	datab => left_a11_a_DATAB_driver,
	datac => left_a11_a_DATAC_driver,
	datad => left_a11_a_DATAD_driver,
	combout => left(11));

B_a10_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a10_a_CLK_driver);

B_a10_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a11_combout,
	dataout => B_a10_a_ASDATA_driver);

B_a10_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a10_a_ENA_driver);

-- Location: FF_X37_Y24_N3
B_a10_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a10_a_CLK_driver,
	asdata => B_a10_a_ASDATA_driver,
	sload => VCC,
	ena => B_a10_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(10));

B_a9_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a9_a_CLK_driver);

B_a9_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a10_combout,
	dataout => B_a9_a_ASDATA_driver);

B_a9_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a9_a_ENA_driver);

-- Location: FF_X38_Y25_N25
B_a9_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a9_a_CLK_driver,
	asdata => B_a9_a_ASDATA_driver,
	sload => VCC,
	ena => B_a9_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(9));

left_a8_a_a23_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a8_a_a23_DATAA_driver);

left_a8_a_a23_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a8_a_a23_DATAB_driver);

left_a8_a_a23_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(8),
	dataout => left_a8_a_a23_DATAC_driver);

left_a8_a_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(7),
	dataout => left_a8_a_a23_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N14
left_a8_a_a23 : cycloneive_lcell_comb
-- Equation(s):
-- left_a8_a_a23_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(7)))) # (!mux_alu_left_a1_a_ainput_o & (A(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a8_a_a23_DATAA_driver,
	datab => left_a8_a_a23_DATAB_driver,
	datac => left_a8_a_a23_DATAC_driver,
	datad => left_a8_a_a23_DATAD_driver,
	combout => left_a8_a_a23_combout);

left_a8_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a8_a_DATAA_driver);

left_a8_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(8),
	dataout => left_a8_a_DATAB_driver);

left_a8_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a8_a_a23_combout,
	dataout => left_a8_a_DATAC_driver);

left_a8_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(8),
	dataout => left_a8_a_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N20
left_a8_a : cycloneive_lcell_comb
-- Equation(s):
-- left(8) = (mux_alu_left_a0_a_ainput_o & ((left_a8_a_a23_combout & ((HI(8)))) # (!left_a8_a_a23_combout & (PC(8))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a8_a_a23_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a8_a_DATAA_driver,
	datab => left_a8_a_DATAB_driver,
	datac => left_a8_a_DATAC_driver,
	datad => left_a8_a_DATAD_driver,
	combout => left(8));

left_a7_a_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a7_a_a24_DATAA_driver);

left_a7_a_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a7_a_a24_DATAB_driver);

left_a7_a_a24_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(7),
	dataout => left_a7_a_a24_DATAC_driver);

left_a7_a_a24_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(6),
	dataout => left_a7_a_a24_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N2
left_a7_a_a24 : cycloneive_lcell_comb
-- Equation(s):
-- left_a7_a_a24_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(6)))) # (!mux_alu_left_a1_a_ainput_o & (A(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a7_a_a24_DATAA_driver,
	datab => left_a7_a_a24_DATAB_driver,
	datac => left_a7_a_a24_DATAC_driver,
	datad => left_a7_a_a24_DATAD_driver,
	combout => left_a7_a_a24_combout);

left_a7_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a7_a_DATAA_driver);

left_a7_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a7_a_a24_combout,
	dataout => left_a7_a_DATAB_driver);

left_a7_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(7),
	dataout => left_a7_a_DATAC_driver);

left_a7_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(7),
	dataout => left_a7_a_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N30
left_a7_a : cycloneive_lcell_comb
-- Equation(s):
-- left(7) = (mux_alu_left_a0_a_ainput_o & ((left_a7_a_a24_combout & ((HI(7)))) # (!left_a7_a_a24_combout & (PC(7))))) # (!mux_alu_left_a0_a_ainput_o & (left_a7_a_a24_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a7_a_DATAA_driver,
	datab => left_a7_a_DATAB_driver,
	datac => left_a7_a_DATAC_driver,
	datad => left_a7_a_DATAD_driver,
	combout => left(7));

left_a6_a_a25_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a6_a_a25_DATAA_driver);

left_a6_a_a25_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a6_a_a25_DATAB_driver);

left_a6_a_a25_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(6),
	dataout => left_a6_a_a25_DATAC_driver);

left_a6_a_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(5),
	dataout => left_a6_a_a25_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N4
left_a6_a_a25 : cycloneive_lcell_comb
-- Equation(s):
-- left_a6_a_a25_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(5)))) # (!mux_alu_left_a1_a_ainput_o & (A(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a6_a_a25_DATAA_driver,
	datab => left_a6_a_a25_DATAB_driver,
	datac => left_a6_a_a25_DATAC_driver,
	datad => left_a6_a_a25_DATAD_driver,
	combout => left_a6_a_a25_combout);

left_a6_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a6_a_DATAA_driver);

left_a6_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(6),
	dataout => left_a6_a_DATAB_driver);

left_a6_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a6_a_a25_combout,
	dataout => left_a6_a_DATAC_driver);

left_a6_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(6),
	dataout => left_a6_a_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N6
left_a6_a : cycloneive_lcell_comb
-- Equation(s):
-- left(6) = (mux_alu_left_a0_a_ainput_o & ((left_a6_a_a25_combout & (HI(6))) # (!left_a6_a_a25_combout & ((PC(6)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a6_a_a25_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a6_a_DATAA_driver,
	datab => left_a6_a_DATAB_driver,
	datac => left_a6_a_DATAC_driver,
	datad => left_a6_a_DATAD_driver,
	combout => left(6));

left_a5_a_a26_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a5_a_a26_DATAA_driver);

left_a5_a_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a5_a_a26_DATAB_driver);

left_a5_a_a26_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(5),
	dataout => left_a5_a_a26_DATAC_driver);

left_a5_a_a26_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(4),
	dataout => left_a5_a_a26_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N8
left_a5_a_a26 : cycloneive_lcell_comb
-- Equation(s):
-- left_a5_a_a26_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(4)))) # (!mux_alu_left_a1_a_ainput_o & (A(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a5_a_a26_DATAA_driver,
	datab => left_a5_a_a26_DATAB_driver,
	datac => left_a5_a_a26_DATAC_driver,
	datad => left_a5_a_a26_DATAD_driver,
	combout => left_a5_a_a26_combout);

left_a5_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a5_a_DATAA_driver);

left_a5_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(5),
	dataout => left_a5_a_DATAB_driver);

left_a5_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a5_a_a26_combout,
	dataout => left_a5_a_DATAC_driver);

left_a5_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(5),
	dataout => left_a5_a_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N22
left_a5_a : cycloneive_lcell_comb
-- Equation(s):
-- left(5) = (mux_alu_left_a0_a_ainput_o & ((left_a5_a_a26_combout & (HI(5))) # (!left_a5_a_a26_combout & ((PC(5)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a5_a_a26_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a5_a_DATAA_driver,
	datab => left_a5_a_DATAB_driver,
	datac => left_a5_a_DATAC_driver,
	datad => left_a5_a_DATAD_driver,
	combout => left(5));

left_a4_a_a27_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a4_a_a27_DATAA_driver);

left_a4_a_a27_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a4_a_a27_DATAB_driver);

left_a4_a_a27_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(4),
	dataout => left_a4_a_a27_DATAC_driver);

left_a4_a_a27_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(3),
	dataout => left_a4_a_a27_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N10
left_a4_a_a27 : cycloneive_lcell_comb
-- Equation(s):
-- left_a4_a_a27_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(3))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a4_a_a27_DATAA_driver,
	datab => left_a4_a_a27_DATAB_driver,
	datac => left_a4_a_a27_DATAC_driver,
	datad => left_a4_a_a27_DATAD_driver,
	combout => left_a4_a_a27_combout);

left_a4_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(4),
	dataout => left_a4_a_DATAA_driver);

left_a4_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a4_a_DATAB_driver);

left_a4_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(4),
	dataout => left_a4_a_DATAC_driver);

left_a4_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a4_a_a27_combout,
	dataout => left_a4_a_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N20
left_a4_a : cycloneive_lcell_comb
-- Equation(s):
-- left(4) = (mux_alu_left_a0_a_ainput_o & ((left_a4_a_a27_combout & ((HI(4)))) # (!left_a4_a_a27_combout & (PC(4))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a4_a_a27_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a4_a_DATAA_driver,
	datab => left_a4_a_DATAB_driver,
	datac => left_a4_a_DATAC_driver,
	datad => left_a4_a_DATAD_driver,
	combout => left(4));

left_a3_a_a28_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a3_a_a28_DATAA_driver);

left_a3_a_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(3),
	dataout => left_a3_a_a28_DATAB_driver);

left_a3_a_a28_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a3_a_a28_DATAC_driver);

left_a3_a_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(2),
	dataout => left_a3_a_a28_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N18
left_a3_a_a28 : cycloneive_lcell_comb
-- Equation(s):
-- left_a3_a_a28_combout = (mux_alu_left_a1_a_ainput_o & (((mux_alu_left_a0_a_ainput_o) # (HI(2))))) # (!mux_alu_left_a1_a_ainput_o & (A(3) & (!mux_alu_left_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a3_a_a28_DATAA_driver,
	datab => left_a3_a_a28_DATAB_driver,
	datac => left_a3_a_a28_DATAC_driver,
	datad => left_a3_a_a28_DATAD_driver,
	combout => left_a3_a_a28_combout);

left_a3_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(3),
	dataout => left_a3_a_DATAA_driver);

left_a3_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a3_a_DATAB_driver);

left_a3_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(3),
	dataout => left_a3_a_DATAC_driver);

left_a3_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a3_a_a28_combout,
	dataout => left_a3_a_DATAD_driver);

-- Location: LCCOMB_X37_Y29_N10
left_a3_a : cycloneive_lcell_comb
-- Equation(s):
-- left(3) = (mux_alu_left_a0_a_ainput_o & ((left_a3_a_a28_combout & (HI(3))) # (!left_a3_a_a28_combout & ((PC(3)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a3_a_a28_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a3_a_DATAA_driver,
	datab => left_a3_a_DATAB_driver,
	datac => left_a3_a_DATAC_driver,
	datad => left_a3_a_DATAD_driver,
	combout => left(3));

left_a2_a_a29_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a2_a_a29_DATAA_driver);

left_a2_a_a29_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a2_a_a29_DATAB_driver);

left_a2_a_a29_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(2),
	dataout => left_a2_a_a29_DATAC_driver);

left_a2_a_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(1),
	dataout => left_a2_a_a29_DATAD_driver);

-- Location: LCCOMB_X37_Y29_N8
left_a2_a_a29 : cycloneive_lcell_comb
-- Equation(s):
-- left_a2_a_a29_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(1))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a2_a_a29_DATAA_driver,
	datab => left_a2_a_a29_DATAB_driver,
	datac => left_a2_a_a29_DATAC_driver,
	datad => left_a2_a_a29_DATAD_driver,
	combout => left_a2_a_a29_combout);

left_a2_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(2),
	dataout => left_a2_a_DATAA_driver);

left_a2_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a2_a_DATAB_driver);

left_a2_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a2_a_a29_combout,
	dataout => left_a2_a_DATAC_driver);

left_a2_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(2),
	dataout => left_a2_a_DATAD_driver);

-- Location: LCCOMB_X37_Y29_N12
left_a2_a : cycloneive_lcell_comb
-- Equation(s):
-- left(2) = (mux_alu_left_a0_a_ainput_o & ((left_a2_a_a29_combout & (HI(2))) # (!left_a2_a_a29_combout & ((PC(2)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a2_a_a29_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a2_a_DATAA_driver,
	datab => left_a2_a_DATAB_driver,
	datac => left_a2_a_DATAC_driver,
	datad => left_a2_a_DATAD_driver,
	combout => left(2));

B_a1_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a1_a_CLK_driver);

B_a1_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a2_combout,
	dataout => B_a1_a_ASDATA_driver);

B_a1_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a1_a_ENA_driver);

-- Location: FF_X37_Y31_N11
B_a1_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a1_a_CLK_driver,
	asdata => B_a1_a_ASDATA_driver,
	sload => VCC,
	ena => B_a1_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(1));

B_a0_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a0_a_CLK_driver);

B_a0_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a0_combout,
	dataout => B_a0_a_ASDATA_driver);

B_a0_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a0_a_ENA_driver);

-- Location: FF_X37_Y31_N25
B_a0_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a0_a_CLK_driver,
	asdata => B_a0_a_ASDATA_driver,
	sload => VCC,
	ena => B_a0_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(0));

HI_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a5_DATAA_driver);

HI_a5_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(2),
	dataout => HI_a5_DATAB_driver);

HI_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a8_combout,
	dataout => HI_a5_DATAC_driver);

HI_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a5_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N10
HI_a5 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a5_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a8_combout))) # (!Add0_a66_combout & (HI(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a5_DATAA_driver,
	datab => HI_a5_DATAB_driver,
	datac => HI_a5_DATAC_driver,
	datad => HI_a5_DATAD_driver,
	combout => HI_a5_combout);

LO_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(8),
	dataout => LO_a10_DATAB_driver);

LO_a10_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a10_DATAC_driver);

LO_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a9_a_ainput_o,
	dataout => LO_a10_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N6
LO_a10 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a10_combout = (mux_lo_a1_a_ainput_o & ((data_in_a9_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a10_DATAB_driver,
	datac => LO_a10_DATAC_driver,
	datad => LO_a10_DATAD_driver,
	combout => LO_a10_combout);

LO_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a20_DATAA_driver);

LO_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(18),
	dataout => LO_a20_DATAB_driver);

LO_a20_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a19_a_ainput_o,
	dataout => LO_a20_DATAC_driver);

-- Location: LCCOMB_X35_Y28_N8
LO_a20 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a20_combout = (mux_lo_a1_a_ainput_o & ((data_in_a19_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(18)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a20_DATAA_driver,
	datab => LO_a20_DATAB_driver,
	datac => LO_a20_DATAC_driver,
	combout => LO_a20_combout);

LO_a21_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(19),
	dataout => LO_a21_DATAB_driver);

LO_a21_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a21_DATAC_driver);

LO_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a20_a_ainput_o,
	dataout => LO_a21_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N22
LO_a21 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a21_combout = (mux_lo_a1_a_ainput_o & ((data_in_a20_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a21_DATAB_driver,
	datac => LO_a21_DATAC_driver,
	datad => LO_a21_DATAD_driver,
	combout => LO_a21_combout);

A_a29_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a29_DATAC_driver);

A_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a28_a_ainput_o,
	dataout => A_a29_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N30
A_a29 : cycloneive_lcell_comb
-- Equation(s):
-- A_a29_combout = (!reset_ainput_o & data_in_a28_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a29_DATAC_driver,
	datad => A_a29_DATAD_driver,
	combout => A_a29_combout);

O_n_a31_a_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_n_a31_a_a5_DATAA_driver);

O_n_a31_a_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a2_a_ainput_o,
	dataout => O_n_a31_a_a5_DATAC_driver);

O_n_a31_a_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_n_a31_a_a5_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N20
O_n_a31_a_a5 : cycloneive_lcell_comb
-- Equation(s):
-- O_n_a31_a_a5_combout = mux_out_a2_a_ainput_o $ (((mux_out_a0_a_ainput_o & !mux_out_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_n_a31_a_a5_DATAA_driver,
	datac => O_n_a31_a_a5_DATAC_driver,
	datad => O_n_a31_a_a5_DATAD_driver,
	combout => O_n_a31_a_a5_combout);

op2_real_a126_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a126_DATAA_driver);

op2_real_a126_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a126_DATAB_driver);

op2_real_a126_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(0),
	dataout => op2_real_a126_DATAC_driver);

op2_real_a126_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a0_a_ainput_o,
	dataout => op2_real_a126_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N24
op2_real_a126 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a126_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & ((intr_right_a0_a_ainput_o))) # (!mux_alu_right_a0_a_ainput_o & (B(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a126_DATAA_driver,
	datab => op2_real_a126_DATAB_driver,
	datac => op2_real_a126_DATAC_driver,
	datad => op2_real_a126_DATAD_driver,
	combout => op2_real_a126_combout);

op2_real_a127_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a127_DATAA_driver);

op2_real_a127_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a127_DATAC_driver);

op2_real_a127_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a126_combout,
	dataout => op2_real_a127_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N14
op2_real_a127 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a127_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a126_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a126_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a127_DATAA_driver,
	datac => op2_real_a127_DATAC_driver,
	datad => op2_real_a127_DATAD_driver,
	combout => op2_real_a127_combout);

op2_real_a128_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a128_DATAA_driver);

op2_real_a128_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a1_a_ainput_o,
	dataout => op2_real_a128_DATAB_driver);

op2_real_a128_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(1),
	dataout => op2_real_a128_DATAC_driver);

op2_real_a128_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a128_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N10
op2_real_a128 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a128_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a1_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a128_DATAA_driver,
	datab => op2_real_a128_DATAB_driver,
	datac => op2_real_a128_DATAC_driver,
	datad => op2_real_a128_DATAD_driver,
	combout => op2_real_a128_combout);

op2_real_a129_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a129_DATAB_driver);

op2_real_a129_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a129_DATAC_driver);

op2_real_a129_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a128_combout,
	dataout => op2_real_a129_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N28
op2_real_a129 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a129_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a128_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a128_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a129_DATAB_driver,
	datac => op2_real_a129_DATAC_driver,
	datad => op2_real_a129_DATAD_driver,
	combout => op2_real_a129_combout);

op2_real_a142_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a142_DATAA_driver);

op2_real_a142_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a9_a_ainput_o,
	dataout => op2_real_a142_DATAB_driver);

op2_real_a142_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(9),
	dataout => op2_real_a142_DATAC_driver);

op2_real_a142_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a142_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N24
op2_real_a142 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a142_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a9_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a142_DATAA_driver,
	datab => op2_real_a142_DATAB_driver,
	datac => op2_real_a142_DATAC_driver,
	datad => op2_real_a142_DATAD_driver,
	combout => op2_real_a142_combout);

op2_real_a143_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a143_DATAB_driver);

op2_real_a143_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a143_DATAC_driver);

op2_real_a143_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a142_combout,
	dataout => op2_real_a143_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N30
op2_real_a143 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a143_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a142_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a142_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a143_DATAB_driver,
	datac => op2_real_a143_DATAC_driver,
	datad => op2_real_a143_DATAD_driver,
	combout => op2_real_a143_combout);

op2_real_a144_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a10_a_ainput_o,
	dataout => op2_real_a144_DATAA_driver);

op2_real_a144_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a144_DATAB_driver);

op2_real_a144_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(10),
	dataout => op2_real_a144_DATAC_driver);

op2_real_a144_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a144_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N2
op2_real_a144 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a144_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a10_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a144_DATAA_driver,
	datab => op2_real_a144_DATAB_driver,
	datac => op2_real_a144_DATAC_driver,
	datad => op2_real_a144_DATAD_driver,
	combout => op2_real_a144_combout);

op2_real_a145_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a145_DATAA_driver);

op2_real_a145_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a145_DATAB_driver);

op2_real_a145_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a144_combout,
	dataout => op2_real_a145_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N16
op2_real_a145 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a145_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a144_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a144_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a145_DATAA_driver,
	datab => op2_real_a145_DATAB_driver,
	datad => op2_real_a145_DATAD_driver,
	combout => op2_real_a145_combout);

op2_real_a148_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a148_DATAA_driver);

op2_real_a148_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a148_DATAB_driver);

op2_real_a148_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(12),
	dataout => op2_real_a148_DATAC_driver);

op2_real_a148_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a12_a_ainput_o,
	dataout => op2_real_a148_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N10
op2_real_a148 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a148_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & ((intr_right_a12_a_ainput_o))) # (!mux_alu_right_a0_a_ainput_o & (B(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a148_DATAA_driver,
	datab => op2_real_a148_DATAB_driver,
	datac => op2_real_a148_DATAC_driver,
	datad => op2_real_a148_DATAD_driver,
	combout => op2_real_a148_combout);

op2_real_a149_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a149_DATAA_driver);

op2_real_a149_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a149_DATAB_driver);

op2_real_a149_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a148_combout,
	dataout => op2_real_a149_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N28
op2_real_a149 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a149_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a148_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a148_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a149_DATAA_driver,
	datab => op2_real_a149_DATAB_driver,
	datad => op2_real_a149_DATAD_driver,
	combout => op2_real_a149_combout);

op2_real_a150_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a13_a_ainput_o,
	dataout => op2_real_a150_DATAA_driver);

op2_real_a150_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a150_DATAB_driver);

op2_real_a150_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(13),
	dataout => op2_real_a150_DATAC_driver);

op2_real_a150_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a150_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N20
op2_real_a150 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a150_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a13_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a150_DATAA_driver,
	datab => op2_real_a150_DATAB_driver,
	datac => op2_real_a150_DATAC_driver,
	datad => op2_real_a150_DATAD_driver,
	combout => op2_real_a150_combout);

op2_real_a151_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a151_DATAA_driver);

op2_real_a151_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a151_DATAB_driver);

op2_real_a151_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a150_combout,
	dataout => op2_real_a151_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N22
op2_real_a151 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a151_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a150_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a150_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a151_DATAA_driver,
	datab => op2_real_a151_DATAB_driver,
	datad => op2_real_a151_DATAD_driver,
	combout => op2_real_a151_combout);

op2_real_a152_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a14_a_ainput_o,
	dataout => op2_real_a152_DATAA_driver);

op2_real_a152_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a152_DATAB_driver);

op2_real_a152_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(14),
	dataout => op2_real_a152_DATAC_driver);

op2_real_a152_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a152_DATAD_driver);

-- Location: LCCOMB_X41_Y28_N10
op2_real_a152 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a152_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a14_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a152_DATAA_driver,
	datab => op2_real_a152_DATAB_driver,
	datac => op2_real_a152_DATAC_driver,
	datad => op2_real_a152_DATAD_driver,
	combout => op2_real_a152_combout);

op2_real_a153_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a153_DATAA_driver);

op2_real_a153_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a153_DATAC_driver);

op2_real_a153_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a152_combout,
	dataout => op2_real_a153_DATAD_driver);

-- Location: LCCOMB_X41_Y28_N12
op2_real_a153 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a153_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a152_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a152_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a153_DATAA_driver,
	datac => op2_real_a153_DATAC_driver,
	datad => op2_real_a153_DATAD_driver,
	combout => op2_real_a153_combout);

op2_real_a156_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a156_DATAA_driver);

op2_real_a156_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a16_a_ainput_o,
	dataout => op2_real_a156_DATAB_driver);

op2_real_a156_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(16),
	dataout => op2_real_a156_DATAC_driver);

op2_real_a156_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a156_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N18
op2_real_a156 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a156_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a16_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(16)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a156_DATAA_driver,
	datab => op2_real_a156_DATAB_driver,
	datac => op2_real_a156_DATAC_driver,
	datad => op2_real_a156_DATAD_driver,
	combout => op2_real_a156_combout);

op2_real_a157_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a157_DATAB_driver);

op2_real_a157_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a157_DATAC_driver);

op2_real_a157_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a156_combout,
	dataout => op2_real_a157_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N0
op2_real_a157 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a157_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a156_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a156_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a157_DATAB_driver,
	datac => op2_real_a157_DATAC_driver,
	datad => op2_real_a157_DATAD_driver,
	combout => op2_real_a157_combout);

op2_real_a158_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a158_DATAA_driver);

op2_real_a158_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a158_DATAB_driver);

op2_real_a158_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(17),
	dataout => op2_real_a158_DATAC_driver);

op2_real_a158_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a17_a_ainput_o,
	dataout => op2_real_a158_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N8
op2_real_a158 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a158_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & ((intr_right_a17_a_ainput_o))) # (!mux_alu_right_a0_a_ainput_o & (B(17))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a158_DATAA_driver,
	datab => op2_real_a158_DATAB_driver,
	datac => op2_real_a158_DATAC_driver,
	datad => op2_real_a158_DATAD_driver,
	combout => op2_real_a158_combout);

op2_real_a159_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a159_DATAA_driver);

op2_real_a159_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a159_DATAB_driver);

op2_real_a159_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a158_combout,
	dataout => op2_real_a159_DATAC_driver);

-- Location: LCCOMB_X37_Y31_N26
op2_real_a159 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a159_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a158_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a158_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a159_DATAA_driver,
	datab => op2_real_a159_DATAB_driver,
	datac => op2_real_a159_DATAC_driver,
	combout => op2_real_a159_combout);

op2_real_a172_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a172_DATAA_driver);

op2_real_a172_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a24_a_ainput_o,
	dataout => op2_real_a172_DATAB_driver);

op2_real_a172_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(24),
	dataout => op2_real_a172_DATAC_driver);

op2_real_a172_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a172_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N12
op2_real_a172 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a172_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a24_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(24)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a172_DATAA_driver,
	datab => op2_real_a172_DATAB_driver,
	datac => op2_real_a172_DATAC_driver,
	datad => op2_real_a172_DATAD_driver,
	combout => op2_real_a172_combout);

op2_real_a173_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a173_DATAB_driver);

op2_real_a173_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a173_DATAC_driver);

op2_real_a173_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a172_combout,
	dataout => op2_real_a173_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N2
op2_real_a173 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a173_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a172_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a172_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a173_DATAB_driver,
	datac => op2_real_a173_DATAC_driver,
	datad => op2_real_a173_DATAD_driver,
	combout => op2_real_a173_combout);

op2_real_a174_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a25_a_ainput_o,
	dataout => op2_real_a174_DATAA_driver);

op2_real_a174_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a174_DATAB_driver);

op2_real_a174_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(25),
	dataout => op2_real_a174_DATAC_driver);

op2_real_a174_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a174_DATAD_driver);

-- Location: LCCOMB_X42_Y27_N0
op2_real_a174 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a174_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a25_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(25)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a174_DATAA_driver,
	datab => op2_real_a174_DATAB_driver,
	datac => op2_real_a174_DATAC_driver,
	datad => op2_real_a174_DATAD_driver,
	combout => op2_real_a174_combout);

op2_real_a175_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a175_DATAB_driver);

op2_real_a175_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a175_DATAC_driver);

op2_real_a175_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a174_combout,
	dataout => op2_real_a175_DATAD_driver);

-- Location: LCCOMB_X42_Y27_N14
op2_real_a175 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a175_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a174_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a174_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a175_DATAB_driver,
	datac => op2_real_a175_DATAC_driver,
	datad => op2_real_a175_DATAD_driver,
	combout => op2_real_a175_combout);

op2_real_a176_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a176_DATAA_driver);

op2_real_a176_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a26_a_ainput_o,
	dataout => op2_real_a176_DATAB_driver);

op2_real_a176_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(26),
	dataout => op2_real_a176_DATAC_driver);

op2_real_a176_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a176_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N2
op2_real_a176 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a176_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a26_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(26)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a176_DATAA_driver,
	datab => op2_real_a176_DATAB_driver,
	datac => op2_real_a176_DATAC_driver,
	datad => op2_real_a176_DATAD_driver,
	combout => op2_real_a176_combout);

op2_real_a177_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a177_DATAA_driver);

op2_real_a177_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a177_DATAC_driver);

op2_real_a177_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a176_combout,
	dataout => op2_real_a177_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N4
op2_real_a177 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a177_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a176_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a176_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a177_DATAA_driver,
	datac => op2_real_a177_DATAC_driver,
	datad => op2_real_a177_DATAD_driver,
	combout => op2_real_a177_combout);

op2_real_a178_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a27_a_ainput_o,
	dataout => op2_real_a178_DATAA_driver);

op2_real_a178_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a178_DATAB_driver);

op2_real_a178_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(27),
	dataout => op2_real_a178_DATAC_driver);

op2_real_a178_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a178_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N30
op2_real_a178 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a178_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a27_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(27)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a178_DATAA_driver,
	datab => op2_real_a178_DATAB_driver,
	datac => op2_real_a178_DATAC_driver,
	datad => op2_real_a178_DATAD_driver,
	combout => op2_real_a178_combout);

op2_real_a179_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a179_DATAA_driver);

op2_real_a179_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a178_combout,
	dataout => op2_real_a179_DATAC_driver);

op2_real_a179_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a179_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N0
op2_real_a179 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a179_combout = (mux_alu_right_a1_a_ainput_o & (op2_real_a178_combout & alu_add_sub_ainput_o)) # (!mux_alu_right_a1_a_ainput_o & (op2_real_a178_combout $ (alu_add_sub_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a179_DATAA_driver,
	datac => op2_real_a179_DATAC_driver,
	datad => op2_real_a179_DATAD_driver,
	combout => op2_real_a179_combout);

op2_real_a180_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a28_a_ainput_o,
	dataout => op2_real_a180_DATAA_driver);

op2_real_a180_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a180_DATAB_driver);

op2_real_a180_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(28),
	dataout => op2_real_a180_DATAC_driver);

op2_real_a180_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a180_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N8
op2_real_a180 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a180_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a28_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(28)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a180_DATAA_driver,
	datab => op2_real_a180_DATAB_driver,
	datac => op2_real_a180_DATAC_driver,
	datad => op2_real_a180_DATAD_driver,
	combout => op2_real_a180_combout);

op2_real_a181_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a181_DATAA_driver);

op2_real_a181_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a181_DATAB_driver);

op2_real_a181_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a180_combout,
	dataout => op2_real_a181_DATAC_driver);

-- Location: LCCOMB_X37_Y24_N6
op2_real_a181 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a181_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a180_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a180_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a181_DATAA_driver,
	datab => op2_real_a181_DATAB_driver,
	datac => op2_real_a181_DATAC_driver,
	combout => op2_real_a181_combout);

op2_real_a184_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a184_DATAA_driver);

op2_real_a184_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a30_a_ainput_o,
	dataout => op2_real_a184_DATAB_driver);

op2_real_a184_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(30),
	dataout => op2_real_a184_DATAC_driver);

op2_real_a184_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a184_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N16
op2_real_a184 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a184_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a30_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(30)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a184_DATAA_driver,
	datab => op2_real_a184_DATAB_driver,
	datac => op2_real_a184_DATAC_driver,
	datad => op2_real_a184_DATAD_driver,
	combout => op2_real_a184_combout);

op2_real_a185_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a185_DATAA_driver);

op2_real_a185_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a185_DATAC_driver);

op2_real_a185_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a184_combout,
	dataout => op2_real_a185_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N22
op2_real_a185 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a185_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a184_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a184_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a185_DATAA_driver,
	datac => op2_real_a185_DATAC_driver,
	datad => op2_real_a185_DATAD_driver,
	combout => op2_real_a185_combout);

op2_real_a186_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a31_a_ainput_o,
	dataout => op2_real_a186_DATAA_driver);

op2_real_a186_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a186_DATAB_driver);

op2_real_a186_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(31),
	dataout => op2_real_a186_DATAC_driver);

op2_real_a186_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a186_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N4
op2_real_a186 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a186_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a31_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(31)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a186_DATAA_driver,
	datab => op2_real_a186_DATAB_driver,
	datac => op2_real_a186_DATAC_driver,
	datad => op2_real_a186_DATAD_driver,
	combout => op2_real_a186_combout);

op2_real_a187_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a187_DATAA_driver);

op2_real_a187_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a186_combout,
	dataout => op2_real_a187_DATAC_driver);

op2_real_a187_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a187_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N26
op2_real_a187 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a187_combout = (mux_alu_right_a1_a_ainput_o & (op2_real_a186_combout & alu_add_sub_ainput_o)) # (!mux_alu_right_a1_a_ainput_o & (op2_real_a186_combout $ (alu_add_sub_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a187_DATAA_driver,
	datac => op2_real_a187_DATAC_driver,
	datad => op2_real_a187_DATAD_driver,
	combout => op2_real_a187_combout);

address_reg_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(1),
	dataout => address_reg_a1_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y39_N1
address_reg_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a1_a_ainput_I_driver,
	o => address_reg_a1_a_ainput_o);

address_reg_a4_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(4),
	dataout => address_reg_a4_a_ainput_I_driver);

-- Location: IOIBUF_X43_Y0_N29
address_reg_a4_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a4_a_ainput_I_driver,
	o => address_reg_a4_a_ainput_o);

address_reg_a5_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(5),
	dataout => address_reg_a5_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y30_N8
address_reg_a5_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a5_a_ainput_I_driver,
	o => address_reg_a5_a_ainput_o);

address_reg_a9_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(9),
	dataout => address_reg_a9_a_ainput_I_driver);

-- Location: IOIBUF_X34_Y0_N1
address_reg_a9_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a9_a_ainput_I_driver,
	o => address_reg_a9_a_ainput_o);

address_reg_a15_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(15),
	dataout => address_reg_a15_a_ainput_I_driver);

-- Location: IOIBUF_X50_Y0_N8
address_reg_a15_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a15_a_ainput_I_driver,
	o => address_reg_a15_a_ainput_o);

address_reg_a21_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(21),
	dataout => address_reg_a21_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y28_N22
address_reg_a21_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a21_a_ainput_I_driver,
	o => address_reg_a21_a_ainput_o);

address_reg_a22_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(22),
	dataout => address_reg_a22_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y36_N8
address_reg_a22_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a22_a_ainput_I_driver,
	o => address_reg_a22_a_ainput_o);

address_reg_a23_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(23),
	dataout => address_reg_a23_a_ainput_I_driver);

-- Location: IOIBUF_X27_Y0_N1
address_reg_a23_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a23_a_ainput_I_driver,
	o => address_reg_a23_a_ainput_o);

intr_right_a31_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(31),
	dataout => intr_right_a31_a_ainput_I_driver);

-- Location: IOIBUF_X18_Y0_N15
intr_right_a31_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a31_a_ainput_I_driver,
	o => intr_right_a31_a_ainput_o);

intr_right_a30_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(30),
	dataout => intr_right_a30_a_ainput_I_driver);

-- Location: IOIBUF_X5_Y43_N8
intr_right_a30_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a30_a_ainput_I_driver,
	o => intr_right_a30_a_ainput_o);

intr_right_a29_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(29),
	dataout => intr_right_a29_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y16_N8
intr_right_a29_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a29_a_ainput_I_driver,
	o => intr_right_a29_a_ainput_o);

intr_right_a28_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(28),
	dataout => intr_right_a28_a_ainput_I_driver);

-- Location: IOIBUF_X36_Y0_N8
intr_right_a28_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a28_a_ainput_I_driver,
	o => intr_right_a28_a_ainput_o);

intr_right_a27_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(27),
	dataout => intr_right_a27_a_ainput_I_driver);

-- Location: IOIBUF_X36_Y0_N1
intr_right_a27_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a27_a_ainput_I_driver,
	o => intr_right_a27_a_ainput_o);

intr_right_a26_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(26),
	dataout => intr_right_a26_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y25_N22
intr_right_a26_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a26_a_ainput_I_driver,
	o => intr_right_a26_a_ainput_o);

intr_right_a25_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(25),
	dataout => intr_right_a25_a_ainput_I_driver);

-- Location: IOIBUF_X65_Y43_N22
intr_right_a25_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a25_a_ainput_I_driver,
	o => intr_right_a25_a_ainput_o);

intr_right_a24_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(24),
	dataout => intr_right_a24_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y34_N1
intr_right_a24_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a24_a_ainput_I_driver,
	o => intr_right_a24_a_ainput_o);

intr_right_a17_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(17),
	dataout => intr_right_a17_a_ainput_I_driver);

-- Location: IOIBUF_X56_Y43_N22
intr_right_a17_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a17_a_ainput_I_driver,
	o => intr_right_a17_a_ainput_o);

intr_right_a16_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(16),
	dataout => intr_right_a16_a_ainput_I_driver);

-- Location: IOIBUF_X29_Y0_N29
intr_right_a16_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a16_a_ainput_I_driver,
	o => intr_right_a16_a_ainput_o);

intr_right_a14_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(14),
	dataout => intr_right_a14_a_ainput_I_driver);

-- Location: IOIBUF_X52_Y43_N15
intr_right_a14_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a14_a_ainput_I_driver,
	o => intr_right_a14_a_ainput_o);

intr_right_a13_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(13),
	dataout => intr_right_a13_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y20_N1
intr_right_a13_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a13_a_ainput_I_driver,
	o => intr_right_a13_a_ainput_o);

intr_right_a12_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(12),
	dataout => intr_right_a12_a_ainput_I_driver);

-- Location: IOIBUF_X45_Y0_N1
intr_right_a12_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a12_a_ainput_I_driver,
	o => intr_right_a12_a_ainput_o);

intr_right_a11_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(11),
	dataout => intr_right_a11_a_ainput_I_driver);

-- Location: IOIBUF_X18_Y0_N22
intr_right_a11_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a11_a_ainput_I_driver,
	o => intr_right_a11_a_ainput_o);

intr_right_a10_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(10),
	dataout => intr_right_a10_a_ainput_I_driver);

-- Location: IOIBUF_X38_Y0_N22
intr_right_a10_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a10_a_ainput_I_driver,
	o => intr_right_a10_a_ainput_o);

intr_right_a9_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(9),
	dataout => intr_right_a9_a_ainput_I_driver);

-- Location: IOIBUF_X45_Y0_N15
intr_right_a9_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a9_a_ainput_I_driver,
	o => intr_right_a9_a_ainput_o);

intr_right_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(1),
	dataout => intr_right_a1_a_ainput_I_driver);

-- Location: IOIBUF_X9_Y43_N15
intr_right_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a1_a_ainput_I_driver,
	o => intr_right_a1_a_ainput_o);

intr_right_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(0),
	dataout => intr_right_a0_a_ainput_I_driver);

-- Location: IOIBUF_X38_Y43_N22
intr_right_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a0_a_ainput_I_driver,
	o => intr_right_a0_a_ainput_o);

clk_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_clk,
	dataout => clk_ainput_I_driver);

-- Location: IOIBUF_X0_Y21_N8
clk_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => clk_ainput_I_driver,
	o => clk_ainput_o);

mux_hi_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_hi(0),
	dataout => mux_hi_a0_a_ainput_I_driver);

-- Location: IOIBUF_X50_Y0_N29
mux_hi_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_hi_a0_a_ainput_I_driver,
	o => mux_hi_a0_a_ainput_o);

intr_pc_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(1),
	dataout => intr_pc_a1_a_ainput_I_driver);

-- Location: IOIBUF_X38_Y0_N15
intr_pc_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a1_a_ainput_I_driver,
	o => intr_pc_a1_a_ainput_o);

intr_pc_a2_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(2),
	dataout => intr_pc_a2_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y37_N15
intr_pc_a2_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a2_a_ainput_I_driver,
	o => intr_pc_a2_a_ainput_o);

intr_pc_a3_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(3),
	dataout => intr_pc_a3_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y30_N15
intr_pc_a3_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a3_a_ainput_I_driver,
	o => intr_pc_a3_a_ainput_o);

intr_pc_a4_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(4),
	dataout => intr_pc_a4_a_ainput_I_driver);

-- Location: IOIBUF_X50_Y0_N1
intr_pc_a4_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a4_a_ainput_I_driver,
	o => intr_pc_a4_a_ainput_o);

intr_pc_a5_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(5),
	dataout => intr_pc_a5_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y34_N22
intr_pc_a5_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a5_a_ainput_I_driver,
	o => intr_pc_a5_a_ainput_o);

intr_pc_a6_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(6),
	dataout => intr_pc_a6_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y19_N8
intr_pc_a6_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a6_a_ainput_I_driver,
	o => intr_pc_a6_a_ainput_o);

intr_pc_a7_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(7),
	dataout => intr_pc_a7_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y18_N22
intr_pc_a7_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a7_a_ainput_I_driver,
	o => intr_pc_a7_a_ainput_o);

intr_pc_a8_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(8),
	dataout => intr_pc_a8_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y18_N15
intr_pc_a8_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a8_a_ainput_I_driver,
	o => intr_pc_a8_a_ainput_o);

intr_pc_a9_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(9),
	dataout => intr_pc_a9_a_ainput_I_driver);

-- Location: IOIBUF_X20_Y0_N15
intr_pc_a9_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a9_a_ainput_I_driver,
	o => intr_pc_a9_a_ainput_o);

intr_pc_a10_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(10),
	dataout => intr_pc_a10_a_ainput_I_driver);

-- Location: IOIBUF_X20_Y43_N29
intr_pc_a10_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a10_a_ainput_I_driver,
	o => intr_pc_a10_a_ainput_o);

intr_pc_a11_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(11),
	dataout => intr_pc_a11_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y34_N15
intr_pc_a11_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a11_a_ainput_I_driver,
	o => intr_pc_a11_a_ainput_o);

intr_pc_a16_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(16),
	dataout => intr_pc_a16_a_ainput_I_driver);

-- Location: IOIBUF_X16_Y0_N1
intr_pc_a16_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a16_a_ainput_I_driver,
	o => intr_pc_a16_a_ainput_o);

intr_pc_a24_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(24),
	dataout => intr_pc_a24_a_ainput_I_driver);

-- Location: IOIBUF_X52_Y0_N8
intr_pc_a24_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a24_a_ainput_I_driver,
	o => intr_pc_a24_a_ainput_o);

clk_ainputclkctrl_INCLK_a0_a_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainput_o,
	dataout => clk_ainputclkctrl_INCLK_bus(0));

clk_ainputclkctrl_INCLK_a1_a_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => vcc,
	dataout => clk_ainputclkctrl_INCLK_bus(1));

clk_ainputclkctrl_INCLK_a2_a_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => vcc,
	dataout => clk_ainputclkctrl_INCLK_bus(2));

clk_ainputclkctrl_INCLK_a3_a_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => vcc,
	dataout => clk_ainputclkctrl_INCLK_bus(3));

-- Location: CLKCTRL_G2
clk_ainputclkctrl : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => clk_ainputclkctrl_INCLK_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => clk_ainputclkctrl_outclk);

data_out_a0_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a3_combout,
	dataout => data_out_a0_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y36_N23
data_out_a0_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a0_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a0_a_aoutput_o);

data_out_a1_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a6_combout,
	dataout => data_out_a1_a_aoutput_I_driver);

-- Location: IOOBUF_X22_Y43_N2
data_out_a1_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a1_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a1_a_aoutput_o);

data_out_a2_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a9_combout,
	dataout => data_out_a2_a_aoutput_I_driver);

-- Location: IOOBUF_X7_Y43_N16
data_out_a2_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a2_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a2_a_aoutput_o);

data_out_a3_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a12_combout,
	dataout => data_out_a3_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y37_N2
data_out_a3_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a3_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a3_a_aoutput_o);

data_out_a4_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a15_combout,
	dataout => data_out_a4_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y13_N16
data_out_a4_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a4_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a4_a_aoutput_o);

data_out_a5_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a18_combout,
	dataout => data_out_a5_a_aoutput_I_driver);

-- Location: IOOBUF_X22_Y0_N9
data_out_a5_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a5_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a5_a_aoutput_o);

data_out_a6_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a21_combout,
	dataout => data_out_a6_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y35_N16
data_out_a6_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a6_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a6_a_aoutput_o);

data_out_a7_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a24_combout,
	dataout => data_out_a7_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y35_N9
data_out_a7_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a7_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a7_a_aoutput_o);

data_out_a8_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a27_combout,
	dataout => data_out_a8_a_aoutput_I_driver);

-- Location: IOOBUF_X54_Y43_N30
data_out_a8_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a8_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a8_a_aoutput_o);

data_out_a9_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a30_combout,
	dataout => data_out_a9_a_aoutput_I_driver);

-- Location: IOOBUF_X16_Y0_N9
data_out_a9_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a9_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a9_a_aoutput_o);

data_out_a10_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a33_combout,
	dataout => data_out_a10_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y38_N23
data_out_a10_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a10_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a10_a_aoutput_o);

data_out_a11_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a36_combout,
	dataout => data_out_a11_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y10_N16
data_out_a11_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a11_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a11_a_aoutput_o);

data_out_a12_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a39_combout,
	dataout => data_out_a12_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y15_N2
data_out_a12_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a12_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a12_a_aoutput_o);

data_out_a13_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a42_combout,
	dataout => data_out_a13_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y10_N23
data_out_a13_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a13_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a13_a_aoutput_o);

data_out_a14_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a45_combout,
	dataout => data_out_a14_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y14_N2
data_out_a14_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a14_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a14_a_aoutput_o);

data_out_a15_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a48_combout,
	dataout => data_out_a15_a_aoutput_I_driver);

-- Location: IOOBUF_X50_Y43_N23
data_out_a15_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a15_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a15_a_aoutput_o);

data_out_a16_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a51_combout,
	dataout => data_out_a16_a_aoutput_I_driver);

-- Location: IOOBUF_X52_Y43_N30
data_out_a16_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a16_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a16_a_aoutput_o);

data_out_a17_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a54_combout,
	dataout => data_out_a17_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y28_N9
data_out_a17_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a17_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a17_a_aoutput_o);

data_out_a18_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a57_combout,
	dataout => data_out_a18_a_aoutput_I_driver);

-- Location: IOOBUF_X27_Y0_N9
data_out_a18_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a18_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a18_a_aoutput_o);

data_out_a19_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a60_combout,
	dataout => data_out_a19_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y12_N23
data_out_a19_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a19_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a19_a_aoutput_o);

data_out_a20_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a63_combout,
	dataout => data_out_a20_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y19_N16
data_out_a20_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a20_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a20_a_aoutput_o);

data_out_a21_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a66_combout,
	dataout => data_out_a21_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y36_N16
data_out_a21_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a21_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a21_a_aoutput_o);

data_out_a22_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a69_combout,
	dataout => data_out_a22_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y13_N9
data_out_a22_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a22_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a22_a_aoutput_o);

data_out_a23_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a72_combout,
	dataout => data_out_a23_a_aoutput_I_driver);

-- Location: IOOBUF_X52_Y0_N16
data_out_a23_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a23_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a23_a_aoutput_o);

data_out_a24_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a75_combout,
	dataout => data_out_a24_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y20_N16
data_out_a24_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a24_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a24_a_aoutput_o);

data_out_a25_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a78_combout,
	dataout => data_out_a25_a_aoutput_I_driver);

-- Location: IOOBUF_X5_Y43_N30
data_out_a25_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a25_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a25_a_aoutput_o);

data_out_a26_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a81_combout,
	dataout => data_out_a26_a_aoutput_I_driver);

-- Location: IOOBUF_X20_Y43_N9
data_out_a26_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a26_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a26_a_aoutput_o);

data_out_a27_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a84_combout,
	dataout => data_out_a27_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y19_N2
data_out_a27_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a27_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a27_a_aoutput_o);

data_out_a28_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a87_combout,
	dataout => data_out_a28_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y31_N16
data_out_a28_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a28_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a28_a_aoutput_o);

data_out_a29_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a90_combout,
	dataout => data_out_a29_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y34_N9
data_out_a29_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a29_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a29_a_aoutput_o);

data_out_a30_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a93_combout,
	dataout => data_out_a30_a_aoutput_I_driver);

-- Location: IOOBUF_X59_Y43_N16
data_out_a30_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a30_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a30_a_aoutput_o);

data_out_a31_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a96_combout,
	dataout => data_out_a31_a_aoutput_I_driver);

-- Location: IOOBUF_X3_Y43_N2
data_out_a31_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => data_out_a31_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => data_out_a31_a_aoutput_o);

address_a0_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a1_combout,
	dataout => address_a0_a_aoutput_I_driver);

-- Location: IOOBUF_X45_Y43_N2
address_a0_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a0_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a0_a_aoutput_o);

address_a1_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a3_combout,
	dataout => address_a1_a_aoutput_I_driver);

-- Location: IOOBUF_X29_Y43_N2
address_a1_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a1_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a1_a_aoutput_o);

address_a2_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a5_combout,
	dataout => address_a2_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y36_N16
address_a2_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a2_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a2_a_aoutput_o);

address_a3_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a7_combout,
	dataout => address_a3_a_aoutput_I_driver);

-- Location: IOOBUF_X25_Y43_N2
address_a3_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a3_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a3_a_aoutput_o);

address_a4_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a9_combout,
	dataout => address_a4_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y37_N23
address_a4_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a4_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a4_a_aoutput_o);

address_a5_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a11_combout,
	dataout => address_a5_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y41_N9
address_a5_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a5_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a5_a_aoutput_o);

address_a6_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a13_combout,
	dataout => address_a6_a_aoutput_I_driver);

-- Location: IOOBUF_X5_Y43_N2
address_a6_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a6_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a6_a_aoutput_o);

address_a7_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a15_combout,
	dataout => address_a7_a_aoutput_I_driver);

-- Location: IOOBUF_X22_Y0_N2
address_a7_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a7_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a7_a_aoutput_o);

address_a8_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a17_combout,
	dataout => address_a8_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y20_N9
address_a8_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a8_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a8_a_aoutput_o);

address_a9_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a19_combout,
	dataout => address_a9_a_aoutput_I_driver);

-- Location: IOOBUF_X22_Y43_N30
address_a9_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a9_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a9_a_aoutput_o);

address_a10_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a21_combout,
	dataout => address_a10_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y10_N9
address_a10_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a10_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a10_a_aoutput_o);

address_a11_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a23_combout,
	dataout => address_a11_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y13_N2
address_a11_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a11_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a11_a_aoutput_o);

address_a12_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a25_combout,
	dataout => address_a12_a_aoutput_I_driver);

-- Location: IOOBUF_X18_Y0_N9
address_a12_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a12_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a12_a_aoutput_o);

address_a13_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a27_combout,
	dataout => address_a13_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y15_N9
address_a13_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a13_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a13_a_aoutput_o);

address_a14_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a29_combout,
	dataout => address_a14_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y19_N23
address_a14_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a14_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a14_a_aoutput_o);

address_a15_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a31_combout,
	dataout => address_a15_a_aoutput_I_driver);

-- Location: IOOBUF_X50_Y0_N23
address_a15_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a15_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a15_a_aoutput_o);

address_a16_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a33_combout,
	dataout => address_a16_a_aoutput_I_driver);

-- Location: IOOBUF_X48_Y0_N30
address_a16_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a16_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a16_a_aoutput_o);

address_a17_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a35_combout,
	dataout => address_a17_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y36_N2
address_a17_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a17_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a17_a_aoutput_o);

address_a18_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a37_combout,
	dataout => address_a18_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y17_N16
address_a18_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a18_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a18_a_aoutput_o);

address_a19_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a39_combout,
	dataout => address_a19_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y28_N23
address_a19_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a19_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a19_a_aoutput_o);

address_a20_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a41_combout,
	dataout => address_a20_a_aoutput_I_driver);

-- Location: IOOBUF_X9_Y43_N30
address_a20_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a20_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a20_a_aoutput_o);

address_a21_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a43_combout,
	dataout => address_a21_a_aoutput_I_driver);

-- Location: IOOBUF_X45_Y0_N23
address_a21_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a21_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a21_a_aoutput_o);

address_a22_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a45_combout,
	dataout => address_a22_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y28_N2
address_a22_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a22_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a22_a_aoutput_o);

address_a23_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a47_combout,
	dataout => address_a23_a_aoutput_I_driver);

-- Location: IOOBUF_X25_Y43_N16
address_a23_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a23_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a23_a_aoutput_o);

address_a24_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a49_combout,
	dataout => address_a24_a_aoutput_I_driver);

-- Location: IOOBUF_X5_Y43_N16
address_a24_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a24_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a24_a_aoutput_o);

address_a25_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a51_combout,
	dataout => address_a25_a_aoutput_I_driver);

-- Location: IOOBUF_X25_Y43_N9
address_a25_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a25_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a25_a_aoutput_o);

address_a26_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a53_combout,
	dataout => address_a26_a_aoutput_I_driver);

-- Location: IOOBUF_X54_Y43_N23
address_a26_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a26_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a26_a_aoutput_o);

address_a27_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a55_combout,
	dataout => address_a27_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y19_N16
address_a27_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a27_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a27_a_aoutput_o);

address_a28_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a57_combout,
	dataout => address_a28_a_aoutput_I_driver);

-- Location: IOOBUF_X3_Y43_N30
address_a28_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a28_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a28_a_aoutput_o);

address_a29_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a59_combout,
	dataout => address_a29_a_aoutput_I_driver);

-- Location: IOOBUF_X67_Y19_N9
address_a29_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a29_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a29_a_aoutput_o);

address_a30_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a61_combout,
	dataout => address_a30_a_aoutput_I_driver);

-- Location: IOOBUF_X22_Y0_N16
address_a30_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a30_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a30_a_aoutput_o);

address_a31_a_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a63_combout,
	dataout => address_a31_a_aoutput_I_driver);

-- Location: IOOBUF_X0_Y32_N16
address_a31_a_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => address_a31_a_aoutput_I_driver,
	devoe => ww_devoe,
	o => address_a31_a_aoutput_o);

a_zero_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a10_combout,
	dataout => a_zero_aoutput_I_driver);

-- Location: IOOBUF_X20_Y0_N23
a_zero_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => a_zero_aoutput_I_driver,
	devoe => ww_devoe,
	o => a_zero_aoutput_o);

a_msb_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(31),
	dataout => a_msb_aoutput_I_driver);

-- Location: IOOBUF_X61_Y43_N30
a_msb_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => a_msb_aoutput_I_driver,
	devoe => ww_devoe,
	o => a_msb_aoutput_o);

alu_c_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_Add0_a66_combout,
	dataout => alu_c_aoutput_I_driver);

-- Location: IOOBUF_X67_Y25_N16
alu_c_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => alu_c_aoutput_I_driver,
	devoe => ww_devoe,
	o => alu_c_aoutput_o);

alu_msb_aoutput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a64_combout,
	dataout => alu_msb_aoutput_I_driver);

-- Location: IOOBUF_X32_Y43_N9
alu_msb_aoutput : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => alu_msb_aoutput_I_driver,
	devoe => ww_devoe,
	o => alu_msb_aoutput_o);

mux_data_out_a2_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_data_out(2),
	dataout => mux_data_out_a2_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y21_N15
mux_data_out_a2_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_data_out_a2_a_ainput_I_driver,
	o => mux_data_out_a2_a_ainput_o);

mux_pc_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_pc(1),
	dataout => mux_pc_a1_a_ainput_I_driver);

-- Location: IOIBUF_X32_Y43_N29
mux_pc_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_pc_a1_a_ainput_I_driver,
	o => mux_pc_a1_a_ainput_o);

mux_alu_left_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_alu_left(0),
	dataout => mux_alu_left_a0_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y30_N22
mux_alu_left_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_alu_left_a0_a_ainput_I_driver,
	o => mux_alu_left_a0_a_ainput_o);

reset_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_reset,
	dataout => reset_ainput_I_driver);

-- Location: IOIBUF_X38_Y43_N29
reset_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => reset_ainput_I_driver,
	o => reset_ainput_o);

data_in_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(0),
	dataout => data_in_a0_a_ainput_I_driver);

-- Location: IOIBUF_X45_Y43_N22
data_in_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a0_a_ainput_I_driver,
	o => data_in_a0_a_ainput_o);

A_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a0_DATAB_driver);

A_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a0_a_ainput_o,
	dataout => A_a0_DATAC_driver);

-- Location: LCCOMB_X35_Y27_N26
A_a0 : cycloneive_lcell_comb
-- Equation(s):
-- A_a0_combout = (!reset_ainput_o & data_in_a0_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a0_DATAB_driver,
	datac => A_a0_DATAC_driver,
	combout => A_a0_combout);

mux_a_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_a(0),
	dataout => mux_a_a0_a_ainput_I_driver);

-- Location: IOIBUF_X54_Y43_N8
mux_a_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_a_a0_a_ainput_I_driver,
	o => mux_a_a0_a_ainput_o);

A_a3_a_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_a_a0_a_ainput_o,
	dataout => A_a3_a_a1_DATAB_driver);

A_a3_a_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a3_a_a1_DATAC_driver);

-- Location: LCCOMB_X39_Y28_N26
A_a3_a_a1 : cycloneive_lcell_comb
-- Equation(s):
-- A_a3_a_a1_combout = (mux_a_a0_a_ainput_o) # (reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a3_a_a1_DATAB_driver,
	datac => A_a3_a_a1_DATAC_driver,
	combout => A_a3_a_a1_combout);

A_a0_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a0_a_CLK_driver);

A_a0_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a0_combout,
	dataout => A_a0_a_ASDATA_driver);

A_a0_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a0_a_ENA_driver);

-- Location: FF_X37_Y29_N1
A_a0_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a0_a_CLK_driver,
	asdata => A_a0_a_ASDATA_driver,
	sload => VCC,
	ena => A_a0_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(0));

mux_lo_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_lo(1),
	dataout => mux_lo_a1_a_ainput_I_driver);

-- Location: IOIBUF_X18_Y43_N1
mux_lo_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_lo_a1_a_ainput_I_driver,
	o => mux_lo_a1_a_ainput_o);

data_in_a31_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(31),
	dataout => data_in_a31_a_ainput_I_driver);

-- Location: IOIBUF_X14_Y43_N15
data_in_a31_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a31_a_ainput_I_driver,
	o => data_in_a31_a_ainput_o);

data_in_a29_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(29),
	dataout => data_in_a29_a_ainput_I_driver);

-- Location: IOIBUF_X34_Y0_N8
data_in_a29_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a29_a_ainput_I_driver,
	o => data_in_a29_a_ainput_o);

data_in_a28_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(28),
	dataout => data_in_a28_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y34_N15
data_in_a28_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a28_a_ainput_I_driver,
	o => data_in_a28_a_ainput_o);

LO_a29_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(27),
	dataout => LO_a29_DATAA_driver);

LO_a29_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a29_DATAB_driver);

LO_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a28_a_ainput_o,
	dataout => LO_a29_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N18
LO_a29 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a29_combout = (mux_lo_a1_a_ainput_o & ((data_in_a28_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a29_DATAA_driver,
	datab => LO_a29_DATAB_driver,
	datad => LO_a29_DATAD_driver,
	combout => LO_a29_combout);

mux_lo_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_lo(0),
	dataout => mux_lo_a0_a_ainput_I_driver);

-- Location: IOIBUF_X48_Y0_N22
mux_lo_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_lo_a0_a_ainput_I_driver,
	o => mux_lo_a0_a_ainput_o);

LO_a0_a_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a0_a_a1_DATAB_driver);

LO_a0_a_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a0_a_a1_DATAC_driver);

LO_a0_a_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a0_a_ainput_o,
	dataout => LO_a0_a_a1_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N14
LO_a0_a_a1 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a0_a_a1_combout = (reset_ainput_o) # (mux_lo_a1_a_ainput_o $ (mux_lo_a0_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a0_a_a1_DATAB_driver,
	datac => LO_a0_a_a1_DATAC_driver,
	datad => LO_a0_a_a1_DATAD_driver,
	combout => LO_a0_a_a1_combout);

LO_a28_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a28_a_CLK_driver);

LO_a28_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a29_combout,
	dataout => LO_a28_a_D_driver);

LO_a28_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a28_a_SCLR_driver);

LO_a28_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a28_a_ENA_driver);

-- Location: FF_X35_Y27_N19
LO_a28_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a28_a_CLK_driver,
	d => LO_a28_a_D_driver,
	sclr => LO_a28_a_SCLR_driver,
	ena => LO_a28_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(28));

LO_a30_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a29_a_ainput_o,
	dataout => LO_a30_DATAB_driver);

LO_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a30_DATAC_driver);

LO_a30_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(28),
	dataout => LO_a30_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N14
LO_a30 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a30_combout = (mux_lo_a1_a_ainput_o & (data_in_a29_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(28))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a30_DATAB_driver,
	datac => LO_a30_DATAC_driver,
	datad => LO_a30_DATAD_driver,
	combout => LO_a30_combout);

LO_a29_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a29_a_CLK_driver);

LO_a29_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a30_combout,
	dataout => LO_a29_a_D_driver);

LO_a29_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a29_a_SCLR_driver);

LO_a29_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a29_a_ENA_driver);

-- Location: FF_X35_Y27_N15
LO_a29_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a29_a_CLK_driver,
	d => LO_a29_a_D_driver,
	sclr => LO_a29_a_SCLR_driver,
	ena => LO_a29_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(29));

LO_a31_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a30_a_ainput_o,
	dataout => LO_a31_DATAA_driver);

LO_a31_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(29),
	dataout => LO_a31_DATAB_driver);

LO_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a31_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N6
LO_a31 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a31_combout = (mux_lo_a1_a_ainput_o & (data_in_a30_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(29))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a31_DATAA_driver,
	datab => LO_a31_DATAB_driver,
	datad => LO_a31_DATAD_driver,
	combout => LO_a31_combout);

LO_a30_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a30_a_CLK_driver);

LO_a30_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a31_combout,
	dataout => LO_a30_a_D_driver);

LO_a30_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a30_a_SCLR_driver);

LO_a30_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a30_a_ENA_driver);

-- Location: FF_X35_Y27_N7
LO_a30_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a30_a_CLK_driver,
	d => LO_a30_a_D_driver,
	sclr => LO_a30_a_SCLR_driver,
	ena => LO_a30_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(30));

LO_a32_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a32_DATAB_driver);

LO_a32_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a31_a_ainput_o,
	dataout => LO_a32_DATAC_driver);

LO_a32_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(30),
	dataout => LO_a32_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N2
LO_a32 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a32_combout = (mux_lo_a1_a_ainput_o & (data_in_a31_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a32_DATAB_driver,
	datac => LO_a32_DATAC_driver,
	datad => LO_a32_DATAD_driver,
	combout => LO_a32_combout);

LO_a31_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a31_a_CLK_driver);

LO_a31_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a32_combout,
	dataout => LO_a31_a_ASDATA_driver);

LO_a31_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a31_a_SCLR_driver);

LO_a31_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a31_a_ENA_driver);

-- Location: FF_X35_Y29_N9
LO_a31_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a31_a_CLK_driver,
	asdata => LO_a31_a_ASDATA_driver,
	sclr => LO_a31_a_SCLR_driver,
	sload => VCC,
	ena => LO_a31_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(31));

left_a0_a_a31_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a0_a_a31_DATAA_driver);

left_a0_a_a31_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a0_a_a31_DATAB_driver);

left_a0_a_a31_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(0),
	dataout => left_a0_a_a31_DATAC_driver);

left_a0_a_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(31),
	dataout => left_a0_a_a31_DATAD_driver);

-- Location: LCCOMB_X37_Y29_N0
left_a0_a_a31 : cycloneive_lcell_comb
-- Equation(s):
-- left_a0_a_a31_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((LO(31))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a0_a_a31_DATAA_driver,
	datab => left_a0_a_a31_DATAB_driver,
	datac => left_a0_a_a31_DATAC_driver,
	datad => left_a0_a_a31_DATAD_driver,
	combout => left_a0_a_a31_combout);

left_a0_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(0),
	dataout => left_a0_a_DATAA_driver);

left_a0_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a0_a_DATAB_driver);

left_a0_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(0),
	dataout => left_a0_a_DATAC_driver);

left_a0_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a0_a_a31_combout,
	dataout => left_a0_a_DATAD_driver);

-- Location: LCCOMB_X37_Y29_N4
left_a0_a : cycloneive_lcell_comb
-- Equation(s):
-- left(0) = (mux_alu_left_a0_a_ainput_o & ((left_a0_a_a31_combout & (HI(0))) # (!left_a0_a_a31_combout & ((PC(0)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a0_a_a31_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a0_a_DATAA_driver,
	datab => left_a0_a_DATAB_driver,
	datac => left_a0_a_DATAC_driver,
	datad => left_a0_a_DATAD_driver,
	combout => left(0));

Add0_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => Add0_a1_DATAA_driver);

-- Location: LCCOMB_X37_Y29_N14
Add0_a1 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a1_cout = CARRY(alu_add_sub_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a1_DATAA_driver,
	datad => VCC,
	cout => Add0_a1_cout);

Add0_a2_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a127_combout,
	dataout => Add0_a2_DATAA_driver);

Add0_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(0),
	dataout => Add0_a2_DATAB_driver);

Add0_a2_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a1_cout,
	dataout => Add0_a2_CIN_driver);

-- Location: LCCOMB_X37_Y29_N16
Add0_a2 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a2_combout = (op2_real_a127_combout & ((left(0) & (Add0_a1_cout & VCC)) # (!left(0) & (!Add0_a1_cout)))) # (!op2_real_a127_combout & ((left(0) & (!Add0_a1_cout)) # (!left(0) & ((Add0_a1_cout) # (GND)))))
-- Add0_a3 = CARRY((op2_real_a127_combout & (!left(0) & !Add0_a1_cout)) # (!op2_real_a127_combout & ((!Add0_a1_cout) # (!left(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a2_DATAA_driver,
	datab => Add0_a2_DATAB_driver,
	datad => VCC,
	cin => Add0_a2_CIN_driver,
	combout => Add0_a2_combout,
	cout => Add0_a3);

PC_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a0_DATAB_driver);

PC_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a2_combout,
	dataout => PC_a0_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N8
PC_a0 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a0_combout = (!mux_pc_a1_a_ainput_o & Add0_a2_combout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => PC_a0_DATAB_driver,
	datad => PC_a0_DATAD_driver,
	combout => PC_a0_combout);

mux_pc_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_pc(0),
	dataout => mux_pc_a0_a_ainput_I_driver);

-- Location: IOIBUF_X32_Y43_N22
mux_pc_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_pc_a0_a_ainput_I_driver,
	o => mux_pc_a0_a_ainput_o);

PC_a1_a_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a1_a_a1_DATAB_driver);

PC_a1_a_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a1_a_a1_DATAC_driver);

PC_a1_a_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a0_a_ainput_o,
	dataout => PC_a1_a_a1_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N8
PC_a1_a_a1 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a1_a_a1_combout = (mux_pc_a1_a_ainput_o) # ((reset_ainput_o) # (mux_pc_a0_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => PC_a1_a_a1_DATAB_driver,
	datac => PC_a1_a_a1_DATAC_driver,
	datad => PC_a1_a_a1_DATAD_driver,
	combout => PC_a1_a_a1_combout);

PC_a0_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a0_a_CLK_driver);

PC_a0_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a0_combout,
	dataout => PC_a0_a_D_driver);

PC_a0_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a0_a_ainput_o,
	dataout => PC_a0_a_ASDATA_driver);

PC_a0_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a0_a_SCLR_driver);

PC_a0_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a0_a_SLOAD_driver);

PC_a0_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a0_a_ENA_driver);

-- Location: FF_X39_Y29_N9
PC_a0_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a0_a_CLK_driver,
	d => PC_a0_a_D_driver,
	asdata => PC_a0_a_ASDATA_driver,
	sclr => PC_a0_a_SCLR_driver,
	sload => PC_a0_a_SLOAD_driver,
	ena => PC_a0_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(0));

mux_data_out_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_data_out(1),
	dataout => mux_data_out_a1_a_ainput_I_driver);

-- Location: IOIBUF_X36_Y43_N8
mux_data_out_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_data_out_a1_a_ainput_I_driver,
	o => mux_data_out_a1_a_ainput_o);

mux_data_out_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_data_out(0),
	dataout => mux_data_out_a0_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y21_N22
mux_data_out_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_data_out_a0_a_ainput_I_driver,
	o => mux_data_out_a0_a_ainput_o);

data_out_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a0_DATAB_driver);

data_out_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a0_DATAC_driver);

data_out_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a0_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N12
data_out_a0 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a0_combout = (!mux_data_out_a1_a_ainput_o & (mux_data_out_a2_a_ainput_o & !mux_data_out_a0_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => data_out_a0_DATAB_driver,
	datac => data_out_a0_DATAC_driver,
	datad => data_out_a0_DATAD_driver,
	combout => data_out_a0_combout);

alu_add_sub_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_alu_add_sub,
	dataout => alu_add_sub_ainput_I_driver);

-- Location: IOIBUF_X67_Y31_N1
alu_add_sub_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => alu_add_sub_ainput_I_driver,
	o => alu_add_sub_ainput_o);

mux_hi_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_hi(1),
	dataout => mux_hi_a1_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y14_N22
mux_hi_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_hi_a1_a_ainput_I_driver,
	o => mux_hi_a1_a_ainput_o);

HI_a14_a_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => HI_a14_a_a0_DATAC_driver);

HI_a14_a_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_hi_a1_a_ainput_o,
	dataout => HI_a14_a_a0_DATAD_driver);

-- Location: LCCOMB_X41_Y28_N0
HI_a14_a_a0 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a14_a_a0_combout = (reset_ainput_o) # (mux_hi_a1_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => HI_a14_a_a0_DATAC_driver,
	datad => HI_a14_a_a0_DATAD_driver,
	combout => HI_a14_a_a0_combout);

mux_alu_right_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_alu_right(0),
	dataout => mux_alu_right_a0_a_ainput_I_driver);

-- Location: IOIBUF_X38_Y43_N1
mux_alu_right_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_alu_right_a0_a_ainput_I_driver,
	o => mux_alu_right_a0_a_ainput_o);

A_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a29_a_ainput_o,
	dataout => A_a30_DATAC_driver);

A_a30_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a30_DATAD_driver);

-- Location: LCCOMB_X35_Y25_N16
A_a30 : cycloneive_lcell_comb
-- Equation(s):
-- A_a30_combout = (data_in_a29_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a30_DATAC_driver,
	datad => A_a30_DATAD_driver,
	combout => A_a30_combout);

mux_b_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_b(0),
	dataout => mux_b_a0_a_ainput_I_driver);

-- Location: IOIBUF_X52_Y0_N22
mux_b_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_b_a0_a_ainput_I_driver,
	o => mux_b_a0_a_ainput_o);

B_a22_a_a0_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => B_a22_a_a0_DATAA_driver);

B_a22_a_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_b_a0_a_ainput_o,
	dataout => B_a22_a_a0_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N14
B_a22_a_a0 : cycloneive_lcell_comb
-- Equation(s):
-- B_a22_a_a0_combout = (reset_ainput_o) # (mux_b_a0_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => B_a22_a_a0_DATAA_driver,
	datad => B_a22_a_a0_DATAD_driver,
	combout => B_a22_a_a0_combout);

B_a29_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a29_a_CLK_driver);

B_a29_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a30_combout,
	dataout => B_a29_a_ASDATA_driver);

B_a29_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a29_a_ENA_driver);

-- Location: FF_X37_Y24_N15
B_a29_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a29_a_CLK_driver,
	asdata => B_a29_a_ASDATA_driver,
	sload => VCC,
	ena => B_a29_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(29));

mux_alu_right_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_alu_right(1),
	dataout => mux_alu_right_a1_a_ainput_I_driver);

-- Location: IOIBUF_X41_Y0_N29
mux_alu_right_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_alu_right_a1_a_ainput_I_driver,
	o => mux_alu_right_a1_a_ainput_o);

op2_real_a182_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a29_a_ainput_o,
	dataout => op2_real_a182_DATAA_driver);

op2_real_a182_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a182_DATAB_driver);

op2_real_a182_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(29),
	dataout => op2_real_a182_DATAC_driver);

op2_real_a182_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a182_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N14
op2_real_a182 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a182_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a29_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(29)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a182_DATAA_driver,
	datab => op2_real_a182_DATAB_driver,
	datac => op2_real_a182_DATAC_driver,
	datad => op2_real_a182_DATAD_driver,
	combout => op2_real_a182_combout);

op2_real_a183_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a183_DATAB_driver);

op2_real_a183_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a182_combout,
	dataout => op2_real_a183_DATAC_driver);

op2_real_a183_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a183_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N24
op2_real_a183 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a183_combout = (alu_add_sub_ainput_o & (op2_real_a182_combout $ (!mux_alu_right_a1_a_ainput_o))) # (!alu_add_sub_ainput_o & (op2_real_a182_combout & !mux_alu_right_a1_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a183_DATAB_driver,
	datac => op2_real_a183_DATAC_driver,
	datad => op2_real_a183_DATAD_driver,
	combout => op2_real_a183_combout);

data_in_a27_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(27),
	dataout => data_in_a27_a_ainput_I_driver);

-- Location: IOIBUF_X32_Y43_N1
data_in_a27_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a27_a_ainput_I_driver,
	o => data_in_a27_a_ainput_o);

A_a28_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a27_a_ainput_o,
	dataout => A_a28_DATAC_driver);

A_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a28_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N22
A_a28 : cycloneive_lcell_comb
-- Equation(s):
-- A_a28_combout = (data_in_a27_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a28_DATAC_driver,
	datad => A_a28_DATAD_driver,
	combout => A_a28_combout);

A_a27_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a27_a_CLK_driver);

A_a27_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a28_combout,
	dataout => A_a27_a_ASDATA_driver);

A_a27_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a27_a_ENA_driver);

-- Location: FF_X38_Y27_N5
A_a27_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a27_a_CLK_driver,
	asdata => A_a27_a_ASDATA_driver,
	sload => VCC,
	ena => A_a27_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(27));

mux_alu_left_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_alu_left(1),
	dataout => mux_alu_left_a1_a_ainput_I_driver);

-- Location: IOIBUF_X36_Y43_N22
mux_alu_left_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_alu_left_a1_a_ainput_I_driver,
	o => mux_alu_left_a1_a_ainput_o);

data_in_a26_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(26),
	dataout => data_in_a26_a_ainput_I_driver);

-- Location: IOIBUF_X34_Y0_N22
data_in_a26_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a26_a_ainput_I_driver,
	o => data_in_a26_a_ainput_o);

A_a27_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a26_a_ainput_o,
	dataout => A_a27_DATAC_driver);

A_a27_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a27_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N26
A_a27 : cycloneive_lcell_comb
-- Equation(s):
-- A_a27_combout = (data_in_a26_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a27_DATAC_driver,
	datad => A_a27_DATAD_driver,
	combout => A_a27_combout);

A_a26_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a26_a_CLK_driver);

A_a26_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a27_combout,
	dataout => A_a26_a_ASDATA_driver);

A_a26_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a26_a_ENA_driver);

-- Location: FF_X38_Y27_N15
A_a26_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a26_a_CLK_driver,
	asdata => A_a26_a_ASDATA_driver,
	sload => VCC,
	ena => A_a26_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(26));

left_a26_a_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(25),
	dataout => left_a26_a_a5_DATAA_driver);

left_a26_a_a5_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a26_a_a5_DATAB_driver);

left_a26_a_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(26),
	dataout => left_a26_a_a5_DATAC_driver);

left_a26_a_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a26_a_a5_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N14
left_a26_a_a5 : cycloneive_lcell_comb
-- Equation(s):
-- left_a26_a_a5_combout = (mux_alu_left_a1_a_ainput_o & ((HI(25)) # ((mux_alu_left_a0_a_ainput_o)))) # (!mux_alu_left_a1_a_ainput_o & (((A(26) & !mux_alu_left_a0_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a26_a_a5_DATAA_driver,
	datab => left_a26_a_a5_DATAB_driver,
	datac => left_a26_a_a5_DATAC_driver,
	datad => left_a26_a_a5_DATAD_driver,
	combout => left_a26_a_a5_combout);

PC_a27_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a24_a_ainput_o,
	dataout => PC_a27_DATAA_driver);

PC_a27_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a27_DATAB_driver);

PC_a27_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a54_combout,
	dataout => PC_a27_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N6
PC_a27 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a27_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a24_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a54_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a27_DATAA_driver,
	datab => PC_a27_DATAB_driver,
	datad => PC_a27_DATAD_driver,
	combout => PC_a27_combout);

PC_a26_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a26_a_CLK_driver);

PC_a26_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a27_combout,
	dataout => PC_a26_a_D_driver);

PC_a26_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a26_a_ainput_o,
	dataout => PC_a26_a_ASDATA_driver);

PC_a26_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a26_a_SCLR_driver);

PC_a26_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a26_a_SLOAD_driver);

PC_a26_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a26_a_ENA_driver);

-- Location: FF_X36_Y26_N7
PC_a26_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a26_a_CLK_driver,
	d => PC_a26_a_D_driver,
	asdata => PC_a26_a_ASDATA_driver,
	sclr => PC_a26_a_SCLR_driver,
	sload => PC_a26_a_SLOAD_driver,
	ena => PC_a26_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(26));

left_a26_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(26),
	dataout => left_a26_a_DATAA_driver);

left_a26_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a26_a_DATAB_driver);

left_a26_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a26_a_a5_combout,
	dataout => left_a26_a_DATAC_driver);

left_a26_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(26),
	dataout => left_a26_a_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N24
left_a26_a : cycloneive_lcell_comb
-- Equation(s):
-- left(26) = (mux_alu_left_a0_a_ainput_o & ((left_a26_a_a5_combout & (HI(26))) # (!left_a26_a_a5_combout & ((PC(26)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a26_a_a5_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a26_a_DATAA_driver,
	datab => left_a26_a_DATAB_driver,
	datac => left_a26_a_DATAC_driver,
	datad => left_a26_a_DATAD_driver,
	combout => left(26));

intr_pc_a23_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(23),
	dataout => intr_pc_a23_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y36_N1
intr_pc_a23_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a23_a_ainput_I_driver,
	o => intr_pc_a23_a_ainput_o);

PC_a26_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a52_combout,
	dataout => PC_a26_DATAA_driver);

PC_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a26_DATAB_driver);

PC_a26_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a23_a_ainput_o,
	dataout => PC_a26_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N4
PC_a26 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a26_combout = (mux_pc_a1_a_ainput_o & ((intr_pc_a23_a_ainput_o))) # (!mux_pc_a1_a_ainput_o & (Add0_a52_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a26_DATAA_driver,
	datab => PC_a26_DATAB_driver,
	datad => PC_a26_DATAD_driver,
	combout => PC_a26_combout);

data_in_a25_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(25),
	dataout => data_in_a25_a_ainput_I_driver);

-- Location: IOIBUF_X36_Y43_N15
data_in_a25_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a25_a_ainput_I_driver,
	o => data_in_a25_a_ainput_o);

PC_a25_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a25_a_CLK_driver);

PC_a25_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a26_combout,
	dataout => PC_a25_a_D_driver);

PC_a25_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a25_a_ainput_o,
	dataout => PC_a25_a_ASDATA_driver);

PC_a25_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a25_a_SCLR_driver);

PC_a25_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a25_a_SLOAD_driver);

PC_a25_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a25_a_ENA_driver);

-- Location: FF_X36_Y26_N5
PC_a25_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a25_a_CLK_driver,
	d => PC_a25_a_D_driver,
	asdata => PC_a25_a_ASDATA_driver,
	sclr => PC_a25_a_SCLR_driver,
	sload => PC_a25_a_SLOAD_driver,
	ena => PC_a25_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(25));

A_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a26_DATAB_driver);

A_a26_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a25_a_ainput_o,
	dataout => A_a26_DATAC_driver);

-- Location: LCCOMB_X35_Y28_N30
A_a26 : cycloneive_lcell_comb
-- Equation(s):
-- A_a26_combout = (!reset_ainput_o & data_in_a25_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a26_DATAB_driver,
	datac => A_a26_DATAC_driver,
	combout => A_a26_combout);

A_a25_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a25_a_CLK_driver);

A_a25_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a26_combout,
	dataout => A_a25_a_ASDATA_driver);

A_a25_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a25_a_ENA_driver);

-- Location: FF_X37_Y26_N31
A_a25_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a25_a_CLK_driver,
	asdata => A_a25_a_ASDATA_driver,
	sload => VCC,
	ena => A_a25_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(25));

intr_right_a22_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(22),
	dataout => intr_right_a22_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y14_N8
intr_right_a22_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a22_a_ainput_I_driver,
	o => intr_right_a22_a_ainput_o);

data_in_a22_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(22),
	dataout => data_in_a22_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y17_N22
data_in_a22_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a22_a_ainput_I_driver,
	o => data_in_a22_a_ainput_o);

A_a23_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a23_DATAC_driver);

A_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a22_a_ainput_o,
	dataout => A_a23_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N16
A_a23 : cycloneive_lcell_comb
-- Equation(s):
-- A_a23_combout = (!reset_ainput_o & data_in_a22_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a23_DATAC_driver,
	datad => A_a23_DATAD_driver,
	combout => A_a23_combout);

B_a22_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a22_a_CLK_driver);

B_a22_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a23_combout,
	dataout => B_a22_a_ASDATA_driver);

B_a22_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a22_a_ENA_driver);

-- Location: FF_X38_Y25_N5
B_a22_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a22_a_CLK_driver,
	asdata => B_a22_a_ASDATA_driver,
	sload => VCC,
	ena => B_a22_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(22));

op2_real_a168_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a168_DATAA_driver);

op2_real_a168_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a22_a_ainput_o,
	dataout => op2_real_a168_DATAB_driver);

op2_real_a168_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(22),
	dataout => op2_real_a168_DATAC_driver);

op2_real_a168_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a168_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N4
op2_real_a168 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a168_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a22_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(22)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a168_DATAA_driver,
	datab => op2_real_a168_DATAB_driver,
	datac => op2_real_a168_DATAC_driver,
	datad => op2_real_a168_DATAD_driver,
	combout => op2_real_a168_combout);

op2_real_a169_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a169_DATAA_driver);

op2_real_a169_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a168_combout,
	dataout => op2_real_a169_DATAC_driver);

op2_real_a169_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a169_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N22
op2_real_a169 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a169_combout = (mux_alu_right_a1_a_ainput_o & (op2_real_a168_combout & alu_add_sub_ainput_o)) # (!mux_alu_right_a1_a_ainput_o & (op2_real_a168_combout $ (alu_add_sub_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a169_DATAA_driver,
	datac => op2_real_a169_DATAC_driver,
	datad => op2_real_a169_DATAD_driver,
	combout => op2_real_a169_combout);

intr_right_a21_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(21),
	dataout => intr_right_a21_a_ainput_I_driver);

-- Location: IOIBUF_X50_Y0_N15
intr_right_a21_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a21_a_ainput_I_driver,
	o => intr_right_a21_a_ainput_o);

data_in_a21_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(21),
	dataout => data_in_a21_a_ainput_I_driver);

-- Location: IOIBUF_X38_Y0_N8
data_in_a21_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a21_a_ainput_I_driver,
	o => data_in_a21_a_ainput_o);

A_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a22_DATAB_driver);

A_a22_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a21_a_ainput_o,
	dataout => A_a22_DATAC_driver);

-- Location: LCCOMB_X39_Y28_N20
A_a22 : cycloneive_lcell_comb
-- Equation(s):
-- A_a22_combout = (!reset_ainput_o & data_in_a21_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a22_DATAB_driver,
	datac => A_a22_DATAC_driver,
	combout => A_a22_combout);

B_a21_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a21_a_CLK_driver);

B_a21_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a22_combout,
	dataout => B_a21_a_ASDATA_driver);

B_a21_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a21_a_ENA_driver);

-- Location: FF_X38_Y25_N27
B_a21_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a21_a_CLK_driver,
	asdata => B_a21_a_ASDATA_driver,
	sload => VCC,
	ena => B_a21_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(21));

op2_real_a166_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a166_DATAA_driver);

op2_real_a166_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a21_a_ainput_o,
	dataout => op2_real_a166_DATAB_driver);

op2_real_a166_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(21),
	dataout => op2_real_a166_DATAC_driver);

op2_real_a166_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a166_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N26
op2_real_a166 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a166_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a21_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(21)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a166_DATAA_driver,
	datab => op2_real_a166_DATAB_driver,
	datac => op2_real_a166_DATAC_driver,
	datad => op2_real_a166_DATAD_driver,
	combout => op2_real_a166_combout);

op2_real_a167_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a167_DATAA_driver);

op2_real_a167_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a166_combout,
	dataout => op2_real_a167_DATAC_driver);

op2_real_a167_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a167_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N0
op2_real_a167 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a167_combout = (mux_alu_right_a1_a_ainput_o & (op2_real_a166_combout & alu_add_sub_ainput_o)) # (!mux_alu_right_a1_a_ainput_o & (op2_real_a166_combout $ (alu_add_sub_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a167_DATAA_driver,
	datac => op2_real_a167_DATAC_driver,
	datad => op2_real_a167_DATAD_driver,
	combout => op2_real_a167_combout);

intr_right_a20_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(20),
	dataout => intr_right_a20_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y17_N15
intr_right_a20_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a20_a_ainput_I_driver,
	o => intr_right_a20_a_ainput_o);

A_a21_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a20_a_ainput_o,
	dataout => A_a21_DATAA_driver);

A_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a21_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N0
A_a21 : cycloneive_lcell_comb
-- Equation(s):
-- A_a21_combout = (data_in_a20_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a21_DATAA_driver,
	datad => A_a21_DATAD_driver,
	combout => A_a21_combout);

B_a20_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a20_a_CLK_driver);

B_a20_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a21_combout,
	dataout => B_a20_a_ASDATA_driver);

B_a20_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a20_a_ENA_driver);

-- Location: FF_X38_Y25_N21
B_a20_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a20_a_CLK_driver,
	asdata => B_a20_a_ASDATA_driver,
	sload => VCC,
	ena => B_a20_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(20));

op2_real_a164_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a164_DATAA_driver);

op2_real_a164_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a20_a_ainput_o,
	dataout => op2_real_a164_DATAB_driver);

op2_real_a164_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(20),
	dataout => op2_real_a164_DATAC_driver);

op2_real_a164_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a164_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N20
op2_real_a164 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a164_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a20_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(20)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a164_DATAA_driver,
	datab => op2_real_a164_DATAB_driver,
	datac => op2_real_a164_DATAC_driver,
	datad => op2_real_a164_DATAD_driver,
	combout => op2_real_a164_combout);

op2_real_a165_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a165_DATAB_driver);

op2_real_a165_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a165_DATAC_driver);

op2_real_a165_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a164_combout,
	dataout => op2_real_a165_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N14
op2_real_a165 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a165_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a164_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a164_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a165_DATAB_driver,
	datac => op2_real_a165_DATAC_driver,
	datad => op2_real_a165_DATAD_driver,
	combout => op2_real_a165_combout);

intr_right_a19_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(19),
	dataout => intr_right_a19_a_ainput_I_driver);

-- Location: IOIBUF_X25_Y43_N22
intr_right_a19_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a19_a_ainput_I_driver,
	o => intr_right_a19_a_ainput_o);

data_in_a19_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(19),
	dataout => data_in_a19_a_ainput_I_driver);

-- Location: IOIBUF_X27_Y43_N1
data_in_a19_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a19_a_ainput_I_driver,
	o => data_in_a19_a_ainput_o);

A_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a20_DATAB_driver);

A_a20_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a19_a_ainput_o,
	dataout => A_a20_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N30
A_a20 : cycloneive_lcell_comb
-- Equation(s):
-- A_a20_combout = (!reset_ainput_o & data_in_a19_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a20_DATAB_driver,
	datad => A_a20_DATAD_driver,
	combout => A_a20_combout);

B_a19_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a19_a_CLK_driver);

B_a19_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a20_combout,
	dataout => B_a19_a_ASDATA_driver);

B_a19_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a19_a_ENA_driver);

-- Location: FF_X37_Y31_N5
B_a19_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a19_a_CLK_driver,
	asdata => B_a19_a_ASDATA_driver,
	sload => VCC,
	ena => B_a19_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(19));

op2_real_a162_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a162_DATAA_driver);

op2_real_a162_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a19_a_ainput_o,
	dataout => op2_real_a162_DATAB_driver);

op2_real_a162_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(19),
	dataout => op2_real_a162_DATAC_driver);

op2_real_a162_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a162_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N4
op2_real_a162 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a162_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a19_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(19)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a162_DATAA_driver,
	datab => op2_real_a162_DATAB_driver,
	datac => op2_real_a162_DATAC_driver,
	datad => op2_real_a162_DATAD_driver,
	combout => op2_real_a162_combout);

op2_real_a163_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a163_DATAA_driver);

op2_real_a163_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a163_DATAB_driver);

op2_real_a163_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a162_combout,
	dataout => op2_real_a163_DATAC_driver);

-- Location: LCCOMB_X37_Y31_N30
op2_real_a163 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a163_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a162_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a162_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a163_DATAA_driver,
	datab => op2_real_a163_DATAB_driver,
	datac => op2_real_a163_DATAC_driver,
	combout => op2_real_a163_combout);

intr_right_a18_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(18),
	dataout => intr_right_a18_a_ainput_I_driver);

-- Location: IOIBUF_X5_Y43_N22
intr_right_a18_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a18_a_ainput_I_driver,
	o => intr_right_a18_a_ainput_o);

data_in_a18_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(18),
	dataout => data_in_a18_a_ainput_I_driver);

-- Location: IOIBUF_X32_Y43_N15
data_in_a18_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a18_a_ainput_I_driver,
	o => data_in_a18_a_ainput_o);

A_a19_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a19_DATAB_driver);

A_a19_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a18_a_ainput_o,
	dataout => A_a19_DATAC_driver);

-- Location: LCCOMB_X35_Y28_N18
A_a19 : cycloneive_lcell_comb
-- Equation(s):
-- A_a19_combout = (!reset_ainput_o & data_in_a18_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a19_DATAB_driver,
	datac => A_a19_DATAC_driver,
	combout => A_a19_combout);

B_a18_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a18_a_CLK_driver);

B_a18_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a19_combout,
	dataout => B_a18_a_ASDATA_driver);

B_a18_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a18_a_ENA_driver);

-- Location: FF_X37_Y31_N23
B_a18_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a18_a_CLK_driver,
	asdata => B_a18_a_ASDATA_driver,
	sload => VCC,
	ena => B_a18_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(18));

op2_real_a160_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a160_DATAA_driver);

op2_real_a160_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a18_a_ainput_o,
	dataout => op2_real_a160_DATAB_driver);

op2_real_a160_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(18),
	dataout => op2_real_a160_DATAC_driver);

op2_real_a160_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a160_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N22
op2_real_a160 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a160_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a18_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(18)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a160_DATAA_driver,
	datab => op2_real_a160_DATAB_driver,
	datac => op2_real_a160_DATAC_driver,
	datad => op2_real_a160_DATAD_driver,
	combout => op2_real_a160_combout);

op2_real_a161_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a161_DATAB_driver);

op2_real_a161_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a160_combout,
	dataout => op2_real_a161_DATAC_driver);

op2_real_a161_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a161_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N12
op2_real_a161 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a161_combout = (alu_add_sub_ainput_o & (op2_real_a160_combout $ (!mux_alu_right_a1_a_ainput_o))) # (!alu_add_sub_ainput_o & (op2_real_a160_combout & !mux_alu_right_a1_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a161_DATAB_driver,
	datac => op2_real_a161_DATAC_driver,
	datad => op2_real_a161_DATAD_driver,
	combout => op2_real_a161_combout);

intr_pc_a15_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(15),
	dataout => intr_pc_a15_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y35_N1
intr_pc_a15_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a15_a_ainput_I_driver,
	o => intr_pc_a15_a_ainput_o);

A_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a16_a_ainput_o,
	dataout => A_a17_DATAA_driver);

A_a17_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a17_DATAC_driver);

-- Location: LCCOMB_X39_Y28_N2
A_a17 : cycloneive_lcell_comb
-- Equation(s):
-- A_a17_combout = (data_in_a16_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a17_DATAA_driver,
	datac => A_a17_DATAC_driver,
	combout => A_a17_combout);

A_a16_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a16_a_CLK_driver);

A_a16_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a17_combout,
	dataout => A_a16_a_ASDATA_driver);

A_a16_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a16_a_ENA_driver);

-- Location: FF_X38_Y28_N5
A_a16_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a16_a_CLK_driver,
	asdata => A_a16_a_ASDATA_driver,
	sload => VCC,
	ena => A_a16_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(16));

intr_right_a15_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(15),
	dataout => intr_right_a15_a_ainput_I_driver);

-- Location: IOIBUF_X54_Y0_N22
intr_right_a15_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a15_a_ainput_I_driver,
	o => intr_right_a15_a_ainput_o);

A_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a15_a_ainput_o,
	dataout => A_a16_DATAA_driver);

A_a16_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a16_DATAC_driver);

-- Location: LCCOMB_X39_Y28_N0
A_a16 : cycloneive_lcell_comb
-- Equation(s):
-- A_a16_combout = (data_in_a15_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a16_DATAA_driver,
	datac => A_a16_DATAC_driver,
	combout => A_a16_combout);

B_a15_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a15_a_CLK_driver);

B_a15_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a16_combout,
	dataout => B_a15_a_ASDATA_driver);

B_a15_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a15_a_ENA_driver);

-- Location: FF_X38_Y25_N19
B_a15_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a15_a_CLK_driver,
	asdata => B_a15_a_ASDATA_driver,
	sload => VCC,
	ena => B_a15_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(15));

op2_real_a154_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a154_DATAA_driver);

op2_real_a154_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a15_a_ainput_o,
	dataout => op2_real_a154_DATAB_driver);

op2_real_a154_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(15),
	dataout => op2_real_a154_DATAC_driver);

op2_real_a154_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a154_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N18
op2_real_a154 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a154_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a15_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(15)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a154_DATAA_driver,
	datab => op2_real_a154_DATAB_driver,
	datac => op2_real_a154_DATAC_driver,
	datad => op2_real_a154_DATAD_driver,
	combout => op2_real_a154_combout);

op2_real_a155_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a155_DATAB_driver);

op2_real_a155_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a155_DATAC_driver);

op2_real_a155_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a154_combout,
	dataout => op2_real_a155_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N16
op2_real_a155 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a155_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a154_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a154_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a155_DATAB_driver,
	datac => op2_real_a155_DATAC_driver,
	datad => op2_real_a155_DATAD_driver,
	combout => op2_real_a155_combout);

data_in_a12_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(12),
	dataout => data_in_a12_a_ainput_I_driver);

-- Location: IOIBUF_X59_Y43_N1
data_in_a12_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a12_a_ainput_I_driver,
	o => data_in_a12_a_ainput_o);

A_a13_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a13_DATAA_driver);

A_a13_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a12_a_ainput_o,
	dataout => A_a13_DATAC_driver);

-- Location: LCCOMB_X36_Y26_N24
A_a13 : cycloneive_lcell_comb
-- Equation(s):
-- A_a13_combout = (!reset_ainput_o & data_in_a12_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a13_DATAA_driver,
	datac => A_a13_DATAC_driver,
	combout => A_a13_combout);

A_a12_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a12_a_CLK_driver);

A_a12_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a13_combout,
	dataout => A_a12_a_ASDATA_driver);

A_a12_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a12_a_ENA_driver);

-- Location: FF_X37_Y26_N9
A_a12_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a12_a_CLK_driver,
	asdata => A_a12_a_ASDATA_driver,
	sload => VCC,
	ena => A_a12_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(12));

intr_right_a7_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(7),
	dataout => intr_right_a7_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y18_N8
intr_right_a7_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a7_a_ainput_I_driver,
	o => intr_right_a7_a_ainput_o);

data_in_a7_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(7),
	dataout => data_in_a7_a_ainput_I_driver);

-- Location: IOIBUF_X45_Y43_N8
data_in_a7_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a7_a_ainput_I_driver,
	o => data_in_a7_a_ainput_o);

A_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a8_DATAA_driver);

A_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a7_a_ainput_o,
	dataout => A_a8_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N0
A_a8 : cycloneive_lcell_comb
-- Equation(s):
-- A_a8_combout = (!reset_ainput_o & data_in_a7_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a8_DATAA_driver,
	datad => A_a8_DATAD_driver,
	combout => A_a8_combout);

B_a7_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a7_a_CLK_driver);

B_a7_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a8_combout,
	dataout => B_a7_a_ASDATA_driver);

B_a7_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a7_a_ENA_driver);

-- Location: FF_X36_Y25_N11
B_a7_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a7_a_CLK_driver,
	asdata => B_a7_a_ASDATA_driver,
	sload => VCC,
	ena => B_a7_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(7));

op2_real_a138_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a138_DATAA_driver);

op2_real_a138_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a7_a_ainput_o,
	dataout => op2_real_a138_DATAB_driver);

op2_real_a138_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(7),
	dataout => op2_real_a138_DATAC_driver);

op2_real_a138_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a138_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N10
op2_real_a138 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a138_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a7_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a138_DATAA_driver,
	datab => op2_real_a138_DATAB_driver,
	datac => op2_real_a138_DATAC_driver,
	datad => op2_real_a138_DATAD_driver,
	combout => op2_real_a138_combout);

op2_real_a139_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a139_DATAA_driver);

op2_real_a139_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a139_DATAC_driver);

op2_real_a139_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a138_combout,
	dataout => op2_real_a139_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N24
op2_real_a139 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a139_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a138_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a138_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a139_DATAA_driver,
	datac => op2_real_a139_DATAC_driver,
	datad => op2_real_a139_DATAD_driver,
	combout => op2_real_a139_combout);

intr_right_a6_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(6),
	dataout => intr_right_a6_a_ainput_I_driver);

-- Location: IOIBUF_X38_Y0_N29
intr_right_a6_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a6_a_ainput_I_driver,
	o => intr_right_a6_a_ainput_o);

A_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a6_a_ainput_o,
	dataout => A_a7_DATAA_driver);

A_a7_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a7_DATAC_driver);

-- Location: LCCOMB_X38_Y30_N6
A_a7 : cycloneive_lcell_comb
-- Equation(s):
-- A_a7_combout = (data_in_a6_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a7_DATAA_driver,
	datac => A_a7_DATAC_driver,
	combout => A_a7_combout);

B_a6_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a6_a_CLK_driver);

B_a6_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a7_combout,
	dataout => B_a6_a_ASDATA_driver);

B_a6_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a6_a_ENA_driver);

-- Location: FF_X38_Y25_N7
B_a6_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a6_a_CLK_driver,
	asdata => B_a6_a_ASDATA_driver,
	sload => VCC,
	ena => B_a6_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(6));

op2_real_a136_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a136_DATAA_driver);

op2_real_a136_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a6_a_ainput_o,
	dataout => op2_real_a136_DATAB_driver);

op2_real_a136_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(6),
	dataout => op2_real_a136_DATAC_driver);

op2_real_a136_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a136_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N6
op2_real_a136 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a136_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a6_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a136_DATAA_driver,
	datab => op2_real_a136_DATAB_driver,
	datac => op2_real_a136_DATAC_driver,
	datad => op2_real_a136_DATAD_driver,
	combout => op2_real_a136_combout);

op2_real_a137_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a137_DATAB_driver);

op2_real_a137_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a137_DATAC_driver);

op2_real_a137_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a136_combout,
	dataout => op2_real_a137_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N8
op2_real_a137 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a137_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a136_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a136_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a137_DATAB_driver,
	datac => op2_real_a137_DATAC_driver,
	datad => op2_real_a137_DATAD_driver,
	combout => op2_real_a137_combout);

intr_right_a5_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(5),
	dataout => intr_right_a5_a_ainput_I_driver);

-- Location: IOIBUF_X52_Y0_N1
intr_right_a5_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a5_a_ainput_I_driver,
	o => intr_right_a5_a_ainput_o);

A_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a5_a_ainput_o,
	dataout => A_a6_DATAA_driver);

A_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a6_DATAC_driver);

-- Location: LCCOMB_X38_Y30_N16
A_a6 : cycloneive_lcell_comb
-- Equation(s):
-- A_a6_combout = (data_in_a5_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a6_DATAA_driver,
	datac => A_a6_DATAC_driver,
	combout => A_a6_combout);

B_a5_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a5_a_CLK_driver);

B_a5_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a6_combout,
	dataout => B_a5_a_ASDATA_driver);

B_a5_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a5_a_ENA_driver);

-- Location: FF_X36_Y25_N21
B_a5_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a5_a_CLK_driver,
	asdata => B_a5_a_ASDATA_driver,
	sload => VCC,
	ena => B_a5_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(5));

op2_real_a134_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a134_DATAA_driver);

op2_real_a134_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a5_a_ainput_o,
	dataout => op2_real_a134_DATAB_driver);

op2_real_a134_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(5),
	dataout => op2_real_a134_DATAC_driver);

op2_real_a134_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a134_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N20
op2_real_a134 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a134_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a5_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a134_DATAA_driver,
	datab => op2_real_a134_DATAB_driver,
	datac => op2_real_a134_DATAC_driver,
	datad => op2_real_a134_DATAD_driver,
	combout => op2_real_a134_combout);

op2_real_a135_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a135_DATAA_driver);

op2_real_a135_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a135_DATAC_driver);

op2_real_a135_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a134_combout,
	dataout => op2_real_a135_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N6
op2_real_a135 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a135_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a134_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a134_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a135_DATAA_driver,
	datac => op2_real_a135_DATAC_driver,
	datad => op2_real_a135_DATAD_driver,
	combout => op2_real_a135_combout);

intr_right_a4_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(4),
	dataout => intr_right_a4_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y16_N15
intr_right_a4_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a4_a_ainput_I_driver,
	o => intr_right_a4_a_ainput_o);

A_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a4_a_ainput_o,
	dataout => A_a5_DATAA_driver);

A_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a5_DATAC_driver);

-- Location: LCCOMB_X38_Y30_N10
A_a5 : cycloneive_lcell_comb
-- Equation(s):
-- A_a5_combout = (data_in_a4_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a5_DATAA_driver,
	datac => A_a5_DATAC_driver,
	combout => A_a5_combout);

B_a4_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a4_a_CLK_driver);

B_a4_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a5_combout,
	dataout => B_a4_a_ASDATA_driver);

B_a4_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a4_a_ENA_driver);

-- Location: FF_X36_Y25_N19
B_a4_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a4_a_CLK_driver,
	asdata => B_a4_a_ASDATA_driver,
	sload => VCC,
	ena => B_a4_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(4));

op2_real_a132_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a132_DATAA_driver);

op2_real_a132_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a4_a_ainput_o,
	dataout => op2_real_a132_DATAB_driver);

op2_real_a132_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(4),
	dataout => op2_real_a132_DATAC_driver);

op2_real_a132_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a132_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N18
op2_real_a132 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a132_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a4_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a132_DATAA_driver,
	datab => op2_real_a132_DATAB_driver,
	datac => op2_real_a132_DATAC_driver,
	datad => op2_real_a132_DATAD_driver,
	combout => op2_real_a132_combout);

op2_real_a133_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a133_DATAA_driver);

op2_real_a133_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a133_DATAC_driver);

op2_real_a133_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a132_combout,
	dataout => op2_real_a133_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N12
op2_real_a133 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a133_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a132_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a132_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a133_DATAA_driver,
	datac => op2_real_a133_DATAC_driver,
	datad => op2_real_a133_DATAD_driver,
	combout => op2_real_a133_combout);

intr_right_a3_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(3),
	dataout => intr_right_a3_a_ainput_I_driver);

-- Location: IOIBUF_X9_Y43_N1
intr_right_a3_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a3_a_ainput_I_driver,
	o => intr_right_a3_a_ainput_o);

A_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a3_a_ainput_o,
	dataout => A_a4_DATAA_driver);

A_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a4_DATAC_driver);

-- Location: LCCOMB_X38_Y29_N8
A_a4 : cycloneive_lcell_comb
-- Equation(s):
-- A_a4_combout = (data_in_a3_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a4_DATAA_driver,
	datac => A_a4_DATAC_driver,
	combout => A_a4_combout);

B_a3_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a3_a_CLK_driver);

B_a3_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a4_combout,
	dataout => B_a3_a_ASDATA_driver);

B_a3_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a3_a_ENA_driver);

-- Location: FF_X37_Y31_N21
B_a3_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a3_a_CLK_driver,
	asdata => B_a3_a_ASDATA_driver,
	sload => VCC,
	ena => B_a3_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(3));

op2_real_a130_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a130_DATAA_driver);

op2_real_a130_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a3_a_ainput_o,
	dataout => op2_real_a130_DATAB_driver);

op2_real_a130_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(3),
	dataout => op2_real_a130_DATAC_driver);

op2_real_a130_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a130_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N20
op2_real_a130 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a130_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a3_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a130_DATAA_driver,
	datab => op2_real_a130_DATAB_driver,
	datac => op2_real_a130_DATAC_driver,
	datad => op2_real_a130_DATAD_driver,
	combout => op2_real_a130_combout);

op2_real_a131_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a131_DATAA_driver);

op2_real_a131_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a131_DATAC_driver);

op2_real_a131_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a130_combout,
	dataout => op2_real_a131_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N6
op2_real_a131 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a131_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a130_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a130_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a131_DATAA_driver,
	datac => op2_real_a131_DATAC_driver,
	datad => op2_real_a131_DATAD_driver,
	combout => op2_real_a131_combout);

intr_right_a2_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(2),
	dataout => intr_right_a2_a_ainput_I_driver);

-- Location: IOIBUF_X9_Y43_N8
intr_right_a2_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a2_a_ainput_I_driver,
	o => intr_right_a2_a_ainput_o);

op2_real_a124_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a124_DATAA_driver);

op2_real_a124_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a124_DATAB_driver);

op2_real_a124_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a2_a_ainput_o,
	dataout => op2_real_a124_DATAC_driver);

op2_real_a124_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a124_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N16
op2_real_a124 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a124_combout = alu_add_sub_ainput_o $ ((((!mux_alu_right_a1_a_ainput_o & intr_right_a2_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101001110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a124_DATAA_driver,
	datab => op2_real_a124_DATAB_driver,
	datac => op2_real_a124_DATAC_driver,
	datad => op2_real_a124_DATAD_driver,
	combout => op2_real_a124_combout);

data_in_a2_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(2),
	dataout => data_in_a2_a_ainput_I_driver);

-- Location: IOIBUF_X56_Y43_N15
data_in_a2_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a2_a_ainput_I_driver,
	o => data_in_a2_a_ainput_o);

A_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a2_a_ainput_o,
	dataout => A_a3_DATAB_driver);

A_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a3_DATAC_driver);

-- Location: LCCOMB_X38_Y30_N24
A_a3 : cycloneive_lcell_comb
-- Equation(s):
-- A_a3_combout = (data_in_a2_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a3_DATAB_driver,
	datac => A_a3_DATAC_driver,
	combout => A_a3_combout);

B_a2_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a2_a_CLK_driver);

B_a2_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_combout,
	dataout => B_a2_a_ASDATA_driver);

B_a2_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a2_a_ENA_driver);

-- Location: FF_X37_Y31_N3
B_a2_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a2_a_CLK_driver,
	asdata => B_a2_a_ASDATA_driver,
	sload => VCC,
	ena => B_a2_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(2));

op2_real_a125_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a125_DATAA_driver);

op2_real_a125_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a124_combout,
	dataout => op2_real_a125_DATAB_driver);

op2_real_a125_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(2),
	dataout => op2_real_a125_DATAC_driver);

op2_real_a125_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a125_DATAD_driver);

-- Location: LCCOMB_X37_Y31_N2
op2_real_a125 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a125_combout = op2_real_a124_combout $ (((!mux_alu_right_a0_a_ainput_o & (!B(2) & !mux_alu_right_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a125_DATAA_driver,
	datab => op2_real_a125_DATAB_driver,
	datac => op2_real_a125_DATAC_driver,
	datad => op2_real_a125_DATAD_driver,
	combout => op2_real_a125_combout);

Add0_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a129_combout,
	dataout => Add0_a4_DATAA_driver);

Add0_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(1),
	dataout => Add0_a4_DATAB_driver);

Add0_a4_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a3,
	dataout => Add0_a4_CIN_driver);

-- Location: LCCOMB_X37_Y29_N18
Add0_a4 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a4_combout = ((op2_real_a129_combout $ (left(1) $ (!Add0_a3)))) # (GND)
-- Add0_a5 = CARRY((op2_real_a129_combout & ((left(1)) # (!Add0_a3))) # (!op2_real_a129_combout & (left(1) & !Add0_a3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a4_DATAA_driver,
	datab => Add0_a4_DATAB_driver,
	datad => VCC,
	cin => Add0_a4_CIN_driver,
	combout => Add0_a4_combout,
	cout => Add0_a5);

HI_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a1_DATAA_driver);

HI_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a2_combout,
	dataout => HI_a1_DATAB_driver);

HI_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(31),
	dataout => HI_a1_DATAC_driver);

HI_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a1_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N2
HI_a1 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a1_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a2_combout)) # (!Add0_a66_combout & ((LO(31))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a1_DATAA_driver,
	datab => HI_a1_DATAB_driver,
	datac => HI_a1_DATAC_driver,
	datad => HI_a1_DATAD_driver,
	combout => HI_a1_combout);

HI_a14_a_a2_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_hi_a0_a_ainput_o,
	dataout => HI_a14_a_a2_DATAA_driver);

HI_a14_a_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => HI_a14_a_a2_DATAC_driver);

HI_a14_a_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_hi_a1_a_ainput_o,
	dataout => HI_a14_a_a2_DATAD_driver);

-- Location: LCCOMB_X41_Y28_N2
HI_a14_a_a2 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a14_a_a2_combout = (reset_ainput_o) # (mux_hi_a0_a_ainput_o $ (mux_hi_a1_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a14_a_a2_DATAA_driver,
	datac => HI_a14_a_a2_DATAC_driver,
	datad => HI_a14_a_a2_DATAD_driver,
	combout => HI_a14_a_a2_combout);

HI_a0_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a0_a_CLK_driver);

HI_a0_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a1_combout,
	dataout => HI_a0_a_D_driver);

HI_a0_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a0_a_ENA_driver);

-- Location: FF_X36_Y29_N3
HI_a0_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a0_a_CLK_driver,
	d => HI_a0_a_D_driver,
	ena => HI_a0_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(0));

HI_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a3_DATAA_driver);

HI_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a4_combout,
	dataout => HI_a3_DATAB_driver);

HI_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(0),
	dataout => HI_a3_DATAC_driver);

HI_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a3_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N14
HI_a3 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a3_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a4_combout)) # (!Add0_a66_combout & ((HI(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a3_DATAA_driver,
	datab => HI_a3_DATAB_driver,
	datac => HI_a3_DATAC_driver,
	datad => HI_a3_DATAD_driver,
	combout => HI_a3_combout);

HI_a1_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a1_a_CLK_driver);

HI_a1_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a3_combout,
	dataout => HI_a1_a_D_driver);

HI_a1_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a1_a_ENA_driver);

-- Location: FF_X36_Y29_N15
HI_a1_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a1_a_CLK_driver,
	d => HI_a1_a_D_driver,
	ena => HI_a1_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(1));

data_in_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(1),
	dataout => data_in_a1_a_ainput_I_driver);

-- Location: IOIBUF_X61_Y43_N15
data_in_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a1_a_ainput_I_driver,
	o => data_in_a1_a_ainput_o);

A_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a1_a_ainput_o,
	dataout => A_a2_DATAB_driver);

A_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a2_DATAC_driver);

-- Location: LCCOMB_X38_Y30_N18
A_a2 : cycloneive_lcell_comb
-- Equation(s):
-- A_a2_combout = (data_in_a1_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a2_DATAB_driver,
	datac => A_a2_DATAC_driver,
	combout => A_a2_combout);

A_a1_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a1_a_CLK_driver);

A_a1_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a2_combout,
	dataout => A_a1_a_ASDATA_driver);

A_a1_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a1_a_ENA_driver);

-- Location: FF_X37_Y29_N7
A_a1_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a1_a_CLK_driver,
	asdata => A_a1_a_ASDATA_driver,
	sload => VCC,
	ena => A_a1_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(1));

left_a1_a_a30_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a1_a_a30_DATAA_driver);

left_a1_a_a30_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a1_a_a30_DATAB_driver);

left_a1_a_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(1),
	dataout => left_a1_a_a30_DATAC_driver);

left_a1_a_a30_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(0),
	dataout => left_a1_a_a30_DATAD_driver);

-- Location: LCCOMB_X37_Y29_N6
left_a1_a_a30 : cycloneive_lcell_comb
-- Equation(s):
-- left_a1_a_a30_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(0))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a1_a_a30_DATAA_driver,
	datab => left_a1_a_a30_DATAB_driver,
	datac => left_a1_a_a30_DATAC_driver,
	datad => left_a1_a_a30_DATAD_driver,
	combout => left_a1_a_a30_combout);

left_a1_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(1),
	dataout => left_a1_a_DATAA_driver);

left_a1_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a1_a_DATAB_driver);

left_a1_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(1),
	dataout => left_a1_a_DATAC_driver);

left_a1_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a1_a_a30_combout,
	dataout => left_a1_a_DATAD_driver);

-- Location: LCCOMB_X37_Y29_N2
left_a1_a : cycloneive_lcell_comb
-- Equation(s):
-- left(1) = (mux_alu_left_a0_a_ainput_o & ((left_a1_a_a30_combout & ((HI(1)))) # (!left_a1_a_a30_combout & (PC(1))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a1_a_a30_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a1_a_DATAA_driver,
	datab => left_a1_a_DATAB_driver,
	datac => left_a1_a_DATAC_driver,
	datad => left_a1_a_DATAD_driver,
	combout => left(1));

Add0_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(2),
	dataout => Add0_a6_DATAA_driver);

Add0_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a125_combout,
	dataout => Add0_a6_DATAB_driver);

Add0_a6_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a5,
	dataout => Add0_a6_CIN_driver);

-- Location: LCCOMB_X37_Y29_N20
Add0_a6 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a6_combout = (left(2) & ((op2_real_a125_combout & (Add0_a5 & VCC)) # (!op2_real_a125_combout & (!Add0_a5)))) # (!left(2) & ((op2_real_a125_combout & (!Add0_a5)) # (!op2_real_a125_combout & ((Add0_a5) # (GND)))))
-- Add0_a7 = CARRY((left(2) & (!op2_real_a125_combout & !Add0_a5)) # (!left(2) & ((!Add0_a5) # (!op2_real_a125_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a6_DATAA_driver,
	datab => Add0_a6_DATAB_driver,
	datad => VCC,
	cin => Add0_a6_CIN_driver,
	combout => Add0_a6_combout,
	cout => Add0_a7);

Add0_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(3),
	dataout => Add0_a8_DATAA_driver);

Add0_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a131_combout,
	dataout => Add0_a8_DATAB_driver);

Add0_a8_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a7,
	dataout => Add0_a8_CIN_driver);

-- Location: LCCOMB_X37_Y29_N22
Add0_a8 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a8_combout = ((left(3) $ (op2_real_a131_combout $ (!Add0_a7)))) # (GND)
-- Add0_a9 = CARRY((left(3) & ((op2_real_a131_combout) # (!Add0_a7))) # (!left(3) & (op2_real_a131_combout & !Add0_a7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a8_DATAA_driver,
	datab => Add0_a8_DATAB_driver,
	datad => VCC,
	cin => Add0_a8_CIN_driver,
	combout => Add0_a8_combout,
	cout => Add0_a9);

Add0_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(4),
	dataout => Add0_a10_DATAA_driver);

Add0_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a133_combout,
	dataout => Add0_a10_DATAB_driver);

Add0_a10_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a9,
	dataout => Add0_a10_CIN_driver);

-- Location: LCCOMB_X37_Y29_N24
Add0_a10 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a10_combout = (left(4) & ((op2_real_a133_combout & (Add0_a9 & VCC)) # (!op2_real_a133_combout & (!Add0_a9)))) # (!left(4) & ((op2_real_a133_combout & (!Add0_a9)) # (!op2_real_a133_combout & ((Add0_a9) # (GND)))))
-- Add0_a11 = CARRY((left(4) & (!op2_real_a133_combout & !Add0_a9)) # (!left(4) & ((!Add0_a9) # (!op2_real_a133_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a10_DATAA_driver,
	datab => Add0_a10_DATAB_driver,
	datad => VCC,
	cin => Add0_a10_CIN_driver,
	combout => Add0_a10_combout,
	cout => Add0_a11);

Add0_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(5),
	dataout => Add0_a12_DATAA_driver);

Add0_a12_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a135_combout,
	dataout => Add0_a12_DATAB_driver);

Add0_a12_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a11,
	dataout => Add0_a12_CIN_driver);

-- Location: LCCOMB_X37_Y29_N26
Add0_a12 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a12_combout = ((left(5) $ (op2_real_a135_combout $ (!Add0_a11)))) # (GND)
-- Add0_a13 = CARRY((left(5) & ((op2_real_a135_combout) # (!Add0_a11))) # (!left(5) & (op2_real_a135_combout & !Add0_a11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a12_DATAA_driver,
	datab => Add0_a12_DATAB_driver,
	datad => VCC,
	cin => Add0_a12_CIN_driver,
	combout => Add0_a12_combout,
	cout => Add0_a13);

Add0_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(6),
	dataout => Add0_a14_DATAA_driver);

Add0_a14_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a137_combout,
	dataout => Add0_a14_DATAB_driver);

Add0_a14_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a13,
	dataout => Add0_a14_CIN_driver);

-- Location: LCCOMB_X37_Y29_N28
Add0_a14 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a14_combout = (left(6) & ((op2_real_a137_combout & (Add0_a13 & VCC)) # (!op2_real_a137_combout & (!Add0_a13)))) # (!left(6) & ((op2_real_a137_combout & (!Add0_a13)) # (!op2_real_a137_combout & ((Add0_a13) # (GND)))))
-- Add0_a15 = CARRY((left(6) & (!op2_real_a137_combout & !Add0_a13)) # (!left(6) & ((!Add0_a13) # (!op2_real_a137_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a14_DATAA_driver,
	datab => Add0_a14_DATAB_driver,
	datad => VCC,
	cin => Add0_a14_CIN_driver,
	combout => Add0_a14_combout,
	cout => Add0_a15);

Add0_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(7),
	dataout => Add0_a16_DATAA_driver);

Add0_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a139_combout,
	dataout => Add0_a16_DATAB_driver);

Add0_a16_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a15,
	dataout => Add0_a16_CIN_driver);

-- Location: LCCOMB_X37_Y29_N30
Add0_a16 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a16_combout = ((left(7) $ (op2_real_a139_combout $ (!Add0_a15)))) # (GND)
-- Add0_a17 = CARRY((left(7) & ((op2_real_a139_combout) # (!Add0_a15))) # (!left(7) & (op2_real_a139_combout & !Add0_a15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a16_DATAA_driver,
	datab => Add0_a16_DATAB_driver,
	datad => VCC,
	cin => Add0_a16_CIN_driver,
	combout => Add0_a16_combout,
	cout => Add0_a17);

HI_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(6),
	dataout => HI_a9_DATAA_driver);

HI_a9_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a9_DATAB_driver);

HI_a9_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a16_combout,
	dataout => HI_a9_DATAC_driver);

HI_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a9_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N12
HI_a9 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a9_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a16_combout))) # (!Add0_a66_combout & (HI(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a9_DATAA_driver,
	datab => HI_a9_DATAB_driver,
	datac => HI_a9_DATAC_driver,
	datad => HI_a9_DATAD_driver,
	combout => HI_a9_combout);

HI_a7_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a7_a_CLK_driver);

HI_a7_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a9_combout,
	dataout => HI_a7_a_D_driver);

HI_a7_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a7_a_ENA_driver);

-- Location: FF_X37_Y30_N13
HI_a7_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a7_a_CLK_driver,
	d => HI_a7_a_D_driver,
	ena => HI_a7_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(7));

intr_right_a8_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(8),
	dataout => intr_right_a8_a_ainput_I_driver);

-- Location: IOIBUF_X25_Y43_N29
intr_right_a8_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a8_a_ainput_I_driver,
	o => intr_right_a8_a_ainput_o);

data_in_a8_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(8),
	dataout => data_in_a8_a_ainput_I_driver);

-- Location: IOIBUF_X56_Y43_N8
data_in_a8_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a8_a_ainput_I_driver,
	o => data_in_a8_a_ainput_o);

A_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a9_DATAA_driver);

A_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a8_a_ainput_o,
	dataout => A_a9_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N20
A_a9 : cycloneive_lcell_comb
-- Equation(s):
-- A_a9_combout = (!reset_ainput_o & data_in_a8_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a9_DATAA_driver,
	datad => A_a9_DATAD_driver,
	combout => A_a9_combout);

B_a8_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a8_a_CLK_driver);

B_a8_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a9_combout,
	dataout => B_a8_a_ASDATA_driver);

B_a8_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a8_a_ENA_driver);

-- Location: FF_X36_Y25_N29
B_a8_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a8_a_CLK_driver,
	asdata => B_a8_a_ASDATA_driver,
	sload => VCC,
	ena => B_a8_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(8));

op2_real_a140_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a140_DATAA_driver);

op2_real_a140_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a8_a_ainput_o,
	dataout => op2_real_a140_DATAB_driver);

op2_real_a140_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(8),
	dataout => op2_real_a140_DATAC_driver);

op2_real_a140_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a140_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N28
op2_real_a140 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a140_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a8_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a140_DATAA_driver,
	datab => op2_real_a140_DATAB_driver,
	datac => op2_real_a140_DATAC_driver,
	datad => op2_real_a140_DATAD_driver,
	combout => op2_real_a140_combout);

op2_real_a141_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a141_DATAA_driver);

op2_real_a141_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a141_DATAC_driver);

op2_real_a141_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a140_combout,
	dataout => op2_real_a141_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N30
op2_real_a141 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a141_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a140_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a140_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a141_DATAA_driver,
	datac => op2_real_a141_DATAC_driver,
	datad => op2_real_a141_DATAD_driver,
	combout => op2_real_a141_combout);

Add0_a18_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(8),
	dataout => Add0_a18_DATAA_driver);

Add0_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a141_combout,
	dataout => Add0_a18_DATAB_driver);

Add0_a18_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a17,
	dataout => Add0_a18_CIN_driver);

-- Location: LCCOMB_X37_Y28_N0
Add0_a18 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a18_combout = (left(8) & ((op2_real_a141_combout & (Add0_a17 & VCC)) # (!op2_real_a141_combout & (!Add0_a17)))) # (!left(8) & ((op2_real_a141_combout & (!Add0_a17)) # (!op2_real_a141_combout & ((Add0_a17) # (GND)))))
-- Add0_a19 = CARRY((left(8) & (!op2_real_a141_combout & !Add0_a17)) # (!left(8) & ((!Add0_a17) # (!op2_real_a141_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a18_DATAA_driver,
	datab => Add0_a18_DATAB_driver,
	datad => VCC,
	cin => Add0_a18_CIN_driver,
	combout => Add0_a18_combout,
	cout => Add0_a19);

HI_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a10_DATAA_driver);

HI_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(7),
	dataout => HI_a10_DATAB_driver);

HI_a10_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a10_DATAC_driver);

HI_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a18_combout,
	dataout => HI_a10_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N8
HI_a10 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a10_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a18_combout))) # (!Add0_a66_combout & (HI(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a10_DATAA_driver,
	datab => HI_a10_DATAB_driver,
	datac => HI_a10_DATAC_driver,
	datad => HI_a10_DATAD_driver,
	combout => HI_a10_combout);

HI_a8_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a8_a_CLK_driver);

HI_a8_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a10_combout,
	dataout => HI_a8_a_D_driver);

HI_a8_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a8_a_ENA_driver);

-- Location: FF_X38_Y26_N9
HI_a8_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a8_a_CLK_driver,
	d => HI_a8_a_D_driver,
	ena => HI_a8_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(8));

PC_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a7_a_ainput_o,
	dataout => PC_a10_DATAA_driver);

PC_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a10_DATAB_driver);

PC_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a20_combout,
	dataout => PC_a10_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N18
PC_a10 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a10_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a7_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a20_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a10_DATAA_driver,
	datab => PC_a10_DATAB_driver,
	datad => PC_a10_DATAD_driver,
	combout => PC_a10_combout);

data_in_a9_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(9),
	dataout => data_in_a9_a_ainput_I_driver);

-- Location: IOIBUF_X43_Y0_N22
data_in_a9_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a9_a_ainput_I_driver,
	o => data_in_a9_a_ainput_o);

PC_a9_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a9_a_CLK_driver);

PC_a9_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a10_combout,
	dataout => PC_a9_a_D_driver);

PC_a9_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a9_a_ainput_o,
	dataout => PC_a9_a_ASDATA_driver);

PC_a9_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a9_a_SCLR_driver);

PC_a9_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a9_a_SLOAD_driver);

PC_a9_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a9_a_ENA_driver);

-- Location: FF_X36_Y26_N19
PC_a9_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a9_a_CLK_driver,
	d => PC_a9_a_D_driver,
	asdata => PC_a9_a_ASDATA_driver,
	sclr => PC_a9_a_SCLR_driver,
	sload => PC_a9_a_SLOAD_driver,
	ena => PC_a9_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(9));

A_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a10_DATAA_driver);

A_a10_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a9_a_ainput_o,
	dataout => A_a10_DATAC_driver);

-- Location: LCCOMB_X36_Y26_N30
A_a10 : cycloneive_lcell_comb
-- Equation(s):
-- A_a10_combout = (!reset_ainput_o & data_in_a9_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a10_DATAA_driver,
	datac => A_a10_DATAC_driver,
	combout => A_a10_combout);

A_a9_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a9_a_CLK_driver);

A_a9_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a10_combout,
	dataout => A_a9_a_ASDATA_driver);

A_a9_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a9_a_ENA_driver);

-- Location: FF_X38_Y26_N11
A_a9_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a9_a_CLK_driver,
	asdata => A_a9_a_ASDATA_driver,
	sload => VCC,
	ena => A_a9_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(9));

left_a9_a_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a9_a_a22_DATAA_driver);

left_a9_a_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a9_a_a22_DATAB_driver);

left_a9_a_a22_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(8),
	dataout => left_a9_a_a22_DATAC_driver);

left_a9_a_a22_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(9),
	dataout => left_a9_a_a22_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N2
left_a9_a_a22 : cycloneive_lcell_comb
-- Equation(s):
-- left_a9_a_a22_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(8))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & ((A(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a9_a_a22_DATAA_driver,
	datab => left_a9_a_a22_DATAB_driver,
	datac => left_a9_a_a22_DATAC_driver,
	datad => left_a9_a_a22_DATAD_driver,
	combout => left_a9_a_a22_combout);

left_a9_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a9_a_DATAA_driver);

left_a9_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(9),
	dataout => left_a9_a_DATAB_driver);

left_a9_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(9),
	dataout => left_a9_a_DATAC_driver);

left_a9_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a9_a_a22_combout,
	dataout => left_a9_a_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N16
left_a9_a : cycloneive_lcell_comb
-- Equation(s):
-- left(9) = (mux_alu_left_a0_a_ainput_o & ((left_a9_a_a22_combout & (HI(9))) # (!left_a9_a_a22_combout & ((PC(9)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a9_a_a22_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a9_a_DATAA_driver,
	datab => left_a9_a_DATAB_driver,
	datac => left_a9_a_DATAC_driver,
	datad => left_a9_a_DATAD_driver,
	combout => left(9));

Add0_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a143_combout,
	dataout => Add0_a20_DATAA_driver);

Add0_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(9),
	dataout => Add0_a20_DATAB_driver);

Add0_a20_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a19,
	dataout => Add0_a20_CIN_driver);

-- Location: LCCOMB_X37_Y28_N2
Add0_a20 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a20_combout = ((op2_real_a143_combout $ (left(9) $ (!Add0_a19)))) # (GND)
-- Add0_a21 = CARRY((op2_real_a143_combout & ((left(9)) # (!Add0_a19))) # (!op2_real_a143_combout & (left(9) & !Add0_a19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a20_DATAA_driver,
	datab => Add0_a20_DATAB_driver,
	datad => VCC,
	cin => Add0_a20_CIN_driver,
	combout => Add0_a20_combout,
	cout => Add0_a21);

HI_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a11_DATAA_driver);

HI_a11_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(8),
	dataout => HI_a11_DATAB_driver);

HI_a11_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a20_combout,
	dataout => HI_a11_DATAC_driver);

HI_a11_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a11_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N24
HI_a11 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a11_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a20_combout))) # (!Add0_a66_combout & (HI(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a11_DATAA_driver,
	datab => HI_a11_DATAB_driver,
	datac => HI_a11_DATAC_driver,
	datad => HI_a11_DATAD_driver,
	combout => HI_a11_combout);

HI_a9_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a9_a_CLK_driver);

HI_a9_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a11_combout,
	dataout => HI_a9_a_D_driver);

HI_a9_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a9_a_ENA_driver);

-- Location: FF_X37_Y26_N25
HI_a9_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a9_a_CLK_driver,
	d => HI_a9_a_D_driver,
	ena => HI_a9_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(9));

data_in_a10_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(10),
	dataout => data_in_a10_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y26_N22
data_in_a10_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a10_a_ainput_I_driver,
	o => data_in_a10_a_ainput_o);

A_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a11_DATAA_driver);

A_a11_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a10_a_ainput_o,
	dataout => A_a11_DATAC_driver);

-- Location: LCCOMB_X36_Y26_N8
A_a11 : cycloneive_lcell_comb
-- Equation(s):
-- A_a11_combout = (!reset_ainput_o & data_in_a10_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a11_DATAA_driver,
	datac => A_a11_DATAC_driver,
	combout => A_a11_combout);

A_a10_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a10_a_CLK_driver);

A_a10_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a11_combout,
	dataout => A_a10_a_ASDATA_driver);

A_a10_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a10_a_ENA_driver);

-- Location: FF_X37_Y26_N21
A_a10_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a10_a_CLK_driver,
	asdata => A_a10_a_ASDATA_driver,
	sload => VCC,
	ena => A_a10_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(10));

left_a10_a_a21_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a10_a_a21_DATAA_driver);

left_a10_a_a21_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a10_a_a21_DATAB_driver);

left_a10_a_a21_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(10),
	dataout => left_a10_a_a21_DATAC_driver);

left_a10_a_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(9),
	dataout => left_a10_a_a21_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N20
left_a10_a_a21 : cycloneive_lcell_comb
-- Equation(s):
-- left_a10_a_a21_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(9)))) # (!mux_alu_left_a1_a_ainput_o & (A(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a10_a_a21_DATAA_driver,
	datab => left_a10_a_a21_DATAB_driver,
	datac => left_a10_a_a21_DATAC_driver,
	datad => left_a10_a_a21_DATAD_driver,
	combout => left_a10_a_a21_combout);

PC_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a8_a_ainput_o,
	dataout => PC_a11_DATAA_driver);

PC_a11_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a11_DATAB_driver);

PC_a11_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a22_combout,
	dataout => PC_a11_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N12
PC_a11 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a11_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a8_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a22_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a11_DATAA_driver,
	datab => PC_a11_DATAB_driver,
	datad => PC_a11_DATAD_driver,
	combout => PC_a11_combout);

PC_a10_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a10_a_CLK_driver);

PC_a10_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a11_combout,
	dataout => PC_a10_a_D_driver);

PC_a10_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a10_a_ainput_o,
	dataout => PC_a10_a_ASDATA_driver);

PC_a10_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a10_a_SCLR_driver);

PC_a10_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a10_a_SLOAD_driver);

PC_a10_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a10_a_ENA_driver);

-- Location: FF_X36_Y26_N13
PC_a10_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a10_a_CLK_driver,
	d => PC_a10_a_D_driver,
	asdata => PC_a10_a_ASDATA_driver,
	sclr => PC_a10_a_SCLR_driver,
	sload => PC_a10_a_SLOAD_driver,
	ena => PC_a10_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(10));

left_a10_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a10_a_DATAA_driver);

left_a10_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a10_a_a21_combout,
	dataout => left_a10_a_DATAB_driver);

left_a10_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(10),
	dataout => left_a10_a_DATAC_driver);

left_a10_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(10),
	dataout => left_a10_a_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N10
left_a10_a : cycloneive_lcell_comb
-- Equation(s):
-- left(10) = (mux_alu_left_a0_a_ainput_o & ((left_a10_a_a21_combout & (HI(10))) # (!left_a10_a_a21_combout & ((PC(10)))))) # (!mux_alu_left_a0_a_ainput_o & (left_a10_a_a21_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a10_a_DATAA_driver,
	datab => left_a10_a_DATAB_driver,
	datac => left_a10_a_DATAC_driver,
	datad => left_a10_a_DATAD_driver,
	combout => left(10));

Add0_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a145_combout,
	dataout => Add0_a22_DATAA_driver);

Add0_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(10),
	dataout => Add0_a22_DATAB_driver);

Add0_a22_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a21,
	dataout => Add0_a22_CIN_driver);

-- Location: LCCOMB_X37_Y28_N4
Add0_a22 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a22_combout = (op2_real_a145_combout & ((left(10) & (Add0_a21 & VCC)) # (!left(10) & (!Add0_a21)))) # (!op2_real_a145_combout & ((left(10) & (!Add0_a21)) # (!left(10) & ((Add0_a21) # (GND)))))
-- Add0_a23 = CARRY((op2_real_a145_combout & (!left(10) & !Add0_a21)) # (!op2_real_a145_combout & ((!Add0_a21) # (!left(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a22_DATAA_driver,
	datab => Add0_a22_DATAB_driver,
	datad => VCC,
	cin => Add0_a22_CIN_driver,
	combout => Add0_a22_combout,
	cout => Add0_a23);

HI_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a12_DATAA_driver);

HI_a12_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(9),
	dataout => HI_a12_DATAB_driver);

HI_a12_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a22_combout,
	dataout => HI_a12_DATAC_driver);

HI_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a12_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N26
HI_a12 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a12_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a22_combout))) # (!Add0_a66_combout & (HI(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a12_DATAA_driver,
	datab => HI_a12_DATAB_driver,
	datac => HI_a12_DATAC_driver,
	datad => HI_a12_DATAD_driver,
	combout => HI_a12_combout);

HI_a10_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a10_a_CLK_driver);

HI_a10_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a12_combout,
	dataout => HI_a10_a_D_driver);

HI_a10_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a10_a_ENA_driver);

-- Location: FF_X37_Y26_N27
HI_a10_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a10_a_CLK_driver,
	d => HI_a10_a_D_driver,
	ena => HI_a10_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(10));

data_in_a11_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(11),
	dataout => data_in_a11_a_ainput_I_driver);

-- Location: IOIBUF_X29_Y43_N22
data_in_a11_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a11_a_ainput_I_driver,
	o => data_in_a11_a_ainput_o);

A_a12_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a11_a_ainput_o,
	dataout => A_a12_DATAC_driver);

A_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a12_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N26
A_a12 : cycloneive_lcell_comb
-- Equation(s):
-- A_a12_combout = (data_in_a11_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a12_DATAC_driver,
	datad => A_a12_DATAD_driver,
	combout => A_a12_combout);

B_a11_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a11_a_CLK_driver);

B_a11_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a12_combout,
	dataout => B_a11_a_ASDATA_driver);

B_a11_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a11_a_ENA_driver);

-- Location: FF_X37_Y24_N13
B_a11_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a11_a_CLK_driver,
	asdata => B_a11_a_ASDATA_driver,
	sload => VCC,
	ena => B_a11_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(11));

op2_real_a146_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a11_a_ainput_o,
	dataout => op2_real_a146_DATAA_driver);

op2_real_a146_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a146_DATAB_driver);

op2_real_a146_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(11),
	dataout => op2_real_a146_DATAC_driver);

op2_real_a146_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a146_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N12
op2_real_a146 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a146_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a11_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a146_DATAA_driver,
	datab => op2_real_a146_DATAB_driver,
	datac => op2_real_a146_DATAC_driver,
	datad => op2_real_a146_DATAD_driver,
	combout => op2_real_a146_combout);

op2_real_a147_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a147_DATAA_driver);

op2_real_a147_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a147_DATAB_driver);

op2_real_a147_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a146_combout,
	dataout => op2_real_a147_DATAD_driver);

-- Location: LCCOMB_X37_Y24_N18
op2_real_a147 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a147_combout = (mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o & op2_real_a146_combout)) # (!mux_alu_right_a1_a_ainput_o & (alu_add_sub_ainput_o $ (op2_real_a146_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a147_DATAA_driver,
	datab => op2_real_a147_DATAB_driver,
	datad => op2_real_a147_DATAD_driver,
	combout => op2_real_a147_combout);

Add0_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(11),
	dataout => Add0_a24_DATAA_driver);

Add0_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a147_combout,
	dataout => Add0_a24_DATAB_driver);

Add0_a24_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a23,
	dataout => Add0_a24_CIN_driver);

-- Location: LCCOMB_X37_Y28_N6
Add0_a24 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a24_combout = ((left(11) $ (op2_real_a147_combout $ (!Add0_a23)))) # (GND)
-- Add0_a25 = CARRY((left(11) & ((op2_real_a147_combout) # (!Add0_a23))) # (!left(11) & (op2_real_a147_combout & !Add0_a23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a24_DATAA_driver,
	datab => Add0_a24_DATAB_driver,
	datad => VCC,
	cin => Add0_a24_CIN_driver,
	combout => Add0_a24_combout,
	cout => Add0_a25);

HI_a13_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a13_DATAA_driver);

HI_a13_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(10),
	dataout => HI_a13_DATAB_driver);

HI_a13_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a24_combout,
	dataout => HI_a13_DATAC_driver);

HI_a13_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a13_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N28
HI_a13 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a13_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a24_combout))) # (!Add0_a66_combout & (HI(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a13_DATAA_driver,
	datab => HI_a13_DATAB_driver,
	datac => HI_a13_DATAC_driver,
	datad => HI_a13_DATAD_driver,
	combout => HI_a13_combout);

HI_a11_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a11_a_CLK_driver);

HI_a11_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a13_combout,
	dataout => HI_a11_a_D_driver);

HI_a11_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a11_a_ENA_driver);

-- Location: FF_X37_Y26_N29
HI_a11_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a11_a_CLK_driver,
	d => HI_a11_a_D_driver,
	ena => HI_a11_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(11));

left_a12_a_a19_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a12_a_a19_DATAA_driver);

left_a12_a_a19_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a12_a_a19_DATAB_driver);

left_a12_a_a19_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(12),
	dataout => left_a12_a_a19_DATAC_driver);

left_a12_a_a19_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(11),
	dataout => left_a12_a_a19_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N8
left_a12_a_a19 : cycloneive_lcell_comb
-- Equation(s):
-- left_a12_a_a19_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(11)))) # (!mux_alu_left_a1_a_ainput_o & (A(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a12_a_a19_DATAA_driver,
	datab => left_a12_a_a19_DATAB_driver,
	datac => left_a12_a_a19_DATAC_driver,
	datad => left_a12_a_a19_DATAD_driver,
	combout => left_a12_a_a19_combout);

PC_a13_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a10_a_ainput_o,
	dataout => PC_a13_DATAA_driver);

PC_a13_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a13_DATAB_driver);

PC_a13_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a26_combout,
	dataout => PC_a13_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N28
PC_a13 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a13_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a10_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a26_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a13_DATAA_driver,
	datab => PC_a13_DATAB_driver,
	datad => PC_a13_DATAD_driver,
	combout => PC_a13_combout);

PC_a12_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a12_a_CLK_driver);

PC_a12_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a13_combout,
	dataout => PC_a12_a_D_driver);

PC_a12_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a12_a_ainput_o,
	dataout => PC_a12_a_ASDATA_driver);

PC_a12_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a12_a_SCLR_driver);

PC_a12_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a12_a_SLOAD_driver);

PC_a12_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a12_a_ENA_driver);

-- Location: FF_X36_Y26_N29
PC_a12_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a12_a_CLK_driver,
	d => PC_a12_a_D_driver,
	asdata => PC_a12_a_ASDATA_driver,
	sclr => PC_a12_a_SCLR_driver,
	sload => PC_a12_a_SLOAD_driver,
	ena => PC_a12_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(12));

left_a12_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a12_a_DATAA_driver);

left_a12_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(12),
	dataout => left_a12_a_DATAB_driver);

left_a12_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a12_a_a19_combout,
	dataout => left_a12_a_DATAC_driver);

left_a12_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(12),
	dataout => left_a12_a_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N12
left_a12_a : cycloneive_lcell_comb
-- Equation(s):
-- left(12) = (mux_alu_left_a0_a_ainput_o & ((left_a12_a_a19_combout & (HI(12))) # (!left_a12_a_a19_combout & ((PC(12)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a12_a_a19_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a12_a_DATAA_driver,
	datab => left_a12_a_DATAB_driver,
	datac => left_a12_a_DATAC_driver,
	datad => left_a12_a_DATAD_driver,
	combout => left(12));

Add0_a26_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a149_combout,
	dataout => Add0_a26_DATAA_driver);

Add0_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(12),
	dataout => Add0_a26_DATAB_driver);

Add0_a26_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a25,
	dataout => Add0_a26_CIN_driver);

-- Location: LCCOMB_X37_Y28_N8
Add0_a26 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a26_combout = (op2_real_a149_combout & ((left(12) & (Add0_a25 & VCC)) # (!left(12) & (!Add0_a25)))) # (!op2_real_a149_combout & ((left(12) & (!Add0_a25)) # (!left(12) & ((Add0_a25) # (GND)))))
-- Add0_a27 = CARRY((op2_real_a149_combout & (!left(12) & !Add0_a25)) # (!op2_real_a149_combout & ((!Add0_a25) # (!left(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a26_DATAA_driver,
	datab => Add0_a26_DATAB_driver,
	datad => VCC,
	cin => Add0_a26_CIN_driver,
	combout => Add0_a26_combout,
	cout => Add0_a27);

HI_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a14_DATAA_driver);

HI_a14_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a26_combout,
	dataout => HI_a14_DATAB_driver);

HI_a14_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(11),
	dataout => HI_a14_DATAC_driver);

HI_a14_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a14_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N18
HI_a14 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a14_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a26_combout)) # (!Add0_a66_combout & ((HI(11))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a14_DATAA_driver,
	datab => HI_a14_DATAB_driver,
	datac => HI_a14_DATAC_driver,
	datad => HI_a14_DATAD_driver,
	combout => HI_a14_combout);

HI_a12_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a12_a_CLK_driver);

HI_a12_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_combout,
	dataout => HI_a12_a_D_driver);

HI_a12_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a12_a_ENA_driver);

-- Location: FF_X37_Y26_N19
HI_a12_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a12_a_CLK_driver,
	d => HI_a12_a_D_driver,
	ena => HI_a12_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(12));

data_in_a13_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(13),
	dataout => data_in_a13_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y29_N1
data_in_a13_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a13_a_ainput_I_driver,
	o => data_in_a13_a_ainput_o);

A_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a14_DATAA_driver);

A_a14_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a13_a_ainput_o,
	dataout => A_a14_DATAC_driver);

-- Location: LCCOMB_X36_Y26_N2
A_a14 : cycloneive_lcell_comb
-- Equation(s):
-- A_a14_combout = (!reset_ainput_o & data_in_a13_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => A_a14_DATAA_driver,
	datac => A_a14_DATAC_driver,
	combout => A_a14_combout);

A_a13_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a13_a_CLK_driver);

A_a13_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a14_combout,
	dataout => A_a13_a_ASDATA_driver);

A_a13_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a13_a_ENA_driver);

-- Location: FF_X37_Y26_N15
A_a13_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a13_a_CLK_driver,
	asdata => A_a13_a_ASDATA_driver,
	sload => VCC,
	ena => A_a13_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(13));

left_a13_a_a18_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a13_a_a18_DATAA_driver);

left_a13_a_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a13_a_a18_DATAB_driver);

left_a13_a_a18_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(13),
	dataout => left_a13_a_a18_DATAC_driver);

left_a13_a_a18_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(12),
	dataout => left_a13_a_a18_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N14
left_a13_a_a18 : cycloneive_lcell_comb
-- Equation(s):
-- left_a13_a_a18_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(12)))) # (!mux_alu_left_a1_a_ainput_o & (A(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a13_a_a18_DATAA_driver,
	datab => left_a13_a_a18_DATAB_driver,
	datac => left_a13_a_a18_DATAC_driver,
	datad => left_a13_a_a18_DATAD_driver,
	combout => left_a13_a_a18_combout);

PC_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a11_a_ainput_o,
	dataout => PC_a14_DATAA_driver);

PC_a14_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a14_DATAB_driver);

PC_a14_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a28_combout,
	dataout => PC_a14_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N14
PC_a14 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a14_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a11_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a28_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a14_DATAA_driver,
	datab => PC_a14_DATAB_driver,
	datad => PC_a14_DATAD_driver,
	combout => PC_a14_combout);

PC_a13_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a13_a_CLK_driver);

PC_a13_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a14_combout,
	dataout => PC_a13_a_D_driver);

PC_a13_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a13_a_ainput_o,
	dataout => PC_a13_a_ASDATA_driver);

PC_a13_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a13_a_SCLR_driver);

PC_a13_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a13_a_SLOAD_driver);

PC_a13_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a13_a_ENA_driver);

-- Location: FF_X36_Y26_N15
PC_a13_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a13_a_CLK_driver,
	d => PC_a13_a_D_driver,
	asdata => PC_a13_a_ASDATA_driver,
	sclr => PC_a13_a_SCLR_driver,
	sload => PC_a13_a_SLOAD_driver,
	ena => PC_a13_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(13));

left_a13_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a13_a_DATAA_driver);

left_a13_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(13),
	dataout => left_a13_a_DATAB_driver);

left_a13_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a13_a_a18_combout,
	dataout => left_a13_a_DATAC_driver);

left_a13_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(13),
	dataout => left_a13_a_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N6
left_a13_a : cycloneive_lcell_comb
-- Equation(s):
-- left(13) = (mux_alu_left_a0_a_ainput_o & ((left_a13_a_a18_combout & (HI(13))) # (!left_a13_a_a18_combout & ((PC(13)))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a13_a_a18_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a13_a_DATAA_driver,
	datab => left_a13_a_DATAB_driver,
	datac => left_a13_a_DATAC_driver,
	datad => left_a13_a_DATAD_driver,
	combout => left(13));

Add0_a28_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a151_combout,
	dataout => Add0_a28_DATAA_driver);

Add0_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(13),
	dataout => Add0_a28_DATAB_driver);

Add0_a28_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a27,
	dataout => Add0_a28_CIN_driver);

-- Location: LCCOMB_X37_Y28_N10
Add0_a28 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a28_combout = ((op2_real_a151_combout $ (left(13) $ (!Add0_a27)))) # (GND)
-- Add0_a29 = CARRY((op2_real_a151_combout & ((left(13)) # (!Add0_a27))) # (!op2_real_a151_combout & (left(13) & !Add0_a27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a28_DATAA_driver,
	datab => Add0_a28_DATAB_driver,
	datad => VCC,
	cin => Add0_a28_CIN_driver,
	combout => Add0_a28_combout,
	cout => Add0_a29);

HI_a15_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a15_DATAA_driver);

HI_a15_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(12),
	dataout => HI_a15_DATAB_driver);

HI_a15_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a15_DATAC_driver);

HI_a15_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a28_combout,
	dataout => HI_a15_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N18
HI_a15 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a15_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a28_combout))) # (!Add0_a66_combout & (HI(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a15_DATAA_driver,
	datab => HI_a15_DATAB_driver,
	datac => HI_a15_DATAC_driver,
	datad => HI_a15_DATAD_driver,
	combout => HI_a15_combout);

HI_a13_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a13_a_CLK_driver);

HI_a13_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a15_combout,
	dataout => HI_a13_a_D_driver);

HI_a13_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a13_a_ENA_driver);

-- Location: FF_X38_Y26_N19
HI_a13_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a13_a_CLK_driver,
	d => HI_a13_a_D_driver,
	ena => HI_a13_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(13));

Add0_a30_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a153_combout,
	dataout => Add0_a30_DATAA_driver);

Add0_a30_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(14),
	dataout => Add0_a30_DATAB_driver);

Add0_a30_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a29,
	dataout => Add0_a30_CIN_driver);

-- Location: LCCOMB_X37_Y28_N12
Add0_a30 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a30_combout = (op2_real_a153_combout & ((left(14) & (Add0_a29 & VCC)) # (!left(14) & (!Add0_a29)))) # (!op2_real_a153_combout & ((left(14) & (!Add0_a29)) # (!left(14) & ((Add0_a29) # (GND)))))
-- Add0_a31 = CARRY((op2_real_a153_combout & (!left(14) & !Add0_a29)) # (!op2_real_a153_combout & ((!Add0_a29) # (!left(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a30_DATAA_driver,
	datab => Add0_a30_DATAB_driver,
	datad => VCC,
	cin => Add0_a30_CIN_driver,
	combout => Add0_a30_combout,
	cout => Add0_a31);

HI_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a16_DATAA_driver);

HI_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(13),
	dataout => HI_a16_DATAB_driver);

HI_a16_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a16_DATAC_driver);

HI_a16_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a30_combout,
	dataout => HI_a16_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N24
HI_a16 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a16_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a30_combout))) # (!Add0_a66_combout & (HI(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a16_DATAA_driver,
	datab => HI_a16_DATAB_driver,
	datac => HI_a16_DATAC_driver,
	datad => HI_a16_DATAD_driver,
	combout => HI_a16_combout);

HI_a14_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a14_a_CLK_driver);

HI_a14_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a16_combout,
	dataout => HI_a14_a_D_driver);

HI_a14_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a14_a_ENA_driver);

-- Location: FF_X38_Y26_N25
HI_a14_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a14_a_CLK_driver,
	d => HI_a14_a_D_driver,
	ena => HI_a14_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(14));

intr_pc_a12_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(12),
	dataout => intr_pc_a12_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y18_N1
intr_pc_a12_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a12_a_ainput_I_driver,
	o => intr_pc_a12_a_ainput_o);

PC_a15_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a15_DATAA_driver);

PC_a15_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a12_a_ainput_o,
	dataout => PC_a15_DATAB_driver);

PC_a15_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a30_combout,
	dataout => PC_a15_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N24
PC_a15 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a15_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a12_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a30_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a15_DATAA_driver,
	datab => PC_a15_DATAB_driver,
	datad => PC_a15_DATAD_driver,
	combout => PC_a15_combout);

data_in_a14_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(14),
	dataout => data_in_a14_a_ainput_I_driver);

-- Location: IOIBUF_X45_Y43_N15
data_in_a14_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a14_a_ainput_I_driver,
	o => data_in_a14_a_ainput_o);

PC_a14_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a14_a_CLK_driver);

PC_a14_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a15_combout,
	dataout => PC_a14_a_D_driver);

PC_a14_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a14_a_ainput_o,
	dataout => PC_a14_a_ASDATA_driver);

PC_a14_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a14_a_SCLR_driver);

PC_a14_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a14_a_SLOAD_driver);

PC_a14_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a14_a_ENA_driver);

-- Location: FF_X39_Y26_N25
PC_a14_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a14_a_CLK_driver,
	d => PC_a14_a_D_driver,
	asdata => PC_a14_a_ASDATA_driver,
	sclr => PC_a14_a_SCLR_driver,
	sload => PC_a14_a_SLOAD_driver,
	ena => PC_a14_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(14));

left_a14_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a14_a_a17_combout,
	dataout => left_a14_a_DATAA_driver);

left_a14_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(14),
	dataout => left_a14_a_DATAB_driver);

left_a14_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a14_a_DATAC_driver);

left_a14_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(14),
	dataout => left_a14_a_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N0
left_a14_a : cycloneive_lcell_comb
-- Equation(s):
-- left(14) = (left_a14_a_a17_combout & ((HI(14)) # ((!mux_alu_left_a0_a_ainput_o)))) # (!left_a14_a_a17_combout & (((mux_alu_left_a0_a_ainput_o & PC(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a14_a_DATAA_driver,
	datab => left_a14_a_DATAB_driver,
	datac => left_a14_a_DATAC_driver,
	datad => left_a14_a_DATAD_driver,
	combout => left(14));

Add0_a32_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(15),
	dataout => Add0_a32_DATAA_driver);

Add0_a32_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a155_combout,
	dataout => Add0_a32_DATAB_driver);

Add0_a32_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a31,
	dataout => Add0_a32_CIN_driver);

-- Location: LCCOMB_X37_Y28_N14
Add0_a32 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a32_combout = ((left(15) $ (op2_real_a155_combout $ (!Add0_a31)))) # (GND)
-- Add0_a33 = CARRY((left(15) & ((op2_real_a155_combout) # (!Add0_a31))) # (!left(15) & (op2_real_a155_combout & !Add0_a31)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a32_DATAA_driver,
	datab => Add0_a32_DATAB_driver,
	datad => VCC,
	cin => Add0_a32_CIN_driver,
	combout => Add0_a32_combout,
	cout => Add0_a33);

HI_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(14),
	dataout => HI_a17_DATAA_driver);

HI_a17_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a17_DATAB_driver);

HI_a17_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a32_combout,
	dataout => HI_a17_DATAC_driver);

HI_a17_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a17_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N28
HI_a17 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a17_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a32_combout))) # (!Add0_a66_combout & (HI(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a17_DATAA_driver,
	datab => HI_a17_DATAB_driver,
	datac => HI_a17_DATAC_driver,
	datad => HI_a17_DATAD_driver,
	combout => HI_a17_combout);

HI_a15_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a15_a_CLK_driver);

HI_a15_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a17_combout,
	dataout => HI_a15_a_D_driver);

HI_a15_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a15_a_ENA_driver);

-- Location: FF_X38_Y28_N29
HI_a15_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a15_a_CLK_driver,
	d => HI_a15_a_D_driver,
	ena => HI_a15_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(15));

left_a16_a_a15_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a16_a_a15_DATAA_driver);

left_a16_a_a15_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a16_a_a15_DATAB_driver);

left_a16_a_a15_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(16),
	dataout => left_a16_a_a15_DATAC_driver);

left_a16_a_a15_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(15),
	dataout => left_a16_a_a15_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N14
left_a16_a_a15 : cycloneive_lcell_comb
-- Equation(s):
-- left_a16_a_a15_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(15)))) # (!mux_alu_left_a1_a_ainput_o & (A(16)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a16_a_a15_DATAA_driver,
	datab => left_a16_a_a15_DATAB_driver,
	datac => left_a16_a_a15_DATAC_driver,
	datad => left_a16_a_a15_DATAD_driver,
	combout => left_a16_a_a15_combout);

HI_a18_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a34_combout,
	dataout => HI_a18_DATAA_driver);

HI_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(15),
	dataout => HI_a18_DATAB_driver);

HI_a18_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a18_DATAC_driver);

HI_a18_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a18_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N26
HI_a18 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a18_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a34_combout)) # (!Add0_a66_combout & ((HI(15))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a18_DATAA_driver,
	datab => HI_a18_DATAB_driver,
	datac => HI_a18_DATAC_driver,
	datad => HI_a18_DATAD_driver,
	combout => HI_a18_combout);

HI_a16_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a16_a_CLK_driver);

HI_a16_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a18_combout,
	dataout => HI_a16_a_D_driver);

HI_a16_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a16_a_ENA_driver);

-- Location: FF_X38_Y28_N27
HI_a16_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a16_a_CLK_driver,
	d => HI_a16_a_D_driver,
	ena => HI_a16_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(16));

intr_pc_a14_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(14),
	dataout => intr_pc_a14_a_ainput_I_driver);

-- Location: IOIBUF_X54_Y43_N15
intr_pc_a14_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a14_a_ainput_I_driver,
	o => intr_pc_a14_a_ainput_o);

Add0_a34_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a157_combout,
	dataout => Add0_a34_DATAA_driver);

Add0_a34_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(16),
	dataout => Add0_a34_DATAB_driver);

Add0_a34_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a33,
	dataout => Add0_a34_CIN_driver);

-- Location: LCCOMB_X37_Y28_N16
Add0_a34 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a34_combout = (op2_real_a157_combout & ((left(16) & (Add0_a33 & VCC)) # (!left(16) & (!Add0_a33)))) # (!op2_real_a157_combout & ((left(16) & (!Add0_a33)) # (!left(16) & ((Add0_a33) # (GND)))))
-- Add0_a35 = CARRY((op2_real_a157_combout & (!left(16) & !Add0_a33)) # (!op2_real_a157_combout & ((!Add0_a33) # (!left(16)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a34_DATAA_driver,
	datab => Add0_a34_DATAB_driver,
	datad => VCC,
	cin => Add0_a34_CIN_driver,
	combout => Add0_a34_combout,
	cout => Add0_a35);

PC_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a17_DATAA_driver);

PC_a17_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a14_a_ainput_o,
	dataout => PC_a17_DATAB_driver);

PC_a17_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a34_combout,
	dataout => PC_a17_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N18
PC_a17 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a17_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a14_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a34_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a17_DATAA_driver,
	datab => PC_a17_DATAB_driver,
	datad => PC_a17_DATAD_driver,
	combout => PC_a17_combout);

data_in_a16_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(16),
	dataout => data_in_a16_a_ainput_I_driver);

-- Location: IOIBUF_X20_Y43_N1
data_in_a16_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a16_a_ainput_I_driver,
	o => data_in_a16_a_ainput_o);

PC_a16_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a16_a_CLK_driver);

PC_a16_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a17_combout,
	dataout => PC_a16_a_D_driver);

PC_a16_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a16_a_ainput_o,
	dataout => PC_a16_a_ASDATA_driver);

PC_a16_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a16_a_SCLR_driver);

PC_a16_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a16_a_SLOAD_driver);

PC_a16_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a16_a_ENA_driver);

-- Location: FF_X39_Y28_N19
PC_a16_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a16_a_CLK_driver,
	d => PC_a16_a_D_driver,
	asdata => PC_a16_a_ASDATA_driver,
	sclr => PC_a16_a_SCLR_driver,
	sload => PC_a16_a_SLOAD_driver,
	ena => PC_a16_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(16));

left_a16_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a16_a_DATAA_driver);

left_a16_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a16_a_a15_combout,
	dataout => left_a16_a_DATAB_driver);

left_a16_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(16),
	dataout => left_a16_a_DATAC_driver);

left_a16_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(16),
	dataout => left_a16_a_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N24
left_a16_a : cycloneive_lcell_comb
-- Equation(s):
-- left(16) = (mux_alu_left_a0_a_ainput_o & ((left_a16_a_a15_combout & (HI(16))) # (!left_a16_a_a15_combout & ((PC(16)))))) # (!mux_alu_left_a0_a_ainput_o & (left_a16_a_a15_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a16_a_DATAA_driver,
	datab => left_a16_a_DATAB_driver,
	datac => left_a16_a_DATAC_driver,
	datad => left_a16_a_DATAD_driver,
	combout => left(16));

Add0_a36_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a159_combout,
	dataout => Add0_a36_DATAA_driver);

Add0_a36_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(17),
	dataout => Add0_a36_DATAB_driver);

Add0_a36_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a35,
	dataout => Add0_a36_CIN_driver);

-- Location: LCCOMB_X37_Y28_N18
Add0_a36 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a36_combout = ((op2_real_a159_combout $ (left(17) $ (!Add0_a35)))) # (GND)
-- Add0_a37 = CARRY((op2_real_a159_combout & ((left(17)) # (!Add0_a35))) # (!op2_real_a159_combout & (left(17) & !Add0_a35)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a36_DATAA_driver,
	datab => Add0_a36_DATAB_driver,
	datad => VCC,
	cin => Add0_a36_CIN_driver,
	combout => Add0_a36_combout,
	cout => Add0_a37);

PC_a18_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a18_DATAA_driver);

PC_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a15_a_ainput_o,
	dataout => PC_a18_DATAB_driver);

PC_a18_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a36_combout,
	dataout => PC_a18_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N16
PC_a18 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a18_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a15_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a36_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a18_DATAA_driver,
	datab => PC_a18_DATAB_driver,
	datad => PC_a18_DATAD_driver,
	combout => PC_a18_combout);

data_in_a17_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(17),
	dataout => data_in_a17_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y18_N15
data_in_a17_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a17_a_ainput_I_driver,
	o => data_in_a17_a_ainput_o);

PC_a17_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a17_a_CLK_driver);

PC_a17_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a18_combout,
	dataout => PC_a17_a_D_driver);

PC_a17_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a17_a_ainput_o,
	dataout => PC_a17_a_ASDATA_driver);

PC_a17_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a17_a_SCLR_driver);

PC_a17_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a17_a_SLOAD_driver);

PC_a17_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a17_a_ENA_driver);

-- Location: FF_X39_Y28_N17
PC_a17_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a17_a_CLK_driver,
	d => PC_a17_a_D_driver,
	asdata => PC_a17_a_ASDATA_driver,
	sclr => PC_a17_a_SCLR_driver,
	sload => PC_a17_a_SLOAD_driver,
	ena => PC_a17_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(17));

A_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a17_a_ainput_o,
	dataout => A_a18_DATAB_driver);

A_a18_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a18_DATAC_driver);

-- Location: LCCOMB_X39_Y28_N4
A_a18 : cycloneive_lcell_comb
-- Equation(s):
-- A_a18_combout = (data_in_a17_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a18_DATAB_driver,
	datac => A_a18_DATAC_driver,
	combout => A_a18_combout);

A_a17_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a17_a_CLK_driver);

A_a17_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a18_combout,
	dataout => A_a17_a_ASDATA_driver);

A_a17_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a17_a_ENA_driver);

-- Location: FF_X38_Y28_N17
A_a17_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a17_a_CLK_driver,
	asdata => A_a17_a_ASDATA_driver,
	sload => VCC,
	ena => A_a17_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(17));

left_a17_a_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a17_a_a14_DATAA_driver);

left_a17_a_a14_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a17_a_a14_DATAB_driver);

left_a17_a_a14_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(16),
	dataout => left_a17_a_a14_DATAC_driver);

left_a17_a_a14_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(17),
	dataout => left_a17_a_a14_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N22
left_a17_a_a14 : cycloneive_lcell_comb
-- Equation(s):
-- left_a17_a_a14_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & (HI(16))) # (!mux_alu_left_a1_a_ainput_o & ((A(17))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a17_a_a14_DATAA_driver,
	datab => left_a17_a_a14_DATAB_driver,
	datac => left_a17_a_a14_DATAC_driver,
	datad => left_a17_a_a14_DATAD_driver,
	combout => left_a17_a_a14_combout);

HI_a19_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a36_combout,
	dataout => HI_a19_DATAA_driver);

HI_a19_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a19_DATAB_driver);

HI_a19_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(16),
	dataout => HI_a19_DATAC_driver);

HI_a19_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a19_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N18
HI_a19 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a19_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a36_combout)) # (!Add0_a66_combout & ((HI(16))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a19_DATAA_driver,
	datab => HI_a19_DATAB_driver,
	datac => HI_a19_DATAC_driver,
	datad => HI_a19_DATAD_driver,
	combout => HI_a19_combout);

HI_a17_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a17_a_CLK_driver);

HI_a17_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a19_combout,
	dataout => HI_a17_a_D_driver);

HI_a17_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a17_a_ENA_driver);

-- Location: FF_X38_Y28_N19
HI_a17_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a17_a_CLK_driver,
	d => HI_a17_a_D_driver,
	ena => HI_a17_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(17));

left_a17_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a17_a_DATAA_driver);

left_a17_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(17),
	dataout => left_a17_a_DATAB_driver);

left_a17_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a17_a_a14_combout,
	dataout => left_a17_a_DATAC_driver);

left_a17_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(17),
	dataout => left_a17_a_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N8
left_a17_a : cycloneive_lcell_comb
-- Equation(s):
-- left(17) = (mux_alu_left_a0_a_ainput_o & ((left_a17_a_a14_combout & ((HI(17)))) # (!left_a17_a_a14_combout & (PC(17))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a17_a_a14_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a17_a_DATAA_driver,
	datab => left_a17_a_DATAB_driver,
	datac => left_a17_a_DATAC_driver,
	datad => left_a17_a_DATAD_driver,
	combout => left(17));

Add0_a38_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(18),
	dataout => Add0_a38_DATAA_driver);

Add0_a38_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a161_combout,
	dataout => Add0_a38_DATAB_driver);

Add0_a38_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a37,
	dataout => Add0_a38_CIN_driver);

-- Location: LCCOMB_X37_Y28_N20
Add0_a38 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a38_combout = (left(18) & ((op2_real_a161_combout & (Add0_a37 & VCC)) # (!op2_real_a161_combout & (!Add0_a37)))) # (!left(18) & ((op2_real_a161_combout & (!Add0_a37)) # (!op2_real_a161_combout & ((Add0_a37) # (GND)))))
-- Add0_a39 = CARRY((left(18) & (!op2_real_a161_combout & !Add0_a37)) # (!left(18) & ((!Add0_a37) # (!op2_real_a161_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a38_DATAA_driver,
	datab => Add0_a38_DATAB_driver,
	datad => VCC,
	cin => Add0_a38_CIN_driver,
	combout => Add0_a38_combout,
	cout => Add0_a39);

Add0_a40_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(19),
	dataout => Add0_a40_DATAA_driver);

Add0_a40_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a163_combout,
	dataout => Add0_a40_DATAB_driver);

Add0_a40_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a39,
	dataout => Add0_a40_CIN_driver);

-- Location: LCCOMB_X37_Y28_N22
Add0_a40 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a40_combout = ((left(19) $ (op2_real_a163_combout $ (!Add0_a39)))) # (GND)
-- Add0_a41 = CARRY((left(19) & ((op2_real_a163_combout) # (!Add0_a39))) # (!left(19) & (op2_real_a163_combout & !Add0_a39)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a40_DATAA_driver,
	datab => Add0_a40_DATAB_driver,
	datad => VCC,
	cin => Add0_a40_CIN_driver,
	combout => Add0_a40_combout,
	cout => Add0_a41);

Add0_a42_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(20),
	dataout => Add0_a42_DATAA_driver);

Add0_a42_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a165_combout,
	dataout => Add0_a42_DATAB_driver);

Add0_a42_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a41,
	dataout => Add0_a42_CIN_driver);

-- Location: LCCOMB_X37_Y28_N24
Add0_a42 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a42_combout = (left(20) & ((op2_real_a165_combout & (Add0_a41 & VCC)) # (!op2_real_a165_combout & (!Add0_a41)))) # (!left(20) & ((op2_real_a165_combout & (!Add0_a41)) # (!op2_real_a165_combout & ((Add0_a41) # (GND)))))
-- Add0_a43 = CARRY((left(20) & (!op2_real_a165_combout & !Add0_a41)) # (!left(20) & ((!Add0_a41) # (!op2_real_a165_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a42_DATAA_driver,
	datab => Add0_a42_DATAB_driver,
	datad => VCC,
	cin => Add0_a42_CIN_driver,
	combout => Add0_a42_combout,
	cout => Add0_a43);

Add0_a44_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(21),
	dataout => Add0_a44_DATAA_driver);

Add0_a44_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a167_combout,
	dataout => Add0_a44_DATAB_driver);

Add0_a44_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a43,
	dataout => Add0_a44_CIN_driver);

-- Location: LCCOMB_X37_Y28_N26
Add0_a44 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a44_combout = ((left(21) $ (op2_real_a167_combout $ (!Add0_a43)))) # (GND)
-- Add0_a45 = CARRY((left(21) & ((op2_real_a167_combout) # (!Add0_a43))) # (!left(21) & (op2_real_a167_combout & !Add0_a43)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a44_DATAA_driver,
	datab => Add0_a44_DATAB_driver,
	datad => VCC,
	cin => Add0_a44_CIN_driver,
	combout => Add0_a44_combout,
	cout => Add0_a45);

Add0_a46_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(22),
	dataout => Add0_a46_DATAA_driver);

Add0_a46_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a169_combout,
	dataout => Add0_a46_DATAB_driver);

Add0_a46_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a45,
	dataout => Add0_a46_CIN_driver);

-- Location: LCCOMB_X37_Y28_N28
Add0_a46 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a46_combout = (left(22) & ((op2_real_a169_combout & (Add0_a45 & VCC)) # (!op2_real_a169_combout & (!Add0_a45)))) # (!left(22) & ((op2_real_a169_combout & (!Add0_a45)) # (!op2_real_a169_combout & ((Add0_a45) # (GND)))))
-- Add0_a47 = CARRY((left(22) & (!op2_real_a169_combout & !Add0_a45)) # (!left(22) & ((!Add0_a45) # (!op2_real_a169_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a46_DATAA_driver,
	datab => Add0_a46_DATAB_driver,
	datad => VCC,
	cin => Add0_a46_CIN_driver,
	combout => Add0_a46_combout,
	cout => Add0_a47);

HI_a23_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(20),
	dataout => HI_a23_DATAA_driver);

HI_a23_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a44_combout,
	dataout => HI_a23_DATAB_driver);

HI_a23_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a23_DATAC_driver);

HI_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a23_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N14
HI_a23 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a23_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a44_combout))) # (!Add0_a66_combout & (HI(20)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a23_DATAA_driver,
	datab => HI_a23_DATAB_driver,
	datac => HI_a23_DATAC_driver,
	datad => HI_a23_DATAD_driver,
	combout => HI_a23_combout);

HI_a21_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a21_a_CLK_driver);

HI_a21_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a23_combout,
	dataout => HI_a21_a_D_driver);

HI_a21_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a21_a_ENA_driver);

-- Location: FF_X36_Y28_N15
HI_a21_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a21_a_CLK_driver,
	d => HI_a21_a_D_driver,
	ena => HI_a21_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(21));

HI_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a24_DATAA_driver);

HI_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a46_combout,
	dataout => HI_a24_DATAB_driver);

HI_a24_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(21),
	dataout => HI_a24_DATAC_driver);

HI_a24_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a24_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N18
HI_a24 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a24_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a46_combout)) # (!Add0_a66_combout & ((HI(21))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a24_DATAA_driver,
	datab => HI_a24_DATAB_driver,
	datac => HI_a24_DATAC_driver,
	datad => HI_a24_DATAD_driver,
	combout => HI_a24_combout);

HI_a22_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a22_a_CLK_driver);

HI_a22_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a24_combout,
	dataout => HI_a22_a_D_driver);

HI_a22_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a22_a_ENA_driver);

-- Location: FF_X36_Y28_N19
HI_a22_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a22_a_CLK_driver,
	d => HI_a22_a_D_driver,
	ena => HI_a22_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(22));

HI_a25_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a48_combout,
	dataout => HI_a25_DATAA_driver);

HI_a25_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(22),
	dataout => HI_a25_DATAB_driver);

HI_a25_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a25_DATAC_driver);

HI_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a25_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N2
HI_a25 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a25_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a48_combout)) # (!Add0_a66_combout & ((HI(22))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a25_DATAA_driver,
	datab => HI_a25_DATAB_driver,
	datac => HI_a25_DATAC_driver,
	datad => HI_a25_DATAD_driver,
	combout => HI_a25_combout);

HI_a23_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a23_a_CLK_driver);

HI_a23_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a25_combout,
	dataout => HI_a23_a_D_driver);

HI_a23_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a23_a_ENA_driver);

-- Location: FF_X36_Y28_N3
HI_a23_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a23_a_CLK_driver,
	d => HI_a23_a_D_driver,
	ena => HI_a23_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(23));

HI_a26_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a50_combout,
	dataout => HI_a26_DATAA_driver);

HI_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(23),
	dataout => HI_a26_DATAB_driver);

HI_a26_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a26_DATAC_driver);

HI_a26_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a26_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N0
HI_a26 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a26_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a50_combout)) # (!Add0_a66_combout & ((HI(23))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a26_DATAA_driver,
	datab => HI_a26_DATAB_driver,
	datac => HI_a26_DATAC_driver,
	datad => HI_a26_DATAD_driver,
	combout => HI_a26_combout);

HI_a24_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a24_a_CLK_driver);

HI_a24_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a26_combout,
	dataout => HI_a24_a_D_driver);

HI_a24_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a24_a_ENA_driver);

-- Location: FF_X38_Y27_N1
HI_a24_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a24_a_CLK_driver,
	d => HI_a24_a_D_driver,
	ena => HI_a24_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(24));

left_a25_a_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a25_a_a6_DATAA_driver);

left_a25_a_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a25_a_a6_DATAB_driver);

left_a25_a_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(25),
	dataout => left_a25_a_a6_DATAC_driver);

left_a25_a_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(24),
	dataout => left_a25_a_a6_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N30
left_a25_a_a6 : cycloneive_lcell_comb
-- Equation(s):
-- left_a25_a_a6_combout = (mux_alu_left_a0_a_ainput_o & (mux_alu_left_a1_a_ainput_o)) # (!mux_alu_left_a0_a_ainput_o & ((mux_alu_left_a1_a_ainput_o & ((HI(24)))) # (!mux_alu_left_a1_a_ainput_o & (A(25)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a25_a_a6_DATAA_driver,
	datab => left_a25_a_a6_DATAB_driver,
	datac => left_a25_a_a6_DATAC_driver,
	datad => left_a25_a_a6_DATAD_driver,
	combout => left_a25_a_a6_combout);

HI_a27_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a52_combout,
	dataout => HI_a27_DATAA_driver);

HI_a27_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a27_DATAB_driver);

HI_a27_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a27_DATAC_driver);

HI_a27_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(24),
	dataout => HI_a27_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N6
HI_a27 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a27_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a52_combout)) # (!Add0_a66_combout & ((HI(24))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a27_DATAA_driver,
	datab => HI_a27_DATAB_driver,
	datac => HI_a27_DATAC_driver,
	datad => HI_a27_DATAD_driver,
	combout => HI_a27_combout);

HI_a25_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a25_a_CLK_driver);

HI_a25_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a27_combout,
	dataout => HI_a25_a_D_driver);

HI_a25_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a25_a_ENA_driver);

-- Location: FF_X38_Y27_N7
HI_a25_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a25_a_CLK_driver,
	d => HI_a25_a_D_driver,
	ena => HI_a25_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(25));

left_a25_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a25_a_DATAA_driver);

left_a25_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(25),
	dataout => left_a25_a_DATAB_driver);

left_a25_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a25_a_a6_combout,
	dataout => left_a25_a_DATAC_driver);

left_a25_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(25),
	dataout => left_a25_a_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N0
left_a25_a : cycloneive_lcell_comb
-- Equation(s):
-- left(25) = (mux_alu_left_a0_a_ainput_o & ((left_a25_a_a6_combout & ((HI(25)))) # (!left_a25_a_a6_combout & (PC(25))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a25_a_a6_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a25_a_DATAA_driver,
	datab => left_a25_a_DATAB_driver,
	datac => left_a25_a_DATAC_driver,
	datad => left_a25_a_DATAD_driver,
	combout => left(25));

data_in_a24_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(24),
	dataout => data_in_a24_a_ainput_I_driver);

-- Location: IOIBUF_X18_Y0_N1
data_in_a24_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a24_a_ainput_I_driver,
	o => data_in_a24_a_ainput_o);

A_a25_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a24_a_ainput_o,
	dataout => A_a25_DATAC_driver);

A_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a25_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N14
A_a25 : cycloneive_lcell_comb
-- Equation(s):
-- A_a25_combout = (data_in_a24_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a25_DATAC_driver,
	datad => A_a25_DATAD_driver,
	combout => A_a25_combout);

A_a24_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a24_a_CLK_driver);

A_a24_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a25_combout,
	dataout => A_a24_a_ASDATA_driver);

A_a24_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a24_a_ENA_driver);

-- Location: FF_X36_Y28_N31
A_a24_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a24_a_CLK_driver,
	asdata => A_a24_a_ASDATA_driver,
	sload => VCC,
	ena => A_a24_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(24));

left_a24_a_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a24_a_a7_DATAA_driver);

left_a24_a_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a24_a_a7_DATAB_driver);

left_a24_a_a7_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(24),
	dataout => left_a24_a_a7_DATAC_driver);

left_a24_a_a7_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(23),
	dataout => left_a24_a_a7_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N30
left_a24_a_a7 : cycloneive_lcell_comb
-- Equation(s):
-- left_a24_a_a7_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(23))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a24_a_a7_DATAA_driver,
	datab => left_a24_a_a7_DATAB_driver,
	datac => left_a24_a_a7_DATAC_driver,
	datad => left_a24_a_a7_DATAD_driver,
	combout => left_a24_a_a7_combout);

left_a24_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(24),
	dataout => left_a24_a_DATAA_driver);

left_a24_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a24_a_DATAB_driver);

left_a24_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a24_a_a7_combout,
	dataout => left_a24_a_DATAC_driver);

left_a24_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(24),
	dataout => left_a24_a_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N22
left_a24_a : cycloneive_lcell_comb
-- Equation(s):
-- left(24) = (mux_alu_left_a0_a_ainput_o & ((left_a24_a_a7_combout & ((HI(24)))) # (!left_a24_a_a7_combout & (PC(24))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a24_a_a7_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a24_a_DATAA_driver,
	datab => left_a24_a_DATAB_driver,
	datac => left_a24_a_DATAC_driver,
	datad => left_a24_a_DATAD_driver,
	combout => left(24));

intr_right_a23_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_right(23),
	dataout => intr_right_a23_a_ainput_I_driver);

-- Location: IOIBUF_X50_Y43_N1
intr_right_a23_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_right_a23_a_ainput_I_driver,
	o => intr_right_a23_a_ainput_o);

data_in_a23_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(23),
	dataout => data_in_a23_a_ainput_I_driver);

-- Location: IOIBUF_X54_Y43_N1
data_in_a23_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a23_a_ainput_I_driver,
	o => data_in_a23_a_ainput_o);

A_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a24_DATAB_driver);

A_a24_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a23_a_ainput_o,
	dataout => A_a24_DATAC_driver);

-- Location: LCCOMB_X39_Y28_N10
A_a24 : cycloneive_lcell_comb
-- Equation(s):
-- A_a24_combout = (!reset_ainput_o & data_in_a23_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a24_DATAB_driver,
	datac => A_a24_DATAC_driver,
	combout => A_a24_combout);

B_a23_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => B_a23_a_CLK_driver);

B_a23_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a24_combout,
	dataout => B_a23_a_ASDATA_driver);

B_a23_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B_a22_a_a0_combout,
	dataout => B_a23_a_ENA_driver);

-- Location: FF_X38_Y25_N11
B_a23_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => B_a23_a_CLK_driver,
	asdata => B_a23_a_ASDATA_driver,
	sload => VCC,
	ena => B_a23_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => B(23));

op2_real_a170_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a170_DATAA_driver);

op2_real_a170_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_right_a23_a_ainput_o,
	dataout => op2_real_a170_DATAB_driver);

op2_real_a170_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => B(23),
	dataout => op2_real_a170_DATAC_driver);

op2_real_a170_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a0_a_ainput_o,
	dataout => op2_real_a170_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N10
op2_real_a170 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a170_combout = (mux_alu_right_a1_a_ainput_o) # ((mux_alu_right_a0_a_ainput_o & (intr_right_a23_a_ainput_o)) # (!mux_alu_right_a0_a_ainput_o & ((B(23)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => op2_real_a170_DATAA_driver,
	datab => op2_real_a170_DATAB_driver,
	datac => op2_real_a170_DATAC_driver,
	datad => op2_real_a170_DATAD_driver,
	combout => op2_real_a170_combout);

op2_real_a171_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => op2_real_a171_DATAB_driver);

op2_real_a171_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_right_a1_a_ainput_o,
	dataout => op2_real_a171_DATAC_driver);

op2_real_a171_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a170_combout,
	dataout => op2_real_a171_DATAD_driver);

-- Location: LCCOMB_X38_Y25_N28
op2_real_a171 : cycloneive_lcell_comb
-- Equation(s):
-- op2_real_a171_combout = (alu_add_sub_ainput_o & (mux_alu_right_a1_a_ainput_o $ (!op2_real_a170_combout))) # (!alu_add_sub_ainput_o & (!mux_alu_right_a1_a_ainput_o & op2_real_a170_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => op2_real_a171_DATAB_driver,
	datac => op2_real_a171_DATAC_driver,
	datad => op2_real_a171_DATAD_driver,
	combout => op2_real_a171_combout);

Add0_a48_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(23),
	dataout => Add0_a48_DATAA_driver);

Add0_a48_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a171_combout,
	dataout => Add0_a48_DATAB_driver);

Add0_a48_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a47,
	dataout => Add0_a48_CIN_driver);

-- Location: LCCOMB_X37_Y28_N30
Add0_a48 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a48_combout = ((left(23) $ (op2_real_a171_combout $ (!Add0_a47)))) # (GND)
-- Add0_a49 = CARRY((left(23) & ((op2_real_a171_combout) # (!Add0_a47))) # (!left(23) & (op2_real_a171_combout & !Add0_a47)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a48_DATAA_driver,
	datab => Add0_a48_DATAB_driver,
	datad => VCC,
	cin => Add0_a48_CIN_driver,
	combout => Add0_a48_combout,
	cout => Add0_a49);

Add0_a50_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a173_combout,
	dataout => Add0_a50_DATAA_driver);

Add0_a50_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(24),
	dataout => Add0_a50_DATAB_driver);

Add0_a50_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a49,
	dataout => Add0_a50_CIN_driver);

-- Location: LCCOMB_X37_Y27_N0
Add0_a50 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a50_combout = (op2_real_a173_combout & ((left(24) & (Add0_a49 & VCC)) # (!left(24) & (!Add0_a49)))) # (!op2_real_a173_combout & ((left(24) & (!Add0_a49)) # (!left(24) & ((Add0_a49) # (GND)))))
-- Add0_a51 = CARRY((op2_real_a173_combout & (!left(24) & !Add0_a49)) # (!op2_real_a173_combout & ((!Add0_a49) # (!left(24)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a50_DATAA_driver,
	datab => Add0_a50_DATAB_driver,
	datad => VCC,
	cin => Add0_a50_CIN_driver,
	combout => Add0_a50_combout,
	cout => Add0_a51);

Add0_a52_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a175_combout,
	dataout => Add0_a52_DATAA_driver);

Add0_a52_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(25),
	dataout => Add0_a52_DATAB_driver);

Add0_a52_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a51,
	dataout => Add0_a52_CIN_driver);

-- Location: LCCOMB_X37_Y27_N2
Add0_a52 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a52_combout = ((op2_real_a175_combout $ (left(25) $ (!Add0_a51)))) # (GND)
-- Add0_a53 = CARRY((op2_real_a175_combout & ((left(25)) # (!Add0_a51))) # (!op2_real_a175_combout & (left(25) & !Add0_a51)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a52_DATAA_driver,
	datab => Add0_a52_DATAB_driver,
	datad => VCC,
	cin => Add0_a52_CIN_driver,
	combout => Add0_a52_combout,
	cout => Add0_a53);

Add0_a54_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a177_combout,
	dataout => Add0_a54_DATAA_driver);

Add0_a54_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(26),
	dataout => Add0_a54_DATAB_driver);

Add0_a54_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a53,
	dataout => Add0_a54_CIN_driver);

-- Location: LCCOMB_X37_Y27_N4
Add0_a54 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a54_combout = (op2_real_a177_combout & ((left(26) & (Add0_a53 & VCC)) # (!left(26) & (!Add0_a53)))) # (!op2_real_a177_combout & ((left(26) & (!Add0_a53)) # (!left(26) & ((Add0_a53) # (GND)))))
-- Add0_a55 = CARRY((op2_real_a177_combout & (!left(26) & !Add0_a53)) # (!op2_real_a177_combout & ((!Add0_a53) # (!left(26)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a54_DATAA_driver,
	datab => Add0_a54_DATAB_driver,
	datad => VCC,
	cin => Add0_a54_CIN_driver,
	combout => Add0_a54_combout,
	cout => Add0_a55);

HI_a28_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(25),
	dataout => HI_a28_DATAA_driver);

HI_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a54_combout,
	dataout => HI_a28_DATAB_driver);

HI_a28_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a28_DATAC_driver);

HI_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a28_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N12
HI_a28 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a28_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a54_combout))) # (!Add0_a66_combout & (HI(25)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a28_DATAA_driver,
	datab => HI_a28_DATAB_driver,
	datac => HI_a28_DATAC_driver,
	datad => HI_a28_DATAD_driver,
	combout => HI_a28_combout);

HI_a26_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a26_a_CLK_driver);

HI_a26_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a28_combout,
	dataout => HI_a26_a_D_driver);

HI_a26_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a26_a_ENA_driver);

-- Location: FF_X38_Y27_N13
HI_a26_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a26_a_CLK_driver,
	d => HI_a26_a_D_driver,
	ena => HI_a26_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(26));

left_a27_a_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a27_a_a4_DATAA_driver);

left_a27_a_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a27_a_a4_DATAB_driver);

left_a27_a_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(27),
	dataout => left_a27_a_a4_DATAC_driver);

left_a27_a_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(26),
	dataout => left_a27_a_a4_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N4
left_a27_a_a4 : cycloneive_lcell_comb
-- Equation(s):
-- left_a27_a_a4_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(26))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(27))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a27_a_a4_DATAA_driver,
	datab => left_a27_a_a4_DATAB_driver,
	datac => left_a27_a_a4_DATAC_driver,
	datad => left_a27_a_a4_DATAD_driver,
	combout => left_a27_a_a4_combout);

Add0_a56_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a179_combout,
	dataout => Add0_a56_DATAA_driver);

Add0_a56_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(27),
	dataout => Add0_a56_DATAB_driver);

Add0_a56_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a55,
	dataout => Add0_a56_CIN_driver);

-- Location: LCCOMB_X37_Y27_N6
Add0_a56 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a56_combout = ((op2_real_a179_combout $ (left(27) $ (!Add0_a55)))) # (GND)
-- Add0_a57 = CARRY((op2_real_a179_combout & ((left(27)) # (!Add0_a55))) # (!op2_real_a179_combout & (left(27) & !Add0_a55)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a56_DATAA_driver,
	datab => Add0_a56_DATAB_driver,
	datad => VCC,
	cin => Add0_a56_CIN_driver,
	combout => Add0_a56_combout,
	cout => Add0_a57);

HI_a29_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(26),
	dataout => HI_a29_DATAA_driver);

HI_a29_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a56_combout,
	dataout => HI_a29_DATAB_driver);

HI_a29_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a29_DATAC_driver);

HI_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a29_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N18
HI_a29 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a29_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a56_combout))) # (!Add0_a66_combout & (HI(26)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a29_DATAA_driver,
	datab => HI_a29_DATAB_driver,
	datac => HI_a29_DATAC_driver,
	datad => HI_a29_DATAD_driver,
	combout => HI_a29_combout);

HI_a27_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a27_a_CLK_driver);

HI_a27_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a29_combout,
	dataout => HI_a27_a_D_driver);

HI_a27_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a27_a_ENA_driver);

-- Location: FF_X38_Y27_N19
HI_a27_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a27_a_CLK_driver,
	d => HI_a27_a_D_driver,
	ena => HI_a27_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(27));

left_a27_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(27),
	dataout => left_a27_a_DATAA_driver);

left_a27_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a27_a_DATAB_driver);

left_a27_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a27_a_a4_combout,
	dataout => left_a27_a_DATAC_driver);

left_a27_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(27),
	dataout => left_a27_a_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N10
left_a27_a : cycloneive_lcell_comb
-- Equation(s):
-- left(27) = (mux_alu_left_a0_a_ainput_o & ((left_a27_a_a4_combout & ((HI(27)))) # (!left_a27_a_a4_combout & (PC(27))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a27_a_a4_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a27_a_DATAA_driver,
	datab => left_a27_a_DATAB_driver,
	datac => left_a27_a_DATAC_driver,
	datad => left_a27_a_DATAD_driver,
	combout => left(27));

Add0_a58_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a181_combout,
	dataout => Add0_a58_DATAA_driver);

Add0_a58_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(28),
	dataout => Add0_a58_DATAB_driver);

Add0_a58_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a57,
	dataout => Add0_a58_CIN_driver);

-- Location: LCCOMB_X37_Y27_N8
Add0_a58 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a58_combout = (op2_real_a181_combout & ((left(28) & (Add0_a57 & VCC)) # (!left(28) & (!Add0_a57)))) # (!op2_real_a181_combout & ((left(28) & (!Add0_a57)) # (!left(28) & ((Add0_a57) # (GND)))))
-- Add0_a59 = CARRY((op2_real_a181_combout & (!left(28) & !Add0_a57)) # (!op2_real_a181_combout & ((!Add0_a57) # (!left(28)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a58_DATAA_driver,
	datab => Add0_a58_DATAB_driver,
	datad => VCC,
	cin => Add0_a58_CIN_driver,
	combout => Add0_a58_combout,
	cout => Add0_a59);

PC_a29_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a28_a_ainput_o,
	dataout => PC_a29_DATAB_driver);

PC_a29_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a29_DATAC_driver);

PC_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a58_combout,
	dataout => PC_a29_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N8
PC_a29 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a29_combout = (mux_pc_a1_a_ainput_o & (data_in_a28_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a58_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => PC_a29_DATAB_driver,
	datac => PC_a29_DATAC_driver,
	datad => PC_a29_DATAD_driver,
	combout => PC_a29_combout);

PC_a29_a_a30_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a29_a_a30_DATAA_driver);

PC_a29_a_a30_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a29_a_a30_DATAB_driver);

PC_a29_a_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a0_a_ainput_o,
	dataout => PC_a29_a_a30_DATAC_driver);

-- Location: LCCOMB_X35_Y27_N28
PC_a29_a_a30 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a29_a_a30_combout = (reset_ainput_o) # (mux_pc_a1_a_ainput_o $ (mux_pc_a0_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111011011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a29_a_a30_DATAA_driver,
	datab => PC_a29_a_a30_DATAB_driver,
	datac => PC_a29_a_a30_DATAC_driver,
	combout => PC_a29_a_a30_combout);

PC_a28_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a28_a_CLK_driver);

PC_a28_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a29_combout,
	dataout => PC_a28_a_D_driver);

PC_a28_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a28_a_SCLR_driver);

PC_a28_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a29_a_a30_combout,
	dataout => PC_a28_a_ENA_driver);

-- Location: FF_X35_Y27_N9
PC_a28_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a28_a_CLK_driver,
	d => PC_a28_a_D_driver,
	sclr => PC_a28_a_SCLR_driver,
	ena => PC_a28_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(28));

HI_a30_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a58_combout,
	dataout => HI_a30_DATAA_driver);

HI_a30_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a30_DATAB_driver);

HI_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a30_DATAC_driver);

HI_a30_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(27),
	dataout => HI_a30_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N20
HI_a30 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a30_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & (Add0_a58_combout)) # (!Add0_a66_combout & ((HI(27))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a30_DATAA_driver,
	datab => HI_a30_DATAB_driver,
	datac => HI_a30_DATAC_driver,
	datad => HI_a30_DATAD_driver,
	combout => HI_a30_combout);

HI_a28_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a28_a_CLK_driver);

HI_a28_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a30_combout,
	dataout => HI_a28_a_D_driver);

HI_a28_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a28_a_ENA_driver);

-- Location: FF_X38_Y27_N21
HI_a28_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a28_a_CLK_driver,
	d => HI_a28_a_D_driver,
	ena => HI_a28_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(28));

left_a28_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a28_a_a3_combout,
	dataout => left_a28_a_DATAA_driver);

left_a28_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a28_a_DATAB_driver);

left_a28_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(28),
	dataout => left_a28_a_DATAC_driver);

left_a28_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(28),
	dataout => left_a28_a_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N16
left_a28_a : cycloneive_lcell_comb
-- Equation(s):
-- left(28) = (left_a28_a_a3_combout & (((HI(28))) # (!mux_alu_left_a0_a_ainput_o))) # (!left_a28_a_a3_combout & (mux_alu_left_a0_a_ainput_o & (PC(28))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a28_a_DATAA_driver,
	datab => left_a28_a_DATAB_driver,
	datac => left_a28_a_DATAC_driver,
	datad => left_a28_a_DATAD_driver,
	combout => left(28));

Add0_a60_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(29),
	dataout => Add0_a60_DATAA_driver);

Add0_a60_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a183_combout,
	dataout => Add0_a60_DATAB_driver);

Add0_a60_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a59,
	dataout => Add0_a60_CIN_driver);

-- Location: LCCOMB_X37_Y27_N10
Add0_a60 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a60_combout = ((left(29) $ (op2_real_a183_combout $ (!Add0_a59)))) # (GND)
-- Add0_a61 = CARRY((left(29) & ((op2_real_a183_combout) # (!Add0_a59))) # (!left(29) & (op2_real_a183_combout & !Add0_a59)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a60_DATAA_driver,
	datab => Add0_a60_DATAB_driver,
	datad => VCC,
	cin => Add0_a60_CIN_driver,
	combout => Add0_a60_combout,
	cout => Add0_a61);

Add0_a62_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a185_combout,
	dataout => Add0_a62_DATAA_driver);

Add0_a62_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(30),
	dataout => Add0_a62_DATAB_driver);

Add0_a62_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a61,
	dataout => Add0_a62_CIN_driver);

-- Location: LCCOMB_X37_Y27_N12
Add0_a62 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a62_combout = (op2_real_a185_combout & ((left(30) & (Add0_a61 & VCC)) # (!left(30) & (!Add0_a61)))) # (!op2_real_a185_combout & ((left(30) & (!Add0_a61)) # (!left(30) & ((Add0_a61) # (GND)))))
-- Add0_a63 = CARRY((op2_real_a185_combout & (!left(30) & !Add0_a61)) # (!op2_real_a185_combout & ((!Add0_a61) # (!left(30)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a62_DATAA_driver,
	datab => Add0_a62_DATAB_driver,
	datad => VCC,
	cin => Add0_a62_CIN_driver,
	combout => Add0_a62_combout,
	cout => Add0_a63);

HI_a32_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(29),
	dataout => HI_a32_DATAA_driver);

HI_a32_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a32_DATAB_driver);

HI_a32_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a32_DATAC_driver);

HI_a32_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a62_combout,
	dataout => HI_a32_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N28
HI_a32 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a32_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a62_combout))) # (!Add0_a66_combout & (HI(29)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a32_DATAA_driver,
	datab => HI_a32_DATAB_driver,
	datac => HI_a32_DATAC_driver,
	datad => HI_a32_DATAD_driver,
	combout => HI_a32_combout);

HI_a30_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a30_a_CLK_driver);

HI_a30_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a32_combout,
	dataout => HI_a30_a_D_driver);

HI_a30_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a30_a_ENA_driver);

-- Location: FF_X38_Y27_N29
HI_a30_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a30_a_CLK_driver,
	d => HI_a30_a_D_driver,
	ena => HI_a30_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(30));

PC_a32_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a30_a_ainput_o,
	dataout => PC_a32_DATAA_driver);

PC_a32_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a32_DATAC_driver);

PC_a32_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a62_combout,
	dataout => PC_a32_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N16
PC_a32 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a32_combout = (mux_pc_a1_a_ainput_o & (data_in_a30_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a62_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a32_DATAA_driver,
	datac => PC_a32_DATAC_driver,
	datad => PC_a32_DATAD_driver,
	combout => PC_a32_combout);

PC_a30_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a30_a_CLK_driver);

PC_a30_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a32_combout,
	dataout => PC_a30_a_D_driver);

PC_a30_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a30_a_SCLR_driver);

PC_a30_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a29_a_a30_combout,
	dataout => PC_a30_a_ENA_driver);

-- Location: FF_X35_Y27_N17
PC_a30_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a30_a_CLK_driver,
	d => PC_a30_a_D_driver,
	sclr => PC_a30_a_SCLR_driver,
	ena => PC_a30_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(30));

left_a30_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a30_a_a1_combout,
	dataout => left_a30_a_DATAA_driver);

left_a30_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(30),
	dataout => left_a30_a_DATAB_driver);

left_a30_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(30),
	dataout => left_a30_a_DATAC_driver);

left_a30_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a30_a_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N24
left_a30_a : cycloneive_lcell_comb
-- Equation(s):
-- left(30) = (left_a30_a_a1_combout & ((HI(30)) # ((!mux_alu_left_a0_a_ainput_o)))) # (!left_a30_a_a1_combout & (((PC(30) & mux_alu_left_a0_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a30_a_DATAA_driver,
	datab => left_a30_a_DATAB_driver,
	datac => left_a30_a_DATAC_driver,
	datad => left_a30_a_DATAD_driver,
	combout => left(30));

Add0_a64_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => op2_real_a187_combout,
	dataout => Add0_a64_DATAA_driver);

Add0_a64_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left(31),
	dataout => Add0_a64_DATAB_driver);

Add0_a64_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a63,
	dataout => Add0_a64_CIN_driver);

-- Location: LCCOMB_X37_Y27_N14
Add0_a64 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a64_combout = ((op2_real_a187_combout $ (left(31) $ (!Add0_a63)))) # (GND)
-- Add0_a65 = CARRY((op2_real_a187_combout & ((left(31)) # (!Add0_a63))) # (!op2_real_a187_combout & (left(31) & !Add0_a63)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => Add0_a64_DATAA_driver,
	datab => Add0_a64_DATAB_driver,
	datad => VCC,
	cin => Add0_a64_CIN_driver,
	combout => Add0_a64_combout,
	cout => Add0_a65);

HI_a33_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(30),
	dataout => HI_a33_DATAA_driver);

HI_a33_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a33_DATAB_driver);

HI_a33_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a33_DATAC_driver);

HI_a33_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a64_combout,
	dataout => HI_a33_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N26
HI_a33 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a33_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a64_combout))) # (!Add0_a66_combout & (HI(30)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a33_DATAA_driver,
	datab => HI_a33_DATAB_driver,
	datac => HI_a33_DATAC_driver,
	datad => HI_a33_DATAD_driver,
	combout => HI_a33_combout);

HI_a31_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a31_a_CLK_driver);

HI_a31_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a33_combout,
	dataout => HI_a31_a_D_driver);

HI_a31_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a31_a_ENA_driver);

-- Location: FF_X38_Y27_N27
HI_a31_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a31_a_CLK_driver,
	d => HI_a31_a_D_driver,
	ena => HI_a31_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(31));

A_a32_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a32_DATAB_driver);

A_a32_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a31_a_ainput_o,
	dataout => A_a32_DATAC_driver);

-- Location: LCCOMB_X35_Y27_N4
A_a32 : cycloneive_lcell_comb
-- Equation(s):
-- A_a32_combout = (!reset_ainput_o & data_in_a31_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => A_a32_DATAB_driver,
	datac => A_a32_DATAC_driver,
	combout => A_a32_combout);

A_a31_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a31_a_CLK_driver);

A_a31_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a32_combout,
	dataout => A_a31_a_ASDATA_driver);

A_a31_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a31_a_ENA_driver);

-- Location: FF_X39_Y27_N9
A_a31_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a31_a_CLK_driver,
	asdata => A_a31_a_ASDATA_driver,
	sload => VCC,
	ena => A_a31_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(31));

left_a31_a_a0_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a1_a_ainput_o,
	dataout => left_a31_a_a0_DATAA_driver);

left_a31_a_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a31_a_a0_DATAB_driver);

left_a31_a_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(31),
	dataout => left_a31_a_a0_DATAC_driver);

left_a31_a_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(30),
	dataout => left_a31_a_a0_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N28
left_a31_a_a0 : cycloneive_lcell_comb
-- Equation(s):
-- left_a31_a_a0_combout = (mux_alu_left_a1_a_ainput_o & ((mux_alu_left_a0_a_ainput_o) # ((HI(30))))) # (!mux_alu_left_a1_a_ainput_o & (!mux_alu_left_a0_a_ainput_o & (A(31))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a31_a_a0_DATAA_driver,
	datab => left_a31_a_a0_DATAB_driver,
	datac => left_a31_a_a0_DATAC_driver,
	datad => left_a31_a_a0_DATAD_driver,
	combout => left_a31_a_a0_combout);

left_a31_a_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(31),
	dataout => left_a31_a_DATAA_driver);

left_a31_a_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_alu_left_a0_a_ainput_o,
	dataout => left_a31_a_DATAB_driver);

left_a31_a_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(31),
	dataout => left_a31_a_DATAC_driver);

left_a31_a_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => left_a31_a_a0_combout,
	dataout => left_a31_a_DATAD_driver);

-- Location: LCCOMB_X38_Y27_N8
left_a31_a : cycloneive_lcell_comb
-- Equation(s):
-- left(31) = (mux_alu_left_a0_a_ainput_o & ((left_a31_a_a0_combout & ((HI(31)))) # (!left_a31_a_a0_combout & (PC(31))))) # (!mux_alu_left_a0_a_ainput_o & (((left_a31_a_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => left_a31_a_DATAA_driver,
	datab => left_a31_a_DATAB_driver,
	datac => left_a31_a_DATAC_driver,
	datad => left_a31_a_DATAD_driver,
	combout => left(31));

Add0_a66_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => alu_add_sub_ainput_o,
	dataout => Add0_a66_DATAD_driver);

Add0_a66_CIN_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a65,
	dataout => Add0_a66_CIN_driver);

-- Location: LCCOMB_X37_Y27_N16
Add0_a66 : cycloneive_lcell_comb
-- Equation(s):
-- Add0_a66_combout = Add0_a65 $ (!alu_add_sub_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => Add0_a66_DATAD_driver,
	cin => Add0_a66_CIN_driver,
	combout => Add0_a66_combout);

LO_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a0_a_ainput_o,
	dataout => LO_a0_DATAB_driver);

LO_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a0_DATAC_driver);

LO_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => LO_a0_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N8
LO_a0 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a0_combout = (mux_lo_a1_a_ainput_o & (data_in_a0_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((Add0_a66_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a0_DATAB_driver,
	datac => LO_a0_DATAC_driver,
	datad => LO_a0_DATAD_driver,
	combout => LO_a0_combout);

LO_a0_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a0_a_CLK_driver);

LO_a0_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_combout,
	dataout => LO_a0_a_D_driver);

LO_a0_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a0_a_SCLR_driver);

LO_a0_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a0_a_ENA_driver);

-- Location: FF_X41_Y27_N9
LO_a0_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a0_a_CLK_driver,
	d => LO_a0_a_D_driver,
	sclr => LO_a0_a_SCLR_driver,
	ena => LO_a0_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(0));

data_out_a2_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a1_combout,
	dataout => data_out_a2_DATAA_driver);

data_out_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(0),
	dataout => data_out_a2_DATAB_driver);

data_out_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a2_DATAC_driver);

data_out_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(0),
	dataout => data_out_a2_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N10
data_out_a2 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a2_combout = (data_out_a1_combout & ((LO(0)) # ((!mux_data_out_a1_a_ainput_o)))) # (!data_out_a1_combout & (((mux_data_out_a1_a_ainput_o & HI(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a2_DATAA_driver,
	datab => data_out_a2_DATAB_driver,
	datac => data_out_a2_DATAC_driver,
	datad => data_out_a2_DATAD_driver,
	combout => data_out_a2_combout);

data_out_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a3_DATAA_driver);

data_out_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(0),
	dataout => data_out_a3_DATAB_driver);

data_out_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a3_DATAC_driver);

data_out_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a2_combout,
	dataout => data_out_a3_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N16
data_out_a3 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a3_combout = (mux_data_out_a2_a_ainput_o & (PC(0) & (data_out_a0_combout))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a2_combout) # ((PC(0) & data_out_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a3_DATAA_driver,
	datab => data_out_a3_DATAB_driver,
	datac => data_out_a3_DATAC_driver,
	datad => data_out_a3_DATAD_driver,
	combout => data_out_a3_combout);

LO_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(0),
	dataout => LO_a2_DATAB_driver);

LO_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a2_DATAC_driver);

LO_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a1_a_ainput_o,
	dataout => LO_a2_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N2
LO_a2 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a2_combout = (mux_lo_a1_a_ainput_o & ((data_in_a1_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a2_DATAB_driver,
	datac => LO_a2_DATAC_driver,
	datad => LO_a2_DATAD_driver,
	combout => LO_a2_combout);

LO_a1_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a1_a_CLK_driver);

LO_a1_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a2_combout,
	dataout => LO_a1_a_D_driver);

LO_a1_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a1_a_SCLR_driver);

LO_a1_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a1_a_ENA_driver);

-- Location: FF_X41_Y27_N3
LO_a1_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a1_a_CLK_driver,
	d => LO_a1_a_D_driver,
	sclr => LO_a1_a_SCLR_driver,
	ena => LO_a1_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(1));

data_out_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a4_combout,
	dataout => data_out_a5_DATAA_driver);

data_out_a5_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(1),
	dataout => data_out_a5_DATAB_driver);

data_out_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a5_DATAC_driver);

data_out_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(1),
	dataout => data_out_a5_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N16
data_out_a5 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a5_combout = (data_out_a4_combout & ((LO(1)) # ((!mux_data_out_a0_a_ainput_o)))) # (!data_out_a4_combout & (((mux_data_out_a0_a_ainput_o & A(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a5_DATAA_driver,
	datab => data_out_a5_DATAB_driver,
	datac => data_out_a5_DATAC_driver,
	datad => data_out_a5_DATAD_driver,
	combout => data_out_a5_combout);

PC_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a2_DATAB_driver);

PC_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a4_combout,
	dataout => PC_a2_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N20
PC_a2 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a2_combout = (!mux_pc_a1_a_ainput_o & Add0_a4_combout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => PC_a2_DATAB_driver,
	datad => PC_a2_DATAD_driver,
	combout => PC_a2_combout);

PC_a1_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a1_a_CLK_driver);

PC_a1_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a2_combout,
	dataout => PC_a1_a_D_driver);

PC_a1_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a1_a_ainput_o,
	dataout => PC_a1_a_ASDATA_driver);

PC_a1_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a1_a_SCLR_driver);

PC_a1_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a1_a_SLOAD_driver);

PC_a1_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a1_a_ENA_driver);

-- Location: FF_X38_Y30_N21
PC_a1_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a1_a_CLK_driver,
	d => PC_a1_a_D_driver,
	asdata => PC_a1_a_ASDATA_driver,
	sclr => PC_a1_a_SCLR_driver,
	sload => PC_a1_a_SLOAD_driver,
	ena => PC_a1_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(1));

data_out_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a6_DATAA_driver);

data_out_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a5_combout,
	dataout => data_out_a6_DATAB_driver);

data_out_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(1),
	dataout => data_out_a6_DATAC_driver);

data_out_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a6_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N24
data_out_a6 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a6_combout = (mux_data_out_a2_a_ainput_o & (((PC(1) & data_out_a0_combout)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a5_combout) # ((PC(1) & data_out_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a6_DATAA_driver,
	datab => data_out_a6_DATAB_driver,
	datac => data_out_a6_DATAC_driver,
	datad => data_out_a6_DATAD_driver,
	combout => data_out_a6_combout);

intr_pc_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(0),
	dataout => intr_pc_a0_a_ainput_I_driver);

-- Location: IOIBUF_X1_Y43_N29
intr_pc_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a0_a_ainput_I_driver,
	o => intr_pc_a0_a_ainput_o);

PC_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a6_combout,
	dataout => PC_a3_DATAA_driver);

PC_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a3_DATAB_driver);

PC_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a0_a_ainput_o,
	dataout => PC_a3_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N16
PC_a3 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a3_combout = (mux_pc_a1_a_ainput_o & ((intr_pc_a0_a_ainput_o))) # (!mux_pc_a1_a_ainput_o & (Add0_a6_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a3_DATAA_driver,
	datab => PC_a3_DATAB_driver,
	datad => PC_a3_DATAD_driver,
	combout => PC_a3_combout);

PC_a2_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a2_a_CLK_driver);

PC_a2_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a3_combout,
	dataout => PC_a2_a_D_driver);

PC_a2_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a2_a_ainput_o,
	dataout => PC_a2_a_ASDATA_driver);

PC_a2_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a2_a_SCLR_driver);

PC_a2_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a2_a_SLOAD_driver);

PC_a2_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a2_a_ENA_driver);

-- Location: FF_X36_Y26_N17
PC_a2_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a2_a_CLK_driver,
	d => PC_a2_a_D_driver,
	asdata => PC_a2_a_ASDATA_driver,
	sclr => PC_a2_a_SCLR_driver,
	sload => PC_a2_a_SLOAD_driver,
	ena => PC_a2_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(2));

HI_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a4_DATAA_driver);

HI_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(1),
	dataout => HI_a4_DATAB_driver);

HI_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a6_combout,
	dataout => HI_a4_DATAC_driver);

HI_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a4_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N16
HI_a4 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a4_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a6_combout))) # (!Add0_a66_combout & (HI(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a4_DATAA_driver,
	datab => HI_a4_DATAB_driver,
	datac => HI_a4_DATAC_driver,
	datad => HI_a4_DATAD_driver,
	combout => HI_a4_combout);

HI_a2_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a2_a_CLK_driver);

HI_a2_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a4_combout,
	dataout => HI_a2_a_D_driver);

HI_a2_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a2_a_ENA_driver);

-- Location: FF_X36_Y29_N17
HI_a2_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a2_a_CLK_driver,
	d => HI_a2_a_D_driver,
	ena => HI_a2_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(2));

LO_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a3_DATAA_driver);

LO_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a2_a_ainput_o,
	dataout => LO_a3_DATAC_driver);

LO_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(1),
	dataout => LO_a3_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N28
LO_a3 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a3_combout = (mux_lo_a1_a_ainput_o & (data_in_a2_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a3_DATAA_driver,
	datac => LO_a3_DATAC_driver,
	datad => LO_a3_DATAD_driver,
	combout => LO_a3_combout);

LO_a2_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a2_a_CLK_driver);

LO_a2_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a3_combout,
	dataout => LO_a2_a_D_driver);

LO_a2_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a2_a_SCLR_driver);

LO_a2_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a2_a_ENA_driver);

-- Location: FF_X41_Y27_N29
LO_a2_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a2_a_CLK_driver,
	d => LO_a2_a_D_driver,
	sclr => LO_a2_a_SCLR_driver,
	ena => LO_a2_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(2));

A_a2_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a2_a_CLK_driver);

A_a2_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_combout,
	dataout => A_a2_a_ASDATA_driver);

A_a2_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a2_a_ENA_driver);

-- Location: FF_X37_Y29_N9
A_a2_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a2_a_CLK_driver,
	asdata => A_a2_a_ASDATA_driver,
	sload => VCC,
	ena => A_a2_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(2));

mux_out_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_out(0),
	dataout => mux_out_a0_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y29_N15
mux_out_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_out_a0_a_ainput_I_driver,
	o => mux_out_a0_a_ainput_o);

O_a2_a_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a6_combout,
	dataout => O_a2_a_a1_DATAA_driver);

O_a2_a_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a2_a_a1_DATAB_driver);

O_a2_a_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a2_a_ainput_o,
	dataout => O_a2_a_a1_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N4
O_a2_a_a1 : cycloneive_lcell_comb
-- Equation(s):
-- O_a2_a_a1_combout = (mux_out_a0_a_ainput_o & (Add0_a6_combout)) # (!mux_out_a0_a_ainput_o & ((data_in_a2_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a2_a_a1_DATAA_driver,
	datab => O_a2_a_a1_DATAB_driver,
	datad => O_a2_a_a1_DATAD_driver,
	combout => O_a2_a_a1_combout);

data_in_a3_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(3),
	dataout => data_in_a3_a_ainput_I_driver);

-- Location: IOIBUF_X41_Y43_N8
data_in_a3_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a3_a_ainput_I_driver,
	o => data_in_a3_a_ainput_o);

O_a3_a_a2_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a8_combout,
	dataout => O_a3_a_a2_DATAA_driver);

O_a3_a_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a3_a_a2_DATAB_driver);

O_a3_a_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a3_a_ainput_o,
	dataout => O_a3_a_a2_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N22
O_a3_a_a2 : cycloneive_lcell_comb
-- Equation(s):
-- O_a3_a_a2_combout = (mux_out_a0_a_ainput_o & (Add0_a8_combout)) # (!mux_out_a0_a_ainput_o & ((data_in_a3_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a3_a_a2_DATAA_driver,
	datab => O_a3_a_a2_DATAB_driver,
	datad => O_a3_a_a2_DATAD_driver,
	combout => O_a3_a_a2_combout);

data_in_a4_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(4),
	dataout => data_in_a4_a_ainput_I_driver);

-- Location: IOIBUF_X59_Y43_N8
data_in_a4_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a4_a_ainput_I_driver,
	o => data_in_a4_a_ainput_o);

O_a4_a_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a4_a_a3_DATAA_driver);

O_a4_a_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a4_a_ainput_o,
	dataout => O_a4_a_a3_DATAB_driver);

O_a4_a_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a10_combout,
	dataout => O_a4_a_a3_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N8
O_a4_a_a3 : cycloneive_lcell_comb
-- Equation(s):
-- O_a4_a_a3_combout = (mux_out_a0_a_ainput_o & ((Add0_a10_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a4_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a4_a_a3_DATAA_driver,
	datab => O_a4_a_a3_DATAB_driver,
	datad => O_a4_a_a3_DATAD_driver,
	combout => O_a4_a_a3_combout);

data_in_a5_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(5),
	dataout => data_in_a5_a_ainput_I_driver);

-- Location: IOIBUF_X61_Y43_N1
data_in_a5_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a5_a_ainput_I_driver,
	o => data_in_a5_a_ainput_o);

O_a5_a_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a5_a_a4_DATAA_driver);

O_a5_a_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a5_a_ainput_o,
	dataout => O_a5_a_a4_DATAB_driver);

O_a5_a_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a12_combout,
	dataout => O_a5_a_a4_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N26
O_a5_a_a4 : cycloneive_lcell_comb
-- Equation(s):
-- O_a5_a_a4_combout = (mux_out_a0_a_ainput_o & ((Add0_a12_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a5_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a5_a_a4_DATAA_driver,
	datab => O_a5_a_a4_DATAB_driver,
	datad => O_a5_a_a4_DATAD_driver,
	combout => O_a5_a_a4_combout);

data_in_a6_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(6),
	dataout => data_in_a6_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y31_N22
data_in_a6_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a6_a_ainput_I_driver,
	o => data_in_a6_a_ainput_o);

O_a6_a_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a6_a_a5_DATAA_driver);

O_a6_a_a5_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a6_a_ainput_o,
	dataout => O_a6_a_a5_DATAB_driver);

O_a6_a_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a14_combout,
	dataout => O_a6_a_a5_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N30
O_a6_a_a5 : cycloneive_lcell_comb
-- Equation(s):
-- O_a6_a_a5_combout = (mux_out_a0_a_ainput_o & ((Add0_a14_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a6_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a6_a_a5_DATAA_driver,
	datab => O_a6_a_a5_DATAB_driver,
	datad => O_a6_a_a5_DATAD_driver,
	combout => O_a6_a_a5_combout);

O_a7_a_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a16_combout,
	dataout => O_a7_a_a6_DATAA_driver);

O_a7_a_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a7_a_ainput_o,
	dataout => O_a7_a_a6_DATAB_driver);

O_a7_a_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a7_a_a6_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N28
O_a7_a_a6 : cycloneive_lcell_comb
-- Equation(s):
-- O_a7_a_a6_combout = (mux_out_a0_a_ainput_o & (Add0_a16_combout)) # (!mux_out_a0_a_ainput_o & ((data_in_a7_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a7_a_a6_DATAA_driver,
	datab => O_a7_a_a6_DATAB_driver,
	datad => O_a7_a_a6_DATAD_driver,
	combout => O_a7_a_a6_combout);

O_a8_a_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a8_a_a7_DATAA_driver);

O_a8_a_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a8_a_ainput_o,
	dataout => O_a8_a_a7_DATAB_driver);

O_a8_a_a7_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a18_combout,
	dataout => O_a8_a_a7_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N22
O_a8_a_a7 : cycloneive_lcell_comb
-- Equation(s):
-- O_a8_a_a7_combout = (mux_out_a0_a_ainput_o & ((Add0_a18_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a8_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a8_a_a7_DATAA_driver,
	datab => O_a8_a_a7_DATAB_driver,
	datad => O_a8_a_a7_DATAD_driver,
	combout => O_a8_a_a7_combout);

O_a9_a_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a9_a_a8_DATAA_driver);

O_a9_a_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a9_a_ainput_o,
	dataout => O_a9_a_a8_DATAB_driver);

O_a9_a_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a20_combout,
	dataout => O_a9_a_a8_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N26
O_a9_a_a8 : cycloneive_lcell_comb
-- Equation(s):
-- O_a9_a_a8_combout = (mux_out_a0_a_ainput_o & ((Add0_a20_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a9_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a9_a_a8_DATAA_driver,
	datab => O_a9_a_a8_DATAB_driver,
	datad => O_a9_a_a8_DATAD_driver,
	combout => O_a9_a_a8_combout);

O_a10_a_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a10_a_a9_DATAA_driver);

O_a10_a_a9_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a10_a_ainput_o,
	dataout => O_a10_a_a9_DATAB_driver);

O_a10_a_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a22_combout,
	dataout => O_a10_a_a9_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N14
O_a10_a_a9 : cycloneive_lcell_comb
-- Equation(s):
-- O_a10_a_a9_combout = (mux_out_a0_a_ainput_o & ((Add0_a22_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a10_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a10_a_a9_DATAA_driver,
	datab => O_a10_a_a9_DATAB_driver,
	datad => O_a10_a_a9_DATAD_driver,
	combout => O_a10_a_a9_combout);

O_a11_a_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a11_a_a10_DATAA_driver);

O_a11_a_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a11_a_ainput_o,
	dataout => O_a11_a_a10_DATAB_driver);

O_a11_a_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a24_combout,
	dataout => O_a11_a_a10_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N8
O_a11_a_a10 : cycloneive_lcell_comb
-- Equation(s):
-- O_a11_a_a10_combout = (mux_out_a0_a_ainput_o & ((Add0_a24_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a11_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a11_a_a10_DATAA_driver,
	datab => O_a11_a_a10_DATAB_driver,
	datad => O_a11_a_a10_DATAD_driver,
	combout => O_a11_a_a10_combout);

O_a12_a_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a12_a_ainput_o,
	dataout => O_a12_a_a11_DATAA_driver);

O_a12_a_a11_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a26_combout,
	dataout => O_a12_a_a11_DATAB_driver);

O_a12_a_a11_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a12_a_a11_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N4
O_a12_a_a11 : cycloneive_lcell_comb
-- Equation(s):
-- O_a12_a_a11_combout = (mux_out_a0_a_ainput_o & ((Add0_a26_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a12_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a12_a_a11_DATAA_driver,
	datab => O_a12_a_a11_DATAB_driver,
	datad => O_a12_a_a11_DATAD_driver,
	combout => O_a12_a_a11_combout);

O_a13_a_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a13_a_a12_DATAA_driver);

O_a13_a_a12_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a13_a_ainput_o,
	dataout => O_a13_a_a12_DATAB_driver);

O_a13_a_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a28_combout,
	dataout => O_a13_a_a12_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N30
O_a13_a_a12 : cycloneive_lcell_comb
-- Equation(s):
-- O_a13_a_a12_combout = (mux_out_a0_a_ainput_o & ((Add0_a28_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a13_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a13_a_a12_DATAA_driver,
	datab => O_a13_a_a12_DATAB_driver,
	datad => O_a13_a_a12_DATAD_driver,
	combout => O_a13_a_a12_combout);

O_a14_a_a13_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a14_a_a13_DATAA_driver);

O_a14_a_a13_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a14_a_ainput_o,
	dataout => O_a14_a_a13_DATAB_driver);

O_a14_a_a13_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a30_combout,
	dataout => O_a14_a_a13_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N0
O_a14_a_a13 : cycloneive_lcell_comb
-- Equation(s):
-- O_a14_a_a13_combout = (mux_out_a0_a_ainput_o & ((Add0_a30_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a14_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a14_a_a13_DATAA_driver,
	datab => O_a14_a_a13_DATAB_driver,
	datad => O_a14_a_a13_DATAD_driver,
	combout => O_a14_a_a13_combout);

O_a15_a_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a15_a_ainput_o,
	dataout => O_a15_a_a14_DATAA_driver);

O_a15_a_a14_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a32_combout,
	dataout => O_a15_a_a14_DATAB_driver);

O_a15_a_a14_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a15_a_a14_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N2
O_a15_a_a14 : cycloneive_lcell_comb
-- Equation(s):
-- O_a15_a_a14_combout = (mux_out_a0_a_ainput_o & ((Add0_a32_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a15_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a15_a_a14_DATAA_driver,
	datab => O_a15_a_a14_DATAB_driver,
	datad => O_a15_a_a14_DATAD_driver,
	combout => O_a15_a_a14_combout);

O_a16_a_a15_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a16_a_ainput_o,
	dataout => O_a16_a_a15_DATAA_driver);

O_a16_a_a15_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a16_a_a15_DATAB_driver);

O_a16_a_a15_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a34_combout,
	dataout => O_a16_a_a15_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N16
O_a16_a_a15 : cycloneive_lcell_comb
-- Equation(s):
-- O_a16_a_a15_combout = (mux_out_a0_a_ainput_o & ((Add0_a34_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a16_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a16_a_a15_DATAA_driver,
	datab => O_a16_a_a15_DATAB_driver,
	datad => O_a16_a_a15_DATAD_driver,
	combout => O_a16_a_a15_combout);

O_a17_a_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a17_a_ainput_o,
	dataout => O_a17_a_a16_DATAA_driver);

O_a17_a_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a17_a_a16_DATAB_driver);

O_a17_a_a16_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a36_combout,
	dataout => O_a17_a_a16_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N28
O_a17_a_a16 : cycloneive_lcell_comb
-- Equation(s):
-- O_a17_a_a16_combout = (mux_out_a0_a_ainput_o & ((Add0_a36_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a17_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a17_a_a16_DATAA_driver,
	datab => O_a17_a_a16_DATAB_driver,
	datad => O_a17_a_a16_DATAD_driver,
	combout => O_a17_a_a16_combout);

O_a18_a_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a18_a_ainput_o,
	dataout => O_a18_a_a17_DATAA_driver);

O_a18_a_a17_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a18_a_a17_DATAB_driver);

O_a18_a_a17_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a38_combout,
	dataout => O_a18_a_a17_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N14
O_a18_a_a17 : cycloneive_lcell_comb
-- Equation(s):
-- O_a18_a_a17_combout = (mux_out_a0_a_ainput_o & ((Add0_a38_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a18_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a18_a_a17_DATAA_driver,
	datab => O_a18_a_a17_DATAB_driver,
	datad => O_a18_a_a17_DATAD_driver,
	combout => O_a18_a_a17_combout);

O_a19_a_a18_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a19_a_ainput_o,
	dataout => O_a19_a_a18_DATAA_driver);

O_a19_a_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a19_a_a18_DATAB_driver);

O_a19_a_a18_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a40_combout,
	dataout => O_a19_a_a18_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N4
O_a19_a_a18 : cycloneive_lcell_comb
-- Equation(s):
-- O_a19_a_a18_combout = (mux_out_a0_a_ainput_o & ((Add0_a40_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a19_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a19_a_a18_DATAA_driver,
	datab => O_a19_a_a18_DATAB_driver,
	datad => O_a19_a_a18_DATAD_driver,
	combout => O_a19_a_a18_combout);

O_a20_a_a19_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a20_a_ainput_o,
	dataout => O_a20_a_a19_DATAA_driver);

O_a20_a_a19_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a20_a_a19_DATAB_driver);

O_a20_a_a19_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a42_combout,
	dataout => O_a20_a_a19_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N22
O_a20_a_a19 : cycloneive_lcell_comb
-- Equation(s):
-- O_a20_a_a19_combout = (mux_out_a0_a_ainput_o & ((Add0_a42_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a20_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a20_a_a19_DATAA_driver,
	datab => O_a20_a_a19_DATAB_driver,
	datad => O_a20_a_a19_DATAD_driver,
	combout => O_a20_a_a19_combout);

O_a21_a_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a21_a_ainput_o,
	dataout => O_a21_a_a20_DATAA_driver);

O_a21_a_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a21_a_a20_DATAB_driver);

O_a21_a_a20_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a44_combout,
	dataout => O_a21_a_a20_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N26
O_a21_a_a20 : cycloneive_lcell_comb
-- Equation(s):
-- O_a21_a_a20_combout = (mux_out_a0_a_ainput_o & ((Add0_a44_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a21_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a21_a_a20_DATAA_driver,
	datab => O_a21_a_a20_DATAB_driver,
	datad => O_a21_a_a20_DATAD_driver,
	combout => O_a21_a_a20_combout);

O_a22_a_a21_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a22_a_a21_DATAA_driver);

O_a22_a_a21_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a22_a_ainput_o,
	dataout => O_a22_a_a21_DATAB_driver);

O_a22_a_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a46_combout,
	dataout => O_a22_a_a21_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N14
O_a22_a_a21 : cycloneive_lcell_comb
-- Equation(s):
-- O_a22_a_a21_combout = (mux_out_a0_a_ainput_o & ((Add0_a46_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a22_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a22_a_a21_DATAA_driver,
	datab => O_a22_a_a21_DATAB_driver,
	datad => O_a22_a_a21_DATAD_driver,
	combout => O_a22_a_a21_combout);

O_a23_a_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a23_a_a22_DATAA_driver);

O_a23_a_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a23_a_ainput_o,
	dataout => O_a23_a_a22_DATAB_driver);

O_a23_a_a22_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a48_combout,
	dataout => O_a23_a_a22_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N8
O_a23_a_a22 : cycloneive_lcell_comb
-- Equation(s):
-- O_a23_a_a22_combout = (mux_out_a0_a_ainput_o & ((Add0_a48_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a23_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a23_a_a22_DATAA_driver,
	datab => O_a23_a_a22_DATAB_driver,
	datad => O_a23_a_a22_DATAD_driver,
	combout => O_a23_a_a22_combout);

O_a24_a_a23_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a24_a_ainput_o,
	dataout => O_a24_a_a23_DATAA_driver);

O_a24_a_a23_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a50_combout,
	dataout => O_a24_a_a23_DATAB_driver);

O_a24_a_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a24_a_a23_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N30
O_a24_a_a23 : cycloneive_lcell_comb
-- Equation(s):
-- O_a24_a_a23_combout = (mux_out_a0_a_ainput_o & ((Add0_a50_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a24_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a24_a_a23_DATAA_driver,
	datab => O_a24_a_a23_DATAB_driver,
	datad => O_a24_a_a23_DATAD_driver,
	combout => O_a24_a_a23_combout);

O_a25_a_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a25_a_ainput_o,
	dataout => O_a25_a_a24_DATAA_driver);

O_a25_a_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a25_a_a24_DATAB_driver);

O_a25_a_a24_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a52_combout,
	dataout => O_a25_a_a24_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N24
O_a25_a_a24 : cycloneive_lcell_comb
-- Equation(s):
-- O_a25_a_a24_combout = (mux_out_a0_a_ainput_o & ((Add0_a52_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a25_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a25_a_a24_DATAA_driver,
	datab => O_a25_a_a24_DATAB_driver,
	datad => O_a25_a_a24_DATAD_driver,
	combout => O_a25_a_a24_combout);

O_a26_a_a25_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a26_a_ainput_o,
	dataout => O_a26_a_a25_DATAA_driver);

O_a26_a_a25_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a26_a_a25_DATAB_driver);

O_a26_a_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a54_combout,
	dataout => O_a26_a_a25_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N26
O_a26_a_a25 : cycloneive_lcell_comb
-- Equation(s):
-- O_a26_a_a25_combout = (mux_out_a0_a_ainput_o & ((Add0_a54_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a26_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a26_a_a25_DATAA_driver,
	datab => O_a26_a_a25_DATAB_driver,
	datad => O_a26_a_a25_DATAD_driver,
	combout => O_a26_a_a25_combout);

O_a27_a_a26_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a27_a_a26_DATAA_driver);

O_a27_a_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a27_a_ainput_o,
	dataout => O_a27_a_a26_DATAB_driver);

O_a27_a_a26_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a56_combout,
	dataout => O_a27_a_a26_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N28
O_a27_a_a26 : cycloneive_lcell_comb
-- Equation(s):
-- O_a27_a_a26_combout = (mux_out_a0_a_ainput_o & ((Add0_a56_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a27_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a27_a_a26_DATAA_driver,
	datab => O_a27_a_a26_DATAB_driver,
	datad => O_a27_a_a26_DATAD_driver,
	combout => O_a27_a_a26_combout);

O_a28_a_a27_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a28_a_ainput_o,
	dataout => O_a28_a_a27_DATAA_driver);

O_a28_a_a27_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a28_a_a27_DATAB_driver);

O_a28_a_a27_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a58_combout,
	dataout => O_a28_a_a27_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N2
O_a28_a_a27 : cycloneive_lcell_comb
-- Equation(s):
-- O_a28_a_a27_combout = (mux_out_a0_a_ainput_o & ((Add0_a58_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a28_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a28_a_a27_DATAA_driver,
	datab => O_a28_a_a27_DATAB_driver,
	datad => O_a28_a_a27_DATAD_driver,
	combout => O_a28_a_a27_combout);

O_a29_a_a28_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a29_a_a28_DATAA_driver);

O_a29_a_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a60_combout,
	dataout => O_a29_a_a28_DATAB_driver);

O_a29_a_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a29_a_ainput_o,
	dataout => O_a29_a_a28_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N4
O_a29_a_a28 : cycloneive_lcell_comb
-- Equation(s):
-- O_a29_a_a28_combout = (mux_out_a0_a_ainput_o & (Add0_a60_combout)) # (!mux_out_a0_a_ainput_o & ((data_in_a29_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a29_a_a28_DATAA_driver,
	datab => O_a29_a_a28_DATAB_driver,
	datad => O_a29_a_a28_DATAD_driver,
	combout => O_a29_a_a28_combout);

data_in_a30_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(30),
	dataout => data_in_a30_a_ainput_I_driver);

-- Location: IOIBUF_X34_Y0_N29
data_in_a30_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a30_a_ainput_I_driver,
	o => data_in_a30_a_ainput_o);

O_a30_a_a29_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a30_a_a29_DATAA_driver);

O_a30_a_a29_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a62_combout,
	dataout => O_a30_a_a29_DATAB_driver);

O_a30_a_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a30_a_ainput_o,
	dataout => O_a30_a_a29_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N22
O_a30_a_a29 : cycloneive_lcell_comb
-- Equation(s):
-- O_a30_a_a29_combout = (mux_out_a0_a_ainput_o & (Add0_a62_combout)) # (!mux_out_a0_a_ainput_o & ((data_in_a30_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a30_a_a29_DATAA_driver,
	datab => O_a30_a_a29_DATAB_driver,
	datad => O_a30_a_a29_DATAD_driver,
	combout => O_a30_a_a29_combout);

mux_out_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_out(1),
	dataout => mux_out_a1_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y31_N8
mux_out_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_out_a1_a_ainput_I_driver,
	o => mux_out_a1_a_ainput_o);

O_n_a31_a_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a2_a_ainput_o,
	dataout => O_n_a31_a_a4_DATAA_driver);

O_n_a31_a_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a64_combout,
	dataout => O_n_a31_a_a4_DATAC_driver);

O_n_a31_a_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a31_a_ainput_o,
	dataout => O_n_a31_a_a4_DATAD_driver);

-- Location: LCCOMB_X37_Y27_N30
O_n_a31_a_a4 : cycloneive_lcell_comb
-- Equation(s):
-- O_n_a31_a_a4_combout = (mux_out_a2_a_ainput_o & ((data_in_a31_a_ainput_o))) # (!mux_out_a2_a_ainput_o & (Add0_a64_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_n_a31_a_a4_DATAA_driver,
	datac => O_n_a31_a_a4_DATAC_driver,
	datad => O_n_a31_a_a4_DATAD_driver,
	combout => O_n_a31_a_a4_combout);

O_n_a31_a_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a31_a_a5_combout,
	dataout => O_n_a31_a_a6_DATAA_driver);

O_n_a31_a_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_n_a31_a_a6_DATAB_driver);

O_n_a31_a_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(31),
	dataout => O_n_a31_a_a6_DATAC_driver);

O_n_a31_a_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a31_a_a4_combout,
	dataout => O_n_a31_a_a6_DATAD_driver);

-- Location: LCCOMB_X37_Y27_N20
O_n_a31_a_a6 : cycloneive_lcell_comb
-- Equation(s):
-- O_n_a31_a_a6_combout = (O_n_a31_a_a5_combout & ((mux_out_a1_a_ainput_o & (O(31))) # (!mux_out_a1_a_ainput_o & ((O_n_a31_a_a4_combout))))) # (!O_n_a31_a_a5_combout & (!mux_out_a1_a_ainput_o & (O(31))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_n_a31_a_a6_DATAA_driver,
	datab => O_n_a31_a_a6_DATAB_driver,
	datac => O_n_a31_a_a6_DATAC_driver,
	datad => O_n_a31_a_a6_DATAD_driver,
	combout => O_n_a31_a_a6_combout);

O_a31_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a31_a_CLK_driver);

O_a31_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a31_a_a6_combout,
	dataout => O_a31_a_D_driver);

O_a31_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => O_a31_a_SCLR_driver);

-- Location: FF_X37_Y27_N21
O_a31_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a31_a_CLK_driver,
	d => O_a31_a_D_driver,
	sclr => O_a31_a_SCLR_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(31));

O_a27_a_a30_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a27_a_a30_DATAA_driver);

O_a27_a_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => O_a27_a_a30_DATAC_driver);

O_a27_a_a30_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a27_a_a30_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N18
O_a27_a_a30 : cycloneive_lcell_comb
-- Equation(s):
-- O_a27_a_a30_combout = (reset_ainput_o) # ((mux_out_a0_a_ainput_o & mux_out_a1_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a27_a_a30_DATAA_driver,
	datac => O_a27_a_a30_DATAC_driver,
	datad => O_a27_a_a30_DATAD_driver,
	combout => O_a27_a_a30_combout);

mux_out_a2_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_out(2),
	dataout => mux_out_a2_a_ainput_I_driver);

-- Location: IOIBUF_X11_Y43_N29
mux_out_a2_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_out_a2_a_ainput_I_driver,
	o => mux_out_a2_a_ainput_o);

O_a27_a_a31_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a27_a_a31_DATAA_driver);

O_a27_a_a31_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => O_a27_a_a31_DATAB_driver);

O_a27_a_a31_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a2_a_ainput_o,
	dataout => O_a27_a_a31_DATAC_driver);

O_a27_a_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a27_a_a31_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N12
O_a27_a_a31 : cycloneive_lcell_comb
-- Equation(s):
-- O_a27_a_a31_combout = (reset_ainput_o) # (mux_out_a2_a_ainput_o $ (((mux_out_a0_a_ainput_o) # (mux_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a27_a_a31_DATAA_driver,
	datab => O_a27_a_a31_DATAB_driver,
	datac => O_a27_a_a31_DATAC_driver,
	datad => O_a27_a_a31_DATAD_driver,
	combout => O_a27_a_a31_combout);

O_a30_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a30_a_CLK_driver);

O_a30_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a30_a_a29_combout,
	dataout => O_a30_a_D_driver);

O_a30_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(31),
	dataout => O_a30_a_ASDATA_driver);

O_a30_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a30_a_SCLR_driver);

O_a30_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a30_a_SLOAD_driver);

O_a30_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a30_a_ENA_driver);

-- Location: FF_X36_Y27_N23
O_a30_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a30_a_CLK_driver,
	d => O_a30_a_D_driver,
	asdata => O_a30_a_ASDATA_driver,
	sclr => O_a30_a_SCLR_driver,
	sload => O_a30_a_SLOAD_driver,
	ena => O_a30_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(30));

O_a29_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a29_a_CLK_driver);

O_a29_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a29_a_a28_combout,
	dataout => O_a29_a_D_driver);

O_a29_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(30),
	dataout => O_a29_a_ASDATA_driver);

O_a29_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a29_a_SCLR_driver);

O_a29_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a29_a_SLOAD_driver);

O_a29_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a29_a_ENA_driver);

-- Location: FF_X36_Y27_N5
O_a29_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a29_a_CLK_driver,
	d => O_a29_a_D_driver,
	asdata => O_a29_a_ASDATA_driver,
	sclr => O_a29_a_SCLR_driver,
	sload => O_a29_a_SLOAD_driver,
	ena => O_a29_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(29));

O_a28_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a28_a_CLK_driver);

O_a28_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a28_a_a27_combout,
	dataout => O_a28_a_D_driver);

O_a28_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(29),
	dataout => O_a28_a_ASDATA_driver);

O_a28_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a28_a_SCLR_driver);

O_a28_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a28_a_SLOAD_driver);

O_a28_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a28_a_ENA_driver);

-- Location: FF_X36_Y27_N3
O_a28_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a28_a_CLK_driver,
	d => O_a28_a_D_driver,
	asdata => O_a28_a_ASDATA_driver,
	sclr => O_a28_a_SCLR_driver,
	sload => O_a28_a_SLOAD_driver,
	ena => O_a28_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(28));

O_a27_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a27_a_CLK_driver);

O_a27_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a26_combout,
	dataout => O_a27_a_D_driver);

O_a27_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(28),
	dataout => O_a27_a_ASDATA_driver);

O_a27_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a27_a_SCLR_driver);

O_a27_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a27_a_SLOAD_driver);

O_a27_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a27_a_ENA_driver);

-- Location: FF_X36_Y27_N29
O_a27_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a27_a_CLK_driver,
	d => O_a27_a_D_driver,
	asdata => O_a27_a_ASDATA_driver,
	sclr => O_a27_a_SCLR_driver,
	sload => O_a27_a_SLOAD_driver,
	ena => O_a27_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(27));

O_a26_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a26_a_CLK_driver);

O_a26_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a26_a_a25_combout,
	dataout => O_a26_a_D_driver);

O_a26_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(27),
	dataout => O_a26_a_ASDATA_driver);

O_a26_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a26_a_SCLR_driver);

O_a26_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a26_a_SLOAD_driver);

O_a26_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a26_a_ENA_driver);

-- Location: FF_X36_Y27_N27
O_a26_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a26_a_CLK_driver,
	d => O_a26_a_D_driver,
	asdata => O_a26_a_ASDATA_driver,
	sclr => O_a26_a_SCLR_driver,
	sload => O_a26_a_SLOAD_driver,
	ena => O_a26_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(26));

O_a25_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a25_a_CLK_driver);

O_a25_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a25_a_a24_combout,
	dataout => O_a25_a_D_driver);

O_a25_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(26),
	dataout => O_a25_a_ASDATA_driver);

O_a25_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a25_a_SCLR_driver);

O_a25_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a25_a_SLOAD_driver);

O_a25_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a25_a_ENA_driver);

-- Location: FF_X36_Y27_N25
O_a25_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a25_a_CLK_driver,
	d => O_a25_a_D_driver,
	asdata => O_a25_a_ASDATA_driver,
	sclr => O_a25_a_SCLR_driver,
	sload => O_a25_a_SLOAD_driver,
	ena => O_a25_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(25));

O_a24_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a24_a_CLK_driver);

O_a24_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a24_a_a23_combout,
	dataout => O_a24_a_D_driver);

O_a24_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(25),
	dataout => O_a24_a_ASDATA_driver);

O_a24_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a24_a_SCLR_driver);

O_a24_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a24_a_SLOAD_driver);

O_a24_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a24_a_ENA_driver);

-- Location: FF_X36_Y27_N31
O_a24_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a24_a_CLK_driver,
	d => O_a24_a_D_driver,
	asdata => O_a24_a_ASDATA_driver,
	sclr => O_a24_a_SCLR_driver,
	sload => O_a24_a_SLOAD_driver,
	ena => O_a24_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(24));

O_a23_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a23_a_CLK_driver);

O_a23_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a23_a_a22_combout,
	dataout => O_a23_a_D_driver);

O_a23_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(24),
	dataout => O_a23_a_ASDATA_driver);

O_a23_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a23_a_SCLR_driver);

O_a23_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a23_a_SLOAD_driver);

O_a23_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a23_a_ENA_driver);

-- Location: FF_X36_Y27_N9
O_a23_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a23_a_CLK_driver,
	d => O_a23_a_D_driver,
	asdata => O_a23_a_ASDATA_driver,
	sclr => O_a23_a_SCLR_driver,
	sload => O_a23_a_SLOAD_driver,
	ena => O_a23_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(23));

O_a22_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a22_a_CLK_driver);

O_a22_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a22_a_a21_combout,
	dataout => O_a22_a_D_driver);

O_a22_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(23),
	dataout => O_a22_a_ASDATA_driver);

O_a22_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a22_a_SCLR_driver);

O_a22_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a22_a_SLOAD_driver);

O_a22_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a22_a_ENA_driver);

-- Location: FF_X36_Y27_N15
O_a22_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a22_a_CLK_driver,
	d => O_a22_a_D_driver,
	asdata => O_a22_a_ASDATA_driver,
	sclr => O_a22_a_SCLR_driver,
	sload => O_a22_a_SLOAD_driver,
	ena => O_a22_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(22));

O_a21_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a21_a_CLK_driver);

O_a21_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a21_a_a20_combout,
	dataout => O_a21_a_D_driver);

O_a21_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(22),
	dataout => O_a21_a_ASDATA_driver);

O_a21_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a21_a_SCLR_driver);

O_a21_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a21_a_SLOAD_driver);

O_a21_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a21_a_ENA_driver);

-- Location: FF_X37_Y25_N27
O_a21_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a21_a_CLK_driver,
	d => O_a21_a_D_driver,
	asdata => O_a21_a_ASDATA_driver,
	sclr => O_a21_a_SCLR_driver,
	sload => O_a21_a_SLOAD_driver,
	ena => O_a21_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(21));

O_a20_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a20_a_CLK_driver);

O_a20_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a20_a_a19_combout,
	dataout => O_a20_a_D_driver);

O_a20_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(21),
	dataout => O_a20_a_ASDATA_driver);

O_a20_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a20_a_SCLR_driver);

O_a20_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a20_a_SLOAD_driver);

O_a20_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a20_a_ENA_driver);

-- Location: FF_X37_Y25_N23
O_a20_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a20_a_CLK_driver,
	d => O_a20_a_D_driver,
	asdata => O_a20_a_ASDATA_driver,
	sclr => O_a20_a_SCLR_driver,
	sload => O_a20_a_SLOAD_driver,
	ena => O_a20_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(20));

O_a19_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a19_a_CLK_driver);

O_a19_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a19_a_a18_combout,
	dataout => O_a19_a_D_driver);

O_a19_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(20),
	dataout => O_a19_a_ASDATA_driver);

O_a19_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a19_a_SCLR_driver);

O_a19_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a19_a_SLOAD_driver);

O_a19_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a19_a_ENA_driver);

-- Location: FF_X37_Y25_N5
O_a19_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a19_a_CLK_driver,
	d => O_a19_a_D_driver,
	asdata => O_a19_a_ASDATA_driver,
	sclr => O_a19_a_SCLR_driver,
	sload => O_a19_a_SLOAD_driver,
	ena => O_a19_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(19));

O_a18_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a18_a_CLK_driver);

O_a18_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a18_a_a17_combout,
	dataout => O_a18_a_D_driver);

O_a18_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(19),
	dataout => O_a18_a_ASDATA_driver);

O_a18_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a18_a_SCLR_driver);

O_a18_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a18_a_SLOAD_driver);

O_a18_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a18_a_ENA_driver);

-- Location: FF_X37_Y25_N15
O_a18_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a18_a_CLK_driver,
	d => O_a18_a_D_driver,
	asdata => O_a18_a_ASDATA_driver,
	sclr => O_a18_a_SCLR_driver,
	sload => O_a18_a_SLOAD_driver,
	ena => O_a18_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(18));

O_a17_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a17_a_CLK_driver);

O_a17_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a17_a_a16_combout,
	dataout => O_a17_a_D_driver);

O_a17_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(18),
	dataout => O_a17_a_ASDATA_driver);

O_a17_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a17_a_SCLR_driver);

O_a17_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a17_a_SLOAD_driver);

O_a17_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a17_a_ENA_driver);

-- Location: FF_X37_Y25_N29
O_a17_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a17_a_CLK_driver,
	d => O_a17_a_D_driver,
	asdata => O_a17_a_ASDATA_driver,
	sclr => O_a17_a_SCLR_driver,
	sload => O_a17_a_SLOAD_driver,
	ena => O_a17_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(17));

O_a16_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a16_a_CLK_driver);

O_a16_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a16_a_a15_combout,
	dataout => O_a16_a_D_driver);

O_a16_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(17),
	dataout => O_a16_a_ASDATA_driver);

O_a16_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a16_a_SCLR_driver);

O_a16_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a16_a_SLOAD_driver);

O_a16_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a16_a_ENA_driver);

-- Location: FF_X36_Y27_N17
O_a16_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a16_a_CLK_driver,
	d => O_a16_a_D_driver,
	asdata => O_a16_a_ASDATA_driver,
	sclr => O_a16_a_SCLR_driver,
	sload => O_a16_a_SLOAD_driver,
	ena => O_a16_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(16));

O_a15_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a15_a_CLK_driver);

O_a15_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a15_a_a14_combout,
	dataout => O_a15_a_D_driver);

O_a15_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(16),
	dataout => O_a15_a_ASDATA_driver);

O_a15_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a15_a_SCLR_driver);

O_a15_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a15_a_SLOAD_driver);

O_a15_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a15_a_ENA_driver);

-- Location: FF_X36_Y30_N3
O_a15_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a15_a_CLK_driver,
	d => O_a15_a_D_driver,
	asdata => O_a15_a_ASDATA_driver,
	sclr => O_a15_a_SCLR_driver,
	sload => O_a15_a_SLOAD_driver,
	ena => O_a15_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(15));

O_a14_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a14_a_CLK_driver);

O_a14_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a14_a_a13_combout,
	dataout => O_a14_a_D_driver);

O_a14_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(15),
	dataout => O_a14_a_ASDATA_driver);

O_a14_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a14_a_SCLR_driver);

O_a14_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a14_a_SLOAD_driver);

O_a14_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a14_a_ENA_driver);

-- Location: FF_X36_Y30_N1
O_a14_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a14_a_CLK_driver,
	d => O_a14_a_D_driver,
	asdata => O_a14_a_ASDATA_driver,
	sclr => O_a14_a_SCLR_driver,
	sload => O_a14_a_SLOAD_driver,
	ena => O_a14_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(14));

O_a13_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a13_a_CLK_driver);

O_a13_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a13_a_a12_combout,
	dataout => O_a13_a_D_driver);

O_a13_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(14),
	dataout => O_a13_a_ASDATA_driver);

O_a13_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a13_a_SCLR_driver);

O_a13_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a13_a_SLOAD_driver);

O_a13_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a13_a_ENA_driver);

-- Location: FF_X36_Y30_N31
O_a13_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a13_a_CLK_driver,
	d => O_a13_a_D_driver,
	asdata => O_a13_a_ASDATA_driver,
	sclr => O_a13_a_SCLR_driver,
	sload => O_a13_a_SLOAD_driver,
	ena => O_a13_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(13));

O_a12_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a12_a_CLK_driver);

O_a12_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a12_a_a11_combout,
	dataout => O_a12_a_D_driver);

O_a12_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(13),
	dataout => O_a12_a_ASDATA_driver);

O_a12_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a12_a_SCLR_driver);

O_a12_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a12_a_SLOAD_driver);

O_a12_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a12_a_ENA_driver);

-- Location: FF_X36_Y30_N5
O_a12_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a12_a_CLK_driver,
	d => O_a12_a_D_driver,
	asdata => O_a12_a_ASDATA_driver,
	sclr => O_a12_a_SCLR_driver,
	sload => O_a12_a_SLOAD_driver,
	ena => O_a12_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(12));

O_a11_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a11_a_CLK_driver);

O_a11_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a11_a_a10_combout,
	dataout => O_a11_a_D_driver);

O_a11_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(12),
	dataout => O_a11_a_ASDATA_driver);

O_a11_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a11_a_SCLR_driver);

O_a11_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a11_a_SLOAD_driver);

O_a11_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a11_a_ENA_driver);

-- Location: FF_X36_Y30_N9
O_a11_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a11_a_CLK_driver,
	d => O_a11_a_D_driver,
	asdata => O_a11_a_ASDATA_driver,
	sclr => O_a11_a_SCLR_driver,
	sload => O_a11_a_SLOAD_driver,
	ena => O_a11_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(11));

O_a10_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a10_a_CLK_driver);

O_a10_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a10_a_a9_combout,
	dataout => O_a10_a_D_driver);

O_a10_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(11),
	dataout => O_a10_a_ASDATA_driver);

O_a10_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a10_a_SCLR_driver);

O_a10_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a10_a_SLOAD_driver);

O_a10_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a10_a_ENA_driver);

-- Location: FF_X36_Y30_N15
O_a10_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a10_a_CLK_driver,
	d => O_a10_a_D_driver,
	asdata => O_a10_a_ASDATA_driver,
	sclr => O_a10_a_SCLR_driver,
	sload => O_a10_a_SLOAD_driver,
	ena => O_a10_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(10));

O_a9_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a9_a_CLK_driver);

O_a9_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a9_a_a8_combout,
	dataout => O_a9_a_D_driver);

O_a9_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(10),
	dataout => O_a9_a_ASDATA_driver);

O_a9_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a9_a_SCLR_driver);

O_a9_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a9_a_SLOAD_driver);

O_a9_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a9_a_ENA_driver);

-- Location: FF_X36_Y30_N27
O_a9_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a9_a_CLK_driver,
	d => O_a9_a_D_driver,
	asdata => O_a9_a_ASDATA_driver,
	sclr => O_a9_a_SCLR_driver,
	sload => O_a9_a_SLOAD_driver,
	ena => O_a9_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(9));

O_a8_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a8_a_CLK_driver);

O_a8_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a8_a_a7_combout,
	dataout => O_a8_a_D_driver);

O_a8_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(9),
	dataout => O_a8_a_ASDATA_driver);

O_a8_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a8_a_SCLR_driver);

O_a8_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a8_a_SLOAD_driver);

O_a8_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a8_a_ENA_driver);

-- Location: FF_X36_Y30_N23
O_a8_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a8_a_CLK_driver,
	d => O_a8_a_D_driver,
	asdata => O_a8_a_ASDATA_driver,
	sclr => O_a8_a_SCLR_driver,
	sload => O_a8_a_SLOAD_driver,
	ena => O_a8_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(8));

O_a7_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a7_a_CLK_driver);

O_a7_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a7_a_a6_combout,
	dataout => O_a7_a_D_driver);

O_a7_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(8),
	dataout => O_a7_a_ASDATA_driver);

O_a7_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a7_a_SCLR_driver);

O_a7_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a7_a_SLOAD_driver);

O_a7_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a7_a_ENA_driver);

-- Location: FF_X36_Y30_N29
O_a7_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a7_a_CLK_driver,
	d => O_a7_a_D_driver,
	asdata => O_a7_a_ASDATA_driver,
	sclr => O_a7_a_SCLR_driver,
	sload => O_a7_a_SLOAD_driver,
	ena => O_a7_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(7));

O_a6_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a6_a_CLK_driver);

O_a6_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a6_a_a5_combout,
	dataout => O_a6_a_D_driver);

O_a6_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(7),
	dataout => O_a6_a_ASDATA_driver);

O_a6_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a6_a_SCLR_driver);

O_a6_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a6_a_SLOAD_driver);

O_a6_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a6_a_ENA_driver);

-- Location: FF_X36_Y29_N31
O_a6_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a6_a_CLK_driver,
	d => O_a6_a_D_driver,
	asdata => O_a6_a_ASDATA_driver,
	sclr => O_a6_a_SCLR_driver,
	sload => O_a6_a_SLOAD_driver,
	ena => O_a6_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(6));

O_a5_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a5_a_CLK_driver);

O_a5_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a5_a_a4_combout,
	dataout => O_a5_a_D_driver);

O_a5_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(6),
	dataout => O_a5_a_ASDATA_driver);

O_a5_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a5_a_SCLR_driver);

O_a5_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a5_a_SLOAD_driver);

O_a5_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a5_a_ENA_driver);

-- Location: FF_X36_Y29_N27
O_a5_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a5_a_CLK_driver,
	d => O_a5_a_D_driver,
	asdata => O_a5_a_ASDATA_driver,
	sclr => O_a5_a_SCLR_driver,
	sload => O_a5_a_SLOAD_driver,
	ena => O_a5_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(5));

O_a4_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a4_a_CLK_driver);

O_a4_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a4_a_a3_combout,
	dataout => O_a4_a_D_driver);

O_a4_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(5),
	dataout => O_a4_a_ASDATA_driver);

O_a4_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a4_a_SCLR_driver);

O_a4_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a4_a_SLOAD_driver);

O_a4_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a4_a_ENA_driver);

-- Location: FF_X36_Y29_N9
O_a4_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a4_a_CLK_driver,
	d => O_a4_a_D_driver,
	asdata => O_a4_a_ASDATA_driver,
	sclr => O_a4_a_SCLR_driver,
	sload => O_a4_a_SLOAD_driver,
	ena => O_a4_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(4));

O_a3_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a3_a_CLK_driver);

O_a3_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a3_a_a2_combout,
	dataout => O_a3_a_D_driver);

O_a3_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(4),
	dataout => O_a3_a_ASDATA_driver);

O_a3_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a3_a_SCLR_driver);

O_a3_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a3_a_SLOAD_driver);

O_a3_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a3_a_ENA_driver);

-- Location: FF_X36_Y29_N23
O_a3_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a3_a_CLK_driver,
	d => O_a3_a_D_driver,
	asdata => O_a3_a_ASDATA_driver,
	sclr => O_a3_a_SCLR_driver,
	sload => O_a3_a_SLOAD_driver,
	ena => O_a3_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(3));

O_a2_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a2_a_CLK_driver);

O_a2_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a2_a_a1_combout,
	dataout => O_a2_a_D_driver);

O_a2_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(3),
	dataout => O_a2_a_ASDATA_driver);

O_a2_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a2_a_SCLR_driver);

O_a2_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a2_a_SLOAD_driver);

O_a2_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a2_a_ENA_driver);

-- Location: FF_X36_Y29_N5
O_a2_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a2_a_CLK_driver,
	d => O_a2_a_D_driver,
	asdata => O_a2_a_ASDATA_driver,
	sclr => O_a2_a_SCLR_driver,
	sload => O_a2_a_SLOAD_driver,
	ena => O_a2_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(2));

data_out_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a7_DATAA_driver);

data_out_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(2),
	dataout => data_out_a7_DATAB_driver);

data_out_a7_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(2),
	dataout => data_out_a7_DATAC_driver);

data_out_a7_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a7_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N20
data_out_a7 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a7_combout = (mux_data_out_a0_a_ainput_o & ((A(2)) # ((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & (((O(2) & !mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a7_DATAA_driver,
	datab => data_out_a7_DATAB_driver,
	datac => data_out_a7_DATAC_driver,
	datad => data_out_a7_DATAD_driver,
	combout => data_out_a7_combout);

data_out_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a8_DATAA_driver);

data_out_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(2),
	dataout => data_out_a8_DATAB_driver);

data_out_a8_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(2),
	dataout => data_out_a8_DATAC_driver);

data_out_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a7_combout,
	dataout => data_out_a8_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N18
data_out_a8 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a8_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a7_combout & ((LO(2)))) # (!data_out_a7_combout & (HI(2))))) # (!mux_data_out_a1_a_ainput_o & (((data_out_a7_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a8_DATAA_driver,
	datab => data_out_a8_DATAB_driver,
	datac => data_out_a8_DATAC_driver,
	datad => data_out_a8_DATAD_driver,
	combout => data_out_a8_combout);

data_out_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(2),
	dataout => data_out_a9_DATAA_driver);

data_out_a9_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a9_DATAB_driver);

data_out_a9_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a9_DATAC_driver);

data_out_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a8_combout,
	dataout => data_out_a9_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N0
data_out_a9 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a9_combout = (PC(2) & ((data_out_a0_combout) # ((!mux_data_out_a2_a_ainput_o & data_out_a8_combout)))) # (!PC(2) & (!mux_data_out_a2_a_ainput_o & ((data_out_a8_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a9_DATAA_driver,
	datab => data_out_a9_DATAB_driver,
	datac => data_out_a9_DATAC_driver,
	datad => data_out_a9_DATAD_driver,
	combout => data_out_a9_combout);

PC_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a1_a_ainput_o,
	dataout => PC_a4_DATAA_driver);

PC_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a4_DATAB_driver);

PC_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a8_combout,
	dataout => PC_a4_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N30
PC_a4 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a4_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a1_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a8_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a4_DATAA_driver,
	datab => PC_a4_DATAB_driver,
	datad => PC_a4_DATAD_driver,
	combout => PC_a4_combout);

PC_a3_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a3_a_CLK_driver);

PC_a3_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a4_combout,
	dataout => PC_a3_a_D_driver);

PC_a3_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a3_a_ainput_o,
	dataout => PC_a3_a_ASDATA_driver);

PC_a3_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a3_a_SCLR_driver);

PC_a3_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a3_a_SLOAD_driver);

PC_a3_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a3_a_ENA_driver);

-- Location: FF_X38_Y30_N31
PC_a3_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a3_a_CLK_driver,
	d => PC_a3_a_D_driver,
	asdata => PC_a3_a_ASDATA_driver,
	sclr => PC_a3_a_SCLR_driver,
	sload => PC_a3_a_SLOAD_driver,
	ena => PC_a3_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(3));

A_a3_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a3_a_CLK_driver);

A_a3_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a4_combout,
	dataout => A_a3_a_ASDATA_driver);

A_a3_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a3_a_ENA_driver);

-- Location: FF_X38_Y29_N1
A_a3_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a3_a_CLK_driver,
	asdata => A_a3_a_ASDATA_driver,
	sload => VCC,
	ena => A_a3_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(3));

LO_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(2),
	dataout => LO_a4_DATAB_driver);

LO_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a4_DATAC_driver);

LO_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a3_a_ainput_o,
	dataout => LO_a4_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N30
LO_a4 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a4_combout = (mux_lo_a1_a_ainput_o & ((data_in_a3_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a4_DATAB_driver,
	datac => LO_a4_DATAC_driver,
	datad => LO_a4_DATAD_driver,
	combout => LO_a4_combout);

LO_a3_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a3_a_CLK_driver);

LO_a3_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a4_combout,
	dataout => LO_a3_a_D_driver);

LO_a3_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a3_a_SCLR_driver);

LO_a3_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a3_a_ENA_driver);

-- Location: FF_X41_Y27_N31
LO_a3_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a3_a_CLK_driver,
	d => LO_a3_a_D_driver,
	sclr => LO_a3_a_SCLR_driver,
	ena => LO_a3_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(3));

data_out_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a10_combout,
	dataout => data_out_a11_DATAA_driver);

data_out_a11_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a11_DATAB_driver);

data_out_a11_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(3),
	dataout => data_out_a11_DATAC_driver);

data_out_a11_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(3),
	dataout => data_out_a11_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N0
data_out_a11 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a11_combout = (data_out_a10_combout & (((LO(3))) # (!mux_data_out_a0_a_ainput_o))) # (!data_out_a10_combout & (mux_data_out_a0_a_ainput_o & (A(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a11_DATAA_driver,
	datab => data_out_a11_DATAB_driver,
	datac => data_out_a11_DATAC_driver,
	datad => data_out_a11_DATAD_driver,
	combout => data_out_a11_combout);

data_out_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a12_DATAA_driver);

data_out_a12_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(3),
	dataout => data_out_a12_DATAB_driver);

data_out_a12_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a11_combout,
	dataout => data_out_a12_DATAC_driver);

data_out_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a12_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N22
data_out_a12 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a12_combout = (mux_data_out_a2_a_ainput_o & (PC(3) & ((data_out_a0_combout)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a11_combout) # ((PC(3) & data_out_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a12_DATAA_driver,
	datab => data_out_a12_DATAB_driver,
	datac => data_out_a12_DATAC_driver,
	datad => data_out_a12_DATAD_driver,
	combout => data_out_a12_combout);

HI_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(3),
	dataout => HI_a6_DATAA_driver);

HI_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a6_DATAB_driver);

HI_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a10_combout,
	dataout => HI_a6_DATAC_driver);

HI_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a6_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N28
HI_a6 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a6_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a10_combout))) # (!Add0_a66_combout & (HI(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a6_DATAA_driver,
	datab => HI_a6_DATAB_driver,
	datac => HI_a6_DATAC_driver,
	datad => HI_a6_DATAD_driver,
	combout => HI_a6_combout);

HI_a4_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a4_a_CLK_driver);

HI_a4_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a6_combout,
	dataout => HI_a4_a_D_driver);

HI_a4_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a4_a_ENA_driver);

-- Location: FF_X37_Y30_N29
HI_a4_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a4_a_CLK_driver,
	d => HI_a4_a_D_driver,
	ena => HI_a4_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(4));

LO_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(3),
	dataout => LO_a5_DATAA_driver);

LO_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a5_DATAC_driver);

LO_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a4_a_ainput_o,
	dataout => LO_a5_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N16
LO_a5 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a5_combout = (mux_lo_a1_a_ainput_o & ((data_in_a4_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a5_DATAA_driver,
	datac => LO_a5_DATAC_driver,
	datad => LO_a5_DATAD_driver,
	combout => LO_a5_combout);

LO_a4_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a4_a_CLK_driver);

LO_a4_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a5_combout,
	dataout => LO_a4_a_D_driver);

LO_a4_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a4_a_SCLR_driver);

LO_a4_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a4_a_ENA_driver);

-- Location: FF_X41_Y27_N17
LO_a4_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a4_a_CLK_driver,
	d => LO_a4_a_D_driver,
	sclr => LO_a4_a_SCLR_driver,
	ena => LO_a4_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(4));

data_out_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a13_combout,
	dataout => data_out_a14_DATAA_driver);

data_out_a14_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a14_DATAB_driver);

data_out_a14_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(4),
	dataout => data_out_a14_DATAC_driver);

data_out_a14_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(4),
	dataout => data_out_a14_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N4
data_out_a14 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a14_combout = (data_out_a13_combout & (((LO(4))) # (!mux_data_out_a1_a_ainput_o))) # (!data_out_a13_combout & (mux_data_out_a1_a_ainput_o & (HI(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a14_DATAA_driver,
	datab => data_out_a14_DATAB_driver,
	datac => data_out_a14_DATAC_driver,
	datad => data_out_a14_DATAD_driver,
	combout => data_out_a14_combout);

PC_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a2_a_ainput_o,
	dataout => PC_a5_DATAA_driver);

PC_a5_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a5_DATAB_driver);

PC_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a10_combout,
	dataout => PC_a5_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N30
PC_a5 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a5_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a2_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a10_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a5_DATAA_driver,
	datab => PC_a5_DATAB_driver,
	datad => PC_a5_DATAD_driver,
	combout => PC_a5_combout);

PC_a4_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a4_a_CLK_driver);

PC_a4_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a5_combout,
	dataout => PC_a4_a_D_driver);

PC_a4_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a4_a_ainput_o,
	dataout => PC_a4_a_ASDATA_driver);

PC_a4_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a4_a_SCLR_driver);

PC_a4_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a4_a_SLOAD_driver);

PC_a4_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a4_a_ENA_driver);

-- Location: FF_X39_Y29_N31
PC_a4_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a4_a_CLK_driver,
	d => PC_a4_a_D_driver,
	asdata => PC_a4_a_ASDATA_driver,
	sclr => PC_a4_a_SCLR_driver,
	sload => PC_a4_a_SLOAD_driver,
	ena => PC_a4_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(4));

data_out_a15_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a15_DATAA_driver);

data_out_a15_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a14_combout,
	dataout => data_out_a15_DATAB_driver);

data_out_a15_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(4),
	dataout => data_out_a15_DATAC_driver);

data_out_a15_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a15_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N14
data_out_a15 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a15_combout = (data_out_a0_combout & ((PC(4)) # ((data_out_a14_combout & !mux_data_out_a2_a_ainput_o)))) # (!data_out_a0_combout & (data_out_a14_combout & ((!mux_data_out_a2_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a15_DATAA_driver,
	datab => data_out_a15_DATAB_driver,
	datac => data_out_a15_DATAC_driver,
	datad => data_out_a15_DATAD_driver,
	combout => data_out_a15_combout);

HI_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a7_DATAA_driver);

HI_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(4),
	dataout => HI_a7_DATAB_driver);

HI_a7_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a12_combout,
	dataout => HI_a7_DATAC_driver);

HI_a7_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a7_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N16
HI_a7 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a7_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a12_combout))) # (!Add0_a66_combout & (HI(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a7_DATAA_driver,
	datab => HI_a7_DATAB_driver,
	datac => HI_a7_DATAC_driver,
	datad => HI_a7_DATAD_driver,
	combout => HI_a7_combout);

HI_a5_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a5_a_CLK_driver);

HI_a5_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a7_combout,
	dataout => HI_a5_a_D_driver);

HI_a5_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a5_a_ENA_driver);

-- Location: FF_X37_Y30_N17
HI_a5_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a5_a_CLK_driver,
	d => HI_a5_a_D_driver,
	ena => HI_a5_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(5));

data_out_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a16_DATAA_driver);

data_out_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(5),
	dataout => data_out_a16_DATAB_driver);

data_out_a16_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a16_DATAC_driver);

data_out_a16_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(5),
	dataout => data_out_a16_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N18
data_out_a16 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a16_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o & (HI(5))) # (!mux_data_out_a1_a_ainput_o & ((O(5))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a16_DATAA_driver,
	datab => data_out_a16_DATAB_driver,
	datac => data_out_a16_DATAC_driver,
	datad => data_out_a16_DATAD_driver,
	combout => data_out_a16_combout);

A_a5_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a5_a_CLK_driver);

A_a5_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a6_combout,
	dataout => A_a5_a_ASDATA_driver);

A_a5_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a5_a_ENA_driver);

-- Location: FF_X37_Y30_N27
A_a5_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a5_a_CLK_driver,
	asdata => A_a5_a_ASDATA_driver,
	sload => VCC,
	ena => A_a5_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(5));

LO_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a5_a_ainput_o,
	dataout => LO_a6_DATAA_driver);

LO_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(4),
	dataout => LO_a6_DATAB_driver);

LO_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a6_DATAC_driver);

-- Location: LCCOMB_X41_Y27_N26
LO_a6 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a6_combout = (mux_lo_a1_a_ainput_o & (data_in_a5_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a6_DATAA_driver,
	datab => LO_a6_DATAB_driver,
	datac => LO_a6_DATAC_driver,
	combout => LO_a6_combout);

LO_a5_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a5_a_CLK_driver);

LO_a5_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a6_combout,
	dataout => LO_a5_a_D_driver);

LO_a5_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a5_a_SCLR_driver);

LO_a5_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a5_a_ENA_driver);

-- Location: FF_X41_Y27_N27
LO_a5_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a5_a_CLK_driver,
	d => LO_a5_a_D_driver,
	sclr => LO_a5_a_SCLR_driver,
	ena => LO_a5_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(5));

data_out_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a17_DATAA_driver);

data_out_a17_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a16_combout,
	dataout => data_out_a17_DATAB_driver);

data_out_a17_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(5),
	dataout => data_out_a17_DATAC_driver);

data_out_a17_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(5),
	dataout => data_out_a17_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N26
data_out_a17 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a17_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a16_combout & ((LO(5)))) # (!data_out_a16_combout & (A(5))))) # (!mux_data_out_a0_a_ainput_o & (data_out_a16_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a17_DATAA_driver,
	datab => data_out_a17_DATAB_driver,
	datac => data_out_a17_DATAC_driver,
	datad => data_out_a17_DATAD_driver,
	combout => data_out_a17_combout);

PC_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a3_a_ainput_o,
	dataout => PC_a6_DATAA_driver);

PC_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a6_DATAB_driver);

PC_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a12_combout,
	dataout => PC_a6_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N4
PC_a6 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a6_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a3_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a12_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a6_DATAA_driver,
	datab => PC_a6_DATAB_driver,
	datad => PC_a6_DATAD_driver,
	combout => PC_a6_combout);

PC_a5_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a5_a_CLK_driver);

PC_a5_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a6_combout,
	dataout => PC_a5_a_D_driver);

PC_a5_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a5_a_ainput_o,
	dataout => PC_a5_a_ASDATA_driver);

PC_a5_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a5_a_SCLR_driver);

PC_a5_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a5_a_SLOAD_driver);

PC_a5_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a5_a_ENA_driver);

-- Location: FF_X38_Y30_N5
PC_a5_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a5_a_CLK_driver,
	d => PC_a5_a_D_driver,
	asdata => PC_a5_a_ASDATA_driver,
	sclr => PC_a5_a_SCLR_driver,
	sload => PC_a5_a_SLOAD_driver,
	ena => PC_a5_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(5));

data_out_a18_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a18_DATAA_driver);

data_out_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a17_combout,
	dataout => data_out_a18_DATAB_driver);

data_out_a18_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(5),
	dataout => data_out_a18_DATAC_driver);

data_out_a18_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a18_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N28
data_out_a18 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a18_combout = (mux_data_out_a2_a_ainput_o & (((PC(5) & data_out_a0_combout)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a17_combout) # ((PC(5) & data_out_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a18_DATAA_driver,
	datab => data_out_a18_DATAB_driver,
	datac => data_out_a18_DATAC_driver,
	datad => data_out_a18_DATAD_driver,
	combout => data_out_a18_combout);

PC_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a4_a_ainput_o,
	dataout => PC_a7_DATAA_driver);

PC_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a7_DATAB_driver);

PC_a7_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a14_combout,
	dataout => PC_a7_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N22
PC_a7 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a7_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a4_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a14_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a7_DATAA_driver,
	datab => PC_a7_DATAB_driver,
	datad => PC_a7_DATAD_driver,
	combout => PC_a7_combout);

PC_a6_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a6_a_CLK_driver);

PC_a6_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a7_combout,
	dataout => PC_a6_a_D_driver);

PC_a6_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a6_a_ainput_o,
	dataout => PC_a6_a_ASDATA_driver);

PC_a6_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a6_a_SCLR_driver);

PC_a6_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a6_a_SLOAD_driver);

PC_a6_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a6_a_ENA_driver);

-- Location: FF_X38_Y30_N23
PC_a6_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a6_a_CLK_driver,
	d => PC_a6_a_D_driver,
	asdata => PC_a6_a_ASDATA_driver,
	sclr => PC_a6_a_SCLR_driver,
	sload => PC_a6_a_SLOAD_driver,
	ena => PC_a6_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(6));

HI_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a8_DATAA_driver);

HI_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(5),
	dataout => HI_a8_DATAB_driver);

HI_a8_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a14_combout,
	dataout => HI_a8_DATAC_driver);

HI_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a8_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N0
HI_a8 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a8_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a14_combout))) # (!Add0_a66_combout & (HI(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a8_DATAA_driver,
	datab => HI_a8_DATAB_driver,
	datac => HI_a8_DATAC_driver,
	datad => HI_a8_DATAD_driver,
	combout => HI_a8_combout);

HI_a6_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a6_a_CLK_driver);

HI_a6_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a8_combout,
	dataout => HI_a6_a_D_driver);

HI_a6_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a6_a_ENA_driver);

-- Location: FF_X37_Y30_N1
HI_a6_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a6_a_CLK_driver,
	d => HI_a6_a_D_driver,
	ena => HI_a6_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(6));

A_a6_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a6_a_CLK_driver);

A_a6_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a7_combout,
	dataout => A_a6_a_ASDATA_driver);

A_a6_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a6_a_ENA_driver);

-- Location: FF_X37_Y30_N11
A_a6_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a6_a_CLK_driver,
	asdata => A_a6_a_ASDATA_driver,
	sload => VCC,
	ena => A_a6_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(6));

data_out_a19_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a19_DATAA_driver);

data_out_a19_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(6),
	dataout => data_out_a19_DATAB_driver);

data_out_a19_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(6),
	dataout => data_out_a19_DATAC_driver);

data_out_a19_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a19_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N10
data_out_a19 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a19_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o & ((A(6)))) # (!mux_data_out_a0_a_ainput_o & (O(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a19_DATAA_driver,
	datab => data_out_a19_DATAB_driver,
	datac => data_out_a19_DATAC_driver,
	datad => data_out_a19_DATAD_driver,
	combout => data_out_a19_combout);

data_out_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(6),
	dataout => data_out_a20_DATAA_driver);

data_out_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(6),
	dataout => data_out_a20_DATAB_driver);

data_out_a20_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a20_DATAC_driver);

data_out_a20_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a19_combout,
	dataout => data_out_a20_DATAD_driver);

-- Location: LCCOMB_X37_Y30_N24
data_out_a20 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a20_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a19_combout & (LO(6))) # (!data_out_a19_combout & ((HI(6)))))) # (!mux_data_out_a1_a_ainput_o & (((data_out_a19_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a20_DATAA_driver,
	datab => data_out_a20_DATAB_driver,
	datac => data_out_a20_DATAC_driver,
	datad => data_out_a20_DATAD_driver,
	combout => data_out_a20_combout);

data_out_a21_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a21_DATAA_driver);

data_out_a21_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(6),
	dataout => data_out_a21_DATAB_driver);

data_out_a21_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a20_combout,
	dataout => data_out_a21_DATAC_driver);

data_out_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a21_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N18
data_out_a21 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a21_combout = (data_out_a0_combout & ((PC(6)) # ((data_out_a20_combout & !mux_data_out_a2_a_ainput_o)))) # (!data_out_a0_combout & (((data_out_a20_combout & !mux_data_out_a2_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a21_DATAA_driver,
	datab => data_out_a21_DATAB_driver,
	datac => data_out_a21_DATAC_driver,
	datad => data_out_a21_DATAD_driver,
	combout => data_out_a21_combout);

data_out_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a22_DATAA_driver);

data_out_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(7),
	dataout => data_out_a22_DATAB_driver);

data_out_a22_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(7),
	dataout => data_out_a22_DATAC_driver);

data_out_a22_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a22_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N10
data_out_a22 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a22_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o & ((HI(7)))) # (!mux_data_out_a1_a_ainput_o & (O(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a22_DATAA_driver,
	datab => data_out_a22_DATAB_driver,
	datac => data_out_a22_DATAC_driver,
	datad => data_out_a22_DATAD_driver,
	combout => data_out_a22_combout);

LO_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(5),
	dataout => LO_a7_DATAA_driver);

LO_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a6_a_ainput_o,
	dataout => LO_a7_DATAB_driver);

LO_a7_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a7_DATAC_driver);

-- Location: LCCOMB_X41_Y27_N24
LO_a7 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a7_combout = (mux_lo_a1_a_ainput_o & ((data_in_a6_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a7_DATAA_driver,
	datab => LO_a7_DATAB_driver,
	datac => LO_a7_DATAC_driver,
	combout => LO_a7_combout);

LO_a6_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a6_a_CLK_driver);

LO_a6_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a7_combout,
	dataout => LO_a6_a_D_driver);

LO_a6_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a6_a_SCLR_driver);

LO_a6_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a6_a_ENA_driver);

-- Location: FF_X41_Y27_N25
LO_a6_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a6_a_CLK_driver,
	d => LO_a6_a_D_driver,
	sclr => LO_a6_a_SCLR_driver,
	ena => LO_a6_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(6));

LO_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(6),
	dataout => LO_a8_DATAB_driver);

LO_a8_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a8_DATAC_driver);

LO_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a7_a_ainput_o,
	dataout => LO_a8_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N22
LO_a8 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a8_combout = (mux_lo_a1_a_ainput_o & ((data_in_a7_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a8_DATAB_driver,
	datac => LO_a8_DATAC_driver,
	datad => LO_a8_DATAD_driver,
	combout => LO_a8_combout);

LO_a7_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a7_a_CLK_driver);

LO_a7_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a8_combout,
	dataout => LO_a7_a_D_driver);

LO_a7_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a7_a_SCLR_driver);

LO_a7_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a7_a_ENA_driver);

-- Location: FF_X41_Y27_N23
LO_a7_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a7_a_CLK_driver,
	d => LO_a7_a_D_driver,
	sclr => LO_a7_a_SCLR_driver,
	ena => LO_a7_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(7));

data_out_a23_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(7),
	dataout => data_out_a23_DATAA_driver);

data_out_a23_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a23_DATAB_driver);

data_out_a23_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a22_combout,
	dataout => data_out_a23_DATAC_driver);

data_out_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(7),
	dataout => data_out_a23_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N10
data_out_a23 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a23_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a22_combout & ((LO(7)))) # (!data_out_a22_combout & (A(7))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a22_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a23_DATAA_driver,
	datab => data_out_a23_DATAB_driver,
	datac => data_out_a23_DATAC_driver,
	datad => data_out_a23_DATAD_driver,
	combout => data_out_a23_combout);

PC_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a5_a_ainput_o,
	dataout => PC_a8_DATAA_driver);

PC_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a8_DATAB_driver);

PC_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a16_combout,
	dataout => PC_a8_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N0
PC_a8 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a8_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a5_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a16_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a8_DATAA_driver,
	datab => PC_a8_DATAB_driver,
	datad => PC_a8_DATAD_driver,
	combout => PC_a8_combout);

PC_a7_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a7_a_CLK_driver);

PC_a7_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a8_combout,
	dataout => PC_a7_a_D_driver);

PC_a7_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a7_a_ainput_o,
	dataout => PC_a7_a_ASDATA_driver);

PC_a7_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a7_a_SCLR_driver);

PC_a7_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a7_a_SLOAD_driver);

PC_a7_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a7_a_ENA_driver);

-- Location: FF_X38_Y30_N1
PC_a7_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a7_a_CLK_driver,
	d => PC_a7_a_D_driver,
	asdata => PC_a7_a_ASDATA_driver,
	sclr => PC_a7_a_SCLR_driver,
	sload => PC_a7_a_SLOAD_driver,
	ena => PC_a7_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(7));

data_out_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a24_DATAA_driver);

data_out_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a23_combout,
	dataout => data_out_a24_DATAB_driver);

data_out_a24_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(7),
	dataout => data_out_a24_DATAC_driver);

data_out_a24_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a24_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N0
data_out_a24 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a24_combout = (data_out_a0_combout & ((PC(7)) # ((data_out_a23_combout & !mux_data_out_a2_a_ainput_o)))) # (!data_out_a0_combout & (data_out_a23_combout & ((!mux_data_out_a2_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a24_DATAA_driver,
	datab => data_out_a24_DATAB_driver,
	datac => data_out_a24_DATAC_driver,
	datad => data_out_a24_DATAD_driver,
	combout => data_out_a24_combout);

PC_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a6_a_ainput_o,
	dataout => PC_a9_DATAA_driver);

PC_a9_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a9_DATAB_driver);

PC_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a18_combout,
	dataout => PC_a9_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N14
PC_a9 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a9_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a6_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a18_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a9_DATAA_driver,
	datab => PC_a9_DATAB_driver,
	datad => PC_a9_DATAD_driver,
	combout => PC_a9_combout);

PC_a8_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a8_a_CLK_driver);

PC_a8_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a9_combout,
	dataout => PC_a8_a_D_driver);

PC_a8_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a8_a_ainput_o,
	dataout => PC_a8_a_ASDATA_driver);

PC_a8_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a8_a_SCLR_driver);

PC_a8_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a8_a_SLOAD_driver);

PC_a8_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a8_a_ENA_driver);

-- Location: FF_X38_Y30_N15
PC_a8_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a8_a_CLK_driver,
	d => PC_a8_a_D_driver,
	asdata => PC_a8_a_ASDATA_driver,
	sclr => PC_a8_a_SCLR_driver,
	sload => PC_a8_a_SLOAD_driver,
	ena => PC_a8_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(8));

LO_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a9_DATAA_driver);

LO_a9_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a8_a_ainput_o,
	dataout => LO_a9_DATAC_driver);

LO_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(7),
	dataout => LO_a9_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N4
LO_a9 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a9_combout = (mux_lo_a1_a_ainput_o & (data_in_a8_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a9_DATAA_driver,
	datac => LO_a9_DATAC_driver,
	datad => LO_a9_DATAD_driver,
	combout => LO_a9_combout);

LO_a8_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a8_a_CLK_driver);

LO_a8_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a9_combout,
	dataout => LO_a8_a_D_driver);

LO_a8_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a8_a_SCLR_driver);

LO_a8_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a8_a_ENA_driver);

-- Location: FF_X41_Y27_N5
LO_a8_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a8_a_CLK_driver,
	d => LO_a8_a_D_driver,
	sclr => LO_a8_a_SCLR_driver,
	ena => LO_a8_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(8));

data_out_a26_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a25_combout,
	dataout => data_out_a26_DATAA_driver);

data_out_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a26_DATAB_driver);

data_out_a26_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(8),
	dataout => data_out_a26_DATAC_driver);

data_out_a26_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(8),
	dataout => data_out_a26_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N28
data_out_a26 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a26_combout = (data_out_a25_combout & (((LO(8))) # (!mux_data_out_a1_a_ainput_o))) # (!data_out_a25_combout & (mux_data_out_a1_a_ainput_o & (HI(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a26_DATAA_driver,
	datab => data_out_a26_DATAB_driver,
	datac => data_out_a26_DATAC_driver,
	datad => data_out_a26_DATAD_driver,
	combout => data_out_a26_combout);

data_out_a27_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a27_DATAA_driver);

data_out_a27_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(8),
	dataout => data_out_a27_DATAB_driver);

data_out_a27_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a27_DATAC_driver);

data_out_a27_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a26_combout,
	dataout => data_out_a27_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N26
data_out_a27 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a27_combout = (mux_data_out_a2_a_ainput_o & (PC(8) & (data_out_a0_combout))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a26_combout) # ((PC(8) & data_out_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a27_DATAA_driver,
	datab => data_out_a27_DATAB_driver,
	datac => data_out_a27_DATAC_driver,
	datad => data_out_a27_DATAD_driver,
	combout => data_out_a27_combout);

data_out_a28_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(9),
	dataout => data_out_a28_DATAA_driver);

data_out_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a28_DATAB_driver);

data_out_a28_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a28_DATAC_driver);

data_out_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(9),
	dataout => data_out_a28_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N20
data_out_a28 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a28_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o & ((HI(9)))) # (!mux_data_out_a1_a_ainput_o & (O(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a28_DATAA_driver,
	datab => data_out_a28_DATAB_driver,
	datac => data_out_a28_DATAC_driver,
	datad => data_out_a28_DATAD_driver,
	combout => data_out_a28_combout);

data_out_a29_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(9),
	dataout => data_out_a29_DATAA_driver);

data_out_a29_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a28_combout,
	dataout => data_out_a29_DATAB_driver);

data_out_a29_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(9),
	dataout => data_out_a29_DATAC_driver);

data_out_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a29_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N10
data_out_a29 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a29_combout = (data_out_a28_combout & ((LO(9)) # ((!mux_data_out_a0_a_ainput_o)))) # (!data_out_a28_combout & (((A(9) & mux_data_out_a0_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a29_DATAA_driver,
	datab => data_out_a29_DATAB_driver,
	datac => data_out_a29_DATAC_driver,
	datad => data_out_a29_DATAD_driver,
	combout => data_out_a29_combout);

data_out_a30_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a29_combout,
	dataout => data_out_a30_DATAA_driver);

data_out_a30_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(9),
	dataout => data_out_a30_DATAB_driver);

data_out_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a30_DATAC_driver);

data_out_a30_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a30_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N28
data_out_a30 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a30_combout = (data_out_a29_combout & (((PC(9) & data_out_a0_combout)) # (!mux_data_out_a2_a_ainput_o))) # (!data_out_a29_combout & (PC(9) & ((data_out_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a30_DATAA_driver,
	datab => data_out_a30_DATAB_driver,
	datac => data_out_a30_DATAC_driver,
	datad => data_out_a30_DATAD_driver,
	combout => data_out_a30_combout);

LO_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(9),
	dataout => LO_a11_DATAA_driver);

LO_a11_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a11_DATAC_driver);

LO_a11_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a10_a_ainput_o,
	dataout => LO_a11_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N20
LO_a11 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a11_combout = (mux_lo_a1_a_ainput_o & ((data_in_a10_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a11_DATAA_driver,
	datac => LO_a11_DATAC_driver,
	datad => LO_a11_DATAD_driver,
	combout => LO_a11_combout);

LO_a10_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a10_a_CLK_driver);

LO_a10_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a11_combout,
	dataout => LO_a10_a_D_driver);

LO_a10_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a10_a_SCLR_driver);

LO_a10_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a10_a_ENA_driver);

-- Location: FF_X41_Y27_N21
LO_a10_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a10_a_CLK_driver,
	d => LO_a10_a_D_driver,
	sclr => LO_a10_a_SCLR_driver,
	ena => LO_a10_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(10));

data_out_a32_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a31_combout,
	dataout => data_out_a32_DATAA_driver);

data_out_a32_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a32_DATAB_driver);

data_out_a32_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(10),
	dataout => data_out_a32_DATAC_driver);

data_out_a32_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(10),
	dataout => data_out_a32_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N24
data_out_a32 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a32_combout = (data_out_a31_combout & (((LO(10))) # (!mux_data_out_a1_a_ainput_o))) # (!data_out_a31_combout & (mux_data_out_a1_a_ainput_o & (HI(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a32_DATAA_driver,
	datab => data_out_a32_DATAB_driver,
	datac => data_out_a32_DATAC_driver,
	datad => data_out_a32_DATAD_driver,
	combout => data_out_a32_combout);

data_out_a33_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a33_DATAA_driver);

data_out_a33_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a33_DATAB_driver);

data_out_a33_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(10),
	dataout => data_out_a33_DATAC_driver);

data_out_a33_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a32_combout,
	dataout => data_out_a33_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N26
data_out_a33 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a33_combout = (data_out_a0_combout & ((PC(10)) # ((!mux_data_out_a2_a_ainput_o & data_out_a32_combout)))) # (!data_out_a0_combout & (!mux_data_out_a2_a_ainput_o & ((data_out_a32_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a33_DATAA_driver,
	datab => data_out_a33_DATAB_driver,
	datac => data_out_a33_DATAC_driver,
	datad => data_out_a33_DATAD_driver,
	combout => data_out_a33_combout);

PC_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a9_a_ainput_o,
	dataout => PC_a12_DATAA_driver);

PC_a12_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a12_DATAB_driver);

PC_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a24_combout,
	dataout => PC_a12_DATAD_driver);

-- Location: LCCOMB_X36_Y26_N10
PC_a12 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a12_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a9_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a24_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a12_DATAA_driver,
	datab => PC_a12_DATAB_driver,
	datad => PC_a12_DATAD_driver,
	combout => PC_a12_combout);

PC_a11_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a11_a_CLK_driver);

PC_a11_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a12_combout,
	dataout => PC_a11_a_D_driver);

PC_a11_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a11_a_ainput_o,
	dataout => PC_a11_a_ASDATA_driver);

PC_a11_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a11_a_SCLR_driver);

PC_a11_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a11_a_SLOAD_driver);

PC_a11_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a11_a_ENA_driver);

-- Location: FF_X36_Y26_N11
PC_a11_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a11_a_CLK_driver,
	d => PC_a11_a_D_driver,
	asdata => PC_a11_a_ASDATA_driver,
	sclr => PC_a11_a_SCLR_driver,
	sload => PC_a11_a_SLOAD_driver,
	ena => PC_a11_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(11));

LO_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a11_a_ainput_o,
	dataout => LO_a12_DATAA_driver);

LO_a12_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a12_DATAC_driver);

LO_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(10),
	dataout => LO_a12_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N18
LO_a12 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a12_combout = (mux_lo_a1_a_ainput_o & (data_in_a11_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a12_DATAA_driver,
	datac => LO_a12_DATAC_driver,
	datad => LO_a12_DATAD_driver,
	combout => LO_a12_combout);

LO_a11_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a11_a_CLK_driver);

LO_a11_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a12_combout,
	dataout => LO_a11_a_D_driver);

LO_a11_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a11_a_SCLR_driver);

LO_a11_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a11_a_ENA_driver);

-- Location: FF_X41_Y27_N19
LO_a11_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a11_a_CLK_driver,
	d => LO_a11_a_D_driver,
	sclr => LO_a11_a_SCLR_driver,
	ena => LO_a11_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(11));

data_out_a34_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(11),
	dataout => data_out_a34_DATAA_driver);

data_out_a34_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(11),
	dataout => data_out_a34_DATAB_driver);

data_out_a34_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a34_DATAC_driver);

data_out_a34_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a34_DATAD_driver);

-- Location: LCCOMB_X36_Y30_N16
data_out_a34 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a34_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o & (HI(11))) # (!mux_data_out_a1_a_ainput_o & ((O(11))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a34_DATAA_driver,
	datab => data_out_a34_DATAB_driver,
	datac => data_out_a34_DATAC_driver,
	datad => data_out_a34_DATAD_driver,
	combout => data_out_a34_combout);

A_a11_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a11_a_CLK_driver);

A_a11_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a12_combout,
	dataout => A_a11_a_ASDATA_driver);

A_a11_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a11_a_ENA_driver);

-- Location: FF_X37_Y26_N3
A_a11_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a11_a_CLK_driver,
	asdata => A_a11_a_ASDATA_driver,
	sload => VCC,
	ena => A_a11_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(11));

data_out_a35_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a35_DATAA_driver);

data_out_a35_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(11),
	dataout => data_out_a35_DATAB_driver);

data_out_a35_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a34_combout,
	dataout => data_out_a35_DATAC_driver);

data_out_a35_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(11),
	dataout => data_out_a35_DATAD_driver);

-- Location: LCCOMB_X39_Y30_N0
data_out_a35 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a35_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a34_combout & (LO(11))) # (!data_out_a34_combout & ((A(11)))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a34_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a35_DATAA_driver,
	datab => data_out_a35_DATAB_driver,
	datac => data_out_a35_DATAC_driver,
	datad => data_out_a35_DATAD_driver,
	combout => data_out_a35_combout);

data_out_a36_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a36_DATAA_driver);

data_out_a36_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a36_DATAB_driver);

data_out_a36_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(11),
	dataout => data_out_a36_DATAC_driver);

data_out_a36_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a35_combout,
	dataout => data_out_a36_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N10
data_out_a36 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a36_combout = (mux_data_out_a2_a_ainput_o & (data_out_a0_combout & (PC(11)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a35_combout) # ((data_out_a0_combout & PC(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a36_DATAA_driver,
	datab => data_out_a36_DATAB_driver,
	datac => data_out_a36_DATAC_driver,
	datad => data_out_a36_DATAD_driver,
	combout => data_out_a36_combout);

data_out_a37_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(12),
	dataout => data_out_a37_DATAA_driver);

data_out_a37_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a37_DATAB_driver);

data_out_a37_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(12),
	dataout => data_out_a37_DATAC_driver);

data_out_a37_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a37_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N18
data_out_a37 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a37_combout = (mux_data_out_a0_a_ainput_o & ((A(12)) # ((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & (((O(12) & !mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a37_DATAA_driver,
	datab => data_out_a37_DATAB_driver,
	datac => data_out_a37_DATAC_driver,
	datad => data_out_a37_DATAD_driver,
	combout => data_out_a37_combout);

LO_a13_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(11),
	dataout => LO_a13_DATAB_driver);

LO_a13_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a13_DATAC_driver);

LO_a13_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a12_a_ainput_o,
	dataout => LO_a13_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N12
LO_a13 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a13_combout = (mux_lo_a1_a_ainput_o & ((data_in_a12_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a13_DATAB_driver,
	datac => LO_a13_DATAC_driver,
	datad => LO_a13_DATAD_driver,
	combout => LO_a13_combout);

LO_a12_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a12_a_CLK_driver);

LO_a12_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a13_combout,
	dataout => LO_a12_a_D_driver);

LO_a12_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a12_a_SCLR_driver);

LO_a12_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a12_a_ENA_driver);

-- Location: FF_X41_Y27_N13
LO_a12_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a12_a_CLK_driver,
	d => LO_a12_a_D_driver,
	sclr => LO_a12_a_SCLR_driver,
	ena => LO_a12_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(12));

data_out_a38_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(12),
	dataout => data_out_a38_DATAA_driver);

data_out_a38_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a37_combout,
	dataout => data_out_a38_DATAB_driver);

data_out_a38_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(12),
	dataout => data_out_a38_DATAC_driver);

data_out_a38_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a38_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N24
data_out_a38 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a38_combout = (data_out_a37_combout & (((LO(12)) # (!mux_data_out_a1_a_ainput_o)))) # (!data_out_a37_combout & (HI(12) & ((mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a38_DATAA_driver,
	datab => data_out_a38_DATAB_driver,
	datac => data_out_a38_DATAC_driver,
	datad => data_out_a38_DATAD_driver,
	combout => data_out_a38_combout);

data_out_a39_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a39_DATAA_driver);

data_out_a39_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a39_DATAB_driver);

data_out_a39_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(12),
	dataout => data_out_a39_DATAC_driver);

data_out_a39_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a38_combout,
	dataout => data_out_a39_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N12
data_out_a39 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a39_combout = (mux_data_out_a2_a_ainput_o & (data_out_a0_combout & (PC(12)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a38_combout) # ((data_out_a0_combout & PC(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a39_DATAA_driver,
	datab => data_out_a39_DATAB_driver,
	datac => data_out_a39_DATAC_driver,
	datad => data_out_a39_DATAD_driver,
	combout => data_out_a39_combout);

LO_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a14_DATAA_driver);

LO_a14_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a13_a_ainput_o,
	dataout => LO_a14_DATAC_driver);

LO_a14_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(12),
	dataout => LO_a14_DATAD_driver);

-- Location: LCCOMB_X41_Y27_N10
LO_a14 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a14_combout = (mux_lo_a1_a_ainput_o & (data_in_a13_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a14_DATAA_driver,
	datac => LO_a14_DATAC_driver,
	datad => LO_a14_DATAD_driver,
	combout => LO_a14_combout);

LO_a13_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a13_a_CLK_driver);

LO_a13_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a14_combout,
	dataout => LO_a13_a_D_driver);

LO_a13_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a13_a_SCLR_driver);

LO_a13_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a13_a_ENA_driver);

-- Location: FF_X41_Y27_N11
LO_a13_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a13_a_CLK_driver,
	d => LO_a13_a_D_driver,
	sclr => LO_a13_a_SCLR_driver,
	ena => LO_a13_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(13));

data_out_a41_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a40_combout,
	dataout => data_out_a41_DATAA_driver);

data_out_a41_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(13),
	dataout => data_out_a41_DATAB_driver);

data_out_a41_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a41_DATAC_driver);

data_out_a41_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(13),
	dataout => data_out_a41_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N28
data_out_a41 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a41_combout = (data_out_a40_combout & (((LO(13)) # (!mux_data_out_a0_a_ainput_o)))) # (!data_out_a40_combout & (A(13) & (mux_data_out_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a41_DATAA_driver,
	datab => data_out_a41_DATAB_driver,
	datac => data_out_a41_DATAC_driver,
	datad => data_out_a41_DATAD_driver,
	combout => data_out_a41_combout);

data_out_a42_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a42_DATAA_driver);

data_out_a42_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a42_DATAB_driver);

data_out_a42_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(13),
	dataout => data_out_a42_DATAC_driver);

data_out_a42_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a41_combout,
	dataout => data_out_a42_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N26
data_out_a42 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a42_combout = (mux_data_out_a2_a_ainput_o & (data_out_a0_combout & (PC(13)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a41_combout) # ((data_out_a0_combout & PC(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a42_DATAA_driver,
	datab => data_out_a42_DATAB_driver,
	datac => data_out_a42_DATAC_driver,
	datad => data_out_a42_DATAD_driver,
	combout => data_out_a42_combout);

A_a15_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a15_DATAC_driver);

A_a15_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a14_a_ainput_o,
	dataout => A_a15_DATAD_driver);

-- Location: LCCOMB_X38_Y30_N12
A_a15 : cycloneive_lcell_comb
-- Equation(s):
-- A_a15_combout = (!reset_ainput_o & data_in_a14_a_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a15_DATAC_driver,
	datad => A_a15_DATAD_driver,
	combout => A_a15_combout);

A_a14_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a14_a_CLK_driver);

A_a14_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a15_combout,
	dataout => A_a14_a_ASDATA_driver);

A_a14_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a14_a_ENA_driver);

-- Location: FF_X38_Y26_N15
A_a14_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a14_a_CLK_driver,
	asdata => A_a14_a_ASDATA_driver,
	sload => VCC,
	ena => A_a14_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(14));

data_out_a43_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a43_DATAA_driver);

data_out_a43_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(14),
	dataout => data_out_a43_DATAB_driver);

data_out_a43_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(14),
	dataout => data_out_a43_DATAC_driver);

data_out_a43_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a43_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N14
data_out_a43 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a43_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o & ((A(14)))) # (!mux_data_out_a0_a_ainput_o & (O(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a43_DATAA_driver,
	datab => data_out_a43_DATAB_driver,
	datac => data_out_a43_DATAC_driver,
	datad => data_out_a43_DATAD_driver,
	combout => data_out_a43_combout);

LO_a15_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(13),
	dataout => LO_a15_DATAA_driver);

LO_a15_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a14_a_ainput_o,
	dataout => LO_a15_DATAB_driver);

LO_a15_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a15_DATAC_driver);

-- Location: LCCOMB_X41_Y27_N0
LO_a15 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a15_combout = (mux_lo_a1_a_ainput_o & ((data_in_a14_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a15_DATAA_driver,
	datab => LO_a15_DATAB_driver,
	datac => LO_a15_DATAC_driver,
	combout => LO_a15_combout);

LO_a14_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a14_a_CLK_driver);

LO_a14_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a15_combout,
	dataout => LO_a14_a_D_driver);

LO_a14_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a14_a_SCLR_driver);

LO_a14_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a14_a_ENA_driver);

-- Location: FF_X41_Y27_N1
LO_a14_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a14_a_CLK_driver,
	d => LO_a14_a_D_driver,
	sclr => LO_a14_a_SCLR_driver,
	ena => LO_a14_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(14));

data_out_a44_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a44_DATAA_driver);

data_out_a44_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(14),
	dataout => data_out_a44_DATAB_driver);

data_out_a44_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a43_combout,
	dataout => data_out_a44_DATAC_driver);

data_out_a44_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(14),
	dataout => data_out_a44_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N4
data_out_a44 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a44_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a43_combout & ((LO(14)))) # (!data_out_a43_combout & (HI(14))))) # (!mux_data_out_a1_a_ainput_o & (((data_out_a43_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a44_DATAA_driver,
	datab => data_out_a44_DATAB_driver,
	datac => data_out_a44_DATAC_driver,
	datad => data_out_a44_DATAD_driver,
	combout => data_out_a44_combout);

data_out_a45_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a45_DATAA_driver);

data_out_a45_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a44_combout,
	dataout => data_out_a45_DATAB_driver);

data_out_a45_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a45_DATAC_driver);

data_out_a45_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(14),
	dataout => data_out_a45_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N0
data_out_a45 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a45_combout = (mux_data_out_a2_a_ainput_o & (((data_out_a0_combout & PC(14))))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a44_combout) # ((data_out_a0_combout & PC(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a45_DATAA_driver,
	datab => data_out_a45_DATAB_driver,
	datac => data_out_a45_DATAC_driver,
	datad => data_out_a45_DATAD_driver,
	combout => data_out_a45_combout);

data_in_a15_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(15),
	dataout => data_in_a15_a_ainput_I_driver);

-- Location: IOIBUF_X61_Y43_N8
data_in_a15_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a15_a_ainput_I_driver,
	o => data_in_a15_a_ainput_o);

LO_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(14),
	dataout => LO_a16_DATAB_driver);

LO_a16_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a16_DATAC_driver);

LO_a16_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a15_a_ainput_o,
	dataout => LO_a16_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N28
LO_a16 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a16_combout = (mux_lo_a1_a_ainput_o & ((data_in_a15_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a16_DATAB_driver,
	datac => LO_a16_DATAC_driver,
	datad => LO_a16_DATAD_driver,
	combout => LO_a16_combout);

LO_a15_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a15_a_CLK_driver);

LO_a15_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a16_combout,
	dataout => LO_a15_a_D_driver);

LO_a15_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a15_a_SCLR_driver);

LO_a15_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a15_a_ENA_driver);

-- Location: FF_X35_Y28_N29
LO_a15_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a15_a_CLK_driver,
	d => LO_a15_a_D_driver,
	sclr => LO_a15_a_SCLR_driver,
	ena => LO_a15_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(15));

A_a15_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a15_a_CLK_driver);

A_a15_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a16_combout,
	dataout => A_a15_a_ASDATA_driver);

A_a15_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a15_a_ENA_driver);

-- Location: FF_X38_Y26_N23
A_a15_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a15_a_CLK_driver,
	asdata => A_a15_a_ASDATA_driver,
	sload => VCC,
	ena => A_a15_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(15));

data_out_a47_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a46_combout,
	dataout => data_out_a47_DATAA_driver);

data_out_a47_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(15),
	dataout => data_out_a47_DATAB_driver);

data_out_a47_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(15),
	dataout => data_out_a47_DATAC_driver);

data_out_a47_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a47_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N22
data_out_a47 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a47_combout = (data_out_a46_combout & ((LO(15)) # ((!mux_data_out_a0_a_ainput_o)))) # (!data_out_a46_combout & (((A(15) & mux_data_out_a0_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a47_DATAA_driver,
	datab => data_out_a47_DATAB_driver,
	datac => data_out_a47_DATAC_driver,
	datad => data_out_a47_DATAD_driver,
	combout => data_out_a47_combout);

intr_pc_a13_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(13),
	dataout => intr_pc_a13_a_ainput_I_driver);

-- Location: IOIBUF_X11_Y43_N15
intr_pc_a13_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a13_a_ainput_I_driver,
	o => intr_pc_a13_a_ainput_o);

PC_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a16_DATAA_driver);

PC_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a13_a_ainput_o,
	dataout => PC_a16_DATAB_driver);

PC_a16_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a32_combout,
	dataout => PC_a16_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N24
PC_a16 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a16_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a13_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a32_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a16_DATAA_driver,
	datab => PC_a16_DATAB_driver,
	datad => PC_a16_DATAD_driver,
	combout => PC_a16_combout);

PC_a15_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a15_a_CLK_driver);

PC_a15_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a16_combout,
	dataout => PC_a15_a_D_driver);

PC_a15_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a15_a_ainput_o,
	dataout => PC_a15_a_ASDATA_driver);

PC_a15_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a15_a_SCLR_driver);

PC_a15_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a15_a_SLOAD_driver);

PC_a15_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a15_a_ENA_driver);

-- Location: FF_X39_Y28_N25
PC_a15_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a15_a_CLK_driver,
	d => PC_a15_a_D_driver,
	asdata => PC_a15_a_ASDATA_driver,
	sclr => PC_a15_a_SCLR_driver,
	sload => PC_a15_a_SLOAD_driver,
	ena => PC_a15_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(15));

data_out_a48_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a47_combout,
	dataout => data_out_a48_DATAA_driver);

data_out_a48_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a48_DATAB_driver);

data_out_a48_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a48_DATAC_driver);

data_out_a48_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(15),
	dataout => data_out_a48_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N16
data_out_a48 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a48_combout = (data_out_a47_combout & (((data_out_a0_combout & PC(15))) # (!mux_data_out_a2_a_ainput_o))) # (!data_out_a47_combout & (data_out_a0_combout & ((PC(15)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a48_DATAA_driver,
	datab => data_out_a48_DATAB_driver,
	datac => data_out_a48_DATAC_driver,
	datad => data_out_a48_DATAD_driver,
	combout => data_out_a48_combout);

data_out_a49_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(16),
	dataout => data_out_a49_DATAA_driver);

data_out_a49_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a49_DATAB_driver);

data_out_a49_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(16),
	dataout => data_out_a49_DATAC_driver);

data_out_a49_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a49_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N4
data_out_a49 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a49_combout = (mux_data_out_a0_a_ainput_o & (((A(16)) # (mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & (O(16) & ((!mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a49_DATAA_driver,
	datab => data_out_a49_DATAB_driver,
	datac => data_out_a49_DATAC_driver,
	datad => data_out_a49_DATAD_driver,
	combout => data_out_a49_combout);

LO_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a16_a_ainput_o,
	dataout => LO_a17_DATAA_driver);

LO_a17_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a17_DATAC_driver);

LO_a17_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(15),
	dataout => LO_a17_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N2
LO_a17 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a17_combout = (mux_lo_a1_a_ainput_o & (data_in_a16_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a17_DATAA_driver,
	datac => LO_a17_DATAC_driver,
	datad => LO_a17_DATAD_driver,
	combout => LO_a17_combout);

LO_a16_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a16_a_CLK_driver);

LO_a16_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a17_combout,
	dataout => LO_a16_a_D_driver);

LO_a16_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a16_a_SCLR_driver);

LO_a16_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a16_a_ENA_driver);

-- Location: FF_X35_Y28_N3
LO_a16_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a16_a_CLK_driver,
	d => LO_a16_a_D_driver,
	sclr => LO_a16_a_SCLR_driver,
	ena => LO_a16_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(16));

data_out_a50_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a50_DATAA_driver);

data_out_a50_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a49_combout,
	dataout => data_out_a50_DATAB_driver);

data_out_a50_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(16),
	dataout => data_out_a50_DATAC_driver);

data_out_a50_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(16),
	dataout => data_out_a50_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N10
data_out_a50 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a50_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a49_combout & ((LO(16)))) # (!data_out_a49_combout & (HI(16))))) # (!mux_data_out_a1_a_ainput_o & (data_out_a49_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a50_DATAA_driver,
	datab => data_out_a50_DATAB_driver,
	datac => data_out_a50_DATAC_driver,
	datad => data_out_a50_DATAD_driver,
	combout => data_out_a50_combout);

data_out_a51_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(16),
	dataout => data_out_a51_DATAA_driver);

data_out_a51_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a51_DATAB_driver);

data_out_a51_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a51_DATAC_driver);

data_out_a51_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a50_combout,
	dataout => data_out_a51_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N6
data_out_a51 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a51_combout = (PC(16) & ((data_out_a0_combout) # ((!mux_data_out_a2_a_ainput_o & data_out_a50_combout)))) # (!PC(16) & (((!mux_data_out_a2_a_ainput_o & data_out_a50_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a51_DATAA_driver,
	datab => data_out_a51_DATAB_driver,
	datac => data_out_a51_DATAC_driver,
	datad => data_out_a51_DATAD_driver,
	combout => data_out_a51_combout);

data_out_a52_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(17),
	dataout => data_out_a52_DATAA_driver);

data_out_a52_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(17),
	dataout => data_out_a52_DATAB_driver);

data_out_a52_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a52_DATAC_driver);

data_out_a52_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a52_DATAD_driver);

-- Location: LCCOMB_X39_Y25_N24
data_out_a52 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a52_combout = (mux_data_out_a1_a_ainput_o & (((HI(17)) # (mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & (O(17) & ((!mux_data_out_a0_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a52_DATAA_driver,
	datab => data_out_a52_DATAB_driver,
	datac => data_out_a52_DATAC_driver,
	datad => data_out_a52_DATAD_driver,
	combout => data_out_a52_combout);

data_out_a53_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(17),
	dataout => data_out_a53_DATAA_driver);

data_out_a53_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(17),
	dataout => data_out_a53_DATAB_driver);

data_out_a53_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a53_DATAC_driver);

data_out_a53_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a52_combout,
	dataout => data_out_a53_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N30
data_out_a53 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a53_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a52_combout & (LO(17))) # (!data_out_a52_combout & ((A(17)))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a52_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a53_DATAA_driver,
	datab => data_out_a53_DATAB_driver,
	datac => data_out_a53_DATAC_driver,
	datad => data_out_a53_DATAD_driver,
	combout => data_out_a53_combout);

data_out_a54_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a53_combout,
	dataout => data_out_a54_DATAA_driver);

data_out_a54_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a54_DATAB_driver);

data_out_a54_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a54_DATAC_driver);

data_out_a54_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(17),
	dataout => data_out_a54_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N20
data_out_a54 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a54_combout = (data_out_a53_combout & (((data_out_a0_combout & PC(17))) # (!mux_data_out_a2_a_ainput_o))) # (!data_out_a53_combout & (((data_out_a0_combout & PC(17)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a54_DATAA_driver,
	datab => data_out_a54_DATAB_driver,
	datac => data_out_a54_DATAC_driver,
	datad => data_out_a54_DATAD_driver,
	combout => data_out_a54_combout);

LO_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(16),
	dataout => LO_a18_DATAB_driver);

LO_a18_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a18_DATAC_driver);

LO_a18_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a17_a_ainput_o,
	dataout => LO_a18_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N0
LO_a18 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a18_combout = (mux_lo_a1_a_ainput_o & ((data_in_a17_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(16)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a18_DATAB_driver,
	datac => LO_a18_DATAC_driver,
	datad => LO_a18_DATAD_driver,
	combout => LO_a18_combout);

LO_a17_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a17_a_CLK_driver);

LO_a17_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a18_combout,
	dataout => LO_a17_a_D_driver);

LO_a17_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a17_a_SCLR_driver);

LO_a17_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a17_a_ENA_driver);

-- Location: FF_X35_Y28_N1
LO_a17_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a17_a_CLK_driver,
	d => LO_a17_a_D_driver,
	sclr => LO_a17_a_SCLR_driver,
	ena => LO_a17_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(17));

LO_a19_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a18_a_ainput_o,
	dataout => LO_a19_DATAA_driver);

LO_a19_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a19_DATAC_driver);

LO_a19_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(17),
	dataout => LO_a19_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N14
LO_a19 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a19_combout = (mux_lo_a1_a_ainput_o & (data_in_a18_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(17))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a19_DATAA_driver,
	datac => LO_a19_DATAC_driver,
	datad => LO_a19_DATAD_driver,
	combout => LO_a19_combout);

LO_a18_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a18_a_CLK_driver);

LO_a18_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a19_combout,
	dataout => LO_a18_a_D_driver);

LO_a18_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a18_a_SCLR_driver);

LO_a18_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a18_a_ENA_driver);

-- Location: FF_X35_Y28_N15
LO_a18_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a18_a_CLK_driver,
	d => LO_a18_a_D_driver,
	sclr => LO_a18_a_SCLR_driver,
	ena => LO_a18_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(18));

HI_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a20_DATAA_driver);

HI_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(17),
	dataout => HI_a20_DATAB_driver);

HI_a20_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a20_DATAC_driver);

HI_a20_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a38_combout,
	dataout => HI_a20_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N18
HI_a20 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a20_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a38_combout))) # (!Add0_a66_combout & (HI(17)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a20_DATAA_driver,
	datab => HI_a20_DATAB_driver,
	datac => HI_a20_DATAC_driver,
	datad => HI_a20_DATAD_driver,
	combout => HI_a20_combout);

HI_a18_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a18_a_CLK_driver);

HI_a18_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a20_combout,
	dataout => HI_a18_a_D_driver);

HI_a18_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a18_a_ENA_driver);

-- Location: FF_X37_Y25_N19
HI_a18_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a18_a_CLK_driver,
	d => HI_a18_a_D_driver,
	ena => HI_a18_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(18));

data_out_a56_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a55_combout,
	dataout => data_out_a56_DATAA_driver);

data_out_a56_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(18),
	dataout => data_out_a56_DATAB_driver);

data_out_a56_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a56_DATAC_driver);

data_out_a56_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(18),
	dataout => data_out_a56_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N24
data_out_a56 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a56_combout = (data_out_a55_combout & ((LO(18)) # ((!mux_data_out_a1_a_ainput_o)))) # (!data_out_a55_combout & (((mux_data_out_a1_a_ainput_o & HI(18)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a56_DATAA_driver,
	datab => data_out_a56_DATAB_driver,
	datac => data_out_a56_DATAC_driver,
	datad => data_out_a56_DATAD_driver,
	combout => data_out_a56_combout);

PC_a19_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a16_a_ainput_o,
	dataout => PC_a19_DATAA_driver);

PC_a19_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a19_DATAB_driver);

PC_a19_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a38_combout,
	dataout => PC_a19_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N22
PC_a19 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a19_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a16_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a38_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a19_DATAA_driver,
	datab => PC_a19_DATAB_driver,
	datad => PC_a19_DATAD_driver,
	combout => PC_a19_combout);

PC_a18_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a18_a_CLK_driver);

PC_a18_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a19_combout,
	dataout => PC_a18_a_D_driver);

PC_a18_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a18_a_ainput_o,
	dataout => PC_a18_a_ASDATA_driver);

PC_a18_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a18_a_SCLR_driver);

PC_a18_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a18_a_SLOAD_driver);

PC_a18_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a18_a_ENA_driver);

-- Location: FF_X39_Y28_N23
PC_a18_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a18_a_CLK_driver,
	d => PC_a18_a_D_driver,
	asdata => PC_a18_a_ASDATA_driver,
	sclr => PC_a18_a_SCLR_driver,
	sload => PC_a18_a_SLOAD_driver,
	ena => PC_a18_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(18));

data_out_a57_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a56_combout,
	dataout => data_out_a57_DATAA_driver);

data_out_a57_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(18),
	dataout => data_out_a57_DATAB_driver);

data_out_a57_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a57_DATAC_driver);

data_out_a57_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a57_DATAD_driver);

-- Location: LCCOMB_X35_Y29_N30
data_out_a57 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a57_combout = (data_out_a56_combout & (((PC(18) & data_out_a0_combout)) # (!mux_data_out_a2_a_ainput_o))) # (!data_out_a56_combout & (PC(18) & ((data_out_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a57_DATAA_driver,
	datab => data_out_a57_DATAB_driver,
	datac => data_out_a57_DATAC_driver,
	datad => data_out_a57_DATAD_driver,
	combout => data_out_a57_combout);

A_a19_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a19_a_CLK_driver);

A_a19_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a20_combout,
	dataout => A_a19_a_ASDATA_driver);

A_a19_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a19_a_ENA_driver);

-- Location: FF_X38_Y29_N25
A_a19_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a19_a_CLK_driver,
	asdata => A_a19_a_ASDATA_driver,
	sload => VCC,
	ena => A_a19_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(19));

HI_a21_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a21_DATAA_driver);

HI_a21_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(18),
	dataout => HI_a21_DATAB_driver);

HI_a21_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a21_DATAC_driver);

HI_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a40_combout,
	dataout => HI_a21_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N2
HI_a21 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a21_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a40_combout))) # (!Add0_a66_combout & (HI(18)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a21_DATAA_driver,
	datab => HI_a21_DATAB_driver,
	datac => HI_a21_DATAC_driver,
	datad => HI_a21_DATAD_driver,
	combout => HI_a21_combout);

HI_a19_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a19_a_CLK_driver);

HI_a19_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a21_combout,
	dataout => HI_a19_a_D_driver);

HI_a19_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a19_a_ENA_driver);

-- Location: FF_X37_Y25_N3
HI_a19_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a19_a_CLK_driver,
	d => HI_a19_a_D_driver,
	ena => HI_a19_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(19));

data_out_a58_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a58_DATAA_driver);

data_out_a58_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(19),
	dataout => data_out_a58_DATAB_driver);

data_out_a58_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a58_DATAC_driver);

data_out_a58_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(19),
	dataout => data_out_a58_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N6
data_out_a58 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a58_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o) # (HI(19))))) # (!mux_data_out_a1_a_ainput_o & (O(19) & (!mux_data_out_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a58_DATAA_driver,
	datab => data_out_a58_DATAB_driver,
	datac => data_out_a58_DATAC_driver,
	datad => data_out_a58_DATAD_driver,
	combout => data_out_a58_combout);

data_out_a59_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(19),
	dataout => data_out_a59_DATAA_driver);

data_out_a59_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a59_DATAB_driver);

data_out_a59_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(19),
	dataout => data_out_a59_DATAC_driver);

data_out_a59_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a58_combout,
	dataout => data_out_a59_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N24
data_out_a59 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a59_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a58_combout & (LO(19))) # (!data_out_a58_combout & ((A(19)))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a58_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a59_DATAA_driver,
	datab => data_out_a59_DATAB_driver,
	datac => data_out_a59_DATAC_driver,
	datad => data_out_a59_DATAD_driver,
	combout => data_out_a59_combout);

intr_pc_a17_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(17),
	dataout => intr_pc_a17_a_ainput_I_driver);

-- Location: IOIBUF_X50_Y43_N8
intr_pc_a17_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a17_a_ainput_I_driver,
	o => intr_pc_a17_a_ainput_o);

PC_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a20_DATAA_driver);

PC_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a17_a_ainput_o,
	dataout => PC_a20_DATAB_driver);

PC_a20_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a40_combout,
	dataout => PC_a20_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N28
PC_a20 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a20_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a17_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a40_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a20_DATAA_driver,
	datab => PC_a20_DATAB_driver,
	datad => PC_a20_DATAD_driver,
	combout => PC_a20_combout);

PC_a19_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a19_a_CLK_driver);

PC_a19_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a20_combout,
	dataout => PC_a19_a_D_driver);

PC_a19_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a19_a_ainput_o,
	dataout => PC_a19_a_ASDATA_driver);

PC_a19_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a19_a_SCLR_driver);

PC_a19_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a19_a_SLOAD_driver);

PC_a19_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a19_a_ENA_driver);

-- Location: FF_X39_Y28_N29
PC_a19_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a19_a_CLK_driver,
	d => PC_a19_a_D_driver,
	asdata => PC_a19_a_ASDATA_driver,
	sclr => PC_a19_a_SCLR_driver,
	sload => PC_a19_a_SLOAD_driver,
	ena => PC_a19_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(19));

data_out_a60_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a60_DATAA_driver);

data_out_a60_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a59_combout,
	dataout => data_out_a60_DATAB_driver);

data_out_a60_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(19),
	dataout => data_out_a60_DATAC_driver);

data_out_a60_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a60_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N16
data_out_a60 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a60_combout = (data_out_a0_combout & ((PC(19)) # ((data_out_a59_combout & !mux_data_out_a2_a_ainput_o)))) # (!data_out_a0_combout & (data_out_a59_combout & ((!mux_data_out_a2_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a60_DATAA_driver,
	datab => data_out_a60_DATAB_driver,
	datac => data_out_a60_DATAC_driver,
	datad => data_out_a60_DATAD_driver,
	combout => data_out_a60_combout);

intr_pc_a18_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(18),
	dataout => intr_pc_a18_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y11_N8
intr_pc_a18_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a18_a_ainput_I_driver,
	o => intr_pc_a18_a_ainput_o);

PC_a21_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a21_DATAA_driver);

PC_a21_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a18_a_ainput_o,
	dataout => PC_a21_DATAB_driver);

PC_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a42_combout,
	dataout => PC_a21_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N14
PC_a21 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a21_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a18_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a42_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a21_DATAA_driver,
	datab => PC_a21_DATAB_driver,
	datad => PC_a21_DATAD_driver,
	combout => PC_a21_combout);

data_in_a20_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_data_in(20),
	dataout => data_in_a20_a_ainput_I_driver);

-- Location: IOIBUF_X36_Y0_N22
data_in_a20_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data_in_a20_a_ainput_I_driver,
	o => data_in_a20_a_ainput_o);

PC_a20_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a20_a_CLK_driver);

PC_a20_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a21_combout,
	dataout => PC_a20_a_D_driver);

PC_a20_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a20_a_ainput_o,
	dataout => PC_a20_a_ASDATA_driver);

PC_a20_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a20_a_SCLR_driver);

PC_a20_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a20_a_SLOAD_driver);

PC_a20_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a20_a_ENA_driver);

-- Location: FF_X39_Y28_N15
PC_a20_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a20_a_CLK_driver,
	d => PC_a20_a_D_driver,
	asdata => PC_a20_a_ASDATA_driver,
	sclr => PC_a20_a_SCLR_driver,
	sload => PC_a20_a_SLOAD_driver,
	ena => PC_a20_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(20));

HI_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a22_DATAA_driver);

HI_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(19),
	dataout => HI_a22_DATAB_driver);

HI_a22_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a22_DATAC_driver);

HI_a22_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a42_combout,
	dataout => HI_a22_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N8
HI_a22 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a22_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a42_combout))) # (!Add0_a66_combout & (HI(19)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a22_DATAA_driver,
	datab => HI_a22_DATAB_driver,
	datac => HI_a22_DATAC_driver,
	datad => HI_a22_DATAD_driver,
	combout => HI_a22_combout);

HI_a20_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a20_a_CLK_driver);

HI_a20_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a22_combout,
	dataout => HI_a20_a_D_driver);

HI_a20_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a20_a_ENA_driver);

-- Location: FF_X37_Y25_N9
HI_a20_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a20_a_CLK_driver,
	d => HI_a20_a_D_driver,
	ena => HI_a20_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(20));

A_a20_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a20_a_CLK_driver);

A_a20_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a21_combout,
	dataout => A_a20_a_ASDATA_driver);

A_a20_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a20_a_ENA_driver);

-- Location: FF_X38_Y28_N31
A_a20_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a20_a_CLK_driver,
	asdata => A_a20_a_ASDATA_driver,
	sload => VCC,
	ena => A_a20_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(20));

data_out_a61_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(20),
	dataout => data_out_a61_DATAA_driver);

data_out_a61_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a61_DATAB_driver);

data_out_a61_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a61_DATAC_driver);

data_out_a61_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(20),
	dataout => data_out_a61_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N20
data_out_a61 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a61_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o) # (A(20))))) # (!mux_data_out_a0_a_ainput_o & (O(20) & (!mux_data_out_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a61_DATAA_driver,
	datab => data_out_a61_DATAB_driver,
	datac => data_out_a61_DATAC_driver,
	datad => data_out_a61_DATAD_driver,
	combout => data_out_a61_combout);

data_out_a62_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(20),
	dataout => data_out_a62_DATAA_driver);

data_out_a62_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a62_DATAB_driver);

data_out_a62_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(20),
	dataout => data_out_a62_DATAC_driver);

data_out_a62_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a61_combout,
	dataout => data_out_a62_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N0
data_out_a62 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a62_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a61_combout & (LO(20))) # (!data_out_a61_combout & ((HI(20)))))) # (!mux_data_out_a1_a_ainput_o & (((data_out_a61_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a62_DATAA_driver,
	datab => data_out_a62_DATAB_driver,
	datac => data_out_a62_DATAC_driver,
	datad => data_out_a62_DATAD_driver,
	combout => data_out_a62_combout);

data_out_a63_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a63_DATAA_driver);

data_out_a63_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a63_DATAB_driver);

data_out_a63_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(20),
	dataout => data_out_a63_DATAC_driver);

data_out_a63_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a62_combout,
	dataout => data_out_a63_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N18
data_out_a63 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a63_combout = (mux_data_out_a2_a_ainput_o & (data_out_a0_combout & (PC(20)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a62_combout) # ((data_out_a0_combout & PC(20)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a63_DATAA_driver,
	datab => data_out_a63_DATAB_driver,
	datac => data_out_a63_DATAC_driver,
	datad => data_out_a63_DATAD_driver,
	combout => data_out_a63_combout);

intr_pc_a19_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(19),
	dataout => intr_pc_a19_a_ainput_I_driver);

-- Location: IOIBUF_X36_Y0_N15
intr_pc_a19_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a19_a_ainput_I_driver,
	o => intr_pc_a19_a_ainput_o);

PC_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a22_DATAA_driver);

PC_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a19_a_ainput_o,
	dataout => PC_a22_DATAB_driver);

PC_a22_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a44_combout,
	dataout => PC_a22_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N8
PC_a22 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a22_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a19_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a44_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a22_DATAA_driver,
	datab => PC_a22_DATAB_driver,
	datad => PC_a22_DATAD_driver,
	combout => PC_a22_combout);

PC_a21_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a21_a_CLK_driver);

PC_a21_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a22_combout,
	dataout => PC_a21_a_D_driver);

PC_a21_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a21_a_ainput_o,
	dataout => PC_a21_a_ASDATA_driver);

PC_a21_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a21_a_SCLR_driver);

PC_a21_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a21_a_SLOAD_driver);

PC_a21_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a21_a_ENA_driver);

-- Location: FF_X39_Y28_N9
PC_a21_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a21_a_CLK_driver,
	d => PC_a21_a_D_driver,
	asdata => PC_a21_a_ASDATA_driver,
	sclr => PC_a21_a_SCLR_driver,
	sload => PC_a21_a_SLOAD_driver,
	ena => PC_a21_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(21));

A_a21_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a21_a_CLK_driver);

A_a21_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a22_combout,
	dataout => A_a21_a_ASDATA_driver);

A_a21_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a21_a_ENA_driver);

-- Location: FF_X36_Y28_N21
A_a21_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a21_a_CLK_driver,
	asdata => A_a21_a_ASDATA_driver,
	sload => VCC,
	ena => A_a21_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(21));

data_out_a64_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a64_DATAA_driver);

data_out_a64_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(21),
	dataout => data_out_a64_DATAB_driver);

data_out_a64_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(21),
	dataout => data_out_a64_DATAC_driver);

data_out_a64_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a64_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N24
data_out_a64 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a64_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o & ((HI(21)))) # (!mux_data_out_a1_a_ainput_o & (O(21)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a64_DATAA_driver,
	datab => data_out_a64_DATAB_driver,
	datac => data_out_a64_DATAC_driver,
	datad => data_out_a64_DATAD_driver,
	combout => data_out_a64_combout);

LO_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(20),
	dataout => LO_a22_DATAA_driver);

LO_a22_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a22_DATAC_driver);

LO_a22_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a21_a_ainput_o,
	dataout => LO_a22_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N12
LO_a22 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a22_combout = (mux_lo_a1_a_ainput_o & ((data_in_a21_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(20)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a22_DATAA_driver,
	datac => LO_a22_DATAC_driver,
	datad => LO_a22_DATAD_driver,
	combout => LO_a22_combout);

LO_a21_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a21_a_CLK_driver);

LO_a21_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a22_combout,
	dataout => LO_a21_a_D_driver);

LO_a21_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a21_a_SCLR_driver);

LO_a21_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a21_a_ENA_driver);

-- Location: FF_X35_Y28_N13
LO_a21_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a21_a_CLK_driver,
	d => LO_a21_a_D_driver,
	sclr => LO_a21_a_SCLR_driver,
	ena => LO_a21_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(21));

data_out_a65_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a65_DATAA_driver);

data_out_a65_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(21),
	dataout => data_out_a65_DATAB_driver);

data_out_a65_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a64_combout,
	dataout => data_out_a65_DATAC_driver);

data_out_a65_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(21),
	dataout => data_out_a65_DATAD_driver);

-- Location: LCCOMB_X41_Y28_N20
data_out_a65 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a65_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a64_combout & ((LO(21)))) # (!data_out_a64_combout & (A(21))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a64_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a65_DATAA_driver,
	datab => data_out_a65_DATAB_driver,
	datac => data_out_a65_DATAC_driver,
	datad => data_out_a65_DATAD_driver,
	combout => data_out_a65_combout);

data_out_a66_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a66_DATAA_driver);

data_out_a66_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a66_DATAB_driver);

data_out_a66_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(21),
	dataout => data_out_a66_DATAC_driver);

data_out_a66_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a65_combout,
	dataout => data_out_a66_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N2
data_out_a66 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a66_combout = (mux_data_out_a2_a_ainput_o & (data_out_a0_combout & (PC(21)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a65_combout) # ((data_out_a0_combout & PC(21)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a66_DATAA_driver,
	datab => data_out_a66_DATAB_driver,
	datac => data_out_a66_DATAC_driver,
	datad => data_out_a66_DATAD_driver,
	combout => data_out_a66_combout);

intr_pc_a20_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(20),
	dataout => intr_pc_a20_a_ainput_I_driver);

-- Location: IOIBUF_X50_Y43_N15
intr_pc_a20_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a20_a_ainput_I_driver,
	o => intr_pc_a20_a_ainput_o);

PC_a23_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a23_DATAA_driver);

PC_a23_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a20_a_ainput_o,
	dataout => PC_a23_DATAB_driver);

PC_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a46_combout,
	dataout => PC_a23_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N6
PC_a23 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a23_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a20_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a46_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a23_DATAA_driver,
	datab => PC_a23_DATAB_driver,
	datad => PC_a23_DATAD_driver,
	combout => PC_a23_combout);

PC_a22_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a22_a_CLK_driver);

PC_a22_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a23_combout,
	dataout => PC_a22_a_D_driver);

PC_a22_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a22_a_ainput_o,
	dataout => PC_a22_a_ASDATA_driver);

PC_a22_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a22_a_SCLR_driver);

PC_a22_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a22_a_SLOAD_driver);

PC_a22_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a22_a_ENA_driver);

-- Location: FF_X39_Y28_N7
PC_a22_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a22_a_CLK_driver,
	d => PC_a22_a_D_driver,
	asdata => PC_a22_a_ASDATA_driver,
	sclr => PC_a22_a_SCLR_driver,
	sload => PC_a22_a_SLOAD_driver,
	ena => PC_a22_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(22));

A_a22_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a22_a_CLK_driver);

A_a22_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a23_combout,
	dataout => A_a22_a_ASDATA_driver);

A_a22_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a22_a_ENA_driver);

-- Location: FF_X36_Y28_N1
A_a22_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a22_a_CLK_driver,
	asdata => A_a22_a_ASDATA_driver,
	sload => VCC,
	ena => A_a22_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(22));

data_out_a67_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a67_DATAA_driver);

data_out_a67_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(22),
	dataout => data_out_a67_DATAB_driver);

data_out_a67_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(22),
	dataout => data_out_a67_DATAC_driver);

data_out_a67_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a67_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N0
data_out_a67 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a67_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o)))) # (!mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o & ((A(22)))) # (!mux_data_out_a0_a_ainput_o & (O(22)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a67_DATAA_driver,
	datab => data_out_a67_DATAB_driver,
	datac => data_out_a67_DATAC_driver,
	datad => data_out_a67_DATAD_driver,
	combout => data_out_a67_combout);

LO_a23_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(21),
	dataout => LO_a23_DATAA_driver);

LO_a23_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a23_DATAC_driver);

LO_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a22_a_ainput_o,
	dataout => LO_a23_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N10
LO_a23 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a23_combout = (mux_lo_a1_a_ainput_o & ((data_in_a22_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a23_DATAA_driver,
	datac => LO_a23_DATAC_driver,
	datad => LO_a23_DATAD_driver,
	combout => LO_a23_combout);

LO_a22_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a22_a_CLK_driver);

LO_a22_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a23_combout,
	dataout => LO_a22_a_D_driver);

LO_a22_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a22_a_SCLR_driver);

LO_a22_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a22_a_ENA_driver);

-- Location: FF_X35_Y28_N11
LO_a22_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a22_a_CLK_driver,
	d => LO_a22_a_D_driver,
	sclr => LO_a22_a_SCLR_driver,
	ena => LO_a22_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(22));

data_out_a68_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a68_DATAA_driver);

data_out_a68_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a67_combout,
	dataout => data_out_a68_DATAB_driver);

data_out_a68_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(22),
	dataout => data_out_a68_DATAC_driver);

data_out_a68_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(22),
	dataout => data_out_a68_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N6
data_out_a68 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a68_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a67_combout & (LO(22))) # (!data_out_a67_combout & ((HI(22)))))) # (!mux_data_out_a1_a_ainput_o & (data_out_a67_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a68_DATAA_driver,
	datab => data_out_a68_DATAB_driver,
	datac => data_out_a68_DATAC_driver,
	datad => data_out_a68_DATAD_driver,
	combout => data_out_a68_combout);

data_out_a69_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a69_DATAA_driver);

data_out_a69_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a69_DATAB_driver);

data_out_a69_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(22),
	dataout => data_out_a69_DATAC_driver);

data_out_a69_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a68_combout,
	dataout => data_out_a69_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N8
data_out_a69 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a69_combout = (mux_data_out_a2_a_ainput_o & (data_out_a0_combout & (PC(22)))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a68_combout) # ((data_out_a0_combout & PC(22)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a69_DATAA_driver,
	datab => data_out_a69_DATAB_driver,
	datac => data_out_a69_DATAC_driver,
	datad => data_out_a69_DATAD_driver,
	combout => data_out_a69_combout);

LO_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(22),
	dataout => LO_a24_DATAA_driver);

LO_a24_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a24_DATAC_driver);

LO_a24_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a23_a_ainput_o,
	dataout => LO_a24_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N20
LO_a24 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a24_combout = (mux_lo_a1_a_ainput_o & ((data_in_a23_a_ainput_o))) # (!mux_lo_a1_a_ainput_o & (LO(22)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a24_DATAA_driver,
	datac => LO_a24_DATAC_driver,
	datad => LO_a24_DATAD_driver,
	combout => LO_a24_combout);

LO_a23_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a23_a_CLK_driver);

LO_a23_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a24_combout,
	dataout => LO_a23_a_D_driver);

LO_a23_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a23_a_SCLR_driver);

LO_a23_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a23_a_ENA_driver);

-- Location: FF_X35_Y28_N21
LO_a23_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a23_a_CLK_driver,
	d => LO_a23_a_D_driver,
	sclr => LO_a23_a_SCLR_driver,
	ena => LO_a23_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(23));

A_a23_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a23_a_CLK_driver);

A_a23_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a24_combout,
	dataout => A_a23_a_ASDATA_driver);

A_a23_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a23_a_ENA_driver);

-- Location: FF_X36_Y28_N9
A_a23_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a23_a_CLK_driver,
	asdata => A_a23_a_ASDATA_driver,
	sload => VCC,
	ena => A_a23_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(23));

data_out_a71_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a70_combout,
	dataout => data_out_a71_DATAA_driver);

data_out_a71_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(23),
	dataout => data_out_a71_DATAB_driver);

data_out_a71_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(23),
	dataout => data_out_a71_DATAC_driver);

data_out_a71_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a71_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N8
data_out_a71 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a71_combout = (data_out_a70_combout & ((LO(23)) # ((!mux_data_out_a0_a_ainput_o)))) # (!data_out_a70_combout & (((A(23) & mux_data_out_a0_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a71_DATAA_driver,
	datab => data_out_a71_DATAB_driver,
	datac => data_out_a71_DATAC_driver,
	datad => data_out_a71_DATAD_driver,
	combout => data_out_a71_combout);

intr_pc_a21_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(21),
	dataout => intr_pc_a21_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y28_N15
intr_pc_a21_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a21_a_ainput_I_driver,
	o => intr_pc_a21_a_ainput_o);

PC_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a48_combout,
	dataout => PC_a24_DATAA_driver);

PC_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a24_DATAB_driver);

PC_a24_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a21_a_ainput_o,
	dataout => PC_a24_DATAD_driver);

-- Location: LCCOMB_X39_Y28_N12
PC_a24 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a24_combout = (mux_pc_a1_a_ainput_o & ((intr_pc_a21_a_ainput_o))) # (!mux_pc_a1_a_ainput_o & (Add0_a48_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a24_DATAA_driver,
	datab => PC_a24_DATAB_driver,
	datad => PC_a24_DATAD_driver,
	combout => PC_a24_combout);

PC_a23_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a23_a_CLK_driver);

PC_a23_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a24_combout,
	dataout => PC_a23_a_D_driver);

PC_a23_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a23_a_ainput_o,
	dataout => PC_a23_a_ASDATA_driver);

PC_a23_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a23_a_SCLR_driver);

PC_a23_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a23_a_SLOAD_driver);

PC_a23_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a23_a_ENA_driver);

-- Location: FF_X39_Y28_N13
PC_a23_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a23_a_CLK_driver,
	d => PC_a23_a_D_driver,
	asdata => PC_a23_a_ASDATA_driver,
	sclr => PC_a23_a_SCLR_driver,
	sload => PC_a23_a_SLOAD_driver,
	ena => PC_a23_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(23));

data_out_a72_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a71_combout,
	dataout => data_out_a72_DATAA_driver);

data_out_a72_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a72_DATAB_driver);

data_out_a72_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a72_DATAC_driver);

data_out_a72_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(23),
	dataout => data_out_a72_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N14
data_out_a72 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a72_combout = (data_out_a71_combout & (((data_out_a0_combout & PC(23))) # (!mux_data_out_a2_a_ainput_o))) # (!data_out_a71_combout & (data_out_a0_combout & ((PC(23)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a72_DATAA_driver,
	datab => data_out_a72_DATAB_driver,
	datac => data_out_a72_DATAC_driver,
	datad => data_out_a72_DATAD_driver,
	combout => data_out_a72_combout);

LO_a25_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a25_DATAA_driver);

LO_a25_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a24_a_ainput_o,
	dataout => LO_a25_DATAC_driver);

LO_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(23),
	dataout => LO_a25_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N6
LO_a25 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a25_combout = (mux_lo_a1_a_ainput_o & (data_in_a24_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a25_DATAA_driver,
	datac => LO_a25_DATAC_driver,
	datad => LO_a25_DATAD_driver,
	combout => LO_a25_combout);

LO_a24_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a24_a_CLK_driver);

LO_a24_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a25_combout,
	dataout => LO_a24_a_D_driver);

LO_a24_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a24_a_SCLR_driver);

LO_a24_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a24_a_ENA_driver);

-- Location: FF_X35_Y28_N7
LO_a24_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a24_a_CLK_driver,
	d => LO_a24_a_D_driver,
	sclr => LO_a24_a_SCLR_driver,
	ena => LO_a24_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(24));

data_out_a74_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a73_combout,
	dataout => data_out_a74_DATAA_driver);

data_out_a74_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(24),
	dataout => data_out_a74_DATAB_driver);

data_out_a74_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(24),
	dataout => data_out_a74_DATAC_driver);

data_out_a74_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a74_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N0
data_out_a74 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a74_combout = (data_out_a73_combout & (((LO(24)) # (!mux_data_out_a1_a_ainput_o)))) # (!data_out_a73_combout & (HI(24) & ((mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a74_DATAA_driver,
	datab => data_out_a74_DATAB_driver,
	datac => data_out_a74_DATAC_driver,
	datad => data_out_a74_DATAD_driver,
	combout => data_out_a74_combout);

intr_pc_a22_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(22),
	dataout => intr_pc_a22_a_ainput_I_driver);

-- Location: IOIBUF_X34_Y0_N15
intr_pc_a22_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a22_a_ainput_I_driver,
	o => intr_pc_a22_a_ainput_o);

PC_a25_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a25_DATAA_driver);

PC_a25_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a50_combout,
	dataout => PC_a25_DATAB_driver);

PC_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a22_a_ainput_o,
	dataout => PC_a25_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N20
PC_a25 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a25_combout = (mux_pc_a1_a_ainput_o & ((intr_pc_a22_a_ainput_o))) # (!mux_pc_a1_a_ainput_o & (Add0_a50_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a25_DATAA_driver,
	datab => PC_a25_DATAB_driver,
	datad => PC_a25_DATAD_driver,
	combout => PC_a25_combout);

PC_a24_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a24_a_CLK_driver);

PC_a24_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a25_combout,
	dataout => PC_a24_a_D_driver);

PC_a24_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a24_a_ainput_o,
	dataout => PC_a24_a_ASDATA_driver);

PC_a24_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a24_a_SCLR_driver);

PC_a24_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a24_a_SLOAD_driver);

PC_a24_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a24_a_ENA_driver);

-- Location: FF_X34_Y27_N21
PC_a24_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a24_a_CLK_driver,
	d => PC_a24_a_D_driver,
	asdata => PC_a24_a_ASDATA_driver,
	sclr => PC_a24_a_SCLR_driver,
	sload => PC_a24_a_SLOAD_driver,
	ena => PC_a24_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(24));

data_out_a75_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a75_DATAA_driver);

data_out_a75_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a74_combout,
	dataout => data_out_a75_DATAB_driver);

data_out_a75_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a75_DATAC_driver);

data_out_a75_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(24),
	dataout => data_out_a75_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N18
data_out_a75 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a75_combout = (data_out_a0_combout & ((PC(24)) # ((data_out_a74_combout & !mux_data_out_a2_a_ainput_o)))) # (!data_out_a0_combout & (data_out_a74_combout & (!mux_data_out_a2_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a75_DATAA_driver,
	datab => data_out_a75_DATAB_driver,
	datac => data_out_a75_DATAC_driver,
	datad => data_out_a75_DATAD_driver,
	combout => data_out_a75_combout);

LO_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a25_a_ainput_o,
	dataout => LO_a26_DATAB_driver);

LO_a26_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a26_DATAC_driver);

LO_a26_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(24),
	dataout => LO_a26_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N4
LO_a26 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a26_combout = (mux_lo_a1_a_ainput_o & (data_in_a25_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a26_DATAB_driver,
	datac => LO_a26_DATAC_driver,
	datad => LO_a26_DATAD_driver,
	combout => LO_a26_combout);

LO_a25_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a25_a_CLK_driver);

LO_a25_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a26_combout,
	dataout => LO_a25_a_D_driver);

LO_a25_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a25_a_SCLR_driver);

LO_a25_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a25_a_ENA_driver);

-- Location: FF_X35_Y28_N5
LO_a25_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a25_a_CLK_driver,
	d => LO_a25_a_D_driver,
	sclr => LO_a25_a_SCLR_driver,
	ena => LO_a25_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(25));

data_out_a77_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a76_combout,
	dataout => data_out_a77_DATAA_driver);

data_out_a77_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(25),
	dataout => data_out_a77_DATAB_driver);

data_out_a77_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a77_DATAC_driver);

data_out_a77_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(25),
	dataout => data_out_a77_DATAD_driver);

-- Location: LCCOMB_X38_Y26_N30
data_out_a77 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a77_combout = (data_out_a76_combout & ((LO(25)) # ((!mux_data_out_a0_a_ainput_o)))) # (!data_out_a76_combout & (((mux_data_out_a0_a_ainput_o & A(25)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a77_DATAA_driver,
	datab => data_out_a77_DATAB_driver,
	datac => data_out_a77_DATAC_driver,
	datad => data_out_a77_DATAD_driver,
	combout => data_out_a77_combout);

data_out_a78_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a78_DATAA_driver);

data_out_a78_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(25),
	dataout => data_out_a78_DATAB_driver);

data_out_a78_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a78_DATAC_driver);

data_out_a78_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a77_combout,
	dataout => data_out_a78_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N16
data_out_a78 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a78_combout = (data_out_a0_combout & ((PC(25)) # ((!mux_data_out_a2_a_ainput_o & data_out_a77_combout)))) # (!data_out_a0_combout & (((!mux_data_out_a2_a_ainput_o & data_out_a77_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a78_DATAA_driver,
	datab => data_out_a78_DATAB_driver,
	datac => data_out_a78_DATAC_driver,
	datad => data_out_a78_DATAD_driver,
	combout => data_out_a78_combout);

LO_a27_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a27_DATAA_driver);

LO_a27_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a26_a_ainput_o,
	dataout => LO_a27_DATAB_driver);

LO_a27_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(25),
	dataout => LO_a27_DATAC_driver);

-- Location: LCCOMB_X35_Y28_N26
LO_a27 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a27_combout = (mux_lo_a1_a_ainput_o & (data_in_a26_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(25))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => LO_a27_DATAA_driver,
	datab => LO_a27_DATAB_driver,
	datac => LO_a27_DATAC_driver,
	combout => LO_a27_combout);

LO_a26_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a26_a_CLK_driver);

LO_a26_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a27_combout,
	dataout => LO_a26_a_D_driver);

LO_a26_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a26_a_SCLR_driver);

LO_a26_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a26_a_ENA_driver);

-- Location: FF_X35_Y28_N27
LO_a26_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a26_a_CLK_driver,
	d => LO_a26_a_D_driver,
	sclr => LO_a26_a_SCLR_driver,
	ena => LO_a26_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(26));

data_out_a79_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(26),
	dataout => data_out_a79_DATAA_driver);

data_out_a79_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a79_DATAB_driver);

data_out_a79_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(26),
	dataout => data_out_a79_DATAC_driver);

data_out_a79_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a79_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N16
data_out_a79 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a79_combout = (mux_data_out_a0_a_ainput_o & (((A(26)) # (mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & (O(26) & ((!mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a79_DATAA_driver,
	datab => data_out_a79_DATAB_driver,
	datac => data_out_a79_DATAC_driver,
	datad => data_out_a79_DATAD_driver,
	combout => data_out_a79_combout);

data_out_a80_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a80_DATAA_driver);

data_out_a80_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(26),
	dataout => data_out_a80_DATAB_driver);

data_out_a80_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(26),
	dataout => data_out_a80_DATAC_driver);

data_out_a80_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a79_combout,
	dataout => data_out_a80_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N2
data_out_a80 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a80_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a79_combout & ((LO(26)))) # (!data_out_a79_combout & (HI(26))))) # (!mux_data_out_a1_a_ainput_o & (((data_out_a79_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a80_DATAA_driver,
	datab => data_out_a80_DATAB_driver,
	datac => data_out_a80_DATAC_driver,
	datad => data_out_a80_DATAD_driver,
	combout => data_out_a80_combout);

data_out_a81_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(26),
	dataout => data_out_a81_DATAA_driver);

data_out_a81_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a80_combout,
	dataout => data_out_a81_DATAB_driver);

data_out_a81_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a81_DATAC_driver);

data_out_a81_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a81_DATAD_driver);

-- Location: LCCOMB_X35_Y29_N20
data_out_a81 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a81_combout = (PC(26) & ((data_out_a0_combout) # ((data_out_a80_combout & !mux_data_out_a2_a_ainput_o)))) # (!PC(26) & (data_out_a80_combout & (!mux_data_out_a2_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a81_DATAA_driver,
	datab => data_out_a81_DATAB_driver,
	datac => data_out_a81_DATAC_driver,
	datad => data_out_a81_DATAD_driver,
	combout => data_out_a81_combout);

intr_pc_a25_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_intr_pc(25),
	dataout => intr_pc_a25_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y11_N22
intr_pc_a25_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => intr_pc_a25_a_ainput_I_driver,
	o => intr_pc_a25_a_ainput_o);

PC_a28_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a28_DATAA_driver);

PC_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => intr_pc_a25_a_ainput_o,
	dataout => PC_a28_DATAB_driver);

PC_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a56_combout,
	dataout => PC_a28_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N10
PC_a28 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a28_combout = (mux_pc_a1_a_ainput_o & (intr_pc_a25_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a56_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a28_DATAA_driver,
	datab => PC_a28_DATAB_driver,
	datad => PC_a28_DATAD_driver,
	combout => PC_a28_combout);

PC_a27_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a27_a_CLK_driver);

PC_a27_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a28_combout,
	dataout => PC_a27_a_D_driver);

PC_a27_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a27_a_ainput_o,
	dataout => PC_a27_a_ASDATA_driver);

PC_a27_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a27_a_SCLR_driver);

PC_a27_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ALT_INV_mux_pc_a0_a_ainput_o,
	dataout => PC_a27_a_SLOAD_driver);

PC_a27_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a1_a_a1_combout,
	dataout => PC_a27_a_ENA_driver);

-- Location: FF_X34_Y27_N11
PC_a27_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a27_a_CLK_driver,
	d => PC_a27_a_D_driver,
	asdata => PC_a27_a_ASDATA_driver,
	sclr => PC_a27_a_SCLR_driver,
	sload => PC_a27_a_SLOAD_driver,
	ena => PC_a27_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(27));

LO_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a27_a_ainput_o,
	dataout => LO_a28_DATAB_driver);

LO_a28_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_lo_a1_a_ainput_o,
	dataout => LO_a28_DATAC_driver);

LO_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(26),
	dataout => LO_a28_DATAD_driver);

-- Location: LCCOMB_X35_Y28_N24
LO_a28 : cycloneive_lcell_comb
-- Equation(s):
-- LO_a28_combout = (mux_lo_a1_a_ainput_o & (data_in_a27_a_ainput_o)) # (!mux_lo_a1_a_ainput_o & ((LO(26))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => LO_a28_DATAB_driver,
	datac => LO_a28_DATAC_driver,
	datad => LO_a28_DATAD_driver,
	combout => LO_a28_combout);

LO_a27_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => LO_a27_a_CLK_driver);

LO_a27_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a28_combout,
	dataout => LO_a27_a_D_driver);

LO_a27_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => LO_a27_a_SCLR_driver);

LO_a27_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO_a0_a_a1_combout,
	dataout => LO_a27_a_ENA_driver);

-- Location: FF_X35_Y28_N25
LO_a27_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => LO_a27_a_CLK_driver,
	d => LO_a27_a_D_driver,
	sclr => LO_a27_a_SCLR_driver,
	ena => LO_a27_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => LO(27));

data_out_a82_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a82_DATAA_driver);

data_out_a82_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a82_DATAB_driver);

data_out_a82_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(27),
	dataout => data_out_a82_DATAC_driver);

data_out_a82_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(27),
	dataout => data_out_a82_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N0
data_out_a82 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a82_combout = (mux_data_out_a1_a_ainput_o & ((mux_data_out_a0_a_ainput_o) # ((HI(27))))) # (!mux_data_out_a1_a_ainput_o & (!mux_data_out_a0_a_ainput_o & (O(27))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a82_DATAA_driver,
	datab => data_out_a82_DATAB_driver,
	datac => data_out_a82_DATAC_driver,
	datad => data_out_a82_DATAD_driver,
	combout => data_out_a82_combout);

data_out_a83_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(27),
	dataout => data_out_a83_DATAA_driver);

data_out_a83_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(27),
	dataout => data_out_a83_DATAB_driver);

data_out_a83_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a83_DATAC_driver);

data_out_a83_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a82_combout,
	dataout => data_out_a83_DATAD_driver);

-- Location: LCCOMB_X36_Y28_N4
data_out_a83 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a83_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a82_combout & ((LO(27)))) # (!data_out_a82_combout & (A(27))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a82_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a83_DATAA_driver,
	datab => data_out_a83_DATAB_driver,
	datac => data_out_a83_DATAC_driver,
	datad => data_out_a83_DATAD_driver,
	combout => data_out_a83_combout);

data_out_a84_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(27),
	dataout => data_out_a84_DATAA_driver);

data_out_a84_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a84_DATAB_driver);

data_out_a84_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a84_DATAC_driver);

data_out_a84_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a83_combout,
	dataout => data_out_a84_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N26
data_out_a84 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a84_combout = (PC(27) & ((data_out_a0_combout) # ((!mux_data_out_a2_a_ainput_o & data_out_a83_combout)))) # (!PC(27) & (((!mux_data_out_a2_a_ainput_o & data_out_a83_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a84_DATAA_driver,
	datab => data_out_a84_DATAB_driver,
	datac => data_out_a84_DATAC_driver,
	datad => data_out_a84_DATAD_driver,
	combout => data_out_a84_combout);

data_out_a86_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a85_combout,
	dataout => data_out_a86_DATAA_driver);

data_out_a86_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(28),
	dataout => data_out_a86_DATAB_driver);

data_out_a86_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(28),
	dataout => data_out_a86_DATAC_driver);

data_out_a86_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a86_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N22
data_out_a86 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a86_combout = (data_out_a85_combout & ((LO(28)) # ((!mux_data_out_a1_a_ainput_o)))) # (!data_out_a85_combout & (((HI(28) & mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a86_DATAA_driver,
	datab => data_out_a86_DATAB_driver,
	datac => data_out_a86_DATAC_driver,
	datad => data_out_a86_DATAD_driver,
	combout => data_out_a86_combout);

data_out_a87_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a86_combout,
	dataout => data_out_a87_DATAA_driver);

data_out_a87_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a87_DATAB_driver);

data_out_a87_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(28),
	dataout => data_out_a87_DATAC_driver);

data_out_a87_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a87_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N0
data_out_a87 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a87_combout = (data_out_a86_combout & (((data_out_a0_combout & PC(28))) # (!mux_data_out_a2_a_ainput_o))) # (!data_out_a86_combout & (data_out_a0_combout & (PC(28))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a87_DATAA_driver,
	datab => data_out_a87_DATAB_driver,
	datac => data_out_a87_DATAC_driver,
	datad => data_out_a87_DATAD_driver,
	combout => data_out_a87_combout);

A_a29_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a29_a_CLK_driver);

A_a29_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a30_combout,
	dataout => A_a29_a_ASDATA_driver);

A_a29_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a29_a_ENA_driver);

-- Location: FF_X39_Y27_N5
A_a29_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a29_a_CLK_driver,
	asdata => A_a29_a_ASDATA_driver,
	sload => VCC,
	ena => A_a29_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(29));

HI_a31_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(28),
	dataout => HI_a31_DATAA_driver);

HI_a31_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a0_combout,
	dataout => HI_a31_DATAB_driver);

HI_a31_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a60_combout,
	dataout => HI_a31_DATAC_driver);

HI_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => HI_a31_DATAD_driver);

-- Location: LCCOMB_X37_Y27_N18
HI_a31 : cycloneive_lcell_comb
-- Equation(s):
-- HI_a31_combout = (!HI_a14_a_a0_combout & ((Add0_a66_combout & ((Add0_a60_combout))) # (!Add0_a66_combout & (HI(28)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => HI_a31_DATAA_driver,
	datab => HI_a31_DATAB_driver,
	datac => HI_a31_DATAC_driver,
	datad => HI_a31_DATAD_driver,
	combout => HI_a31_combout);

HI_a29_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => HI_a29_a_CLK_driver);

HI_a29_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a31_combout,
	dataout => HI_a29_a_D_driver);

HI_a29_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI_a14_a_a2_combout,
	dataout => HI_a29_a_ENA_driver);

-- Location: FF_X37_Y27_N19
HI_a29_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => HI_a29_a_CLK_driver,
	d => HI_a29_a_D_driver,
	ena => HI_a29_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => HI(29));

data_out_a88_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a88_DATAA_driver);

data_out_a88_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(29),
	dataout => data_out_a88_DATAB_driver);

data_out_a88_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(29),
	dataout => data_out_a88_DATAC_driver);

data_out_a88_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a88_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N6
data_out_a88 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a88_combout = (mux_data_out_a0_a_ainput_o & (((mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & ((mux_data_out_a1_a_ainput_o & (HI(29))) # (!mux_data_out_a1_a_ainput_o & ((O(29))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a88_DATAA_driver,
	datab => data_out_a88_DATAB_driver,
	datac => data_out_a88_DATAC_driver,
	datad => data_out_a88_DATAD_driver,
	combout => data_out_a88_combout);

data_out_a89_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(29),
	dataout => data_out_a89_DATAA_driver);

data_out_a89_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a89_DATAB_driver);

data_out_a89_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(29),
	dataout => data_out_a89_DATAC_driver);

data_out_a89_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a88_combout,
	dataout => data_out_a89_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N4
data_out_a89 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a89_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a88_combout & (LO(29))) # (!data_out_a88_combout & ((A(29)))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a88_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a89_DATAA_driver,
	datab => data_out_a89_DATAB_driver,
	datac => data_out_a89_DATAC_driver,
	datad => data_out_a89_DATAD_driver,
	combout => data_out_a89_combout);

PC_a31_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a29_a_ainput_o,
	dataout => PC_a31_DATAB_driver);

PC_a31_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a31_DATAC_driver);

PC_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a60_combout,
	dataout => PC_a31_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N12
PC_a31 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a31_combout = (mux_pc_a1_a_ainput_o & (data_in_a29_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a60_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => PC_a31_DATAB_driver,
	datac => PC_a31_DATAC_driver,
	datad => PC_a31_DATAD_driver,
	combout => PC_a31_combout);

PC_a29_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a29_a_CLK_driver);

PC_a29_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a31_combout,
	dataout => PC_a29_a_D_driver);

PC_a29_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a29_a_SCLR_driver);

PC_a29_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a29_a_a30_combout,
	dataout => PC_a29_a_ENA_driver);

-- Location: FF_X35_Y27_N13
PC_a29_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a29_a_CLK_driver,
	d => PC_a29_a_D_driver,
	sclr => PC_a29_a_SCLR_driver,
	ena => PC_a29_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(29));

data_out_a90_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a90_DATAA_driver);

data_out_a90_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a89_combout,
	dataout => data_out_a90_DATAB_driver);

data_out_a90_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(29),
	dataout => data_out_a90_DATAC_driver);

data_out_a90_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a90_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N20
data_out_a90 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a90_combout = (data_out_a0_combout & ((PC(29)) # ((data_out_a89_combout & !mux_data_out_a2_a_ainput_o)))) # (!data_out_a0_combout & (data_out_a89_combout & ((!mux_data_out_a2_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a90_DATAA_driver,
	datab => data_out_a90_DATAB_driver,
	datac => data_out_a90_DATAC_driver,
	datad => data_out_a90_DATAD_driver,
	combout => data_out_a90_combout);

A_a31_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a30_a_ainput_o,
	dataout => A_a31_DATAC_driver);

A_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => A_a31_DATAD_driver);

-- Location: LCCOMB_X36_Y25_N8
A_a31 : cycloneive_lcell_comb
-- Equation(s):
-- A_a31_combout = (data_in_a30_a_ainput_o & !reset_ainput_o)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => A_a31_DATAC_driver,
	datad => A_a31_DATAD_driver,
	combout => A_a31_combout);

A_a30_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a30_a_CLK_driver);

A_a30_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a31_combout,
	dataout => A_a30_a_ASDATA_driver);

A_a30_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a30_a_ENA_driver);

-- Location: FF_X39_Y27_N31
A_a30_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a30_a_CLK_driver,
	asdata => A_a30_a_ASDATA_driver,
	sload => VCC,
	ena => A_a30_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(30));

data_out_a91_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(30),
	dataout => data_out_a91_DATAA_driver);

data_out_a91_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a91_DATAB_driver);

data_out_a91_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(30),
	dataout => data_out_a91_DATAC_driver);

data_out_a91_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a91_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N30
data_out_a91 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a91_combout = (mux_data_out_a0_a_ainput_o & (((A(30)) # (mux_data_out_a1_a_ainput_o)))) # (!mux_data_out_a0_a_ainput_o & (O(30) & ((!mux_data_out_a1_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a91_DATAA_driver,
	datab => data_out_a91_DATAB_driver,
	datac => data_out_a91_DATAC_driver,
	datad => data_out_a91_DATAD_driver,
	combout => data_out_a91_combout);

data_out_a92_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a92_DATAA_driver);

data_out_a92_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a91_combout,
	dataout => data_out_a92_DATAB_driver);

data_out_a92_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(30),
	dataout => data_out_a92_DATAC_driver);

data_out_a92_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(30),
	dataout => data_out_a92_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N12
data_out_a92 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a92_combout = (mux_data_out_a1_a_ainput_o & ((data_out_a91_combout & (LO(30))) # (!data_out_a91_combout & ((HI(30)))))) # (!mux_data_out_a1_a_ainput_o & (data_out_a91_combout))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a92_DATAA_driver,
	datab => data_out_a92_DATAB_driver,
	datac => data_out_a92_DATAC_driver,
	datad => data_out_a92_DATAD_driver,
	combout => data_out_a92_combout);

data_out_a93_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a93_DATAA_driver);

data_out_a93_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a92_combout,
	dataout => data_out_a93_DATAB_driver);

data_out_a93_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(30),
	dataout => data_out_a93_DATAC_driver);

data_out_a93_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a93_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N10
data_out_a93 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a93_combout = (data_out_a0_combout & ((PC(30)) # ((data_out_a92_combout & !mux_data_out_a2_a_ainput_o)))) # (!data_out_a0_combout & (data_out_a92_combout & ((!mux_data_out_a2_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a93_DATAA_driver,
	datab => data_out_a93_DATAB_driver,
	datac => data_out_a93_DATAC_driver,
	datad => data_out_a93_DATAD_driver,
	combout => data_out_a93_combout);

data_out_a94_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(31),
	dataout => data_out_a94_DATAA_driver);

data_out_a94_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a1_a_ainput_o,
	dataout => data_out_a94_DATAB_driver);

data_out_a94_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a94_DATAC_driver);

data_out_a94_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => HI(31),
	dataout => data_out_a94_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N10
data_out_a94 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a94_combout = (mux_data_out_a1_a_ainput_o & (((mux_data_out_a0_a_ainput_o) # (HI(31))))) # (!mux_data_out_a1_a_ainput_o & (O(31) & (!mux_data_out_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a94_DATAA_driver,
	datab => data_out_a94_DATAB_driver,
	datac => data_out_a94_DATAC_driver,
	datad => data_out_a94_DATAD_driver,
	combout => data_out_a94_combout);

data_out_a95_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => LO(31),
	dataout => data_out_a95_DATAA_driver);

data_out_a95_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a0_a_ainput_o,
	dataout => data_out_a95_DATAB_driver);

data_out_a95_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(31),
	dataout => data_out_a95_DATAC_driver);

data_out_a95_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a94_combout,
	dataout => data_out_a95_DATAD_driver);

-- Location: LCCOMB_X39_Y27_N8
data_out_a95 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a95_combout = (mux_data_out_a0_a_ainput_o & ((data_out_a94_combout & (LO(31))) # (!data_out_a94_combout & ((A(31)))))) # (!mux_data_out_a0_a_ainput_o & (((data_out_a94_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a95_DATAA_driver,
	datab => data_out_a95_DATAB_driver,
	datac => data_out_a95_DATAC_driver,
	datad => data_out_a95_DATAD_driver,
	combout => data_out_a95_combout);

PC_a33_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_pc_a1_a_ainput_o,
	dataout => PC_a33_DATAA_driver);

PC_a33_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a31_a_ainput_o,
	dataout => PC_a33_DATAC_driver);

PC_a33_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a64_combout,
	dataout => PC_a33_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N20
PC_a33 : cycloneive_lcell_comb
-- Equation(s):
-- PC_a33_combout = (mux_pc_a1_a_ainput_o & (data_in_a31_a_ainput_o)) # (!mux_pc_a1_a_ainput_o & ((Add0_a64_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => PC_a33_DATAA_driver,
	datac => PC_a33_DATAC_driver,
	datad => PC_a33_DATAD_driver,
	combout => PC_a33_combout);

PC_a31_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => PC_a31_a_CLK_driver);

PC_a31_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a33_combout,
	dataout => PC_a31_a_D_driver);

PC_a31_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => PC_a31_a_SCLR_driver);

PC_a31_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC_a29_a_a30_combout,
	dataout => PC_a31_a_ENA_driver);

-- Location: FF_X35_Y27_N21
PC_a31_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => PC_a31_a_CLK_driver,
	d => PC_a31_a_D_driver,
	sclr => PC_a31_a_SCLR_driver,
	ena => PC_a31_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => PC(31));

data_out_a96_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_data_out_a2_a_ainput_o,
	dataout => data_out_a96_DATAA_driver);

data_out_a96_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a0_combout,
	dataout => data_out_a96_DATAB_driver);

data_out_a96_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_out_a95_combout,
	dataout => data_out_a96_DATAC_driver);

data_out_a96_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(31),
	dataout => data_out_a96_DATAD_driver);

-- Location: LCCOMB_X35_Y27_N24
data_out_a96 : cycloneive_lcell_comb
-- Equation(s):
-- data_out_a96_combout = (mux_data_out_a2_a_ainput_o & (data_out_a0_combout & ((PC(31))))) # (!mux_data_out_a2_a_ainput_o & ((data_out_a95_combout) # ((data_out_a0_combout & PC(31)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => data_out_a96_DATAA_driver,
	datab => data_out_a96_DATAB_driver,
	datac => data_out_a96_DATAC_driver,
	datad => data_out_a96_DATAD_driver,
	combout => data_out_a96_combout);

mux_address_a1_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_address(1),
	dataout => mux_address_a1_a_ainput_I_driver);

-- Location: IOIBUF_X45_Y43_N29
mux_address_a1_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_address_a1_a_ainput_I_driver,
	o => mux_address_a1_a_ainput_o);

mux_address_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_mux_address(0),
	dataout => mux_address_a0_a_ainput_I_driver);

-- Location: IOIBUF_X41_Y43_N15
mux_address_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => mux_address_a0_a_ainput_I_driver,
	o => mux_address_a0_a_ainput_o);

O_n_a0_a_a0_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(1),
	dataout => O_n_a0_a_a0_DATAA_driver);

O_n_a0_a_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_n_a0_a_a0_DATAB_driver);

O_n_a0_a_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_n_a0_a_a0_DATAC_driver);

O_n_a0_a_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a66_combout,
	dataout => O_n_a0_a_a0_DATAD_driver);

-- Location: LCCOMB_X37_Y27_N24
O_n_a0_a_a0 : cycloneive_lcell_comb
-- Equation(s):
-- O_n_a0_a_a0_combout = (mux_out_a1_a_ainput_o & ((mux_out_a0_a_ainput_o & ((!Add0_a66_combout))) # (!mux_out_a0_a_ainput_o & (O(1))))) # (!mux_out_a1_a_ainput_o & (((mux_out_a0_a_ainput_o))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_n_a0_a_a0_DATAA_driver,
	datab => O_n_a0_a_a0_DATAB_driver,
	datac => O_n_a0_a_a0_DATAC_driver,
	datad => O_n_a0_a_a0_DATAD_driver,
	combout => O_n_a0_a_a0_combout);

O_n_a0_a_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(0),
	dataout => O_n_a0_a_a1_DATAA_driver);

O_n_a0_a_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_n_a0_a_a1_DATAB_driver);

O_n_a0_a_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a2_a_ainput_o,
	dataout => O_n_a0_a_a1_DATAC_driver);

O_n_a0_a_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a0_a_a0_combout,
	dataout => O_n_a0_a_a1_DATAD_driver);

-- Location: LCCOMB_X37_Y27_N26
O_n_a0_a_a1 : cycloneive_lcell_comb
-- Equation(s):
-- O_n_a0_a_a1_combout = (O(0) & ((O_n_a0_a_a0_combout) # (mux_out_a1_a_ainput_o $ (!mux_out_a2_a_ainput_o)))) # (!O(0) & (((!mux_out_a2_a_ainput_o & O_n_a0_a_a0_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_n_a0_a_a1_DATAA_driver,
	datab => O_n_a0_a_a1_DATAB_driver,
	datac => O_n_a0_a_a1_DATAC_driver,
	datad => O_n_a0_a_a1_DATAD_driver,
	combout => O_n_a0_a_a1_combout);

O_n_a0_a_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_n_a0_a_a2_DATAB_driver);

O_n_a0_a_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a2_a_ainput_o,
	dataout => O_n_a0_a_a2_DATAC_driver);

O_n_a0_a_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a0_a_a0_combout,
	dataout => O_n_a0_a_a2_DATAD_driver);

-- Location: LCCOMB_X37_Y27_N28
O_n_a0_a_a2 : cycloneive_lcell_comb
-- Equation(s):
-- O_n_a0_a_a2_combout = (!mux_out_a1_a_ainput_o & (mux_out_a2_a_ainput_o $ (O_n_a0_a_a0_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => O_n_a0_a_a2_DATAB_driver,
	datac => O_n_a0_a_a2_DATAC_driver,
	datad => O_n_a0_a_a2_DATAD_driver,
	combout => O_n_a0_a_a2_combout);

O_n_a0_a_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a2_combout,
	dataout => O_n_a0_a_a3_DATAA_driver);

O_n_a0_a_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a0_a_ainput_o,
	dataout => O_n_a0_a_a3_DATAB_driver);

O_n_a0_a_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a0_a_a1_combout,
	dataout => O_n_a0_a_a3_DATAC_driver);

O_n_a0_a_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a0_a_a2_combout,
	dataout => O_n_a0_a_a3_DATAD_driver);

-- Location: LCCOMB_X37_Y27_N22
O_n_a0_a_a3 : cycloneive_lcell_comb
-- Equation(s):
-- O_n_a0_a_a3_combout = (O_n_a0_a_a1_combout & ((Add0_a2_combout) # ((!O_n_a0_a_a2_combout)))) # (!O_n_a0_a_a1_combout & (((data_in_a0_a_ainput_o & O_n_a0_a_a2_combout))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_n_a0_a_a3_DATAA_driver,
	datab => O_n_a0_a_a3_DATAB_driver,
	datac => O_n_a0_a_a3_DATAC_driver,
	datad => O_n_a0_a_a3_DATAD_driver,
	combout => O_n_a0_a_a3_combout);

O_a0_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a0_a_CLK_driver);

O_a0_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_n_a0_a_a3_combout,
	dataout => O_a0_a_D_driver);

O_a0_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => reset_ainput_o,
	dataout => O_a0_a_SCLR_driver);

-- Location: FF_X37_Y27_N23
O_a0_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a0_a_CLK_driver,
	d => O_a0_a_D_driver,
	sclr => O_a0_a_SCLR_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(0));

address_reg_a0_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(0),
	dataout => address_reg_a0_a_ainput_I_driver);

-- Location: IOIBUF_X63_Y43_N22
address_reg_a0_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a0_a_ainput_I_driver,
	o => address_reg_a0_a_ainput_o);

address_a0_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a0_DATAA_driver);

address_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(0),
	dataout => address_a0_DATAB_driver);

address_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a0_DATAC_driver);

address_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a0_a_ainput_o,
	dataout => address_a0_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N20
address_a0 : cycloneive_lcell_comb
-- Equation(s):
-- address_a0_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a0_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a0_DATAA_driver,
	datab => address_a0_DATAB_driver,
	datac => address_a0_DATAC_driver,
	datad => address_a0_DATAD_driver,
	combout => address_a0_combout);

address_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a1_DATAA_driver);

address_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a1_DATAB_driver);

address_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(0),
	dataout => address_a1_DATAC_driver);

address_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a0_combout,
	dataout => address_a1_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N18
address_a1 : cycloneive_lcell_comb
-- Equation(s):
-- address_a1_combout = (address_a0_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a1_DATAA_driver,
	datab => address_a1_DATAB_driver,
	datac => address_a1_DATAC_driver,
	datad => address_a1_DATAD_driver,
	combout => address_a1_combout);

O_a1_a_a0_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a0_a_ainput_o,
	dataout => O_a1_a_a0_DATAA_driver);

O_a1_a_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => data_in_a1_a_ainput_o,
	dataout => O_a1_a_a0_DATAB_driver);

O_a1_a_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Add0_a4_combout,
	dataout => O_a1_a_a0_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N24
O_a1_a_a0 : cycloneive_lcell_comb
-- Equation(s):
-- O_a1_a_a0_combout = (mux_out_a0_a_ainput_o & ((Add0_a4_combout))) # (!mux_out_a0_a_ainput_o & (data_in_a1_a_ainput_o))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => O_a1_a_a0_DATAA_driver,
	datab => O_a1_a_a0_DATAB_driver,
	datad => O_a1_a_a0_DATAD_driver,
	combout => O_a1_a_a0_combout);

O_a1_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => O_a1_a_CLK_driver);

O_a1_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a1_a_a0_combout,
	dataout => O_a1_a_D_driver);

O_a1_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(2),
	dataout => O_a1_a_ASDATA_driver);

O_a1_a_SCLR_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a30_combout,
	dataout => O_a1_a_SCLR_driver);

O_a1_a_SLOAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_out_a1_a_ainput_o,
	dataout => O_a1_a_SLOAD_driver);

O_a1_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O_a27_a_a31_combout,
	dataout => O_a1_a_ENA_driver);

-- Location: FF_X36_Y29_N25
O_a1_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => O_a1_a_CLK_driver,
	d => O_a1_a_D_driver,
	asdata => O_a1_a_ASDATA_driver,
	sclr => O_a1_a_SCLR_driver,
	sload => O_a1_a_SLOAD_driver,
	ena => O_a1_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => O(1));

address_a2_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a1_a_ainput_o,
	dataout => address_a2_DATAA_driver);

address_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a2_DATAB_driver);

address_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(1),
	dataout => address_a2_DATAC_driver);

address_a2_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a2_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N26
address_a2 : cycloneive_lcell_comb
-- Equation(s):
-- address_a2_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a1_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a2_DATAA_driver,
	datab => address_a2_DATAB_driver,
	datac => address_a2_DATAC_driver,
	datad => address_a2_DATAD_driver,
	combout => address_a2_combout);

address_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a3_DATAA_driver);

address_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(1),
	dataout => address_a3_DATAB_driver);

address_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a2_combout,
	dataout => address_a3_DATAC_driver);

address_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a3_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N12
address_a3 : cycloneive_lcell_comb
-- Equation(s):
-- address_a3_combout = (address_a2_combout) # ((!mux_address_a1_a_ainput_o & (O(1) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a3_DATAA_driver,
	datab => address_a3_DATAB_driver,
	datac => address_a3_DATAC_driver,
	datad => address_a3_DATAD_driver,
	combout => address_a3_combout);

address_reg_a2_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(2),
	dataout => address_reg_a2_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y29_N22
address_reg_a2_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a2_a_ainput_I_driver,
	o => address_reg_a2_a_ainput_o);

address_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(2),
	dataout => address_a4_DATAA_driver);

address_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a2_a_ainput_o,
	dataout => address_a4_DATAB_driver);

address_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a4_DATAC_driver);

address_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a4_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N12
address_a4 : cycloneive_lcell_comb
-- Equation(s):
-- address_a4_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a2_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a4_DATAA_driver,
	datab => address_a4_DATAB_driver,
	datac => address_a4_DATAC_driver,
	datad => address_a4_DATAD_driver,
	combout => address_a4_combout);

address_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a4_combout,
	dataout => address_a5_DATAA_driver);

address_a5_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(2),
	dataout => address_a5_DATAB_driver);

address_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a5_DATAC_driver);

address_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a5_DATAD_driver);

-- Location: LCCOMB_X36_Y29_N28
address_a5 : cycloneive_lcell_comb
-- Equation(s):
-- address_a5_combout = (address_a4_combout) # ((O(2) & (!mux_address_a1_a_ainput_o & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a5_DATAA_driver,
	datab => address_a5_DATAB_driver,
	datac => address_a5_DATAC_driver,
	datad => address_a5_DATAD_driver,
	combout => address_a5_combout);

address_reg_a3_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(3),
	dataout => address_reg_a3_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y37_N15
address_reg_a3_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a3_a_ainput_I_driver,
	o => address_reg_a3_a_ainput_o);

address_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a6_DATAA_driver);

address_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a6_DATAB_driver);

address_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(3),
	dataout => address_a6_DATAC_driver);

address_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a3_a_ainput_o,
	dataout => address_a6_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N30
address_a6 : cycloneive_lcell_comb
-- Equation(s):
-- address_a6_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a3_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a6_DATAA_driver,
	datab => address_a6_DATAB_driver,
	datac => address_a6_DATAC_driver,
	datad => address_a6_DATAD_driver,
	combout => address_a6_combout);

address_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a7_DATAA_driver);

address_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(3),
	dataout => address_a7_DATAB_driver);

address_a7_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a6_combout,
	dataout => address_a7_DATAC_driver);

address_a7_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a7_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N16
address_a7 : cycloneive_lcell_comb
-- Equation(s):
-- address_a7_combout = (address_a6_combout) # ((!mux_address_a1_a_ainput_o & (O(3) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a7_DATAA_driver,
	datab => address_a7_DATAB_driver,
	datac => address_a7_DATAC_driver,
	datad => address_a7_DATAD_driver,
	combout => address_a7_combout);

address_a8_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a4_a_ainput_o,
	dataout => address_a8_DATAA_driver);

address_a8_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a8_DATAB_driver);

address_a8_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(4),
	dataout => address_a8_DATAC_driver);

address_a8_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a8_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N0
address_a8 : cycloneive_lcell_comb
-- Equation(s):
-- address_a8_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a4_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a8_DATAA_driver,
	datab => address_a8_DATAB_driver,
	datac => address_a8_DATAC_driver,
	datad => address_a8_DATAD_driver,
	combout => address_a8_combout);

address_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a9_DATAA_driver);

address_a9_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a8_combout,
	dataout => address_a9_DATAB_driver);

address_a9_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a9_DATAC_driver);

address_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(4),
	dataout => address_a9_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N22
address_a9 : cycloneive_lcell_comb
-- Equation(s):
-- address_a9_combout = (address_a8_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a9_DATAA_driver,
	datab => address_a9_DATAB_driver,
	datac => address_a9_DATAC_driver,
	datad => address_a9_DATAD_driver,
	combout => address_a9_combout);

address_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a5_a_ainput_o,
	dataout => address_a10_DATAA_driver);

address_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a10_DATAB_driver);

address_a10_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(5),
	dataout => address_a10_DATAC_driver);

address_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a10_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N2
address_a10 : cycloneive_lcell_comb
-- Equation(s):
-- address_a10_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a5_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(5))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a10_DATAA_driver,
	datab => address_a10_DATAB_driver,
	datac => address_a10_DATAC_driver,
	datad => address_a10_DATAD_driver,
	combout => address_a10_combout);

address_a11_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a11_DATAA_driver);

address_a11_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a10_combout,
	dataout => address_a11_DATAB_driver);

address_a11_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(5),
	dataout => address_a11_DATAC_driver);

address_a11_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a11_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N8
address_a11 : cycloneive_lcell_comb
-- Equation(s):
-- address_a11_combout = (address_a10_combout) # ((!mux_address_a1_a_ainput_o & (O(5) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a11_DATAA_driver,
	datab => address_a11_DATAB_driver,
	datac => address_a11_DATAC_driver,
	datad => address_a11_DATAD_driver,
	combout => address_a11_combout);

address_reg_a6_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(6),
	dataout => address_reg_a6_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y30_N1
address_reg_a6_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a6_a_ainput_I_driver,
	o => address_reg_a6_a_ainput_o);

address_a12_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a12_DATAA_driver);

address_a12_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a12_DATAB_driver);

address_a12_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a6_a_ainput_o,
	dataout => address_a12_DATAC_driver);

address_a12_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(6),
	dataout => address_a12_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N14
address_a12 : cycloneive_lcell_comb
-- Equation(s):
-- address_a12_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a6_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(6))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a12_DATAA_driver,
	datab => address_a12_DATAB_driver,
	datac => address_a12_DATAC_driver,
	datad => address_a12_DATAD_driver,
	combout => address_a12_combout);

address_a13_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a13_DATAA_driver);

address_a13_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a13_DATAB_driver);

address_a13_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a12_combout,
	dataout => address_a13_DATAC_driver);

address_a13_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(6),
	dataout => address_a13_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N20
address_a13 : cycloneive_lcell_comb
-- Equation(s):
-- address_a13_combout = (address_a12_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a13_DATAA_driver,
	datab => address_a13_DATAB_driver,
	datac => address_a13_DATAC_driver,
	datad => address_a13_DATAD_driver,
	combout => address_a13_combout);

address_reg_a7_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(7),
	dataout => address_reg_a7_a_ainput_I_driver);

-- Location: IOIBUF_X7_Y43_N8
address_reg_a7_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a7_a_ainput_I_driver,
	o => address_reg_a7_a_ainput_o);

address_a14_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a14_DATAA_driver);

address_a14_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a7_a_ainput_o,
	dataout => address_a14_DATAB_driver);

address_a14_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(7),
	dataout => address_a14_DATAC_driver);

address_a14_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a14_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N10
address_a14 : cycloneive_lcell_comb
-- Equation(s):
-- address_a14_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a7_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(7))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a14_DATAA_driver,
	datab => address_a14_DATAB_driver,
	datac => address_a14_DATAC_driver,
	datad => address_a14_DATAD_driver,
	combout => address_a14_combout);

address_a15_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a14_combout,
	dataout => address_a15_DATAA_driver);

address_a15_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a15_DATAB_driver);

address_a15_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(7),
	dataout => address_a15_DATAC_driver);

address_a15_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a15_DATAD_driver);

-- Location: LCCOMB_X34_Y30_N4
address_a15 : cycloneive_lcell_comb
-- Equation(s):
-- address_a15_combout = (address_a14_combout) # ((mux_address_a0_a_ainput_o & (O(7) & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a15_DATAA_driver,
	datab => address_a15_DATAB_driver,
	datac => address_a15_DATAC_driver,
	datad => address_a15_DATAD_driver,
	combout => address_a15_combout);

address_reg_a8_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(8),
	dataout => address_reg_a8_a_ainput_I_driver);

-- Location: IOIBUF_X7_Y43_N22
address_reg_a8_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a8_a_ainput_I_driver,
	o => address_reg_a8_a_ainput_o);

address_a16_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(8),
	dataout => address_a16_DATAA_driver);

address_a16_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a8_a_ainput_o,
	dataout => address_a16_DATAB_driver);

address_a16_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a16_DATAC_driver);

address_a16_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a16_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N12
address_a16 : cycloneive_lcell_comb
-- Equation(s):
-- address_a16_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a8_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a16_DATAA_driver,
	datab => address_a16_DATAB_driver,
	datac => address_a16_DATAC_driver,
	datad => address_a16_DATAD_driver,
	combout => address_a16_combout);

address_a17_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a16_combout,
	dataout => address_a17_DATAA_driver);

address_a17_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(8),
	dataout => address_a17_DATAB_driver);

address_a17_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a17_DATAC_driver);

address_a17_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a17_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N2
address_a17 : cycloneive_lcell_comb
-- Equation(s):
-- address_a17_combout = (address_a16_combout) # ((O(8) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a17_DATAA_driver,
	datab => address_a17_DATAB_driver,
	datac => address_a17_DATAC_driver,
	datad => address_a17_DATAD_driver,
	combout => address_a17_combout);

address_a18_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a9_a_ainput_o,
	dataout => address_a18_DATAA_driver);

address_a18_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(9),
	dataout => address_a18_DATAB_driver);

address_a18_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a18_DATAC_driver);

address_a18_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a18_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N20
address_a18 : cycloneive_lcell_comb
-- Equation(s):
-- address_a18_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a9_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(9))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a18_DATAA_driver,
	datab => address_a18_DATAB_driver,
	datac => address_a18_DATAC_driver,
	datad => address_a18_DATAD_driver,
	combout => address_a18_combout);

address_a19_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(9),
	dataout => address_a19_DATAA_driver);

address_a19_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a19_DATAB_driver);

address_a19_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a19_DATAC_driver);

address_a19_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a18_combout,
	dataout => address_a19_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N10
address_a19 : cycloneive_lcell_comb
-- Equation(s):
-- address_a19_combout = (address_a18_combout) # ((O(9) & (!mux_address_a1_a_ainput_o & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a19_DATAA_driver,
	datab => address_a19_DATAB_driver,
	datac => address_a19_DATAC_driver,
	datad => address_a19_DATAD_driver,
	combout => address_a19_combout);

address_reg_a10_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(10),
	dataout => address_reg_a10_a_ainput_I_driver);

-- Location: IOIBUF_X43_Y0_N1
address_reg_a10_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a10_a_ainput_I_driver,
	o => address_reg_a10_a_ainput_o);

address_a20_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a20_DATAA_driver);

address_a20_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(10),
	dataout => address_a20_DATAB_driver);

address_a20_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a10_a_ainput_o,
	dataout => address_a20_DATAC_driver);

address_a20_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a20_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N4
address_a20 : cycloneive_lcell_comb
-- Equation(s):
-- address_a20_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a10_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a20_DATAA_driver,
	datab => address_a20_DATAB_driver,
	datac => address_a20_DATAC_driver,
	datad => address_a20_DATAD_driver,
	combout => address_a20_combout);

address_a21_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a21_DATAA_driver);

address_a21_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a21_DATAB_driver);

address_a21_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a20_combout,
	dataout => address_a21_DATAC_driver);

address_a21_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(10),
	dataout => address_a21_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N2
address_a21 : cycloneive_lcell_comb
-- Equation(s):
-- address_a21_combout = (address_a20_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a21_DATAA_driver,
	datab => address_a21_DATAB_driver,
	datac => address_a21_DATAC_driver,
	datad => address_a21_DATAD_driver,
	combout => address_a21_combout);

address_reg_a11_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(11),
	dataout => address_reg_a11_a_ainput_I_driver);

-- Location: IOIBUF_X43_Y0_N15
address_reg_a11_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a11_a_ainput_I_driver,
	o => address_reg_a11_a_ainput_o);

address_a22_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a22_DATAA_driver);

address_a22_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a11_a_ainput_o,
	dataout => address_a22_DATAB_driver);

address_a22_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(11),
	dataout => address_a22_DATAC_driver);

address_a22_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a22_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N16
address_a22 : cycloneive_lcell_comb
-- Equation(s):
-- address_a22_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a11_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(11))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a22_DATAA_driver,
	datab => address_a22_DATAB_driver,
	datac => address_a22_DATAC_driver,
	datad => address_a22_DATAD_driver,
	combout => address_a22_combout);

address_a23_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a23_DATAA_driver);

address_a23_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a23_DATAB_driver);

address_a23_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(11),
	dataout => address_a23_DATAC_driver);

address_a23_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a22_combout,
	dataout => address_a23_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N14
address_a23 : cycloneive_lcell_comb
-- Equation(s):
-- address_a23_combout = (address_a22_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a23_DATAA_driver,
	datab => address_a23_DATAB_driver,
	datac => address_a23_DATAC_driver,
	datad => address_a23_DATAD_driver,
	combout => address_a23_combout);

address_reg_a12_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(12),
	dataout => address_reg_a12_a_ainput_I_driver);

-- Location: IOIBUF_X48_Y43_N8
address_reg_a12_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a12_a_ainput_I_driver,
	o => address_reg_a12_a_ainput_o);

address_a24_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a24_DATAA_driver);

address_a24_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a24_DATAB_driver);

address_a24_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(12),
	dataout => address_a24_DATAC_driver);

address_a24_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a12_a_ainput_o,
	dataout => address_a24_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N28
address_a24 : cycloneive_lcell_comb
-- Equation(s):
-- address_a24_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a12_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a24_DATAA_driver,
	datab => address_a24_DATAB_driver,
	datac => address_a24_DATAC_driver,
	datad => address_a24_DATAD_driver,
	combout => address_a24_combout);

address_a25_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a25_DATAA_driver);

address_a25_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a25_DATAB_driver);

address_a25_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(12),
	dataout => address_a25_DATAC_driver);

address_a25_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a24_combout,
	dataout => address_a25_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N6
address_a25 : cycloneive_lcell_comb
-- Equation(s):
-- address_a25_combout = (address_a24_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a25_DATAA_driver,
	datab => address_a25_DATAB_driver,
	datac => address_a25_DATAC_driver,
	datad => address_a25_DATAD_driver,
	combout => address_a25_combout);

address_reg_a13_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(13),
	dataout => address_reg_a13_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y18_N8
address_reg_a13_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a13_a_ainput_I_driver,
	o => address_reg_a13_a_ainput_o);

address_a26_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a26_DATAA_driver);

address_a26_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a26_DATAB_driver);

address_a26_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(13),
	dataout => address_a26_DATAC_driver);

address_a26_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a13_a_ainput_o,
	dataout => address_a26_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N20
address_a26 : cycloneive_lcell_comb
-- Equation(s):
-- address_a26_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a13_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a26_DATAA_driver,
	datab => address_a26_DATAB_driver,
	datac => address_a26_DATAC_driver,
	datad => address_a26_DATAD_driver,
	combout => address_a26_combout);

address_a27_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a27_DATAA_driver);

address_a27_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a27_DATAB_driver);

address_a27_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(13),
	dataout => address_a27_DATAC_driver);

address_a27_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a26_combout,
	dataout => address_a27_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N30
address_a27 : cycloneive_lcell_comb
-- Equation(s):
-- address_a27_combout = (address_a26_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a27_DATAA_driver,
	datab => address_a27_DATAB_driver,
	datac => address_a27_DATAC_driver,
	datad => address_a27_DATAD_driver,
	combout => address_a27_combout);

address_reg_a14_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(14),
	dataout => address_reg_a14_a_ainput_I_driver);

-- Location: IOIBUF_X43_Y0_N8
address_reg_a14_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a14_a_ainput_I_driver,
	o => address_reg_a14_a_ainput_o);

address_a28_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a28_DATAA_driver);

address_a28_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a28_DATAB_driver);

address_a28_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a14_a_ainput_o,
	dataout => address_a28_DATAC_driver);

address_a28_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(14),
	dataout => address_a28_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N8
address_a28 : cycloneive_lcell_comb
-- Equation(s):
-- address_a28_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a14_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(14))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a28_DATAA_driver,
	datab => address_a28_DATAB_driver,
	datac => address_a28_DATAC_driver,
	datad => address_a28_DATAD_driver,
	combout => address_a28_combout);

address_a29_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a29_DATAA_driver);

address_a29_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a29_DATAB_driver);

address_a29_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a28_combout,
	dataout => address_a29_DATAC_driver);

address_a29_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(14),
	dataout => address_a29_DATAD_driver);

-- Location: LCCOMB_X39_Y26_N22
address_a29 : cycloneive_lcell_comb
-- Equation(s):
-- address_a29_combout = (address_a28_combout) # ((!mux_address_a1_a_ainput_o & (mux_address_a0_a_ainput_o & O(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a29_DATAA_driver,
	datab => address_a29_DATAB_driver,
	datac => address_a29_DATAC_driver,
	datad => address_a29_DATAD_driver,
	combout => address_a29_combout);

address_a30_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a15_a_ainput_o,
	dataout => address_a30_DATAA_driver);

address_a30_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(15),
	dataout => address_a30_DATAB_driver);

address_a30_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a30_DATAC_driver);

address_a30_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a30_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N0
address_a30 : cycloneive_lcell_comb
-- Equation(s):
-- address_a30_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a15_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(15))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a30_DATAA_driver,
	datab => address_a30_DATAB_driver,
	datac => address_a30_DATAC_driver,
	datad => address_a30_DATAD_driver,
	combout => address_a30_combout);

address_a31_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(15),
	dataout => address_a31_DATAA_driver);

address_a31_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a30_combout,
	dataout => address_a31_DATAB_driver);

address_a31_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a31_DATAC_driver);

address_a31_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a31_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N26
address_a31 : cycloneive_lcell_comb
-- Equation(s):
-- address_a31_combout = (address_a30_combout) # ((O(15) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a31_DATAA_driver,
	datab => address_a31_DATAB_driver,
	datac => address_a31_DATAC_driver,
	datad => address_a31_DATAD_driver,
	combout => address_a31_combout);

address_reg_a16_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(16),
	dataout => address_reg_a16_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y12_N15
address_reg_a16_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a16_a_ainput_I_driver,
	o => address_reg_a16_a_ainput_o);

address_a32_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(16),
	dataout => address_a32_DATAA_driver);

address_a32_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a32_DATAB_driver);

address_a32_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a32_DATAC_driver);

address_a32_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a16_a_ainput_o,
	dataout => address_a32_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N28
address_a32 : cycloneive_lcell_comb
-- Equation(s):
-- address_a32_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a16_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(16)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a32_DATAA_driver,
	datab => address_a32_DATAB_driver,
	datac => address_a32_DATAC_driver,
	datad => address_a32_DATAD_driver,
	combout => address_a32_combout);

address_a33_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(16),
	dataout => address_a33_DATAA_driver);

address_a33_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a32_combout,
	dataout => address_a33_DATAB_driver);

address_a33_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a33_DATAC_driver);

address_a33_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a33_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N30
address_a33 : cycloneive_lcell_comb
-- Equation(s):
-- address_a33_combout = (address_a32_combout) # ((O(16) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a33_DATAA_driver,
	datab => address_a33_DATAB_driver,
	datac => address_a33_DATAC_driver,
	datad => address_a33_DATAD_driver,
	combout => address_a33_combout);

address_reg_a17_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(17),
	dataout => address_reg_a17_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y11_N1
address_reg_a17_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a17_a_ainput_I_driver,
	o => address_reg_a17_a_ainput_o);

address_a34_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(17),
	dataout => address_a34_DATAA_driver);

address_a34_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a34_DATAB_driver);

address_a34_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a34_DATAC_driver);

address_a34_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a17_a_ainput_o,
	dataout => address_a34_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N4
address_a34 : cycloneive_lcell_comb
-- Equation(s):
-- address_a34_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a17_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(17)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a34_DATAA_driver,
	datab => address_a34_DATAB_driver,
	datac => address_a34_DATAC_driver,
	datad => address_a34_DATAD_driver,
	combout => address_a34_combout);

address_a35_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(17),
	dataout => address_a35_DATAA_driver);

address_a35_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a35_DATAB_driver);

address_a35_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a34_combout,
	dataout => address_a35_DATAC_driver);

address_a35_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a35_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N18
address_a35 : cycloneive_lcell_comb
-- Equation(s):
-- address_a35_combout = (address_a34_combout) # ((O(17) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a35_DATAA_driver,
	datab => address_a35_DATAB_driver,
	datac => address_a35_DATAC_driver,
	datad => address_a35_DATAD_driver,
	combout => address_a35_combout);

address_reg_a18_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(18),
	dataout => address_reg_a18_a_ainput_I_driver);

-- Location: IOIBUF_X45_Y0_N8
address_reg_a18_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a18_a_ainput_I_driver,
	o => address_reg_a18_a_ainput_o);

address_a36_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a36_DATAA_driver);

address_a36_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a18_a_ainput_o,
	dataout => address_a36_DATAB_driver);

address_a36_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(18),
	dataout => address_a36_DATAC_driver);

address_a36_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a36_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N16
address_a36 : cycloneive_lcell_comb
-- Equation(s):
-- address_a36_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a18_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(18))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a36_DATAA_driver,
	datab => address_a36_DATAB_driver,
	datac => address_a36_DATAC_driver,
	datad => address_a36_DATAD_driver,
	combout => address_a36_combout);

address_a37_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a37_DATAA_driver);

address_a37_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a36_combout,
	dataout => address_a37_DATAB_driver);

address_a37_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(18),
	dataout => address_a37_DATAC_driver);

address_a37_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a37_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N30
address_a37 : cycloneive_lcell_comb
-- Equation(s):
-- address_a37_combout = (address_a36_combout) # ((mux_address_a0_a_ainput_o & (O(18) & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a37_DATAA_driver,
	datab => address_a37_DATAB_driver,
	datac => address_a37_DATAC_driver,
	datad => address_a37_DATAD_driver,
	combout => address_a37_combout);

address_reg_a19_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(19),
	dataout => address_reg_a19_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y28_N1
address_reg_a19_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a19_a_ainput_I_driver,
	o => address_reg_a19_a_ainput_o);

address_a38_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(19),
	dataout => address_a38_DATAA_driver);

address_a38_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a19_a_ainput_o,
	dataout => address_a38_DATAB_driver);

address_a38_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a38_DATAC_driver);

address_a38_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a38_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N24
address_a38 : cycloneive_lcell_comb
-- Equation(s):
-- address_a38_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a19_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(19)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a38_DATAA_driver,
	datab => address_a38_DATAB_driver,
	datac => address_a38_DATAC_driver,
	datad => address_a38_DATAD_driver,
	combout => address_a38_combout);

address_a39_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(19),
	dataout => address_a39_DATAA_driver);

address_a39_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a39_DATAB_driver);

address_a39_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a39_DATAC_driver);

address_a39_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a38_combout,
	dataout => address_a39_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N22
address_a39 : cycloneive_lcell_comb
-- Equation(s):
-- address_a39_combout = (address_a38_combout) # ((O(19) & (!mux_address_a1_a_ainput_o & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a39_DATAA_driver,
	datab => address_a39_DATAB_driver,
	datac => address_a39_DATAC_driver,
	datad => address_a39_DATAD_driver,
	combout => address_a39_combout);

address_reg_a20_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(20),
	dataout => address_reg_a20_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y17_N22
address_reg_a20_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a20_a_ainput_I_driver,
	o => address_reg_a20_a_ainput_o);

address_a40_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a40_DATAA_driver);

address_a40_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(20),
	dataout => address_a40_DATAB_driver);

address_a40_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a20_a_ainput_o,
	dataout => address_a40_DATAC_driver);

address_a40_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a40_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N12
address_a40 : cycloneive_lcell_comb
-- Equation(s):
-- address_a40_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a20_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(20)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a40_DATAA_driver,
	datab => address_a40_DATAB_driver,
	datac => address_a40_DATAC_driver,
	datad => address_a40_DATAD_driver,
	combout => address_a40_combout);

address_a41_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(20),
	dataout => address_a41_DATAA_driver);

address_a41_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a41_DATAB_driver);

address_a41_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a41_DATAC_driver);

address_a41_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a40_combout,
	dataout => address_a41_DATAD_driver);

-- Location: LCCOMB_X37_Y25_N6
address_a41 : cycloneive_lcell_comb
-- Equation(s):
-- address_a41_combout = (address_a40_combout) # ((O(20) & (!mux_address_a1_a_ainput_o & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a41_DATAA_driver,
	datab => address_a41_DATAB_driver,
	datac => address_a41_DATAC_driver,
	datad => address_a41_DATAD_driver,
	combout => address_a41_combout);

address_a42_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a21_a_ainput_o,
	dataout => address_a42_DATAA_driver);

address_a42_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(21),
	dataout => address_a42_DATAB_driver);

address_a42_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a42_DATAC_driver);

address_a42_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a42_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N24
address_a42 : cycloneive_lcell_comb
-- Equation(s):
-- address_a42_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a21_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(21))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a42_DATAA_driver,
	datab => address_a42_DATAB_driver,
	datac => address_a42_DATAC_driver,
	datad => address_a42_DATAD_driver,
	combout => address_a42_combout);

address_a43_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(21),
	dataout => address_a43_DATAA_driver);

address_a43_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a42_combout,
	dataout => address_a43_DATAB_driver);

address_a43_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a43_DATAC_driver);

address_a43_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a43_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N22
address_a43 : cycloneive_lcell_comb
-- Equation(s):
-- address_a43_combout = (address_a42_combout) # ((O(21) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a43_DATAA_driver,
	datab => address_a43_DATAB_driver,
	datac => address_a43_DATAC_driver,
	datad => address_a43_DATAD_driver,
	combout => address_a43_combout);

address_a44_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a22_a_ainput_o,
	dataout => address_a44_DATAA_driver);

address_a44_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(22),
	dataout => address_a44_DATAB_driver);

address_a44_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a44_DATAC_driver);

address_a44_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a44_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N12
address_a44 : cycloneive_lcell_comb
-- Equation(s):
-- address_a44_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a22_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(22))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a44_DATAA_driver,
	datab => address_a44_DATAB_driver,
	datac => address_a44_DATAC_driver,
	datad => address_a44_DATAD_driver,
	combout => address_a44_combout);

address_a45_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a44_combout,
	dataout => address_a45_DATAA_driver);

address_a45_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a45_DATAB_driver);

address_a45_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(22),
	dataout => address_a45_DATAC_driver);

address_a45_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a45_DATAD_driver);

-- Location: LCCOMB_X42_Y28_N10
address_a45 : cycloneive_lcell_comb
-- Equation(s):
-- address_a45_combout = (address_a44_combout) # ((mux_address_a0_a_ainput_o & (O(22) & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a45_DATAA_driver,
	datab => address_a45_DATAB_driver,
	datac => address_a45_DATAC_driver,
	datad => address_a45_DATAD_driver,
	combout => address_a45_combout);

address_a46_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a23_a_ainput_o,
	dataout => address_a46_DATAA_driver);

address_a46_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a46_DATAB_driver);

address_a46_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a46_DATAC_driver);

address_a46_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(23),
	dataout => address_a46_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N28
address_a46 : cycloneive_lcell_comb
-- Equation(s):
-- address_a46_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a23_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(23))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a46_DATAA_driver,
	datab => address_a46_DATAB_driver,
	datac => address_a46_DATAC_driver,
	datad => address_a46_DATAD_driver,
	combout => address_a46_combout);

address_a47_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(23),
	dataout => address_a47_DATAA_driver);

address_a47_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a46_combout,
	dataout => address_a47_DATAB_driver);

address_a47_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a47_DATAC_driver);

address_a47_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a47_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N18
address_a47 : cycloneive_lcell_comb
-- Equation(s):
-- address_a47_combout = (address_a46_combout) # ((O(23) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a47_DATAA_driver,
	datab => address_a47_DATAB_driver,
	datac => address_a47_DATAC_driver,
	datad => address_a47_DATAD_driver,
	combout => address_a47_combout);

address_reg_a24_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(24),
	dataout => address_reg_a24_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y27_N15
address_reg_a24_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a24_a_ainput_I_driver,
	o => address_reg_a24_a_ainput_o);

address_a48_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a48_DATAA_driver);

address_a48_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(24),
	dataout => address_a48_DATAB_driver);

address_a48_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a24_a_ainput_o,
	dataout => address_a48_DATAC_driver);

address_a48_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a48_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N4
address_a48 : cycloneive_lcell_comb
-- Equation(s):
-- address_a48_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a24_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(24)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a48_DATAA_driver,
	datab => address_a48_DATAB_driver,
	datac => address_a48_DATAC_driver,
	datad => address_a48_DATAD_driver,
	combout => address_a48_combout);

address_a49_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a49_DATAA_driver);

address_a49_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a48_combout,
	dataout => address_a49_DATAB_driver);

address_a49_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(24),
	dataout => address_a49_DATAC_driver);

address_a49_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a49_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N2
address_a49 : cycloneive_lcell_comb
-- Equation(s):
-- address_a49_combout = (address_a48_combout) # ((!mux_address_a1_a_ainput_o & (O(24) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a49_DATAA_driver,
	datab => address_a49_DATAB_driver,
	datac => address_a49_DATAC_driver,
	datad => address_a49_DATAD_driver,
	combout => address_a49_combout);

address_reg_a25_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(25),
	dataout => address_reg_a25_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y20_N22
address_reg_a25_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a25_a_ainput_I_driver,
	o => address_reg_a25_a_ainput_o);

address_a50_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a50_DATAA_driver);

address_a50_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(25),
	dataout => address_a50_DATAB_driver);

address_a50_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a25_a_ainput_o,
	dataout => address_a50_DATAC_driver);

address_a50_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a50_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N28
address_a50 : cycloneive_lcell_comb
-- Equation(s):
-- address_a50_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a25_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(25)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a50_DATAA_driver,
	datab => address_a50_DATAB_driver,
	datac => address_a50_DATAC_driver,
	datad => address_a50_DATAD_driver,
	combout => address_a50_combout);

address_a51_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a51_DATAA_driver);

address_a51_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a50_combout,
	dataout => address_a51_DATAB_driver);

address_a51_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(25),
	dataout => address_a51_DATAC_driver);

address_a51_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a51_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N22
address_a51 : cycloneive_lcell_comb
-- Equation(s):
-- address_a51_combout = (address_a50_combout) # ((!mux_address_a1_a_ainput_o & (O(25) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a51_DATAA_driver,
	datab => address_a51_DATAB_driver,
	datac => address_a51_DATAC_driver,
	datad => address_a51_DATAD_driver,
	combout => address_a51_combout);

address_reg_a26_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(26),
	dataout => address_reg_a26_a_ainput_I_driver);

-- Location: IOIBUF_X67_Y38_N8
address_reg_a26_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a26_a_ainput_I_driver,
	o => address_reg_a26_a_ainput_o);

address_a52_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a52_DATAA_driver);

address_a52_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a52_DATAB_driver);

address_a52_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(26),
	dataout => address_a52_DATAC_driver);

address_a52_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a26_a_ainput_o,
	dataout => address_a52_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N24
address_a52 : cycloneive_lcell_comb
-- Equation(s):
-- address_a52_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a26_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(26)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a52_DATAA_driver,
	datab => address_a52_DATAB_driver,
	datac => address_a52_DATAC_driver,
	datad => address_a52_DATAD_driver,
	combout => address_a52_combout);

address_a53_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a53_DATAA_driver);

address_a53_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(26),
	dataout => address_a53_DATAB_driver);

address_a53_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a53_DATAC_driver);

address_a53_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a52_combout,
	dataout => address_a53_DATAD_driver);

-- Location: LCCOMB_X39_Y29_N2
address_a53 : cycloneive_lcell_comb
-- Equation(s):
-- address_a53_combout = (address_a52_combout) # ((!mux_address_a1_a_ainput_o & (O(26) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a53_DATAA_driver,
	datab => address_a53_DATAB_driver,
	datac => address_a53_DATAC_driver,
	datad => address_a53_DATAD_driver,
	combout => address_a53_combout);

address_reg_a27_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(27),
	dataout => address_reg_a27_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y34_N8
address_reg_a27_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a27_a_ainput_I_driver,
	o => address_reg_a27_a_ainput_o);

address_a54_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a54_DATAA_driver);

address_a54_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a54_DATAB_driver);

address_a54_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a27_a_ainput_o,
	dataout => address_a54_DATAC_driver);

address_a54_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(27),
	dataout => address_a54_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N24
address_a54 : cycloneive_lcell_comb
-- Equation(s):
-- address_a54_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & (address_reg_a27_a_ainput_o)) # (!mux_address_a1_a_ainput_o & ((PC(27))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a54_DATAA_driver,
	datab => address_a54_DATAB_driver,
	datac => address_a54_DATAC_driver,
	datad => address_a54_DATAD_driver,
	combout => address_a54_combout);

address_a55_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a55_DATAA_driver);

address_a55_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a54_combout,
	dataout => address_a55_DATAB_driver);

address_a55_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(27),
	dataout => address_a55_DATAC_driver);

address_a55_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a55_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N30
address_a55 : cycloneive_lcell_comb
-- Equation(s):
-- address_a55_combout = (address_a54_combout) # ((!mux_address_a1_a_ainput_o & (O(27) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a55_DATAA_driver,
	datab => address_a55_DATAB_driver,
	datac => address_a55_DATAC_driver,
	datad => address_a55_DATAD_driver,
	combout => address_a55_combout);

address_reg_a28_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(28),
	dataout => address_reg_a28_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y31_N22
address_reg_a28_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a28_a_ainput_I_driver,
	o => address_reg_a28_a_ainput_o);

address_a56_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(28),
	dataout => address_a56_DATAA_driver);

address_a56_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a28_a_ainput_o,
	dataout => address_a56_DATAB_driver);

address_a56_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a56_DATAC_driver);

address_a56_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a56_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N0
address_a56 : cycloneive_lcell_comb
-- Equation(s):
-- address_a56_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a28_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(28)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a56_DATAA_driver,
	datab => address_a56_DATAB_driver,
	datac => address_a56_DATAC_driver,
	datad => address_a56_DATAD_driver,
	combout => address_a56_combout);

address_a57_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(28),
	dataout => address_a57_DATAA_driver);

address_a57_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a57_DATAB_driver);

address_a57_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a57_DATAC_driver);

address_a57_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a56_combout,
	dataout => address_a57_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N30
address_a57 : cycloneive_lcell_comb
-- Equation(s):
-- address_a57_combout = (address_a56_combout) # ((O(28) & (!mux_address_a1_a_ainput_o & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a57_DATAA_driver,
	datab => address_a57_DATAB_driver,
	datac => address_a57_DATAC_driver,
	datad => address_a57_DATAD_driver,
	combout => address_a57_combout);

address_reg_a29_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(29),
	dataout => address_reg_a29_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y19_N22
address_reg_a29_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a29_a_ainput_I_driver,
	o => address_reg_a29_a_ainput_o);

address_a58_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a58_DATAA_driver);

address_a58_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(29),
	dataout => address_a58_DATAB_driver);

address_a58_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a29_a_ainput_o,
	dataout => address_a58_DATAC_driver);

address_a58_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a58_DATAD_driver);

-- Location: LCCOMB_X34_Y27_N8
address_a58 : cycloneive_lcell_comb
-- Equation(s):
-- address_a58_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a29_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(29)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a58_DATAA_driver,
	datab => address_a58_DATAB_driver,
	datac => address_a58_DATAC_driver,
	datad => address_a58_DATAD_driver,
	combout => address_a58_combout);

address_a59_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a59_DATAA_driver);

address_a59_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a58_combout,
	dataout => address_a59_DATAB_driver);

address_a59_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(29),
	dataout => address_a59_DATAC_driver);

address_a59_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a59_DATAD_driver);

-- Location: LCCOMB_X36_Y27_N20
address_a59 : cycloneive_lcell_comb
-- Equation(s):
-- address_a59_combout = (address_a58_combout) # ((!mux_address_a1_a_ainput_o & (O(29) & mux_address_a0_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a59_DATAA_driver,
	datab => address_a59_DATAB_driver,
	datac => address_a59_DATAC_driver,
	datad => address_a59_DATAD_driver,
	combout => address_a59_combout);

address_reg_a30_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(30),
	dataout => address_reg_a30_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y28_N8
address_reg_a30_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a30_a_ainput_I_driver,
	o => address_reg_a30_a_ainput_o);

address_a60_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(30),
	dataout => address_a60_DATAA_driver);

address_a60_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a30_a_ainput_o,
	dataout => address_a60_DATAB_driver);

address_a60_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a60_DATAC_driver);

address_a60_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a60_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N16
address_a60 : cycloneive_lcell_comb
-- Equation(s):
-- address_a60_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a30_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(30)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a60_DATAA_driver,
	datab => address_a60_DATAB_driver,
	datac => address_a60_DATAC_driver,
	datad => address_a60_DATAD_driver,
	combout => address_a60_combout);

address_a61_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(30),
	dataout => address_a61_DATAA_driver);

address_a61_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a60_combout,
	dataout => address_a61_DATAB_driver);

address_a61_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a61_DATAC_driver);

address_a61_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a61_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N14
address_a61 : cycloneive_lcell_comb
-- Equation(s):
-- address_a61_combout = (address_a60_combout) # ((O(30) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a61_DATAA_driver,
	datab => address_a61_DATAB_driver,
	datac => address_a61_DATAC_driver,
	datad => address_a61_DATAD_driver,
	combout => address_a61_combout);

address_reg_a31_a_ainput_I_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => ww_address_reg(31),
	dataout => address_reg_a31_a_ainput_I_driver);

-- Location: IOIBUF_X0_Y28_N15
address_reg_a31_a_ainput : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => address_reg_a31_a_ainput_I_driver,
	o => address_reg_a31_a_ainput_o);

address_a62_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => PC(31),
	dataout => address_a62_DATAA_driver);

address_a62_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_reg_a31_a_ainput_o,
	dataout => address_a62_DATAB_driver);

address_a62_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a62_DATAC_driver);

address_a62_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a62_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N8
address_a62 : cycloneive_lcell_comb
-- Equation(s):
-- address_a62_combout = (!mux_address_a0_a_ainput_o & ((mux_address_a1_a_ainput_o & ((address_reg_a31_a_ainput_o))) # (!mux_address_a1_a_ainput_o & (PC(31)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a62_DATAA_driver,
	datab => address_a62_DATAB_driver,
	datac => address_a62_DATAC_driver,
	datad => address_a62_DATAD_driver,
	combout => address_a62_combout);

address_a63_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => address_a62_combout,
	dataout => address_a63_DATAA_driver);

address_a63_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => O(31),
	dataout => address_a63_DATAB_driver);

address_a63_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a0_a_ainput_o,
	dataout => address_a63_DATAC_driver);

address_a63_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => mux_address_a1_a_ainput_o,
	dataout => address_a63_DATAD_driver);

-- Location: LCCOMB_X34_Y28_N26
address_a63 : cycloneive_lcell_comb
-- Equation(s):
-- address_a63_combout = (address_a62_combout) # ((O(31) & (mux_address_a0_a_ainput_o & !mux_address_a1_a_ainput_o)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => address_a63_DATAA_driver,
	datab => address_a63_DATAB_driver,
	datac => address_a63_DATAC_driver,
	datad => address_a63_DATAD_driver,
	combout => address_a63_combout);

A_a4_a_afeeder_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a5_combout,
	dataout => A_a4_a_afeeder_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N4
A_a4_a_afeeder : cycloneive_lcell_comb
-- Equation(s):
-- A_a4_a_afeeder_combout = A_a5_combout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => A_a4_a_afeeder_DATAD_driver,
	combout => A_a4_a_afeeder_combout);

A_a4_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a4_a_CLK_driver);

A_a4_a_D_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a4_a_afeeder_combout,
	dataout => A_a4_a_D_driver);

A_a4_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a4_a_ENA_driver);

-- Location: FF_X38_Y29_N5
A_a4_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a4_a_CLK_driver,
	d => A_a4_a_D_driver,
	ena => A_a4_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(4));

Equal30_a6_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(6),
	dataout => Equal30_a6_DATAA_driver);

Equal30_a6_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(4),
	dataout => Equal30_a6_DATAB_driver);

Equal30_a6_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(5),
	dataout => Equal30_a6_DATAC_driver);

Equal30_a6_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(3),
	dataout => Equal30_a6_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N12
Equal30_a6 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a6_combout = (!A(6) & (!A(4) & (!A(5) & !A(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a6_DATAA_driver,
	datab => Equal30_a6_DATAB_driver,
	datac => Equal30_a6_DATAC_driver,
	datad => Equal30_a6_DATAD_driver,
	combout => Equal30_a6_combout);

A_a8_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a8_a_CLK_driver);

A_a8_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a9_combout,
	dataout => A_a8_a_ASDATA_driver);

A_a8_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a8_a_ENA_driver);

-- Location: FF_X37_Y30_N15
A_a8_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a8_a_CLK_driver,
	asdata => A_a8_a_ASDATA_driver,
	sload => VCC,
	ena => A_a8_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(8));

A_a7_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a7_a_CLK_driver);

A_a7_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a8_combout,
	dataout => A_a7_a_ASDATA_driver);

A_a7_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a7_a_ENA_driver);

-- Location: FF_X37_Y30_N3
A_a7_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a7_a_CLK_driver,
	asdata => A_a7_a_ASDATA_driver,
	sload => VCC,
	ena => A_a7_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(7));

Equal30_a5_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(9),
	dataout => Equal30_a5_DATAA_driver);

Equal30_a5_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(8),
	dataout => Equal30_a5_DATAB_driver);

Equal30_a5_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(10),
	dataout => Equal30_a5_DATAC_driver);

Equal30_a5_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(7),
	dataout => Equal30_a5_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N8
Equal30_a5 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a5_combout = (!A(9) & (!A(8) & (!A(10) & !A(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a5_DATAA_driver,
	datab => Equal30_a5_DATAB_driver,
	datac => Equal30_a5_DATAC_driver,
	datad => Equal30_a5_DATAD_driver,
	combout => Equal30_a5_combout);

Equal30_a7_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(1),
	dataout => Equal30_a7_DATAA_driver);

Equal30_a7_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(0),
	dataout => Equal30_a7_DATAB_driver);

Equal30_a7_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(2),
	dataout => Equal30_a7_DATAC_driver);

Equal30_a7_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(27),
	dataout => Equal30_a7_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N2
Equal30_a7 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a7_combout = (!A(1) & (!A(0) & (!A(2) & !A(27))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a7_DATAA_driver,
	datab => Equal30_a7_DATAB_driver,
	datac => Equal30_a7_DATAC_driver,
	datad => Equal30_a7_DATAD_driver,
	combout => Equal30_a7_combout);

Equal30_a9_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a8_combout,
	dataout => Equal30_a9_DATAA_driver);

Equal30_a9_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a6_combout,
	dataout => Equal30_a9_DATAB_driver);

Equal30_a9_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a5_combout,
	dataout => Equal30_a9_DATAC_driver);

Equal30_a9_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a7_combout,
	dataout => Equal30_a9_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N12
Equal30_a9 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a9_combout = (Equal30_a8_combout & (Equal30_a6_combout & (Equal30_a5_combout & Equal30_a7_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a9_DATAA_driver,
	datab => Equal30_a9_DATAB_driver,
	datac => Equal30_a9_DATAC_driver,
	datad => Equal30_a9_DATAD_driver,
	combout => Equal30_a9_combout);

Equal30_a3_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(14),
	dataout => Equal30_a3_DATAA_driver);

Equal30_a3_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(13),
	dataout => Equal30_a3_DATAB_driver);

Equal30_a3_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(11),
	dataout => Equal30_a3_DATAC_driver);

Equal30_a3_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(12),
	dataout => Equal30_a3_DATAD_driver);

-- Location: LCCOMB_X37_Y26_N2
Equal30_a3 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a3_combout = (!A(14) & (!A(13) & (!A(11) & !A(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a3_DATAA_driver,
	datab => Equal30_a3_DATAB_driver,
	datac => Equal30_a3_DATAC_driver,
	datad => Equal30_a3_DATAD_driver,
	combout => Equal30_a3_combout);

A_a18_a_CLK_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => clk_ainputclkctrl_outclk,
	dataout => A_a18_a_CLK_driver);

A_a18_a_ASDATA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a19_combout,
	dataout => A_a18_a_ASDATA_driver);

A_a18_a_ENA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A_a3_a_a1_combout,
	dataout => A_a18_a_ENA_driver);

-- Location: FF_X38_Y28_N1
A_a18_a : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => A_a18_a_CLK_driver,
	asdata => A_a18_a_ASDATA_driver,
	sload => VCC,
	ena => A_a18_a_ENA_driver,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => A(18));

Equal30_a2_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(18),
	dataout => Equal30_a2_DATAB_driver);

Equal30_a2_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(17),
	dataout => Equal30_a2_DATAC_driver);

-- Location: LCCOMB_X38_Y28_N16
Equal30_a2 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a2_combout = (!A(18) & !A(17))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Equal30_a2_DATAB_driver,
	datac => Equal30_a2_DATAC_driver,
	combout => Equal30_a2_combout);

Equal30_a4_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(15),
	dataout => Equal30_a4_DATAA_driver);

Equal30_a4_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a3_combout,
	dataout => Equal30_a4_DATAB_driver);

Equal30_a4_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(16),
	dataout => Equal30_a4_DATAC_driver);

Equal30_a4_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a2_combout,
	dataout => Equal30_a4_DATAD_driver);

-- Location: LCCOMB_X38_Y28_N12
Equal30_a4 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a4_combout = (!A(15) & (Equal30_a3_combout & (!A(16) & Equal30_a2_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a4_DATAA_driver,
	datab => Equal30_a4_DATAB_driver,
	datac => Equal30_a4_DATAC_driver,
	datad => Equal30_a4_DATAD_driver,
	combout => Equal30_a4_combout);

Equal30_a0_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(25),
	dataout => Equal30_a0_DATAA_driver);

Equal30_a0_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(23),
	dataout => Equal30_a0_DATAB_driver);

Equal30_a0_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(26),
	dataout => Equal30_a0_DATAC_driver);

Equal30_a0_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(24),
	dataout => Equal30_a0_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N14
Equal30_a0 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a0_combout = (!A(25) & (!A(23) & (!A(26) & !A(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a0_DATAA_driver,
	datab => Equal30_a0_DATAB_driver,
	datac => Equal30_a0_DATAC_driver,
	datad => Equal30_a0_DATAD_driver,
	combout => Equal30_a0_combout);

Equal30_a1_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(22),
	dataout => Equal30_a1_DATAA_driver);

Equal30_a1_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(21),
	dataout => Equal30_a1_DATAB_driver);

Equal30_a1_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(19),
	dataout => Equal30_a1_DATAC_driver);

Equal30_a1_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => A(20),
	dataout => Equal30_a1_DATAD_driver);

-- Location: LCCOMB_X38_Y29_N14
Equal30_a1 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a1_combout = (!A(22) & (!A(21) & (!A(19) & !A(20))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a1_DATAA_driver,
	datab => Equal30_a1_DATAB_driver,
	datac => Equal30_a1_DATAC_driver,
	datad => Equal30_a1_DATAD_driver,
	combout => Equal30_a1_combout);

Equal30_a10_DATAA_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a9_combout,
	dataout => Equal30_a10_DATAA_driver);

Equal30_a10_DATAB_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a4_combout,
	dataout => Equal30_a10_DATAB_driver);

Equal30_a10_DATAC_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a0_combout,
	dataout => Equal30_a10_DATAC_driver);

Equal30_a10_DATAD_routing_wire_inst : cycloneive_routing_wire
PORT MAP (
	datain => Equal30_a1_combout,
	dataout => Equal30_a10_DATAD_driver);

-- Location: LCCOMB_X35_Y26_N26
Equal30_a10 : cycloneive_lcell_comb
-- Equation(s):
-- Equal30_a10_combout = (Equal30_a9_combout & (Equal30_a4_combout & (Equal30_a0_combout & Equal30_a1_combout)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Equal30_a10_DATAA_driver,
	datab => Equal30_a10_DATAB_driver,
	datac => Equal30_a10_DATAC_driver,
	datad => Equal30_a10_DATAD_driver,
	combout => Equal30_a10_combout);

ww_data_out(0) <= data_out_a0_a_aoutput_o;

ww_data_out(1) <= data_out_a1_a_aoutput_o;

ww_data_out(2) <= data_out_a2_a_aoutput_o;

ww_data_out(3) <= data_out_a3_a_aoutput_o;

ww_data_out(4) <= data_out_a4_a_aoutput_o;

ww_data_out(5) <= data_out_a5_a_aoutput_o;

ww_data_out(6) <= data_out_a6_a_aoutput_o;

ww_data_out(7) <= data_out_a7_a_aoutput_o;

ww_data_out(8) <= data_out_a8_a_aoutput_o;

ww_data_out(9) <= data_out_a9_a_aoutput_o;

ww_data_out(10) <= data_out_a10_a_aoutput_o;

ww_data_out(11) <= data_out_a11_a_aoutput_o;

ww_data_out(12) <= data_out_a12_a_aoutput_o;

ww_data_out(13) <= data_out_a13_a_aoutput_o;

ww_data_out(14) <= data_out_a14_a_aoutput_o;

ww_data_out(15) <= data_out_a15_a_aoutput_o;

ww_data_out(16) <= data_out_a16_a_aoutput_o;

ww_data_out(17) <= data_out_a17_a_aoutput_o;

ww_data_out(18) <= data_out_a18_a_aoutput_o;

ww_data_out(19) <= data_out_a19_a_aoutput_o;

ww_data_out(20) <= data_out_a20_a_aoutput_o;

ww_data_out(21) <= data_out_a21_a_aoutput_o;

ww_data_out(22) <= data_out_a22_a_aoutput_o;

ww_data_out(23) <= data_out_a23_a_aoutput_o;

ww_data_out(24) <= data_out_a24_a_aoutput_o;

ww_data_out(25) <= data_out_a25_a_aoutput_o;

ww_data_out(26) <= data_out_a26_a_aoutput_o;

ww_data_out(27) <= data_out_a27_a_aoutput_o;

ww_data_out(28) <= data_out_a28_a_aoutput_o;

ww_data_out(29) <= data_out_a29_a_aoutput_o;

ww_data_out(30) <= data_out_a30_a_aoutput_o;

ww_data_out(31) <= data_out_a31_a_aoutput_o;

ww_address(0) <= address_a0_a_aoutput_o;

ww_address(1) <= address_a1_a_aoutput_o;

ww_address(2) <= address_a2_a_aoutput_o;

ww_address(3) <= address_a3_a_aoutput_o;

ww_address(4) <= address_a4_a_aoutput_o;

ww_address(5) <= address_a5_a_aoutput_o;

ww_address(6) <= address_a6_a_aoutput_o;

ww_address(7) <= address_a7_a_aoutput_o;

ww_address(8) <= address_a8_a_aoutput_o;

ww_address(9) <= address_a9_a_aoutput_o;

ww_address(10) <= address_a10_a_aoutput_o;

ww_address(11) <= address_a11_a_aoutput_o;

ww_address(12) <= address_a12_a_aoutput_o;

ww_address(13) <= address_a13_a_aoutput_o;

ww_address(14) <= address_a14_a_aoutput_o;

ww_address(15) <= address_a15_a_aoutput_o;

ww_address(16) <= address_a16_a_aoutput_o;

ww_address(17) <= address_a17_a_aoutput_o;

ww_address(18) <= address_a18_a_aoutput_o;

ww_address(19) <= address_a19_a_aoutput_o;

ww_address(20) <= address_a20_a_aoutput_o;

ww_address(21) <= address_a21_a_aoutput_o;

ww_address(22) <= address_a22_a_aoutput_o;

ww_address(23) <= address_a23_a_aoutput_o;

ww_address(24) <= address_a24_a_aoutput_o;

ww_address(25) <= address_a25_a_aoutput_o;

ww_address(26) <= address_a26_a_aoutput_o;

ww_address(27) <= address_a27_a_aoutput_o;

ww_address(28) <= address_a28_a_aoutput_o;

ww_address(29) <= address_a29_a_aoutput_o;

ww_address(30) <= address_a30_a_aoutput_o;

ww_address(31) <= address_a31_a_aoutput_o;

ww_a_zero <= a_zero_aoutput_o;

ww_a_msb <= a_msb_aoutput_o;

ww_alu_c <= alu_c_aoutput_o;

ww_alu_msb <= alu_msb_aoutput_o;
END datapath;


