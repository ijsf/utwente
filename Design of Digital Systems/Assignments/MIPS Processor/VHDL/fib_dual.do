# funny trick to prevent an error
# create WORK (if it exists a warning is raised; delete WORK and create an empty WORK
vlib work
vdel -all
vlib work

vcom memory.vhd
vcom processor_ent.vhd
vcom testbench_ent.vhd
vcom instructions_pack.vhd
vcom processor_behav_arch.vhd
vcom compare.vhd
vcom processor_behav_slow_arch.vhd
vcom testbench_dual_arch.vhd
vcom conf_tb_fib_dual.vhd

vsim -msgmode both -displaymsgmode both work.conf_testbench_dual

# Compare unit
add wave -label clk sim:/testbench/clk_i
add wave -label reset sim:/testbench/reset_i
add wave -label C_data_CIMO sim:/testbench/data_ci_mo
add wave -label C_data_COMI sim:/testbench/data_co_mi

# Golden unit
add wave -label G_clk sim:/testbench/clk_gold
add wave -label G_nRD_WR sim:/testbench/nRD_WR_gold
add wave -label G_enable sim:/testbench/enable_gold
add wave -label G_syscall sim:/testbench/syscall_gold
add wave -label G_instruction sim:/testbench/proc_gold/proc_proc/instruction
add wave -label G_PC sim:/testbench/proc_gold/proc_proc/PC
add wave -label G_nPC sim:/testbench/proc_gold/proc_proc/nPC
add wave -label G_HI sim:/testbench/proc_gold/proc_proc/HI
add wave -label G_LO sim:/testbench/proc_gold/proc_proc/LO
add wave -label G_offset sim:/testbench/proc_gold/proc_proc/offset

# Architectural unit
add wave -label A_clk sim:/testbench/clk_silver
add wave -label A_nRD_WR sim:/testbench/nRD_WR_silver
add wave -label A_enable sim:/testbench/enable_silver
add wave -label A_syscall sim:/testbench/syscall_silver
add wave -label A_instruction sim:/testbench/proc_silver/proc_proc/instruction
add wave -label A_PC sim:/testbench/proc_silver/proc_proc/PC
add wave -label A_nPC sim:/testbench/proc_silver/proc_proc/nPC
add wave -label A_HI sim:/testbench/proc_silver/proc_proc/HI
add wave -label A_LO sim:/testbench/proc_silver/proc_proc/LO
add wave -label A_offset sim:/testbench/proc_silver/proc_proc/offset

# Memory unit
add wave -label M_nRD_WR sim:/testbench/nRD_WR_mem
add wave -label M_enable sim:/testbench/enable_mem
add wave -label M_address sim:/testbench/address_mem
add wave -label M_d_mem_2800 sim:/testbench/mem/mem_proc/data_mem(512)
add wave -label M_d_mem_2804 sim:/testbench/mem/mem_proc/data_mem(513)

radix -hexadecimal

run -all

wave zoom full