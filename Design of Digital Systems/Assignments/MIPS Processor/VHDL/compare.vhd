LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
--USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY compare IS 
	PORT( 
		clk            : IN  std_logic;
		               
		mem_data_in    : IN  std_logic_vector( 31 DOWNTO 0 ); 
		mem_data_out   : OUT std_logic_vector( 31 DOWNTO 0 ); 
		mem_address    : OUT std_logic_vector( 31 DOWNTO 0 ); 
		mem_rd_wr      : OUT std_logic; --( 0 = read, 1 = write )
		mem_enable     : OUT std_logic;
		
		left_clk       : OUT std_logic;
		left_data_in   : IN  std_logic_vector( 31 DOWNTO 0 ); 
		left_data_out  : OUT std_logic_vector( 31 DOWNTO 0 ); 
		left_address   : IN  std_logic_vector( 31 DOWNTO 0 ); 
		left_rd_wr     : IN  std_logic; --( 0 = read, 1 = write )
		left_enable    : IN  std_logic;
		left_syscall   : IN  std_logic;
		
		right_clk      : OUT std_logic;
		right_data_in  : IN  std_logic_vector( 31 DOWNTO 0 ); 
		right_data_out : OUT std_logic_vector( 31 DOWNTO 0 ); 
		right_address  : IN  std_logic_vector( 31 DOWNTO 0 ); 
		right_rd_wr    : IN  std_logic; --( 0 = read, 1 = write )
		right_enable   : IN  std_logic;
		right_syscall  : IN  std_logic
	); 
END;

ARCHITECTURE behavior OF compare IS    
	SIGNAL internal_enable : std_logic;
	SIGNAL left_enable_d   : std_logic;
	SIGNAL right_enable_d  : std_logic;
BEGIN
	comp_proc: PROCESS
        VARIABLE left_data  : integer;
        VARIABLE right_data : integer;
	BEGIN  
		WAIT UNTIL internal_enable = '1' AND rising_edge( clk );
		
		IF( left_address /= right_address ) THEN
			REPORT "Address mismatch" SEVERITY warning;
		END IF;
		
		IF( left_rd_wr /= right_rd_wr ) THEN
		REPORT "RD/WR mismatch" SEVERITY warning;
		END IF;
		
		IF( left_rd_wr = '1' AND left_rd_wr = '1' AND left_data_in /= right_data_in ) THEN
            left_data  := to_integer( signed( left_data_in ) );
            right_data := to_integer( signed( right_data_in ) );
            
			REPORT "Data mismatch. Gold: " & integer'image( left_data ) &
                   " Silver: " & integer'image( right_data )            
            SEVERITY warning;
		END IF;
	END PROCESS;
  
	clk_proc: PROCESS( clk )
	BEGIN
		IF( left_enable = '1' AND right_enable = '0' ) THEN
			left_clk <= '0';
		ELSE
			left_clk <= clk;
		END IF;
		
		IF( right_enable = '1' AND left_enable = '0' ) THEN
			right_clk <= '0';
		ELSE
			right_clk <= clk;
		END IF;
	END PROCESS;
  
	internal_enable <= left_enable AND right_enable;
  
	left_data_out  <= mem_data_in;
	right_data_out <= mem_data_in;
	
	left_enable_d  <= left_enable;
	right_enable_d <= right_enable;

	mem_data_out   <= left_data_in;
	mem_address    <= left_address;
	mem_rd_wr      <= left_rd_wr;
	mem_enable     <= internal_enable;
END ARCHITECTURE;