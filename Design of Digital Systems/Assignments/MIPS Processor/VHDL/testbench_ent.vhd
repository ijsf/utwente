LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY testbench IS
GENERIC(  
	CLOCK_PERIOD : time := 10 ns
);
END testbench;