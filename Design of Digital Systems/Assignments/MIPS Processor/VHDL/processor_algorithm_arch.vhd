LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY work;
USE work.instructions_pack.ALL;
USE work.functional_units_pack.ALL;

ARCHITECTURE algorithm OF processor IS 
BEGIN
	proc_proc: PROCESS
		CONSTANT reg_offset : std_logic_vector( 31 DOWNTO 0 ) := x"00003000";
		CONSTANT thirty_one : std_logic_vector( 4 DOWNTO 0 )  := "11111";
  
		PROCEDURE memory_read( addr : IN std_logic_vector; data : OUT std_logic_vector ) IS
		BEGIN
			address <= addr;
			rd_wr   <= '0';
			enable  <= '1';
			WAIT UNTIL rising_edge( clk );
			enable  <= '0';
			WAIT UNTIL rising_edge( clk );
			data := data_in;
		END;

		PROCEDURE memory_write( addr : IN std_logic_vector; data : IN std_logic_vector ) IS
		BEGIN
			address  <= addr;
			rd_wr    <= '1';
			enable   <= '1';
			data_out <= data;
			WAIT UNTIL rising_edge( clk );
			enable   <= '0';
		END;

		PROCEDURE reg_write( reg : IN std_logic_vector; data : IN std_logic_vector ) IS
		BEGIN
			memory_write( reg_offset( 31 DOWNTO 7 ) & reg & "00", data );
		END;
  
		PROCEDURE reg_read( reg : IN  std_logic_vector; dat : OUT std_logic_vector ) IS
		BEGIN
			memory_read( reg_offset( 31 DOWNTO 7 ) & reg & "00", dat );
		END;
  
		VARIABLE instruction    : std_logic_vector( 31 DOWNTO 0 );
		VARIABLE op_code        : std_logic_vector( 5 DOWNTO 0 );
		VARIABLE second_op_code : std_logic_vector( 10 DOWNTO 0 );
		
		VARIABLE i_26 : std_logic_vector( 25 DOWNTO 0 );
		VARIABLE i_16 : std_logic_vector( 15 DOWNTO 0 );
		VARIABLE s    : std_logic_vector( 31 DOWNTO 0 );
		VARIABLE t    : std_logic_vector( 31 DOWNTO 0 );
		VARIABLE d    : std_logic_vector( 31 DOWNTO 0 );
		
		VARIABLE PC  : std_logic_vector( 31 DOWNTO 0 ) := ( OTHERS => '0' ); 
		VARIABLE nPC : std_logic_vector( 31 DOWNTO 0 );  
		VARIABLE HI  : std_logic_vector( 31 DOWNTO 0 ); 
		VARIABLE LO  : std_logic_vector( 31 DOWNTO 0 );
		
		VARIABLE offset : signed( 31 DOWNTO 0 );  
		
		---- used for fuctional function calls
		
		VARIABLE carry_out : std_logic;  
		
		VARIABLE zero_bit : std_logic;  
		
		VARIABLE dummy_bit : std_logic;  
		VARIABLE dummy_32  : std_logic_vector( 31 DOWNTO 0 );
		CONSTANT FOUR 	   : std_logic_vector( 31 DOWNTO 0 ) := std_logic_vector( to_unsigned( 4, 32 ) );
		
		-- Division variables
		CONSTANT SUBTRACT    : std_logic := '1';
  		CONSTANT ADD         : std_logic := '0';
		CONSTANT ONE 	     : std_logic_vector( 31 DOWNTO 0 ) := std_logic_vector( to_unsigned( 1, 32 ) );
		CONSTANT ZERO        : std_logic_vector( 31 downto 0 ) := ( OTHERS => '0' );
		VARIABLE sub_add     : std_logic;
	    VARIABLE div_is_zero : std_logic;
	    
        VARIABLE A, M, Q : std_logic_vector( 31 DOWNTO 0 );

	    -- end division variables

		VARIABLE temp_reg1 : std_logic_vector( 31 DOWNTO 0 );		
	BEGIN
		WAIT UNTIL rising_edge( clk );
		
		loop_r: WHILE true LOOP
			IF reset = '1' THEN
				PC := ( OTHERS => '0' );
				HI := ( OTHERS => '0' );
				LO := ( OTHERS => '0' );
				
				data_out <= ( OTHERS => '0' );
				address  <= ( OTHERS => '0' );
				rd_wr    <= '0';
				enable   <= '0';
				syscall  <= '0';
				
				WAIT UNTIL rising_edge( clk );
			ELSE
				memory_read( PC, instruction );
				op_code := instruction( 31 DOWNTO 26 );
				
				add_sub(PC, FOUR, '0', nPC, dummy_bit); --nPC = PC + 4
     
				CASE op_code IS
					WHEN instr6_generic => -- generic
					second_op_code := instruction( 10 DOWNTO 0 );
					
					CASE second_op_code IS
						WHEN instr11_ADDU => --ADDU
							reg_read( instruction( 25 DOWNTO 21 ), s );
							reg_read( instruction( 20 DOWNTO 16 ), t );
							
							add_sub(s, t, '0', d, dummy_bit);
							
							reg_write( instruction( 15 DOWNTO 11 ), d ); 
						
						WHEN instr11_SRLV => --SRLV
							reg_read( instruction( 25 DOWNTO 21 ), s );
							reg_read( instruction( 20 DOWNTO 16 ), t );
							
                            -- only use 5 LSB, clear OTHERS
							s( 31 DOWNTO 5 ) := ( OTHERS => '0' ); 
							
							FOR i IN 1 TO to_integer( unsigned( s ) ) LOOP
                                rshift( t, '0', t );
							END LOOP;
							d := t;
							
							reg_write( instruction( 15 DOWNTO 11 ), d );
						
						WHEN instr11_JR => --JR
							reg_read( instruction( 25 DOWNTO 21 ), s );
							
							nPC := s;
						
						WHEN instr11_DIVU => --DIVU
                            reg_read(instruction( 25 DOWNTO 21 ), s ); -- Q (dividend)
							reg_read(instruction( 20 DOWNTO 16 ), t ); -- M (divisor)
							
							-- init
                            Q := s;
                            M := t;
                            A := ZERO;
                            
							-- Division by zero
							is_zero( M, div_is_zero );
                            
							IF div_is_zero = '1' THEN
							ELSE						
								----- algorithm UDIV
								FOR i IN 0 TO 31 LOOP
                                    lshift( A, Q( 31 ), A );
                                    add_sub( A, M, SUBTRACT, A, dummy_bit ); -- Add divider to hi
                                    
                                    IF dummy_bit = '1' THEN
                                        lshift( Q, '0', Q ); -- Shift quotient bit into result
                                        add_sub( A, M, ADD, A, dummy_bit ); -- Add divider to hi
                                    ELSE
                                        lshift( Q, '1', Q );
                                    END IF;
							    END LOOP;
					        END IF;
                              
                            HI := A;
                            LO := Q;
                            
                            --REPORT "END Division " & integer'image( to_integer( signed( s ) ) ) &" / " & integer'image( to_integer( signed( t ) ) ) & " = " & integer'image(to_integer( signed(LO))) & " mod " & integer'image(to_integer( signed(HI))) SEVERITY note; 
						
						WHEN instr11_MFHI => --MFHI
							d := HI;
							
							reg_write( instruction( 15 DOWNTO 11 ), d );
						
						WHEN instr11_MFLO => --MFLO
							d:=LO;
							
							reg_write( instruction( 15 DOWNTO 11 ), d );
							
						WHEN instr11_SYSCALL => -- SYSCALL kill simulation
							syscall <= '1';
						
						WHEN OTHERS => --error 
							REPORT "Unsupported Instruction" SEVERITY warning;
					END CASE;
						
					WHEN instr6_JAL => --JAL
						i_26 := instruction( 25 DOWNTO 0 );
						
						reg_write( thirty_one, std_logic_vector( nPC ) ); -- no delay slot, so +4 instead of +8
						nPC := PC( 31 DOWNTO 28 ) & i_26 & "00";
					
					WHEN instr6_SLTIU => --SLTIU
						reg_read( instruction( 25 DOWNTO 21 ), s );
						i_16 := instruction( 15 DOWNTO 0 );
						
						
						temp_reg1 := (OTHERS => '0');
						temp_reg1(15 DOWNTO 0) := i_16;					
						-- s < i <=> !((i - s) < 0)
						add_sub(s, temp_reg1, '1', dummy_32, carry_out);
						
						t:=( 0 => carry_out, OTHERS => '0' ); -- output 1 or 0
						
						reg_write( instruction( 20 DOWNTO 16 ), t );
					
					WHEN instr6_BGTZ => --BGTZ
						reg_read( instruction( 25 DOWNTO 21 ), s );
						i_16 := instruction( 15 DOWNTO 0 );
						
						is_zero( s , zero_bit);
						IF not(zero_bit or s(31)) = '1' THEN -- s > 0 <=> !(s==0 or s<0)
							offset := resize( signed( i_16 & "00" ), 32 );
							add_sub(nPC, std_logic_vector(offset), '0', nPC, dummy_bit); --add offset to nPC
						END IF;
					
					when instr6_LW => --LW
						reg_read(instruction(25 DOWNTO 21),s);
						i_16 := instruction(15 DOWNTO 0);
						offset := resize(signed(i_16), 32);
						
						add_sub(s, std_logic_vector(offset), '0', dummy_32, dummy_bit); --add offset to register
						memory_read(dummy_32,t); --offset is signed!
						
						reg_write(instruction(20 DOWNTO 16),t);
					
					WHEN instr6_SW => --SW
						reg_read( instruction( 25 DOWNTO 21 ), s );
						reg_read( instruction( 20 DOWNTO 16 ), t );
						i_16 := instruction( 15 DOWNTO 0 );
						offset := resize( signed( i_16 ), 32 );
						
						add_sub(s, std_logic_vector(offset), '0', dummy_32, dummy_bit); --add offset to register
						memory_write( dummy_32, t ); --offset is signed!
					
					WHEN OTHERS => --error
						REPORT "Unsupported Instruction" SEVERITY warning;
				END CASE; 
				
				PC := nPC;
			END IF;
		END LOOP; 
	END PROCESS;
END ARCHITECTURE;