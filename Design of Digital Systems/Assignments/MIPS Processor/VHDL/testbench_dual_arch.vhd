LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ARCHITECTURE dual OF testbench IS
	SIGNAL clk_i, reset_i : std_logic;
	
	SIGNAL nRD_WR_mem, enable_mem : std_logic;
	SIGNAL data_ci_mo, data_co_mi, address_mem: std_logic_vector( 31 DOWNTO 0 );
	
	SIGNAL clk_gold, nRD_WR_gold, enable_gold, syscall_gold : std_logic;
	SIGNAL data_gi_co, data_go_ci, address_gold: std_logic_vector( 31 DOWNTO 0 );
	
	SIGNAL clk_silver, nRD_WR_silver, enable_silver, syscall_silver : std_logic;
	SIGNAL data_si_co, data_so_ci, address_silver: std_logic_vector( 31 DOWNTO 0 );
    
    SIGNAL syscall_gs : std_logic;
	
	COMPONENT processor IS
		 PORT( 
			 clk       : IN  std_logic;
			 reset     : IN  std_logic;
			 data_in   : IN  std_logic_vector( 31 DOWNTO 0 ); 
			 data_out  : OUT std_logic_vector( 31 DOWNTO 0 ); 
			 address   : OUT std_logic_vector( 31 DOWNTO 0 ); 
			 rd_wr     : OUT std_logic; --( 0 = read, 1 = write )
			 enable    : OUT std_logic;
			 syscall   : OUT std_logic
		); 
	END COMPONENT;

	COMPONENT memory IS
	GENERIC( WORD_SIZE    : natural := 0;
			 CODE_FILE    : string  := "";
			 CODE_ADDRESS : natural := 0;
			 CODE_SIZE    : natural := 0;
			 DATA_FILE    : string  := "";
			 DATA_RESULT  : string  := "";
			 DATA_ADDRESS : natural := 0;
			 DATA_SIZE    : natural := 0;
			 REG_ADDRESS  : natural := 0;
			 NUM_REGS     : natural := 0
			);
	PORT(	 data_in	: IN  std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
			 data_out	: OUT std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
			 address	: IN  std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
			 nRD_WR		: IN  std_logic;
			 clk		: IN  std_logic;
			 enable		: IN  std_logic;
			 comp_mem	: IN  std_logic
		);
	END COMPONENT;
BEGIN
    syscall_gs <= syscall_gold AND syscall_silver;

 	clock : PROCESS
	BEGIN
		clk_i <= '1';
		WAIT FOR CLOCK_PERIOD / 2;
		clk_i <= '0';
		WAIT FOR CLOCK_PERIOD / 2;
		
		-- If the next instruction is a SYSCALL, then 
		-- toggle the clock one more time for the syscall
		-- signal to propagate to the memory.
		IF( syscall_gold = '1' AND syscall_silver = '1' ) THEN
			clk_i <= '1';
			WAIT FOR CLOCK_PERIOD / 2;
			clk_i <= '0';
			WAIT;
		END IF;
	END PROCESS;
	
	reset_proc : PROCESS
	BEGIN
		reset_i <= '1';
		WAIT FOR 2 * CLOCK_PERIOD;
		reset_i <= '0';
		WAIT;
	END PROCESS;
  
	proc_gold : processor 
 	PORT MAP( clk	   => clk_gold,
			  reset	   => reset_i,
			  data_in  => data_gi_co,
			  data_out => data_go_ci,
			  address  => address_gold,
			  rd_wr    => nRD_WR_gold,						
			  enable   => enable_gold,
			  syscall  => syscall_gold
			 );
			 
	proc_silver : processor 
	PORT MAP( clk	   => clk_silver,
			  reset	   => reset_i,
			  data_in  => data_si_co,
			  data_out => data_so_ci,
			  address  => address_silver,
			  rd_wr    => nRD_WR_silver,						
			  enable   => enable_silver,
			  syscall  => syscall_silver
			 );
  
	mem : memory
	PORT MAP( data_in  => data_co_mi,
			  data_out => data_ci_mo,
			  address  => address_mem,
			  nRD_WR   => nRD_WR_mem,
			  clk      => clk_i,
			  enable   => enable_mem,
			  comp_mem => syscall_gs
			 );

	cmp : ENTITY work.compare( behavior )
	PORT MAP( clk            => clk_i,
			  mem_data_out   => data_co_mi,
			  mem_data_in    => data_ci_mo, 
			  mem_address    => address_mem, 
			  mem_rd_wr      => nRD_WR_mem,	
			  mem_enable     => enable_mem,
			  
			  left_clk       => clk_gold,
			  left_data_out  => data_gi_co,
			  left_data_in   => data_go_ci,
			  left_address   => address_gold, 
			  left_rd_wr     => nRD_WR_gold,	
			  left_enable    => enable_gold,
			  left_syscall   => syscall_gold,
			  
			  right_clk      => clk_silver,
			  right_data_out => data_si_co, 
			  right_data_in  => data_so_ci,
			  right_address  => address_silver, 
			  right_rd_wr    => nRD_WR_silver,	
			  right_enable   => enable_silver,
			  right_syscall  => syscall_silver
			 ); 
END dual;