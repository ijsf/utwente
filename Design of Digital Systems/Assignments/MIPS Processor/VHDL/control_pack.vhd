library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

PACKAGE control_pack IS



subtype mux_hi_t is std_logic_vector(1 downto 0);
constant mux_hi_hi 		: mux_hi_t := "00";
constant mux_hi_alu 	: mux_hi_t := "01";
constant mux_hi_zero 	: mux_hi_t := "10";

subtype mux_lo_t is std_logic_vector(1 downto 0);
constant mux_lo_lo 		: mux_lo_t := "00";
constant mux_lo_shift 	: mux_lo_t := "01";
constant mux_lo_di 		: mux_lo_t := "10";

subtype mux_pc_t is std_logic_vector(1 downto 0);
constant mux_pc_pc 		: mux_pc_t := "00";
constant mux_pc_alu 	: mux_pc_t := "01";
constant mux_pc_di 		: mux_pc_t := "10";
constant mux_pc_intr 	: mux_pc_t := "11";

subtype mux_out_t is std_logic_vector(2 downto 0);
constant mux_out_out 		: mux_out_t := "000";
constant mux_out_alu 		: mux_out_t := "001";
constant mux_out_shift 		: mux_out_t := "010";
constant mux_out_sltiu 		: mux_out_t := "011";
constant mux_out_di			: mux_out_t := "100";

subtype mux_a_t is std_logic_vector(0 downto 0);
constant mux_a_a 		: mux_a_t := "0";
constant mux_a_di 		: mux_a_t := "1";

subtype mux_b_t is std_logic_vector(0 downto 0);
constant mux_b_b 		: mux_b_t := "0";
constant mux_b_di 		: mux_b_t := "1";
               	       
subtype mux_alu_left_t is std_logic_vector(1 downto 0);
constant mux_alu_left_a 		: mux_alu_left_t := "00";
constant mux_alu_left_pc		: mux_alu_left_t := "01";
constant mux_alu_left_hilo 		: mux_alu_left_t := "10";
constant mux_alu_left_hi 		: mux_alu_left_t := "11";

subtype mux_alu_right_t is std_logic_vector(1 downto 0);
constant mux_alu_right_b 		: mux_alu_right_t := "00";
constant mux_alu_right_intr		: mux_alu_right_t := "01";
constant mux_alu_right_four 	: mux_alu_right_t := "10";       
       
       
subtype mux_data_out_t is std_logic_vector(2 downto 0);       
constant mux_data_out_out 		: mux_data_out_t := "000";
constant mux_data_out_a 		: mux_data_out_t := "001";
constant mux_data_out_hi 		: mux_data_out_t := "010";
constant mux_data_out_lo 		: mux_data_out_t := "011";
constant mux_data_out_pc 		: mux_data_out_t := "100";
     
     
subtype mux_address_t is std_logic_vector(1 downto 0);
constant mux_address_pc 		: mux_address_t := "00";
constant mux_address_out		: mux_address_t := "01";
constant mux_address_reg 		: mux_address_t := "10";   
  
                      
END;

PACKAGE BODY control_pack IS


                       
END;
