LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ARCHITECTURE single OF testbench IS
	COMPONENT processor IS
		PORT( clk      : IN  std_logic;
			  reset    : IN  std_logic;
			  data_in  : IN  std_logic_vector( 31 DOWNTO 0 ); 
			  data_out : OUT std_logic_vector( 31 DOWNTO 0 ); 
			  address  : OUT std_logic_vector( 31 DOWNTO 0 ); 
			  rd_wr    : OUT std_logic; --( 0 = read, 1 = write )
			  enable   : OUT std_logic;
			  syscall  : OUT std_logic
		); 
	END COMPONENT;

	COMPONENT memory IS
		GENERIC( WORD_SIZE 		: natural := 0;
				 CODE_FILE  	: string  := "";
				 CODE_ADDRESS 	: natural := 0;
				 CODE_SIZE 		: natural := 0;
				 DATA_FILE 		: string  := "";
				 DATA_RESULT	: string  := "";
				 DATA_ADDRESS 	: natural := 0;
				 DATA_SIZE 		: natural := 0
				);  
		PORT(	 data_in 		: IN  std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
				 data_out		: OUT std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
				 address		: IN  std_logic_vector( WORD_SIZE - 1 DOWNTO 0 );
				 nRD_WR			: IN  std_logic;
				 clk			: IN  std_logic;
				 enable			: IN  std_logic;
				 comp_mem		: IN  std_logic
			 );
	END COMPONENT;

	SIGNAL clk_i, reset_i, nRD_WR_i, enable_i, syscall_i : std_logic;
	SIGNAL data_mi_po, data_mo_pi, address_i: std_logic_vector( 31 DOWNTO 0 );
BEGIN
	clock : PROCESS
	BEGIN
		clk_i <= '1';
		WAIT FOR CLOCK_PERIOD / 2;
		clk_i <= '0';
		WAIT FOR CLOCK_PERIOD / 2;
		
		-- If the next instruction is a SYSCALL, then 
		-- toggle the clock one more time for the syscall
		-- signal to propagate to the memory.
		IF( syscall_i = '1' ) THEN
			clk_i <= '1';
			WAIT FOR CLOCK_PERIOD / 2;
			clk_i <= '0';
			WAIT;
		END IF;
	END PROCESS;
	
	reset_proc : PROCESS
	BEGIN
		reset_i <= '1';
		WAIT FOR 2 * CLOCK_PERIOD;
		reset_i <= '0';
		WAIT;
	END PROCESS;

	pr : processor 
	PORT MAP( clk	   => clk_i,
			  reset	   => reset_i,
			  data_in  => data_mo_pi,
			  data_out => data_mi_po,
			  address  => address_i,
			  rd_wr    => nRD_WR_i,						
			  enable   => enable_i,
			  syscall  => syscall_i
			 );
  
	mem : memory
	PORT MAP( data_in  => data_mi_po,
			  data_out => data_mo_pi,
			  address  => address_i,
			  nRD_WR   => nRD_WR_i,
			  clk      => clk_i,
			  enable   => enable_i,
			  comp_mem => syscall_i
			 );
END ARCHITECTURE single;