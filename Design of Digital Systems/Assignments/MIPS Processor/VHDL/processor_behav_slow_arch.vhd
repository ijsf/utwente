LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.all;

LIBRARY work;
USE work.instructions_pack.ALL;

ARCHITECTURE behavior_slow OF processor IS 
BEGIN
	proc_proc: PROCESS
		CONSTANT reg_offset : std_logic_vector( 31 DOWNTO 0 ) := x"00003000";
		CONSTANT thirty_one : std_logic_vector( 4 DOWNTO 0 )  := "11111";
		CONSTANT wait_cycles: natural := 2;
  
		PROCEDURE memory_read( addr : IN std_logic_vector; data : OUT std_logic_vector ) IS
		BEGIN
			address <= addr;
			rd_wr   <= '0';
			enable  <= '1';
			WAIT UNTIL rising_edge( clk );
			enable  <= '0';
			WAIT UNTIL rising_edge( clk );
			data := data_in;
		END;

		PROCEDURE memory_write( addr : IN std_logic_vector; data : IN std_logic_vector ) IS
		BEGIN
			address  <= addr;
			rd_wr    <= '1';
			enable   <= '1';
			data_out <= data;
			WAIT UNTIL rising_edge( clk );
			enable   <= '0';
		END;

		PROCEDURE reg_write( reg : IN std_logic_vector; data : IN std_logic_vector ) IS
		BEGIN
			memory_write( reg_offset( 31 downto 7 ) & reg & "00", data );
		END;
  
		PROCEDURE reg_read( reg : IN  std_logic_vector; dat : OUT std_logic_vector ) IS
		BEGIN
			memory_read( reg_offset( 31 downto 7 ) & reg & "00", dat );
		END;
  
		PROCEDURE add( src1 : IN std_logic_vector; src2 : IN std_logic_vector; sum : OUT std_logic_vector ) IS
		BEGIN
			sum := std_logic_vector( unsigned( src1 ) + unsigned( src2 ) );
			IF TRUE THEN --do not know how to detect overflow
			ELSE
				ASSERT FALSE REPORT "ADD overflow" SEVERITY failure;
			END IF;    
		END;
		
		PROCEDURE slowdown( num_cycles : IN natural ) IS
		BEGIN
			FOR i IN 1 TO num_cycles LOOP
				WAIT UNTIL rising_edge( clk );
			END LOOP;
		END;

		VARIABLE instruction    : std_logic_vector( 31 downto 0 );
		VARIABLE op_code        : std_logic_vector( 5 downto 0 );
		VARIABLE second_op_code : std_logic_vector( 10 downto 0 );
		
		VARIABLE i_26 : std_logic_vector( 25 downto 0 );
		VARIABLE i_16 : std_logic_vector( 15 downto 0 );
		VARIABLE s    : std_logic_vector( 31 downto 0 );
		VARIABLE t    : std_logic_vector( 31 downto 0 );
		VARIABLE d    : std_logic_vector( 31 downto 0 );
		
		VARIABLE PC  : std_logic_vector( 31 downto 0 ) := ( others => '0' ); 
		VARIABLE nPC : std_logic_vector( 31 downto 0 );  
		VARIABLE HI  : std_logic_vector( 31 downto 0 ); 
		VARIABLE LO  : std_logic_vector( 31 downto 0 );
		
		VARIABLE offset : signed( 31 downto 0 );  
	BEGIN
		WAIT UNTIL rising_edge( clk );
		
		loop_r: WHILE true LOOP
			IF reset = '1' THEN
				PC := ( others => '0' );
				HI := ( others => '0' );
				LO := ( others => '0' );
				
				data_out <= ( others => '0' );
				address  <= ( others => '0' );
				rd_wr    <= '0';
				enable   <= '0';
				syscall  <= '0';
				
				WAIT UNTIL rising_edge( clk );
			ELSE
				memory_read( PC, instruction );
				op_code := instruction( 31 downto 26 );
    
				nPC := std_logic_vector( unsigned( PC ) + 4 );
     
				CASE op_code IS
					WHEN instr6_generic => -- generic
					second_op_code := instruction( 10 DOWNTO 0 );
					
					CASE second_op_code IS
						WHEN instr11_ADDU => --ADDU
							reg_read( instruction( 25 DOWNTO 21 ), s );
							reg_read( instruction( 20 DOWNTO 16 ), t );
							add( s, t, d );
							slowdown( wait_cycles );
							reg_write( instruction( 15 downto 11 ), d ); 
						
						WHEN instr11_SRLV => --SRLV
							reg_read( instruction( 25 downto 21 ), s );
							reg_read( instruction( 20 downto 16 ), t );
							d:= std_logic_vector( unsigned( t ) / ( 2 ** to_integer( unsigned( s( 4 downto 0 ) ) ) ) );
							slowdown( wait_cycles );
							reg_write( instruction( 15 downto 11 ), d );
						
						WHEN instr11_JR => --JR
							reg_read( instruction( 25 downto 21 ), s );
							nPC := s;
						
						WHEN instr11_DIVU => --DIVU
							reg_read( instruction( 25 downto 21 ), s );
							reg_read( instruction( 20 downto 16 ), t );
                            
                            -- Divide by zero implemented as Mars
                            -- handles it (though it is incorrect).
                            IF unsigned( t ) = 0 THEN
                                HI := ( others => '0' );
                                LO := s;
                            ELSE
                                HI := std_logic_vector( unsigned( s ) mod unsigned( t ) );
                                LO := std_logic_vector( unsigned( s ) / unsigned( t ) );
                            END IF;
                            
                            slowdown( wait_cycles );
						
						WHEN instr11_MFHI => --MFHI
							d := HI;
							slowdown( wait_cycles );
							reg_write( instruction( 15 downto 11 ), d );
						
						WHEN instr11_MFLO => --MFLO
							d:=LO;
							slowdown( wait_cycles );
							reg_write( instruction( 15 DOWNTO 11 ), d );
						
						WHEN instr11_SYSCALL => --SYSCALL kill simulation
							syscall <= '1';
							--REPORT "SYSCALL kill simulation" SEVERITY FAILURE;
						
						WHEN OTHERS => --error 
							REPORT "Unsupported Instruction" SEVERITY warning;
					END CASE;
						
					WHEN instr6_JAL => --JAL
						i_26 := instruction( 25 downto 0 );
						reg_write( thirty_one, std_logic_vector( nPC ) ); -- no delay slot, so +4 instead of +8
						nPC := PC( 31 DOWNTO 28 ) & i_26 & "00";
					
					WHEN instr6_SLTIU => --SLTIU
						reg_read( instruction( 25 downto 21 ), s );
						i_16 := instruction( 15 downto 0 );
						slowdown( wait_cycles );
						IF unsigned( s ) < unsigned( i_16 ) THEN
							t:=( 0 => '1', OTHERS => '0' );
						ELSE 
							t := ( OTHERS => '0' );
						END IF;
						reg_write( instruction( 20 downto 16 ), t );
					
					WHEN instr6_BGTZ => --BGTZ
						reg_read( instruction( 25 downto 21 ), s );
						i_16 := instruction( 15 downto 0 );
						slowdown( wait_cycles );
						IF signed( s ) > 0 THEN
							offset := resize( signed( i_16 & "00" ), 32 );
							nPC := std_logic_vector( signed( nPC ) + offset ); --offset is signed!
						END IF;
					
					when instr6_LW => --LW
						reg_read(instruction(25 downto 21),s);
						i_16 := instruction(15 downto 0);
						offset := resize(signed(i_16), 32);
						memory_read(std_logic_vector(signed(s)+offset),t); --offset is signed!
						reg_write(instruction(20 downto 16),t);
					
					WHEN instr6_SW => --SW
						reg_read( instruction( 25 downto 21 ), s );
						reg_read( instruction( 20 downto 16 ), t );
						i_16 := instruction( 15 downto 0 );
						offset := resize( signed( i_16 ), 32 );
						memory_write( std_logic_vector( signed( s ) + offset ), t ); --offset is signed!
					
					WHEN OTHERS => --error
						REPORT "Unsupported Instruction" SEVERITY warning;
				END CASE; 
				
				PC := nPC;
			END IF;
		END LOOP; 
	END PROCESS;
END;