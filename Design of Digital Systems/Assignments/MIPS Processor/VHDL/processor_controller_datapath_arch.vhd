LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY work;
USE work.control_pack.ALL;

ARCHITECTURE controller_datapath OF processor IS 
	SIGNAL mux_hi			:  mux_hi_t;
	SIGNAL mux_lo			:  mux_lo_t;
	SIGNAL mux_pc			:  mux_pc_t;
	SIGNAL mux_out			:  mux_out_t;
	SIGNAL mux_a			:  mux_a_t;
	SIGNAL mux_b			:  mux_b_t;
	SIGNAL mux_data_out		:  mux_data_out_t;
	SIGNAL mux_address		:  mux_address_t;
	SIGNAL mux_alu_left		:  mux_alu_left_t;
	SIGNAL mux_alu_right	:  mux_alu_right_t;
	
	SIGNAL alu_add_sub 		: std_logic;
		  
	SIGNAL address_reg  	: std_logic_vector( 31 DOWNTO 0 ); 
	SIGNAL intr_right 		: std_logic_vector( 31 DOWNTO 0 ); 
		  
	SIGNAL intr_pc			: std_logic_vector( 25 DOWNTO 0 ); 	  
		  	
	SIGNAL a_zero			: std_logic;
	SIGNAL a_msb			: std_logic;
		  
	SIGNAL alu_c			: std_logic;
	SIGNAL alu_msb			: std_logic;

	COMPONENT datapath IS 
	PORT( clk      : IN  std_logic;
		  reset    : IN  std_logic;
		  data_in  : IN  std_logic_vector( 31 DOWNTO 0 ); 
		  
		  data_out : OUT std_logic_vector( 31 DOWNTO 0 ); 
		  address  : OUT std_logic_vector( 31 DOWNTO 0 ); 
		  	  
		  mux_hi	: IN mux_hi_t;
		  mux_lo	: IN mux_lo_t;
		  mux_pc	: IN mux_pc_t;
		  
		  mux_out	: IN mux_out_t;
		  mux_a		: IN mux_a_t;
		  mux_b		: IN mux_b_t;
		  
		  mux_data_out	: IN mux_data_out_t;
		  mux_address	: IN mux_address_t;
		  
		  mux_alu_left	: IN mux_alu_left_t;
		  mux_alu_right	: IN mux_alu_right_t;
		  
		  alu_add_sub 	: IN std_logic;
		  
		  address_reg  	: IN std_logic_vector( 31 DOWNTO 0 ); 	  
		  intr_right 	: IN std_logic_vector( 31 DOWNTO 0 ); 
		  
		  intr_pc		: IN std_logic_vector( 25 DOWNTO 0 ); 	  
		  	
		  a_zero	: OUT std_logic;
		  a_msb		: OUT std_logic;
		  
		  alu_c		: OUT std_logic;
		  alu_msb	: OUT std_logic
		); 
	END COMPONENT;

	COMPONENT controller IS 
	PORT( clk      : IN  std_logic;
		  reset    : IN  std_logic;
		  data_in  : IN  std_logic_vector( 31 DOWNTO 0 ); 
		 
		  	  
		  mux_hi	: OUT mux_hi_t;
		  mux_lo	: OUT mux_lo_t;
		  mux_pc	: OUT mux_pc_t;
		  
		  mux_out	: OUT mux_out_t;
		  mux_a		: OUT mux_a_t;
		  mux_b		: OUT mux_b_t;
		  
		  mux_data_out	: OUT mux_data_out_t;
		  mux_address	: OUT mux_address_t;
		  
		  mux_alu_left	: OUT mux_alu_left_t;
		  mux_alu_right	: OUT mux_alu_right_t;
		  
		  alu_add_sub 	: OUT std_logic;
		  
		  address_reg  	: OUT std_logic_vector( 31 DOWNTO 0 ); 
		  intr_right 	: OUT std_logic_vector( 31 DOWNTO 0 ); 
		  
		  intr_pc		: OUT std_logic_vector( 25 DOWNTO 0 ); 	  
		  	
		  a_zero	: IN std_logic;
		  a_msb		: IN std_logic;
		  
		  alu_c		: IN std_logic;
		  alu_msb	: IN std_logic;
		  
		  		  -- Memory access
		  rd_wr    : OUT std_logic; --( 0 = read, 1 = write )
		  enable   : OUT std_logic;
		  
		  syscall  : OUT std_logic
 		); 
	END COMPONENT;
BEGIN
controller_comp : controller
	PORT MAP( clk           => clk,
              reset         => reset,
              data_in       => data_in,
                    
              mux_hi	    => mux_hi,
              mux_lo	    => mux_lo,
              mux_pc	    => mux_pc,
              mux_out	    => mux_out,
              mux_a		    => mux_a,
              mux_b		    => mux_b,
              mux_data_out	=> mux_data_out,
              mux_address	=> mux_address,
              mux_alu_left	=> mux_alu_left,
              mux_alu_right	=> mux_alu_right,
              
              alu_add_sub 	=> alu_add_sub,
              
              address_reg  	=> address_reg,
              intr_right 	=> intr_right,
              
              intr_pc		=> intr_pc,
                
              a_zero	    => a_zero,
              a_msb		    => a_msb,
                
              alu_c		    => alu_c,
              alu_msb	    => alu_msb,
              
              -- Memory access
              rd_wr         => rd_wr,
              enable        => enable,
		  
              syscall       => syscall
		); 
		
datapath_comp : datapath
	PORT MAP( 
		  clk      => clk,
		  reset    => reset,
		  data_in  => data_in,
		  
		  data_out => data_out, 
		  address  => address,
		  	  
		  mux_hi	=> mux_hi,
		  mux_lo	=> mux_lo,
		  mux_pc	=> mux_pc,
		  mux_out	=> mux_out,
		  mux_a		=> mux_a,
		  mux_b		=> mux_b,
		  mux_data_out	=> mux_data_out,
		  mux_address	=> mux_address,
		  mux_alu_left	=> mux_alu_left,
		  mux_alu_right	=> mux_alu_right,
		  
		  alu_add_sub 	=> alu_add_sub,
		  
		  address_reg  	=> address_reg,
		  intr_right 	=> intr_right,
		  
		  intr_pc		=> intr_pc,
		  	
		  a_zero	=> a_zero,
		  a_msb		=> a_msb,
		  
		  alu_c		=> alu_c,
		  alu_msb	=> alu_msb
		); 
END ARCHITECTURE;