#include <helix.h>
#include <render/render.h>
#include <stdint.h>
#include <sys/time.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include "sprites.c"
#include "sprites_cockpit.c"

#define ERREXIT(str) {fprintf(stderr, "Error: " str "\n"); exit(1);}

#define ERREXIT2(str, ...) {fprintf(stderr, "Error: " str "\n", __VA_ARGS__); exit(1);}

#define ERRTHREAD(str, ...) {fprintf(stderr, "Error: " str "\n", __VA_ARGS__); return NULL;}

#define NUM_THREADS				31
#define CACHE_SIZE_BYTES		(CACHELINE_ALIGN)

// Screen resolution
#define SCREEN_HEIGHT		( (DVI_HEIGHT) / 2 )
#define SCREEN_HEIGHT_START	( (SCREEN_HEIGHT) - ( (SCREEN_HEIGHT) / 2 ) )
#define SCREEN_WIDTH		( (DVI_WIDTH) )
#define SCREEN_WIDTH_START	0

// Maximum number of particles per bin (scanline)
#define BIN_MAX_PARTICLES		100

// Particle definitions
#define MAX_PARTICLE_WIDTH	32
#define MAX_PARTICLE_HEIGHT	(MAX_PARTICLE_WIDTH)
#define MAX_PARTICLE_RADIUS	(MAX_PARTICLE_WIDTH/2)

// Screen border dimensions (defined by particle radius)
#define SCREEN_BORDER_WIDTH		(MAX_PARTICLE_RADIUS)
#define SCREEN_BORDER_HEIGHT	(MAX_PARTICLE_RADIUS)

// Global particle size (must not exceed MAX_PARTICLE_WIDTH and MAX_PARTICLE_HEIGHT)
#define PARTICLE_SIZE		32

// Maximum number of entities
#define MAX_ENTITIES		512

// Uses circle-shaped particles if defined
#define PARTICLE_SHAPE_CIRCLES

// Pass debugging
//#define DEBUG_HORIZONTAL_PASS
//#define PARTICLE_DUPLICATION_TEST

const size_t sharedMemLocalSize = sizeof( dvi_pixel ) * SCREEN_WIDTH;
const size_t localMemSize = 4096 * 2;

/*
 * Structure for custom thread-based memory sharing (mailboxes)
 */
struct sharedMem_t
{
	volatile void * pNOC;
	volatile void * pLocal;
};

struct thInfo
{
	uint32_t id;
};

typedef void *pthread_function( void * );

thInfo info[ NUM_THREADS ];

inline long unsigned int getMicroSeconds( void )
{
	struct timeval currenttime;
	gettimeofday( &currenttime, NULL );
	return ( currenttime.tv_usec + currenttime.tv_sec * 1000000 );
}

pthread_barrier_t bar;

#undef blue
#undef green
#undef red

// Entity list structure
typedef uint32_t entity_list_size;
struct entity_list
{
	volatile struct entity_info* entities;
	volatile entity_list_size size;
};
// Entity structure, used for bookkeeping of entities in the world
struct entity_info
{
	/*
	 * Coordinates
	 */
	float x, y, z;
	
	/*
	 * Palette
	 */
	uint8_t palette;
	
	/*
	 * Delta coordinates
	 */
	float dx, dy;
	float ddx, ddy;
	uint8_t dz;
};

// Particle render information structure, used for storage in bins
struct particle_info
{
	/*
	 * Screen-space x coordinate
	 */
	uint16_t x;
	
	/*
	 * Depth coordinate
	 * 
	 * 0 = closest to screen plane.
	 * 
	 * Type must correspond with particle_footprint.data.z
	 */
	uint8_t z;
	
	/*
	 * Palette
	 */
	uint8_t palette;
	
	/*
	 * Due to the scanline-based algorithm, we enforce a constant particle size for all particles.
	 * 
	 * By guaranteeing that particles that are further away are always smaller, the particle rendering
	 * case is considerably easier, resulting in significantly increased performance.
	 * 
	 * Additionally, the screen-space y coordinate is not necessary as this can be implied from the bin in which the particle is located.
	 */
};

// Particle list structure
typedef uint8_t particle_bin_size;
struct particle_bin
{
	volatile struct particle_info*	particles;
	volatile particle_bin_size		size;
};
struct particle_list
{
	/*
	 * List of regular particles, binned in scanlines
	 * 
	 * Volatile because these bins are read and written by different threads at the same time.
	 */
	volatile particle_bin bins[SCREEN_HEIGHT];
};

// Particle footprint structure (encoded in 32-bit pixel data)
struct particle_footprint
{
	union
	{
		dvi_pixel pixel;	// 32-bit
		
		/*
		 * Below structure must be optimized for alignment (e.g. to 1, 2 or 4 bytes) due to performance.
		 * 
		 * Unaligned access in this structure will result in additional and costly bit shifting.
		 */
		struct
		{
			/*
			 * Local horizontal coordinate
			 *
			 * For use after horizontal expansion pass, during vertical expansion pass.
			 * Maximum value of ix must be smaller than MAX_PARTICLE_WIDTH.
			 * 
			 * Initial value must be 0 (center/footprint pixel).
			 * 
			 * Minimal number of necessary bits: 4
			 */
			unsigned int ix : 16;
			
			/*
			 * Depth coordinate
			 * 
			 * Used for particle depth testing, and determines the depth resolution.
			 * Type must correspond with particle_info.z
			 * 
			 * 0 = closest to screen plane.
			 * 
			 * Minimal number of necessary bits: 8
			 */
			unsigned int z : 8;
			
			/*
			 * Intensity level
			 * 
			 * Greyscale color shading value of particle.
			 * Upper bounded by lowest common color resolution of framebuffer (RGB565 -> 5-bit)
			 * 
			 * Minimal number of necessary bits: 5
			 */
			unsigned int intensity : 4;
			
			/*
			 * Palette value
			 * 
			 * Determines the color of the particle.
			 * Currently supports 4 different palettes.
			 * 
			 * Minimal number of necessary bits: 2
			 */
			unsigned int palette : 4;
			
		} data;
	};
};

// Particle queue structure
struct particle_queue
{
	/*
	 * Particle footprint data
	 */
	particle_footprint	footprint;
	
	/*
	 * Local coordinate storage.
	 * 
	 * ia is the absolute position of the start of the footprint.
	 * ib is the end position of the footprint relative to ia.
	 */
	uint16_t			ia;
	uint8_t				ib;
};

/*
 * Global memory
 * 
 * Instances that are among threads below.
 */
sharedMem_t		g_sharedMem[NUM_THREADS];
particle_list	g_particleList;
entity_list		g_entityList;
uint16_t		g_spawnChance;
float			g_timeStart;

#if 0
// Use RGB565 (at the cost of performance)
struct rgb565_pixel
{
	union
	{
		uint16_t raw;
		
		struct
		{
			unsigned int b : 5;
			unsigned int g : 6;
			unsigned int r : 5;
		} rgb_exact;
	} data;
	
	// RGB565 -> dvi_pixel
	operator dvi_pixel () const
	{
		return PIX_CONST(
			this->data.rgb_exact.r << 3,
			this->data.rgb_exact.g << 2,
			this->data.rgb_exact.b << 3
		);
	}
	
	// dvi_pixel -> RGB565
	rgb565_pixel( const dvi_pixel & pixel )
	{
		this->data.rgb_exact.r = pixel.r >> 3;
		this->data.rgb_exact.g = pixel.g >> 2;
		this->data.rgb_exact.b = pixel.b >> 3;
	}
};
#else
// Use RGB888
#define rgb565_pixel dvi_pixel
#endif

/*
 * Original (very slow) pthread synchronization
 */
#define phase()	\
	do{ \
		int res=pthread_barrier_wait(&bar); \
		switch(res){ \
			case 0: \
			case PTHREAD_BARRIER_SERIAL_THREAD: \
			break; \
			default: \
			printf("Error in barrier: %d, %s\n",res,strerror(res)); \
			abort(); \
		}	\
	} while(0);

/*
 * Custom lockstep barrier using mailboxes for improved synchronization performance
 */
void lockstepBarrier( unsigned int uniqueIdentifier, const bool step, const unsigned int core )
{
	// Convert uniqueIdentifier to unique lock-stepped identifier
	uniqueIdentifier = uniqueIdentifier * 2 + ( step ? 1 : 0 );
	
	// Write to all threads
	for( unsigned int i = 0; i < NUM_THREADS; ++i )
	{
		if( i != core )
		{
			*(((volatile unsigned int *)g_sharedMem[i].pNOC) + core) = uniqueIdentifier;
		}
	}
	
	while( 1 )
	{
		int syncCountCorrect = 0;
		for( unsigned int i = 0; i < NUM_THREADS; ++i )
		{
			volatile unsigned int l = *(((volatile unsigned int *)g_sharedMem[core].pLocal) + i);

			if( i != core )
			{
				/*
				 * At this point, we're waiting for all threads to either write the current or the next unique identifier
				 * 
				 * In case the next identifier is written, that particular thread is already ahead, but synchronization
				 * is still achieved because the thread will be waiting for the current (lagging) thread to write the
				 * next identifier during the next lock-step barrier call.
				 */
				if( l == uniqueIdentifier || (l == (uniqueIdentifier + 1)) )
				{
					++syncCountCorrect;
				}
			}
		}
		
		if( syncCountCorrect == ( NUM_THREADS - 1 ) )
		{
			// all threads ready
			break;
		}
	}
}

/*
 * Barrier macro
 */
#if 1
#define CUSTOM_BARRIER( _id )	{ lockstepBarrier(_id, false, threadId); lockstepBarrier(_id, true, threadId); }
#else
#define CUSTOM_BARRIER( _id )	phase()
#endif

#define ALPHA_COLOR		PIX_CONST( 90, 0, 170 )
#define COCKPIT_WIDTH	640
#define COCKPIT_HEIGHT	276
#define COCKPIT_START_X	0
#define COCKPIT_START_Y	203

/*!
 * \brief Copies an image to the frame buffer.
 * \param rawimage pointer to the source image
 * \param width width of the source
 * \param height height of the source
 * \param x_offset x coordinate of the destination
 * \param y_offset y coordinate of the destination
 * \param alpha_color color value that should be treated as alpha
 * \param screen dvi_screen that should be used
 * \return 0 on success, otherwise an errno
 */
int drawimage( const dvi_pixel *rawimage,
			   const uint16_t   width, 
			   const uint16_t   height, 
			   const uint16_t   x_offset, 
			   const uint16_t   y_offset,
			   const dvi_pixel  alpha_color,
					 dvi_frame *frame )
{
	if( !rawimage )
	{
		return EINVAL;
	}
	
	dvi_pixel pixel;

	for( uint16_t y = 0; y < height; ++y )
	{
		for( uint16_t x = 0; x < width; ++x )
		{
			pixel = rawimage[ y * width + x ];
			
			if( pixel.r != alpha_color.r &&
				pixel.g != alpha_color.g &&
				pixel.b != alpha_color.b )
			{
				frame->line[ y + y_offset ].col[ x + x_offset ] = pixel;
			}
		}
	}
	
	return 0;
}

/*
 * Palette to RGB
 */
void paletteToRGB( uint8_t palette, uint8_t& r, uint8_t& g, uint8_t& b )
{
	switch( palette )
	{
		case 0:
			r = 0xFF;
			g = 0xD7;
			b = 0xD7;
			break;
		case 1:
			r = 0xFF;
			g = 0xE9;
			b = 0xD7;
			break;
		case 2:
			r = 0xB5;
			g = 0xD7;
			b = 0xD7;
			break;
		default:
			r = 0xC6;
			g = 0xEB;
			b = 0xC6;
			break;
	}
}

/*
 * Depth conversion
 * 
 * Full depth range: 1 (closer) to 255 (farther)
 * Full radius range: 1 to MAX_PARTICLE_RADIUS
 */
uint8_t depthToRadius( uint8_t depth )
{
	if( depth > 0 )
	{
		return (MAX_PARTICLE_RADIUS - ((depth + 1) / ( MAX_PARTICLE_RADIUS )));
	}
	else
	{
		// Invalid depth
		return 0;
	}
}

/*
 * Point-in-circle test for signed values [-radius, radius]
 */
inline bool pointInCircle( int16_t x, int16_t y, uint8_t radius )
{
	return ((x*x + y*y) <= (radius*radius));
}

/*
 * Point-in-circle test for unsigned values [0, radius * 2 + 1]
 */
inline bool pointInCircleAbs( int16_t x, int16_t y, uint8_t radius )
{
	// x and y are [0, radius*2+1]
	x -= radius - 1;
	y -= radius - 1;
	return ((x*x + y*y) <= (radius*radius));
}

/*
 * Mathematical macros
 */
#define CLAMP( _x, _a, _b ) ((_x)<(_a))?(_a):((_x)>(_b))?(_b):(_x)
#define MAX( _x, _a ) ((_x)<(_a))?(_a):(_x)
#define FABS( _x ) ((_x<0)?(-_x):(_x))

/*
 * Spawns an entity at a specific index in the entity list
 */
void spawnEntity( const entity_list_size entityIndex )
{
	/*
	 * Determine spawn by chance
	 * 
	 * Start with 1 because the first ever rand() call may be zero due to reasons unknown.
	 */
	if( (rand() % g_spawnChance) != 1 )
	{
		/*
		 * Remove entity
		 */
		((entity_info*)g_entityList.entities)[entityIndex].z = 0;
		return;
	}
	
	/*
	 * Spawn configuration
	 */
	const uint16_t xBorder = 64, yBorder = 32;
	const uint16_t xMin = xBorder, xMax = SCREEN_WIDTH - xBorder;
	const uint16_t yMin = yBorder, yMax = SCREEN_HEIGHT - yBorder;
	const int16_t zMin = 200, zMax = 255, zMaxSpeed = 3;
	
	// Determine mid of screen
	const uint16_t xMid = xMin + ((xMax - xMin) / 2);
	const uint16_t yMid = yMin + ((yMax - yMin) / 2);

	// Spawn entity at random x,y,z coordinates
	entity_info entityInfo;
	entityInfo.x = (float)(xMin + (rand() % (xMax - xMin))) - xMid;
	entityInfo.y = (float)(yMin + (rand() % (yMax - yMin))) - yMid;
	entityInfo.z = (float)(zMin + (rand() % (zMax - zMin)));
	
	// Determine random palette
	entityInfo.palette = rand() % 4;

	float xcoeff = entityInfo.x > 0 ? 1 : -1;
	float ycoeff = entityInfo.y > 0 ? 1 : -1;
	
	if( FABS( entityInfo.x ) > FABS( entityInfo.y ) )
	{
		entityInfo.dx = 1;
		entityInfo.dy = FABS( entityInfo.y / entityInfo.x );
	}
	else
	{
		entityInfo.dx = FABS( entityInfo.x / entityInfo.y );
		entityInfo.dy = 1;
	}
	
	entityInfo.dx *= xcoeff;
	entityInfo.dy *= ycoeff;
	entityInfo.dz = 1 + (rand() % zMaxSpeed);
	
	entityInfo.ddx = 0.1 * entityInfo.dx;
	entityInfo.ddy = 0.1 * entityInfo.dy;
	
	entityInfo.x += xMid;
	entityInfo.y += yMid;
	
	// Assign entity to list
	((entity_info*)g_entityList.entities)[entityIndex] = entityInfo;
}

/*
 * Moves all entities
 */
void think( const entity_list_size start, const entity_list_size end )
{
	/*
	 * Cull configuration
	 */
	const uint16_t xBorder = 2, yBorder = 2;
	const uint16_t xMin = xBorder, xMax = SCREEN_WIDTH - xBorder;
	const uint16_t yMin = yBorder, yMax = SCREEN_HEIGHT - yBorder;
	const int16_t zMin = 1, zMax = 255;
	
	for( entity_list_size i = start; i < end; ++i )
	{
		volatile entity_info& entityInfo = g_entityList.entities[i];
			
		// Check for valid entity
		if( entityInfo.z != 0 )
		{
			entityInfo.x += entityInfo.dx;
			entityInfo.y += entityInfo.dy;
			entityInfo.z -= entityInfo.dz;
			
			entityInfo.dx += entityInfo.ddx;
			entityInfo.dy += entityInfo.ddy;
		}
		
		// Check if entity is still valid
		if( entityInfo.x < xMin || entityInfo.x >= xMax || entityInfo.y < yMin || entityInfo.y >= yMax || entityInfo.z < zMin || entityInfo.z >= zMax )
		{
			// Respawn entity
			spawnEntity( i );
			mc_flush( g_entityList.entities[i] );
		}
	}
	
	FlushDCache();
}

/*
 * Initializes a list of entities and generates the corresponding entities.
 */
void initializeEntityList()
{
	// Allocate entity list for maximum number of entities
	g_entityList.entities = (entity_info*)malloc( sizeof( entity_info ) * MAX_ENTITIES );
	g_entityList.size = MAX_ENTITIES;
	
	/*
	 * Clear all initial entities
	 */
	const entity_list_size entityListSize = g_entityList.size;
	for( entity_list_size i = 0; i < entityListSize; ++i )
	{
		((entity_info*)g_entityList.entities)[i].z = 0;
	}
	
	FlushDCache();
}

/*
 * Generate pass
 * 
 * Single-threaded due to the serialized nature of particle generation.
 * 
 * 1. Iterate over particle list and group particles into designated scanline binned particle lists.
 */
void scGenerate( void * pLocal, int threadId )
{
	if( threadId == 0 )
	{
		/*
		 * Determine spawn chance
		 */
		//g_spawnChance = 1 + ((getMicroSeconds() / 1000) - g_timeStart) / 10;
		g_spawnChance = 1 + (int)((1.0f + sinf(3.14f * 0.125f * (((float)(getMicroSeconds()) / 1000.0f) - g_timeStart) / 1000.0f) * 0.5f) * 100.0f - 40.0f);
		g_spawnChance = 200;
		
		/*
		 * Clear all particle bins
		 */
		for( uint16_t y = 0; y < SCREEN_HEIGHT; ++y )
		{
			g_particleList.bins[y].size = 0;
		}
		
		/*
		 * Move all entities into particle bins
		 */
		for( entity_list_size i = 0; i < MAX_ENTITIES; ++i )
		{
			// Read entity info
			const volatile entity_info& entityInfo = g_entityList.entities[i];
			
			// Check for valid depth
			if( entityInfo.z > 0 )
			{
				// Determine screen-space coordinates
				uint16_t x = (uint16_t)entityInfo.x;
				uint16_t y = (uint16_t)entityInfo.y;
				uint16_t z = (uint16_t)entityInfo.z;
				
				// Construct particle info
				particle_info particleInfo;
				particleInfo.x = x;
				particleInfo.z = z;
				particleInfo.palette = entityInfo.palette;
				
				// Move into scanline bin
				((particle_info*)g_particleList.bins[y].particles)[g_particleList.bins[y].size++] = particleInfo;
				
#if defined(PARTICLE_DUPLICATION_TEST)
				for( uint8_t k = 0; k < 8; ++k )
				{
					x += 1;
					z += 1;
					y += 1;
					if( y < SCREEN_HEIGHT )
					{
						particleInfo.x = x;
						particleInfo.z = z;
						((particle_info*)g_particleList.bins[y].particles)[g_particleList.bins[y].size++] = particleInfo;
					}
					else
					{
						break;
					}
				}
#endif
			}
		}
	}
	else
	{
		// Move entities
		const entity_list_size chunkSize = (MAX_ENTITIES / (NUM_THREADS - 1));
		const entity_list_size start = (threadId - 1) * chunkSize;
		if( threadId == (NUM_THREADS - 1) )
		{
			// Remainder
			think( start, MAX_ENTITIES );
		}
		else
		{
			think( start, start + chunkSize );
		}
	}
}

/*
 * Horizontal scatter/scanline pass
 * 
 * Scatter subpass:
 * 1. Clear pixel data in local scanline memory.
 * 2. Iterate over particle list, perform depth test and perform conditional write to local scanline memory (scatter).
 * 3. Write local scanline memory to framebuffer.
 * 
 * Scanline subpass:
 * 1. Divides the screen into rows spanning the entire screen.
 * 2. Extends (scattered) particle footprints horizontally.
 */
void scHorizontal( void * pLocal, int threadId, dvi_frame * pFrame )
{
	const uint32_t nScanlineBytes = sizeof( dvi_pixel ) * SCREEN_WIDTH;

	// Determine number of scanlines for this particular thread
	const uint8_t n = (SCREEN_HEIGHT / NUM_THREADS) + 1;
	
	// Scanline horizontal pass
	for( uint8_t i = 0; i < n; ++i )
	{
		// Check if scanline is within bounds
		uint16_t scanline = i * NUM_THREADS + threadId;
		uint16_t r = SCREEN_HEIGHT_START + scanline;
		if( r >= ( SCREEN_HEIGHT + SCREEN_HEIGHT_START ) )
		{
			break;
		}
		
		/**
		 * Workaround for first-row bug in vertical pass
		 * 
		 * The first scanline within the SCREEN (y=SCREEN_HEIGHT_START) will not contain any particle footprints.
		 */
		bool bSkipParticles = false; //(r == SCREEN_HEIGHT_START);
		
		/** Scatter subpass **/
		{
			// Clear pixel data
			memset( pLocal, 0, sizeof( dvi_pixel ) * SCREEN_WIDTH );
			
			// Color borders
			if(0)
			{
				memset( &(((dvi_pixel *)pLocal)[0]), 0x50, sizeof( dvi_pixel ) * SCREEN_BORDER_WIDTH );
				memset( &(((dvi_pixel *)pLocal)[SCREEN_WIDTH - SCREEN_BORDER_WIDTH]), 0x50, sizeof( dvi_pixel ) * SCREEN_BORDER_WIDTH );
			}
			
			if( !bSkipParticles )
			{
				// Iterate over binned particle list for current scanline
				const volatile particle_bin& bin = g_particleList.bins[scanline];
				const particle_bin_size binSize = bin.size;
				for( particle_bin_size j = 0; j < binSize; ++j )
				{
					// Retrieve particle info
					const volatile particle_info& particleInfo = bin.particles[j];
					
					// Generate new footprint
					particle_footprint pfNew;
					pfNew.data.ix = 0;							// initial unset value
					pfNew.data.z = particleInfo.z;				// copied directly from particle info
					pfNew.data.intensity = particleInfo.z;		// based on depth
					pfNew.data.palette = particleInfo.palette;	// custom palette
				
					// Read current footprint (if any) from local scanline memory and perform depth testing
					// Depth testing checks for smaller values instead of smaller-or-equal values to save on potential writes.
					particle_footprint pfCurrent;
					pfCurrent.pixel = ((dvi_pixel *)pLocal)[particleInfo.x];
					if( pfCurrent.data.z < pfNew.data.z )
					{
						// Scatter footprint into local scanline memory
						// (bounds checking for particleInfo.x omitted, it is assumed that these are within bounds)
						((dvi_pixel *)pLocal)[particleInfo.x] = pfNew.pixel;
					}
				}
			}
		}
		
		/** Scanline subpass **/
		if( !bSkipParticles )
		{
			// Circular buffer (queue) of particles
			// Maximum size is really particle radius + 1, but for performance reasons (modulus with power-of-two) we use the power-of-two particle radius
			const uint8_t queueSize = MAX_PARTICLE_RADIUS;
			particle_queue queue[queueSize];
			memset( queue, 0, sizeof( queue ) );
			
			uint16_t qCurrent = 0, qEnd = 0;
			
			for( uint16_t j = 0; j < (SCREEN_WIDTH - SCREEN_BORDER_WIDTH); ++j )
			{
				// Read particle footprint at target pixel in local scanline memory
				{
					// Target pixel: current pixel + radius
					uint16_t t = j + MAX_PARTICLE_RADIUS + 1;
					
					if( t < SCREEN_WIDTH )
					{
						particle_footprint pfCurrent;
						pfCurrent.pixel = ((dvi_pixel *)pLocal)[t];
						
						// Check if particle footprint is valid
						if( pfCurrent.data.z > 0 )
						{
							// Determine half particle extent size from depth
							uint8_t halfExtentSize = 1 + depthToRadius( pfCurrent.data.z );
							
							// Set initial local x coordinate
							queue[qEnd % queueSize].ia = t - halfExtentSize + 1;
							
							// Put particle footprint in queue
							queue[qEnd % queueSize].footprint = pfCurrent;
							
							// Increment queue end pointer
							++qEnd;
						}
					}
				}
				
				// Check if the next particle in the queue is ready to be processed
				uint16_t qNext = (qCurrent + 1);
				if( qEnd > qNext )
				{
					if( queue[qNext % queueSize].footprint.data.z > 0 && j >= queue[qNext % queueSize].ia )
					{
						if( queue[qNext % queueSize].footprint.data.z < queue[qCurrent % queueSize].footprint.data.z )
						{
							qCurrent = qNext;
						}
					}
				}
				
				// Process current particle footprint in queue, if any
				if( qCurrent != qEnd && queue[qCurrent % queueSize].footprint.data.z > 0 )
				{
					// Check if local x (starting) coordinate is equal or bigger than the current x coordinate
					if( j >= queue[qCurrent % queueSize].ia )
					{
						// Determine half particle extent size from depth
						uint8_t halfExtentSize = 1 + depthToRadius( queue[qCurrent % queueSize].footprint.data.z );

						// Update local x coordinate (relative [0, radius + 1])
						queue[qCurrent % queueSize].footprint.data.ix = (j - queue[qCurrent % queueSize].ia);
						
						// Write to local scanline memory
#if defined(DEBUG_HORIZONTAL_PASS)
						((dvi_pixel *)pLocal)[ j ] = PIX_CONST( (queue[qCurrent % queueSize].footprint.data.z * 40), 64, 0 );
#else
						((dvi_pixel *)pLocal)[ j ] = queue[qCurrent % queueSize].footprint.pixel;
#endif

						// Check if right bound of current particle has been reached
						if( queue[qCurrent % queueSize].footprint.data.ix >= ((halfExtentSize * 2 - 1) - 1) )
						{
							// Consider next particle in queue
							++qCurrent;
						}
					}
				}
			}
		}
		
		// Write scanline from shared memory to framebuffer
		{
			memcpy( &pFrame->line[ r ].col[ SCREEN_WIDTH_START ], pLocal, nScanlineBytes );
		}
		
		// Flush scanline from memory (prevents framebuffer offset issues)
		mc_flush( pFrame->line[ r ].col[ SCREEN_WIDTH_START ] );
	}
}

/*
 * Vertical scanline pass
 * 
 * 1. Divides the screen into columns of width equal to ( cache line size / dvi_pixel ).
 * 2. Extends (horizontally extended) particle footprints vertically.
 * 3. Performs shading of particle footprints and outputs final color.
 * 
 * Framebuffer access MUST be aligned with CACHE_SIZE_BYTES to avoid cache race issues due to multithreaded rendering.
 * 
 */
void scVertical( void * pLocal, int threadId, dvi_frame * pFrame )
{
	const uint32_t nColumnWidth = 8;
	
	// MUST BE ALIGNED WITH CACHE_SIZE_BYTES
	
	// Determine number of scanlines for this particular thread
	const uint8_t n = (SCREEN_WIDTH / NUM_THREADS / nColumnWidth) + 1;
	
	for( uint8_t i = 0; i < n; ++i )
	{
		const uint16_t offset = SCREEN_WIDTH_START + i * NUM_THREADS * nColumnWidth;
		const uint16_t c = offset + threadId * nColumnWidth;
		
		if( c >= SCREEN_WIDTH )
		{
			break;
		}
		
		/*
		 * Column-based structuring in local memory
		 */
		// Read column from framebuffer into shared memory
		if( 1 )
		{
			// Check if current column is within screen bounds
			if( ( c + nColumnWidth ) <= SCREEN_WIDTH )
			{
				for( uint16_t y = 0; y < SCREEN_HEIGHT; ++y )
				{
					for( uint8_t q = 0; q < nColumnWidth; ++q )
					{
						((rgb565_pixel *)pLocal)[ y + SCREEN_HEIGHT * q ] = pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c + q ];
					}
				}
			}
			else
			{
				for( uint16_t y = 0; y < SCREEN_HEIGHT; ++y )
				{
					((rgb565_pixel *)pLocal)[ y + SCREEN_HEIGHT ] = pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c ];
					for( uint8_t q = 1; q < nColumnWidth; ++q )
					{
						if( q < SCREEN_WIDTH )
						{
							((rgb565_pixel *)pLocal)[ y + SCREEN_HEIGHT * q ] = pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c + q ];
						}
					}
				}
			}
		}
		
		/** Scanline subpass **/
		{
			for( uint8_t q = 0; q < nColumnWidth; ++q )
			{
				// Circular buffer (queue) of particles
				// Maximum size is really particle radius + 1, but for performance reasons (modulus with power-of-two) we use the power-of-two particle radius
				const uint8_t queueSize = MAX_PARTICLE_RADIUS;
				particle_queue queue[queueSize];
				memset( queue, 0, sizeof( queue ) );
				
				uint8_t qCurrent = 0, qEnd = 0;
				
				for( int16_t j = -queueSize; j < SCREEN_HEIGHT; ++j )
				{
					// Read particle footprint at target pixel in local scanline memory
					{
						// Target pixel: current pixel + maximum half extent size
						int16_t t = j + MAX_PARTICLE_RADIUS + 1;
						
						if( t >= 0 && t < SCREEN_HEIGHT )
						{
							particle_footprint pfCurrent;
							pfCurrent.pixel = ((dvi_pixel *)pLocal)[t + SCREEN_HEIGHT * q];
							
							// Check if particle footprint is valid
							if( pfCurrent.data.z > 0 )
							{
								// Determine half particle extent size from depth
								uint8_t halfExtentSize = 1 + depthToRadius( pfCurrent.data.z );
					
								// Set initial local start and stop y coordinates
								queue[qEnd].ia = MAX(t - halfExtentSize + 1, 0);	// clamp: [0, N]
								queue[qEnd].ib = ((halfExtentSize * 2) - 1) - 1; //radius * 2 + 1;
								
#if defined(PARTICLE_SHAPE_CIRCLES)
								// Brute force particle size
								uint8_t offset = 0;
								for( uint8_t k = 0; k < (halfExtentSize + 1); ++k )
								{
									if( !pointInCircleAbs( pfCurrent.data.ix, k, halfExtentSize ) )
									{
										++offset;
									}
									else
									{
										break;
									}
								}
								if( offset < (halfExtentSize + 1) )
								{
									queue[qEnd].ia += offset; 
									queue[qEnd].ib -= ( offset * 2 + 0 ); 
									
									// Put particle footprint in queue
									queue[qEnd].footprint = pfCurrent;
									
									// Increment queue end pointer
									qEnd = (qEnd + 1) % queueSize;
								}
#else
								// Put particle footprint in queue
								queue[qEnd].footprint = pfCurrent;
								
								// Increment queue end pointer
								qEnd = (qEnd + 1) % queueSize;
#endif
							}
						}
					}
					if( j < 0 ) continue;

					// Check if the next particle in the queue is ready to be processed
					{
						uint8_t qNext = (qCurrent + 1) % queueSize;
						if( queue[qNext].footprint.data.z > 0 && j >= (queue[qNext].ia) )
						{
							if( queue[qNext].footprint.data.z < queue[qCurrent].footprint.data.z )
							{
								qCurrent = qNext;
							}
						}
					}
					
					#define PIX_WRAP_CONST(_r,_g) PIX_CONST(((_r)<0)?(-(_r)):(_r),((_g)<0)?(-(_g)):(_g),((_r)<0)?(((_g)<0)?255:128):(((_g)<0)?96:0))
					
					// Process current particle footprint in queue, if any
					if( qCurrent != qEnd && queue[qCurrent].footprint.data.z > 0 )
					{
						// Check if local y (starting) coordinate is equal or bigger than the current y coordinate
						if( j >= (queue[qCurrent].ia) )
						{
							{
								// Write to local scanline memory
								{
#if 1
									// Determine half particle extent size from depth
									uint8_t halfExtentSize = depthToRadius( queue[qCurrent].footprint.data.z );
									
									//((dvi_pixel *)pLocal)[ j + SCREEN_HEIGHT * q ] = PIX_WRAP_CONST(
									//	4 * ((queue[qCurrent].footprint.data.ix) - halfExtentSize - 1),
									//	4 * ((j - queue[qCurrent].ia) - halfExtentSize - 1)
									//);
									
									uint8_t extentSize = CLAMP( halfExtentSize, 0, 16 ) * 2 + 1;
									uint8_t ix = CLAMP( (queue[qCurrent].footprint.data.ix), 0, extentSize );
									uint8_t iy = CLAMP( (j - queue[qCurrent].ia), 0, extentSize );
									
									if( 1 )
									{										
										((uint32_t *)pLocal)[ j + SCREEN_HEIGHT * q ] = ( uint32_t )SPRITE_DOMOKUN_PTRS[ halfExtentSize ][ iy * extentSize + ix ];
									}
									else
									{
										if( threadId == 0 )
										{
											printf( "ix: %u, iy: %u, extentSize: %u\n", ix, iy, extentSize );
											sleep( 1 );
										}
										((dvi_pixel *)pLocal)[ j + SCREEN_HEIGHT * q ] = PIX_CONST( 4 * ix, 4 * iy, 4 * extentSize );
									}
#else
	#if 0
									float intensity = (float)((255 - queue[qCurrent].footprint.data.z) / 255.0f);
									uint8_t r, g, b;
									paletteToRGB( queue[qCurrent].footprint.data.palette, r, g, b );
									
									((dvi_pixel *)pLocal)[ j + SCREEN_HEIGHT * q ] = PIX_CONST(
										MAX(r * intensity, 0),
										MAX(g * intensity, 0),
										MAX(b * intensity, 0)
									);
	#else
									// Determine half particle extent size from depth
									//uint8_t halfExtentSize = 1 + depthToRadius( queue[qCurrent].footprint.data.z );
									
									uint8_t r, g, b;
									paletteToRGB( queue[qCurrent].footprint.data.palette, r, g, b );
									
									((dvi_pixel *)pLocal)[ j + SCREEN_HEIGHT * q ] = PIX_CONST(
										MAX(r / ((queue[qCurrent].footprint.data.z - 0) / 48), 0),
										MAX(g / ((queue[qCurrent].footprint.data.z - 0) / 48), 0),
										MAX(b / ((queue[qCurrent].footprint.data.z - 0) / 48), 0)
									);
	#endif
#endif
								}
							}

							// Check if lower bound of current particle has been reached
							if( (j - queue[qCurrent].ia) >= queue[qCurrent].ib )
							{
								// Consider next particle in queue
								qCurrent = (qCurrent + 1) % queueSize;
							}
						}
					}
				}
			}
		}

		// Write column from shared memory into framebuffer
		if( 1 )
		{
			// Check if current column is within screen bounds
			if( ( c + nColumnWidth ) <= SCREEN_WIDTH )
			{
				for( uint16_t y = 0; y < SCREEN_HEIGHT; ++y )
				{
					for( uint8_t q = 0; q < nColumnWidth; ++q )
					{
						pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c + q ] = ((rgb565_pixel *)pLocal)[ y + SCREEN_HEIGHT * q ];
					}

					// Flush column from memory (prevents framebuffer offset issues)
					mc_flush( pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c ] );
				}
			}
			else
			{
				for( uint16_t y = 0; y < SCREEN_HEIGHT; ++y )
				{
					pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c ] = ((rgb565_pixel *)pLocal)[ y + SCREEN_HEIGHT ];
					for( uint8_t q = 1; q < nColumnWidth; ++q )
					{
						if( q < SCREEN_WIDTH )
						{
							pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c + q ] = ((rgb565_pixel *)pLocal)[ y + SCREEN_HEIGHT * q ];
						}
					}

					// Flush column from memory (prevents framebuffer offset issues)
					mc_flush( pFrame->line[ y + SCREEN_HEIGHT_START ].col[ c ] );
				}
			}
		}
	}
}

/*
 * Main thread function
 */
void * scFunc( void * args )
{
	thInfo *info = ( thInfo * )args;
	
	struct timeval lasttime, currenttime;
	gettimeofday( &lasttime, NULL );
	
	unsigned int threadId = info->id;

	for( int i = 0; i < NUM_THREADS; ++i )
	{
		*(((unsigned int *)g_sharedMem[i].pNOC) + threadId) = 0;
		*(((unsigned int *)g_sharedMem[i].pLocal) + threadId) = 0;
	}
	
	// Seed RNG
	srand(get_clock() + threadId);

	FlushDCache();
	phase();
	
	// Check if this is the master/debug thread
	const bool bMasterThread = ( info-> id == 0 );
	
	// Allocate scanline into shared memory
	void * pLocal = smalloc( localMemSize );
	if( !pLocal )
	{
		ERRTHREAD( "Could not allocate shared memory (threadId: %u)", threadId );
	}
	
	while( 1 )
	{
		// Screen setup
		{
			// Synchronize

			CUSTOM_BARRIER( 1 );
			//FlushDCache();
			
			if( bMasterThread )
			{
				// Draw FPS
				{
					gettimeofday( &currenttime, NULL );
					long int timeDiff = ( ( currenttime.tv_usec + currenttime.tv_sec * 1000000 ) - ( lasttime.tv_usec + lasttime.tv_sec * 1000000 ) );

					char buf[32] = {0};
					snprintf( buf, 31, "fps: %d Hz (%d us) | c: %u", (int)(1000 / ( timeDiff / 1000 )), (int)( timeDiff ), g_spawnChance );
					drawstring( 10, 10, buf, PIX_CONST( 255, 255, 255 ) );
					
					lasttime = currenttime;
				}
				
				/*
				 * Flip the buffer
				 * 
				 * This call performs a vertical synchronization, so the effective upper bound of our renderer appears to be 30Hz.
				 */
				render_flip_buffer();
				
				// Clear the entire framebuffer
				for( uint16_t j = 0; j < DVI_HEIGHT; ++j )
				{
					memset( render_get_buffer()->line[ j ].col, 0x00, sizeof( dvi_pixel ) * DVI_WIDTH );
				}
			}
			
			// Synchronize
			FlushDCache();
			CUSTOM_BARRIER( 2 );
		}

		struct timeval timeVbegin, timeVend, timeHbegin, timeHend, timeGbegin, timeGend;
		
		/*
		 * Flush cache to ensure that the (outdated) particle list and entity list aren't left in the cache
		 */
		FlushDCache();

		// Generate pass
		gettimeofday( &timeGbegin, NULL );
		if( 1 )
		{
			scGenerate( pLocal, threadId );
		}
		gettimeofday( &timeGend, NULL );
		CUSTOM_BARRIER( 3 );
		
		/*
		 * Flush cache to ensure that the (outdated) particle list and entity list aren't left in the cache
		 */
		FlushDCache();
		
		// Get current render buffer
		dvi_frame * frame = render_get_buffer();

		// Horizontal pass
		gettimeofday( &timeHbegin, NULL );
		{
			scHorizontal( pLocal, threadId, frame );
		}
		gettimeofday( &timeHend, NULL );
		
#if !defined(DEBUG_HORIZONTAL_PASS)
		// Synchronize between passes
		FlushDCache();
		CUSTOM_BARRIER( 4 );
		
		// Vertical pass
		gettimeofday( &timeVbegin, NULL );
		{
			scVertical( pLocal, threadId, frame );
		}
		gettimeofday( &timeVend, NULL );
#endif
		
		drawimage( ( ( dvi_pixel * )SPRITE_DOMOKUN_PTRS[ 15 ] ), 31, 31, 50, 50, ALPHA_COLOR, frame );
#ifdef COCKPIT
		drawimage( ( ( dvi_pixel * )SPRITE_COCKPIT_DATA ), SPRITE_COCKPIT_WIDTH, SPRITE_COCKPIT_HEIGHT, COCKPIT_START_X, COCKPIT_START_Y, ALPHA_COLOR, frame );
#endif

		// Debug timing
		if( bMasterThread )
		{
			long int timeGdiff = ( ( timeGend.tv_usec + timeGend.tv_sec * 1000000 ) - ( timeGbegin.tv_usec + timeGbegin.tv_sec * 1000000 ) );
			long int timeHdiff = ( ( timeHend.tv_usec + timeHend.tv_sec * 1000000 ) - ( timeHbegin.tv_usec + timeHbegin.tv_sec * 1000000 ) );
			long int timeVdiff = ( ( timeVend.tv_usec + timeVend.tv_sec * 1000000 ) - ( timeVbegin.tv_usec + timeVbegin.tv_sec * 1000000 ) );

			char buf[64] = {0};
			snprintf( buf, 63, "passes (g:%d us h:%d us, v:%d us)", (int)( timeGdiff ), (int)( timeHdiff ), (int)( timeVdiff ) );
			drawstring( 10, 30, buf, PIX_CONST( 255, 255, 255 ) );
		}
	}
	
	return NULL;
}

/*
 * Transposes a sprite.
 * 
 * \param data is an array with raw pixel data.
 * \param dimSize is the size of both dimensions (implies that they must be the same).
 * \param intensity determines the color intensity.
 */
void preprocessSprite( unsigned int *data, uint16_t dimSize, float intensity )
{
	unsigned int tmp[ dimSize ];
	
	uint16_t start = 0;
	
	for( uint16_t col = 0; col < dimSize; ++col )
	{
		for( uint16_t j = start; j < dimSize; ++j )
		{
			tmp[ j ] = data[ col * dimSize + j ];
		}
		
		for( uint16_t j = start; j < dimSize; ++j )
		{
			data[ col * dimSize + j ] = data[ j * dimSize + col ];
			data[ j * dimSize + col ] = tmp[ j ];
		}
		
		start++;
	}
}

/*
 * Preprocess all sprites and transpose them from column-based to row-based.
 */
void preprocessSprites()
{
	uint8_t i = 0;
	for( i = 0; i < sizeof( SPRITE_DOMOKUN_PTRS ) / sizeof( SPRITE_DOMOKUN_PTRS[ 0 ] ); ++i )
	{
		preprocessSprite( SPRITE_DOMOKUN_PTRS[ i ], i * 2 + 1, 1.0f );
	}
}

/*
 * Main function
 */
int main(int argc, char **argv)
{
	preprocessSprites();
	
	printf( "long unsigned int: %lu, uint64_t: %lu, uint32_t: %lu\n", sizeof( long unsigned int ), sizeof( uint64_t ), sizeof( uint32_t ) );
	
	size_t localMemSize  = GetLocalMemSize();
	size_t fifoMemSize   = GetFifoMemSize();
	size_t dCacheMemSize = GetDCacheSize();
	
	printf( "Local Memory Size: %lu\n", localMemSize );
	printf( "FIFO Memory Size: %lu\n", fifoMemSize );
	printf( "Data Cache Memory Size: %lu\n", dCacheMemSize );
	printf( "NOC_ADDR_LOCAL_WIDTH: %u\n", NOC_ADDR_LOCAL_WIDTH );
	
	if( 0 )
	{
		char *currentPath = getcwd( NULL, 0 );
		printf( "Current working directory: %s\n", currentPath );
		free( currentPath );
	}
	
	/*
	 * Reset DVI controller
	 * 
	 * Prevents framebuffer shifting on exit.
	 */
	dvi_turn_off();
	dvi_turn_on();
	
	if(render_init(1) != RENDER_OK)
	{
		printf("Error: init display!\n");
		return 1;
	}
	
	/*
	 * Reset DVI controller
	 * 
	 * Prevents framebuffer shifting on exit.
	 */
	dvi_turn_off();
	dvi_turn_on();

	// allocate fifo (shared) memory
	for( uint8_t i = 0; i < NUM_THREADS; ++i )
	{
		int core = i + 1;
		void * p = dRemoteCall((void*(*)(void*))fmalloc,(void*)(sharedMemLocalSize),core);
		if( p )
		{
			g_sharedMem[ i ].pLocal = MAKE_LOCAL_FIFOMEM_PTR( p );
			g_sharedMem[ i ].pNOC = MAKE_NOC_FIFOMEM_PTR( p, core );
			printf( "fmalloc allocated for core %u (p: 0x%08x, l: 0x%08x, n: 0x%08x)\n", core, (unsigned int)p, (unsigned int)g_sharedMem[ i ].pLocal, (unsigned int)g_sharedMem[ i ].pNOC );
		}
		else
		{
			ERREXIT2( "fmalloc failed for core %u", core );
		}
	}
	
	// Initialize entities
	initializeEntityList();
	
	// Initialize start timer
	g_timeStart = (float)(getMicroSeconds()) / 1000.0f;
	
	// Allocate particle list (based on maximum particles per bin)
	{
		uint16_t nBins = SCREEN_HEIGHT;
		for( uint16_t i = 0; i < nBins; ++i )
		{
			// Allocate bin
			g_particleList.bins[i].particles = (particle_info*)malloc( sizeof( particle_info ) * BIN_MAX_PARTICLES );
			g_particleList.bins[i].size = 0;
		}
	}
		
	// initialize barrier
	VERIFY( pthread_barrier_init( &bar, NULL, NUM_THREADS ), 0 );
	
	pid_t pids[ NUM_THREADS ];
	
	for( uint8_t i = 0; i < NUM_THREADS; ++i )
	{
		info[ i ].id = i;
	}
		
	FlushDCache();
	
	for( uint8_t i = 0; i < NUM_THREADS; ++i )
	{		
		if( int e = CreateProcess( pids[ i ], ( pthread_function * )scFunc, ( void * )&info[ i ], PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, i + 1 ) )
		{
			ERREXIT2( "Process number %u creation failed: %d", i + 1, e );
		}
		
		if( int e = SetProcessFlags( pids[ i ], PROC_FLAG_JOINABLE, i + 1 ) )
		{
			ERREXIT2("While setting process number %u flags: %d", i + 1, e );
		}
	}
	
	for( uint8_t i = 0; i < NUM_THREADS; ++i )
	{
		if( int e = StartProcess( pids[ i ], i + 1 ) )
		{
			ERREXIT2("Could not start process number %u: %d", i + 1, e );
		}
	}

	// FIFOs are destroyed when the pointers goes out of scope
	for( uint8_t i = 0; i < NUM_THREADS; ++i )
	{
		if( int e = WaitProcess( pids[ i ], NULL, i + 1 ) )
		{
			ERREXIT2( "Waiting on thread %i@%i: %i\n", pids[ i ], i + 1, e );
		}
	}
	
	render_destroy();
	
	printf("Done\n");
	
	return 0;
} 
