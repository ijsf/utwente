There are several projects in Module 3.
- QuadratureEncoder: A VHDL implementation of a quadrature encoder.
  Contains a test bench and can be simulated in ModelSim.

- QE_Avalon_Bus: The same quadrature encoder, but as an IP block
  used in building a SOPC. The software for this SOPC resides in
  the ESL/Projects/workspace/software/QuadAvalon(_bsp) folders.
  
- Dual_QE_Avalon_Bus: A project with a Nios SOPC and two quadrature
  encoder IP blocks used for the JIWY setup (Embedded Systems Lab).
  
- PWM: A VHDL implementation of PWM.

- PWM_Avalon: A project with a Nios SOPC and a PWM IP block used for
  the JIWY setup (Embedded Systems Lab).
  
- JIWY: A project with a Nios SOPC and two quadrature encoder, two
  PWM, and an SPI IP blocks used for the JIWY setup
  (Embedded Systems Lab).
  
- JIWY_SPI_VGA: Similar to JIWY, but it uses a custom SPI IP block,
  and connects to the VGA IP block directly, so it is completely 
  separated from the NIOS. Works like a champ!
