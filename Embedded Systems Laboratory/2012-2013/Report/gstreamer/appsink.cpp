/*
	Embedded Systems Laboratory 2013
	University of Twente

	FPGA framebuffer streamer

	(C) 2013, C.E. Etheredge, O. Meteer
*/

#include <gst/gst.h>
#include <glib.h>
#include <gst/app/gstappsrc.h>
#include <gst/app/gstappsink.h>
#include <gst/app/gstappbuffer.h>

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/time.h>

#include <linux/types.h>
#include <linux/spi/spidev.h>

///////////////////////////////////////////////////////////
//
// Configuration
//
///////////////////////////////////////////////////////////

static int		g_iFrameWidth 	     = 160;
static int		g_iFrameHeight       = 120;
static int		g_iFrameRate         = 30;
static int		g_iFrameBitsPerPixel = 16;		// RGB565
static int		g_iFrameDepth        = 16;		// RGB565
static int		g_iFrameEndian       = G_LITTLE_ENDIAN;

static int		g_iInputWidth        = 320;		// camera input width (implies resizing, 0 for no resizing)
static int		g_iInputHeight 	     = 240;		// camera input height

// NOTE: These are the preferred settings, but spidev may override these. Be sure to check the program output!
static uint8_t		g_iSPILSB            = 0;		// MSB first
static uint8_t		g_iSPIMode           = SPI_MODE_0;
static uint32_t 	g_iSPISpeed          = 8000000;		// SPI clock
static uint16_t		g_iSPIDelay          = 0;		// SPI delay in usecs
static uint8_t		g_iSPIBitsPerWord    = 8;

/*
 * Maximum size of a single SPI message
 *
 * Gumstix Overo: 4096 (1 kernel page)
 * Raspberry Pi: 4096 (1 kernel page)
 */
static const size_t	g_nMessageSizeMax = 4096;

///////////////////////////////////////////////////////////
//
// Error handling
//
///////////////////////////////////////////////////////////

static void errorReport( const char * szMessage )
{
	printf( "ERROR: %s\n", szMessage );
}

///////////////////////////////////////////////////////////
//
// SPI I/O
//
///////////////////////////////////////////////////////////
static int		g_fdSPI;

bool spiTransfer( int fd, void * pBufferTx, size_t nBufferTx )
{
	bool bSuccess = true;

	// Frame sync
	{
		uint32_t magic = 0xDDDDCCCC;
		write( fd, &magic, sizeof( magic ) );
	}
	// Frame contents, split into chunks of max message sizes
	{
		size_t nWritten = 0;
		size_t iOffset = 0;
		while( iOffset < nBufferTx )
		{
			size_t nMessageSize = (nBufferTx - iOffset);
			if( nMessageSize > g_nMessageSizeMax ) nMessageSize = g_nMessageSizeMax;

#if 0
			// Test pattern
			uint16_t dummy[g_nMessageSizeMax / 2];
			for( size_t q = 0; q < g_nMessageSizeMax / 2; ++q )
			{
				uint16_t value = 1 << ( q % 160 );
				dummy[q] = ( ( value & 0xFF ) << 8 ) + ( ( value & 0xFF00 ) >> 8 );
				//dummy[ q ] = value;
			}
			printf( "writing %u bytes\n", nMessageSize );
			size_t nWrote = write( fd, dummy, nMessageSize );
#else
			size_t nWrote = write( fd, (((uint8_t*)pBufferTx) + iOffset), nMessageSize );
#endif
			nWritten += nWrote;
			if( nWrote != nMessageSize )
			{
				printf( "ERROR: Cannot send SPI message (got %u expected %u)\n", nWrote, nMessageSize );
				bSuccess = false;
				break;
			}
			iOffset += g_nMessageSizeMax;

			// SPI message delay
			usleep( g_iSPIDelay );
		}
		printf( "Wrote single frame (written %u, expected %u bytes)\n", nWritten, nBufferTx );
	}

	// Framerate profiling
	{
		static timespec tsLast;
		static size_t iFrames = 0;

		timespec ts;
		clock_gettime( CLOCK_REALTIME, &ts );

		if( (ts.tv_sec - tsLast.tv_sec) >= 1 )
		{
			printf( "Framerate: %u\n", iFrames );
			iFrames = 0;
			tsLast = ts;
		}
		++iFrames;
	}

	if( bSuccess )
	{
		return true;
	}
	return false;
}

bool spiClose( int fd )
{
	close( fd );
	return true;
}

bool spiInitialize( const char * szDevice, uint8_t iSPIMode, int & fd )
{
	#define RETURN_CHECK( _setting, _msg, _check, _org )	if( iReturn == -1 || (_check) != (_org) ) { errorReport( _msg ); } else { printf( "[%s]: %u\n", _setting, _org ); }

	int iReturn;

	fd = open( szDevice, O_RDWR );
	if( fd >= 0 )
	{
		uint8_t		iSPILSBC = g_iSPILSB;
		uint8_t		iSPIModeC = g_iSPIMode;
		uint32_t 	iSPISpeedC = g_iSPISpeed;
		uint8_t		iSPIBitsPerWordC = g_iSPIBitsPerWord;

		// SPI read/write mode
		iReturn = ioctl( fd, SPI_IOC_WR_MODE, &iSPIMode );
		RETURN_CHECK( "SPI_IOC_WR_MODE", "Cannot set SPI write mode", iSPIModeC, iSPIMode );
		iReturn = ioctl( fd, SPI_IOC_RD_MODE, &iSPIMode );
		RETURN_CHECK( "SPI_IOC_RD_MODE", "Cannot set SPI read mode", iSPIModeC, iSPIMode );

		// Bits per word
		iReturn = ioctl( fd, SPI_IOC_WR_BITS_PER_WORD, &g_iSPIBitsPerWord );
		RETURN_CHECK( "SPI_IOC_WR_BITS_PER_WORD", "Cannot set SPI write bits per word", iSPIBitsPerWordC, g_iSPIBitsPerWord );
		iReturn = ioctl( fd, SPI_IOC_RD_BITS_PER_WORD, &g_iSPIBitsPerWord );
		RETURN_CHECK( "SPI_IOC_RD_BITS_PER_WORD", "Cannot set SPI read bits per word", iSPIBitsPerWordC, g_iSPIBitsPerWord );

		// LSB first
		iReturn = ioctl( fd, SPI_IOC_WR_LSB_FIRST, &g_iSPILSB );
		RETURN_CHECK( "SPI_IOC_WR_LSB_FIRST", "Cannot set SPI write LSB first", iSPILSBC, g_iSPILSB );
		iReturn = ioctl( fd, SPI_IOC_RD_LSB_FIRST, &g_iSPILSB );
		RETURN_CHECK( "SPI_IOC_RD_LSB_FIRST", "Cannot set SPI read LSB first", iSPILSBC, g_iSPILSB );

		// Max speed
		iReturn = ioctl( fd, SPI_IOC_WR_MAX_SPEED_HZ, &g_iSPISpeed );
		RETURN_CHECK( "SPI_IOC_WR_MAX_SPEED_HZ", "Cannot set SPI write speed", iSPISpeedC, g_iSPISpeed );
		iReturn = ioctl( fd, SPI_IOC_RD_MAX_SPEED_HZ, &g_iSPISpeed );
		RETURN_CHECK( "SPI_IOC_RD_MAX_SPEED_HZ", "Cannot set SPI read speed", iSPISpeedC, g_iSPISpeed );

		printf( "SPI Initialized:\n" );
		printf( "* Mode %u, BitsPerWord %u, MaxSpeedHz: %u\n", iSPIMode, g_iSPIBitsPerWord, g_iSPISpeed );

		return true;
	}
	else
	{
		errorReport( "Cannot open SPI device" );
	}

	return false;
}

///////////////////////////////////////////////////////////
//
// GStreamer
//
///////////////////////////////////////////////////////////

typedef struct UserData
{
} UserData_t;

void gstreamerDebugLog( GstDebugCategory* category, GstDebugLevel level,
                          const gchar* file, const char* function,
                          gint line, GObject* object, GstDebugMessage* message,
                          gpointer data )
{
	switch( level )
	{
		case GST_LEVEL_ERROR:
		case GST_LEVEL_WARNING:
			break;
		default:
			return;
	}
	printf( "DEBUG: %s %s:%d %s\n", file, function, line, gst_debug_message_get(message) );
}

gboolean gstreamerBufferCallback( GstAppSink * sink, UserData_t * data )
{
	GstBuffer * pGstBuffer = NULL;

	// Get buffer data
	g_signal_emit_by_name( sink, "pull-buffer", &pGstBuffer );

	// Verify buffer size (in bytes)
	size_t nBufferSizeExpected = g_iFrameWidth * g_iFrameHeight * ( g_iFrameBitsPerPixel >> 3 );
	size_t nBufferSize = GST_BUFFER_SIZE( pGstBuffer );
	if( nBufferSize != nBufferSizeExpected )
	{
		printf( "WARNING: Got buffer size %u, expected %u\n", nBufferSize, nBufferSizeExpected );
	}

	// Debugging
	if( 0 )
	{
		printf( "Got buffer: 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X\n",
			GST_BUFFER_DATA( pGstBuffer )[0],
			GST_BUFFER_DATA( pGstBuffer )[1],
			GST_BUFFER_DATA( pGstBuffer )[2],
			GST_BUFFER_DATA( pGstBuffer )[3],
			GST_BUFFER_DATA( pGstBuffer )[4],
			GST_BUFFER_DATA( pGstBuffer )[5],
			GST_BUFFER_DATA( pGstBuffer )[6],
			GST_BUFFER_DATA( pGstBuffer )[7]
		);
	}

	// Transmit data over SPI
	spiTransfer( g_fdSPI, GST_BUFFER_DATA( pGstBuffer ), nBufferSize );

	// Unreference buffer
	gst_buffer_unref( pGstBuffer );

	return TRUE;
}


gboolean gstreamerBusCall (GstBus *bus, GstMessage *msg, gpointer data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

///////////////////////////////////////////////////////////
//
// Main logic
//
///////////////////////////////////////////////////////////

int main( int argc, char *argv[] )
{
	GMainLoop *loop;

	GstElement *pipeline, *source, *resizer, *colorspace, *sink;
	GstBus *bus;
	guint bus_watch_id;

	UserData_t UserData;

	// GStreamer debug options
	gst_debug_remove_log_function( gst_debug_log_default );
	gst_debug_add_log_function( gstreamerDebugLog, NULL );
	gst_debug_set_active( TRUE );

	// GStreamer initialization
	gst_init (&argc, &argv);

	loop = g_main_loop_new (NULL, FALSE);

	// Check arguments
	if (argc != 3) {
		g_printerr ("Usage: %s <video input device> <spi output device>\n", argv[0]);
		return -1;
	}

	// Initialize SPI
	if( !spiInitialize( argv[2], g_iSPIMode, g_fdSPI ) )
	{
		return -1;
	}

	// Create gstreamer elements
	pipeline = gst_pipeline_new ("video-player");
	source   = gst_element_factory_make ("v4l2src", "file-source");
	resizer  = gst_element_factory_make( "videoscale", "resizer" );
	colorspace   = gst_element_factory_make( "ffmpegcolorspace", "video-colorspace" );
	sink     = gst_element_factory_make ("appsink", "video-output");

	if (!pipeline || !source || !resizer || !colorspace || !sink)
	{
		g_printerr ("One or more elements could not be created. Exiting. (0x%x 0x%x 0x%x 0x%x 0x%x)\n",
		(uint32_t)pipeline, (uint32_t)source, (uint32_t)resizer, (uint32_t)colorspace, (uint32_t)sink );
		return -1;
	}

	////////////////////////////////////
	//
	// Pipeline setup
	//
	////////////////////////////////////

	// Add all elements to the pipeline: v4l2src | resizer | ffmpegcolorspace | appsink
	gst_bin_add_many (GST_BIN (pipeline), source, resizer, colorspace, sink, NULL);

	// Set up appsink
	gst_app_sink_set_emit_signals( (GstAppSink *)sink, TRUE );	// enable push mode
	gst_app_sink_set_drop( (GstAppSink *)sink, TRUE );		// enable dropping of buffers on max queue
	gst_app_sink_set_max_buffers( (GstAppSink *)sink, 1 );	// set maximum buffer queue to 1

	// Set up appsink buffer callback
	g_signal_connect(
		sink,
		"new-buffer",
		G_CALLBACK( gstreamerBufferCallback ),
		&UserData
	);

	// set source parameters
	g_object_set( G_OBJECT( source ), "device", argv[1], NULL );

	GstElement * chain_cur = source, * chain_next;
	printf( "Setting up chain:\n" );

	size_t iChainCounter = 0;
	printf( "%u. source\n", iChainCounter++ );

	// set up caps for resizing
	if( g_iInputWidth && g_iInputHeight )
	{
		printf( "%u. resizer\n", iChainCounter++ );
		chain_next = resizer;

		GstCaps * pCaps = gst_caps_new_simple(
			// accept any input resolution in YUV
			"video/x-raw-yuv",
			NULL
		);

		if( !gst_element_link_filtered( chain_cur, chain_next, pCaps ) )
		{
			errorReport( "Could not link to resizer" );
			return -1;
		}
		gst_caps_unref( pCaps );

		chain_cur = chain_next;
	}

	// set up caps for colorspace input
	{
		printf( "%u. colorspace\n", iChainCounter++ );
		chain_next = colorspace;

		GstCaps * pCaps = gst_caps_new_simple(
			"video/x-raw-yuv",

			"width", G_TYPE_INT, g_iFrameWidth,
			"height", G_TYPE_INT, g_iFrameHeight,

			"framerate", GST_TYPE_FRACTION, g_iFrameRate, 1,
			NULL
		);

		if( !gst_element_link_filtered( chain_cur, chain_next, pCaps ) )
		{
			errorReport( "Could not link to colorspace" );
			return -1;
		}
		gst_caps_unref( pCaps );

		chain_cur = chain_next;
	}

	// set up caps for SPI sink
	{
		printf( "%u. sink (in %ux%ux%u bpp at %u fps, video/x-raw-rgb)\n", iChainCounter++, g_iFrameWidth, g_iFrameHeight, g_iFrameBitsPerPixel, g_iFrameRate );
		chain_next = sink;

		GstCaps * pCaps = gst_caps_new_simple(
			"video/x-raw-rgb",
			"width", G_TYPE_INT, g_iFrameWidth,
			"height", G_TYPE_INT, g_iFrameHeight,
			"bpp", G_TYPE_INT, g_iFrameBitsPerPixel,
			"depth", G_TYPE_INT, g_iFrameDepth,
			"endianness", G_TYPE_INT, g_iFrameEndian,
			NULL
		);

		if( !gst_element_link_filtered( chain_cur, chain_next, pCaps ) )
		{
			errorReport( "Could not link to sink" );
			return -1;
		}
		gst_caps_unref( pCaps );

		chain_cur = chain_next;
	}

	// Add message handler
	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	bus_watch_id = gst_bus_add_watch (bus, gstreamerBusCall, loop);
	gst_object_unref (bus);

	printf( "GStreamer initialized:\n" );
	printf( "* device: %s\n", argv[1] );
	printf( "* video: %ux%ux%u at %u fps\n", g_iFrameWidth, g_iFrameHeight, g_iFrameBitsPerPixel, g_iFrameRate );

	// Set pipeline to playing state
	gst_element_set_state (pipeline, GST_STATE_PLAYING);

	// Launch main loop
	g_print ("Running...\n");
	g_main_loop_run (loop);

	// Cleanup
	g_print ("Returned, stopping playback\n");
	gst_element_set_state (pipeline, GST_STATE_NULL);

	g_print ("Deleting pipeline\n");
	gst_object_unref (GST_OBJECT (pipeline));
	g_source_remove (bus_watch_id);
	g_main_loop_unref (loop);

	spiClose( g_fdSPI );

	return 0;
}

